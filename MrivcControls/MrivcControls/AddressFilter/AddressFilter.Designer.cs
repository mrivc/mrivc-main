﻿namespace MrivcControls
{
    partial class AddressFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressFilter));
            this.filterTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.addressTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.addressTreeList = new DevExpress.XtraTreeList.TreeList();
            this.item = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.id_parent = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.type = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.name = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.id_company = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.caption = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.imageCollection = new DevExpress.Utils.ImageCollection();
            this.adrGroupControl = new DevExpress.XtraEditors.GroupControl();
            this.selectedAdrListBox = new DevExpress.XtraEditors.ImageListBoxControl();
            this.parameterTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.saveFilterBtn = new DevExpress.XtraEditors.SimpleButton();
            this.openFilterBtn = new DevExpress.XtraEditors.SimpleButton();
            this.selectAllBtn = new DevExpress.XtraEditors.SimpleButton();
            this.cDataSet = new MrivcControls.Data.CDataSet();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu2 = new DevExpress.XtraBars.PopupMenu();
            ((System.ComponentModel.ISupportInitialize)(this.filterTabControl)).BeginInit();
            this.filterTabControl.SuspendLayout();
            this.addressTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addressTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adrGroupControl)).BeginInit();
            this.adrGroupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.selectedAdrListBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).BeginInit();
            this.SuspendLayout();
            // 
            // filterTabControl
            // 
            this.filterTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filterTabControl.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.filterTabControl.Appearance.Options.UseBackColor = true;
            this.filterTabControl.Location = new System.Drawing.Point(0, 0);
            this.filterTabControl.Name = "filterTabControl";
            this.filterTabControl.SelectedTabPage = this.addressTabPage;
            this.filterTabControl.Size = new System.Drawing.Size(595, 340);
            this.filterTabControl.TabIndex = 1;
            this.filterTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.addressTabPage,
            this.parameterTabPage});
            // 
            // addressTabPage
            // 
            this.addressTabPage.Controls.Add(this.splitContainerControl1);
            this.addressTabPage.Name = "addressTabPage";
            this.addressTabPage.Size = new System.Drawing.Size(588, 311);
            this.addressTabPage.Text = "Адреса";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.addressTreeList);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.adrGroupControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(588, 311);
            this.splitContainerControl1.SplitterPosition = 201;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // addressTreeList
            // 
            this.addressTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.item,
            this.id_parent,
            this.type,
            this.name,
            this.id_company,
            this.caption});
            this.addressTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addressTreeList.ImageIndexFieldName = "";
            this.addressTreeList.KeyFieldName = "";
            this.addressTreeList.Location = new System.Drawing.Point(0, 0);
            this.addressTreeList.Name = "addressTreeList";
            this.addressTreeList.OptionsBehavior.Editable = false;
            this.addressTreeList.OptionsSelection.MultiSelect = true;
            this.addressTreeList.OptionsView.ShowColumns = false;
            this.addressTreeList.OptionsView.ShowHorzLines = false;
            this.addressTreeList.OptionsView.ShowIndicator = false;
            this.addressTreeList.OptionsView.ShowVertLines = false;
            this.addressTreeList.ParentFieldName = "";
            this.addressTreeList.RootValue = null;
            this.addressTreeList.Size = new System.Drawing.Size(201, 311);
            this.addressTreeList.StateImageList = this.imageCollection;
            this.addressTreeList.TabIndex = 1;
            this.addressTreeList.BeforeExpand += new DevExpress.XtraTreeList.BeforeExpandEventHandler(this.addressTreeList_BeforeExpand);
            this.addressTreeList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.addressTreeList_MouseDown);
            this.addressTreeList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.addressTreeList_MouseMove);
            // 
            // item
            // 
            this.item.Caption = "item";
            this.item.FieldName = "item";
            this.item.Name = "item";
            // 
            // id_parent
            // 
            this.id_parent.Caption = "id_parent";
            this.id_parent.FieldName = "id_parent";
            this.id_parent.Name = "id_parent";
            // 
            // type
            // 
            this.type.Caption = "type";
            this.type.FieldName = "type";
            this.type.Name = "type";
            // 
            // name
            // 
            this.name.Caption = "name";
            this.name.FieldName = "name";
            this.name.MinWidth = 53;
            this.name.Name = "name";
            this.name.Visible = true;
            this.name.VisibleIndex = 0;
            // 
            // id_company
            // 
            this.id_company.Caption = "id_company";
            this.id_company.FieldName = "id_company";
            this.id_company.Name = "id_company";
            // 
            // caption
            // 
            this.caption.Caption = "caption";
            this.caption.FieldName = "caption";
            this.caption.Name = "caption";
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "archives.png");
            this.imageCollection.Images.SetKeyName(1, "customers1.png");
            this.imageCollection.Images.SetKeyName(2, "house.png");
            this.imageCollection.Images.SetKeyName(3, "city.png");
            this.imageCollection.Images.SetKeyName(4, "town.png");
            this.imageCollection.Images.SetKeyName(5, "rosette.png");
            // 
            // adrGroupControl
            // 
            this.adrGroupControl.Controls.Add(this.selectedAdrListBox);
            this.adrGroupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adrGroupControl.Location = new System.Drawing.Point(0, 0);
            this.adrGroupControl.Margin = new System.Windows.Forms.Padding(0);
            this.adrGroupControl.Name = "adrGroupControl";
            this.adrGroupControl.Size = new System.Drawing.Size(381, 311);
            this.adrGroupControl.TabIndex = 0;
            this.adrGroupControl.Text = "Выбранные адреса (Пустое поле = все адреса)";
            // 
            // selectedAdrListBox
            // 
            this.selectedAdrListBox.AllowDrop = true;
            this.selectedAdrListBox.DisplayMember = "Caption";
            this.selectedAdrListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectedAdrListBox.ImageIndexMember = "TypeID";
            this.selectedAdrListBox.ImageList = this.imageCollection;
            this.selectedAdrListBox.Location = new System.Drawing.Point(2, 22);
            this.selectedAdrListBox.Name = "selectedAdrListBox";
            this.selectedAdrListBox.Size = new System.Drawing.Size(377, 287);
            this.selectedAdrListBox.TabIndex = 0;
            this.selectedAdrListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.selectedAdrListBox_DragDrop);
            this.selectedAdrListBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.selectedAdrListBox_DragEnter);
            this.selectedAdrListBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.selectedAdrListBox_MouseDown);
            this.selectedAdrListBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.selectedAdrListBox_MouseUp);
            // 
            // parameterTabPage
            // 
            this.parameterTabPage.Name = "parameterTabPage";
            this.parameterTabPage.Size = new System.Drawing.Size(588, 311);
            this.parameterTabPage.Text = "Параметры";
            // 
            // saveFilterBtn
            // 
            this.saveFilterBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveFilterBtn.Image = global::MrivcControls.Properties.Resources.Save;
            this.saveFilterBtn.Location = new System.Drawing.Point(136, 342);
            this.saveFilterBtn.Name = "saveFilterBtn";
            this.saveFilterBtn.Size = new System.Drawing.Size(25, 23);
            this.saveFilterBtn.TabIndex = 6;
            this.saveFilterBtn.Click += new System.EventHandler(this.saveFilterBtn_Click);
            // 
            // openFilterBtn
            // 
            this.openFilterBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.openFilterBtn.Image = global::MrivcControls.Properties.Resources.Open;
            this.openFilterBtn.Location = new System.Drawing.Point(106, 342);
            this.openFilterBtn.Name = "openFilterBtn";
            this.openFilterBtn.Size = new System.Drawing.Size(24, 23);
            this.openFilterBtn.TabIndex = 5;
            this.openFilterBtn.Click += new System.EventHandler(this.openFilterBtn_Click);
            // 
            // selectAllBtn
            // 
            this.selectAllBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.selectAllBtn.Location = new System.Drawing.Point(4, 342);
            this.selectAllBtn.Name = "selectAllBtn";
            this.selectAllBtn.Size = new System.Drawing.Size(96, 23);
            this.selectAllBtn.TabIndex = 4;
            this.selectAllBtn.Text = "Выбрать все";
            this.selectAllBtn.Click += new System.EventHandler(this.selectAllBtn_Click);
            // 
            // cDataSet
            // 
            this.cDataSet.DataSetName = "CDataSet";
            this.cDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Удалить запись";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Очистить список";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.barManager1.MaxItemId = 3;
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Очистить список";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // popupMenu2
            // 
            this.popupMenu2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3)});
            this.popupMenu2.Manager = this.barManager1;
            this.popupMenu2.Name = "popupMenu2";
            // 
            // AddressFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.saveFilterBtn);
            this.Controls.Add(this.openFilterBtn);
            this.Controls.Add(this.selectAllBtn);
            this.Controls.Add(this.filterTabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "AddressFilter";
            this.Size = new System.Drawing.Size(598, 372);
            this.Load += new System.EventHandler(this.AddressFilter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.filterTabControl)).EndInit();
            this.filterTabControl.ResumeLayout(false);
            this.addressTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.addressTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adrGroupControl)).EndInit();
            this.adrGroupControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.selectedAdrListBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl filterTabControl;
        private DevExpress.XtraTab.XtraTabPage addressTabPage;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.GroupControl adrGroupControl;
        private DevExpress.XtraTab.XtraTabPage parameterTabPage;
        private DevExpress.XtraEditors.SimpleButton saveFilterBtn;
        private DevExpress.XtraEditors.SimpleButton openFilterBtn;
        private DevExpress.XtraEditors.SimpleButton selectAllBtn;
        private DevExpress.XtraTreeList.TreeList addressTreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn item;
        private DevExpress.XtraTreeList.Columns.TreeListColumn id_parent;
        private DevExpress.XtraTreeList.Columns.TreeListColumn type;
        private DevExpress.XtraTreeList.Columns.TreeListColumn name;
        private DevExpress.XtraTreeList.Columns.TreeListColumn id_company;
        private DevExpress.XtraTreeList.Columns.TreeListColumn caption;
        private Data.CDataSet cDataSet;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.PopupMenu popupMenu2;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraEditors.ImageListBoxControl selectedAdrListBox;
    }
}
