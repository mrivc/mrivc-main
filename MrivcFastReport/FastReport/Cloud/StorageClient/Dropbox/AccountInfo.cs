using System;
using System.Collections.Generic;
using System.Text;

namespace FastReport.Cloud.StorageClient.Dropbox
{
    /// <summary>
    /// Represents the information about dropbox storage account.
    /// </summary>
    public class AccountInfo
    {
        #region Fields

        private string login;
        private string password;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Gets or sets the user login.
        /// </summary>
        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        /// <summary>
        /// Gets or sets the user password.
        /// </summary>
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountInfo"/> class with a specified parameters.
        /// </summary>
        /// <param name="login">The user login.</param>
        /// <param name="password">The user password.</param>
        public AccountInfo(string login, string password)
        {
            this.login = login;
            this.password = password;
        }

        #endregion // Constructors
    }
}
