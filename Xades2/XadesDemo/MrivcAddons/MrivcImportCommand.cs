﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XadesDemo
{
    public abstract class MrivcImportCommand : MrivcCommand
    {
        protected string _importValueName;
        protected int _maxImportValue; //макс. количество добавлений элемента 
        int _importValueCounter = 0; //счетчик добавленных эл-ов
        string _response = "";

        public bool IsEmpty { get; private set; } = true;

        public MrivcImportCommand(string method, string service, string senderID) : base(method, service, senderID)
        {
            //_maxImportValue = maxImportValue;
        }

        public override string Execute()
        {
            IsEmpty = true;
            _response = "";
            _importValueCounter = 0;

            CreateXmlBody();
            if (mrivcXmlFormatter.Contains(new[] { _importValueName }))
            {
                _response = base.Execute();
                IsEmpty = false;
            }

            mrivcXmlFormatter.RemoveNodesByName(new[] { _importValueName });

            return _response;
        }

        public string Execute(bool execute)
        {
            IsEmpty = true;
            _response = "";
            _importValueCounter = 0;
            HasErrors = false;

            if (execute)
            {
                CreateXmlBody();
                if (mrivcXmlFormatter.Contains(new[] { _importValueName }))
                {
                    _response = base.Execute();
                    IsEmpty = false;
                }

                mrivcXmlFormatter.RemoveNodesByName(new[] { _importValueName });
            }

            return _response;
        }

        protected void CounterCheck()
        {
            _importValueCounter++;
            if (_importValueCounter == _maxImportValue)
            {
                _importValueCounter = 0;
                _response = base.Execute();
                IsEmpty = false;
                mrivcXmlFormatter.RemoveNodesByName(new[] { _importValueName });
            }
        }

        Tuple<string, List<Tuple<string[], string>>> AddChildElement(DataRow row, string service, string createMethod, string updateMethod, IEnumerable<Tuple<string[], string>> commonValues,
           IEnumerable<Tuple<string[], string>> createValues, IEnumerable<Tuple<string[], string>> updateValues)
        {
            var commonValuesList = new List<Tuple<string[], string>>();
            if (commonValues != null)
                foreach (Tuple<string[], string> value in commonValues)
                    commonValuesList.Add(new Tuple<string[], string>(value.Item1, row[value.Item2].ToString()));

            var createValuesList = new List<Tuple<string[], string>>();
            if (createValues != null)
                foreach (Tuple<string[], string> value in createValues)
                    createValuesList.Add(new Tuple<string[], string>(value.Item1, row[value.Item2].ToString()));

            var updateValuesList = new List<Tuple<string[], string>>();
            if (updateValues != null)
                foreach (Tuple<string[], string> value in updateValues)
                    updateValuesList.Add(new Tuple<string[], string>(value.Item1, row[value.Item2].ToString()));

            List<Tuple<string[], string>> values = commonValuesList;

            var templatePath = "";
            var childElementName = "";
            if (row["ID_CommandType"].ToString() == "0")
            {
                values.AddRange(createValuesList);
                templatePath = MrivcStatic.GisConfig.Services[service].Methods[createMethod].Template;
                childElementName = MrivcStatic.GisConfig.Services[service].Methods[createMethod].Template;
            }
            else if (row["ID_CommandType"].ToString() == "1")
            {
                values.AddRange(updateValuesList);
                templatePath = MrivcStatic.GisConfig.Services[service].Methods[updateMethod].Template;
                childElementName = MrivcStatic.GisConfig.Services[service].Methods[createMethod].Template;
            }

            return new Tuple<string, List<Tuple<string[], string>>>(templatePath, values);
        }

        protected XmlNode AddChildElement(DataRow row, string service, string createMethod, string updateMethod, IEnumerable<Tuple<string[], string>> commonValues,
            IEnumerable<Tuple<string[], string>> createValues, IEnumerable<Tuple<string[], string>> updateValues, string elementToAddPath)
        {
            var childElementParts = AddChildElement(row, service, createMethod, updateMethod, commonValues, createValues, updateValues);
            return AddChildElement(childElementParts.Item1, childElementParts.Item2, elementToAddPath);
        }

        protected XmlNode AddChildElement(DataRow row, string service, string createMethod, string updateMethod, IEnumerable<Tuple<string[], string>> commonValues,
            IEnumerable<Tuple<string[], string>> createValues, IEnumerable<Tuple<string[], string>> updateValues, XmlNode xmlNodeToAdd)
        {
            var childElementParts = AddChildElement(row, service, createMethod, updateMethod, commonValues, createValues, updateValues);
            return AddChildElement(childElementParts.Item1, childElementParts.Item2, xmlNodeToAdd);
        }

        protected abstract void CreateXmlBody();

    }
}
