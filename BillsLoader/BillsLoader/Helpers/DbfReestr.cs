﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data.Common;

namespace BillsLoader
{
    using BillsLoader.Properties;

    public class DbfReestr
    {
        public static DbDataReader GetDataReader(string fileName)
        {
            FileInfo fi  = new FileInfo(fileName);

            DbConnection con = GetConnection(fi.DirectoryName);
            DbCommand cmd = GetCommand();

            cmd.Connection = con;
            con.Open();
            cmd.CommandText = "select * from " + fi.Name;


            DbDataReader dr = cmd.ExecuteReader(CommandBehavior.Default);
            return dr;
        }

        private static DbConnection GetConnection(string path)
        {
            return
                new OleDbConnection(
                    "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=dBASE IV");
        }

        private static DbCommand GetCommand()
        {
            return new OleDbCommand();
        }
    }
}
