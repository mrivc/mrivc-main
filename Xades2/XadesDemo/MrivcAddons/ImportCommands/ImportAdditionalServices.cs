﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ImportAdditionalServices : MrivcImportCommand
    {
        

        public ImportAdditionalServices(string senderID) : base("importAdditionalServices", "NsiAsync", senderID)
        {
            _importValueName = "ImportAdditionalServiceType";
            Description = "Синхронизация услуг";
        }

        public new void Execute()
        {
            var exportDataProvider = new ExportDataProvider(SenderID);
            exportDataProvider.AddToReport = this.AddToReport;
            exportDataProvider.Description = this.Description;
            var dataProviderRegNumbers = "1";
            exportDataProvider.ExecuteToDataBase(dataProviderRegNumbers.Split(','));

            HasErrors = exportDataProvider.HasErrors;

            if (!exportDataProvider.HasErrors)
                base.Execute();

            if (!this.IsEmpty && !HasErrors)
                exportDataProvider.ExecuteToDataBase(dataProviderRegNumbers.Split(','));
        }

        protected override void CreateXmlBody()
        {
            var addServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.Prepare_Import_Additional_Services @senderID", new[,] { { "@senderID", SenderID } });

            var commonValues = new[]
            {
                new Tuple<string[],string>(new [] { "AdditionalServiceTypeName" },"AdditionalServiceTypeName"),
                new Tuple<string[],string>(new [] { "StringDimensionUnit" },"StringDimensionUnit"),
            };

            var updateValues = new[]
            {
                new Tuple<string[],string>(new [] { "ElementGuid" },"ElementGuid")
            };

            foreach (DataRow row in addServices.Select())
            {
                AddChildElement(row, "NsiAsync", "importAdditionalServices_add", "importAdditionalServices_update", commonValues, null, updateValues, "importAdditionalServicesRequest");
            }
        }
    }
}
