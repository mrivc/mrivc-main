﻿using System;
using System.Collections.Generic;
using System.Text;
using Payments.LogicClasses;
using System.Data;

namespace Payments.DS
{
    public class DAccess
    {
        private static DAccess instance;
        private static DModule dModule;
        private static CashTerminal cashTerminal;

        private ProgramSettings programSettings;
        private DataTable filterClient;
        
        private DAccess() {}

        public bool HasErrors { get; set; } = false;

        private ApplicationUser currentUser;
        public ApplicationUser CurrentUser
        {
            get
            {
                return currentUser;
            }
        }

        /// <summary>
        /// Доступ к драйверу кассы
        /// </summary>
        public static CashTerminal CashTerminal
        {
            get {
                if (cashTerminal == null)
                {
                    cashTerminal = new CashTerminal();
                }
                return cashTerminal;
            }
        }

        public void SetCurrentUser(ApplicationUser user)
        {
            currentUser = user;
        }

        public static DAccess Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DAccess();
                }
                return instance;
            }
        }

        public static DModule DataModule
        {
            get
            {
                if (dModule == null)
                {
                    dModule = new DModule();
                }
                return dModule;
            }
        }

        public ProgramSettings ProgramSettings
        {
            get {
                if (programSettings == null)
                {
                    programSettings = new ProgramSettings();
                }
                return programSettings;
            }
        }

        public DataTable FilterClient
        {
            get
            {
                if (filterClient == null)
                {
                    filterClient = DAccess.DataModule.FilterClientSelect();
                }
                return filterClient;
            }
        }

        private PaymentsDataSet.Cash_TypeDataTable cashType;
        public PaymentsDataSet.Cash_TypeDataTable CashType
        {
            get
            {
                if (cashType == null)
                {
                    cashType = new PaymentsDataSet.Cash_TypeDataTable();
                    DAccess.DataModule.CashTypeSelect(cashType);
                }
                return cashType;
            }
        }

        

        /// <summary>
        /// Проверка, принимаем ли мы платежи на расчетный счет данного клиента
        /// клиенты описаны в БД ЭЛЛИС Reports.Client_Detail
        /// </summary>
        /// <param name="idClient">Клиенты описаны в БД ЭЛЛИС Reports.Client_Detail</param>
        public bool CheckClientFilter(int idClient)
        {
            DataRow[] rows = FilterClient.Select("[ID_Company] = " + idClient.ToString());
            if (rows.Length > 0)
                return true;

            return false;
        }

        /// <summary>
        /// Возвращает префикс файла по коду клиента
        /// </summary>
        /// <param name="idClient">Клиенты описаны в БД ЭЛЛИС Reports.Client_Detail</param>
        public string GetClientFilterPrefix(string clientCode)
        {
            DataRow[] rows = FilterClient.Select("[Code] = " + clientCode);
            if (rows.Length > 0)
                return rows[0]["Prefix"].ToString();

            return "NN";
        }

        /// <summary>
        /// Возвращает значение поля по коду клиента
        /// </summary>
        /// <param name="idClient">Клиенты описаны в БД ЭЛЛИС Reports.Client_Detail</param>
        public string GetClientFilterFieldValue(string clientCode, string fieldName)
        {
            DataRow[] rows = FilterClient.Select("[Code] = " + clientCode);
            if (rows.Length > 0)
                return rows[0][fieldName].ToString();

            return "NN";
        }

    }
}
