﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Payments.Helpers;
using Payments.DS;

namespace Payments
{
    public partial class frmPaymentFormControl : Form
    {
        public frmPaymentFormControl()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtLastName.Text.Length == 0 || txtFirstName.Text.Length == 0
                    || txtDocumentNumber.Text.Length == 0 || txtDocumentSeria.Text.Length == 0
                    || txtDocumentOrg.Text.Length == 0 || txtSum.Value < 5)
            {
                MessageBox.Show("Необходимо заполнить обязательные поля:\nФамилия;\nИмя;\nНомер документа;\nСерия документа;\nКем выдан;\nСумма операции.");
                return;
            }

            WordDocument doc = new WordDocument(System.Windows.Forms.Application.StartupPath + "\\Docums\\p2.dot");
            doc.Visible = true;
            doc.ReplaceString("<PERSONTYPE>", "Физическое лицо", "all");
            doc.ReplaceString("<LASTNAME>", txtLastName.Text, "all");
            doc.ReplaceString("<FIRSTNAME>", txtFirstName.Text, "all");
            doc.ReplaceString("<MIDDLENAME>", txtMiddleName.Text, "all");
            doc.ReplaceString("<INN>", txtINN.Text, "all");
            doc.ReplaceString("<REGNUM>", txtRegNum.Text, "all");
            doc.ReplaceString("<REGNAME>", txtRegName.Text, "all");

            if (txtOrgName.Text.Length < 2)
                doc.ReplaceString("<REGDATE>", "", "all");
            else
                doc.ReplaceString("<REGDATE>", dpRegDate.Value.ToShortDateString(), "all");

            doc.ReplaceString("<COUNTRY>", cbPlaceCountry.Text.ToString(), "all");
            doc.ReplaceString("<CODERF>", cbCodeRFP.Text.ToString(), "all");
            doc.ReplaceString("<DISTRICT>", txtRegion.Text, "all");
            doc.ReplaceString("<CITY>", txtCity.Text, "all");
            doc.ReplaceString("<STREET>", txtStreet.Text, "all");
            doc.ReplaceString("<HOUSE_NUMBER>", txtHouse.Text, "all");
            doc.ReplaceString("<HOUSE_LITER>", txtHouseBlock.Text, "all");
            doc.ReplaceString("<FLAT>", txtFlat.Text, "all");
            doc.ReplaceString("<N_HOUSE_LITER>", (txtHouseBlock.Text.Length>0?"корп. "+txtHouseBlock.Text:""), "all");

            if (txtCityP.Text.Length < 2)
                doc.ReplaceString("<ADDR_REG_SAME>", "X", "all");
            else
                doc.ReplaceString("<ADDR_REG_SAME>", "", "all");

            doc.ReplaceString("<P_COUNTRY>", cbPlaceCountryP.Text.ToString(), "all");
            doc.ReplaceString("<P_CODERF>", cbCodeRFP.Text.ToString(), "all");
            doc.ReplaceString("<P_DISTRICT>", txtRegionP.Text, "all");
            doc.ReplaceString("<P_CITY>", txtCityP.Text, "all");
            doc.ReplaceString("<P_STREET>", txtStreetP.Text, "all");
            doc.ReplaceString("<P_HOUSE_NUMBER>", txtHouseP.Text, "all");
            doc.ReplaceString("<P_HOUSE_LITER>", txtHouseBlockP.Text, "all");
            doc.ReplaceString("<P_FLAT>", txtFlatP.Text, "all");

            doc.ReplaceString("<DOCTYPE>", cbDocumentType.Text.ToString(), "all");
            doc.ReplaceString("<DOCSERIA>", txtDocumentSeria.Text, "all");
            doc.ReplaceString("<DOCNUM>", txtDocumentNumber.Text, "all");
            doc.ReplaceString("<DOCDATE>", dpDocumentDate.Value.ToShortDateString(), "all");
            doc.ReplaceString("<DOCORG>", txtDocumentOrg.Text, "all");
            doc.ReplaceString("<DOCBD>", dpBirthDate.Value.ToShortDateString(), "all");
            doc.ReplaceString("<DOCPB>", txtBirthPlace.Text, "all");
            doc.ReplaceString("<DOCCOUNTRY>", txtDocumentCountry.Text, "all");
            doc.ReplaceString("<DOCCITY>", txtCity.Text, "all");
            doc.ReplaceString("<DOCCITIZEN>", txtCitizenship.Text, "all");

            doc.ReplaceString("<SUM>", string.Format("{0:C2}", txtSum.Value), "all");
            doc.ReplaceString("<OSN>", txtOsn.Text, "all");
            doc.ReplaceString("<PRIZN>", "", "all");
            doc.ReplaceString("<COMMENT>", "", "all");

            doc.ReplaceString("<ORGNAME>", txtOrgName.Text, "all");
            doc.ReplaceString("<ORGINN>", txtOrgInn.Text, "all");
            doc.ReplaceString("<ORGADDRESS>", txtOrgAddress.Text, "all");
            doc.ReplaceString("<ORGADDRESSFACT>", "", "all");
            doc.ReplaceString("<ORGFIO>", txtOrgFio.Text, "all");

            doc.ReplaceString("<RELATION>", cbRelation.Text.ToString(), "all");
            doc.ReplaceString("<DELEGATEFIO>", txtDelegateFIO.Text, "all");
            doc.ReplaceString("<PRESENTFIO>", txtPresentFIO.Text, "all");
            doc.ReplaceString("<RESIVERFIO>", txtResiverFIO.Text, "all");
            doc.ReplaceString("<FIO>", txtFIO.Text, "all");

            doc.ReplaceString("<EMPLOYEE>", DAccess.Instance.CurrentUser.Position + ", " + DAccess.Instance.CurrentUser.Name, "all");
            doc.ReplaceString("<EMPLOYEEDIRECTOR>", "начальник УРС, " + DAccess.Instance.CurrentUser.ManagerFio, "all");

            SavePersonIdentification();
        }

        private void SavePersonIdentification()
        {
            PaymentsDataSet.PersonIdentificationRow row;
            if (paymentsDataSet1.PersonIdentification.Rows.Count == 0)
                row = (PaymentsDataSet.PersonIdentificationRow)paymentsDataSet1.PersonIdentification.NewRow();
            else 
                row = (PaymentsDataSet.PersonIdentificationRow) paymentsDataSet1.PersonIdentification.Rows[0];

            row["IS_COMPANY"] = false;
            row["LASTNAME"] = txtLastName.Text;
            row["FIRSTNAME"] = txtFirstName.Text;
            row["MIDDLENAME"] = txtMiddleName.Text;
            row["INN"] = txtINN.Text;
            row["REGNUM"] = txtRegNum.Text;
            row["REGNAME"] = txtRegName.Text;
            row["REGDATE"] = dpRegDate.Value;

            row["COUNTRY"] = cbPlaceCountry.Text;
            row["CODERF"] = cbCodeRFP.Text;
            row["DISTRICT"] = txtRegion.Text;
            row["CITY"] = txtCity.Text;
            row["STREET"] = txtStreet.Text;
            row["HOUSE_NUMBER"] = (txtHouse.Text.Length>0 ? int.Parse(txtHouse.Text) : -1);
            row["HOUSE_LITER"] = txtHouseBlock.Text;
            row["FLAT"] = txtFlat.Text;
            row["ADDR_REG_SAME"] = true;

            row["P_COUNTRY"] = cbPlaceCountryP.Text;
            row["P_CODERF"] = cbCodeRFP.Text;
            row["P_DISTRICT"] = txtRegionP.Text;
            row["P_CITY"] = txtCityP.Text;
            row["P_STREET"] = txtStreetP.Text;
            row["P_HOUSE_NUMBER"] = (txtHouseP.Text.Length > 0 ? int.Parse(txtHouseP.Text) : -1);
            row["P_HOUSE_LITER"] = txtHouseBlockP.Text;
            row["P_FLAT"] = txtFlatP.Text;

            row["DOCTYPE"] = cbDocumentType.Text;
            row["DOCSERIA"] = txtDocumentSeria.Text;
            row["DOCNUM"] = txtDocumentNumber.Text;
            row["DOCDATE"] = dpDocumentDate.Value.ToShortDateString();
            row["DOCORG"] = txtDocumentOrg.Text;
            row["DOCBD"] = dpBirthDate.Value.ToShortDateString();
            row["DOCPB"] = txtBirthPlace.Text;
            row["DOCCOUNTRY"] = txtDocumentCountry.Text;
            row["DOCCITY"] = txtCity.Text;
            row["DOCCITIZEN"] = txtCitizenship.Text;

            row["SUM"] = txtSum.Value;
            row["OSN"] = txtOsn.Text;
            row["PRIZN"] = "";
            row["COMMENT"] = "";

            row["ORGNAME"] = txtOrgName.Text;
            row["ORGINN"] = txtOrgInn.Text;
            row["ORGADDRESS"] = txtOrgAddress.Text;
            row["ORGADDRESSFACT"] = "";
            row["ORGFIO"] = txtOrgFio.Text;

            row["RELATION"] = cbRelation.Text;
            row["DELEGATEFIO"] = txtDelegateFIO.Text;
            row["PRESENTFIO"] = txtPresentFIO.Text;
            row["RESIVERFIO"] = txtResiverFIO.Text;
            row["FIO"] = txtFIO.Text;

            row["EMPLOYEE"] = DAccess.Instance.CurrentUser.Name;
            row["EMPLOYEEDIRECTOR"] = "";

            row["CreatedBy"] = DAccess.Instance.CurrentUser.Id_User;
            row["CreationDate"] = DateTime.Now;
            
            if (paymentsDataSet1.PersonIdentification.Rows.Count == 0)
                paymentsDataSet1.PersonIdentification.Rows.Add(row);

            DAccess.DataModule.PersonIdentificationUpdate(paymentsDataSet1.PersonIdentification);
        }

        private void SetPersonIdentification(int id)
        {
            paymentsDataSet1.PersonIdentification.Clear();
            DAccess.DataModule.PersonIdentificationSelect(paymentsDataSet1.PersonIdentification, id);
            if (paymentsDataSet1.PersonIdentification.Rows.Count == 0)
                return;

            PaymentsDataSet.PersonIdentificationRow row = (PaymentsDataSet.PersonIdentificationRow) paymentsDataSet1.PersonIdentification.Rows[0];

            txtLastName.Text = isNull(row["LASTNAME"]);
            txtFirstName.Text = isNull(row["FIRSTNAME"]);
            txtMiddleName.Text = isNull(row["MIDDLENAME"]);
            txtINN.Text = isNull(row["INN"]);
            
            txtRegNum.Text = isNull(row["REGNUM"]);
            txtRegName.Text = isNull(row["REGNAME"]);
            dpRegDate.Value = DateTime.Parse(row["REGDATE"].ToString());

            cbPlaceCountry.SelectedValue = isNull(row["COUNTRY"]);
            cbCodeRFP.SelectedValue = isNull(row["CODERF"]);
            txtRegion.Text = isNull(row["DISTRICT"]);
            txtCity.Text = isNull(row["CITY"]);
            txtStreet.Text = isNull(row["STREET"]);

            if (row["HOUSE_NUMBER"] != DBNull.Value && row["HOUSE_NUMBER"].ToString().Length > 1)
                txtHouse.Text = row["HOUSE_NUMBER"].ToString();

            txtHouseBlock.Text = isNull(row["HOUSE_LITER"]);
            txtFlat.Text = isNull(row["FLAT"]);

            cbPlaceCountryP.SelectedValue = isNull(row["P_COUNTRY"]);
            cbCodeRFP.SelectedValue = isNull(row["P_CODERF"]);
            txtRegionP.Text = isNull(row["P_DISTRICT"]);
            txtCityP.Text = isNull(row["P_CITY"]);
            txtStreetP.Text = isNull(row["P_STREET"]);

            txtHouseP.Text = "";
            if (row["P_HOUSE_NUMBER"] != DBNull.Value && row["P_HOUSE_NUMBER"].ToString() != "-1")
                txtHouseP.Text = row["P_HOUSE_NUMBER"].ToString();

            txtHouseBlockP.Text = isNull(row["P_HOUSE_LITER"]);
            txtFlatP.Text = isNull(row["P_FLAT"]);

            cbDocumentType.SelectedValue = isNull(row["DOCTYPE"]);
            txtDocumentSeria.Text = isNull(row["DOCSERIA"]);
            txtDocumentNumber.Text = isNull(row["DOCNUM"]);
            dpDocumentDate.Value = DateTime.Parse(row["DOCDATE"].ToString());
            txtDocumentOrg.Text = isNull(row["DOCORG"]);
            dpBirthDate.Value = DateTime.Parse(row["DOCBD"].ToString());
            txtBirthPlace.Text = isNull(row["DOCPB"]);
            txtDocumentCountry.Text = isNull(row["DOCCOUNTRY"]);
            txtCity.Text = isNull(row["DOCCITY"]);
            txtCitizenship.Text = isNull(row["DOCCITIZEN"]);

            txtSum.Value = decimal.Parse(row["SUM"].ToString());
            txtOsn.Text = isNull(row["OSN"]);

            txtOrgName.Text = isNull(row["ORGNAME"]);
            txtOrgInn.Text = isNull(row["ORGINN"]);
            txtOrgAddress.Text = isNull(row["ORGADDRESS"]);
            txtOrgFio.Text = isNull(row["ORGFIO"]);

            cbRelation.SelectedValue = isNull(row["RELATION"]);
            txtDelegateFIO.Text = isNull(row["DELEGATEFIO"]);
            txtPresentFIO.Text = isNull(row["PRESENTFIO"]);
            txtResiverFIO.Text = isNull(row["RESIVERFIO"]);
            txtFIO.Text = row["FIO"].ToString();
        }

        private string isNull(object val)
        {
            if (val == DBNull.Value)
                return string.Empty;
            else
                return val.ToString();
        }

        private void txtDocumentOrg_Leave(object sender, EventArgs e)
        {
            cbPlaceCountry.Focus();
        }

        private void frmPaymentFormControl_Shown(object sender, EventArgs e)
        {
            cbRelation.SelectedIndex = 0;
            cbCountry.SelectedIndex = 0;
            cbDocumentType.SelectedIndex = 0;
            cbPlaceCountry.SelectedIndex = 0;
            cbPlaceCountryP.SelectedIndex = 0;
            cbCodeRF.SelectedIndex = 32;
            cbCodeRFP.SelectedIndex = 32;
        }

        private void frmPaymentFormControl_Load(object sender, EventArgs e)
        {
            paymentsDataSet1.PersonIdentification.Clear();
           // DAccess.DataModule.PersonIdentificationSelect(paymentsDataSet1.PersonIdentification, -1);
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            frmPaymentFormControlSelect f = new frmPaymentFormControlSelect();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK && f.PersonIdentificationSelectedID > 0)
                SetPersonIdentification(f.PersonIdentificationSelectedID);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            paymentsDataSet1.PersonIdentification.Clear();
            txtLastName.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtMiddleName.Text = string.Empty;
            txtINN.Text = string.Empty;

            txtRegNum.Text = string.Empty;
            txtRegName.Text = string.Empty;
            dpRegDate.Value = DateTime.Now;

            cbPlaceCountry.SelectedValue = 0;
            cbCodeRFP.SelectedValue = 32;
            txtRegion.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtStreet.Text = "ул.";

            txtHouse.Text = string.Empty;

            txtHouseBlock.Text = string.Empty;
            txtFlat.Text = string.Empty;

            cbPlaceCountryP.SelectedValue = 0;
            cbCodeRFP.SelectedValue = 32;
            txtRegionP.Text = string.Empty;
            txtCityP.Text = string.Empty;
            txtStreetP.Text = string.Empty;
            txtHouseP.Text = string.Empty;
            txtHouseBlockP.Text = string.Empty;
            txtFlatP.Text = string.Empty;

            cbDocumentType.SelectedValue = 0;
            txtDocumentSeria.Text = string.Empty;
            txtDocumentNumber.Text = string.Empty;
            dpDocumentDate.Value = DateTime.Now;
            txtDocumentOrg.Text = string.Empty;
            dpBirthDate.Value = DateTime.Now;
            txtBirthPlace.Text = string.Empty;
            txtDocumentCountry.Text = "Россия";
            txtCity.Text = string.Empty;
            txtCitizenship.Text = string.Empty;

            txtSum.Value = 0;
            txtOsn.Text = "Оплата задолженности по квартплате и коммунальным услугам";

            txtOrgName.Text = string.Empty;
            txtOrgInn.Text = string.Empty;
            txtOrgAddress.Text = string.Empty;
            txtOrgFio.Text = string.Empty;

            cbRelation.SelectedValue = 0;
            txtDelegateFIO.Text = string.Empty;
            txtPresentFIO.Text = string.Empty;
            txtResiverFIO.Text = string.Empty;
            txtFIO.Text = string.Empty;
        }

        private void txtFirstName_Leave(object sender, EventArgs e)
        {
         //   txtDelegateFIO.Text = txtLastName.Text + " " + txtFirstName.Text + " " + txtMiddleName.Text;
        }

        private void txtMiddleName_Leave(object sender, EventArgs e)
        {
         //   txtDelegateFIO.Text = txtLastName.Text + " " + txtFirstName.Text + " " + txtMiddleName.Text;
        }

        private void txtLastName_Leave(object sender, EventArgs e)
        {
         //   txtDelegateFIO.Text = txtLastName.Text + " " + txtFirstName.Text + " " + txtMiddleName.Text;
        }
    }
}
