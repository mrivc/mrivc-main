﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using FastReport.Export.Csv;
using System.IO;
using FastReport.Export.Text;
using FastReport.Export.Dbf;
using FastReport.Export.Pdf;
using FastReport.Export.Html;
using FastReport.Export.RichText;
using FastReport.Export.Mht;
using FastReport.Export.Xml;
using FastReport.Export.Odf;
using FastReport.Export.OoXML;
using System.Web;

namespace FastReport.Web
{
#if! WinForms
    public
#endif
    partial class WebReport : WebControl, INamingContainer
    {
        #region RTF format

        /// <summary>
        /// Switch visibility of RTF export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowRtfExport
        {
            get { return Prop.ShowRtfExport; }
            set { Prop.ShowRtfExport = value; }
        }

        /// <summary>
        /// Gets or sets the quality of Jpeg images in RTF file.
        /// </summary>
        /// <remarks>
        /// Default value is 90. This property will be used if you select Jpeg 
        /// in the <see cref="RtfImageFormat"/> property.
        /// </remarks>
        [DefaultValue(90)]
        [Category("Rtf Format")]
        [Browsable(true)]
        public int RtfJpegQuality
        {
            get { return Prop.RtfJpegQuality; }
            set { Prop.RtfJpegQuality = value; }
        }

        /// <summary>
        /// Gets or sets the image format that will be used to save pictures in RTF file.
        /// </summary>
        /// <remarks>
        /// Default value is <b>Metafile</b>. This format is better for exporting such objects as
        /// <b>MSChartObject</b> and <b>ShapeObject</b>.
        /// </remarks>
        [DefaultValue(RTFImageFormat.Metafile)]
        [Category("Rtf Format")]
        [Browsable(true)]
        public RTFImageFormat RtfImageFormat
        {
            get { return Prop.RtfImageFormat; }
            set { Prop.RtfImageFormat = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating that pictures are enabled.
        /// </summary>
        [DefaultValue(true)]
        [Category("Rtf Format")]
        [Browsable(true)]
        public bool RtfPictures
        {
            get { return Prop.RtfPictures; }
            set { Prop.RtfPictures = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating that page breaks are enabled.
        /// </summary>
        [DefaultValue(true)]
        [Category("Rtf Format")]
        [Browsable(true)]
        public bool RtfPageBreaks
        {
            get { return Prop.RtfPageBreaks; }
            set { Prop.RtfPageBreaks = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the wysiwyg mode should be used 
        /// for better results.
        /// </summary>
        /// <remarks>
        /// Default value is <b>true</b>. In wysiwyg mode, the resulting rtf file will look
        /// as close as possible to the prepared report. On the other side, it may have a lot 
        /// of small rows/columns, which will make it less editable. If you set this property
        /// to <b>false</b>, the number of rows/columns in the resulting file will be decreased.
        /// You will get less wysiwyg, but more editable file.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Rtf Format")]
        [Browsable(true)]
        public bool RtfWysiwyg
        {
            get { return Prop.RtfWysiwyg; }
            set { Prop.RtfWysiwyg = value; }
        }

        /// <summary>
        /// Gets or sets the creator of the document.
        /// </summary>
        [DefaultValue("FastReport")]
        [Category("Rtf Format")]
        [Browsable(true)]
        public string RtfCreator
        {
            get { return Prop.RtfCreator; }
            set { Prop.RtfCreator = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the rows in the resulting table 
        /// should calculate its height automatically.
        /// </summary>
        /// <remarks>
        /// Default value for this property is <b>false</b>. In this mode, each row in the
        /// resulting table has fixed height to get maximum wysiwyg. If you set it to <b>true</b>,
        /// the height of resulting table will be calculated automatically by the Word processor.
        /// The document will be more editable, but less wysiwyg.
        /// </remarks>
        [DefaultValue(false)]
        [Category("Rtf Format")]
        [Browsable(true)]
        public bool RtfAutoSize
        {
            get { return Prop.RtfAutoSize; }
            set { Prop.RtfAutoSize = value; }
        }

        #endregion RTF format
        #region MHT format
        /// <summary>
        /// Switch visibility of MHT (web-archive) export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowMhtExport
        {
            get { return Prop.ShowMhtExport; }
            set { Prop.ShowMhtExport = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating that pictures are enabled.
        /// </summary>
        [DefaultValue(true)]
        [Category("Mht Format")]
        [Browsable(true)]
        public bool MhtPictures
        {
            get { return Prop.MhtPictures; }
            set { Prop.MhtPictures = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the wysiwyg mode should be used 
        /// for better results.
        /// </summary>
        /// <remarks>
        /// Default value is <b>true</b>. In wysiwyg mode, the resulting rtf file will look
        /// as close as possible to the prepared report. On the other side, it may have a lot 
        /// of small rows/columns, which will make it less editable. If you set this property
        /// to <b>false</b>, the number of rows/columns in the resulting file will be decreased.
        /// You will get less wysiwyg, but more editable file.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Mht Format")]
        [Browsable(true)]
        public bool MhtWysiwyg
        {
            get { return Prop.MhtWysiwyg; }
            set { Prop.MhtWysiwyg = value; }
        }
        #endregion MHT format
        #region ODS format

        /// <summary>
        /// Switch visibility of Open Office Spreadsheet (ODS) export in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowOdsExport
        {
            get { return Prop.ShowOdsExport; }
            set { Prop.ShowOdsExport = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating that page breaks are enabled.
        /// </summary>
        [DefaultValue(true)]
        [Category("Ods Format")]
        [Browsable(true)]
        public bool OdsPageBreaks
        {
            get { return Prop.OdsPageBreaks; }
            set { Prop.OdsPageBreaks = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the wysiwyg mode should be used 
        /// for better results.
        /// </summary>
        /// <remarks>
        /// Default value is <b>true</b>. In wysiwyg mode, the resulting rtf file will look
        /// as close as possible to the prepared report. On the other side, it may have a lot 
        /// of small rows/columns, which will make it less editable. If you set this property
        /// to <b>false</b>, the number of rows/columns in the resulting file will be decreased.
        /// You will get less wysiwyg, but more editable file.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Ods Format")]
        [Browsable(true)]
        public bool OdsWysiwyg
        {
            get { return Prop.OdsWysiwyg; }
            set { Prop.OdsWysiwyg = value; }
        }

        /// <summary>
        /// Gets or sets the creator of the document.
        /// </summary>
        [DefaultValue("FastReport")]
        [Category("Ods Format")]
        [Browsable(true)]
        public string OdsCreator
        {
            get { return Prop.OdsCreator; }
            set { Prop.OdsCreator = value; }
        }
        #endregion ODS format
        #region ODT format

        /// <summary>
        /// Switch visibility of Open Office Text (ODT) export in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowOdtExport
        {
            get { return Prop.ShowOdtExport; }
            set { Prop.ShowOdtExport = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating that page breaks are enabled.
        /// </summary>
        [DefaultValue(true)]
        [Category("Odt Format")]
        [Browsable(true)]
        public bool OdtPageBreaks
        {
            get { return Prop.OdtPageBreaks; }
            set { Prop.OdtPageBreaks = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the wysiwyg mode should be used 
        /// for better results.
        /// </summary>
        /// <remarks>
        /// Default value is <b>true</b>. In wysiwyg mode, the resulting rtf file will look
        /// as close as possible to the prepared report. On the other side, it may have a lot 
        /// of small rows/columns, which will make it less editable. If you set this property
        /// to <b>false</b>, the number of rows/columns in the resulting file will be decreased.
        /// You will get less wysiwyg, but more editable file.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Odt Format")]
        [Browsable(true)]
        public bool OdtWysiwyg
        {
            get { return Prop.OdtWysiwyg; }
            set { Prop.OdtWysiwyg = value; }
        }

        /// <summary>
        /// Gets or sets the creator of the document.
        /// </summary>
        [DefaultValue("FastReport")]
        [Category("Odt Format")]
        [Browsable(true)]
        public string OdtCreator
        {
            get { return Prop.OdtCreator; }
            set { Prop.OdtCreator = value; }
        }
        #endregion ODT format
        #region XPS format
        /// <summary>
        /// Switch visibility of XPS export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowXpsExport
        {
            get { return Prop.ShowXpsExport; }
            set { Prop.ShowXpsExport = value; }
        }
        #endregion XPS format
        #region DBF format
        /// <summary>
        /// Switch visibility of DBF export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowDbfExport
        {
            get { return Prop.ShowDbfExport; }
            set { Prop.ShowDbfExport = value; }
        }
        #endregion Dbf format
        #region Word2007 format
        /// <summary>
        /// Switch visibility of Word 2007 export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowWord2007Export
        {
            get { return Prop.ShowWord2007Export; }
            set { Prop.ShowWord2007Export = value; }
        }

        /// <summary>
        /// Enable or disable matrix view of Word 2007 document
        /// </summary>
        [DefaultValue(true)]
        [Category("Word 2007 Format")]
        [Browsable(true)]
        public bool DocxMatrixBased
        {
            get { return Prop.DocxMatrixBased; }
            set { Prop.DocxMatrixBased = value; }
        }
        #endregion
        #region Excel2007 format

        /// <summary>
        /// Switch visibility of Excel 2007 export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowExcel2007Export
        {
            get { return Prop.ShowExcel2007Export; }
            set { Prop.ShowExcel2007Export = value; }
        }

        /// <summary>
        ///  Gets or sets a value indicating that page breaks are enabled.
        /// </summary>
        [DefaultValue(false)]
        [Category("Excel 2007 Format")]
        [Browsable(true)]
        public bool XlsxPageBreaks
        {
            get { return Prop.XlsxPageBreaks; }
            set { Prop.XlsxPageBreaks = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the wysiwyg mode should be used 
        /// for better results.
        /// </summary>
        /// <remarks>
        /// Default value is <b>true</b>. In wysiwyg mode, the resulting rtf file will look
        /// as close as possible to the prepared report. On the other side, it may have a lot 
        /// of small rows/columns, which will make it less editable. If you set this property
        /// to <b>false</b>, the number of rows/columns in the resulting file will be decreased.
        /// You will get less wysiwyg, but more editable file.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Excel 2007 Format")]
        [Browsable(true)]
        public bool XlsxWysiwyg
        {
            get { return Prop.XlsxWysiwyg; }
            set { Prop.XlsxWysiwyg = value; }
        }

        /// <summary>
        /// Enable or disable of exporting data without any header/group bands.
        /// </summary>
        [DefaultValue(true)]
        [Category("Excel 2007 Format")]
        [Browsable(true)]
        public bool XlsxDataOnly
        {
          get { return Prop.XlsxDataOnly; }
          set { Prop.XlsxDataOnly = value; }
        }
        #endregion Excel2007 format
        #region PowerPoint2007 format
        /// <summary>
        /// Switch visibility of PowerPoint 2007 export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowPowerPoint2007Export
        {
            get { return Prop.ShowPowerPoint2007Export; }
            set { Prop.ShowPowerPoint2007Export = value; }
        }

        /// <summary>
        /// Gets or sets the image format that will be used to save pictures in PowerPoint file.
        /// </summary>
        [DefaultValue(PptImageFormat.Png)]
        [Category("PowerPoint 2007 Format")]
        [Browsable(true)]
        public PptImageFormat PptxImageFormat
        {
            get { return Prop.PptxImageFormat; }
            set { Prop.PptxImageFormat = value; }
        }
        #endregion PowerPoint2007 format
        #region XML format
        /// <summary>
        ///  Switch visibility of XML (Excel) export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowXmlExcelExport
        {
            get { return Prop.ShowXmlExcelExport; }
            set { Prop.ShowXmlExcelExport = value; }
        }

        /// <summary>
        ///  Gets or sets a value indicating that page breaks are enabled.
        /// </summary>
        [DefaultValue(true)]
        [Category("Xml Excel Format")]
        [Browsable(true)]
        public bool XmlExcelPageBreaks
        {
            get { return Prop.XmlExcelPageBreaks; }
            set { Prop.XmlExcelPageBreaks = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the wysiwyg mode should be used 
        /// for better results.
        /// </summary>
        /// <remarks>
        /// Default value is <b>true</b>. In wysiwyg mode, the resulting rtf file will look
        /// as close as possible to the prepared report. On the other side, it may have a lot 
        /// of small rows/columns, which will make it less editable. If you set this property
        /// to <b>false</b>, the number of rows/columns in the resulting file will be decreased.
        /// You will get less wysiwyg, but more editable file.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Xml Excel Format")]
        [Browsable(true)]
        public bool XmlExcelWysiwyg
        {
            get { return Prop.XmlExcelWysiwyg; }
            set { Prop.XmlExcelWysiwyg = value; }
        }

        /// <summary>
        /// Enable or disable of exporting data without any header/group bands.
        /// </summary>
        [DefaultValue(false)]
        [Category("Xml Excel Format")]
        [Browsable(true)]
        public bool XmlExcelDataOnly
        {
            get { return Prop.XmlExcelDataOnly; }
            set { Prop.XmlExcelDataOnly = value; }
        }
        #endregion XML format
        #region PDF format
        /// <summary>
        ///  Switch visibility of PDF (Adobe Acrobat) export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowPdfExport
        {
            get { return Prop.ShowPdfExport; }
            set { Prop.ShowPdfExport = value; }
        }

        /// <summary>
        /// Enable or disable of embedding the TrueType fonts.
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfEmbeddingFonts
        {
            get { return Prop.PdfEmbeddingFonts; }
            set { Prop.PdfEmbeddingFonts = value; }
        }

        /// <summary>
        /// Enable or disable of exporting the background.
        /// </summary>
        [DefaultValue(false)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfBackground
        {
            get { return Prop.PdfBackground; }
            set { Prop.PdfBackground = value; }
        }

        /// <summary>
        /// Enable or disable of optimization the images for printing. 
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfPrintOptimized
        {
            get { return Prop.PdfPrintOptimized; }
            set { Prop.PdfPrintOptimized = value; }
        }

        /// <summary>
        /// Enable or disable of document's Outline.
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfOutline
        {
            get { return Prop.PdfOutline; }
            set { Prop.PdfOutline = value; }
        }

        /// <summary>
        /// Enable or disable of displaying document's title.
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfDisplayDocTitle
        {
            get { return Prop.PdfDisplayDocTitle; }
            set { Prop.PdfDisplayDocTitle = value; }
        }

        /// <summary>
        /// Enable or disable hide the toolbar. 
        /// </summary>
        [DefaultValue(false)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfHideToolbar
        {
            get { return Prop.PdfHideToolbar; }
            set { Prop.PdfHideToolbar = value; }
        }

        /// <summary>
        /// Enable or disable hide the menu's bar.
        /// </summary>
        [DefaultValue(false)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfHideMenubar
        {
            get { return Prop.PdfHideMenubar; }
            set { Prop.PdfHideMenubar = value; }
        }

        /// <summary>
        /// Enable or disable hide the Windows UI. 
        /// </summary>
        [DefaultValue(false)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfHideWindowUI
        {
            get { return Prop.PdfHideWindowUI; }
            set { Prop.PdfHideWindowUI = value; }
        }

        /// <summary>
        /// Enable or disable of fitting the window. 
        /// </summary>
        [DefaultValue(false)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfFitWindow
        {
            get { return Prop.PdfFitWindow; }
            set { Prop.PdfFitWindow = value; }
        }

        /// <summary>
        ///  Enable or disable centering the window.
        /// </summary>
        [DefaultValue(false)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfCenterWindow
        {
            get { return Prop.PdfCenterWindow; }
            set { Prop.PdfCenterWindow = value; }
        }

        /// <summary>
        /// Enable or disable of scaling the page for shrink to printable area. 
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfPrintScaling
        {
            get { return Prop.PdfPrintScaling; }
            set { Prop.PdfPrintScaling = value; }
        }

        /// <summary>
        /// Title of the document. 
        /// </summary>
        [DefaultValue("")]
        [Category("Pdf Format")]
        [Browsable(true)]
        public string PdfTitle
        {
            get { return Prop.PdfTitle; }
            set { Prop.PdfTitle = value; }
        }

        /// <summary>
        /// Author of the document. 
        /// </summary>
        [DefaultValue("")]
        [Category("Pdf Format")]
        [Browsable(true)]
        public string PdfAuthor
        {
            get { return Prop.PdfAuthor; }
            set { Prop.PdfAuthor = value; }
        }

        /// <summary>
        ///  Subject of the document.
        /// </summary>
        [DefaultValue("")]
        [Category("Pdf Format")]
        [Browsable(true)]
        public string PdfSubject
        {
            get { return Prop.PdfSubject; }
            set { Prop.PdfSubject = value; }
        }

        /// <summary>
        ///  Keywords of the document.
        /// </summary>
        [DefaultValue("")]
        [Category("Pdf Format")]
        [Browsable(true)]
        public string PdfKeywords
        {
            get { return Prop.PdfKeywords; }
            set { Prop.PdfKeywords = value; }
        }

        /// <summary>
        /// Creator of the document.
        /// </summary>
        [DefaultValue("FastReport")]
        [Category("Pdf Format")]
        [Browsable(true)]
        public string PdfCreator
        {
            get { return Prop.PdfCreator; }
            set { Prop.PdfCreator = value; }
        }

        /// <summary>
        /// Producer of the document.
        /// </summary>
        [DefaultValue("FastReport.NET")]
        [Category("Pdf Format")]
        [Browsable(true)]
        public string PdfProducer
        {
            get { return Prop.PdfProducer; }
            set { Prop.PdfProducer = value; }
        }

        /// <summary>
        /// Sets the users password.
        /// </summary>
        [DefaultValue("")]
        [Category("Pdf Format")]
        [Browsable(true)]
        public string PdfUserPassword
        {
            get { return Prop.PdfUserPassword; }
            set { Prop.PdfUserPassword = value; }
        }

        /// <summary>
        /// Sets the owner password. 
        /// </summary>
        [DefaultValue("")]
        [Category("Pdf Format")]
        [Browsable(true)]
        public string PdfOwnerPassword
        {
            get { return Prop.PdfOwnerPassword; }
            set { Prop.PdfOwnerPassword = value; }
        }

        /// <summary>
        /// Enable or disable printing in protected document. 
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfAllowPrint
        {
            get { return Prop.PdfAllowPrint; }
            set { Prop.PdfAllowPrint = value; }
        }

        /// <summary>
        /// Enable or disable modifying in protected document. 
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfAllowModify
        {
            get { return Prop.PdfAllowModify; }
            set { Prop.PdfAllowModify = value; }
        }

        /// <summary>
        /// Enable or disable copying in protected document. 
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfAllowCopy
        {
            get { return Prop.PdfAllowCopy; }
            set { Prop.PdfAllowCopy = value; }
        }

        /// <summary>
        /// Enable or disable annotating in protected document. 
        /// </summary>
        [DefaultValue(true)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfAllowAnnotate
        {
            get { return Prop.PdfAllowAnnotate; }
            set { Prop.PdfAllowAnnotate = value; }
        }

        /// <summary>
        /// Enable or disable write in PDF/A document. 
        /// </summary>
        [DefaultValue(false)]
        [Category("Pdf Format")]
        [Browsable(true)]
        public bool PdfA
        {
            get { return Prop.PdfA; }
            set { Prop.PdfA = value; }
        }
        #endregion PDF format
        #region CSV format
        /// <summary>
        /// Switch visibility of CSV (comma separated values) export in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowCsvExport
        {
            get { return Prop.ShowCsvExport; }
            set { Prop.ShowCsvExport = value; }
        }

        /// <summary>
        /// Gets or sets of cells separator. 
        /// </summary>
        [DefaultValue(";")]
        [Category("Csv Format")]
        [Browsable(true)]
        public string CsvSeparator
        {
            get { return Prop.CsvSeparator; }
            set { Prop.CsvSeparator = value; }
        }

        /// <summary>
        /// Enable or disable of exporting data without any header/group bands.
        /// </summary>
        [DefaultValue(false)]
        [Category("Csv Format")]
        [Browsable(true)]
        public bool CsvDataOnly
        {
            get { return Prop.CsvDataOnly; }
            set { Prop.CsvDataOnly = value; }
        }
        #endregion CSV format
        #region Prepared report
        /// <summary>
        ///  Switch visibility of prepared report export in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowPreparedReport
        {
            get { return Prop.ShowPreparedReport; }
            set { Prop.ShowPreparedReport = value; }
        }
        #endregion Prepared report
        #region Text format
        /// <summary>
        ///  Switch visibility of text (plain text) export in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowTextExport
        {
            get { return Prop.ShowTextExport; }
            set { Prop.ShowTextExport = value; }
        }

        /// <summary>
        /// Enable or disable of exporting data without any header/group bands.
        /// </summary>
        [DefaultValue(false)]
        [Category("Text Format")]
        [Browsable(true)]
        public bool TextDataOnly
        {
            get { return Prop.TextDataOnly; }
            set { Prop.TextDataOnly = value; }
        }

        /// <summary>
        ///  Gets or sets a value indicating that page breaks are enabled.
        /// </summary>
        [DefaultValue(true)]
        [Category("Text Format")]
        [Browsable(true)]
        public bool TextPageBreaks
        {
            get { return Prop.TextPageBreaks; }
            set { Prop.TextPageBreaks = value; }
        }

        /// <summary>
        /// Enable or disable frames in text file.
        /// </summary>
        [DefaultValue(true)]
        [Category("Text Format")]
        [Browsable(true)]
        public bool TextAllowFrames
        {
            get { return Prop.TextAllowFrames; }
            set { Prop.TextAllowFrames = value; }
        }

        /// <summary>
        /// Enable or disable simple (non graphic) frames in text file.
        /// </summary>
        [DefaultValue(true)]
        [Category("Text Format")]
        [Browsable(true)]
        public bool TextSimpleFrames
        {
            get { return Prop.TextSimpleFrames; }
            set { Prop.TextSimpleFrames = value; }
        }

        /// <summary>
        /// Enable or disable empty lines in text file.
        /// </summary>
        [DefaultValue(false)]
        [Category("Text Format")]
        [Browsable(true)]
        public bool TextEmptyLines
        {
            get { return Prop.TextEmptyLines; }
            set { Prop.TextEmptyLines = value; }
        }
        #endregion Text export


        #region All exports

        private string GetExportFileName(string format)
        {
            string s = String.Concat(
                Path.GetFileNameWithoutExtension(
                Report.FileName.Length == 0 ? WebUtils.ReportPrefix : Report.FileName),
                ".",
                format);
            return s; 
        }

        private void ResponseExport(string guid, WebExportItem ExportItem, bool displayInline, HttpContext context)
        {
            ExportItem.FileName = GetExportFileName(ExportItem.Format);

            PutStorage(WebUtils.EXPORT, guid, ExportItem);

            string url = string.Format("~/{0}?{1}={2}&displayinline={3}", WebUtils.HandlerFileName, WebUtils.ConstID, guid, displayInline);
            context.Response.Redirect(url, false);
        }

        private void ResponseExport(string guid, WebExportItem ExportItem, HttpContext context)
        {
            ResponseExport(guid, ExportItem, false, context);
        }

        /// <summary>
        /// Export in CSV format
        /// </summary>
        public void ExportCsv(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                CSVExport csvExport = new CSVExport();
                csvExport.OpenAfterExport = false;
                // set csv export properties
                csvExport.Separator = CsvSeparator;
                csvExport.DataOnly = CsvDataOnly;
                MemoryStream ms = new MemoryStream();
                csvExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "csv";
                ExportItem.ContentType = "text/x-сsv";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in Text format
        /// </summary>
        public void ExportText(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                TextExport textExport = new TextExport();
                textExport.OpenAfterExport = false;
                // set text export properties
                textExport.AvoidDataLoss = true;
                textExport.DataOnly = TextDataOnly;
                textExport.PageBreaks = TextPageBreaks;
                textExport.Frames = TextAllowFrames;
                textExport.TextFrames = TextSimpleFrames;
                textExport.EmptyLines = TextEmptyLines;
                MemoryStream ms = new MemoryStream();
                textExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "txt";
                ExportItem.ContentType = "text/plain";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in DBF format
        /// </summary>
        public void ExportDbf(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                DBFExport dbfExport = new DBFExport();
                dbfExport.OpenAfterExport = false;
                // set text export properties
                dbfExport.DataOnly = true;
                MemoryStream ms = new MemoryStream();
                dbfExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "dbf";
                ExportItem.ContentType = "application/dbf";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in PDF format
        /// </summary>
        public void ExportPdf(HttpContext context)
        {
            ExportPdf(context, false);
        }

        /// <summary>
        /// Export in PDF format inline
        /// </summary>
        public void ExportPdf(HttpContext context, bool displayInline)
        {
            if (State == ReportState.Done)
            {
                WebExportItem ExportItem = new WebExportItem();
                PDFExport pdfExport = new PDFExport();
                pdfExport.OpenAfterExport = false;
                // set pdf export properties
                pdfExport.EmbeddingFonts = PdfEmbeddingFonts;
                pdfExport.Background = PdfBackground;
                pdfExport.PrintOptimized = PdfPrintOptimized;
                pdfExport.Title = PdfTitle;
                pdfExport.Author = PdfAuthor;
                pdfExport.Subject = PdfSubject;
                pdfExport.Keywords = PdfKeywords;
                pdfExport.Creator = PdfCreator;
                pdfExport.Producer = PdfProducer;
                pdfExport.Outline = PdfOutline;
                pdfExport.DisplayDocTitle = PdfDisplayDocTitle;
                pdfExport.HideToolbar = PdfHideToolbar;
                pdfExport.HideMenubar = PdfHideMenubar;
                pdfExport.HideWindowUI = PdfHideWindowUI;
                pdfExport.FitWindow = PdfFitWindow;
                pdfExport.CenterWindow = PdfCenterWindow;
                pdfExport.PrintScaling = PdfPrintScaling;
                pdfExport.UserPassword = PdfUserPassword;
                pdfExport.OwnerPassword = PdfOwnerPassword;
                pdfExport.AllowPrint = PdfAllowPrint;
                pdfExport.AllowCopy = PdfAllowCopy;
                pdfExport.AllowModify = PdfAllowModify;
                pdfExport.AllowAnnotate = PdfAllowAnnotate;
                pdfExport.PdfA = PdfA;
                MemoryStream ms = new MemoryStream();
                pdfExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "pdf";
                ExportItem.ContentType = "application/pdf";
                ResponseExport(ReportGuid, ExportItem, displayInline, context);
                context.Response.End();
            }
        }

        /// <summary>
        /// Export in HTML format inline
        /// </summary>
        public void ExportHtml(HttpContext context, bool displayInline, bool print)
        {
            if (State == ReportState.Done)
            {
                WebExportItem ExportItem = new WebExportItem();
                HTMLExport htmlExport = new HTMLExport();
                htmlExport.OpenAfterExport = false;
                // set html export properties
                htmlExport.Navigator = false;
                htmlExport.Layers = Layers;
                htmlExport.SinglePage = true;
                htmlExport.Pictures = Pictures;
                htmlExport.Print = print;
                htmlExport.Preview = true;
                htmlExport.SubFolder = false;
                htmlExport.WebImagePrefix = String.Concat(WebUtils.GetAppRoot(context, WebUtils.HandlerFileName), "?", WebUtils.PicsPrefix);
                MemoryStream ms = new MemoryStream();
                htmlExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();

                if (htmlExport.PrintPageData != null)
                {
                    // add all pictures in cache
                    for (int i = 0; i < htmlExport.PrintPageData.Pictures.Count; i++)
                    {
                        Stream stream = htmlExport.PrintPageData.Pictures[i];
                        byte[] image = new byte[stream.Length];
                        stream.Position = 0;
                        int n = stream.Read(image, 0, (int)stream.Length);
                        string picGuid = htmlExport.PrintPageData.Guids[i];
                        PutStorage(String.Empty, picGuid, image);
                    }

                    // cleanup 
                    for(int i = 0; i < htmlExport.PrintPageData.Pictures.Count; i++)
                    {
                        Stream stream = htmlExport.PrintPageData.Pictures[i];
                        stream.Dispose();
                        stream = null;
                    }
                    htmlExport.PrintPageData.Pictures.Clear();
                    htmlExport.PrintPageData.Guids.Clear();
                }

                ExportItem.Format = "html";
                ExportItem.ContentType = "text/html";

                ResponseExport(ReportGuid, ExportItem, displayInline, context);
            }
        }

        /// <summary>
        /// Export in RTF format
        /// </summary>
        public void ExportRtf(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                RTFExport rtfExport = new RTFExport();
                rtfExport.OpenAfterExport = false;
                // set Rtf export properties
                rtfExport.JpegQuality = RtfJpegQuality;
                rtfExport.ImageFormat = RtfImageFormat;
                rtfExport.Pictures = RtfPictures;
                rtfExport.PageBreaks = RtfPageBreaks;
                rtfExport.Wysiwyg = RtfWysiwyg;
                rtfExport.Creator = RtfCreator;
                rtfExport.AutoSize = RtfAutoSize;
                MemoryStream ms = new MemoryStream();
                rtfExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "rtf";
                ExportItem.ContentType = "application/rtf";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in MHT format
        /// </summary>
        public void ExportMht(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                MHTExport mhtExport = new MHTExport();
                mhtExport.OpenAfterExport = false;
                // set MHT export properties
                mhtExport.Pictures = MhtPictures;
                mhtExport.Wysiwyg = MhtWysiwyg;
                MemoryStream ms = new MemoryStream();
                mhtExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "mht";
                ExportItem.ContentType = "message/rfc822";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in XML (Excel 2003) format
        /// </summary>
        public void ExportXmlExcel(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                XMLExport xmlExport = new XMLExport();
                xmlExport.OpenAfterExport = false;
                // set xml export properties
                xmlExport.PageBreaks = XmlExcelPageBreaks;
                xmlExport.Wysiwyg = XmlExcelWysiwyg;
                xmlExport.DataOnly = XmlExcelDataOnly;
                MemoryStream ms = new MemoryStream();
                xmlExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "xls";
                ExportItem.ContentType = "application/vnd.ms-excel";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in Open Office Spreadsheet format
        /// </summary>
        public void ExportOds(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                ODSExport odsExport = new ODSExport();
                odsExport.OpenAfterExport = false;
                // set ODS export properties
                odsExport.Creator = OdsCreator;
                odsExport.Wysiwyg = OdsWysiwyg;
                odsExport.PageBreaks = OdsPageBreaks;
                MemoryStream ms = new MemoryStream();
                odsExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "ods";
                ExportItem.ContentType = "application/x-oleobject";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in Open Office Text format
        /// </summary>
        public void ExportOdt(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                ODTExport odtExport = new ODTExport();
                odtExport.OpenAfterExport = false;
                // set ODT export properties
                odtExport.Creator = OdtCreator;
                odtExport.Wysiwyg = OdtWysiwyg;
                odtExport.PageBreaks = OdtPageBreaks;
                MemoryStream ms = new MemoryStream();
                odtExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "odt";
                ExportItem.ContentType = "application/x-oleobject";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in XPS format
        /// </summary>
        public void ExportXps(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                XPSExport xpsExport = new XPSExport();
                xpsExport.OpenAfterExport = false;
                MemoryStream ms = new MemoryStream();
                xpsExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "xps";
                ExportItem.ContentType = "application/vnd.ms xpsdocument";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in Excel 2007 format
        /// </summary>
        public void ExportExcel2007(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                Excel2007Export xlsxExport = new Excel2007Export();
                xlsxExport.OpenAfterExport = false;
                // set Excel 2007 export properties
                xlsxExport.PageBreaks = XlsxPageBreaks;
                xlsxExport.DataOnly = XlsxDataOnly;
                MemoryStream ms = new MemoryStream();
                xlsxExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "xlsx";
                ExportItem.ContentType = "application/vnd.ms-excel";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }


        /// <summary>
        /// Export in Word 2007 format
        /// </summary>
        public void ExportWord2007(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                Word2007Export docxExport = new Word2007Export();
                docxExport.OpenAfterExport = false;
                // set Word 2007 export properties
                docxExport.MatrixBased = DocxMatrixBased;
                MemoryStream ms = new MemoryStream();
                docxExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "docx";
                ExportItem.ContentType = "application/vnd.ms-word";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in PowerPoint 2007 format 
        /// </summary>
        public void ExportPowerPoint2007(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                PowerPoint2007Export pptxExport = new PowerPoint2007Export();
                pptxExport.OpenAfterExport = false;
                // set Power Point 2007 properties
                pptxExport.ImageFormat = PptxImageFormat;
                MemoryStream ms = new MemoryStream();
                pptxExport.Export(Report, ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "pptx";
                ExportItem.ContentType = "application/vnd.ms-powerpoint ";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Export in prepared report
        /// </summary>
        public void ExportPrepared(HttpContext context)
        {
            if (State == ReportState.Done && TotalPages > 0)
            {
                WebExportItem ExportItem = new WebExportItem();
                MemoryStream ms = new MemoryStream();
                Report.SavePrepared(ms);
                ExportItem.File = ms.ToArray();
                ExportItem.Format = "fpx";
                ExportItem.ContentType = "application/fpx";
                ResponseExport(ReportGuid, ExportItem, context);
            }
        }

        /// <summary>
        /// Print in Adobe Acrobat
        /// </summary>
        public void PrintPdf(HttpContext context)
        {
            ExportPdf(context, true);
        }

        /// <summary>
        /// Print in browser
        /// </summary>
        public void PrintHtml(HttpContext context)
        {
            ExportHtml(context, true, true);
        }
        #endregion
    }
}
