﻿using MrivcControls;
using MrivcControls.Data;
using MrivcControls.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo
{
    public partial class CommandReportForm : Form
    {
        public CommandReportForm()
        {
            InitializeComponent();
        }

        Tuple<DateTime,DateTime> CurrentDates()
        {
            var dt = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.CurrentPeriodDatesSelect");

            var beginDate = (DateTime)dt.Rows[0][0];
            var endDate = (DateTime)dt.Rows[0][1];

            return new Tuple<DateTime, DateTime>(beginDate, endDate);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string senderId = null;


            var dates = CurrentDates();

            textBox1.Text = DAccess.DataModule.GisCommandReport(senderId, dates.Item1, false, dates.Item2);
            //textBox2.Text = DAccess.DataModule.GisCommandReport(senderId, (DateTime)comboBox1.SelectedValue, true);
        }

        private void CommandReportForm_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var dates = CurrentDates();

            foreach (string si in MrivcData.SenderList())
            {
                MrivcStatic.SendCommandReport(si, dates.Item1, dates.Item2);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
