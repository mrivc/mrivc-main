﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Payments.DS;

namespace Payments.LogicClasses
{
    public class PaymentClass
    {
        int idPayment;
        int idPaymentClass;
        string name;
        bool makeCheckCopy;
        bool showParameterQuantity;

        public PaymentClass(int idPayment, DataRow paymentClassRow)
        {
            this.idPayment = idPayment;
            idPaymentClass = (int)paymentClassRow["ID_Payment_Class"];
            name = paymentClassRow["Name"].ToString();
            makeCheckCopy = (bool)paymentClassRow["MakeCheckCopy"];
            showParameterQuantity = (bool)paymentClassRow["ShowParameterQuantity"];
        }

        public PaymentClass(DataRow paymentClassRow)
        {
            idPayment = -1;
            idPaymentClass = (int)paymentClassRow["ID_Payment_Class"];
            name = paymentClassRow["Name"].ToString();
            makeCheckCopy = (bool)paymentClassRow["MakeCheckCopy"];
            showParameterQuantity = (bool)paymentClassRow["ShowParameterQuantity"];
        }

        public int IdPayment
        {
            get { return idPayment; }
            set { idPayment = value; }
        }

        public int IdPaymentClass {
            get { return idPaymentClass; } 
        }

        public string Name {
            get { return name; } 
        }

        public bool MakeCheckCopy {
            get { return makeCheckCopy; } 
        }

        public bool ShowParameterQuantity
        {
            get { return showParameterQuantity; } 
        }
    }
}
