﻿namespace GisGkh.Forms
{
    partial class DebtForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExportDept = new System.Windows.Forms.Button();
            this.senderIdOnlyFilter1 = new Helper.Controls.SenderIdOnlyFilter();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.buttonImportDebt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonExportDept
            // 
            this.buttonExportDept.Location = new System.Drawing.Point(9, 106);
            this.buttonExportDept.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonExportDept.Name = "buttonExportDept";
            this.buttonExportDept.Size = new System.Drawing.Size(238, 27);
            this.buttonExportDept.TabIndex = 0;
            this.buttonExportDept.Text = "Экспорт задолженности";
            this.buttonExportDept.UseVisualStyleBackColor = true;
            this.buttonExportDept.Click += new System.EventHandler(this.buttonExportDept_Click);
            // 
            // senderIdOnlyFilter1
            // 
            this.senderIdOnlyFilter1.Location = new System.Drawing.Point(9, 9);
            this.senderIdOnlyFilter1.Name = "senderIdOnlyFilter1";
            this.senderIdOnlyFilter1.Size = new System.Drawing.Size(395, 59);
            this.senderIdOnlyFilter1.TabIndex = 1;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(9, 73);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(153, 73);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker2.TabIndex = 3;
            // 
            // buttonImportDebt
            // 
            this.buttonImportDebt.Location = new System.Drawing.Point(251, 106);
            this.buttonImportDebt.Margin = new System.Windows.Forms.Padding(2);
            this.buttonImportDebt.Name = "buttonImportDebt";
            this.buttonImportDebt.Size = new System.Drawing.Size(238, 27);
            this.buttonImportDebt.TabIndex = 4;
            this.buttonImportDebt.Text = "Импорт задолженности";
            this.buttonImportDebt.UseVisualStyleBackColor = true;
            this.buttonImportDebt.Click += new System.EventHandler(this.buttonImportDebt_Click);
            // 
            // DebtForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 148);
            this.Controls.Add(this.buttonImportDebt);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.senderIdOnlyFilter1);
            this.Controls.Add(this.buttonExportDept);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "DebtForm";
            this.Text = "DebtForm";
            this.Load += new System.EventHandler(this.DebtForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonExportDept;
        private Helper.Controls.SenderIdOnlyFilter senderIdOnlyFilter1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button buttonImportDebt;
    }
}