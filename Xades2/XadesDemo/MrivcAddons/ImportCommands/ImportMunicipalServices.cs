﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ImportMunicipalServices : MrivcImportCommand
    {
        public ImportMunicipalServices(string senderID) : base("importMunicipalServices", "NsiAsync", senderID)
        {
            _importValueName = "ImportMainMunicipalService";
            Description = "Синхронизация услуг";
        }

        public new void Execute()
        {
            var exportDataProvider = new ExportDataProvider(SenderID);
            exportDataProvider.AddToReport = this.AddToReport;
            exportDataProvider.Description = this.Description;
            var dataProviderRegNumbers = "51";
            exportDataProvider.ExecuteToDataBase(dataProviderRegNumbers.Split(','));

            HasErrors = exportDataProvider.HasErrors;

            if (!HasErrors)
                base.Execute();

            if (!this.IsEmpty && !HasErrors)
                exportDataProvider.ExecuteToDataBase(dataProviderRegNumbers.Split(','));
        }

        protected override void CreateXmlBody()
        {
            var munServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.Prepare_Import_Municipal_Services @senderID", new[,] { { "@senderID", SenderID } });

            var commonValues = new[]
            {
                new Tuple<string[],string>(new [] { "MunicipalServiceRef", "Code" },"MunicipalServiceRefCode"),
                new Tuple<string[],string>(new [] { "MunicipalServiceRef", "GUID" },"MunicipalServiceRefGUID"),
                new Tuple<string[],string>(new [] { "MunicipalServiceRef", "Name" },"MunicipalServiceRefName"),
                new Tuple<string[],string>(new [] { "MainMunicipalServiceName" },"MainMunicipalServiceName"),
                new Tuple<string[],string>(new [] { "MunicipalResourceRef", "Code" },"MunicipalResourceRefCode"),
                new Tuple<string[],string>(new [] { "MunicipalResourceRef", "GUID" },"MunicipalResourceRefGUID"),
                new Tuple<string[],string>(new [] { "MunicipalResourceRef", "Name" },"MunicipalResourceRefName"),
                new Tuple<string[],string>(new [] { "OKEI" },"OKEI")
            };

            foreach (DataRow row in munServices.Select())
            {
                AddChildElement(row, "NsiAsync", "importMunicipalServices_add", "importMunicipalServices_add", commonValues, null, null, "importMunicipalServicesRequest");
            }
        }
    }
}
