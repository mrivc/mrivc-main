﻿using System;

namespace PrintDeliverySend
{
    using System.Data;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using System.Threading;
    using System.Windows.Forms;

    using FastReport;
    using FastReport.Export.Pdf;

    using PrintDeliverySend.Helpers;
    using PrintDeliverySend.Properties;

    public class BillSender
    {
        private Report report1;

        private PDFExport pdfExport;

        private SmtpClient smtpClient;

        public event EventHandler SendingFinished;

        public BillSender()
        {
            smtpClient = new SmtpClient(Settings.Default.SmtpServerHost, Settings.Default.SmtpServerPort);
            smtpClient.Credentials = new NetworkCredential(Settings.Default.SmtpServerMailFrom, Settings.Default.SmtpServerPswd);
            smtpClient.EnableSsl = Settings.Default.SmtpServerSSL;
            CountSendedMessages = 0;
            CountSendedMessagesWithError = 0;
        }

        public int CountSendedMessages
        {
            get; private set;
        }

        public int CountSendedMessagesWithError
        {
            get; private set;
        }

        public void StartSending()
        {
            FileHelper.WriteLog("Начата рассылка...");

            PrepareAccounts();

            FileHelper.WriteLog("Закончена рассылка.");

            if (SendingFinished != null)
                this.SendingFinished(this, new EventArgs());


        }

        public void PrepareAccounts()
        {
            InitPdfExport();

            DataTable dt = DModule.Instance.GetPrintDeliveryData();

            foreach (DataRow row in dt.Rows)
            {
                long idPrint = long.Parse(row["ID_Print"].ToString());
                int idAccount = int.Parse(row["ID_Account"].ToString());
                int idClientDetail = int.Parse(row["ID_Client_Detail"].ToString());
                int period = int.Parse(row["Period"].ToString());
                string account = row["Account"].ToString();
                string accountText = row["AccountText"].ToString();
                string address = row["Address"].ToString();
                string billKind = row["Bill_Kind"].ToString();
                string email = row["email"].ToString();
                bool IsDebt = int.Parse(row["IS_DEBT"].ToString()) == 0 ? false : true;
                string periodName = DModule.Instance.GetPeriodName(period);
                int periodYear = DModule.Instance.GetPeriodYear(period);
                int periodMonth = DModule.Instance.GetPeriodMonth(period);

                InitReportTemplate();
                report1.SetParameterValue("IDPrint", idPrint);
                report1.SetParameterValue("IDAccount", idAccount);
                report1.SetParameterValue("IS_DEBT", IsDebt);
                report1.SetParameterValue("PrintSummaryPage", false);
                report1.SetParameterValue("PrintBillsWithSubscription", true);
                report1.SetParameterValue("PrintFio", true);

                string pathToSave = string.Format("{0}\\{1}\\{2}\\", Settings.Default.PathToBill, periodYear, StringHelper.ZeroCode(periodMonth));
                FileHelper.CreateDirectory(pathToSave);

                string fileName = GetFileName(account, idClientDetail, period) + ".pdf";
                if (report1.Prepare(true))
                {
                    pdfExport.Subject = "МУП \"МРИВЦ\"";
                    pdfExport.Title = billKind;

                    report1.Export(pdfExport, pathToSave + fileName);

                    report1.Clear();
                    report1.Dispose();

                    string error = SendMail(Settings.Default.SmtpServerHost,
                           Settings.Default.SmtpServerMailFrom,
                           Settings.Default.SmtpServerPswd,
                           email,
                           Settings.Default.SendCopyTo,
                           Settings.Default.MailSubject.Replace("@Period@", periodName).Replace("@BillKind@", billKind).Replace("@Account@", account).Replace("@Address@", address),
                           Settings.Default.MailBody.Replace("@Period@", periodName).Replace("@BillKind@", billKind).Replace("@Account@", account).Replace("@Address@", address).Replace("@AccountText@", accountText),
                           pathToSave + fileName);

                    Thread.Sleep(1000);

                    if (error.Length == 0)
                    {
                        DModule.Instance.InsertPrintDeliveryInfo(idPrint, period, idAccount, idClientDetail, email, fileName, 1, "");
                    }
                    else
                    {
                        DModule.Instance.InsertPrintDeliveryInfo(idPrint, period, idAccount, idClientDetail, email, fileName, 0, error);
                    }

                    this.CountSendedMessages++;
                }
            }
        }

        private string GetFileName(string account, int idClientDetail, int period)
        {
            return string.Format("{0}_{1}_{2}_{3}", account, DModule.Instance.GetPeriodMY(period), idClientDetail, StringHelper.GetShortTimeDateString());
        }

        private void InitReportTemplate()
        {
            report1 = new Report();
            report1.LoadFromString(BillTemplate);
            report1.DoublePass = false;
            report1.UseFileCache = false;
            report1.SetParameterValue("MainConnection", DModule.Instance.MainConnection.ConnectionString);
        }

        private void InitPdfExport()
        {
            pdfExport = new FastReport.Export.Pdf.PDFExport();
            pdfExport.ShowProgress = false;
            pdfExport.Compressed = true;
            pdfExport.AllowPrint = true;
            pdfExport.EmbeddingFonts = true;
        }

        private string billTemplate = string.Empty;

        public string BillTemplate
        {
            get
            {
                if (billTemplate == string.Empty) billTemplate = DModule.Instance.GetBillStdReport();
                return billTemplate;
            }
        }


        public string SendMail(string smtpServer, string from, string password, string mailto, string mailcopyto, string caption, string message, string attachFile = null)
        { 
            string error = string.Empty;
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);

                string[] mailsTo = mailto.Split(';');
                string[] mailsCopyTo = mailcopyto.Split(';');

                foreach (var s in mailsTo) if (s.Length > 0) mail.To.Add(new MailAddress(s.Trim()));

                foreach (var s in mailsCopyTo) if (s.Length > 0) mail.CC.Add(new MailAddress(s.Trim()));

                mail.Subject = caption;
                mail.Body = message;
                if (!string.IsNullOrEmpty(attachFile)) mail.Attachments.Add(new Attachment(attachFile));

                smtpClient.Send(mail);

                mail.Dispose();
                error = string.Empty;
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog("Mail.Send: " + ex.Message + " mailto:" + mailto);
                error = "Mail.Send: " + ex.Message + " mailto:" + mailto;
                CountSendedMessagesWithError++;
            }

            return error;
        }
    }
}
