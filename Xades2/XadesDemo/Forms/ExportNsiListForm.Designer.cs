﻿namespace XadesDemo
{
    partial class ExportNsiListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportNsiListForm));
            this.exportNsiListButton = new System.Windows.Forms.Button();
            this.exportNsiProviderBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // exportNsiListButton
            // 
            this.exportNsiListButton.Location = new System.Drawing.Point(12, 12);
            this.exportNsiListButton.Name = "exportNsiListButton";
            this.exportNsiListButton.Size = new System.Drawing.Size(194, 23);
            this.exportNsiListButton.TabIndex = 19;
            this.exportNsiListButton.Text = "Экспорт справочников NSI";
            this.exportNsiListButton.UseVisualStyleBackColor = true;
            this.exportNsiListButton.Click += new System.EventHandler(this.exportNsiListButton_Click);
            // 
            // exportNsiProviderBtn
            // 
            this.exportNsiProviderBtn.Location = new System.Drawing.Point(307, 47);
            this.exportNsiProviderBtn.Name = "exportNsiProviderBtn";
            this.exportNsiProviderBtn.Size = new System.Drawing.Size(194, 23);
            this.exportNsiProviderBtn.TabIndex = 24;
            this.exportNsiProviderBtn.Text = "Экспорт услуг";
            this.exportNsiProviderBtn.UseVisualStyleBackColor = true;
            this.exportNsiProviderBtn.Click += new System.EventHandler(this.exportNsiProviderBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(212, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(289, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "Экпорт ИД поставщиков информации";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ExportNsiListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 116);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.exportNsiProviderBtn);
            this.Controls.Add(this.exportNsiListButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportNsiListForm";
            this.Text = "Экспорт справочников и услуг";
            this.Load += new System.EventHandler(this.ExportNsiListForm_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button exportNsiListButton;
        private System.Windows.Forms.Button exportNsiProviderBtn;
        private System.Windows.Forms.Button button1;
    }
}