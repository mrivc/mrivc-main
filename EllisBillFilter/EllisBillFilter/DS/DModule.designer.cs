﻿namespace EllisBillFilter.DS
{
    partial class DModule
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DModule));
            this.sqlMainConnection = new System.Data.SqlClient.SqlConnection();
            this.sqlSelectCommand2 = new System.Data.SqlClient.SqlCommand();
            this.daDeriveCompany = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand3 = new System.Data.SqlClient.SqlCommand();
            this.daPayReceiver = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand4 = new System.Data.SqlClient.SqlCommand();
            this.daPeriod = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand5 = new System.Data.SqlClient.SqlCommand();
            this.daBillFilter = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdAnyCommand = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand7 = new System.Data.SqlClient.SqlCommand();
            this.daClientDetailHouse = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand6 = new System.Data.SqlClient.SqlCommand();
            this.daServiceList = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand8 = new System.Data.SqlClient.SqlCommand();
            this.daFilterType = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand1 = new System.Data.SqlClient.SqlCommand();
            this.daClientDetail = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand9 = new System.Data.SqlClient.SqlCommand();
            this.daHouseHierarhy = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand10 = new System.Data.SqlClient.SqlCommand();
            this.daFilterHouse = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand11 = new System.Data.SqlClient.SqlCommand();
            this.daHouseWithoutBillFilter = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand12 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand2 = new System.Data.SqlClient.SqlCommand();
            this.daClientDetailCopy = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand13 = new System.Data.SqlClient.SqlCommand();
            this.daBillKind = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdBillPrepareReportData = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand14 = new System.Data.SqlClient.SqlCommand();
            this.daBillHouseList = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand16 = new System.Data.SqlClient.SqlCommand();
            this.daClientDetailAgreement = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand17 = new System.Data.SqlClient.SqlCommand();
            this.daClientDetailAccount = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand15 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand2 = new System.Data.SqlClient.SqlCommand();
            this.daAgreement = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand18 = new System.Data.SqlClient.SqlCommand();
            this.daAgreementType = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand20 = new System.Data.SqlClient.SqlCommand();
            this.daRSaldo = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand19 = new System.Data.SqlClient.SqlCommand();
            this.daClients = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand21 = new System.Data.SqlClient.SqlCommand();
            this.daRSaldoHouseFilter = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand23 = new System.Data.SqlClient.SqlCommand();
            this.daProviderGroup = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdBillFilterProviderUpdate = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand24 = new System.Data.SqlClient.SqlCommand();
            this.daProviderGroupFilter = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand22 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand3 = new System.Data.SqlClient.SqlCommand();
            this.daBillFilterDetailProviderFilter = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand25 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand3 = new System.Data.SqlClient.SqlCommand();
            this.daClientDetailDeliverySettings = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand26 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand5 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand5 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand4 = new System.Data.SqlClient.SqlCommand();
            this.daClientDetailDeliveryEmail = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdClientDetailDeliveryLastSend = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand29 = new System.Data.SqlClient.SqlCommand();
            this.daDebtBillClientDetail = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand27 = new System.Data.SqlClient.SqlCommand();
            this.daDebtBillSaldo = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdCreateClientDetail = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand28 = new System.Data.SqlClient.SqlCommand();
            this.daClaimDeriveCompany = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand30 = new System.Data.SqlClient.SqlCommand();
            this.daClaimProviderGroup = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand31 = new System.Data.SqlClient.SqlCommand();
            this.daClaimCanAdd = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            this.cmdCanAddClaim = new System.Data.SqlClient.SqlCommand();
            this.cmdClaimAddEdit = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand32 = new System.Data.SqlClient.SqlCommand();
            this.daCompanyServiceNumber = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand33 = new System.Data.SqlClient.SqlCommand();
            this.daPrintBillDeliveryAccount = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdCheckBindAddress = new System.Data.SqlClient.SqlCommand();
            this.cmdLawsuitAddEdit = new System.Data.SqlClient.SqlCommand();
            this.daRSaldoSber = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlCommand4 = new System.Data.SqlClient.SqlCommand();
            this.daRSaldoSberMeasure = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlCommand5 = new System.Data.SqlClient.SqlCommand();
            // 
            // sqlMainConnection
            // 
            this.sqlMainConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlSelectCommand2
            // 
            this.sqlSelectCommand2.CommandText = "Reports.ClientDetail_DeriveCompany_Select";
            this.sqlSelectCommand2.CommandTimeout = 0;
            this.sqlSelectCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand2.Connection = this.sqlMainConnection;
            this.sqlSelectCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // daDeriveCompany
            // 
            this.daDeriveCompany.SelectCommand = this.sqlSelectCommand2;
            this.daDeriveCompany.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_DeriveCompany_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Company", "ID_Company"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Is_Provider", "Is_Provider"),
                        new System.Data.Common.DataColumnMapping("Is_Derive_Company", "Is_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("Is_Pay_Receiver", "Is_Pay_Receiver"),
                        new System.Data.Common.DataColumnMapping("Code", "Code"),
                        new System.Data.Common.DataColumnMapping("Meter_Future_Period_Day", "Meter_Future_Period_Day"),
                        new System.Data.Common.DataColumnMapping("Load_Measure_In_Future", "Load_Measure_In_Future")})});
            // 
            // sqlSelectCommand3
            // 
            this.sqlSelectCommand3.CommandText = "Reports.ClientDetail_PayReceiverCompany_Select";
            this.sqlSelectCommand3.CommandTimeout = 0;
            this.sqlSelectCommand3.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand3.Connection = this.sqlMainConnection;
            this.sqlSelectCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // daPayReceiver
            // 
            this.daPayReceiver.SelectCommand = this.sqlSelectCommand3;
            this.daPayReceiver.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_PayReceiverCompany_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Company", "ID_Company"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Is_Provider", "Is_Provider"),
                        new System.Data.Common.DataColumnMapping("Is_Derive_Company", "Is_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("Is_Pay_Receiver", "Is_Pay_Receiver"),
                        new System.Data.Common.DataColumnMapping("Code", "Code"),
                        new System.Data.Common.DataColumnMapping("Meter_Future_Period_Day", "Meter_Future_Period_Day"),
                        new System.Data.Common.DataColumnMapping("Load_Measure_In_Future", "Load_Measure_In_Future")})});
            // 
            // sqlSelectCommand4
            // 
            this.sqlSelectCommand4.CommandText = resources.GetString("sqlSelectCommand4.CommandText");
            this.sqlSelectCommand4.CommandTimeout = 0;
            this.sqlSelectCommand4.Connection = this.sqlMainConnection;
            // 
            // daPeriod
            // 
            this.daPeriod.SelectCommand = this.sqlSelectCommand4;
            this.daPeriod.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "VPeriod", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("period", "period"),
                        new System.Data.Common.DataColumnMapping("period_name", "period_name")})});
            // 
            // sqlSelectCommand5
            // 
            this.sqlSelectCommand5.CommandText = "select * from bill_filter";
            this.sqlSelectCommand5.CommandTimeout = 0;
            this.sqlSelectCommand5.Connection = this.sqlMainConnection;
            // 
            // daBillFilter
            // 
            this.daBillFilter.SelectCommand = this.sqlSelectCommand5;
            this.daBillFilter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Bill_Filter", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_bill_filter", "id_bill_filter"),
                        new System.Data.Common.DataColumnMapping("bill_filter", "bill_filter")})});
            // 
            // cmdAnyCommand
            // 
            this.cmdAnyCommand.CommandTimeout = 0;
            this.cmdAnyCommand.Connection = this.sqlMainConnection;
            // 
            // sqlSelectCommand7
            // 
            this.sqlSelectCommand7.CommandText = "Reports.ClientDetailHouseLinked_Select";
            this.sqlSelectCommand7.CommandTimeout = 0;
            this.sqlSelectCommand7.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand7.Connection = this.sqlMainConnection;
            this.sqlSelectCommand7.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Show_All_Houses", System.Data.SqlDbType.Bit, 2)});
            // 
            // daClientDetailHouse
            // 
            this.daClientDetailHouse.SelectCommand = this.sqlSelectCommand7;
            this.daClientDetailHouse.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetailHouseLinked_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_client_detail", "id_client_detail"),
                        new System.Data.Common.DataColumnMapping("ID_House", "ID_House"),
                        new System.Data.Common.DataColumnMapping("House_Address", "House_Address")})});
            // 
            // sqlSelectCommand6
            // 
            this.sqlSelectCommand6.CommandText = "Reports.ClientDetail_ServiceList";
            this.sqlSelectCommand6.CommandTimeout = 0;
            this.sqlSelectCommand6.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand6.Connection = this.sqlMainConnection;
            this.sqlSelectCommand6.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // daServiceList
            // 
            this.daServiceList.SelectCommand = this.sqlSelectCommand6;
            this.daServiceList.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_ServiceList", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Service", "ID_Service"),
                        new System.Data.Common.DataColumnMapping("Name", "Name")})});
            // 
            // sqlSelectCommand8
            // 
            this.sqlSelectCommand8.CommandText = "select *\r\nfrom reports.client_detail_filter_type";
            this.sqlSelectCommand8.CommandTimeout = 0;
            this.sqlSelectCommand8.Connection = this.sqlMainConnection;
            // 
            // daFilterType
            // 
            this.daFilterType.SelectCommand = this.sqlSelectCommand8;
            this.daFilterType.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Client_Detail_Filter_Type", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Filter_Type", "ID_Filter_Type"),
                        new System.Data.Common.DataColumnMapping("Name", "Name")})});
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "Reports.ClientDetail_Data_Select";
            this.sqlSelectCommand1.CommandTimeout = 0;
            this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand1.Connection = this.sqlMainConnection;
            this.sqlSelectCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ShowActive", System.Data.SqlDbType.Bit, 1)});
            // 
            // sqlInsertCommand1
            // 
            this.sqlInsertCommand1.CommandText = "Reports.ClientDetail_Data_Insert";
            this.sqlInsertCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand1.Connection = this.sqlMainConnection;
            this.sqlInsertCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "ID_Client_Detail", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Client", System.Data.SqlDbType.Int, 4, "ID_Client"),
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4, "ID_Derive_Company"),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4, "ID_Bill_Filter"),
            new System.Data.SqlClient.SqlParameter("@Begin_Period", System.Data.SqlDbType.Int, 4, "Begin_Period"),
            new System.Data.SqlClient.SqlParameter("@End_Period", System.Data.SqlDbType.Int, 4, "End_Period"),
            new System.Data.SqlClient.SqlParameter("@Advance", System.Data.SqlDbType.Float, 8, "Advance"),
            new System.Data.SqlClient.SqlParameter("@RoundTo", System.Data.SqlDbType.Int, 4, "RoundTo"),
            new System.Data.SqlClient.SqlParameter("@Prefix", System.Data.SqlDbType.VarChar, 30, "Prefix"),
            new System.Data.SqlClient.SqlParameter("@Show_Meter_For_Services", System.Data.SqlDbType.VarChar, 50, "Show_Meter_For_Services"),
            new System.Data.SqlClient.SqlParameter("@ID_Filter_Type", System.Data.SqlDbType.Int, 4, "ID_Filter_Type"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 2147483647, "Comment"),
            new System.Data.SqlClient.SqlParameter("@Bill_Name", System.Data.SqlDbType.VarChar, 100, "Bill_Name")});
            // 
            // sqlUpdateCommand1
            // 
            this.sqlUpdateCommand1.CommandText = "Reports.ClientDetail_Data_Update";
            this.sqlUpdateCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand1.Connection = this.sqlMainConnection;
            this.sqlUpdateCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, "ID_Client_Detail"),
            new System.Data.SqlClient.SqlParameter("@ID_Client", System.Data.SqlDbType.Int, 4, "ID_Client"),
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4, "ID_Derive_Company"),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4, "ID_Bill_Filter"),
            new System.Data.SqlClient.SqlParameter("@Begin_Period", System.Data.SqlDbType.Int, 4, "Begin_Period"),
            new System.Data.SqlClient.SqlParameter("@End_Period", System.Data.SqlDbType.Int, 4, "End_Period"),
            new System.Data.SqlClient.SqlParameter("@Advance", System.Data.SqlDbType.Float, 8, "Advance"),
            new System.Data.SqlClient.SqlParameter("@RoundTo", System.Data.SqlDbType.Int, 4, "RoundTo"),
            new System.Data.SqlClient.SqlParameter("@Prefix", System.Data.SqlDbType.VarChar, 30, "Prefix"),
            new System.Data.SqlClient.SqlParameter("@Show_Meter_For_Services", System.Data.SqlDbType.VarChar, 50, "Show_Meter_For_Services"),
            new System.Data.SqlClient.SqlParameter("@ID_Filter_Type", System.Data.SqlDbType.Int, 4, "ID_Filter_Type"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 2147483647, "Comment"),
            new System.Data.SqlClient.SqlParameter("@Bill_Name", System.Data.SqlDbType.VarChar, 100, "Bill_Name")});
            // 
            // sqlDeleteCommand1
            // 
            this.sqlDeleteCommand1.CommandText = "Reports.ClientDetail_Data_Delete";
            this.sqlDeleteCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand1.Connection = this.sqlMainConnection;
            this.sqlDeleteCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, "ID_Client_Detail")});
            // 
            // daClientDetail
            // 
            this.daClientDetail.DeleteCommand = this.sqlDeleteCommand1;
            this.daClientDetail.InsertCommand = this.sqlInsertCommand1;
            this.daClientDetail.SelectCommand = this.sqlSelectCommand1;
            this.daClientDetail.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail", "ID_Client_Detail"),
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("ID_Derive_Company", "ID_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Filter", "ID_Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("Begin_Period", "Begin_Period"),
                        new System.Data.Common.DataColumnMapping("End_Period", "End_Period"),
                        new System.Data.Common.DataColumnMapping("Advance", "Advance"),
                        new System.Data.Common.DataColumnMapping("RoundTo", "RoundTo"),
                        new System.Data.Common.DataColumnMapping("Prefix", "Prefix"),
                        new System.Data.Common.DataColumnMapping("Show_Meter_For_Services", "Show_Meter_For_Services"),
                        new System.Data.Common.DataColumnMapping("ID_Filter_Type", "ID_Filter_Type"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("Derive_Company_Code", "Derive_Company_Code"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy"),
                        new System.Data.Common.DataColumnMapping("CreationDate", "CreationDate"),
                        new System.Data.Common.DataColumnMapping("LastUpdateBy", "LastUpdateBy"),
                        new System.Data.Common.DataColumnMapping("LastUpdateDate", "LastUpdateDate"),
                        new System.Data.Common.DataColumnMapping("Bill_Name", "Bill_Name")}),
            new System.Data.Common.DataTableMapping("Table1", "Table1", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail", "ID_Client_Detail"),
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("ID_Derive_Company", "ID_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Filter", "ID_Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("Begin_Period", "Begin_Period"),
                        new System.Data.Common.DataColumnMapping("End_Period", "End_Period"),
                        new System.Data.Common.DataColumnMapping("Advance", "Advance"),
                        new System.Data.Common.DataColumnMapping("RoundTo", "RoundTo"),
                        new System.Data.Common.DataColumnMapping("Prefix", "Prefix"),
                        new System.Data.Common.DataColumnMapping("Show_Meter_For_Services", "Show_Meter_For_Services"),
                        new System.Data.Common.DataColumnMapping("ID_Filter_Type", "ID_Filter_Type"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment")})});
            this.daClientDetail.UpdateCommand = this.sqlUpdateCommand1;
            // 
            // sqlSelectCommand9
            // 
            this.sqlSelectCommand9.CommandText = "Reports.ClientDetail_HouseHierarhy";
            this.sqlSelectCommand9.CommandTimeout = 0;
            this.sqlSelectCommand9.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand9.Connection = this.sqlMainConnection;
            this.sqlSelectCommand9.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // daHouseHierarhy
            // 
            this.daHouseHierarhy.SelectCommand = this.sqlSelectCommand9;
            this.daHouseHierarhy.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_HouseHierarhy", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id", "id"),
                        new System.Data.Common.DataColumnMapping("id_parent", "id_parent"),
                        new System.Data.Common.DataColumnMapping("name", "name")})});
            // 
            // sqlSelectCommand10
            // 
            this.sqlSelectCommand10.CommandText = "Reports.ClientDetail_FilterHouse_Select";
            this.sqlSelectCommand10.CommandTimeout = 0;
            this.sqlSelectCommand10.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand10.Connection = this.sqlMainConnection;
            this.sqlSelectCommand10.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_House", System.Data.SqlDbType.Int, 4)});
            // 
            // daFilterHouse
            // 
            this.daFilterHouse.SelectCommand = this.sqlSelectCommand10;
            this.daFilterHouse.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_FilterHouse_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("IsChecked", "IsChecked"),
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail", "ID_Client_Detail"),
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("ID_Derive_Company", "ID_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Filter", "ID_Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("Begin_Period", "Begin_Period"),
                        new System.Data.Common.DataColumnMapping("End_Period", "End_Period"),
                        new System.Data.Common.DataColumnMapping("Advance", "Advance"),
                        new System.Data.Common.DataColumnMapping("RoundTo", "RoundTo"),
                        new System.Data.Common.DataColumnMapping("Prefix", "Prefix"),
                        new System.Data.Common.DataColumnMapping("Show_Meter_For_Services", "Show_Meter_For_Services"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("ID_Filter_Type", "ID_Filter_Type")})});
            // 
            // sqlSelectCommand11
            // 
            this.sqlSelectCommand11.CommandText = "select *\r\nfrom [Reports].[VClient_Detail_House_Without_BillFilter]\r\norder by hous" +
    "e_address";
            this.sqlSelectCommand11.CommandTimeout = 0;
            this.sqlSelectCommand11.Connection = this.sqlMainConnection;
            // 
            // daHouseWithoutBillFilter
            // 
            this.daHouseWithoutBillFilter.SelectCommand = this.sqlSelectCommand11;
            // 
            // sqlSelectCommand12
            // 
            this.sqlSelectCommand12.CommandText = "Reports.ClientDetail_Data_Select";
            this.sqlSelectCommand12.CommandTimeout = 0;
            this.sqlSelectCommand12.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand12.Connection = this.sqlMainConnection;
            this.sqlSelectCommand12.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ShowActive", System.Data.SqlDbType.Bit, 1)});
            // 
            // sqlInsertCommand2
            // 
            this.sqlInsertCommand2.CommandText = "Reports.ClientDetail_Data_Copy";
            this.sqlInsertCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand2.Connection = this.sqlMainConnection;
            this.sqlInsertCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "ID_Client_Detail", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail_Source", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Client", System.Data.SqlDbType.Int, 4, "ID_Client"),
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4, "ID_Derive_Company"),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4, "ID_Bill_Filter"),
            new System.Data.SqlClient.SqlParameter("@Begin_Period", System.Data.SqlDbType.Int, 4, "Begin_Period"),
            new System.Data.SqlClient.SqlParameter("@End_Period", System.Data.SqlDbType.Int, 4, "End_Period"),
            new System.Data.SqlClient.SqlParameter("@Advance", System.Data.SqlDbType.Float, 8, "Advance"),
            new System.Data.SqlClient.SqlParameter("@RoundTo", System.Data.SqlDbType.Int, 4, "RoundTo"),
            new System.Data.SqlClient.SqlParameter("@Prefix", System.Data.SqlDbType.VarChar, 30, "Prefix"),
            new System.Data.SqlClient.SqlParameter("@Show_Meter_For_Services", System.Data.SqlDbType.VarChar, 50, "Show_Meter_For_Services"),
            new System.Data.SqlClient.SqlParameter("@ID_Filter_Type", System.Data.SqlDbType.Int, 4, "ID_Filter_Type"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 2147483647, "Comment"),
            new System.Data.SqlClient.SqlParameter("@Bill_Name", System.Data.SqlDbType.VarChar, 100, "Bill_Name")});
            // 
            // daClientDetailCopy
            // 
            this.daClientDetailCopy.InsertCommand = this.sqlInsertCommand2;
            this.daClientDetailCopy.SelectCommand = this.sqlSelectCommand12;
            this.daClientDetailCopy.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_Data_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail", "ID_Client_Detail"),
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("ID_Derive_Company", "ID_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("Derive_Company_Code", "Derive_Company_Code"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Filter", "ID_Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("Begin_Period", "Begin_Period"),
                        new System.Data.Common.DataColumnMapping("End_Period", "End_Period"),
                        new System.Data.Common.DataColumnMapping("Advance", "Advance"),
                        new System.Data.Common.DataColumnMapping("RoundTo", "RoundTo"),
                        new System.Data.Common.DataColumnMapping("Prefix", "Prefix"),
                        new System.Data.Common.DataColumnMapping("Show_Meter_For_Services", "Show_Meter_For_Services"),
                        new System.Data.Common.DataColumnMapping("ID_Filter_Type", "ID_Filter_Type"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("DeriveCompanyHasHouses", "DeriveCompanyHasHouses"),
                        new System.Data.Common.DataColumnMapping("Empty", "Empty")})});
            // 
            // sqlSelectCommand13
            // 
            this.sqlSelectCommand13.CommandText = "SELECT        ID_Bill_Kind, Is_Debt, Position, Bill_Kind, Filter_Codes, Order_For" +
    "_Cut\r\nFROM            Reports.Bill_Kind\r\nWHERE        (Is_Debt = 0)\r\nORDER BY Bi" +
    "ll_Kind";
            this.sqlSelectCommand13.CommandTimeout = 0;
            // 
            // daBillKind
            // 
            this.daBillKind.SelectCommand = this.sqlSelectCommand13;
            this.daBillKind.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Bill_Kind", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Bill_Kind", "ID_Bill_Kind"),
                        new System.Data.Common.DataColumnMapping("Is_Debt", "Is_Debt"),
                        new System.Data.Common.DataColumnMapping("Position", "Position"),
                        new System.Data.Common.DataColumnMapping("Bill_Kind", "Bill_Kind"),
                        new System.Data.Common.DataColumnMapping("Filter_Codes", "Filter_Codes"),
                        new System.Data.Common.DataColumnMapping("Order_For_Cut", "Order_For_Cut")})});
            // 
            // cmdBillPrepareReportData
            // 
            this.cmdBillPrepareReportData.CommandText = "Reports.Prepare_Bill_Report_Data";
            this.cmdBillPrepareReportData.CommandTimeout = 0;
            this.cmdBillPrepareReportData.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdBillPrepareReportData.Connection = this.sqlMainConnection;
            this.cmdBillPrepareReportData.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Print", System.Data.SqlDbType.BigInt, 8),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Kind", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Print_Zero_Param", System.Data.SqlDbType.Bit, 2),
            new System.Data.SqlClient.SqlParameter("@Print_Empty_Flat", System.Data.SqlDbType.Bit, 2),
            new System.Data.SqlClient.SqlParameter("@ID_Company", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@SavePrintHistory", System.Data.SqlDbType.Bit, 2),
            new System.Data.SqlClient.SqlParameter("@PrintedBackSide", System.Data.SqlDbType.Bit, 2),
            new System.Data.SqlClient.SqlParameter("@PrintBillsForSubscribers", System.Data.SqlDbType.Bit, 2)});
            // 
            // sqlSelectCommand14
            // 
            this.sqlSelectCommand14.CommandText = "[Reports].[ClientDetail_PrintBillHouseFilter_Select]";
            this.sqlSelectCommand14.CommandTimeout = 0;
            this.sqlSelectCommand14.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand14.Connection = this.sqlMainConnection;
            this.sqlSelectCommand14.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@period", System.Data.SqlDbType.Int, 4, "Period"),
            new System.Data.SqlClient.SqlParameter("@id_client_detail", System.Data.SqlDbType.Int, 4, "ID_Client_Detail")});
            // 
            // daBillHouseList
            // 
            this.daBillHouseList.SelectCommand = this.sqlSelectCommand14;
            this.daBillHouseList.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "VHouse_Address_All", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_house", "id_house"),
                        new System.Data.Common.DataColumnMapping("house_address", "house_address")})});
            // 
            // sqlSelectCommand16
            // 
            this.sqlSelectCommand16.CommandText = resources.GetString("sqlSelectCommand16.CommandText");
            this.sqlSelectCommand16.CommandTimeout = 0;
            this.sqlSelectCommand16.Connection = this.sqlMainConnection;
            this.sqlSelectCommand16.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, "ID_Client_Detail")});
            // 
            // daClientDetailAgreement
            // 
            this.daClientDetailAgreement.DeleteCommand = this.sqlCommand3;
            this.daClientDetailAgreement.InsertCommand = this.sqlCommand1;
            this.daClientDetailAgreement.SelectCommand = this.sqlSelectCommand16;
            this.daClientDetailAgreement.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "VClient_Detail_Agreement", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail", "ID_Client_Detail"),
                        new System.Data.Common.DataColumnMapping("ID_Agreement", "ID_Agreement"),
                        new System.Data.Common.DataColumnMapping("Begin_Period", "Begin_Period"),
                        new System.Data.Common.DataColumnMapping("End_Period", "End_Period"),
                        new System.Data.Common.DataColumnMapping("Agreement_Date", "Agreement_Date"),
                        new System.Data.Common.DataColumnMapping("Description", "Description"),
                        new System.Data.Common.DataColumnMapping("Agent", "Agent"),
                        new System.Data.Common.DataColumnMapping("Principal1", "Principal1"),
                        new System.Data.Common.DataColumnMapping("Principal2", "Principal2"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy"),
                        new System.Data.Common.DataColumnMapping("CreationDate", "CreationDate")})});
            this.daClientDetailAgreement.UpdateCommand = this.sqlCommand2;
            // 
            // sqlCommand3
            // 
            this.sqlCommand3.CommandText = "DELETE FROM Reports.Client_Detail_Agreement\r\nWHERE        (ID_Client_Detail = @ID" +
    "_Client_Detail) AND (ID_Agreement = @ID_Agreement)";
            this.sqlCommand3.Connection = this.sqlMainConnection;
            this.sqlCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID_Client_Detail", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@ID_Agreement", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID_Agreement", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlCommand1
            // 
            this.sqlCommand1.CommandText = resources.GetString("sqlCommand1.CommandText");
            this.sqlCommand1.Connection = this.sqlMainConnection;
            this.sqlCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, "ID_Client_Detail"),
            new System.Data.SqlClient.SqlParameter("@ID_Agreement", System.Data.SqlDbType.Int, 4, "ID_Agreement"),
            new System.Data.SqlClient.SqlParameter("@Begin_Period", System.Data.SqlDbType.Int, 4, "Begin_Period"),
            new System.Data.SqlClient.SqlParameter("@End_Period", System.Data.SqlDbType.Int, 4, "End_Period")});
            // 
            // sqlCommand2
            // 
            this.sqlCommand2.CommandText = resources.GetString("sqlCommand2.CommandText");
            this.sqlCommand2.Connection = this.sqlMainConnection;
            this.sqlCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Begin_Period", System.Data.SqlDbType.Int, 4, "Begin_Period"),
            new System.Data.SqlClient.SqlParameter("@End_Period", System.Data.SqlDbType.Int, 4, "End_Period"),
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID_Client_Detail", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@ID_Agreement", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID_Agreement", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlSelectCommand17
            // 
            this.sqlSelectCommand17.CommandText = "Reports.ClientDetail_Account_BillFilter";
            this.sqlSelectCommand17.CommandTimeout = 0;
            this.sqlSelectCommand17.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand17.Connection = this.sqlMainConnection;
            this.sqlSelectCommand17.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@id_account", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@period", System.Data.SqlDbType.Int, 4)});
            // 
            // daClientDetailAccount
            // 
            this.daClientDetailAccount.SelectCommand = this.sqlSelectCommand17;
            this.daClientDetailAccount.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_Account_BillFilter", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail", "ID_Client_Detail"),
                        new System.Data.Common.DataColumnMapping("ID_Derive_Company", "ID_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Filter", "ID_Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("Period", "Period"),
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("Derive_Company", "Derive_Company"),
                        new System.Data.Common.DataColumnMapping("Company_Code", "Company_Code"),
                        new System.Data.Common.DataColumnMapping("Bill_Filter", "Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("Bill_Filter_Info", "Bill_Filter_Info"),
                        new System.Data.Common.DataColumnMapping("Filter_Code", "Filter_Code"),
                        new System.Data.Common.DataColumnMapping("Client", "Client"),
                        new System.Data.Common.DataColumnMapping("Client_Code", "Client_Code"),
                        new System.Data.Common.DataColumnMapping("НОМЕР_УСЛ", "НОМЕР_УСЛ"),
                        new System.Data.Common.DataColumnMapping("НОМЕР_ДОП", "НОМЕР_ДОП"),
                        new System.Data.Common.DataColumnMapping("Client_Short_Name", "Client_Short_Name"),
                        new System.Data.Common.DataColumnMapping("Advance", "Advance"),
                        new System.Data.Common.DataColumnMapping("RoundTo", "RoundTo"),
                        new System.Data.Common.DataColumnMapping("Prefix", "Prefix"),
                        new System.Data.Common.DataColumnMapping("Show_Meter_For_Services", "Show_Meter_For_Services"),
                        new System.Data.Common.DataColumnMapping("Bill_Name", "Bill_Name"),
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail1", "ID_Client_Detail1"),
                        new System.Data.Common.DataColumnMapping("ID_House", "ID_House"),
                        new System.Data.Common.DataColumnMapping("Period1", "Period1"),
                        new System.Data.Common.DataColumnMapping("IsDebt", "IsDebt")})});
            // 
            // sqlSelectCommand15
            // 
            this.sqlSelectCommand15.CommandText = "town.Agreement_Select_All";
            this.sqlSelectCommand15.CommandTimeout = 0;
            this.sqlSelectCommand15.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand15.Connection = this.sqlMainConnection;
            this.sqlSelectCommand15.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@begin_date", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@end_date", System.Data.SqlDbType.DateTime, 8)});
            // 
            // sqlInsertCommand3
            // 
            this.sqlInsertCommand3.CommandText = "town.Agreement_Insert";
            this.sqlInsertCommand3.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand3.Connection = this.sqlMainConnection;
            this.sqlInsertCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Agreement", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "ID_Agreement", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Agreement_Number", System.Data.SqlDbType.VarChar, 100, "Agreement_Number"),
            new System.Data.SqlClient.SqlParameter("@Agreement_Date", System.Data.SqlDbType.DateTime, 8, "Agreement_Date"),
            new System.Data.SqlClient.SqlParameter("@ID_Agreement_Type", System.Data.SqlDbType.Int, 4, "ID_Agreement_Type"),
            new System.Data.SqlClient.SqlParameter("@Description", System.Data.SqlDbType.VarChar, 255, "Description"),
            new System.Data.SqlClient.SqlParameter("@ID_Agent1", System.Data.SqlDbType.Int, 4, "ID_Agent1"),
            new System.Data.SqlClient.SqlParameter("@ID_Agent2", System.Data.SqlDbType.Int, 4, "ID_Agent2"),
            new System.Data.SqlClient.SqlParameter("@ID_Agent3", System.Data.SqlDbType.Int, 4, "ID_Agent3"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 2147483647, "Comment")});
            // 
            // sqlUpdateCommand2
            // 
            this.sqlUpdateCommand2.CommandText = "town.Agreement_Update";
            this.sqlUpdateCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand2.Connection = this.sqlMainConnection;
            this.sqlUpdateCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Agreement", System.Data.SqlDbType.Int, 4, "ID_Agreement"),
            new System.Data.SqlClient.SqlParameter("@Agreement_Number", System.Data.SqlDbType.VarChar, 100, "Agreement_Number"),
            new System.Data.SqlClient.SqlParameter("@Agreement_Date", System.Data.SqlDbType.DateTime, 8, "Agreement_Date"),
            new System.Data.SqlClient.SqlParameter("@ID_Agreement_Type", System.Data.SqlDbType.Int, 4, "ID_Agreement_Type"),
            new System.Data.SqlClient.SqlParameter("@Description", System.Data.SqlDbType.VarChar, 255, "Description"),
            new System.Data.SqlClient.SqlParameter("@ID_Agent1", System.Data.SqlDbType.Int, 4, "ID_Agent1"),
            new System.Data.SqlClient.SqlParameter("@ID_Agent2", System.Data.SqlDbType.Int, 4, "ID_Agent2"),
            new System.Data.SqlClient.SqlParameter("@ID_Agent3", System.Data.SqlDbType.Int, 4, "ID_Agent3"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 2147483647, "Comment")});
            // 
            // sqlDeleteCommand2
            // 
            this.sqlDeleteCommand2.CommandText = "town.Agreement_Delete";
            this.sqlDeleteCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand2.Connection = this.sqlMainConnection;
            this.sqlDeleteCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Agreement", System.Data.SqlDbType.Int, 4, "ID_Agreement")});
            // 
            // daAgreement
            // 
            this.daAgreement.DeleteCommand = this.sqlDeleteCommand2;
            this.daAgreement.InsertCommand = this.sqlInsertCommand3;
            this.daAgreement.SelectCommand = this.sqlSelectCommand15;
            this.daAgreement.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Agreement_Select_All", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Agreement", "ID_Agreement"),
                        new System.Data.Common.DataColumnMapping("Agreement_Number", "Agreement_Number"),
                        new System.Data.Common.DataColumnMapping("Agreement_Date", "Agreement_Date"),
                        new System.Data.Common.DataColumnMapping("ID_Agreement_Type", "ID_Agreement_Type"),
                        new System.Data.Common.DataColumnMapping("Description", "Description"),
                        new System.Data.Common.DataColumnMapping("ID_Agent1", "ID_Agent1"),
                        new System.Data.Common.DataColumnMapping("ID_Agent2", "ID_Agent2"),
                        new System.Data.Common.DataColumnMapping("ID_Agent3", "ID_Agent3"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy"),
                        new System.Data.Common.DataColumnMapping("CreationDate", "CreationDate")})});
            this.daAgreement.UpdateCommand = this.sqlUpdateCommand2;
            // 
            // sqlSelectCommand18
            // 
            this.sqlSelectCommand18.CommandText = "select *\r\nfrom town.agreement_type";
            this.sqlSelectCommand18.CommandTimeout = 0;
            this.sqlSelectCommand18.Connection = this.sqlMainConnection;
            // 
            // daAgreementType
            // 
            this.daAgreementType.SelectCommand = this.sqlSelectCommand18;
            this.daAgreementType.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Agreement_Type", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Agreement_Type", "ID_Agreement_Type"),
                        new System.Data.Common.DataColumnMapping("Agreement_Type", "Agreement_Type")})});
            // 
            // sqlSelectCommand20
            // 
            this.sqlSelectCommand20.CommandText = "Reports.R_Saldo";
            this.sqlSelectCommand20.CommandTimeout = 0;
            this.sqlSelectCommand20.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand20.Connection = this.sqlMainConnection;
            this.sqlSelectCommand20.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Clients", System.Data.SqlDbType.VarChar, 2147483647),
            new System.Data.SqlClient.SqlParameter("@DC", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Kvit", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@house_filter", System.Data.SqlDbType.VarChar, 4000),
            new System.Data.SqlClient.SqlParameter("@isdebt", System.Data.SqlDbType.Bit, 2),
            new System.Data.SqlClient.SqlParameter("@onlyUninhabitedAccounts", System.Data.SqlDbType.Bit, 2)});
            // 
            // daRSaldo
            // 
            this.daRSaldo.SelectCommand = this.sqlSelectCommand20;
            // 
            // sqlSelectCommand19
            // 
            this.sqlSelectCommand19.CommandText = "Reports.ClientDetail_Clients_Select";
            this.sqlSelectCommand19.CommandTimeout = 0;
            this.sqlSelectCommand19.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand19.Connection = this.sqlMainConnection;
            this.sqlSelectCommand19.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@printed", System.Data.SqlDbType.Bit, 2)});
            // 
            // daClients
            // 
            this.daClients.SelectCommand = this.sqlSelectCommand19;
            this.daClients.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_Clients_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_company", "id_company"),
                        new System.Data.Common.DataColumnMapping("name", "name")})});
            // 
            // sqlSelectCommand21
            // 
            this.sqlSelectCommand21.CommandText = "Reports.R_Salso_House_Filter";
            this.sqlSelectCommand21.CommandTimeout = 0;
            this.sqlSelectCommand21.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand21.Connection = this.sqlMainConnection;
            this.sqlSelectCommand21.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Clients", System.Data.SqlDbType.VarChar, 2147483647)});
            // 
            // daRSaldoHouseFilter
            // 
            this.daRSaldoHouseFilter.SelectCommand = this.sqlSelectCommand21;
            this.daRSaldoHouseFilter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "R_Salso_House_Filter", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_house", "id_house"),
                        new System.Data.Common.DataColumnMapping("house_address", "house_address"),
                        new System.Data.Common.DataColumnMapping("id_derive_company", "id_derive_company"),
                        new System.Data.Common.DataColumnMapping("derive_company_name", "derive_company_name"),
                        new System.Data.Common.DataColumnMapping("settlement", "settlement"),
                        new System.Data.Common.DataColumnMapping("house_number", "house_number"),
                        new System.Data.Common.DataColumnMapping("house_block", "house_block"),
                        new System.Data.Common.DataColumnMapping("house_building", "house_building"),
                        new System.Data.Common.DataColumnMapping("house_liter", "house_liter")})});
            // 
            // sqlSelectCommand23
            // 
            this.sqlSelectCommand23.CommandText = "SELECT        ID_Provider_Group, Provider_Group, ID_Company, Code_Group\r\nFROM    " +
    "        Provider_Group";
            this.sqlSelectCommand23.CommandTimeout = 0;
            this.sqlSelectCommand23.Connection = this.sqlMainConnection;
            // 
            // daProviderGroup
            // 
            this.daProviderGroup.SelectCommand = this.sqlSelectCommand23;
            this.daProviderGroup.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Provider_Group", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Provider_Group", "ID_Provider_Group"),
                        new System.Data.Common.DataColumnMapping("Provider_Group", "Provider_Group"),
                        new System.Data.Common.DataColumnMapping("ID_Company", "ID_Company"),
                        new System.Data.Common.DataColumnMapping("Code_Group", "Code_Group")})});
            // 
            // cmdBillFilterProviderUpdate
            // 
            this.cmdBillFilterProviderUpdate.CommandText = "Reports.ClientDetail_BillFilterProvider_Update";
            this.cmdBillFilterProviderUpdate.CommandTimeout = 0;
            this.cmdBillFilterProviderUpdate.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdBillFilterProviderUpdate.Connection = this.sqlMainConnection;
            this.cmdBillFilterProviderUpdate.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Provider_Group_Filter", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Service", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Condition", System.Data.SqlDbType.VarChar, -1)});
            // 
            // sqlSelectCommand24
            // 
            this.sqlSelectCommand24.CommandText = "select *\r\nfrom dbo.Provider_Group_Filter";
            this.sqlSelectCommand24.CommandTimeout = 0;
            this.sqlSelectCommand24.Connection = this.sqlMainConnection;
            // 
            // daProviderGroupFilter
            // 
            this.daProviderGroupFilter.SelectCommand = this.sqlSelectCommand24;
            this.daProviderGroupFilter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Provider_Group_Filter", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Provider_Group_Filter", "ID_Provider_Group_Filter"),
                        new System.Data.Common.DataColumnMapping("Provider_Group_Filter", "Provider_Group_Filter"),
                        new System.Data.Common.DataColumnMapping("Condition", "Condition"),
                        new System.Data.Common.DataColumnMapping("Info", "Info")})});
            // 
            // sqlSelectCommand22
            // 
            this.sqlSelectCommand22.CommandText = "Reports.ClientDetail_BillFilterDetail_Select";
            this.sqlSelectCommand22.CommandTimeout = 0;
            this.sqlSelectCommand22.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand22.Connection = this.sqlMainConnection;
            this.sqlSelectCommand22.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4)});
            // 
            // sqlUpdateCommand3
            // 
            this.sqlUpdateCommand3.CommandText = "Reports.ClientDetail_BillFilterDetail_Update";
            this.sqlUpdateCommand3.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand3.Connection = this.sqlMainConnection;
            this.sqlUpdateCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4, "ID_Bill_Filter"),
            new System.Data.SqlClient.SqlParameter("@ID_Service", System.Data.SqlDbType.Int, 4, "ID_Service"),
            new System.Data.SqlClient.SqlParameter("@ID_Provider_Group_Filter", System.Data.SqlDbType.Int, 4, "ID_Provider_Group_Filter")});
            // 
            // daBillFilterDetailProviderFilter
            // 
            this.daBillFilterDetailProviderFilter.SelectCommand = this.sqlSelectCommand22;
            this.daBillFilterDetailProviderFilter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_BillFilterDetail_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Bill_Filter", "ID_Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("ID_Service", "ID_Service"),
                        new System.Data.Common.DataColumnMapping("ID_Provider_Group_Filter", "ID_Provider_Group_Filter"),
                        new System.Data.Common.DataColumnMapping("ServiceName", "ServiceName")})});
            this.daBillFilterDetailProviderFilter.UpdateCommand = this.sqlUpdateCommand3;
            // 
            // sqlSelectCommand25
            // 
            this.sqlSelectCommand25.CommandText = "Reports.Client_Detail_Delivery_Settings_Select";
            this.sqlSelectCommand25.CommandTimeout = 0;
            this.sqlSelectCommand25.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand25.Connection = this.sqlMainConnection;
            this.sqlSelectCommand25.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Today", System.Data.SqlDbType.Bit, 2)});
            // 
            // sqlInsertCommand4
            // 
            this.sqlInsertCommand4.CommandText = "Reports.Client_Detail_Delivery_Settings_Insert";
            this.sqlInsertCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand4.Connection = this.sqlMainConnection;
            this.sqlInsertCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Delivery", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "ID_Delivery", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Client", System.Data.SqlDbType.Int, 4, "ID_Client"),
            new System.Data.SqlClient.SqlParameter("@Email_IDs", System.Data.SqlDbType.VarChar, 250, "Email_IDs"),
            new System.Data.SqlClient.SqlParameter("@Days", System.Data.SqlDbType.VarChar, 100, "Days"),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4, "ID_Bill_Filter"),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4, "CreatedBy"),
            new System.Data.SqlClient.SqlParameter("@Is_Debt", System.Data.SqlDbType.Bit, 2, "Is_Debt")});
            // 
            // sqlUpdateCommand4
            // 
            this.sqlUpdateCommand4.CommandText = "Reports.Client_Detail_Delivery_Settings_Update";
            this.sqlUpdateCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand4.Connection = this.sqlMainConnection;
            this.sqlUpdateCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Delivery", System.Data.SqlDbType.Int, 4, "ID_Delivery"),
            new System.Data.SqlClient.SqlParameter("@ID_Client", System.Data.SqlDbType.Int, 4, "ID_Client"),
            new System.Data.SqlClient.SqlParameter("@Email_IDs", System.Data.SqlDbType.VarChar, 250, "Email_IDs"),
            new System.Data.SqlClient.SqlParameter("@Days", System.Data.SqlDbType.VarChar, 100, "Days"),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4, "ID_Bill_Filter"),
            new System.Data.SqlClient.SqlParameter("@Last_Send", System.Data.SqlDbType.DateTime, 8, "Last_Send"),
            new System.Data.SqlClient.SqlParameter("@Last_Send_By", System.Data.SqlDbType.Int, 4, "Last_Send_By"),
            new System.Data.SqlClient.SqlParameter("@SendToday", System.Data.SqlDbType.Bit, 2, "SendToday"),
            new System.Data.SqlClient.SqlParameter("@Is_Debt", System.Data.SqlDbType.Bit, 2, "Is_Debt")});
            // 
            // sqlDeleteCommand3
            // 
            this.sqlDeleteCommand3.CommandText = "Reports.Client_Detail_Delivery_Settings_Delete";
            this.sqlDeleteCommand3.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand3.Connection = this.sqlMainConnection;
            this.sqlDeleteCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Delivery", System.Data.SqlDbType.Int, 4, "ID_Delivery")});
            // 
            // daClientDetailDeliverySettings
            // 
            this.daClientDetailDeliverySettings.DeleteCommand = this.sqlDeleteCommand3;
            this.daClientDetailDeliverySettings.InsertCommand = this.sqlInsertCommand4;
            this.daClientDetailDeliverySettings.SelectCommand = this.sqlSelectCommand25;
            this.daClientDetailDeliverySettings.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Client_Detail_Delivery_Settings_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("Email_IDs", "Email_IDs"),
                        new System.Data.Common.DataColumnMapping("Days", "Days"),
                        new System.Data.Common.DataColumnMapping("Last_Send", "Last_Send"),
                        new System.Data.Common.DataColumnMapping("Last_Send_By", "Last_Send_By"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy"),
                        new System.Data.Common.DataColumnMapping("CreationDate", "CreationDate"),
                        new System.Data.Common.DataColumnMapping("ID_Delivery", "ID_Delivery")})});
            this.daClientDetailDeliverySettings.UpdateCommand = this.sqlUpdateCommand4;
            // 
            // sqlSelectCommand26
            // 
            this.sqlSelectCommand26.CommandText = "Reports.Client_Detail_Delivery_Email_Select";
            this.sqlSelectCommand26.CommandTimeout = 0;
            this.sqlSelectCommand26.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand26.Connection = this.sqlMainConnection;
            this.sqlSelectCommand26.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // sqlInsertCommand5
            // 
            this.sqlInsertCommand5.CommandText = "Reports.Client_Detail_Delivery_Email_Insert";
            this.sqlInsertCommand5.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand5.Connection = this.sqlMainConnection;
            this.sqlInsertCommand5.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Email", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "ID_Email", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 150, "Name"),
            new System.Data.SqlClient.SqlParameter("@Emails", System.Data.SqlDbType.VarChar, 250, "Emails"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 400, "Comment")});
            // 
            // sqlUpdateCommand5
            // 
            this.sqlUpdateCommand5.CommandText = "Reports.Client_Detail_Delivery_Email_Update";
            this.sqlUpdateCommand5.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand5.Connection = this.sqlMainConnection;
            this.sqlUpdateCommand5.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Email", System.Data.SqlDbType.Int, 4, "ID_Email"),
            new System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 150, "Name"),
            new System.Data.SqlClient.SqlParameter("@Emails", System.Data.SqlDbType.VarChar, 250, "Emails"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 400, "Comment")});
            // 
            // sqlDeleteCommand4
            // 
            this.sqlDeleteCommand4.CommandText = "Reports.Client_Detail_Delivery_Email_Delete";
            this.sqlDeleteCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand4.Connection = this.sqlMainConnection;
            this.sqlDeleteCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Email", System.Data.SqlDbType.Int, 4, "ID_Email")});
            // 
            // daClientDetailDeliveryEmail
            // 
            this.daClientDetailDeliveryEmail.DeleteCommand = this.sqlDeleteCommand4;
            this.daClientDetailDeliveryEmail.InsertCommand = this.sqlInsertCommand5;
            this.daClientDetailDeliveryEmail.SelectCommand = this.sqlSelectCommand26;
            this.daClientDetailDeliveryEmail.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Client_Detail_Delivery_Email_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Email", "ID_Email"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Emails", "Emails"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment")})});
            this.daClientDetailDeliveryEmail.UpdateCommand = this.sqlUpdateCommand5;
            // 
            // cmdClientDetailDeliveryLastSend
            // 
            this.cmdClientDetailDeliveryLastSend.CommandText = "Reports.Client_Detail_Delivery_Settings_LastSend";
            this.cmdClientDetailDeliveryLastSend.CommandTimeout = 0;
            this.cmdClientDetailDeliveryLastSend.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdClientDetailDeliveryLastSend.Connection = this.sqlMainConnection;
            this.cmdClientDetailDeliveryLastSend.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Delivery", System.Data.SqlDbType.Int, 4, "ID_Delivery"),
            new System.Data.SqlClient.SqlParameter("@Last_Send", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@Last_Send_By", System.Data.SqlDbType.Int, 4)});
            // 
            // sqlSelectCommand29
            // 
            this.sqlSelectCommand29.CommandText = "Reports.Debt_Bill_Client_Detail_Select";
            this.sqlSelectCommand29.CommandTimeout = 0;
            this.sqlSelectCommand29.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand29.Connection = this.sqlMainConnection;
            this.sqlSelectCommand29.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@id_derive_company", System.Data.SqlDbType.Int, 4)});
            // 
            // daDebtBillClientDetail
            // 
            this.daDebtBillClientDetail.SelectCommand = this.sqlSelectCommand29;
            this.daDebtBillClientDetail.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Debt_Bill_Client_Detail", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_bill_filter", "id_bill_filter"),
                        new System.Data.Common.DataColumnMapping("client_name", "client_name"),
                        new System.Data.Common.DataColumnMapping("bill_filter", "bill_filter"),
                        new System.Data.Common.DataColumnMapping("begin_period", "begin_period"),
                        new System.Data.Common.DataColumnMapping("end_period", "end_period"),
                        new System.Data.Common.DataColumnMapping("begin_period_name", "begin_period_name"),
                        new System.Data.Common.DataColumnMapping("end_period_name", "end_period_name"),
                        new System.Data.Common.DataColumnMapping("id_client", "id_client")})});
            // 
            // sqlSelectCommand27
            // 
            this.sqlSelectCommand27.CommandText = "Reports.Debt_Bill_Saldo_Data_Select";
            this.sqlSelectCommand27.CommandTimeout = 0;
            this.sqlSelectCommand27.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand27.Connection = this.sqlMainConnection;
            this.sqlSelectCommand27.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@id_account", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@id_derive_company", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@id_bill_filter", System.Data.SqlDbType.Int, 4)});
            // 
            // daDebtBillSaldo
            // 
            this.daDebtBillSaldo.SelectCommand = this.sqlSelectCommand27;
            this.daDebtBillSaldo.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Debt_Bill_Data_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("checked", "checked"),
                        new System.Data.Common.DataColumnMapping("period", "period"),
                        new System.Data.Common.DataColumnMapping("id_account", "id_account"),
                        new System.Data.Common.DataColumnMapping("id_service", "id_service"),
                        new System.Data.Common.DataColumnMapping("id_provider", "id_provider"),
                        new System.Data.Common.DataColumnMapping("balance", "balance"),
                        new System.Data.Common.DataColumnMapping("sum_tariff", "sum_tariff"),
                        new System.Data.Common.DataColumnMapping("sum_benefit", "sum_benefit"),
                        new System.Data.Common.DataColumnMapping("ServiceName", "ServiceName"),
                        new System.Data.Common.DataColumnMapping("id_provider_group", "id_provider_group"),
                        new System.Data.Common.DataColumnMapping("provider_group", "provider_group"),
                        new System.Data.Common.DataColumnMapping("id_derive_company", "id_derive_company"),
                        new System.Data.Common.DataColumnMapping("derive_company", "derive_company")}),
            new System.Data.Common.DataTableMapping("Table1", "Table1", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("checked", "checked"),
                        new System.Data.Common.DataColumnMapping("period", "period"),
                        new System.Data.Common.DataColumnMapping("id_account", "id_account"),
                        new System.Data.Common.DataColumnMapping("id_service", "id_service"),
                        new System.Data.Common.DataColumnMapping("id_provider", "id_provider"),
                        new System.Data.Common.DataColumnMapping("balance", "balance"),
                        new System.Data.Common.DataColumnMapping("sum_tariff", "sum_tariff"),
                        new System.Data.Common.DataColumnMapping("sum_benefit", "sum_benefit"),
                        new System.Data.Common.DataColumnMapping("ServiceName", "ServiceName"),
                        new System.Data.Common.DataColumnMapping("id_provider_group", "id_provider_group"),
                        new System.Data.Common.DataColumnMapping("provider_group", "provider_group"),
                        new System.Data.Common.DataColumnMapping("id_derive_company", "id_derive_company"),
                        new System.Data.Common.DataColumnMapping("derive_company", "derive_company")})});
            // 
            // cmdCreateClientDetail
            // 
            this.cmdCreateClientDetail.CommandText = "[Reports].[Debt_Bill_Client_Detail_Create]";
            this.cmdCreateClientDetail.CommandTimeout = 0;
            this.cmdCreateClientDetail.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdCreateClientDetail.Connection = this.sqlMainConnection;
            this.cmdCreateClientDetail.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Client", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Bill_Filter", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Begin_Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@End_Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Advance", System.Data.SqlDbType.Float, 4),
            new System.Data.SqlClient.SqlParameter("@RoundTo", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Prefix", System.Data.SqlDbType.VarChar, 30),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 4000),
            new System.Data.SqlClient.SqlParameter("@Bill_Name", System.Data.SqlDbType.VarChar, 100),
            new System.Data.SqlClient.SqlParameter("@Show_Meter_For_Services", System.Data.SqlDbType.VarChar, 50),
            new System.Data.SqlClient.SqlParameter("@ID_Filter_Type", System.Data.SqlDbType.Int, 4)});
            // 
            // sqlSelectCommand28
            // 
            this.sqlSelectCommand28.CommandText = "dbo.Claim_Derive_Company";
            this.sqlSelectCommand28.CommandTimeout = 0;
            this.sqlSelectCommand28.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand28.Connection = this.sqlMainConnection;
            this.sqlSelectCommand28.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Account", System.Data.SqlDbType.Int, 4)});
            // 
            // daClaimDeriveCompany
            // 
            this.daClaimDeriveCompany.SelectCommand = this.sqlSelectCommand28;
            this.daClaimDeriveCompany.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Claim_Derive_Company", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Company", "ID_Company"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Is_Provider", "Is_Provider"),
                        new System.Data.Common.DataColumnMapping("Is_Derive_Company", "Is_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("Is_Pay_Receiver", "Is_Pay_Receiver"),
                        new System.Data.Common.DataColumnMapping("Code", "Code"),
                        new System.Data.Common.DataColumnMapping("Meter_Future_Period_Day", "Meter_Future_Period_Day"),
                        new System.Data.Common.DataColumnMapping("Load_Measure_In_Future", "Load_Measure_In_Future")})});
            // 
            // sqlSelectCommand30
            // 
            this.sqlSelectCommand30.CommandText = "dbo.Claim_Provider_Group";
            this.sqlSelectCommand30.CommandTimeout = 0;
            this.sqlSelectCommand30.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand30.Connection = this.sqlMainConnection;
            this.sqlSelectCommand30.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Account", System.Data.SqlDbType.Int, 4)});
            // 
            // daClaimProviderGroup
            // 
            this.daClaimProviderGroup.SelectCommand = this.sqlSelectCommand30;
            this.daClaimProviderGroup.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Claim_Provider_Group", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Provider_Group", "ID_Provider_Group"),
                        new System.Data.Common.DataColumnMapping("Provider_Group", "Provider_Group")})});
            // 
            // sqlSelectCommand31
            // 
            this.sqlSelectCommand31.CommandText = "dbo.Can_Claim_Add";
            this.sqlSelectCommand31.CommandTimeout = 0;
            this.sqlSelectCommand31.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand31.Connection = this.sqlMainConnection;
            this.sqlSelectCommand31.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Provider_Group", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Account", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Answer", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // daClaimCanAdd
            // 
            this.daClaimCanAdd.SelectCommand = this.sqlSelectCommand31;
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.ConnectionString = "Data Source=srv-mrivc7;Initial Catalog=Murmansk;User ID=bobrovsky;Password=speake" +
    "r";
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
            // 
            // cmdCanAddClaim
            // 
            this.cmdCanAddClaim.CommandText = "Can_Claim_Add";
            this.cmdCanAddClaim.CommandTimeout = 0;
            this.cmdCanAddClaim.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdCanAddClaim.Connection = this.sqlMainConnection;
            this.cmdCanAddClaim.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Provider_Group", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Account", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Answer", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdClaimAddEdit
            // 
            this.cmdClaimAddEdit.CommandText = "Claim_Add_Edit";
            this.cmdClaimAddEdit.CommandTimeout = 0;
            this.cmdClaimAddEdit.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdClaimAddEdit.Connection = this.sqlMainConnection;
            this.cmdClaimAddEdit.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Account", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Provider_Group", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Sum", System.Data.SqlDbType.Float, 4),
            new System.Data.SqlClient.SqlParameter("@Sum_Fine", System.Data.SqlDbType.Float, 4),
            new System.Data.SqlClient.SqlParameter("@Date_Plan_Repayment", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@ID_Paper_Status", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Note", System.Data.SqlDbType.NVarChar, 4000),
            new System.Data.SqlClient.SqlParameter("@Date_Claim", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@Date_Repayment", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@Anul_Reason", System.Data.SqlDbType.NVarChar, 4000),
            new System.Data.SqlClient.SqlParameter("@BalanceTable", System.Data.SqlDbType.NVarChar, 250),
            new System.Data.SqlClient.SqlParameter("@Debt_Period_Start", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Include_Work_Sum_Tariff", System.Data.SqlDbType.Bit, 2)});
            // 
            // sqlSelectCommand32
            // 
            this.sqlSelectCommand32.CommandText = "Reports.ClientDetail_ServiceNumber_Select";
            this.sqlSelectCommand32.CommandTimeout = 0;
            this.sqlSelectCommand32.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand32.Connection = this.sqlMainConnection;
            this.sqlSelectCommand32.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Company", System.Data.SqlDbType.Int, 4)});
            // 
            // daCompanyServiceNumber
            // 
            this.daCompanyServiceNumber.SelectCommand = this.sqlSelectCommand32;
            this.daCompanyServiceNumber.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ClientDetail_ServiceNumber_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Company", "ID_Company"),
                        new System.Data.Common.DataColumnMapping("ServiceNumber", "ServiceNumber")})});
            // 
            // sqlSelectCommand33
            // 
            this.sqlSelectCommand33.CommandText = "Reports.Print_Delivery_Bill_Account_Select";
            this.sqlSelectCommand33.CommandTimeout = 0;
            this.sqlSelectCommand33.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand33.Connection = this.sqlMainConnection;
            this.sqlSelectCommand33.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Account", System.Data.SqlDbType.Int, 4)});
            // 
            // daPrintBillDeliveryAccount
            // 
            this.daPrintBillDeliveryAccount.SelectCommand = this.sqlSelectCommand33;
            this.daPrintBillDeliveryAccount.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Print_Delivery_Bill_Account_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Print", "ID_Print"),
                        new System.Data.Common.DataColumnMapping("Period", "Period"),
                        new System.Data.Common.DataColumnMapping("ID_Account", "ID_Account"),
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail", "ID_Client_Detail"),
                        new System.Data.Common.DataColumnMapping("Email", "Email"),
                        new System.Data.Common.DataColumnMapping("Send_Date", "Send_Date"),
                        new System.Data.Common.DataColumnMapping("File_Name", "File_Name"),
                        new System.Data.Common.DataColumnMapping("Send_Success", "Send_Success"),
                        new System.Data.Common.DataColumnMapping("Error", "Error"),
                        new System.Data.Common.DataColumnMapping("bill_filter", "bill_filter")})});
            // 
            // cmdCheckBindAddress
            // 
            this.cmdCheckBindAddress.CommandText = "[town].[CheckBindAddress]";
            this.cmdCheckBindAddress.CommandTimeout = 0;
            this.cmdCheckBindAddress.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdCheckBindAddress.Connection = this.sqlMainConnection;
            this.cmdCheckBindAddress.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@account", System.Data.SqlDbType.NVarChar, 20),
            new System.Data.SqlClient.SqlParameter("@street_separator", System.Data.SqlDbType.NVarChar, 20),
            new System.Data.SqlClient.SqlParameter("@house_separator", System.Data.SqlDbType.NVarChar, 20),
            new System.Data.SqlClient.SqlParameter("@building_separator", System.Data.SqlDbType.NVarChar, 20),
            new System.Data.SqlClient.SqlParameter("@block_separator", System.Data.SqlDbType.NVarChar, 20),
            new System.Data.SqlClient.SqlParameter("@flat_separator", System.Data.SqlDbType.NVarChar, 20),
            new System.Data.SqlClient.SqlParameter("@room_separator", System.Data.SqlDbType.NVarChar, 20)});
            // 
            // cmdLawsuitAddEdit
            // 
            this.cmdLawsuitAddEdit.CommandText = "Lawsuit_Add_Edit";
            this.cmdLawsuitAddEdit.CommandTimeout = 0;
            this.cmdLawsuitAddEdit.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdLawsuitAddEdit.Connection = this.sqlMainConnection;
            this.cmdLawsuitAddEdit.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Account", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Derive_Company", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Provider_Group", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Sum", System.Data.SqlDbType.Float, 4),
            new System.Data.SqlClient.SqlParameter("@Sum_Fine", System.Data.SqlDbType.Float, 4),
            new System.Data.SqlClient.SqlParameter("@Date_Plan_Repayment", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@ID_Paper_Status", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Note", System.Data.SqlDbType.NVarChar, 4000),
            new System.Data.SqlClient.SqlParameter("@Date_Claim", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@Date_Repayment", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@Anul_Reason", System.Data.SqlDbType.NVarChar, 4000),
            new System.Data.SqlClient.SqlParameter("@BalanceTable", System.Data.SqlDbType.NVarChar, 250),
            new System.Data.SqlClient.SqlParameter("@Debt_Period_Start", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Include_Work_Sum_Tariff", System.Data.SqlDbType.Bit, 2)});
            // 
            // daRSaldoSber
            // 
            this.daRSaldoSber.SelectCommand = this.sqlCommand4;
            // 
            // sqlCommand4
            // 
            this.sqlCommand4.CommandText = "Reports.R_Saldo_Federal_SBer_standart";
            this.sqlCommand4.CommandTimeout = 0;
            this.sqlCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlCommand4.Connection = this.sqlMainConnection;
            this.sqlCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Clients", System.Data.SqlDbType.VarChar, 2147483647),
            new System.Data.SqlClient.SqlParameter("@DC", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Kvit", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@house_filter", System.Data.SqlDbType.VarChar, 4000),
            new System.Data.SqlClient.SqlParameter("@isdebt", System.Data.SqlDbType.Bit, 2),
            new System.Data.SqlClient.SqlParameter("@onlyUninhabitedAccounts", System.Data.SqlDbType.Bit, 2)});
            // 
            // daRSaldoSberMeasure
            // 
            this.daRSaldoSberMeasure.SelectCommand = this.sqlCommand5;
            // 
            // sqlCommand5
            // 
            this.sqlCommand5.CommandText = "Reports.R_Saldo_Federal_SBer_standart_measure";
            this.sqlCommand5.CommandTimeout = 0;
            this.sqlCommand5.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlCommand5.Connection = this.sqlMainConnection;
            this.sqlCommand5.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Clients", System.Data.SqlDbType.VarChar, 2147483647),
            new System.Data.SqlClient.SqlParameter("@DC", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Kvit", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@house_filter", System.Data.SqlDbType.VarChar, 4000),
            new System.Data.SqlClient.SqlParameter("@isdebt", System.Data.SqlDbType.Bit, 2),
            new System.Data.SqlClient.SqlParameter("@onlyUninhabitedAccounts", System.Data.SqlDbType.Bit, 2)});

        }

        #endregion

        private System.Data.SqlClient.SqlConnection sqlMainConnection;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand2;
        private System.Data.SqlClient.SqlDataAdapter daDeriveCompany;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand3;
        private System.Data.SqlClient.SqlDataAdapter daPayReceiver;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand4;
        private System.Data.SqlClient.SqlDataAdapter daPeriod;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand5;
        private System.Data.SqlClient.SqlDataAdapter daBillFilter;
        private System.Data.SqlClient.SqlCommand cmdAnyCommand;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand7;
        private System.Data.SqlClient.SqlDataAdapter daClientDetailHouse;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand6;
        private System.Data.SqlClient.SqlDataAdapter daServiceList;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand8;
        private System.Data.SqlClient.SqlDataAdapter daFilterType;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand1;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand1;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand1;
        private System.Data.SqlClient.SqlDataAdapter daClientDetail;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand9;
        private System.Data.SqlClient.SqlDataAdapter daHouseHierarhy;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand10;
        private System.Data.SqlClient.SqlDataAdapter daFilterHouse;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand11;
        private System.Data.SqlClient.SqlDataAdapter daHouseWithoutBillFilter;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand12;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand2;
        private System.Data.SqlClient.SqlDataAdapter daClientDetailCopy;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand13;
        private System.Data.SqlClient.SqlDataAdapter daBillKind;
        private System.Data.SqlClient.SqlCommand cmdBillPrepareReportData;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand14;
        private System.Data.SqlClient.SqlDataAdapter daBillHouseList;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand16;
        private System.Data.SqlClient.SqlDataAdapter daClientDetailAgreement;
        private System.Data.SqlClient.SqlCommand sqlCommand3;
        private System.Data.SqlClient.SqlCommand sqlCommand1;
        private System.Data.SqlClient.SqlCommand sqlCommand2;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand17;
        private System.Data.SqlClient.SqlDataAdapter daClientDetailAccount;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand15;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand3;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand2;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand2;
        private System.Data.SqlClient.SqlDataAdapter daAgreement;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand18;
        private System.Data.SqlClient.SqlDataAdapter daAgreementType;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand20;
        private System.Data.SqlClient.SqlDataAdapter daRSaldo;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand19;
        private System.Data.SqlClient.SqlDataAdapter daClients;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand21;
        private System.Data.SqlClient.SqlDataAdapter daRSaldoHouseFilter;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand23;
        private System.Data.SqlClient.SqlDataAdapter daProviderGroup;
        private System.Data.SqlClient.SqlCommand cmdBillFilterProviderUpdate;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand24;
        private System.Data.SqlClient.SqlDataAdapter daProviderGroupFilter;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand22;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand3;
        private System.Data.SqlClient.SqlDataAdapter daBillFilterDetailProviderFilter;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand25;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand4;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand4;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand3;
        private System.Data.SqlClient.SqlDataAdapter daClientDetailDeliverySettings;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand26;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand5;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand5;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand4;
        private System.Data.SqlClient.SqlDataAdapter daClientDetailDeliveryEmail;
        private System.Data.SqlClient.SqlCommand cmdClientDetailDeliveryLastSend;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand29;
        private System.Data.SqlClient.SqlDataAdapter daDebtBillClientDetail;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand27;
        private System.Data.SqlClient.SqlDataAdapter daDebtBillSaldo;
        private System.Data.SqlClient.SqlCommand cmdCreateClientDetail;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand28;
        private System.Data.SqlClient.SqlDataAdapter daClaimDeriveCompany;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand30;
        private System.Data.SqlClient.SqlDataAdapter daClaimProviderGroup;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand31;
        private System.Data.SqlClient.SqlDataAdapter daClaimCanAdd;
        private System.Data.SqlClient.SqlConnection sqlConnection1;
        private System.Data.SqlClient.SqlCommand cmdCanAddClaim;
        private System.Data.SqlClient.SqlCommand cmdClaimAddEdit;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand32;
        private System.Data.SqlClient.SqlDataAdapter daCompanyServiceNumber;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand33;
        private System.Data.SqlClient.SqlDataAdapter daPrintBillDeliveryAccount;
        private System.Data.SqlClient.SqlCommand cmdCheckBindAddress;
        private System.Data.SqlClient.SqlCommand cmdLawsuitAddEdit;
        private System.Data.SqlClient.SqlDataAdapter daRSaldoSber;
        private System.Data.SqlClient.SqlCommand sqlCommand4;
        private System.Data.SqlClient.SqlDataAdapter daRSaldoSberMeasure;
        private System.Data.SqlClient.SqlCommand sqlCommand5;
    }
}
