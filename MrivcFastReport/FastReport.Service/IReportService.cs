﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Collections;
using System.Net;

namespace FastReport.Service
{
    [ServiceContract]
    public interface IFastReportService
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "/about/")]
        string About();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/{path}")]
        List<ReportItem> GetReportsListByPath(string path);
        
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/")]
        List<ReportItem> GetReportsList();
        
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "/gears/")]
        List<GearItem> GetGearList();
        
        [OperationContract]
        [WebInvoke(Method = "POST", 
            RequestFormat = WebMessageFormat.Json, 
            ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "/getreport/")]
        Stream GetReport(ReportRequest request);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/gettest/")]
        Stream GetTest(Stream preparedReport);
        
        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/checkprepared/{uuid}")]
        bool CheckPreparedReport(string uuid);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/putprepared/")]
        string PutPreparedReport(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getprepared/{uuid}")]
        Stream GetPreparedByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,            
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getlogo/{uuid}/{width}/{height}")]
        Stream GetLogoByUUID(string uuid, string width, string height);

        [OperationContract]
        [WebInvoke(Method = "GET",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getlogo/{uuid}")]
        Stream GetLogoByUUIDFixed(string uuid);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getfpx/")]
        Stream GetFPX(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getpdf/")]
        Stream GetPDF(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getxlsx/")]
        Stream GetXLSX(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getdocx/")]
        Stream GetDOCX(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getpptx/")]
        Stream GetPPTX(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getods/")]
        Stream GetODS(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getodt/")]
        Stream GetODT(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getmht/")]
        Stream GetMHT(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getcsv/")]
        Stream GetCSV(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getdbf/")]
        Stream GetDBF(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getxml/")]
        Stream GetXML(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/gettxt/")]
        Stream GetTXT(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/getrtf/")]
        Stream GetRTF(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/gethtml/")]
        Stream GetHTML(Stream preparedReport);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getfpx/{uuid}")]
        Stream GetFPXByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getpdf/{uuid}")]
        Stream GetPDFByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getxlsx/{uuid}")]
        Stream GetXLSXByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getdocx/{uuid}")]
        Stream GetDOCXByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getpptx/{uuid}")]
        Stream GetPPTXByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getods/{uuid}")]
        Stream GetODSByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getodt/{uuid}")]
        Stream GetODTByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getmht/{uuid}")]
        Stream GetMHTByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getcsv/{uuid}")]
        Stream GetCSVByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getdbf/{uuid}")]
        Stream GetDBFByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getxml/{uuid}")]
        Stream GetXMLByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/gettxt/{uuid}")]
        Stream GetTXTByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getrtf/{uuid}")]
        Stream GetRTFByUUID(string uuid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/gethtml/{uuid}")]
        Stream GetHTMLByUUID(string uuid);
    }

    [DataContract]
    public class ReportRequest
    {
        [DataMember]
        public ReportItem Report { get; set; }
        [DataMember]
        public GearItem Gear { get; set; }
    }

    [DataContract]
    public class ReportItem
    {
        private Dictionary<string, string> parameters = new Dictionary<string, string>();

        [DataMember]
        public string Path { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public Dictionary<string, string> Parameters 
        {
            get
            {
                return parameters;
            }
            set
            {
                parameters = value;
            }
        }
    }

    [DataContract]
    public class GearItem
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Dictionary<string, string> Properties { get; set; }
        
        [DataMember]
        public string Extension { get; set; }

        [DataMember]
        public string MimeType { get; set; }
    }

    [DataContract]
    public class ErrorHandler
    {
        [DataMember]
        public int ErrorCode { get; set; }
        [DataMember]
        public string Cause { get; set; }

    }
}
