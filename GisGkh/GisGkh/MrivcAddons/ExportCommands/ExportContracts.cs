﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportContracts : MrivcExportCommand, ISingleHouseCommand
    {
        bool _isRSO;

        public string FiasHouseGuid { get; set; } = null;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                base.SenderID = value;
            }
        }

        public ExportContracts(string senderID) : base("exportCAChAsync", "HomeManagementAsync", senderID, "exportCAChResult")
        {
            Description = "Экспорт договоров управления";
        }

        new void ExecuteToTempDB()
        {
            base.ExecuteToTempDB(new[] { new Tuple<string[], string>(new[] { "UOGUID" }, SenderID), new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) }, _exportNodeName);
        }

        public new void Execute()
        {
            this.ExecuteToTempDB();
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportContractData", new object[,] { { "senderID", SenderID }, { "houseGUID", FiasHouseGuid } });
        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            //return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.ExportHouseData", new object[,] { { "houseGUID", FiasHouseGuid } }, "Houses", "Entrance", "Premises");
            return new DataSet();
        }
    }
}
