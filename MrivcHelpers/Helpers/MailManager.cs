﻿namespace MrivcHelpers.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Mail;

    using MrivcHelpers.Properties;

    public class MailManager
    {
        public static string SendMail(string smtpServer, int smtpServerPort, bool useSSL, string from, string password, string mailto, string mailcopyto, string caption, string message, List<string> attachFiles = null)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);

                mailto = mailto.Replace(" ", "");
                mailcopyto = mailcopyto.Replace(" ", "");

                string[] mailsTo = mailto.Split(new char[] {';', ','});
                string[] mailsCopyTo = mailcopyto.Split(new char[] { ';', ',' });

                foreach (var s in mailsTo)
                    if (s.Length > 0)
                        mail.To.Add(new MailAddress(s.Trim()));

                foreach (var s in mailsCopyTo)
                    if (s.Length > 0)
                        mail.CC.Add(new MailAddress(s.Trim()));

                mail.Subject = caption;
                mail.Body = message;

                if (attachFiles != null && attachFiles.Count > 0)
                {
                    foreach (string file in attachFiles)
                    {
                        mail.Attachments.Add(new Attachment(file));
                    }
                }

                var smtpClient = new SmtpClient(smtpServer, smtpServerPort);
                smtpClient.Credentials = new NetworkCredential(from, password);
                smtpClient.EnableSsl = useSSL;
                smtpClient.Send(mail);

                mail.Dispose();
                return string.Empty;
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog("Mail.Send: " + ex.Message + " mailto:" + mailto);
                return ex.Message;
            }
        }
    }
}
