﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PaymentStatementParser
{
    public partial class MainForm : Form
    {
        private OpenFileDialog _openFileDialog;
        private string _file;
        private string _providerName;
        private Provider _provider;

        public MainForm()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            _file = string.Empty;
            _providerName = string.Empty;
            _provider = null;

            _openFileDialog = new OpenFileDialog();
            _openFileDialog.Multiselect = true;
            _openFileDialog.Filter = "Excel file (*.xlsx;*.xls)|*.xlsx;*.xls";

            providersComboBox.SelectedIndex = 0;

            referenceButton.Enabled = false;
        }

        private string GetAccount(string str)
        {
            Match match = null;

            foreach (Regex reg in Utils.REGEXS)
            {
                match = reg.Match(str);

                if (match.Length > 0) break;
            }

            int outTemp = 0;

            StringBuilder account = new StringBuilder(match.Value);

            if (match.Length == 0)
            {
                foreach (char ch in str)
                {
                    if (!int.TryParse(ch.ToString(), out outTemp)) break;

                    account.Append(ch);
                }
            }

            StringBuilder clearedAccount = new StringBuilder();

            for (int i = 0; i < account.Length; i++)
            {
                string ch = account[i].ToString();

                if (int.TryParse(ch, out outTemp))
                {
                    clearedAccount.Append(ch);
                }
            }

            return clearedAccount.ToString();
        }

        private string ShorteningAccount(string account, int size)
        {
            StringBuilder accountCopy = new StringBuilder(account);

            while (accountCopy.Length > size) accountCopy.Remove(0, 1);

            return accountCopy.ToString();
        }

        private DataTable CreateResultTable(string tName)
        {
            DataTable results = new DataTable(tName);

            results.Columns.Add(Utils.ResultTableColumn.ACCOUNT);
            results.Columns.Add(Utils.ResultTableColumn.SUMPLAT, typeof(double));
            results.Columns.Add(Utils.ResultTableColumn.DATPLAT);

            return results;
        }

        /*
         * TODO:
         * сделать для МРИВЦ загрузку по компании + код фильтра и по коду фильтра
         * в две разные папки
         */
        private Dictionary<string, DataSet> DataDistribution(DataTable source, DataTable errors)
        {
            Dictionary<string, DataSet> dataSetsDictionary = new Dictionary<string, DataSet>();

            DataTable table;
            DataSet container;

            string idProvider = string.Empty;
            string code = string.Empty;
            string account = string.Empty;

            foreach (DataRow row in source.Rows)
            {
                try
                {
                    account = GetAccount(row[_provider.PaymentPurpose].ToString());

                    if (account.Length < Consts.ACCOUNT_SIZE) throw new Exception();

                    if (account.Length == (int)Consts.RightAccountSize.ONE || account.Length == (int)Consts.RightAccountSize.TWO)
                    {
                        idProvider = account.Substring(Consts.ID_PROVIDER_POSITION, Consts.ID_PROVIDER_LENGHT);
                        code = (
                            (isCompanyGroupCheckBox.Checked ? idProvider + "_" : "") + 
                            account.Substring(Consts.CODE_POSITION, Consts.CODE_LENGHT)
                        );
                    }
                    else
                    {
                        code = _provider.DefaultCode;
                    }

                    if (account.Length > Consts.ACCOUNT_SIZE)
                    {
                        account = ShorteningAccount(account, Consts.ACCOUNT_SIZE);
                    }

                    double.Parse(row[_provider.LoanAmount].ToString());

                    if (code == string.Empty) throw new Exception();

                    if (!dataSetsDictionary.ContainsKey(code))
                    {
                        table = CreateResultTable(code);

                        table.LoadDataRow(new string[] {
                            account,
                            row[_provider.LoanAmount].ToString(),
                            Utils.DateDayIncrement(row[_provider.PaymentDate].ToString(), -1).ToString()
                        }, true);

                        container = new DataSet();
                        container.Tables.Add(table);

                        dataSetsDictionary.Add(code, container);
                    }
                    else
                    {
                        dataSetsDictionary[code].Tables[0].LoadDataRow(new string[] {
                            account,
                            row[_provider.LoanAmount].ToString(),
                            Utils.DateDayIncrement(row[_provider.PaymentDate].ToString(), -1).ToString()
                        }, true);
                    }
                }
                catch
                {
                    List<object> errorRow = DataHelper.GetClearedRow(row.ItemArray);

                    if (errorRow.Count > 0)
                    {
                        while (errorRow.Count < errors.Columns.Count)
                        {
                            errorRow.Add(string.Empty);
                        }

                        errors.Rows.Add(errorRow.ToArray());
                    }
                }
            }

            return dataSetsDictionary;
        }

        private void Parse(string file)
        {
            if (_providerName == string.Empty || _provider ==  null) throw new Exception("Укажите нужного поставщика для обработки!");
            if (file == string.Empty) throw new Exception("Добавьте excel файл!");
            if (!FileHelper.CheckFileType(file, ".xls") &&
                !FileHelper.CheckFileType(file, ".xlsx")) throw new Exception("Недопустимый тип файла!");

            DataTable dt = Excel.ReadExcel(file);

            if (dt == null) return;

            DataTable errors = new DataTable(dt.TableName);
            foreach (DataColumn column in dt.Columns)
            {
                errors.Columns.Add();
            }

            Dictionary<string, DataSet> resultDataSets = DataDistribution(dt, errors);

            DataSet tableContainer = new DataSet();

            tableContainer.Tables.Add(errors);

            Excel.WriteDataSetInExcel($"{FileHelper.FilenameWithoutType(file)}_errors.xls", tableContainer);

            tableContainer.Tables.Remove(errors);

            FileHelper.CreateDirectory(FileHelper.FilenameWithoutType(file) + "_results");
            Excel.WriteDataSetDictionaryInExcel(FileHelper.FilenameWithoutType(file), "_results", resultDataSets);
        }

        private void OpenReferenceForm()
        {
            var referencesForm = new ReferenceForm();
            referencesForm.Show();
        }

        #region Events

        private void addButton_Click(object sender, EventArgs e)
        {
            if (_openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _file = _openFileDialog.FileName;
                filePathTextBox.Text = _file;
            }
        }

        private void filePathTextBox_TextChanged(object sender, EventArgs e)
        {
            if (_file != (sender as TextBox).Text)
            {
                _file = (sender as TextBox).Text;
            }
        }

        private async void parseButton_Click(object sender, EventArgs e)
        {
            try
            {
                Enabled = false;
                Cursor = Cursors.WaitCursor;

                await Task.Run(() => Parse(_file));

                MessageBox.Show("Обработка закончена");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Enabled = true;
                Cursor = Cursors.Default;
            }
        }

        private void filePathTextBox_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void filePathTextBox_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (files != null)
            {
                _file = files[0];
                (sender as TextBox).Text = _file;
            }
        }

        private void providersComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            _providerName = (sender as ComboBox).SelectedItem.ToString();

            switch (_providerName)
            {
                case "МРИВЦ":
                    _provider = new Provider(new int[] { 1, 13, 20 });
                    break;
                case "МЭС":
                    _provider = new Provider(new int[] { 0, 2, 3 }, "74");
                    break;
                case "МТЭЦ_1":
                    _provider = new Provider(new int[] { 1, 17, 20 });
                    break;
                case "МТЭЦ_2":
                    _provider = new Provider(new int[] { 2, 8, 11 });
                    break;
            }
        }

        private void referenceButton_Click(object sender, EventArgs e)
        {
            OpenReferenceForm();
        }

        #endregion
    }
}
