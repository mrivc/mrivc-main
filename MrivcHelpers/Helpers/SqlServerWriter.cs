﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MrivcHelpers.Helpers
{
    public class SqlServerWriter
    {
        private readonly string connectionString;

        public bool CreateAddressColumnsAsVarcharType = true;
        public bool CreateAllColumnsAsVarcharType = false;

        public SqlServerWriter(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void WriteDataTable(DataTable dataTable, bool dropTableIfExists = false)
        {
            if (!IsTableExists(dataTable))
            {
                CreateTable(dataTable);
            }
            else
            {
                if (dropTableIfExists)
                {
                    DropTable(dataTable);
                    CreateTable(dataTable);
                }
                else
                    TruncateTable(dataTable);
            }
            BulkCopy(dataTable);
        }

        private void DropTable(DataTable dataTable)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("drop table " + dataTable.TableName, connection);
                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        private void TruncateTable(DataTable dataTable)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("truncate table " + dataTable.TableName, connection);
                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        private void BulkCopy(DataTable dataTable)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlBulkCopy bulkCopy = new SqlBulkCopy(connection);
                bulkCopy.DestinationTableName = dataTable.TableName;

                connection.Open();
                bulkCopy.WriteToServer(dataTable);
            }
        }

        private void CreateTable(DataTable dataTable)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string createCommand = GenerateCreateTableScript(dataTable);
                SqlCommand command = new SqlCommand(createCommand, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        private string GenerateCreateTableScript(DataTable dataTable)
        {
            string tableName = dataTable.TableName;
            var sqlsc = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                DataColumn column = dataTable.Columns[i];
                sqlsc += "\r\n [" + column.ColumnName + "] ";
                string columnType = column.DataType.ToString();

                if (((column.ColumnName.ToLower().Contains("address") || column.ColumnName.ToLower().Contains("flat")
                     || column.ColumnName.ToLower().Contains("house")) && CreateAddressColumnsAsVarcharType) || CreateAllColumnsAsVarcharType)
                {
                    sqlsc += string.Format(" nvarchar({0}) ", column.MaxLength == -1 ? "max" : column.MaxLength.ToString());
                }
                else
                {
                    switch (columnType)
                    {
                        case "System.Int32":
                            sqlsc += " int ";
                            break;
                        case "System.Int64":
                            sqlsc += " bigint ";
                            break;
                        case "System.Int16":
                            sqlsc += " smallint";
                            break;
                        case "System.Byte":
                            sqlsc += " tinyint";
                            break;
                        case "System.Decimal":
                            sqlsc += " decimal ";
                            break;
                        case "System.DateTime":
                            sqlsc += " datetime ";
                            break;
                        case "System.Double":
                            sqlsc += " float ";
                            break;
                        case "System.Boolean":
                            sqlsc += " bit ";
                            break;
                        case "System.Byte[]":
                            sqlsc += " varbinary(MAX) ";
                            break;
                        case "System.TimeSpan":
                            sqlsc += " time(7) ";
                            break;

                        case "System.String":
                        default:
                            sqlsc += string.Format(" nvarchar({0}) ", column.MaxLength == -1 ? "max" : column.MaxLength.ToString());
                            break;
                    }
                }

                if (!column.AllowDBNull)
                    sqlsc += " NOT NULL ";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + "\r\n)";

        }

        private bool IsTableExists(DataTable dataTable)
        {
            string query = string.Format(@"IF EXISTS (SELECT 1 
                                           FROM INFORMATION_SCHEMA.TABLES 
                                           WHERE TABLE_TYPE='BASE TABLE' 
                                           AND TABLE_NAME='{0}') 
                                           SELECT 1 AS res ELSE SELECT 0 AS res;", dataTable.TableName);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                connection.Open();
                int? res = command.ExecuteScalar() as int?;

                return res.HasValue && res.Value > 0;
            }
        }
    }
}
