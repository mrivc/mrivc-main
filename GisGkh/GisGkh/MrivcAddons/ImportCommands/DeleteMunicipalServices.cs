﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class DeleteMunicipalServices : MrivcImportCommand
    {
        public string MunServiceGuid { get; set; }

        public DeleteMunicipalServices(string senderID) : base("importMunicipalServices", "NsiAsync", senderID)
        {
            _importValueName = "DeleteMainMunicipalService";
            Description = "Удаление коммунальных услуг";
        }


        protected override void CreateXmlBody()
        {
            var deleteMunServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.DeleteMunicipalServices @SenderID, @addServiceGuid", new[,] { { "@SenderID", SenderID }, { "@addServiceGuid", MunServiceGuid } });

            foreach (DataRow row in deleteMunServices.Rows)
            {
                var values = new[]
                {
                    new Tuple<string[],string>(new [] { "ElementGuid" }, row["ElementGuid"].ToString())
                };

                AddChildElement(MrivcStatic.GisConfig.Services["NsiAsync"].Methods["importMunicipalServices_delete"].Template, values, "importMunicipalServicesRequest");
            }
        }
    }
}
