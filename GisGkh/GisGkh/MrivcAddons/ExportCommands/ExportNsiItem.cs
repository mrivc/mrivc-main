﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportNsiItem : MrivcExportCommand
    {
        public ExportNsiItem(string senderID) 
            : base("exportNsiItem", "NsiCommonAsync", senderID, "NsiItem")
        {}

        public new void ExecuteToTempDB(string nsiRegistryNumber)
        {
            var values = (new Tuple<string[], string>[]
            {
                new Tuple<string[], string>(new[] { "RegistryNumber" }, nsiRegistryNumber),
                new Tuple<string[], string>(new[] { "ListGroup" }, "NSI"),
                new Tuple<string[], string>(new[] { "ModifiedAfter" }, "2000-01-01T00:00:00.812+03:00")
            }).AsEnumerable();

            ExecuteToTempDB(values, "NsiItem");
        }
    }
}
