﻿namespace EllisBillFilter
{
    partial class FrmMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            FastReport.Design.DesignerSettings designerSettings1 = new FastReport.Design.DesignerSettings();
            FastReport.Design.DesignerRestrictions designerRestrictions1 = new FastReport.Design.DesignerRestrictions();
            FastReport.Export.Email.EmailSettings emailSettings1 = new FastReport.Export.Email.EmailSettings();
            FastReport.PreviewSettings previewSettings1 = new FastReport.PreviewSettings();
            FastReport.ReportSettings reportSettings1 = new FastReport.ReportSettings();
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.lblPeriod = new System.Windows.Forms.Label();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridBillFilterCurrent = new EllisBillFilter.GridBillFilterCurrent();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.tabCompanyHouses = new DevExpress.XtraTab.XtraTabPage();
            this.houseListCurrent = new EllisBillFilter.ClientDetailHouseList();
            this.tabAllHouses = new DevExpress.XtraTab.XtraTabPage();
            this.houseListDebt = new EllisBillFilter.ClientDetailHouseList();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtDeriveCompany = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabActiveClientDetail = new DevExpress.XtraTab.XtraTabPage();
            this.tabHouseClientDetail = new DevExpress.XtraTab.XtraTabPage();
            this.houseClientDetailFilter1 = new EllisBillFilter.HouseClientDetailFilter();
            this.treeHouseHierarhy1 = new EllisBillFilter.TreeHouseHierarhy();
            this.tabPrint = new DevExpress.XtraTab.XtraTabPage();
            this.previewControl1 = new FastReport.Preview.PreviewControl();
            this.lblButtonFind = new System.Windows.Forms.Label();
            this.report1 = new FastReport.Report();
            this.fastReportEnvironmentSettings = new FastReport.EnvironmentSettings();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.tabCompanyHouses.SuspendLayout();
            this.tabAllHouses.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeriveCompany.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tabActiveClientDetail.SuspendLayout();
            this.tabHouseClientDetail.SuspendLayout();
            this.tabPrint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            this.SuspendLayout();
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // cbPeriod
            // 
            this.cbPeriod.Location = new System.Drawing.Point(66, 9);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Properties.Appearance.Options.UseTextOptions = true;
            this.cbPeriod.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPeriod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Period", "Period", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Work_Period", "Is_Work_Period", 86, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.cbPeriod.Properties.DataSource = this.periodBindingSource;
            this.cbPeriod.Properties.DisplayMember = "Name";
            this.cbPeriod.Properties.NullText = "";
            this.cbPeriod.Properties.PopupFormMinSize = new System.Drawing.Size(100, 250);
            this.cbPeriod.Properties.PopupWidth = 60;
            this.cbPeriod.Properties.ShowFooter = false;
            this.cbPeriod.Properties.ShowHeader = false;
            this.cbPeriod.Properties.ValueMember = "Period";
            this.cbPeriod.Properties.EditValueChanged += new System.EventHandler(this.cbPeriod_Properties_EditValueChanged);
            this.cbPeriod.Size = new System.Drawing.Size(130, 20);
            this.cbPeriod.TabIndex = 10;
            // 
            // lblPeriod
            // 
            this.lblPeriod.AutoSize = true;
            this.lblPeriod.Location = new System.Drawing.Point(12, 12);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(48, 13);
            this.lblPeriod.TabIndex = 2;
            this.lblPeriod.Text = "Период:";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridBillFilterCurrent);
            this.splitContainerControl1.Panel1.Text = "Дома по коду фильтра";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl2);
            this.splitContainerControl1.Panel2.Text = "Выбор домов";
            this.splitContainerControl1.Size = new System.Drawing.Size(1138, 616);
            this.splitContainerControl1.SplitterPosition = 769;
            this.splitContainerControl1.TabIndex = 3;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridBillFilterCurrent
            // 
            this.gridBillFilterCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridBillFilterCurrent.Location = new System.Drawing.Point(0, 0);
            this.gridBillFilterCurrent.Name = "gridBillFilterCurrent";
            this.gridBillFilterCurrent.Size = new System.Drawing.Size(769, 616);
            this.gridBillFilterCurrent.TabIndex = 2;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.tabCompanyHouses;
            this.xtraTabControl2.Size = new System.Drawing.Size(363, 616);
            this.xtraTabControl2.TabIndex = 1;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabCompanyHouses,
            this.tabAllHouses});
            // 
            // tabCompanyHouses
            // 
            this.tabCompanyHouses.Controls.Add(this.houseListCurrent);
            this.tabCompanyHouses.Name = "tabCompanyHouses";
            this.tabCompanyHouses.Size = new System.Drawing.Size(356, 587);
            this.tabCompanyHouses.Text = "Текущие дома по УК";
            // 
            // houseListCurrent
            // 
            this.houseListCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.houseListCurrent.Location = new System.Drawing.Point(0, 0);
            this.houseListCurrent.Name = "houseListCurrent";
            this.houseListCurrent.Size = new System.Drawing.Size(356, 587);
            this.houseListCurrent.TabIndex = 0;
            this.houseListCurrent.ShowAllBillFiltersByHouseIDEvent += new EllisBillFilter.ShowAllBillFiltersByHouseIDEventHandler(this.clientDetailHouseList1_ShowAllBillFiltersByHouseIDEvent);
            this.houseListCurrent.RequestReloadHouseListEvent += new EllisBillFilter.RequestReloadHouseListEventHandler(this.houseListCurrent_RequestReloadHouseListEvent);
            // 
            // tabAllHouses
            // 
            this.tabAllHouses.Controls.Add(this.houseListDebt);
            this.tabAllHouses.Name = "tabAllHouses";
            this.tabAllHouses.Size = new System.Drawing.Size(356, 587);
            this.tabAllHouses.Text = "Долговые дома по УК";
            // 
            // houseListDebt
            // 
            this.houseListDebt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.houseListDebt.Location = new System.Drawing.Point(0, 0);
            this.houseListDebt.Name = "houseListDebt";
            this.houseListDebt.Size = new System.Drawing.Size(356, 587);
            this.houseListDebt.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 694);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1151, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsStatusLabel
            // 
            this.tsStatusLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsStatusLabel.Name = "tsStatusLabel";
            this.tsStatusLabel.Size = new System.Drawing.Size(45, 17);
            this.tsStatusLabel.Text = "Статус:";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "house_5840.png");
            this.imageList1.Images.SetKeyName(1, "house_link_7100.png");
            // 
            // txtDeriveCompany
            // 
            this.txtDeriveCompany.Location = new System.Drawing.Point(405, 9);
            this.txtDeriveCompany.Name = "txtDeriveCompany";
            this.txtDeriveCompany.Size = new System.Drawing.Size(169, 20);
            this.txtDeriveCompany.TabIndex = 0;
            this.txtDeriveCompany.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtDeriveCompany_EditValueChanging);
            this.txtDeriveCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDeriveCompany_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(207, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Фильтр по управляющей компании:";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(3, 44);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabActiveClientDetail;
            this.xtraTabControl1.Size = new System.Drawing.Size(1145, 647);
            this.xtraTabControl1.TabIndex = 11;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabActiveClientDetail,
            this.tabHouseClientDetail,
            this.tabPrint});
            this.xtraTabControl1.Click += new System.EventHandler(this.xtraTabControl1_Click);
            // 
            // tabActiveClientDetail
            // 
            this.tabActiveClientDetail.Controls.Add(this.splitContainerControl1);
            this.tabActiveClientDetail.Image = global::EllisBillFilter.Properties.Resources.filter_enable;
            this.tabActiveClientDetail.Name = "tabActiveClientDetail";
            this.tabActiveClientDetail.Size = new System.Drawing.Size(1138, 616);
            this.tabActiveClientDetail.Text = "Активные коды фильтов";
            // 
            // tabHouseClientDetail
            // 
            this.tabHouseClientDetail.Controls.Add(this.houseClientDetailFilter1);
            this.tabHouseClientDetail.Controls.Add(this.treeHouseHierarhy1);
            this.tabHouseClientDetail.Image = global::EllisBillFilter.Properties.Resources.house_5840;
            this.tabHouseClientDetail.Name = "tabHouseClientDetail";
            this.tabHouseClientDetail.Size = new System.Drawing.Size(1138, 616);
            this.tabHouseClientDetail.Text = "Коды фильтров по домам";
            // 
            // houseClientDetailFilter1
            // 
            this.houseClientDetailFilter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.houseClientDetailFilter1.Location = new System.Drawing.Point(0, 0);
            this.houseClientDetailFilter1.Name = "houseClientDetailFilter1";
            this.houseClientDetailFilter1.Size = new System.Drawing.Size(1138, 616);
            this.houseClientDetailFilter1.TabIndex = 1;
            // 
            // treeHouseHierarhy1
            // 
            this.treeHouseHierarhy1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeHouseHierarhy1.Location = new System.Drawing.Point(0, 0);
            this.treeHouseHierarhy1.Name = "treeHouseHierarhy1";
            this.treeHouseHierarhy1.Size = new System.Drawing.Size(1138, 616);
            this.treeHouseHierarhy1.TabIndex = 0;
            // 
            // tabPrint
            // 
            this.tabPrint.Controls.Add(this.previewControl1);
            this.tabPrint.Name = "tabPrint";
            this.tabPrint.Size = new System.Drawing.Size(1138, 616);
            this.tabPrint.Text = "Квитанции";
            // 
            // previewControl1
            // 
            this.previewControl1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.previewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewControl1.Font = new System.Drawing.Font("Tahoma", 8F);
            this.previewControl1.Location = new System.Drawing.Point(0, 0);
            this.previewControl1.Name = "previewControl1";
            this.previewControl1.PageOffset = new System.Drawing.Point(10, 10);
            this.previewControl1.Size = new System.Drawing.Size(1138, 616);
            this.previewControl1.TabIndex = 0;
            this.previewControl1.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
            // 
            // lblButtonFind
            // 
            this.lblButtonFind.AutoSize = true;
            this.lblButtonFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblButtonFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblButtonFind.ForeColor = System.Drawing.Color.Blue;
            this.lblButtonFind.Location = new System.Drawing.Point(580, 12);
            this.lblButtonFind.Name = "lblButtonFind";
            this.lblButtonFind.Size = new System.Drawing.Size(57, 13);
            this.lblButtonFind.TabIndex = 12;
            this.lblButtonFind.Text = "найти >>>";
            this.lblButtonFind.Click += new System.EventHandler(this.lblButtonFind_Click);
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            // 
            // fastReportEnvironmentSettings
            // 
            designerSettings1.ApplicationConnection = null;
            designerSettings1.DefaultFont = new System.Drawing.Font("Arial", 10F);
            designerSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings1.Icon")));
            designerSettings1.Restrictions = designerRestrictions1;
            designerSettings1.Text = "";
            this.fastReportEnvironmentSettings.DesignerSettings = designerSettings1;
            emailSettings1.Address = "";
            emailSettings1.Host = "";
            emailSettings1.MessageTemplate = "";
            emailSettings1.Name = "";
            emailSettings1.Password = "";
            emailSettings1.UserName = "";
            this.fastReportEnvironmentSettings.EmailSettings = emailSettings1;
            previewSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings1.Icon")));
            previewSettings1.Text = "";
            this.fastReportEnvironmentSettings.PreviewSettings = previewSettings1;
            this.fastReportEnvironmentSettings.ReportSettings = reportSettings1;
            this.fastReportEnvironmentSettings.UIStyle = FastReport.Utils.UIStyle.Office2003;
            this.fastReportEnvironmentSettings.StartProgress += new System.EventHandler(this.fastReportEnvironmentSettings_StartProgress);
            this.fastReportEnvironmentSettings.FinishProgress += new System.EventHandler(this.fastReportEnvironmentSettings_FinishProgress);
            this.fastReportEnvironmentSettings.Progress += new FastReport.ProgressEventHandler(this.fastReportEnvironmentSettings_Progress);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 716);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblButtonFind);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDeriveCompany);
            this.Controls.Add(this.lblPeriod);
            this.Controls.Add(this.cbPeriod);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройка кодов фильтров";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Shown += new System.EventHandler(this.FrmMain_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.tabCompanyHouses.ResumeLayout(false);
            this.tabAllHouses.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeriveCompany.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tabActiveClientDetail.ResumeLayout(false);
            this.tabHouseClientDetail.ResumeLayout(false);
            this.tabPrint.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DS.MDataSet mDataSet;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbPeriod;
        private System.Windows.Forms.Label lblPeriod;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.TextEdit txtDeriveCompany;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tabActiveClientDetail;
        private GridBillFilterCurrent gridBillFilterCurrent;
        private DevExpress.XtraTab.XtraTabPage tabHouseClientDetail;
        private TreeHouseHierarhy treeHouseHierarhy1;
        private HouseClientDetailFilter houseClientDetailFilter1;
        private ClientDetailHouseList houseListCurrent;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage tabCompanyHouses;
        private DevExpress.XtraTab.XtraTabPage tabAllHouses;
        private ClientDetailHouseList houseListDebt;
        private System.Windows.Forms.Label lblButtonFind;
        private DevExpress.XtraTab.XtraTabPage tabPrint;
        private FastReport.Preview.PreviewControl previewControl1;
        private FastReport.Report report1;
        private FastReport.EnvironmentSettings fastReportEnvironmentSettings;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsStatusLabel;

    }
}

