﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GisGkh
{
    public class ExportAccountData : MrivcExportCommand, ISingleHouseCommand
    {
        public string FiasHouseGuid { get; set; } = null;

        public ExportAccountData(string senderID) : base("exportAccountData", "HomeManagementAsync", senderID, "exportAccountResult")
        {

            Description = "Экспорт информации о лицевых счетах из ГИС \"ЖКХ\"";
        }

        public void ExecuteToTempDB()
        {
            base.ExecuteToTempDB(new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) }, _exportNodeName);
        }

        public new void Execute()
        {
            MrivcData.CreateTempExportAccountTables();
            this.ExecuteToTempDB();
            if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
            {
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportAccountData", new object[,] { { "@houseGuid", FiasHouseGuid } });
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1("[GIS].[SaveExportedAccountData]");
            }
        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.ExportAccountData", new object[,] { { "@houseGuid", FiasHouseGuid } }, "Accounts");
        }
    }
}
