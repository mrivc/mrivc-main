﻿using System;
using System.Collections.Generic;
using System.Text;
using FastReport;
using FastReport.Export;
using FastReport.Utils;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using FastReport.Table;
using FastReport.Forms;
using System.Collections;
using System.Drawing.Imaging;

namespace FastReport.Export.Svg
{
        /// <summary>
        /// Specifies the image format in SVG export.
        /// </summary>
        public enum SVGImageFormat
        {
            /// <summary>
            /// Specifies the .png format.
            /// </summary>
            Png,

            /// <summary>
            /// Specifies the .jpg format.
            /// </summary>
            Jpeg
        }

        /// <summary>
        /// Represents the SVG export filter.
        /// </summary>
        public class SVGExport : ExportBase
        {
            private int currentPage;
            private SVGImageFormat imageFormat;
            private bool pictures;
            private string path;
            private string fileNameWOext;
            private string extension;
            private List<Stream> generatedStreams;
            private Hashtable hashtable;
            private int quality;

            //private bool TextObjectStyle = true;
            //5private bool TextObjectAngleStyle = true;
            private bool LineStyle = true;
            private bool RectangleStyle = true;
            private bool EllipseStyle = true;
            private bool PolygonStyle = true;

            /// <summary>
            /// Enable or disable the pictures in SVG export
            /// </summary>
            public bool Pictures
            {
                get { return pictures; }
                set { pictures = value; }
            }

            /// <summary>
            /// Gets list of generated streams
            /// </summary>
            public List<Stream> GeneratedStreams
            {
                get { return generatedStreams; }
            }

            /// <summary>
            /// Gets or sets the image format used when exporting.
            /// </summary>
            public SVGImageFormat ImageFormat
            {
                get { return imageFormat; }
                set { imageFormat = value; }
            }

            /// <inheritdoc/>
            protected override void Start()
            {
                currentPage = 0;
                path = Path.GetDirectoryName(FileName);
                fileNameWOext = Path.GetFileNameWithoutExtension(FileName);
                extension = Path.GetExtension(FileName);
                GeneratedFiles.Clear();
                GeneratedStreams.Clear();
                hashtable.Clear();
            }

            /// <inheritdoc/>
            protected override void ExportPage(int pageNo)
            {
                //TextObjectStyle = true;
                //TextObjectAngleStyle = true;
                LineStyle = true;
                RectangleStyle = true;
                EllipseStyle = true;
                PolygonStyle = true;
                using (ReportPage page = GetPage(pageNo))
                {
                    SVGDocument svg = new SVGDocument(fileNameWOext, page.PaperWidth * Units.Millimeters, page.PaperHeight * Units.Millimeters);
                    foreach (Base c in page.AllObjects)
                    {
                        if (c is ReportComponentBase)
                        {
                            ReportComponentBase obj = c as ReportComponentBase;

                            if (obj is CellularTextObject)
                                obj = (obj as CellularTextObject).GetTable();
                            if (obj is TableCell)
                                continue;
                            else if (obj is TableBase)
                            {
                                TableBase table = obj as TableBase;
                                if (table.ColumnCount > 0 && table.RowCount > 0)
                                {
                                    using (TextObject tableback = new TextObject())
                                    {
                                        tableback.Border = table.Border;
                                        tableback.Fill = table.Fill;
                                        tableback.FillColor = table.FillColor;
                                        tableback.Left = table.AbsLeft;
                                        tableback.Top = table.AbsTop;
                                        float tableWidth = 0;
                                        float tableHeight = 0;
                                        for (int i = 0; i < table.ColumnCount; i++)
                                            tableWidth += table[i, 0].Width;
                                        for (int i = 0; i < table.RowCount; i++)
                                            tableHeight += table.Rows[i].Height;
                                        tableback.Width = (tableWidth < table.Width) ? tableWidth : table.Width;
                                        tableback.Height = tableHeight;
                                        AddTextObject(svg, tableback as TextObject, false);
                                        // draw cells
                                        AddTable(svg, table, true);
                                        // draw cells border
                                        AddTable(svg, table, false);
                                        // draw table border
                                        AddBorder(svg, tableback.Border, tableback.AbsLeft, tableback.AbsTop, tableback.Width, tableback.Height);
                                    }
                                }
                            }
                            else if (obj is TextObject)
                            {
                                AddTextObject(svg, obj as TextObject, false);
                            }
                            else if (obj is BandBase)
                            {
                                AddBandObject(svg, obj as BandBase);
                            }
                            else if (obj is LineObject)
                            {
                                AddLine(svg, obj as LineObject);
                            }
                            else if (obj is ShapeObject)
                            {
                                AddShape(svg, obj as ShapeObject);
                            }
                            else
                            {
                                AddPictureObject(svg, obj as ReportComponentBase);
                            }
                        }
                    }

                    string pageFileName = Path.Combine(path, fileNameWOext + currentPage.ToString() + extension);

                    if (Stream == null)
                    {
                        // server mode, save in internal stream collection
                        MemoryStream pageStream = new MemoryStream();
                        svg.Save(pageStream);
                        GeneratedStreams.Add(pageStream);
                        GeneratedFiles.Add(pageFileName);
                    }
                    else if (Directory.Exists(path))
                    {
                        // desktop mode
                        if (currentPage == 0)
                        {
                            // save first page in parent Stream
                            svg.Save(Stream);
                            GeneratedFiles.Add(FileName);
                        }
                        else
                        {
                            // save all page after first in files
                            svg.Save(pageFileName);
                            GeneratedFiles.Add(pageFileName);
                        }
                    }

                    // increment page number
                    currentPage++;
                }
            }

            /// <inheritdoc/>
            protected override void Finish()
            {
                // empty
            }

            /// <inheritdoc/>
            protected override string GetFileFilter()
            {
                return new MyRes("FileFilters").Get("SVGFile");
            }

            /// <inheritdoc/>
            public override void Serialize(FRWriter writer)
            {
                base.Serialize(writer);
                writer.WriteValue("ImageFormat", ImageFormat);
            }

            /// <inheritdoc/>
            public override bool ShowDialog()
            {
                using (SVGExportForm form = new SVGExportForm())
                {
                    form.Init(this);
                    return form.ShowDialog() == DialogResult.OK;
                }
            }

            private void AddTable(SVGDocument svg, TableBase table, bool drawCells)
            {
                float y = 0;
                for (int i = 0; i < table.RowCount; i++)
                {
                    float x = 0;
                    for (int j = 0; j < table.ColumnCount; j++)
                    {
                        if (!table.IsInsideSpan(table[j, i]))
                        {
                            TableCell textcell = table[j, i];
                            textcell.Left = x;
                            textcell.Top = y;
                            if (drawCells)
                            {
                                Border oldBorder = textcell.Border.Clone();
                                textcell.Border.Lines = BorderLines.None;
                                if ((textcell as TextObject) is TextObject)
                                    AddTextObject(svg, textcell as TextObject, false);
                                else
                                    AddPictureObject(svg, textcell as ReportComponentBase);
                                textcell.Border = oldBorder;
                            }
                            else
                                AddBorder(svg, textcell.Border, textcell.AbsLeft, textcell.AbsTop, textcell.Width, textcell.Height);
                        }
                        x += (table.Columns[j]).Width;
                    }
                    y += (table.Rows[i]).Height;
                }
            }

            private void AddPictureObject(SVGDocument svg, ReportComponentBase obj)
            {
                if (pictures)
                {
                    System.Drawing.Imaging.ImageFormat format = (imageFormat == SVGImageFormat.Jpeg) ?
                        System.Drawing.Imaging.ImageFormat.Jpeg :
                        System.Drawing.Imaging.ImageFormat.Png;

                    MemoryStream imageStream = new MemoryStream();

                    using (System.Drawing.Image image = new Bitmap((int)obj.Width, (int)obj.Height))
                    {
                        using (Graphics g = Graphics.FromImage(image))
                        {
                            using (GraphicCache cache = new GraphicCache())
                            {
                                if (imageFormat == SVGImageFormat.Jpeg)
                                    g.Clear(Color.White);

                                float Left = obj.Width >= 0 ? obj.AbsLeft : obj.AbsLeft + obj.Width;
                                float Top = obj.Height >= 0 ? obj.AbsTop : obj.AbsTop + obj.Height;

                                g.TranslateTransform(-Left, -Top);
                                obj.Draw(new FRPaintEventArgs(g, 1, 1, cache));

                            }
                        }

                        if (imageFormat == SVGImageFormat.Jpeg)
                            ExportUtils.SaveJpeg(image, imageStream, quality);
                        else
                            image.Save(imageStream, format);
                    }

                    imageStream.Position = 0;

                    string hash = Crypter.ComputeHash(imageStream);
                    string imageFileName = fileNameWOext + hashtable.Count.ToString() + "." + format.ToString().ToLower();
                    string fullImagePath = Path.Combine(path, imageFileName);

                    if (!hashtable.ContainsKey(hash))
                    {
                        hashtable.Add(hash, imageFileName);
                        GeneratedFiles.Add(fullImagePath);

                        if (Stream == null)
                        {
                            // server mode, save in internal collection
                            generatedStreams.Add(imageStream);
                        }
                        else
                        {
                            // desktop mode, save in file
                            using (FileStream file = new FileStream(fullImagePath, FileMode.Create))
                                imageStream.WriteTo(file);
                        }
                    }
                    else
                        imageFileName = Path.Combine(path, hashtable[hash] as string);

                    // add image in SVG
                    svg.AddImage(imageFileName, obj.AbsLeft, obj.AbsTop, obj.Width, obj.Height);
                }
            }

            private void AddBorder(SVGDocument svg, Border border, float Left, float Top, float Width, float Height)
            {
                if (border.Lines != BorderLines.None)
                {
                    using (TextObject emptyText = new TextObject())
                    {
                        emptyText.Left = Left;
                        emptyText.Top = Top;
                        emptyText.Width = Width;
                        emptyText.Height = Height;
                        emptyText.Border = border;
                        emptyText.Text = String.Empty;
                        emptyText.FillColor = Color.Transparent;
                        AddTextObject(svg, emptyText, true);
                    }
                }
            }
            
            /// <summary>
            /// Add TextObject.
            /// </summary>
            private void AddTextObject(SVGDocument svg, TextObject text, bool Band)
            {
                Font font = text.Font;
                float AbsLeft = text.AbsLeft;
                float AbsTop = text.AbsTop;
                float Width = text.Width;
                float Height = text.Height;
                string HorzAlign = Convert.ToString(text.HorzAlign);
                string VertAlign = Convert.ToString(text.VertAlign);
                float BorderWidth = text.Border.Width;
                string BorderLines = Convert.ToString(text.Border.Lines);
                string Text = text.Text;
                float FontSize = text.Font.Size;
                string FontName = Convert.ToString(text.Font.Name);
                bool Bold = text.Font.Bold;
                bool Italic = text.Font.Italic;
                bool Underline = text.Font.Underline;
                float PaddingLeft = text.Padding.Left;
                float PaddingTop = text.Padding.Top;
                float PaddingRight = text.Padding.Right;
                float PaddingBottom = text.Padding.Bottom;
                bool WordWrap = text.WordWrap;
                string BorderBrush;
                string Background;
                string Foreground;
                float Angle = text.Angle;
                float LeftLine = text.Border.LeftLine.Width;
                float TopLine = text.Border.TopLine.Width;
                float RightLine = text.Border.RightLine.Width;
                float BottomLine = text.Border.BottomLine.Width;
                //Dash------------------------------------------------
                string LeftLineDashStile = Convert.ToString(text.Border.LeftLine.DashStyle);
                string TopLineDashStile = Convert.ToString(text.Border.TopLine.DashStyle);
                string RightLineDashStile = Convert.ToString(text.Border.RightLine.DashStyle);
                string BottomLineDashStile = Convert.ToString(text.Border.BottomLine.DashStyle);

                string colorLeftLine = ToARGB(text.Border.LeftLine.Color.R, text.Border.LeftLine.Color.G, text.Border.LeftLine.Color.B);
                string colorTopLine = ToARGB(text.Border.TopLine.Color.R, text.Border.TopLine.Color.G, text.Border.TopLine.Color.B);
                string colorRightLine = ToARGB(text.Border.RightLine.Color.R, text.Border.RightLine.Color.G, text.Border.RightLine.Color.B);
                string colorBottomLine = ToARGB(text.Border.BottomLine.Color.R, text.Border.BottomLine.Color.G, text.Border.BottomLine.Color.B);
               
                //GlassFill
                bool Glass = false;
                string colorTop = null;
                if (text.Fill is GlassFill)
                {
                    Glass = true;
                    Color color = GetBlendColor((text.Fill as GlassFill).Color, (text.Fill as GlassFill).Blend);
                    colorTop = ToARGB(color.R, color.G, color.B);
                }

                NormalizeBorderBrushColor(text, out BorderBrush);
                NormalizeForegroundColor(text, out Foreground);
                NormalizeBackgroundColor(text, out Background);
                
                //Shadow----
                float ShadowWidth = text.Border.ShadowWidth;
                string ShadowColor = ToARGB(text.Border.ShadowColor.R, text.Border.ShadowColor.G, text.Border.ShadowColor.B);
                
                if (Band)
                {
                    HorzAlign = null;
                    VertAlign = null;
                    Text = null;
                    FontSize = 0;
                    Foreground = null;
                    FontName = null;
                    Bold = false;
                    Italic = false;
                    Underline = false;
                    PaddingLeft = 0;
                    PaddingTop = 0;
                    PaddingRight = 0;
                    PaddingBottom = 0;
                    WordWrap = false;
                }
                {
                   /* if (TextObjectStyle == true)
                    {
                        xaml.AddResourceTextObject(); TextObjectStyle = false;
                    }*/

                    svg.AddTextObject(AbsLeft, AbsTop, Width, Height, HorzAlign, VertAlign, BorderBrush,
                     BorderWidth, LeftLine, TopLine, RightLine, BottomLine, LeftLineDashStile, TopLineDashStile,
                     RightLineDashStile, BottomLineDashStile, colorLeftLine, colorTopLine, colorRightLine, colorBottomLine,
                     text.Border.Shadow, ShadowColor, ShadowWidth, Background, BorderLines, Text, Foreground, PaddingLeft, PaddingTop, PaddingRight, PaddingBottom, WordWrap, Angle, Glass, colorTop, font);
                }
            }
            
            /// <summary>
            /// Add BandObject.
            /// </summary>
            private void AddBandObject(SVGDocument svg, BandBase band)
            {
                using (TextObject newObj = new TextObject())
                {
                    newObj.Left = band.AbsLeft;
                    newObj.Top = band.AbsTop;
                    newObj.Width = band.Width;
                    newObj.Height = band.Height;
                    newObj.Fill = band.Fill;
                    newObj.Border = band.Border;

                    AddTextObject(svg, newObj, true);
                }
            }
            
            /// <summary>
            /// Add Line.
            /// </summary>
            private void AddLine(SVGDocument svg, LineObject line)
            {
                float AbsLeft = line.AbsLeft;
                float AbsTop = line.AbsTop;
                float Width = line.Width;
                float Height = line.Height;
                string Fill = Convert.ToString(GetColorFromFill(line.Fill));
                float Border = line.Border.Width;

                if (LineStyle == true)
                {
                    //xaml.AddResourceLine(); LineStyle = false;
                }

                string BorderBrush = ToARGB(/*line.Border.Color.A,*/ line.Border.Color.R, line.Border.Color.G, line.Border.Color.B);
                if (line.StartCap.Style == CapStyle.Arrow)
                {
                    float x3, y3, x4, y4;
                    DrawArrow(line.StartCap, Border, Width + AbsLeft, Height + AbsTop, AbsLeft, AbsTop, out x3, out y3, out x4, out y4);

                    svg.AddLine(AbsLeft, AbsTop, x3, y3, BorderBrush, Border/*, Fill*/);
                    svg.AddLine(AbsLeft, AbsTop, x4, y4, BorderBrush, Border/*, Fill*/);
                }

                if (line.EndCap.Style == CapStyle.Arrow)
                {
                    float x3, y3, x4, y4;
                    DrawArrow(line.EndCap, Border, AbsLeft, AbsTop, Width + AbsLeft, Height + AbsTop, out x3, out y3, out x4, out y4);

                    svg.AddLine(Width + AbsLeft, AbsTop + Height, x3, y3, BorderBrush, Border/*, Fill*/);
                    svg.AddLine(Width + AbsLeft, AbsTop + Height, x4, y4, BorderBrush, Border/*, Fill*/);

                }
                svg.AddLine(AbsLeft, AbsTop, AbsLeft + Width, AbsTop + Height, BorderBrush, Border/*, Fill*/);
            }

            private void DrawArrow(CapSettings Arrow, float lineWidth, float x1, float y1, float x2, float y2, out float x3, out float y3, out float x4, out float y4)
            {
                float k1, a, b, c, d;
                float xp, yp;
                float wd = Arrow.Width * lineWidth;
                float ld = Arrow.Height * lineWidth;
                if (Math.Abs(x2 - x1) > 0)
                {
                    k1 = (y2 - y1) / (x2 - x1);
                    a = (float)(Math.Pow(k1, 2) + 1);
                    b = 2 * (k1 * ((x2 * y1 - x1 * y2) / (x2 - x1) - y2) - x2);
                    c = (float)(Math.Pow(x2, 2) + Math.Pow(y2, 2) - Math.Pow(ld, 2) +
                        Math.Pow((x2 * y1 - x1 * y2) / (x2 - x1), 2) -
                        2 * y2 * (x2 * y1 - x1 * y2) / (x2 - x1));
                    d = (float)(Math.Pow(b, 2) - 4 * a * c);
                    xp = (float)((-b + Math.Sqrt(d)) / (2 * a));
                    if ((xp > x1) && (xp > x2) || (xp < x1) && (xp < x2))
                        xp = (float)((-b - Math.Sqrt(d)) / (2 * a));
                    yp = xp * k1 + (x2 * y1 - x1 * y2) / (x2 - x1);
                    if (y2 != y1)
                    {
                        x3 = (float)(xp + wd * Math.Sin(Math.Atan(k1)));
                        y3 = (float)(yp - wd * Math.Cos(Math.Atan(k1)));
                        x4 = (float)(xp - wd * Math.Sin(Math.Atan(k1)));
                        y4 = (float)(yp + wd * Math.Cos(Math.Atan(k1)));
                    }
                    else
                    {
                        x3 = xp; y3 = yp - wd;
                        x4 = xp; y4 = yp + wd;
                    }
                }
                else
                {
                    xp = x2; yp = y2 - ld;
                    if ((yp > y1) && (yp > y2) || (yp < y1) && (yp < y2))
                        yp = y2 + ld;
                    x3 = xp - wd; y3 = yp;
                    x4 = xp + wd; y4 = yp;
                }
            }
            
            /// <summary>
            /// Add Shape.
            /// </summary>
            private void AddShape(SVGDocument svg, ShapeObject shape)
            {
                float AbsLeft = shape.AbsLeft;
                float AbsTop = shape.AbsTop;
                float Width = shape.Width;
                float Height = shape.Height;
                float BorderWidth = shape.Border.Width;
                string BorderLines = Convert.ToString(shape.Border.Lines);
                string BorderBrush;
                string Background;

                NormalizeColor(shape, out BorderBrush, out Background);

                if (shape.Shape == ShapeKind.Rectangle)
                {
                    if (RectangleStyle == true)
                    {
                       // svg.AddResourceRectangle(); RectangleStyle = false;
                    }
                    svg.AddRectangle(AbsLeft, AbsTop, Width, Height, BorderBrush, BorderWidth, Background, false);
                }

                if (shape.Shape == ShapeKind.RoundRectangle)
                {
                    if (RectangleStyle == true)
                    {
                        //svg.AddResourceRectangle(); RectangleStyle = false;
                    }
                    svg.AddRectangle(AbsLeft, AbsTop, Width, Height, BorderBrush, BorderWidth, Background, true);
                }

                if (shape.Shape == ShapeKind.Ellipse)
                {
                    if (EllipseStyle == true)
                    {
                       // svg.AddResourceEllipse(); EllipseStyle = false;
                    }

                    svg.AddEllipse(AbsLeft, AbsTop, Width, Height, BorderBrush, BorderWidth, Background);

                }

                if (shape.Shape == ShapeKind.Triangle)
                {
                    if (PolygonStyle == true)
                    {
                       // svg.AddResourcePolygon(); PolygonStyle = false;
                    }
                    svg.AddTriangle(AbsLeft, AbsTop, Width, Height, BorderBrush, BorderWidth, Background);
                }

                if (shape.Shape == ShapeKind.Diamond)
                {
                    if (PolygonStyle == true)
                    {
                       // svg.AddResourcePolygon(); PolygonStyle = false;
                    }
                    svg.AddDiamond(AbsLeft, AbsTop, Width, Height, BorderBrush, BorderWidth, Background);
                }
            }
            
            internal Color GetColorFromFill(FillBase Fill)
            {
                if (Fill is SolidFill)
                    return (Fill as SolidFill).Color;
                else if (Fill is GlassFill)
                    return (Fill as GlassFill).Color;
                else if (Fill is HatchFill)
                    return (Fill as HatchFill).BackColor;
                else if (Fill is PathGradientFill)
                    return (Fill as PathGradientFill).CenterColor;
                else if (Fill is LinearGradientFill)
                    return GetMiddleColor((Fill as LinearGradientFill).StartColor, (Fill as LinearGradientFill).EndColor);
                else
                    return Color.White;
            }

            private Color GetMiddleColor(Color color1, Color color2)
            {
                return Color.FromArgb(255, (color1.R + color2.R) / 2, (color1.G + color2.G) / 2, (color1.B + color2.B) / 2);
            }

            private Color GetBlendColor(Color c, float Blend)
            {
                return Color.FromArgb(255, (int)Math.Round(c.R + (255 - c.R) * Blend),
                        (int)Math.Round(c.G + (255 - c.G) * Blend),
                        (int)Math.Round(c.B + (255 - c.B) * Blend));
            }
            
            private void NormalizeBorderBrushColor(TextObject obj, out string BorderBrush)
            {
                obj.FillColor = GetColorFromFill(obj.Fill);
                BorderBrush = ToARGB(/*obj.Border.Color.A,*/ obj.Border.Color.R, obj.Border.Color.G, obj.Border.Color.B);
            }

            private void NormalizeBackgroundColor(TextObject obj, out string Background)
            {
                obj.FillColor = GetColorFromFill(obj.Fill);
                if (obj.FillColor.Name == "Transparent")
                {
                    Background = "none";
                }
                else Background = ToARGB(/*obj.FillColor.A,*/ obj.FillColor.R, obj.FillColor.G, obj.FillColor.B);
            }

            private void NormalizeForegroundColor(TextObject obj, out string Foreground)
            {
                obj.FillColor = GetColorFromFill(obj.Fill);
                Foreground = ToARGB(/*obj.TextColor.A,*/ obj.TextColor.R, obj.TextColor.G, obj.TextColor.B);
            }

            private void NormalizeColor(ShapeObject obj, out string BorderBrush, out string Background)
            {
                obj.FillColor = GetColorFromFill(obj.Fill);

                BorderBrush = ToARGB(/*obj.Border.Color.A,*/ obj.Border.Color.R, obj.Border.Color.G, obj.Border.Color.B);
                if (obj.FillColor.Name == "Transparent")
                {
                    Background = "none";
                }
                else Background = ToARGB(/*obj.FillColor.A,*/ obj.FillColor.R, obj.FillColor.G, obj.FillColor.B);
            }

            ///<summary>
            ///Convert To ARG.
            /// </summary>
            public string ToARGB(/*int A,*/ int R, int G, int B)
            {
                string color = string.Format("#{0:X2}{1:X2}{2:X2}", R, G, B);
                return color;
            }
            
            /// <summary>
            /// Initializes a new instance of the <see cref="SVGExport"/> class.
            /// </summary>
            public SVGExport()
            {
                HasMultipleFiles = true;
                pictures = true;
                quality = 90;
                imageFormat = SVGImageFormat.Png;
                generatedStreams = new List<Stream>();
                hashtable = new Hashtable();
            }
        }

}
