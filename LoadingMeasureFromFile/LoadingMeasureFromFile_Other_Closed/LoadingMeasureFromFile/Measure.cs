﻿using System;

namespace LoadingMeasureFromFile_Other
{
    /// <summary>
    /// Класс для хранения данных по счетчику из файла
    /// </summary>
    public class Measure
    {
        public int IdMeter { get; }
        public DateTime MeasureDate { get; }
        public double MeasureValue { get; }

        public string SqlFormattedDate => MeasureDate.ToString("dd-MM-yyyy HH:mm:ss");

        public Measure(int id, DateTime date, double value)
        {
            IdMeter = id;
            MeasureDate = date;
            MeasureValue = value;
        }
    }
}
