﻿namespace GisGkh
{
    partial class DeleteAdditionalServicesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteAddServ = new System.Windows.Forms.Button();
            this.senderIdOnlyFilter1 = new Helper.Controls.SenderIdOnlyFilter();
            this.tbAddServGuid = new System.Windows.Forms.TextBox();
            this.lbAddServGuid = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDeleteAddServ
            // 
            this.btnDeleteAddServ.Location = new System.Drawing.Point(147, 229);
            this.btnDeleteAddServ.Name = "btnDeleteAddServ";
            this.btnDeleteAddServ.Size = new System.Drawing.Size(187, 23);
            this.btnDeleteAddServ.TabIndex = 0;
            this.btnDeleteAddServ.Text = "Удалить услугу";
            this.btnDeleteAddServ.UseVisualStyleBackColor = true;
            this.btnDeleteAddServ.Click += new System.EventHandler(this.btnDeleteAddServ_Click);
            // 
            // senderIdOnlyFilter1
            // 
            this.senderIdOnlyFilter1.Location = new System.Drawing.Point(12, 12);
            this.senderIdOnlyFilter1.Name = "senderIdOnlyFilter1";
            this.senderIdOnlyFilter1.Size = new System.Drawing.Size(395, 59);
            this.senderIdOnlyFilter1.TabIndex = 1;
            // 
            // tbAddServGuid
            // 
            this.tbAddServGuid.Location = new System.Drawing.Point(12, 92);
            this.tbAddServGuid.Name = "tbAddServGuid";
            this.tbAddServGuid.Size = new System.Drawing.Size(455, 20);
            this.tbAddServGuid.TabIndex = 2;
            // 
            // lbAddServGuid
            // 
            this.lbAddServGuid.AutoSize = true;
            this.lbAddServGuid.Location = new System.Drawing.Point(12, 74);
            this.lbAddServGuid.Name = "lbAddServGuid";
            this.lbAddServGuid.Size = new System.Drawing.Size(210, 13);
            this.lbAddServGuid.TabIndex = 3;
            this.lbAddServGuid.Text = "Гуид доп. услуги (все буквы маленькие)";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(340, 229);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(187, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Удалить мун. услугу";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 202);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Восстановить мун. услуги";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // DeleteAdditionalServicesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 264);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbAddServGuid);
            this.Controls.Add(this.tbAddServGuid);
            this.Controls.Add(this.senderIdOnlyFilter1);
            this.Controls.Add(this.btnDeleteAddServ);
            this.Name = "DeleteAdditionalServicesForm";
            this.Text = "DeleteAdditionalServicesForm";
            this.Load += new System.EventHandler(this.DeleteAdditionalServicesForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDeleteAddServ;
        private Helper.Controls.SenderIdOnlyFilter senderIdOnlyFilter1;
        private System.Windows.Forms.TextBox tbAddServGuid;
        private System.Windows.Forms.Label lbAddServGuid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}