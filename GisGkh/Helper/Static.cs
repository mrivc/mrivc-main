﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//using System.Threading.Tasks;

namespace Helper
{
    public class Static
    {
        public static bool RunWithTimeout(ParameterizedThreadStart threadStart, TimeSpan timeout, object parameter)
        {
            Thread workerThread = new Thread(threadStart);

            workerThread.Start(parameter);

            bool finished = workerThread.Join(timeout);
            if (!finished)
                workerThread.Abort();

            return finished;
        }
    }
}
