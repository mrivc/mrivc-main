﻿using GisGkh;
using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gis
{
    class Program
    {
        static void Main(string[] args)
        {
            DAccess.DataModule.Connection1 = new SqlConnection(MrivcData.ConnectionString);
            MrivcStatic.Check = true;
            MrivcStatic.Await = true;
            MrivcStatic.RunConsole(args,null,-1,-1);
        }
    }
}
