﻿using MrivcControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter.Forms
{
    public partial class FrmExcelImport : Form
    {
        private string tempDatabaseName = "Hlam";

        delegate void DoOperation();

        private ZXTimer timerCounter;

        public FrmExcelImport()
        {
            InitializeComponent();
            this.Text = this.Text + (" (" + MrivcControls.Data.DAccess.DataModule.Connection.DataSource + " - " + MrivcControls.Data.DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;

            txtTimer.Text = string.Empty;
            timerCounter = new ZXTimer();
            //gridExcelImport1.OpenFile(@"E:\Работа\ИМПОРТ\ТЕСТ.xls");
            gridBindAddress1.LoadDataTablePicker();
            gridDynamicDataImport1.LoadData();
            //gridDataPreview1.LoadData();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            btnImport.Enabled = false;

            ImportData();
            LoadBindAddressGrid();
            MessageBox.Show("Импорт успешно закончен.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

            btnImport.Enabled = true;
        }

        private void ImportData()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                gridExcelImport1.ImportExcelDataInDataBase(tempDatabaseName);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LoadBindAddressGrid()
        {
            tabControl1.SelectedTab = tabDataPrepare;
            gridBindAddress1.LoadData($"{gridExcelImport1.ImportTableName}", true);
        }

        private void btnBindAddress_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnBindAddress.Enabled = false;

                gridBindAddress1.BindAccounts();

                MessageBox.Show("Операция завершена.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ApplicationException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                btnBindAddress.Enabled = true;
                this.Cursor = Cursors.Default;
            }
            //StartOperation(new DoOperation(gridBindAddress1.BindAccounts), "Операция закончена.");
        }

        #region Some operation start
        private void StartOperation(DoOperation doOperation, string operationCompletedMessage)
        {
            StartTimer();
            doOperation.BeginInvoke(new AsyncCallback(OperationInProgress), operationCompletedMessage);
        }

        private void StartTimer()
        {
            timerCounter.Reset();
            timer1.Enabled = true;
        }

        private void StopTimer()
        {
            timerCounter.Reset();
            txtTimer.Text = "Операция завершена.";
            timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.BeginInvoke((Action)(() =>
            {
                timerCounter.Tick();
                txtTimer.Text = $"Пожалуйста подождите, выполняется длительная операция: {timerCounter.StringValue}";
            }));
        }

        private void OperationInProgress(IAsyncResult result)
        {
            DoOperation operationDelegate = ((AsyncResult)result).AsyncDelegate as DoOperation;

            try
            {        
                operationDelegate.EndInvoke(result);

                timer1.Enabled = false;
                txtTimer.Text = "Операция закончена.";
                MessageBox.Show(result.AsyncState.ToString(), "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        private void btnExcuteDynamicImportScript_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnExcuteDynamicImportScript.Enabled = false;
                gridDynamicDataImport1.ExcuteDynamicImportScript();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                btnExcuteDynamicImportScript.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
    }
}
