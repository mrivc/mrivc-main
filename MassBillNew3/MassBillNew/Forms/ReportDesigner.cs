﻿using FastReport.Design;
using FastReport.Design.StandardDesigner;
using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MassBillNew
{
    public partial class ReportDesigner : Form
    {
  

        public ReportDesigner()
        {
            InitializeComponent();

        }

        static void designerForm_Load(object sender, EventArgs e)
        {
            DesignerForm form = sender as DesignerForm;

            form.Designer.MdiMode = true;

            //form.UpdateContent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DesignerForm designerForm = new DesignerForm(DAccess.DataModule.ConnectionString);
            //designerForm.Load += designerForm_Load;

            designerForm.Show();
        }

    }
}
