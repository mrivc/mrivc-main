﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using Ionic.Zip;
using Payments.DS;
using Payments.Properties;
using System.Data.Odbc;
using System.Data.Common;

namespace Payments
{
    public class DbfReestr
    {
        public static readonly string UnloadedPath = Application.StartupPath + "\\Выгрузки\\";
        public static DateTime LastUnloadedPaymentsDate = DateTime.Today;
        private static SortedList<string, string> unloadedFiles = new SortedList<string, string>();
        private static string lastConnectionString = string.Empty;
        

        public static void UnloadToDBF(string fileName, DataTable dataTable)
        {
            UnloadToDBF(fileName, dataTable, string.Empty);
        }

        public static void UnloadToDBF(string fileName, DataTable dataTable, string clientName)
        {
            string templatePath = Application.StartupPath + "\\" + "TEMPLATE.DBF";
            string unloadedFileName = fileName.Substring(0, 8); //уменьшаем имя файла до 8ми знаков

            if (!FileHelper.ExistFile(templatePath))
                throw new Exception("Не найден файл шаблона " + templatePath + ". Восстановите файл шаблона и повторите выгрузку.");

            if (!FileHelper.DirectoryExists(UnloadedPath))
                FileHelper.CreateDirectory(UnloadedPath);

            FileHelper.CopyFile(templatePath, UnloadedPath + unloadedFileName + ".DBF");

            DbConnection con = GetConnection(UnloadedPath);
            DbCommand cmd = GetCommand();

            cmd.Connection = con;
            con.Open();
            cmd.CommandText = "delete from " + unloadedFileName;
            cmd.ExecuteNonQuery();

            foreach (DataRow row in dataTable.Rows)
            {
                string insertSql = "insert into " + unloadedFileName + " values("
                    + "'" + row["LS"].ToString() + "',"
                    + "'" + StringHelper.ReplaceEscape(row["ADRES"].ToString()) + "',"
                    + "'" + StringHelper.ReplaceEscape(row["UK"].ToString()) + "',"
                    + "'" + row["FAM"].ToString() + "',"
                    + "'" + row["IM"].ToString() + "',"
                    + "'" + row["OT"].ToString() + "',"
                    + "'" + row["AUTHCOD"].ToString() + "',"
                    + "'" + row["KVITID"].ToString() + "',"
                    + "'" + row["DATPLAT"].ToString() + "',"
                    + "" + row["SUMPLAT"].ToString().Replace(",",".") + ","
                    + "'" + row["PU1"].ToString() + "',"
                    + "" + row["POK1"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU2"].ToString() + "',"
                    + "" + row["POK2"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU3"].ToString() + "',"
                    + "" + row["POK3"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU4"].ToString() + "',"
                    + "" + row["POK4"].ToString().Replace(",", ".") + ","
                    + "'" + StringHelper.ReplaceEscape(row["COMMENT"].ToString()) + "'"
                    + ")";
                cmd.CommandText = insertSql;
                cmd.ExecuteNonQuery();
            }

            cmd.CommandText = "select SUM(sumplat) as summ from " + unloadedFileName;
            string summ = cmd.ExecuteScalar().ToString();

            //возвращаем файлу исходное наименование
            FileHelper.RewriteFileWithRename(UnloadedPath + unloadedFileName + ".DBF", UnloadedPath + fileName + ".DBF");
            unloadedFiles.Add(UnloadedPath + fileName + ".DBF", "Файл " + fileName + ".PDF"
                + " - количество: " + dataTable.Rows.Count.ToString()
                + ", сумма: " + summ + "руб. "
                + "("+ clientName +")");
            lastConnectionString = con.ConnectionString;
            con.Close();
        }

        public static string CompressLastUploadedFiles(bool deleteOriginalFiles)
        {
            List<string> files = new List<string>();
            string info = "";

            using (ZipFile zip = new ZipFile())
            {
                foreach (KeyValuePair<string, string> kvp in unloadedFiles)
                {
                    files.Add(kvp.Key);
                    info += kvp.Value + "\n";
                }

                FileHelper.WriteTextFile(UnloadedPath + "Information.txt", info, Encoding.UTF8);

                string saveZippedFile = UnloadedPath + LastUnloadedPaymentsDate.Year.ToString() +
                                            StringHelper.ZeroCode(LastUnloadedPaymentsDate.Month) +
                                            StringHelper.ZeroCode(LastUnloadedPaymentsDate.Day) + "_" +
                                            StringHelper.ZeroCode(DateTime.Now.Hour) +
                                            StringHelper.ZeroCode(DateTime.Now.Minute) + "_" +
                                            DAccess.Instance.CurrentUser.Name + ".zip";
                zip.AddFiles(files, false, "");
                zip.AddFile(UnloadedPath + "Information.txt", "");
                zip.Save(saveZippedFile);

                if (deleteOriginalFiles)
                    FileHelper.DeleteFiles(files);

                FileHelper.DeleteFileIfExist(UnloadedPath + "Information.txt");
                return saveZippedFile;
            }
        }

        public static void ClearUnloadedFiles()
        {
            unloadedFiles.Clear();
        }

        public static string LastConnectionString
        {
            get {
                return lastConnectionString;
            }
        }

        private static DbConnection GetConnection(string path)
        {
            if (Settings.Default.DbfReestrUseOleDbConnection)
                return new OleDbConnection(Settings.Default.DbfReestrOleDbConnection.Replace("FileNameHere", path));
            else
                return new OdbcConnection(Settings.Default.DbfReestrOdbcConnection.Replace("FileNameHere", path));
        }

        private static DbCommand GetCommand()
        {
            if (Settings.Default.DbfReestrUseOleDbConnection)
                return new OleDbCommand();
            else
                return new OdbcCommand();
        }
    }
}
