﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillsLoader.DM
{
    public static class StaticQuery
    {
        public static readonly string TmpLoad =
                                    "IF OBJECT_ID('TEMPDB..#tmpLoad') is not null drop table #tmpLoad " + Environment.NewLine +
                                    "create table #tmpLoad (" + Environment.NewLine +
                                        "LS Varchar(12), ADRES Varchar(300), PU Varchar(12), SUMPLAT Float," + Environment.NewLine +
                                        "ND Int, ADDR_NT Varchar(300), UK Varchar(100), AUTHCOD Varchar(24)," + Environment.NewLine +
                                        "KVITID Varchar(12), PU1 Varchar(40), POK1 Float, PU2 Varchar(40)," + Environment.NewLine +
                                        "POK2 Float, PU3 Varchar(40), POK3 Float, PU4 Varchar(40), POK4 Float," + Environment.NewLine +
                                        "DATPLAT DateTime, UNO Varchar(50))" + Environment.NewLine +

                                    "IF OBJECT_ID('TEMPDB..#tLoadErr') is not null drop table #tLoadErr" + Environment.NewLine +
                                    "IF OBJECT_ID('TEMPDB..#ErrT') is not null drop table #ErrT" + Environment.NewLine +

                                    "create table #ErrT (" + Environment.NewLine +
                                        "NPP Int, DATPLAT DateTime, ADRES Varchar(150), AUTHCOD Varchar(24), SUMPLAT Float," + Environment.NewLine +
                                        "OP1 Float, OP2 Float, OP3 Float, OP4 Float, NP1 Float, NP2 Float, NP3 Float, NP4 Float," + Environment.NewLine +
                                        "COMMENT Varchar(300), Processed Varchar(10));";
        
        public static readonly string TmpErr =
                                "create table ErrT (NPP varchar(150), DATPLAT varchar(150), ADRES varchar(150), AUTHCOD varchar(24), SUMPLAT varchar(150)," + Environment.NewLine +
                                "OP1  varchar(150), OP2 varchar(150), OP3 varchar(150), OP4 varchar(150), NP1 varchar(150), NP2 varchar(150), NP3 varchar(150), NP4 varchar(150)," + Environment.NewLine +
                                "COMMENT varchar(250), Processed varchar(10));";
    }
}
