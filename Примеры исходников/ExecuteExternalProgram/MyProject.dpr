library MyProject;

{$R *.res}

uses
  ShellApi,
  SysUtils,
  Classes,
  Controls,
  StdCtrls,
  Windows,Dialogs,ADODB,
  EllisGKH_TLB in 'Uses\EllisGKH_TLB.pas',
  Forms in 'Uses\Forms.pas',
  Graphics in 'Uses\Graphics.pas',
  uLibrary in 'Uses\uLibrary.pas',
  uMDIChild in 'Uses\uMDIChild.pas' {MDIChildForm},
  uMessages in 'Uses\uMessages.pas',
  uProperties in 'Uses\uProperties.pas',
  uPropertyCollection in 'Uses\uPropertyCollection.pas',
  uReportUtils in 'Uses\uReportUtils.pas',
  uMyUnit in 'uMyUnit.pas' {frmMyUnit};

function GetModuleName():String;
  var S,SQLStr,QValue,ModuleName : String;
begin
  SetLength(S, MAX_PATH);
  GetModuleFileName(HInstance, PChar(S), MAX_PATH);
  Result := ExtractFileName(S);
end;

function GetShortModuleName():String;
  var ModuleName : String;
  Index: Integer;
begin
  ModuleName := GetModuleName();
  Index := Pos('.',ModuleName);
  if (Index > 0) then
      ModuleName := Copy(ModuleName, 0, Index-1);
  Result := ModuleName;
end;

Procedure Execute(AOptions: TPropertyCollection);
  var S,SQLStr,QValue,ModuleName,ModuleCaption,ModulePath : String;
  Params: PAnsiChar;
  AccountID: string;
  FConnection: TADOConnection;
  ADOQuery : TADOQuery;
  Result: _Recordset;
Begin

  ModuleName := GetShortModuleName();

  FConnection := TADOConnection(Integer(AOptions[pConnection]));
  ModuleCaption := CaptionByDescription(HInstance);
  SQLStr := 'select Info from module where [Name]='''+ModuleName+'''';


  ADOQuery := TADOQuery.Create(nil);
  ADOQuery.Connection := FConnection;
  ADOQuery.SQL.Add(SQLStr);
  ModulePath:=ExtractFileDir(Application.ExeName);

 AccountID:=AOptions.GetPropertyByName('ID_Account').Value;

  try
    ADOQuery.Active := True;
    Result := ADOQuery.Recordset;
    Result.MoveFirst;
    QValue := Result.Fields['Info'].Value;
    ModulePath:=ModulePath+'\System\'+copy(QValue,1,pos('|',QValue)-1);
    Params := PAnsiChar(AnsiString(FConnection.ConnectionString+'|Module='+QValue+'|AccountID='+AccountID));
    if (FileExists(ModulePath)) then
    begin
      Screen.Cursor := crHourGlass;
      ShellExecute(Application.Handle, 'open', pchar(ModulePath), Params, nil, SW_SHOWNORMAL);
      Screen.Cursor := crDefault;
    end else
    begin
      ShowMessage('������ �������� ������: ' + ModuleName + '. ��������� ������� ������ �� ����: '+ModulePath);
    end;
  except
    on e: EADOError do
    begin
      MessageDlg('Error while doing query', mtError,
                  [mbOK], 0);
      Exit;
    end;
  end;

 // MessageDlg('');
 // uLibrary.ExecuteMDIChild(AOptions, TfrmMyUnit);
End;

Exports Execute;

begin
end.
