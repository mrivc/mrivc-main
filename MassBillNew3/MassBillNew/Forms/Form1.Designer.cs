﻿namespace MassBillNew
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            FastReport.Design.DesignerSettings designerSettings4 = new FastReport.Design.DesignerSettings();
            FastReport.Design.DesignerRestrictions designerRestrictions4 = new FastReport.Design.DesignerRestrictions();
            FastReport.Export.Email.EmailSettings emailSettings4 = new FastReport.Export.Email.EmailSettings();
            FastReport.PreviewSettings previewSettings4 = new FastReport.PreviewSettings();
            FastReport.ReportSettings reportSettings4 = new FastReport.ReportSettings();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chBxTwoDebtComp = new System.Windows.Forms.CheckBox();
            this.chbDebtFilterCodePeriod = new System.Windows.Forms.CheckBox();
            this.btnDebtFilterCodePeriod = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.customEdit1 = new MassBillNew.CustomEdit();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.murmanskDataSet = new MassBillNew.MurmanskDataSet();
            this.button8 = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.vPeriodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.billKindBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new MassBillNew.MultiSelectTreeview();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.parameterFilter2 = new MassBillNew.ParameterFilter();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.previewControl1 = new FastReport.Preview.PreviewControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.parameterOwnerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataTable2BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.parameterDictionaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vPeriod1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vPeriodBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataTable2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.vPeriodTableAdapter = new MassBillNew.MurmanskDataSetTableAdapters.VPeriodTableAdapter();
            this.companyTableAdapter = new MassBillNew.MurmanskDataSetTableAdapters.CompanyTableAdapter();
            this.bill_KindTableAdapter = new MassBillNew.MurmanskDataSetTableAdapters.Bill_KindTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.environmentSettings1 = new FastReport.EnvironmentSettings();
            this.dataTable2TableAdapter = new MassBillNew.MurmanskDataSetTableAdapters.DataTable2TableAdapter();
            this.vPeriod1TableAdapter = new MassBillNew.MurmanskDataSetTableAdapters.VPeriod1TableAdapter();
            this.parameter_DictionaryTableAdapter = new MassBillNew.MurmanskDataSetTableAdapters.Parameter_DictionaryTableAdapter();
            this.parameter_OwnerTableAdapter = new MassBillNew.MurmanskDataSetTableAdapters.Parameter_OwnerTableAdapter();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tableAdapterManager = new MassBillNew.MurmanskDataSetTableAdapters.TableAdapterManager();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.report1 = new FastReport.Report();
            this.report2 = new FastReport.Report();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.murmanskDataSet)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vPeriodBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billKindBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.parameterOwnerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2BindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parameterDictionaryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPeriod1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPeriodBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(2, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(824, 626);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.chBxTwoDebtComp);
            this.tabPage1.Controls.Add(this.chbDebtFilterCodePeriod);
            this.tabPage1.Controls.Add(this.btnDebtFilterCodePeriod);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.tabControl2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(816, 600);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Фильтр";
            // 
            // chBxTwoDebtComp
            // 
            this.chBxTwoDebtComp.AutoSize = true;
            this.chBxTwoDebtComp.Enabled = false;
            this.chBxTwoDebtComp.Location = new System.Drawing.Point(214, 112);
            this.chBxTwoDebtComp.Name = "chBxTwoDebtComp";
            this.chBxTwoDebtComp.Size = new System.Drawing.Size(228, 17);
            this.chBxTwoDebtComp.TabIndex = 16;
            this.chBxTwoDebtComp.Text = "Сформировать несколько долговых УК";
            this.chBxTwoDebtComp.UseVisualStyleBackColor = true;
            this.chBxTwoDebtComp.CheckedChanged += new System.EventHandler(this.chBxTwoDebtComp_CheckedChanged);
            // 
            // chbDebtFilterCodePeriod
            // 
            this.chbDebtFilterCodePeriod.AutoSize = true;
            this.chbDebtFilterCodePeriod.Location = new System.Drawing.Point(446, 112);
            this.chbDebtFilterCodePeriod.Name = "chbDebtFilterCodePeriod";
            this.chbDebtFilterCodePeriod.Size = new System.Drawing.Size(96, 17);
            this.chbDebtFilterCodePeriod.TabIndex = 15;
            this.chbDebtFilterCodePeriod.Text = "Долги по к.ф.";
            this.chbDebtFilterCodePeriod.UseVisualStyleBackColor = true;
            this.chbDebtFilterCodePeriod.Visible = false;
            this.chbDebtFilterCodePeriod.CheckedChanged += new System.EventHandler(this.chbDebtFilterCodePeriod_CheckedChanged);
            // 
            // btnDebtFilterCodePeriod
            // 
            this.btnDebtFilterCodePeriod.Enabled = false;
            this.btnDebtFilterCodePeriod.Image = global::MassBillNew.Properties.Resources.green_arrow_down;
            this.btnDebtFilterCodePeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDebtFilterCodePeriod.Location = new System.Drawing.Point(446, 139);
            this.btnDebtFilterCodePeriod.Name = "btnDebtFilterCodePeriod";
            this.btnDebtFilterCodePeriod.Size = new System.Drawing.Size(236, 28);
            this.btnDebtFilterCodePeriod.TabIndex = 14;
            this.btnDebtFilterCodePeriod.Text = "Выбрать долги по К.Ф. и периоду";
            this.btnDebtFilterCodePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDebtFilterCodePeriod.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnDebtFilterCodePeriod.UseVisualStyleBackColor = true;
            this.btnDebtFilterCodePeriod.Visible = false;
            this.btnDebtFilterCodePeriod.Click += new System.EventHandler(this.btnDebtFilterCodePeriod_Click);
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.Location = new System.Drawing.Point(122, 569);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(113, 28);
            this.button6.TabIndex = 12;
            this.button6.Text = "Сохранить";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(647, 103);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 13;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Visible = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(3, 569);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(113, 28);
            this.button5.TabIndex = 11;
            this.button5.Text = "Загрузить";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(214, 135);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(201, 17);
            this.checkBox2.TabIndex = 5;
            this.checkBox2.Text = "Пустующее муниципальное жилье";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(215, 158);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(147, 17);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Люди = 0 и площадь = 0";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.customEdit1);
            this.groupBox4.Controls.Add(this.button8);
            this.groupBox4.Controls.Add(this.comboBox3);
            this.groupBox4.Location = new System.Drawing.Point(8, 93);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 100);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "УК";
            // 
            // customEdit1
            // 
            this.customEdit1.EditValue = "";
            this.customEdit1.Enabled = false;
            this.customEdit1.Location = new System.Drawing.Point(7, 46);
            this.customEdit1.Name = "customEdit1";
            this.customEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.customEdit1.Properties.DataSource = this.companyBindingSource;
            this.customEdit1.Properties.DisplayMember = "name";
            this.customEdit1.Properties.HideSelection = false;
            this.customEdit1.Properties.IncrementalSearch = true;
            this.customEdit1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null)});
            this.customEdit1.Properties.SelectAllItemVisible = false;
            this.customEdit1.Properties.ShowButtons = false;
            this.customEdit1.Properties.ValueMember = "id_company";
            this.customEdit1.Size = new System.Drawing.Size(187, 20);
            this.customEdit1.TabIndex = 16;
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataMember = "Company";
            this.companyBindingSource.DataSource = this.murmanskDataSet;
            // 
            // murmanskDataSet
            // 
            this.murmanskDataSet.DataSetName = "MurmanskDataSet";
            this.murmanskDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Image = global::MassBillNew.Properties.Resources.green_arrow_down;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(7, 74);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(187, 20);
            this.button8.TabIndex = 1;
            this.button8.Text = "Выбрать все по УК";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // comboBox3
            // 
            this.comboBox3.DataSource = this.companyBindingSource;
            this.comboBox3.DisplayMember = "Name";
            this.comboBox3.DropDownHeight = 300;
            this.comboBox3.DropDownWidth = 500;
            this.comboBox3.Enabled = false;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.IntegralHeight = false;
            this.comboBox3.Location = new System.Drawing.Point(7, 19);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(187, 21);
            this.comboBox3.TabIndex = 0;
            this.comboBox3.ValueMember = "ID_Company";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboBox2);
            this.groupBox3.Location = new System.Drawing.Point(346, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 70);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Период";
            // 
            // comboBox2
            // 
            this.comboBox2.DataSource = this.vPeriodBindingSource;
            this.comboBox2.DisplayMember = "Period_Name";
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(6, 20);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(188, 21);
            this.comboBox2.TabIndex = 0;
            this.comboBox2.ValueMember = "Period";
            this.comboBox2.SelectionChangeCommitted += new System.EventHandler(this.comboBox2_SelectionChangeCommitted);
            // 
            // vPeriodBindingSource
            // 
            this.vPeriodBindingSource.DataMember = "VPeriod";
            this.vPeriodBindingSource.DataSource = this.murmanskDataSet;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Location = new System.Drawing.Point(140, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 70);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Вид квитанции";
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox1.DataSource = this.billKindBindingSource;
            this.comboBox1.DisplayMember = "Bill_Kind";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.DropDownWidth = 300;
            this.comboBox1.Location = new System.Drawing.Point(6, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(188, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.ValueMember = "ID_Bill_Kind";
            this.comboBox1.SelectionChangeCommitted += new System.EventHandler(this.comboBox1_SelectionChangeCommitted);
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            // 
            // billKindBindingSource
            // 
            this.billKindBindingSource.DataMember = "Bill_Kind";
            this.billKindBindingSource.DataSource = this.murmanskDataSet;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Location = new System.Drawing.Point(8, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(126, 70);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тип квитанции";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(7, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(75, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Долговая";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.rb2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(7, 20);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(70, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Текущая";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.rb1_CheckedChanged);
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(3, 199);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(810, 364);
            this.tabControl2.TabIndex = 10;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.textBox2);
            this.tabPage3.Controls.Add(this.splitContainer1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(802, 338);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Адреса";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.Location = new System.Drawing.Point(669, 305);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(127, 28);
            this.button7.TabIndex = 13;
            this.button7.Text = "Очистить";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(6, 305);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(127, 28);
            this.button3.TabIndex = 12;
            this.button3.Text = "Выбрать все";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(139, 312);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Быстрый ввод Л/С";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(355, 306);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(24, 24);
            this.button2.TabIndex = 10;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox2.Location = new System.Drawing.Point(248, 308);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(107, 20);
            this.textBox2.TabIndex = 9;
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.listBox1);
            this.splitContainer1.Size = new System.Drawing.Size(796, 298);
            this.splitContainer1.SplitterDistance = 200;
            this.splitContainer1.TabIndex = 8;
            // 
            // treeView1
            // 
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.FullRowSelect = true;
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.PathSeparator = ", ";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.SelectedNodes = ((System.Collections.Generic.List<System.Windows.Forms.TreeNode>)(resources.GetObject("treeView1.SelectedNodes")));
            this.treeView1.ShowLines = false;
            this.treeView1.Size = new System.Drawing.Size(200, 298);
            this.treeView1.TabIndex = 6;
            this.treeView1.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterExpand);
            this.treeView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseDown);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "rosette.png");
            this.imageList1.Images.SetKeyName(1, "town.png");
            this.imageList1.Images.SetKeyName(2, "city.png");
            this.imageList1.Images.SetKeyName(3, "house.png");
            this.imageList1.Images.SetKeyName(4, "customers1.png");
            this.imageList1.Images.SetKeyName(5, "archives.png");
            this.imageList1.Images.SetKeyName(6, "print.png");
            this.imageList1.Images.SetKeyName(7, "pixel.png");
            // 
            // listBox1
            // 
            this.listBox1.AllowDrop = true;
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.IntegralHeight = false;
            this.listBox1.Location = new System.Drawing.Point(0, 0);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox1.Size = new System.Drawing.Size(592, 298);
            this.listBox1.TabIndex = 7;
            this.listBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox1_DragDrop);
            this.listBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBox1_DragEnter);
            this.listBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBox1_KeyDown);
            this.listBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listBox1_KeyPress);
            this.listBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseDoubleClick);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.parameterFilter2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(802, 338);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Параметры";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // parameterFilter2
            // 
            this.parameterFilter2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parameterFilter2.Location = new System.Drawing.Point(3, 3);
            this.parameterFilter2.Name = "parameterFilter2";
            this.parameterFilter2.Size = new System.Drawing.Size(796, 332);
            this.parameterFilter2.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.previewControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(816, 600);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Просмотр";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // previewControl1
            // 
            this.previewControl1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.previewControl1.Buttons = ((FastReport.PreviewButtons)(((((((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Open) 
            | FastReport.PreviewButtons.Save) 
            | FastReport.PreviewButtons.Email) 
            | FastReport.PreviewButtons.Find) 
            | FastReport.PreviewButtons.Zoom) 
            | FastReport.PreviewButtons.Outline) 
            | FastReport.PreviewButtons.PageSetup) 
            | FastReport.PreviewButtons.Edit) 
            | FastReport.PreviewButtons.Watermark) 
            | FastReport.PreviewButtons.Navigator)));
            this.previewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewControl1.Font = new System.Drawing.Font("Tahoma", 8F);
            this.previewControl1.Location = new System.Drawing.Point(3, 3);
            this.previewControl1.Name = "previewControl1";
            this.previewControl1.PageOffset = new System.Drawing.Point(10, 10);
            this.previewControl1.Size = new System.Drawing.Size(810, 594);
            this.previewControl1.TabIndex = 0;
            this.previewControl1.UIStyle = FastReport.Utils.UIStyle.VisualStudio2005;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(816, 600);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "История печати";
            this.tabPage5.UseVisualStyleBackColor = true;
            this.tabPage5.Enter += new System.EventHandler(this.tabPage5_Enter);
            // 
            // parameterOwnerBindingSource
            // 
            this.parameterOwnerBindingSource.DataMember = "Parameter_Owner";
            this.parameterOwnerBindingSource.DataSource = this.murmanskDataSet;
            // 
            // dataTable2BindingSource1
            // 
            this.dataTable2BindingSource1.DataMember = "DataTable2";
            this.dataTable2BindingSource1.DataSource = this.murmanskDataSet;
            // 
            // parameterDictionaryBindingSource
            // 
            this.parameterDictionaryBindingSource.DataMember = "Parameter_Dictionary";
            this.parameterDictionaryBindingSource.DataSource = this.murmanskDataSet;
            // 
            // vPeriod1BindingSource
            // 
            this.vPeriod1BindingSource.DataMember = "VPeriod1";
            this.vPeriod1BindingSource.DataSource = this.murmanskDataSet;
            // 
            // vPeriodBindingSource1
            // 
            this.vPeriodBindingSource1.DataMember = "VPeriod";
            this.vPeriodBindingSource1.DataSource = this.murmanskDataSet;
            // 
            // dataTable2BindingSource
            // 
            this.dataTable2BindingSource.DataMember = "DataTable2";
            this.dataTable2BindingSource.DataSource = this.murmanskDataSet;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(6, 632);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "Сформировать";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // vPeriodTableAdapter
            // 
            this.vPeriodTableAdapter.ClearBeforeFill = true;
            // 
            // companyTableAdapter
            // 
            this.companyTableAdapter.ClearBeforeFill = true;
            // 
            // bill_KindTableAdapter
            // 
            this.bill_KindTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(311, 640);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // environmentSettings1
            // 
            designerSettings4.ApplicationConnection = null;
            designerSettings4.DefaultFont = new System.Drawing.Font("Arial", 10F);
            designerSettings4.Icon = null;
            designerSettings4.Restrictions = designerRestrictions4;
            designerSettings4.Text = "";
            this.environmentSettings1.DesignerSettings = designerSettings4;
            emailSettings4.Address = "";
            emailSettings4.Host = "";
            emailSettings4.MessageTemplate = "";
            emailSettings4.Name = "";
            emailSettings4.Password = "";
            emailSettings4.UserName = "";
            this.environmentSettings1.EmailSettings = emailSettings4;
            previewSettings4.Buttons = ((FastReport.PreviewButtons)(((((((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Open) 
            | FastReport.PreviewButtons.Save) 
            | FastReport.PreviewButtons.Email) 
            | FastReport.PreviewButtons.Find) 
            | FastReport.PreviewButtons.Zoom) 
            | FastReport.PreviewButtons.Outline) 
            | FastReport.PreviewButtons.PageSetup) 
            | FastReport.PreviewButtons.Edit) 
            | FastReport.PreviewButtons.Watermark) 
            | FastReport.PreviewButtons.Navigator)));
            previewSettings4.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings4.Icon")));
            previewSettings4.Text = "";
            this.environmentSettings1.PreviewSettings = previewSettings4;
            this.environmentSettings1.ReportSettings = reportSettings4;
            this.environmentSettings1.UIStyle = FastReport.Utils.UIStyle.VisualStudio2005;
            this.environmentSettings1.StartProgress += new System.EventHandler(this.environmentSettings1_StartProgress);
            this.environmentSettings1.Progress += new FastReport.ProgressEventHandler(this.environmentSettings1_Progress);
            // 
            // dataTable2TableAdapter
            // 
            this.dataTable2TableAdapter.ClearBeforeFill = true;
            // 
            // vPeriod1TableAdapter
            // 
            this.vPeriod1TableAdapter.ClearBeforeFill = true;
            // 
            // parameter_DictionaryTableAdapter
            // 
            this.parameter_DictionaryTableAdapter.ClearBeforeFill = true;
            // 
            // parameter_OwnerTableAdapter
            // 
            this.parameter_OwnerTableAdapter.ClearBeforeFill = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Настройки фильтров (*.afl)|*.afl|Список идентификаторов (*.txt)|*.txt";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Настройки фильтров (*.afl)|*.afl|Список идентификаторов (*.txt)|*.txt";
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Bill_FilterTableAdapter = null;
            this.tableAdapterManager.Bill_KindTableAdapter = this.bill_KindTableAdapter;
            this.tableAdapterManager.Company1TableAdapter = null;
            this.tableAdapterManager.Company2TableAdapter = null;
            this.tableAdapterManager.House_Derive_CompanyTableAdapter = null;
            this.tableAdapterManager.ModuleTableAdapter = null;
            this.tableAdapterManager.Pack_TypeTableAdapter = null;
            this.tableAdapterManager.Report_MenuTableAdapter = null;
            this.tableAdapterManager.ServiceTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MassBillNew.MurmanskDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // textBox3
            // 
            this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox3.Location = new System.Drawing.Point(663, 635);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(161, 20);
            this.textBox3.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(609, 638);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "ID_Print:";
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button4.Location = new System.Drawing.Point(140, 632);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(165, 28);
            this.button4.TabIndex = 5;
            this.button4.Text = "Сформировать по улицам";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.DefaultExt = "fpx";
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            // 
            // report2
            // 
            this.report2.ReportResourceString = resources.GetString("report2.ReportResourceString");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 663);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Массовая печать (новая)";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.customEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.murmanskDataSet)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vPeriodBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.billKindBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.parameterOwnerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2BindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parameterDictionaryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPeriod1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPeriodBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button button1;
        private FastReport.Preview.PreviewControl previewControl1;
        private MurmanskDataSet murmanskDataSet;
        private System.Windows.Forms.BindingSource vPeriodBindingSource;
        private MassBillNew.MurmanskDataSetTableAdapters.VPeriodTableAdapter vPeriodTableAdapter;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private MassBillNew.MurmanskDataSetTableAdapters.CompanyTableAdapter companyTableAdapter;
        private System.Windows.Forms.BindingSource billKindBindingSource;
        private MassBillNew.MurmanskDataSetTableAdapters.Bill_KindTableAdapter bill_KindTableAdapter;
        private System.Windows.Forms.Label label1;
        private FastReport.EnvironmentSettings environmentSettings1;
        private System.Windows.Forms.ImageList imageList1;
        private MultiSelectTreeview treeView1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.BindingSource dataTable2BindingSource;
        private MassBillNew.MurmanskDataSetTableAdapters.DataTable2TableAdapter dataTable2TableAdapter;
        private System.Windows.Forms.BindingSource dataTable2BindingSource1;
        private System.Windows.Forms.BindingSource vPeriodBindingSource1;
        private System.Windows.Forms.BindingSource vPeriod1BindingSource;
        private MassBillNew.MurmanskDataSetTableAdapters.VPeriod1TableAdapter vPeriod1TableAdapter;
        private System.Windows.Forms.BindingSource parameterDictionaryBindingSource;
        private MassBillNew.MurmanskDataSetTableAdapters.Parameter_DictionaryTableAdapter parameter_DictionaryTableAdapter;
        private System.Windows.Forms.BindingSource parameterOwnerBindingSource;
        private MassBillNew.MurmanskDataSetTableAdapters.Parameter_OwnerTableAdapter parameter_OwnerTableAdapter;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private MassBillNew.MurmanskDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button8;
        private ParameterFilter parameterFilter1;
        private System.Windows.Forms.Button button4;
        private ParameterFilter parameterFilter2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnDebtFilterCodePeriod;
        private System.Windows.Forms.CheckBox chbDebtFilterCodePeriod;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private CustomEdit customEdit1;
        private FastReport.Report report1;
        private FastReport.Report report2;
        private System.Windows.Forms.CheckBox chBxTwoDebtComp;
    }
}

