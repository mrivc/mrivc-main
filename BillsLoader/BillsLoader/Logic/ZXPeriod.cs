﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillsLoader.Logic
{
    using MrivcHelpers.Helpers;

    public class ZXPeriod
    {
        public int month;
        public int year;

        /// <summary>
        /// Создает объект периода из строки "2014,1" (год,месяц)
        /// </summary>
        /// <param name="period"></param>
        public ZXPeriod(string period)
        {
            string y = period.Substring(0, 4);
            string m = period.Substring(5, period.Length-5);

            this.month = int.Parse(m);
            this.year = int.Parse(y);
        }

        public ZXPeriod(int month, int year)
        {
            this.month = month;
            this.year = year;
        }

        public int Month
        {
            get { return month; }
        }

        public int Year
        {
            get { return year; }
        }

        public string AuthcodPeriod {
            get {
                return StringHelper.ZeroCode(month) + StringHelper.ZeroCode(year);
            }
        }
    }
}
