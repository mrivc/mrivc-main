﻿namespace Helper.Controls
{
    partial class SenderIdOnlyFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbSender = new System.Windows.Forms.ComboBox();
            this.gbSender = new System.Windows.Forms.GroupBox();
            this.gbSender.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbSender
            // 
            this.cbSender.FormattingEnabled = true;
            this.cbSender.Location = new System.Drawing.Point(6, 19);
            this.cbSender.Name = "cbSender";
            this.cbSender.Size = new System.Drawing.Size(374, 21);
            this.cbSender.TabIndex = 3;
            // 
            // gbSender
            // 
            this.gbSender.Controls.Add(this.cbSender);
            this.gbSender.Location = new System.Drawing.Point(3, 3);
            this.gbSender.Name = "gbSender";
            this.gbSender.Size = new System.Drawing.Size(386, 50);
            this.gbSender.TabIndex = 3;
            this.gbSender.TabStop = false;
            this.gbSender.Text = "Поставщик информации";
            // 
            // SenderIdOnlyFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbSender);
            this.Name = "SenderIdOnlyFilter";
            this.Size = new System.Drawing.Size(395, 59);
            this.gbSender.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbSender;
        private System.Windows.Forms.GroupBox gbSender;
    }
}
