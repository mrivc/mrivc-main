﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ImportResponses : MrivcImportCommand
    {
        bool _preview = false;

        public ImportResponses(string senderID) : base("ImportResponses", "DebtRequestsServiceAsync", senderID)
        {
            _maxImportValue = 100;
            _importValueName = "action";
            Description = "Загрузка в ГИС ЖКХ запросов о наличии задолженности за ЖКУ (для РСО)";
        }

        protected override void CreateXmlBody()
        {
            var _preparedDataSet = PrepareToDataSet();

            foreach (DataRow payInfo in _preparedDataSet.Tables["importDSR"].Rows)
            {
                var values1 = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "subrequestGUID" },payInfo["subrequestGUID"].ToString()),
                    new Tuple<string[], string>( new [] { "responseData", "hasDebt" },payInfo["hasDebt"].ToString()),
                    new Tuple<string[], string>( new [] { "responseData", "debtInfo", "person", "firstName" },payInfo["firstName"].ToString()),
                    new Tuple<string[], string>( new [] { "responseData", "debtInfo", "person", "lastName" },payInfo["lastName"].ToString()),
                    new Tuple<string[], string>( new [] { "responseData", "debtInfo", "person", "middleName" },payInfo["middleName"].ToString()),
                    new Tuple<string[], string>( new [] { "responseData", "executorGUID" },payInfo["executorGUID"].ToString()),

                };

                AddChildElement(MrivcStatic.GisConfig.Services["DebtRequestsServiceAsync"].Methods["action_add"].Template, values1, "importDSRResponsesRequest");
                CounterCheck();
            }
        }

        public DataSet PrepareToDataSet()
        {
            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.Prepare_Import_DSR_Responses", new object[,] { { "SenderID", SenderID }, { "preview", _preview } }, "importDSR");
        }
    }
}
