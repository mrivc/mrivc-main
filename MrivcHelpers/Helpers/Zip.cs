﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EllisBillFilter.Helpers
{
    using MrivcHelpers.Helpers;

    public class Zip
    {
        public static void Create(List<string> files, string zipFileName)
        {
            try
            {
                using (ZipFile zip = new ZipFile(Encoding.GetEncoding(866)))
                {
                    zip.AddFiles(files, false, "");
                    zip.Save(zipFileName);
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog(string.Format("ОШИБКА: {0}", ex.Message));
                throw;
            }
        }

        public static void CreateFromDirectory(string directory, string zipFileName)
        {
            try
            {
                if (!FileHelper.DirectoryExists(directory))
                    return;

                using (ZipFile zip = new ZipFile(Encoding.GetEncoding(866)))
                {
                    zip.AddDirectory(directory);
                    zip.Save(zipFileName);
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog(string.Format("ОШИБКА: {0}", ex.Message)); 
                throw;
            }

        }
    }
}
