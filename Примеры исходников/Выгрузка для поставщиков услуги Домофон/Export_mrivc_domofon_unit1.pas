{------------------------------�������� �����--------------------------------
----------�������� ��� ���������� ������ �������   --------------------------
---------------------------����� �.�. 25.03.2010-----------------------------
-----------------------------------------------------------------------------}
Unit Export_mrivc_domofon_unit1;
Interface

Uses
  Forms, uMDIChild, dbf, DB, ADODB, JvThread, JvThreadDialog, JvComponentBase,
  Classes, ActnList, StdCtrls, Mask, JvExMask, JvToolEdit, uPeriodFilter,
  ExtCtrls,Dialogs, JvExStdCtrls, JvButton, JvCtrls, JvFooter, Controls, JvExExtCtrls,
  JvExtComponent, uPropertyCollection, uCheckedFilter, uSelectEditFilter,ShellAPI,
  uAddrParam,uTypes, JvStringHolder, JvDialogs, Buttons, JvExButtons, JvBitBtn,
  JvEdit, cxControls, cxContainer, cxEdit, cxCheckBox, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxGroupBox, cxRadioGroup, windows;

Type
  TfrmUSZN = Class(TMDIChildForm)
    AddrFilter: TAddrParamFilter;
    dbf1: TDbf;
    JvSaveDialog1: TJvSaveDialog;
    JvFooter1: TJvFooter;
    JvFooterBtn1: TJvFooterBtn;
    JvFooterBtn2: TJvFooterBtn;
    JvEdit2: TJvEdit;
    JvBitBtn2: TJvBitBtn;
    ADOStoredProc1: TADOStoredProc;
    JvThread1: TJvThread;
    JvThreadSimpleDialog1: TJvThreadSimpleDialog;
    SelectEditFilter1: TSelectEditFilter;
    PeriodFilter1: TPeriodFilter;
    ADOQuery1: TADOQuery;
    Procedure jvThreadExecute(Sender: TObject; Params: Pointer);
    procedure AddrFilterbtnSelectClick(Sender: TObject);
    procedure JvFooterBtn2Click(Sender: TObject);
    procedure AddrFiltercbAllClick(Sender: TObject);
    procedure JvBitBtn2Click(Sender: TObject);
    procedure JvFooterBtn1Click(Sender: TObject);
  Private
    FIsFormed: Boolean;
    FOldDS: Char;
  Public
    Constructor Create(AOwner: TComponent; Options: TPropertyCollection); Reintroduce; Overload;
    Destructor Destroy; Override;
  End;

Implementation

{$R *.dfm}

Uses uReportUtils, SysUtils, dbf_lang,dbf_fields, dbf_dbffile;


Constructor TfrmUSZN.Create(AOwner: TComponent; Options: TPropertyCollection);
Begin
  Inherited Create(AOwner, Options);
  FOldDS := DecimalSeparator;
  DecimalSeparator := '.';
  FIsFormed := False;
  Caption := CaptionByDescription(HInstance);
  AssignDataSetConnections(Self, FConnection);
  ADOStoredProc1.Connection:=FConnection;
  AssignImageList(JvFooter1, FImageList);
  ADOQuery1.Connection:=FConnection;
  AddrFilter.Initialize(FConnection, fo_House);
  PeriodFilter1.Initialize(FConnection);
  SelectEditFilter1.Initialize(FConnection,'SELECT ID_Provider_Group, Provider_Group FROM dbo.Provider_Group ORDER BY Provider_Group');
  //cxDateEdit1.Date:=Now;
End;


Destructor TfrmUSZN.Destroy;
Begin
  AddrFilter.Destroy;
  SelectEditFilter1.Destroy;
  PeriodFilter1.Destroy;
  DecimalSeparator := FOldDS;
  Inherited;
End;

procedure TfrmUSZN.JvBitBtn2Click(Sender: TObject);
  var t:String;
begin
    if Not JvSaveDialog1.Execute then exit;
    JvEdit2.Text:=JvSaveDialog1.FileName;
    t:= JvSaveDialog1.FileName;
    if (t[Length(t)-3]='.')and (LowerCase(t[Length(t)-2])='d')and (LowerCase(t[Length(t)-1])='b')and (LowerCase(t[Length(t)])='f') then begin
        dbf1.TableName:=JvSaveDialog1.FileName;
        JvEdit2.Text:= JvSaveDialog1.FileName;
    end else begin
        dbf1.TableName:=JvSaveDialog1.FileName+'.dbf';
        JvEdit2.Text:= JvSaveDialog1.FileName+'.dbf';
    end;
end;

procedure TfrmUSZN.JvFooterBtn1Click(Sender: TObject);
begin
    if  Length(JvSaveDialog1.FileName)<3 then begin
      ShowMessage('�� ������� ��� �����');
      exit;
    end;
    JvThread1.Execute(nil);
    //jvThreadExecute(nil,nil);
    ShowMessage('�������� ���������');
end;

procedure TfrmUSZN.JvFooterBtn2Click(Sender: TObject);
begin
     close;
end;

procedure TfrmUSZN.AddrFiltercbAllClick(Sender: TObject);
begin
  AddrFilter.cbAllClick(Sender);
end;


procedure TfrmUSZN.AddrFilterbtnSelectClick(Sender: TObject);
begin
     AddrFilter.btnSelectClick(Sender);
end;

Procedure TfrmUSZN.jvThreadExecute(Sender: TObject; Params: Pointer);
Var   i: Integer;
       FieldDefs1: TDbfFieldDefs;
Begin
    if (AddrFilter.GetTableName='') and (AddrFilter.cbAll.Checked=false) then begin
        exit;
    end;


  if AddrFilter.cbAll.Checked then
    ADOQuery1.SQL.Text:=
    'if object_ID(''tempdb..##export_domofon_acc'')is not null DROP TABLE ##export_domofon_acc ' +
    'select id_HOUSE INTO ##export_domofon_acc from dbo.house'
  else
    ADOQuery1.SQL.Text:=
    'if object_ID(''tempdb..##export_domofon_acc'')is not null DROP TABLE ##export_domofon_acc ' +
    'select id as id_House INTO ##export_domofon_acc from '+AddrFilter.GetTableName;

    ADOQuery1.ExecSQL;


  DbfGlobals.DefaultOpenCodePage := 866;
  //Dbf1.LanguageID := DbfLangId_RUS;
  FieldDefs1:=TDbfFieldDefs.Create(dbf1);

  dbf1.TableName:= JvSaveDialog1.FileName;

  FieldDefs1.Clear;


 with FieldDefs1.AddFieldDef  do begin
   FieldName:='LS'; FieldType:=ftString; Size:=12;
 end;
 with FieldDefs1.AddFieldDef  do begin
   FieldName:='ADRES'; FieldType:=ftString; Size:=50;
 end;
with FieldDefs1.AddFieldDef  do begin
   FieldName:='KV'; FieldType:=ftString; Size:=12;
 end;
with FieldDefs1.AddFieldDef  do begin
   FieldName:='MD'; FieldType:=ftString; Size:=12;// Precision:=0;
 end;
with FieldDefs1.AddFieldDef  do begin
   FieldName:='F_name'; FieldType:=ftString; Size:=50;
 end;

 dbf1.CreateTableEx(FieldDefs1);
 ADOStoredProc1.Parameters.ParamByName('@id_Provider_Group').Value:=SelectEditFilter1.GetID();
 ADOStoredProc1.Parameters.ParamByName('@period').Value:=PeriodFilter1.GetPeriod;
 //ADOStoredProc1.ExecProc;
 ADOStoredProc1.Open;
 ADOStoredProc1.First;
 dbf1.Open;
 while NOT ADOStoredProc1.Eof do BEGIN
   dbf1.Insert;
   for i := 0 to ADOStoredProc1.FieldCount - 1 do begin
        dbf1.Fields[i].Value:=ADOStoredProc1.Fields[i].Value;
   end;
   dbf1.Post;
   ADOStoredProc1.Next;
 END;
 dbf1.close;
 ADOStoredProc1.close;

End;

End.

//====== ��������� TDbf ======
//====== ��������� ========
//��� ������� DOS ��������� dbf-����� ���������� ��������� ������:
//- � ''Uses'' �������� ������ ''dbf_dbffile, dbf_lang'' -
//����� ��������� ������� ��������� DOS-��������� <code>
//DbfGlobals.DefaultOpenCodePage := 866;
//Dbf.LanguageID := DbfLangId_RUS


///Provider=SQLOLEDB.1;Password=1;Persist Security Info=True;User ID=tpoto;Initial Catalog=Murmansk2;Data Source=SERVER-SQL
