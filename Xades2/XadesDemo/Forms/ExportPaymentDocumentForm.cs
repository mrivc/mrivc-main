﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo
{
    public partial class ExportPaymentDocumentForm : Form
    {
        public ExportPaymentDocumentForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var hg = MrivcData.HouseFiasGuidList(null);
            foreach (var item in hg)
            {
                    var exportPaymentDocument = new ExportPaymentDocument("", "");
                    exportPaymentDocument.FiasHouseGuid = item;
                    exportPaymentDocument.Execute();
            }
        }

        private void ExportPaymentDocumentForm_Load(object sender, EventArgs e)
        {

        }
    }
}
