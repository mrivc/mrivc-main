﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaymentStatementParser
{
    public partial class ReferenceForm : Form
    {
        public ReferenceForm()
        {
            InitializeComponent();

            ShowIcon = false;
            WindowState = FormWindowState.Maximized;

            webBrowser.IsWebBrowserContextMenuEnabled = false;

            string curDir = Directory.GetCurrentDirectory();
            string html = @"Reference/reference.html";

            webBrowser.Url = new Uri(string.Format("file:///{0}/{1}", curDir, html));
        }
    }
}
