﻿using System;

namespace PrintDeliverySend
{
    using PrintDeliverySend.Helpers;
    using Properties;
    using System.Threading;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            FileHelper.WriteLogEmptyLine();

            BillSender bs = new BillSender();
            bs.SendingFinished += Bs_SendingFinished;
            bs.StartSending();


            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
        }

        private static void Bs_SendingFinished(object sender, EventArgs e)
        {
            var bs = sender as BillSender;
            var logFile = FileHelper.ZipLogFile();
            
            string error = bs.SendMail(Settings.Default.SmtpServerHost,
                   Settings.Default.SmtpServerMailFrom,
                   Settings.Default.SmtpServerPswd,
                   Settings.Default.SendLogsTo,
                   "",
                   string.Format("Отчет по авторассылке квитанций от {0}, всего отправлено {1}, ошибок {2}", DateTime.Now.ToShortDateString(), bs.CountSendedMessages, bs.CountSendedMessagesWithError),
                   string.Format("Отчет по авторассылке квитанций от {0}, всего отправлено {1}, ошибок {2}", DateTime.Now.ToShortDateString(), bs.CountSendedMessages, bs.CountSendedMessagesWithError),
                   FileHelper.ExistFile(logFile) ? logFile : string.Empty);
            Thread.Sleep(500);
        }
    }
}
