﻿using System;
using System.Collections.Generic;
using Xades.Abstractions;
using Xades.Helpers;
using GisGkh.Configurations.Options;
using GisGkh.Configurations.Sections;

namespace GisGkh.Commands
{
    public class VerifyCommand : XadesCommandBase<VerifyOptions>
    {
        public VerifyCommand(VerifyOptions option, IXadesService xadesService, SigningConfiguration signingConfig) : base(option, xadesService, signingConfig)
        {
        }

        protected override string OnExecute(VerifyOptions option, string xmlBody, string senderID)
        {
            Info(string.Format("Выполняется чтение файла {0}...", option.InputFileName));
            var xmlDocument = XmlDocumentHelper.Load(option.InputFileName);
            var elementId = option.Element;

            Info("Проверка подписи файла...");
            Validate(xmlDocument, elementId);
            Success("Подпись элемента верна");
            return "";
        }
    }
}