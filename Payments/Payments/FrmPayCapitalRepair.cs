﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Payments.Controls;
using Payments.DS;
using Payments.LogicClasses;

namespace Payments
{
    public partial class FrmPayCapitalRepair : Form, IPayForm
    {
        PaymentClass paymentClass;

        private Client client;

        public FrmPayCapitalRepair(PaymentClass paymentClass)
        {
            this.paymentClass = paymentClass;
            InitializeComponent();


            this.client = new Client("9999");
            panelPayment1.SetCommissionRate(client.CommissionRate, client.CommissionMinSumm);
        }

        public PaymentClass PaymentClass
        {
            get { return paymentClass; }
        }

        public PanelPayment PaymentInfo
        {
            get
            {
                return panelPayment1;
            }
        }

        public Form Window
        {
            get { return this; }
        }

        public void SavePaymentDetail()
        {
            if (paymentClass.IdPayment == -1)
                throw new Exception("Нельзя сохранить делали платежа в таблицу \"dbo.Payment_Parameter_Detail\", т.к. идентификатор платежа ID_Payment = -1");
            gridPaymentParameter1.SaveData();
        }

        private bool panelPayment1_PayEvent(PanelPayment sender, EventArgs e)
        {
            if (!ValidatePayment())
                return false;

            //если оплата кредиткой
            if (sender.CashType == 2)
            {
                return PayByVisaCard();
            }

            List<string> freeStrings = new List<string>();

            freeStrings.Add("ПОЛУЧАТЕЛЬ: " + client.PayeerName);
            freeStrings.Add("ИНН: " + txtBarcode.Text.Substring(0,10));
            freeStrings.Add("КПП: " + client.Kpp);
            freeStrings.Add("СЧЕТ: " + client.Bill);
            freeStrings.Add("БАНК: ");
            freeStrings.Add(client.BankName);
            freeStrings.Add("");
            freeStrings.Add("Штрих-код: " + txtBarcode.Text);
            freeStrings.Add("");

            if (gridPaymentParameter1.GetValue("ADDRESS").ToString().Length > 27)
            {
                freeStrings.Add("Адрес: " + gridPaymentParameter1.GetValue("ADDRESS").ToString().Substring(0, 27));
                freeStrings.Add("" + gridPaymentParameter1.GetValue("ADDRESS").ToString().Substring(27));
            }
            else
                freeStrings.Add("Адрес: " + gridPaymentParameter1.GetValue("ADDRESS"));

            freeStrings.Add("Сумма платежа: " + panelPayment1.PaymentSumm.ToString());
            freeStrings.Add("Комиссия с платежа: " + panelPayment1.PaymentSummCommission.ToString());

            freeStrings.Add("");

            string ofdProductName = StringHelper.GenerateOfdProductNameCapitalRepair(txtBarcode.Text);

            if (DAccess.CashTerminal.Sale(panelPayment1.PaymentSummTotal, panelPayment1.PaymentCash, 1,
                paymentClass.Name, freeStrings, ofdProductName, panelPayment1.PhoneEmail, Properties.Settings.Default.Department_Number, paymentClass.MakeCheckCopy))
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
                return true;
            }

            return false;
        }

        private bool PayByVisaCard()
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
            return true;
        }

        DataTable paymentVerificationinfo;
        private void ReadBarCode()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                paymentVerificationinfo = DAccess.DataModule.GetPaymentVerificationInfo(txtBarcode.Text.Replace(" ", ""));

                if (!ValidatePayment())
                {
                    this.Cursor = Cursors.Default;
                    return;
                }

                panelPayment1.PaymentSumm = (paymentVerificationinfo.Rows[0]["SummPay"] != DBNull.Value
                    ? (decimal) paymentVerificationinfo.Rows[0]["SummPay"]
                    : 0);

                gridPaymentParameter1.SetValue("ACCOUNT", paymentVerificationinfo.Rows[0]["Account"].ToString());
                gridPaymentParameter1.SetValue("ADDRESS", paymentVerificationinfo.Rows[0]["Address"].ToString());
                gridPaymentParameter1.SetValue("BARCODE", paymentVerificationinfo.Rows[0]["Barcode"].ToString());
                gridPaymentParameter1.SetValue("CLIENT_ID", client.CompanyId.ToString());
            }
            catch (Exception ex)
            {
                panelPayment1.SetAllowPayment(false);
                MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private bool ValidatePayment()
        {
            if (paymentVerificationinfo == null || paymentVerificationinfo.Rows.Count == 0)
            {
                MessageBox.Show("По данному коду штрих-коду нет информации в базе данных. Прием платежа запрещен.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                panelPayment1.SetAllowPayment(false);
                return false;
            }

            panelPayment1.SetAllowPayment(true);
            return true;
        }

        private void FrmPayCapitalRepair_Shown(object sender, EventArgs e)
        {
            gridPaymentParameter1.LoadLayout("gridPaymentParameterCR");
            gridPaymentParameter1.LoadData(paymentClass);
            gridPaymentParameter1.SetColumnWidth("Display_Name", 180);
        }

        private void txtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
            {
                if ((txtBarcode.Text.Replace(" ", "")).Length != 39)
                {
                    MessageBox.Show("Штрих-код указан неверно. Длина штрих-кода должна быть 39 знаков.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ReadBarCode();
                panelPayment1.SetDefaultFocus();
            }
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            if ((txtBarcode.Text.ToString().Replace(" ", "")).Length == 39)
            {
                ReadBarCode();
                panelPayment1.SetDefaultFocus();
            }
        }

        private void panelPayment1_PaymentSummChangedEvent(PanelPayment sender, EventArgs e)
        {
            gridPaymentParameter1.SetValue("COMMISSION_SUMM", Math.Round(sender.PaymentSummCommission, 2).ToString());
        }
    }
}
