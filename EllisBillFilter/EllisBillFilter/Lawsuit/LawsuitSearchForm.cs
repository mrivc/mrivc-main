﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace EllisBillFilter.Lawsuit
{
    using DS;

    public partial class LawsuitSearchForm : Form
    {
        private DataTable searchResult;

        public LawsuitSearchForm()
        {
            InitializeComponent();

            try
            {
                init();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void refresh()
        {
            mDataSet.LawsuitSearch.Clear();

            searchResult = mDataSet.LawsuitSearch.Copy();

            DAccess.DataModule.FillLawsuitSearchTable(mDataSet.LawsuitSearch);

            runSearch();

            dataGridView.DataSource = searchResult;
        }

        private void dataGridInit()
        {
            dataGridView.Columns["ID_Account"].Visible = false;
            dataGridView.Columns["ID_Lawsuit"].Visible = false;

            dataGridView.Columns["account"].HeaderText = "ЛС";
            dataGridView.Columns["address"].HeaderText = "Адрес";
            dataGridView.Columns["case_number"].HeaderText = "Номер дела";
            dataGridView.Columns["list_number"].HeaderText = "Исполнительный лист";
            dataGridView.Columns["stady"].HeaderText = "Стадия рассмотрения";
            dataGridView.Columns["FIO"].HeaderText = "ФИО";
            dataGridView.Columns["ratio"].HeaderText = "Доля";
            dataGridView.Columns["company"].HeaderText = "Управляющая компания";
            dataGridView.Columns["Date_Debt_Begin"].HeaderText = "Начало периода взыскания";
            dataGridView.Columns["Date_Debt_End"].HeaderText = "Окончание периода взыскания";
            dataGridView.Columns["Sum"].HeaderText = "Сумма иска";
            dataGridView.Columns["Sum_Fine"].HeaderText = "Пени";
            dataGridView.Columns["Legal_Costs"].HeaderText = "Гос. пошлина";
            dataGridView.Columns["Date_Proceedings_Begin"].HeaderText = "Дата принятия СП";
            dataGridView.Columns["Date_Proceedings_End"].HeaderText = "Дата отмены СП";
            dataGridView.Columns["court"].HeaderText = "Судебный орган";

            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void init()
        {
            ContextMenuStrip cMenuStrip = new ContextMenuStrip();

            ToolStripMenuItem updateRow = new ToolStripMenuItem("Редактировать");

            cMenuStrip.Items.AddRange(new[] { updateRow });

            dataGridView.ContextMenuStrip = cMenuStrip;

            updateRow.Click += UpdateMenu_Click;

            refresh();

            dataGridInit();
        }

        private void runSearch()
        {
            try
            {
                string caseText = caseTextBox.Text.Replace("*", @"(\w|\W)*");
                string fullnameText = fullnameTextBox.Text.Replace("*", @"(\w|\W)*");
                string address = addressTextBox.Text.Replace("*", @"(\w|\W)*");

                search(new SearchRatio[] {
                    new SearchRatio("case_number", $@"(\w|\W)*{caseText}(\w|\W)*"),
                    new SearchRatio("FIO", $@"(\w|\W)*{fullnameText}(\w|\W)*"),
                    new SearchRatio("address", $@"(\w|\W)*{address}(\w|\W)*")
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void search(SearchRatio[] searchRatios)
        {
            DataTable temp = searchResult.Clone();
            dataGridView.DataSource = temp;

            searchResult.Clear();

            foreach (DataRow row in mDataSet.LawsuitSearch.Rows)
            {
                bool isMatch = true;

                foreach (SearchRatio searchRatio in searchRatios)
                {
                    string rowText = row[searchRatio.Column].ToString();

                    if (!Regex.IsMatch(rowText, searchRatio.Pattern, RegexOptions.IgnoreCase)) isMatch = false;
                }

                if (isMatch) searchResult.Rows.Add(row.ItemArray);
            }

            dataGridView.DataSource = searchResult;
        }

        private void UpdateMenu_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedIndex = dataGridView.SelectedRows[0].Index;

                DataRow selectedRow = searchResult.Rows[selectedIndex];

                int idAccount = int.Parse(selectedRow["ID_Account"].ToString());
                int idLawsuit = int.Parse(selectedRow["ID_Lawsuit"].ToString());

                Form editForm = new LawsuitEditForm(idAccount, DAccess.Instance.WorkPeriod, idLawsuit);
                editForm.ShowDialog();

                refresh();

                if (selectedIndex < dataGridView.Rows.Count)
                {
                    dataGridView.Rows[selectedIndex].Selected = true;
                }
                else if (dataGridView.Rows.Count > 0)
                {
                    int lastIndex = dataGridView.Rows.Count - 1;
                    dataGridView.Rows[lastIndex].Selected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            runSearch();
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            caseTextBox.Text = "";
            fullnameTextBox.Text = "";
            addressTextBox.Text = "";

            runSearch();
        }

        private void dataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            (e.Control as TextBox).ReadOnly = true;
        }
    }
}
