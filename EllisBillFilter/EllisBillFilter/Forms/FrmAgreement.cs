﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using EllisBillFilter.DS;

namespace EllisBillFilter
{
    public partial class FrmAgreement : Form
    {
        private bool initializedGridData;
        private bool allowEdit;
        private MDataSet.AgreementRow agreementData;

        public FrmAgreement(bool allowEdit)
        {
            this.allowEdit = allowEdit;
            initializedGridData = false;
            InitializeComponent();
            gridAgreement1.GridViewAgreement.DoubleClick += new EventHandler(GridViewAgreement_DoubleClick);
        }

        void GridViewAgreement_DoubleClick(object sender, EventArgs e)
        {
            if (gridAgreement1.GridViewAgreement.FocusedColumn.ReadOnly)
            {
                agreementData = (MDataSet.AgreementRow)gridAgreement1.GridViewAgreement.GetDataRow(gridAgreement1.GridViewAgreement.FocusedRowHandle);
                if (agreementData.ID_Agreement == -1)
                {
                    return;
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
                return;
            }

            if (!allowEdit)
            {
                agreementData = (MDataSet.AgreementRow)gridAgreement1.GridViewAgreement.GetDataRow(gridAgreement1.GridViewAgreement.FocusedRowHandle);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
        }

        private void FrmAgreement_Load(object sender, EventArgs e)
        {
            deBeginDate.DateTime = new DateTime(2008, 1, 1); //DateTime.Now.AddMonths(-3);
            deEndDate.DateTime = DateTime.Now;

            gridAgreement1.LoadData(deBeginDate.DateTime, deEndDate.DateTime, allowEdit);
            initializedGridData = true;
        }

        public MDataSet.AgreementRow AgreementData
        {
            get
            {
                return agreementData;
            }
        }

        private void deBeginDate_EditValueChanged(object sender, EventArgs e)
        {
            if(initializedGridData)
                gridAgreement1.ReLoadData(deBeginDate.DateTime, deEndDate.DateTime);
        }

        private void deEndDate_EditValueChanged(object sender, EventArgs e)
        {
            if (initializedGridData)
                gridAgreement1.ReLoadData(deBeginDate.DateTime, deEndDate.DateTime);
        }

        private void txtFilterAgent_TextChanged(object sender, EventArgs e)
        {
            if (txtFilterAgent.Text.Length > 1)
            {
                gridAgreement1.GridViewAgreement.Columns["ID_Agent1"].ClearFilter();
                gridAgreement1.GridViewAgreement.ActiveFilter.Add(new ViewColumnFilterInfo(gridAgreement1.GridViewAgreement.Columns["ID_Agent1"], new DevExpress.XtraGrid.Columns.ColumnFilterInfo("[ID_Agent1] like '%" + txtFilterAgent.Text + "%'")));
            }
            else
                gridAgreement1.GridViewAgreement.Columns["ID_Agent1"].ClearFilter();
        }

        private void txtFilterPrincipal_TextChanged(object sender, EventArgs e)
        {
            if (txtFilterPrincipal.Text.Length > 1)
            {
                gridAgreement1.GridViewAgreement.Columns["ID_Agent2"].ClearFilter();
                gridAgreement1.GridViewAgreement.ActiveFilter.Add(new ViewColumnFilterInfo(gridAgreement1.GridViewAgreement.Columns["ID_Agent2"], new DevExpress.XtraGrid.Columns.ColumnFilterInfo("[ID_Agent2] like '%" + txtFilterPrincipal.Text + "%'")));
            }
            else
                gridAgreement1.GridViewAgreement.Columns["ID_Agent2"].ClearFilter();
        }

        private void txtFilterNumber_TextChanged(object sender, EventArgs e)
        {
            if (txtFilterNumber.Text.Length > 1)
            {
                gridAgreement1.GridViewAgreement.Columns["Agreement_Number"].ClearFilter();
                gridAgreement1.GridViewAgreement.ActiveFilter.Add(new ViewColumnFilterInfo(gridAgreement1.GridViewAgreement.Columns["Agreement_Number"], new DevExpress.XtraGrid.Columns.ColumnFilterInfo("[Agreement_Number] like '%" + txtFilterNumber.Text + "%'")));
            }
            else
                gridAgreement1.GridViewAgreement.Columns["Agreement_Number"].ClearFilter();
        }
    }
}
