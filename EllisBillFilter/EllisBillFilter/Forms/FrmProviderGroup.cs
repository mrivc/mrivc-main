﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using EllisBillFilter.DS;

    public partial class FrmProviderGroup : Form
    {
        public FrmProviderGroup()
        {
            InitializeComponent();
            gridProviderGroup1.GridView.DoubleClick += GridView_DoubleClick;
        }

        public MDataSet.ProviderGroupRow SelectedProviderGroupRow
        {
            get
            {
                return gridProviderGroup1.SelectedProviderGroupRow;
            }
        }

        void GridView_DoubleClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void FrmProviderGroup_Load(object sender, EventArgs e)
        {
            gridProviderGroup1.LoadData();
        }
    }
}
