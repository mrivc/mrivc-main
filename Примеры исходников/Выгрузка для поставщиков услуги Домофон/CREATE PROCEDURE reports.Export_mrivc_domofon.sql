SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�.
-- Create date: 24.03.2010
-- Description:	�������� ��������� ��� �������� �� ���������. ����� Export_mrivc_domofon.dll
-- =============================================
ALTER PROCEDURE [reports].[Export_mrivc_domofon]
					@id_Provider_Group INT,
					@period INT = null
AS
BEGIN
					--11	�������
					if @period is null SET @Period = dbo.Work_period()
					select AA.Account LS,
										town.Address(default,default,default,Street,House_Number, 
															House_Liter, House_Block, House_Building,default,default,default) ADRES,
										dbo.Address(default,default,default,default,flat_number, Flat_liter) KV,
										COALESCE(rooms,'') MD,
										COALESCE(Reports.Account_Owner_List_Export_Domofon(AA.id_Account,@period),'') F_name
					
					FROM ##export_domofon_acc E
					LEFT JOIN dbo.VAccount_Address_All aa
										ON AA.id_House = E.ID_House
					INNER JOIN dbo.VAccount_Service_provider ASP
										ON ASP.id_Account = AA.id_Account 
															AND ASP.id_Service = 11
															AND (ASP.End_date>=dbo.PeriodBeginDate(@period) or ASP.End_date is NULL)
															AND id_Provider_Group=@id_Provider_Group

					ORDER BY Street,House_Number,House_Liter,House_Block,House_Building,flat_number,Flat_liter
	

END