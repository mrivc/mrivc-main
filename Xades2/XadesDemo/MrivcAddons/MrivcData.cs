﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class MrivcData
    {
        public const string ConnectionString = "Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Integrated Security=false;User=butorin;Password=sr4kx54";

        public int[] NsiRegisterNumbers()
        {
            var res = DAccess.DataModule.GetDataTableByQueryCommand("select RegistryNumber from GIS.NsiList");
            var regNumbers = new int[res.Rows.Count];
            for (int i = 0; i < regNumbers.Length; i++)
            {
                regNumbers[i] = (int)res.Rows[i][0];
            }
            return regNumbers;
        }

        public static IEnumerable<string> HouseFiasGuidList(object SenderID)
        {
            var houseGuidTable = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.HouseGuidListBySenderID @SenderID", new object[,] { { "@SenderID", SenderID } });
            //return houseGuidTable;
            return from gu in houseGuidTable.AsEnumerable() select gu[0].ToString();
        }

        public static void CreateTempExportHouseTables()
        {
            DAccess.DataModule.ExecuteNonQueryCommand1(DAccess.DataModule.ExecuteScalarStoredProcedure1("GIS.CreateTempExportHouseTablesSql").ToString());
        }

        public static void CreateTempExportAccountTables()
        {
            DAccess.DataModule.ExecuteNonQueryCommand1(DAccess.DataModule.ExecuteScalarStoredProcedure1("GIS.CreateTempExportAccountTablesSql").ToString());
        }

        public static void CreateTempExportMeterTables()
        {
            DAccess.DataModule.ExecuteNonQueryCommand1(DAccess.DataModule.ExecuteScalarStoredProcedure1("GIS.CreateTempExportMeterTablesSql").ToString());
        }

        public static void CreateTempExportBillTables()
        {
            DAccess.DataModule.ExecuteNonQueryCommand1(DAccess.DataModule.ExecuteScalarStoredProcedure1("GIS.CreateTempExportBillTablesSql").ToString());
        }

        public static void CreateTempExportMeasureTables()
        {
            DAccess.DataModule.ExecuteNonQueryCommand1(DAccess.DataModule.ExecuteScalarStoredProcedure1("GIS.CreateTempExportMeasureTablesSql").ToString());
        }

        public static List<string> SenderList(string senderIdValues)
        {
            var dt = DAccess.DataModule.GetDataTableByQueryCommand1("select distinct senderID from gis.provider where @senderIdValues is null or senderid in (select * from dbo.list_to_table2(@senderIdValues,default))", new[,] { { "senderIdValues", senderIdValues } });
            var list = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }
            return list;
        }

        public static List<string> SenderList()
        {
            var dt = DAccess.DataModule.GetDataTableByQueryCommand1("select distinct lower(senderID) senderID,isRso from gis.vprovider order by isRso");
            var list = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }
            return list;
        }
    }
}
