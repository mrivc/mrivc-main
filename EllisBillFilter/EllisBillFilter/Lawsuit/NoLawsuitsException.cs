﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EllisBillFilter.Lawsuit
{
    public class NoLawsuitsException : Exception
    {
        public NoLawsuitsException(string message) : base(message) { }
    }
}
