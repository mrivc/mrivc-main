﻿namespace EllisBillFilter.Controls
{
    partial class GridDynamicDataImport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.cbRunInTestMode = new System.Windows.Forms.CheckBox();
            this.btnShowScript = new System.Windows.Forms.Button();
            this.dateCreationPicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDataCheck = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ddDynamicDataImport = new EllisBillFilter.BaseDropDown();
            this.ddDynamicDataImportView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.gridDataPreview1 = new EllisBillFilter.Controls.GridDataPreview();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddDynamicDataImport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddDynamicDataImportView)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridDataPreview1);
            this.splitContainer1.Size = new System.Drawing.Size(867, 463);
            this.splitContainer1.SplitterDistance = 359;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.cbRunInTestMode);
            this.splitContainer2.Panel1.Controls.Add(this.btnShowScript);
            this.splitContainer2.Panel1.Controls.Add(this.dateCreationPicker);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.btnDataCheck);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.ddDynamicDataImport);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.txtNotes);
            this.splitContainer2.Size = new System.Drawing.Size(359, 463);
            this.splitContainer2.SplitterDistance = 373;
            this.splitContainer2.TabIndex = 0;
            // 
            // cbRunInTestMode
            // 
            this.cbRunInTestMode.AutoSize = true;
            this.cbRunInTestMode.Location = new System.Drawing.Point(11, 76);
            this.cbRunInTestMode.Name = "cbRunInTestMode";
            this.cbRunInTestMode.Size = new System.Drawing.Size(298, 17);
            this.cbRunInTestMode.TabIndex = 6;
            this.cbRunInTestMode.Text = "проверить загрузку данных только для одной записи";
            this.cbRunInTestMode.UseVisualStyleBackColor = true;
            // 
            // btnShowScript
            // 
            this.btnShowScript.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnShowScript.Location = new System.Drawing.Point(86, 344);
            this.btnShowScript.Name = "btnShowScript";
            this.btnShowScript.Size = new System.Drawing.Size(120, 23);
            this.btnShowScript.TabIndex = 5;
            this.btnShowScript.Text = "Показать скрипт";
            this.btnShowScript.UseVisualStyleBackColor = true;
            this.btnShowScript.Click += new System.EventHandler(this.btnShowScript_Click);
            // 
            // dateCreationPicker
            // 
            this.dateCreationPicker.Location = new System.Drawing.Point(98, 41);
            this.dateCreationPicker.Name = "dateCreationPicker";
            this.dateCreationPicker.Size = new System.Drawing.Size(243, 20);
            this.dateCreationPicker.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Дата загрузки:";
            // 
            // btnDataCheck
            // 
            this.btnDataCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDataCheck.Location = new System.Drawing.Point(5, 344);
            this.btnDataCheck.Name = "btnDataCheck";
            this.btnDataCheck.Size = new System.Drawing.Size(75, 23);
            this.btnDataCheck.TabIndex = 2;
            this.btnDataCheck.Text = "Проверить данные";
            this.btnDataCheck.UseVisualStyleBackColor = true;
            this.btnDataCheck.Click += new System.EventHandler(this.btnDataCheck_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Запрос:";
            // 
            // ddDynamicDataImport
            // 
            this.ddDynamicDataImport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddDynamicDataImport.EditValue = "";
            this.ddDynamicDataImport.Location = new System.Drawing.Point(64, 12);
            this.ddDynamicDataImport.Name = "ddDynamicDataImport";
            this.ddDynamicDataImport.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddDynamicDataImport.Properties.NullText = "";
            this.ddDynamicDataImport.Properties.PopupFormSize = new System.Drawing.Size(400, 200);
            this.ddDynamicDataImport.Properties.View = this.ddDynamicDataImportView;
            this.ddDynamicDataImport.Size = new System.Drawing.Size(277, 20);
            this.ddDynamicDataImport.TabIndex = 0;
            this.ddDynamicDataImport.EditValueChanged += new System.EventHandler(this.ddDynamicDataImport_EditValueChanged);
            // 
            // ddDynamicDataImportView
            // 
            this.ddDynamicDataImportView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.ddDynamicDataImportView.Name = "ddDynamicDataImportView";
            this.ddDynamicDataImportView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.ddDynamicDataImportView.OptionsView.ShowColumnHeaders = false;
            this.ddDynamicDataImportView.OptionsView.ShowGroupPanel = false;
            this.ddDynamicDataImportView.OptionsView.ShowIndicator = false;
            // 
            // txtNotes
            // 
            this.txtNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNotes.Location = new System.Drawing.Point(0, 0);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.ReadOnly = true;
            this.txtNotes.Size = new System.Drawing.Size(357, 84);
            this.txtNotes.TabIndex = 0;
            // 
            // gridDataPreview1
            // 
            this.gridDataPreview1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDataPreview1.Location = new System.Drawing.Point(0, 0);
            this.gridDataPreview1.Name = "gridDataPreview1";
            this.gridDataPreview1.Size = new System.Drawing.Size(504, 463);
            this.gridDataPreview1.TabIndex = 0;
            // 
            // GridDynamicDataImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "GridDynamicDataImport";
            this.Size = new System.Drawing.Size(867, 463);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddDynamicDataImport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddDynamicDataImportView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private GridDataPreview gridDataPreview1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private BaseDropDown ddDynamicDataImport;
        private DevExpress.XtraGrid.Views.Grid.GridView ddDynamicDataImportView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Button btnDataCheck;
        private System.Windows.Forms.DateTimePicker dateCreationPicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnShowScript;
        private System.Windows.Forms.CheckBox cbRunInTestMode;
    }
}
