﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GisGkh
{
    public class ExportPaymentDocument : MrivcExportCommand, ISingleHouseCommand
    {
        public string FiasHouseGuid { get; set; }

        public string Period { get; set; }
        public bool NoAccounts { get; private set; } = false;

        public ExportPaymentDocument(string senderID, string period) : base("exportPaymentDocument", "BillsAsync", senderID, "exportPaymentDocResult")
        {
            Period = period;
           
        }

        public void ExecuteToTempDB()
        {
            MrivcData.CreateTempExportBillTables();

            var month = DAccess.DataModule.GetDataTableByQueryCommand1("select month, year from dbo.period where period = @period", new[,] { { "period", Period.ToString() } });

            var accounts = DAccess.DataModule.GetDataTableByQueryCommand1($@"select distinct UnifiedAccountNumber from GIS.AccountBillList (@senderID , @houseGuid , @period)", new[,] { { "senderID", SenderID }, { "houseGuid", FiasHouseGuid }, { "period", Period.ToString() } });

            NoAccounts = false;

            if (accounts.Rows.Count != 0)
            {
                var values = new List<Tuple<string[], string>>
                {
                            new Tuple<string[], string> (new [] { "Year" },month.Rows[0][1].ToString()),
                            new Tuple<string[], string> (new [] { "Month" },month.Rows[0][0].ToString()),
                            //new Tuple<string[], string> (new [] { "FIASHouseGuid" },FiasHouseGuid),
                };

                Values = values;

                foreach (DataRow row in accounts.Rows)
                {
                    var values1 = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string> (new [] { "UnifiedAccountNumber" },row["UnifiedAccountNumber"].ToString())
                        //new Tuple<string[], string> (new [] { "AccountNumber" },row["AccountNumber"].ToString())
                    };

                    AddChildElement(MrivcStatic.GisConfig.Services[Service].Methods["exportPaymentDocument_account_add"].Template, values1, "exportPaymentDocumentRequest");
                }

                base.ExecuteToTempDB(_exportNodeName);
                if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
                {
                    DAccess.DataModule.ExecuteNonQueryCommand1("exec GIS.ExportPaymentDocumentData");
                    DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.SaveExportedBillData", new[,] { { "SenderID", SenderID }, { "FIASHouseGUID", FiasHouseGuid }, { "@period", Period } });
                }
                
            }
            else
            {
                NoAccounts = true;
            }

            
        }

        public new void Execute()
        {
             this.ExecuteToTempDB();          
        }

        public DataSet PrepareToDataSet()
        {
            return new DataSet();
        }

    }
}
