﻿namespace Payments.DS
{
    partial class DModule
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.sqlPaymentConnection = new System.Data.SqlClient.SqlConnection();
            this.sqlEllisConnection = new System.Data.SqlClient.SqlConnection();
            this.cmdProgramSettingsSelectByCode = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand2 = new System.Data.SqlClient.SqlCommand();
            this.daPaymentClass = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand3 = new System.Data.SqlClient.SqlCommand();
            this.daPaymentParameterDetail = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand5 = new System.Data.SqlClient.SqlCommand();
            this.daPaymentInfo = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlConnection2 = new System.Data.SqlClient.SqlConnection();
            this.sqlSelectCommand6 = new System.Data.SqlClient.SqlCommand();
            this.daFilter_Client = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand7 = new System.Data.SqlClient.SqlCommand();
            this.daPaymentEllisReestr = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlCommand1 = new System.Data.SqlClient.SqlCommand();
            this.cmdClearDb = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand2 = new System.Data.SqlClient.SqlCommand();
            this.daUser = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand8 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand4 = new System.Data.SqlClient.SqlCommand();
            this.daPersonIdentification = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand9 = new System.Data.SqlClient.SqlCommand();
            this.daPersonIdentificationSearch = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand10 = new System.Data.SqlClient.SqlCommand();
            this.daPaymentVerification = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdRegistryVerificationLineInsert = new System.Data.SqlClient.SqlCommand();
            this.cmdRegistryVerification = new System.Data.SqlClient.SqlCommand();
            this.cmdRegistryVerificationSetLoadedStatus = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand1 = new System.Data.SqlClient.SqlCommand();
            this.daPayment = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            this.sqlSelectCommand11 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand5 = new System.Data.SqlClient.SqlCommand();
            this.daPaymentTxtReestr = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand12 = new System.Data.SqlClient.SqlCommand();
            this.daCashType = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlConnection3 = new System.Data.SqlClient.SqlConnection();
            // 
            // sqlPaymentConnection
            // 
            this.sqlPaymentConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlEllisConnection
            // 
            this.sqlEllisConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // cmdProgramSettingsSelectByCode
            // 
            this.cmdProgramSettingsSelectByCode.CommandText = "sp_Program_Settings_SelectByCode";
            this.cmdProgramSettingsSelectByCode.CommandTimeout = 0;
            this.cmdProgramSettingsSelectByCode.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdProgramSettingsSelectByCode.Connection = this.sqlPaymentConnection;
            this.cmdProgramSettingsSelectByCode.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Code", System.Data.SqlDbType.VarChar)});
            // 
            // sqlSelectCommand2
            // 
            this.sqlSelectCommand2.CommandText = "dbo.sp_Payment_Class_Select";
            this.sqlSelectCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand2.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // daPaymentClass
            // 
            this.daPaymentClass.SelectCommand = this.sqlSelectCommand2;
            this.daPaymentClass.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_Payment_Class_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Payment_Class", "ID_Payment_Class"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("MakeCheckCopy", "MakeCheckCopy")})});
            // 
            // sqlSelectCommand4
            // 
            this.sqlSelectCommand4.CommandText = "dbo.sp_Payment_Parameter_Detail_Select";
            this.sqlSelectCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand4.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Payment_Class", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4)});
            // 
            // sqlInsertCommand3
            // 
            this.sqlInsertCommand3.CommandText = "dbo.sp_Payment_Parameter_Detail_Insert";
            this.sqlInsertCommand3.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand3.Connection = this.sqlPaymentConnection;
            this.sqlInsertCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4, "ID_Payment"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Code", System.Data.SqlDbType.VarChar, 100, "Code"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Value", System.Data.SqlDbType.VarChar, 250, "Parameter_Value"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Quantity", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Parameter_Quantity", System.Data.DataRowVersion.Current, ""),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4, "CreatedBy")});
            // 
            // sqlUpdateCommand3
            // 
            this.sqlUpdateCommand3.CommandText = "dbo.sp_Payment_Parameter_Detail_Update";
            this.sqlUpdateCommand3.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand3.Connection = this.sqlPaymentConnection;
            this.sqlUpdateCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4, "ID_Payment"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Code", System.Data.SqlDbType.VarChar, 100, "Code"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Value", System.Data.SqlDbType.VarChar, 250, "Parameter_Value"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Quantity", System.Data.SqlDbType.Int, 4, "Parameter_Quantity"),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4, "CreatedBy")});
            // 
            // sqlDeleteCommand3
            // 
            this.sqlDeleteCommand3.CommandText = "dbo.sp_Payment_Parameter_Detail_Delete";
            this.sqlDeleteCommand3.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand3.Connection = this.sqlPaymentConnection;
            this.sqlDeleteCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4, "ID_Payment"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Code", System.Data.SqlDbType.VarChar, 100, "Code"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Value", System.Data.SqlDbType.VarChar, 250, "Parameter_Value"),
            new System.Data.SqlClient.SqlParameter("@Parameter_Quantity", System.Data.SqlDbType.Int, 4, "Parameter_Quantity"),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4, "CreatedBy")});
            // 
            // daPaymentParameterDetail
            // 
            this.daPaymentParameterDetail.DeleteCommand = this.sqlDeleteCommand3;
            this.daPaymentParameterDetail.InsertCommand = this.sqlInsertCommand3;
            this.daPaymentParameterDetail.SelectCommand = this.sqlSelectCommand4;
            this.daPaymentParameterDetail.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_Payment_Parameter_Detail_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Payment", "ID_Payment"),
                        new System.Data.Common.DataColumnMapping("Code", "Code"),
                        new System.Data.Common.DataColumnMapping("Display_Name", "Display_Name"),
                        new System.Data.Common.DataColumnMapping("ID_Parameter_Type", "ID_Parameter_Type"),
                        new System.Data.Common.DataColumnMapping("Parameter_Value", "Parameter_Value"),
                        new System.Data.Common.DataColumnMapping("Parameter_Quantity", "Parameter_Quantity"),
                        new System.Data.Common.DataColumnMapping("Category_Type", "Category_Type"),
                        new System.Data.Common.DataColumnMapping("Category_Type_Rank", "Category_Type_Rank"),
                        new System.Data.Common.DataColumnMapping("Parameter_Rank", "Parameter_Rank"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy"),
                        new System.Data.Common.DataColumnMapping("Print_Name", "Print_Name")}),
            new System.Data.Common.DataTableMapping("Table1", "Table1", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Payment", "ID_Payment"),
                        new System.Data.Common.DataColumnMapping("Code", "Code"),
                        new System.Data.Common.DataColumnMapping("Display_Name", "Display_Name"),
                        new System.Data.Common.DataColumnMapping("ID_Parameter_Type", "ID_Parameter_Type"),
                        new System.Data.Common.DataColumnMapping("Parameter_Value", "Parameter_Value"),
                        new System.Data.Common.DataColumnMapping("Parameter_Quantity", "Parameter_Quantity"),
                        new System.Data.Common.DataColumnMapping("Category_Type", "Category_Type"),
                        new System.Data.Common.DataColumnMapping("Category_Type_Rank", "Category_Type_Rank"),
                        new System.Data.Common.DataColumnMapping("Parameter_Rank", "Parameter_Rank"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy")})});
            this.daPaymentParameterDetail.UpdateCommand = this.sqlUpdateCommand3;
            // 
            // sqlSelectCommand5
            // 
            this.sqlSelectCommand5.CommandText = "town.Payment_Info";
            this.sqlSelectCommand5.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand5.Connection = this.sqlEllisConnection;
            this.sqlSelectCommand5.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Authcod", System.Data.SqlDbType.VarChar, 24)});
            // 
            // daPaymentInfo
            // 
            this.daPaymentInfo.SelectCommand = this.sqlSelectCommand5;
            this.daPaymentInfo.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Payment_Info", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Derive_Company", "ID_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("Derive_Company_Name", "Derive_Company_Name"),
                        new System.Data.Common.DataColumnMapping("Derive_Company_Code", "Derive_Company_Code"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Filter", "ID_Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("Bill_Filter_Name", "Bill_Filter_Name"),
                        new System.Data.Common.DataColumnMapping("ID_Company", "ID_Company"),
                        new System.Data.Common.DataColumnMapping("КОМ_НАИМ", "КОМ_НАИМ"),
                        new System.Data.Common.DataColumnMapping("КОМ_ИНН", "КОМ_ИНН"),
                        new System.Data.Common.DataColumnMapping("КОМ_КПП", "КОМ_КПП"),
                        new System.Data.Common.DataColumnMapping("КОМ_РС", "КОМ_РС"),
                        new System.Data.Common.DataColumnMapping("КОМ_БИК", "КОМ_БИК"),
                        new System.Data.Common.DataColumnMapping("КОМ_БАНК", "КОМ_БАНК"),
                        new System.Data.Common.DataColumnMapping("Address", "Address")})});
            // 
            // sqlConnection2
            // 
            this.sqlConnection2.ConnectionString = "Data Source=srv-MRIVC5-1;Initial Catalog=Murmansk;Persist Security Info=True;User" +
    " ID=sa";
            this.sqlConnection2.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlSelectCommand6
            // 
            this.sqlSelectCommand6.CommandText = "dbo.sp_Filter_Client_Select";
            this.sqlSelectCommand6.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand6.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand6.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // daFilter_Client
            // 
            this.daFilter_Client.SelectCommand = this.sqlSelectCommand6;
            this.daFilter_Client.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_Filter_Client_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Company", "ID_Company"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment")})});
            // 
            // sqlSelectCommand7
            // 
            this.sqlSelectCommand7.CommandText = "dbo.sp_Payment_Ellis_Reestr";
            this.sqlSelectCommand7.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand7.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand7.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Payment_Begin_Date", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@Payment_End_Date", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@ID_User", System.Data.SqlDbType.Int, 4)});
            // 
            // daPaymentEllisReestr
            // 
            this.daPaymentEllisReestr.SelectCommand = this.sqlSelectCommand7;
            this.daPaymentEllisReestr.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_Payment_Ellis_Reestr", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Payment", "ID_Payment"),
                        new System.Data.Common.DataColumnMapping("CLIENT_NAME", "CLIENT_NAME"),
                        new System.Data.Common.DataColumnMapping("CLIENT_CODE", "CLIENT_CODE"),
                        new System.Data.Common.DataColumnMapping("LS", "LS"),
                        new System.Data.Common.DataColumnMapping("ADRES", "ADRES"),
                        new System.Data.Common.DataColumnMapping("UK", "UK"),
                        new System.Data.Common.DataColumnMapping("FAM", "FAM"),
                        new System.Data.Common.DataColumnMapping("IM", "IM"),
                        new System.Data.Common.DataColumnMapping("OT", "OT"),
                        new System.Data.Common.DataColumnMapping("AUTHCOD", "AUTHCOD"),
                        new System.Data.Common.DataColumnMapping("KVITID", "KVITID"),
                        new System.Data.Common.DataColumnMapping("DATPLAT", "DATPLAT"),
                        new System.Data.Common.DataColumnMapping("SUMPLAT", "SUMPLAT"),
                        new System.Data.Common.DataColumnMapping("PU1", "PU1"),
                        new System.Data.Common.DataColumnMapping("POK1", "POK1"),
                        new System.Data.Common.DataColumnMapping("PU2", "PU2"),
                        new System.Data.Common.DataColumnMapping("POK2", "POK2"),
                        new System.Data.Common.DataColumnMapping("PU3", "PU3"),
                        new System.Data.Common.DataColumnMapping("POK3", "POK3"),
                        new System.Data.Common.DataColumnMapping("PU4", "PU4"),
                        new System.Data.Common.DataColumnMapping("POK4", "POK4"),
                        new System.Data.Common.DataColumnMapping("COMMENT", "COMMENT"),
                        new System.Data.Common.DataColumnMapping("ID_Loaded_Status", "ID_Loaded_Status")})});
            this.daPaymentEllisReestr.UpdateCommand = this.sqlCommand1;
            // 
            // sqlCommand1
            // 
            this.sqlCommand1.CommandText = "sp_Payment_Loaded_Status_Update";
            this.sqlCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlCommand1.Connection = this.sqlPaymentConnection;
            this.sqlCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4, "ID_Payment"),
            new System.Data.SqlClient.SqlParameter("@ID_Loaded_Status", System.Data.SqlDbType.Int, 4, "ID_Loaded_Status")});
            // 
            // cmdClearDb
            // 
            this.cmdClearDb.CommandText = "sp_Clear_DB";
            this.cmdClearDb.CommandTimeout = 0;
            this.cmdClearDb.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdClearDb.Connection = this.sqlPaymentConnection;
            // 
            // sqlSelectCommand3
            // 
            this.sqlSelectCommand3.CommandText = "dbo.sp_Program_User_Select";
            this.sqlSelectCommand3.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand3.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // sqlInsertCommand2
            // 
            this.sqlInsertCommand2.CommandText = "dbo.sp_Program_User_Insert";
            this.sqlInsertCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand2.Connection = this.sqlPaymentConnection;
            this.sqlInsertCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_User", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "ID_User", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Login", System.Data.SqlDbType.VarChar, 100, "Login"),
            new System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 250, "Name"),
            new System.Data.SqlClient.SqlParameter("@Fiscal_User_ID", System.Data.SqlDbType.Int, 4, "Fiscal_User_ID"),
            new System.Data.SqlClient.SqlParameter("@Fiscal_User_Password", System.Data.SqlDbType.Int, 4, "Fiscal_User_Password"),
            new System.Data.SqlClient.SqlParameter("@CreatedDate", System.Data.SqlDbType.DateTime, 8, "CreatedDate")});
            // 
            // sqlUpdateCommand2
            // 
            this.sqlUpdateCommand2.CommandText = "dbo.sp_Program_User_Update";
            this.sqlUpdateCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand2.Connection = this.sqlPaymentConnection;
            this.sqlUpdateCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_User", System.Data.SqlDbType.Int, 4, "ID_User"),
            new System.Data.SqlClient.SqlParameter("@Login", System.Data.SqlDbType.VarChar, 100, "Login"),
            new System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 250, "Name"),
            new System.Data.SqlClient.SqlParameter("@Fiscal_User_ID", System.Data.SqlDbType.Int, 4, "Fiscal_User_ID"),
            new System.Data.SqlClient.SqlParameter("@Fiscal_User_Password", System.Data.SqlDbType.Int, 4, "Fiscal_User_Password"),
            new System.Data.SqlClient.SqlParameter("@CreatedDate", System.Data.SqlDbType.DateTime, 8, "CreatedDate")});
            // 
            // sqlDeleteCommand2
            // 
            this.sqlDeleteCommand2.CommandText = "dbo.sp_Program_User_Delete";
            this.sqlDeleteCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand2.Connection = this.sqlPaymentConnection;
            this.sqlDeleteCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_User", System.Data.SqlDbType.Int, 4, "ID_User")});
            // 
            // daUser
            // 
            this.daUser.DeleteCommand = this.sqlDeleteCommand2;
            this.daUser.InsertCommand = this.sqlInsertCommand2;
            this.daUser.SelectCommand = this.sqlSelectCommand3;
            this.daUser.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_Program_User_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_User", "ID_User"),
                        new System.Data.Common.DataColumnMapping("Login", "Login"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Full_Name", "Full_Name"),
                        new System.Data.Common.DataColumnMapping("Fiscal_User_ID", "Fiscal_User_ID"),
                        new System.Data.Common.DataColumnMapping("Fiscal_User_Password", "Fiscal_User_Password"),
                        new System.Data.Common.DataColumnMapping("ReadOnlyPrivilegy", "ReadOnlyPrivilegy"),
                        new System.Data.Common.DataColumnMapping("FilterPaymentsByUser", "FilterPaymentsByUser"),
                        new System.Data.Common.DataColumnMapping("SimulateTerminalMode", "SimulateTerminalMode"),
                        new System.Data.Common.DataColumnMapping("CreatedDate", "CreatedDate")})});
            this.daUser.UpdateCommand = this.sqlUpdateCommand2;
            // 
            // sqlSelectCommand8
            // 
            this.sqlSelectCommand8.CommandText = "dbo.sp_PersonIdentification_Select";
            this.sqlSelectCommand8.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand8.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand8.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 4)});
            // 
            // sqlInsertCommand4
            // 
            this.sqlInsertCommand4.CommandText = "dbo.sp_PersonIdentification_Insert";
            this.sqlInsertCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand4.Connection = this.sqlPaymentConnection;
            this.sqlInsertCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@IS_COMPANY", System.Data.SqlDbType.Bit, 1, "IS_COMPANY"),
            new System.Data.SqlClient.SqlParameter("@LASTNAME", System.Data.SqlDbType.VarChar, 100, "LASTNAME"),
            new System.Data.SqlClient.SqlParameter("@FIRSTNAME", System.Data.SqlDbType.VarChar, 100, "FIRSTNAME"),
            new System.Data.SqlClient.SqlParameter("@MIDDLENAME", System.Data.SqlDbType.VarChar, 100, "MIDDLENAME"),
            new System.Data.SqlClient.SqlParameter("@INN", System.Data.SqlDbType.VarChar, 20, "INN"),
            new System.Data.SqlClient.SqlParameter("@REGNUM", System.Data.SqlDbType.VarChar, 40, "REGNUM"),
            new System.Data.SqlClient.SqlParameter("@REGNAME", System.Data.SqlDbType.VarChar, 100, "REGNAME"),
            new System.Data.SqlClient.SqlParameter("@REGDATE", System.Data.SqlDbType.DateTime, 8, "REGDATE"),
            new System.Data.SqlClient.SqlParameter("@COUNTRY", System.Data.SqlDbType.VarChar, 100, "COUNTRY"),
            new System.Data.SqlClient.SqlParameter("@CODERF", System.Data.SqlDbType.VarChar, 100, "CODERF"),
            new System.Data.SqlClient.SqlParameter("@DISTRICT", System.Data.SqlDbType.VarChar, 100, "DISTRICT"),
            new System.Data.SqlClient.SqlParameter("@CITY", System.Data.SqlDbType.VarChar, 100, "CITY"),
            new System.Data.SqlClient.SqlParameter("@STREET", System.Data.SqlDbType.VarChar, 100, "STREET"),
            new System.Data.SqlClient.SqlParameter("@HOUSE_NUMBER", System.Data.SqlDbType.Int, 4, "HOUSE_NUMBER"),
            new System.Data.SqlClient.SqlParameter("@HOUSE_LITER", System.Data.SqlDbType.VarChar, 100, "HOUSE_LITER"),
            new System.Data.SqlClient.SqlParameter("@FLAT", System.Data.SqlDbType.VarChar, 100, "FLAT"),
            new System.Data.SqlClient.SqlParameter("@ADDR_REG_SAME", System.Data.SqlDbType.Bit, 1, "ADDR_REG_SAME"),
            new System.Data.SqlClient.SqlParameter("@P_COUNTRY", System.Data.SqlDbType.VarChar, 100, "P_COUNTRY"),
            new System.Data.SqlClient.SqlParameter("@P_CODERF", System.Data.SqlDbType.VarChar, 100, "P_CODERF"),
            new System.Data.SqlClient.SqlParameter("@P_DISTRICT", System.Data.SqlDbType.VarChar, 100, "P_DISTRICT"),
            new System.Data.SqlClient.SqlParameter("@P_CITY", System.Data.SqlDbType.VarChar, 100, "P_CITY"),
            new System.Data.SqlClient.SqlParameter("@P_STREET", System.Data.SqlDbType.VarChar, 100, "P_STREET"),
            new System.Data.SqlClient.SqlParameter("@P_HOUSE_NUMBER", System.Data.SqlDbType.VarChar, 100, "P_HOUSE_NUMBER"),
            new System.Data.SqlClient.SqlParameter("@P_HOUSE_LITER", System.Data.SqlDbType.VarChar, 100, "P_HOUSE_LITER"),
            new System.Data.SqlClient.SqlParameter("@P_FLAT", System.Data.SqlDbType.VarChar, 100, "P_FLAT"),
            new System.Data.SqlClient.SqlParameter("@DOCTYPE", System.Data.SqlDbType.VarChar, 100, "DOCTYPE"),
            new System.Data.SqlClient.SqlParameter("@DOCSERIA", System.Data.SqlDbType.VarChar, 20, "DOCSERIA"),
            new System.Data.SqlClient.SqlParameter("@DOCNUM", System.Data.SqlDbType.VarChar, 20, "DOCNUM"),
            new System.Data.SqlClient.SqlParameter("@DOCDATE", System.Data.SqlDbType.DateTime, 8, "DOCDATE"),
            new System.Data.SqlClient.SqlParameter("@DOCORG", System.Data.SqlDbType.VarChar, 100, "DOCORG"),
            new System.Data.SqlClient.SqlParameter("@DOCBD", System.Data.SqlDbType.DateTime, 8, "DOCBD"),
            new System.Data.SqlClient.SqlParameter("@DOCPB", System.Data.SqlDbType.VarChar, 100, "DOCPB"),
            new System.Data.SqlClient.SqlParameter("@DOCCOUNTRY", System.Data.SqlDbType.VarChar, 100, "DOCCOUNTRY"),
            new System.Data.SqlClient.SqlParameter("@DOCCITY", System.Data.SqlDbType.VarChar, 100, "DOCCITY"),
            new System.Data.SqlClient.SqlParameter("@DOCCITIZEN", System.Data.SqlDbType.VarChar, 100, "DOCCITIZEN"),
            new System.Data.SqlClient.SqlParameter("@SUM", System.Data.SqlDbType.Money, 8, "SUM"),
            new System.Data.SqlClient.SqlParameter("@OSN", System.Data.SqlDbType.VarChar, 1000, "OSN"),
            new System.Data.SqlClient.SqlParameter("@PRIZN", System.Data.SqlDbType.VarChar, 100, "PRIZN"),
            new System.Data.SqlClient.SqlParameter("@COMMENT", System.Data.SqlDbType.VarChar, 1000, "COMMENT"),
            new System.Data.SqlClient.SqlParameter("@ORGNAME", System.Data.SqlDbType.VarChar, 255, "ORGNAME"),
            new System.Data.SqlClient.SqlParameter("@ORGINN", System.Data.SqlDbType.VarChar, 40, "ORGINN"),
            new System.Data.SqlClient.SqlParameter("@ORGADDRESS", System.Data.SqlDbType.VarChar, 255, "ORGADDRESS"),
            new System.Data.SqlClient.SqlParameter("@ORGADDRESSFACT", System.Data.SqlDbType.VarChar, 255, "ORGADDRESSFACT"),
            new System.Data.SqlClient.SqlParameter("@ORGFIO", System.Data.SqlDbType.VarChar, 255, "ORGFIO"),
            new System.Data.SqlClient.SqlParameter("@RELATION", System.Data.SqlDbType.VarChar, 255, "RELATION"),
            new System.Data.SqlClient.SqlParameter("@DELEGATEFIO", System.Data.SqlDbType.VarChar, 255, "DELEGATEFIO"),
            new System.Data.SqlClient.SqlParameter("@PRESENTFIO", System.Data.SqlDbType.VarChar, 255, "PRESENTFIO"),
            new System.Data.SqlClient.SqlParameter("@RESIVERFIO", System.Data.SqlDbType.VarChar, 255, "RESIVERFIO"),
            new System.Data.SqlClient.SqlParameter("@FIO", System.Data.SqlDbType.VarChar, 255, "FIO"),
            new System.Data.SqlClient.SqlParameter("@EMPLOYEE", System.Data.SqlDbType.VarChar, 255, "EMPLOYEE"),
            new System.Data.SqlClient.SqlParameter("@EMPLOYEEDIRECTOR", System.Data.SqlDbType.VarChar, 255, "EMPLOYEEDIRECTOR"),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4, "CreatedBy"),
            new System.Data.SqlClient.SqlParameter("@CreationDate", System.Data.SqlDbType.DateTime, 8, "CreationDate")});
            // 
            // sqlUpdateCommand4
            // 
            this.sqlUpdateCommand4.CommandText = "dbo.sp_PersonIdentification_Update";
            this.sqlUpdateCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand4.Connection = this.sqlPaymentConnection;
            this.sqlUpdateCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@IS_COMPANY", System.Data.SqlDbType.Bit, 1, "IS_COMPANY"),
            new System.Data.SqlClient.SqlParameter("@LASTNAME", System.Data.SqlDbType.VarChar, 100, "LASTNAME"),
            new System.Data.SqlClient.SqlParameter("@FIRSTNAME", System.Data.SqlDbType.VarChar, 100, "FIRSTNAME"),
            new System.Data.SqlClient.SqlParameter("@MIDDLENAME", System.Data.SqlDbType.VarChar, 100, "MIDDLENAME"),
            new System.Data.SqlClient.SqlParameter("@INN", System.Data.SqlDbType.VarChar, 20, "INN"),
            new System.Data.SqlClient.SqlParameter("@REGNUM", System.Data.SqlDbType.VarChar, 40, "REGNUM"),
            new System.Data.SqlClient.SqlParameter("@REGNAME", System.Data.SqlDbType.VarChar, 100, "REGNAME"),
            new System.Data.SqlClient.SqlParameter("@REGDATE", System.Data.SqlDbType.DateTime, 8, "REGDATE"),
            new System.Data.SqlClient.SqlParameter("@COUNTRY", System.Data.SqlDbType.VarChar, 100, "COUNTRY"),
            new System.Data.SqlClient.SqlParameter("@CODERF", System.Data.SqlDbType.VarChar, 100, "CODERF"),
            new System.Data.SqlClient.SqlParameter("@DISTRICT", System.Data.SqlDbType.VarChar, 100, "DISTRICT"),
            new System.Data.SqlClient.SqlParameter("@CITY", System.Data.SqlDbType.VarChar, 100, "CITY"),
            new System.Data.SqlClient.SqlParameter("@STREET", System.Data.SqlDbType.VarChar, 100, "STREET"),
            new System.Data.SqlClient.SqlParameter("@HOUSE_NUMBER", System.Data.SqlDbType.Int, 4, "HOUSE_NUMBER"),
            new System.Data.SqlClient.SqlParameter("@HOUSE_LITER", System.Data.SqlDbType.VarChar, 100, "HOUSE_LITER"),
            new System.Data.SqlClient.SqlParameter("@FLAT", System.Data.SqlDbType.VarChar, 100, "FLAT"),
            new System.Data.SqlClient.SqlParameter("@ADDR_REG_SAME", System.Data.SqlDbType.Bit, 1, "ADDR_REG_SAME"),
            new System.Data.SqlClient.SqlParameter("@P_COUNTRY", System.Data.SqlDbType.VarChar, 100, "P_COUNTRY"),
            new System.Data.SqlClient.SqlParameter("@P_CODERF", System.Data.SqlDbType.VarChar, 100, "P_CODERF"),
            new System.Data.SqlClient.SqlParameter("@P_DISTRICT", System.Data.SqlDbType.VarChar, 100, "P_DISTRICT"),
            new System.Data.SqlClient.SqlParameter("@P_CITY", System.Data.SqlDbType.VarChar, 100, "P_CITY"),
            new System.Data.SqlClient.SqlParameter("@P_STREET", System.Data.SqlDbType.VarChar, 100, "P_STREET"),
            new System.Data.SqlClient.SqlParameter("@P_HOUSE_NUMBER", System.Data.SqlDbType.VarChar, 100, "P_HOUSE_NUMBER"),
            new System.Data.SqlClient.SqlParameter("@P_HOUSE_LITER", System.Data.SqlDbType.VarChar, 100, "P_HOUSE_LITER"),
            new System.Data.SqlClient.SqlParameter("@P_FLAT", System.Data.SqlDbType.VarChar, 100, "P_FLAT"),
            new System.Data.SqlClient.SqlParameter("@DOCTYPE", System.Data.SqlDbType.VarChar, 100, "DOCTYPE"),
            new System.Data.SqlClient.SqlParameter("@DOCSERIA", System.Data.SqlDbType.VarChar, 20, "DOCSERIA"),
            new System.Data.SqlClient.SqlParameter("@DOCNUM", System.Data.SqlDbType.VarChar, 20, "DOCNUM"),
            new System.Data.SqlClient.SqlParameter("@DOCDATE", System.Data.SqlDbType.DateTime, 8, "DOCDATE"),
            new System.Data.SqlClient.SqlParameter("@DOCORG", System.Data.SqlDbType.VarChar, 100, "DOCORG"),
            new System.Data.SqlClient.SqlParameter("@DOCBD", System.Data.SqlDbType.DateTime, 8, "DOCBD"),
            new System.Data.SqlClient.SqlParameter("@DOCPB", System.Data.SqlDbType.VarChar, 100, "DOCPB"),
            new System.Data.SqlClient.SqlParameter("@DOCCOUNTRY", System.Data.SqlDbType.VarChar, 100, "DOCCOUNTRY"),
            new System.Data.SqlClient.SqlParameter("@DOCCITY", System.Data.SqlDbType.VarChar, 100, "DOCCITY"),
            new System.Data.SqlClient.SqlParameter("@DOCCITIZEN", System.Data.SqlDbType.VarChar, 100, "DOCCITIZEN"),
            new System.Data.SqlClient.SqlParameter("@SUM", System.Data.SqlDbType.Money, 8, "SUM"),
            new System.Data.SqlClient.SqlParameter("@OSN", System.Data.SqlDbType.VarChar, 1000, "OSN"),
            new System.Data.SqlClient.SqlParameter("@PRIZN", System.Data.SqlDbType.VarChar, 100, "PRIZN"),
            new System.Data.SqlClient.SqlParameter("@COMMENT", System.Data.SqlDbType.VarChar, 1000, "COMMENT"),
            new System.Data.SqlClient.SqlParameter("@ORGNAME", System.Data.SqlDbType.VarChar, 255, "ORGNAME"),
            new System.Data.SqlClient.SqlParameter("@ORGINN", System.Data.SqlDbType.VarChar, 40, "ORGINN"),
            new System.Data.SqlClient.SqlParameter("@ORGADDRESS", System.Data.SqlDbType.VarChar, 255, "ORGADDRESS"),
            new System.Data.SqlClient.SqlParameter("@ORGADDRESSFACT", System.Data.SqlDbType.VarChar, 255, "ORGADDRESSFACT"),
            new System.Data.SqlClient.SqlParameter("@ORGFIO", System.Data.SqlDbType.VarChar, 255, "ORGFIO"),
            new System.Data.SqlClient.SqlParameter("@RELATION", System.Data.SqlDbType.VarChar, 255, "RELATION"),
            new System.Data.SqlClient.SqlParameter("@DELEGATEFIO", System.Data.SqlDbType.VarChar, 255, "DELEGATEFIO"),
            new System.Data.SqlClient.SqlParameter("@PRESENTFIO", System.Data.SqlDbType.VarChar, 255, "PRESENTFIO"),
            new System.Data.SqlClient.SqlParameter("@RESIVERFIO", System.Data.SqlDbType.VarChar, 255, "RESIVERFIO"),
            new System.Data.SqlClient.SqlParameter("@FIO", System.Data.SqlDbType.VarChar, 255, "FIO"),
            new System.Data.SqlClient.SqlParameter("@EMPLOYEE", System.Data.SqlDbType.VarChar, 255, "EMPLOYEE"),
            new System.Data.SqlClient.SqlParameter("@EMPLOYEEDIRECTOR", System.Data.SqlDbType.VarChar, 255, "EMPLOYEEDIRECTOR"),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4, "CreatedBy"),
            new System.Data.SqlClient.SqlParameter("@CreationDate", System.Data.SqlDbType.DateTime, 8, "CreationDate"),
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID")});
            // 
            // sqlDeleteCommand4
            // 
            this.sqlDeleteCommand4.CommandText = "dbo.sp_PersonIdentification_Delete";
            this.sqlDeleteCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand4.Connection = this.sqlPaymentConnection;
            this.sqlDeleteCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID")});
            // 
            // daPersonIdentification
            // 
            this.daPersonIdentification.DeleteCommand = this.sqlDeleteCommand4;
            this.daPersonIdentification.InsertCommand = this.sqlInsertCommand4;
            this.daPersonIdentification.SelectCommand = this.sqlSelectCommand8;
            this.daPersonIdentification.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_PersonIdentification_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID", "ID"),
                        new System.Data.Common.DataColumnMapping("IS_COMPANY", "IS_COMPANY"),
                        new System.Data.Common.DataColumnMapping("LASTNAME", "LASTNAME"),
                        new System.Data.Common.DataColumnMapping("FIRSTNAME", "FIRSTNAME"),
                        new System.Data.Common.DataColumnMapping("MIDDLENAME", "MIDDLENAME"),
                        new System.Data.Common.DataColumnMapping("INN", "INN"),
                        new System.Data.Common.DataColumnMapping("REGNUM", "REGNUM"),
                        new System.Data.Common.DataColumnMapping("REGNAME", "REGNAME"),
                        new System.Data.Common.DataColumnMapping("REGDATE", "REGDATE"),
                        new System.Data.Common.DataColumnMapping("COUNTRY", "COUNTRY"),
                        new System.Data.Common.DataColumnMapping("CODERF", "CODERF"),
                        new System.Data.Common.DataColumnMapping("DISTRICT", "DISTRICT"),
                        new System.Data.Common.DataColumnMapping("CITY", "CITY"),
                        new System.Data.Common.DataColumnMapping("STREET", "STREET"),
                        new System.Data.Common.DataColumnMapping("HOUSE_NUMBER", "HOUSE_NUMBER"),
                        new System.Data.Common.DataColumnMapping("HOUSE_LITER", "HOUSE_LITER"),
                        new System.Data.Common.DataColumnMapping("FLAT", "FLAT"),
                        new System.Data.Common.DataColumnMapping("ADDR_REG_SAME", "ADDR_REG_SAME"),
                        new System.Data.Common.DataColumnMapping("P_COUNTRY", "P_COUNTRY"),
                        new System.Data.Common.DataColumnMapping("P_CODERF", "P_CODERF"),
                        new System.Data.Common.DataColumnMapping("P_DISTRICT", "P_DISTRICT"),
                        new System.Data.Common.DataColumnMapping("P_CITY", "P_CITY"),
                        new System.Data.Common.DataColumnMapping("P_STREET", "P_STREET"),
                        new System.Data.Common.DataColumnMapping("P_HOUSE_NUMBER", "P_HOUSE_NUMBER"),
                        new System.Data.Common.DataColumnMapping("P_HOUSE_LITER", "P_HOUSE_LITER"),
                        new System.Data.Common.DataColumnMapping("P_FLAT", "P_FLAT"),
                        new System.Data.Common.DataColumnMapping("DOCTYPE", "DOCTYPE"),
                        new System.Data.Common.DataColumnMapping("DOCSERIA", "DOCSERIA"),
                        new System.Data.Common.DataColumnMapping("DOCNUM", "DOCNUM"),
                        new System.Data.Common.DataColumnMapping("DOCDATE", "DOCDATE"),
                        new System.Data.Common.DataColumnMapping("DOCORG", "DOCORG"),
                        new System.Data.Common.DataColumnMapping("DOCBD", "DOCBD"),
                        new System.Data.Common.DataColumnMapping("DOCPB", "DOCPB"),
                        new System.Data.Common.DataColumnMapping("DOCCOUNTRY", "DOCCOUNTRY"),
                        new System.Data.Common.DataColumnMapping("DOCCITY", "DOCCITY"),
                        new System.Data.Common.DataColumnMapping("DOCCITIZEN", "DOCCITIZEN"),
                        new System.Data.Common.DataColumnMapping("SUM", "SUM"),
                        new System.Data.Common.DataColumnMapping("OSN", "OSN"),
                        new System.Data.Common.DataColumnMapping("PRIZN", "PRIZN"),
                        new System.Data.Common.DataColumnMapping("COMMENT", "COMMENT"),
                        new System.Data.Common.DataColumnMapping("ORGNAME", "ORGNAME"),
                        new System.Data.Common.DataColumnMapping("ORGINN", "ORGINN"),
                        new System.Data.Common.DataColumnMapping("ORGADDRESS", "ORGADDRESS"),
                        new System.Data.Common.DataColumnMapping("ORGADDRESSFACT", "ORGADDRESSFACT"),
                        new System.Data.Common.DataColumnMapping("ORGFIO", "ORGFIO"),
                        new System.Data.Common.DataColumnMapping("RELATION", "RELATION"),
                        new System.Data.Common.DataColumnMapping("DELEGATEFIO", "DELEGATEFIO"),
                        new System.Data.Common.DataColumnMapping("PRESENTFIO", "PRESENTFIO"),
                        new System.Data.Common.DataColumnMapping("RESIVERFIO", "RESIVERFIO"),
                        new System.Data.Common.DataColumnMapping("FIO", "FIO"),
                        new System.Data.Common.DataColumnMapping("EMPLOYEE", "EMPLOYEE"),
                        new System.Data.Common.DataColumnMapping("EMPLOYEEDIRECTOR", "EMPLOYEEDIRECTOR"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy"),
                        new System.Data.Common.DataColumnMapping("CreationDate", "CreationDate")})});
            this.daPersonIdentification.UpdateCommand = this.sqlUpdateCommand4;
            // 
            // sqlSelectCommand9
            // 
            this.sqlSelectCommand9.CommandText = "dbo.sp_PersonIdentification_Search";
            this.sqlSelectCommand9.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand9.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand9.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@LastName", System.Data.SqlDbType.VarChar, 100)});
            // 
            // daPersonIdentificationSearch
            // 
            this.daPersonIdentificationSearch.SelectCommand = this.sqlSelectCommand9;
            this.daPersonIdentificationSearch.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_PersonIdentification_Search", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID", "ID"),
                        new System.Data.Common.DataColumnMapping("IS_COMPANY", "IS_COMPANY"),
                        new System.Data.Common.DataColumnMapping("LASTNAME", "LASTNAME"),
                        new System.Data.Common.DataColumnMapping("FIRSTNAME", "FIRSTNAME"),
                        new System.Data.Common.DataColumnMapping("MIDDLENAME", "MIDDLENAME"),
                        new System.Data.Common.DataColumnMapping("INN", "INN"),
                        new System.Data.Common.DataColumnMapping("REGNUM", "REGNUM"),
                        new System.Data.Common.DataColumnMapping("REGNAME", "REGNAME"),
                        new System.Data.Common.DataColumnMapping("REGDATE", "REGDATE"),
                        new System.Data.Common.DataColumnMapping("COUNTRY", "COUNTRY"),
                        new System.Data.Common.DataColumnMapping("CODERF", "CODERF"),
                        new System.Data.Common.DataColumnMapping("DISTRICT", "DISTRICT"),
                        new System.Data.Common.DataColumnMapping("CITY", "CITY"),
                        new System.Data.Common.DataColumnMapping("STREET", "STREET"),
                        new System.Data.Common.DataColumnMapping("HOUSE_NUMBER", "HOUSE_NUMBER"),
                        new System.Data.Common.DataColumnMapping("HOUSE_LITER", "HOUSE_LITER"),
                        new System.Data.Common.DataColumnMapping("FLAT", "FLAT"),
                        new System.Data.Common.DataColumnMapping("ADDR_REG_SAME", "ADDR_REG_SAME"),
                        new System.Data.Common.DataColumnMapping("P_COUNTRY", "P_COUNTRY"),
                        new System.Data.Common.DataColumnMapping("P_CODERF", "P_CODERF"),
                        new System.Data.Common.DataColumnMapping("P_DISTRICT", "P_DISTRICT"),
                        new System.Data.Common.DataColumnMapping("P_CITY", "P_CITY"),
                        new System.Data.Common.DataColumnMapping("P_STREET", "P_STREET"),
                        new System.Data.Common.DataColumnMapping("P_HOUSE_NUMBER", "P_HOUSE_NUMBER"),
                        new System.Data.Common.DataColumnMapping("P_HOUSE_LITER", "P_HOUSE_LITER"),
                        new System.Data.Common.DataColumnMapping("P_FLAT", "P_FLAT"),
                        new System.Data.Common.DataColumnMapping("DOCTYPE", "DOCTYPE"),
                        new System.Data.Common.DataColumnMapping("DOCSERIA", "DOCSERIA"),
                        new System.Data.Common.DataColumnMapping("DOCNUM", "DOCNUM"),
                        new System.Data.Common.DataColumnMapping("DOCDATE", "DOCDATE"),
                        new System.Data.Common.DataColumnMapping("DOCORG", "DOCORG"),
                        new System.Data.Common.DataColumnMapping("DOCBD", "DOCBD"),
                        new System.Data.Common.DataColumnMapping("DOCPB", "DOCPB"),
                        new System.Data.Common.DataColumnMapping("DOCCOUNTRY", "DOCCOUNTRY"),
                        new System.Data.Common.DataColumnMapping("DOCCITY", "DOCCITY"),
                        new System.Data.Common.DataColumnMapping("DOCCITIZEN", "DOCCITIZEN"),
                        new System.Data.Common.DataColumnMapping("SUM", "SUM"),
                        new System.Data.Common.DataColumnMapping("OSN", "OSN"),
                        new System.Data.Common.DataColumnMapping("PRIZN", "PRIZN"),
                        new System.Data.Common.DataColumnMapping("COMMENT", "COMMENT"),
                        new System.Data.Common.DataColumnMapping("ORGNAME", "ORGNAME"),
                        new System.Data.Common.DataColumnMapping("ORGINN", "ORGINN"),
                        new System.Data.Common.DataColumnMapping("ORGADDRESS", "ORGADDRESS"),
                        new System.Data.Common.DataColumnMapping("ORGADDRESSFACT", "ORGADDRESSFACT"),
                        new System.Data.Common.DataColumnMapping("ORGFIO", "ORGFIO"),
                        new System.Data.Common.DataColumnMapping("RELATION", "RELATION"),
                        new System.Data.Common.DataColumnMapping("DELEGATEFIO", "DELEGATEFIO"),
                        new System.Data.Common.DataColumnMapping("PRESENTFIO", "PRESENTFIO"),
                        new System.Data.Common.DataColumnMapping("RESIVERFIO", "RESIVERFIO"),
                        new System.Data.Common.DataColumnMapping("FIO", "FIO"),
                        new System.Data.Common.DataColumnMapping("EMPLOYEE", "EMPLOYEE"),
                        new System.Data.Common.DataColumnMapping("EMPLOYEEDIRECTOR", "EMPLOYEEDIRECTOR"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy"),
                        new System.Data.Common.DataColumnMapping("CreationDate", "CreationDate")})});
            // 
            // sqlSelectCommand10
            // 
            this.sqlSelectCommand10.CommandText = "dbo.sp_Payment_Verify";
            this.sqlSelectCommand10.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand10.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand10.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Barcode", System.Data.SqlDbType.VarChar, 128)});
            // 
            // daPaymentVerification
            // 
            this.daPaymentVerification.SelectCommand = this.sqlSelectCommand10;
            this.daPaymentVerification.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_Payment_Verify", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("Address", "Address"),
                        new System.Data.Common.DataColumnMapping("Account", "Account"),
                        new System.Data.Common.DataColumnMapping("SummPay", "SummPay"),
                        new System.Data.Common.DataColumnMapping("SummFine", "SummFine"),
                        new System.Data.Common.DataColumnMapping("Barcode", "Barcode"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("CreationDate", "CreationDate")})});
            // 
            // cmdRegistryVerificationLineInsert
            // 
            this.cmdRegistryVerificationLineInsert.CommandText = "dbo.sp_Registry_Verification_Load";
            this.cmdRegistryVerificationLineInsert.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdRegistryVerificationLineInsert.Connection = this.sqlPaymentConnection;
            this.cmdRegistryVerificationLineInsert.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Registry_Verification", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_Client", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Address", System.Data.SqlDbType.VarChar, 250),
            new System.Data.SqlClient.SqlParameter("@Account", System.Data.SqlDbType.VarChar, 50),
            new System.Data.SqlClient.SqlParameter("@SummPay", System.Data.SqlDbType.Money, 8),
            new System.Data.SqlClient.SqlParameter("@SummFine", System.Data.SqlDbType.Money, 8),
            new System.Data.SqlClient.SqlParameter("@Barcode", System.Data.SqlDbType.VarChar, 250),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 4000)});
            // 
            // cmdRegistryVerification
            // 
            this.cmdRegistryVerification.CommandText = "dbo.sp_Registry_Verification_Create";
            this.cmdRegistryVerification.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdRegistryVerification.Connection = this.sqlPaymentConnection;
            this.cmdRegistryVerification.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Registry_Verification", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Client", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@FileName", System.Data.SqlDbType.VarChar, 255),
            new System.Data.SqlClient.SqlParameter("@EmailAddress", System.Data.SqlDbType.VarChar, 255),
            new System.Data.SqlClient.SqlParameter("@EmailSubject", System.Data.SqlDbType.VarChar, 255),
            new System.Data.SqlClient.SqlParameter("@EmailDate", System.Data.SqlDbType.DateTime, 10)});
            // 
            // cmdRegistryVerificationSetLoadedStatus
            // 
            this.cmdRegistryVerificationSetLoadedStatus.CommandText = "dbo.sp_Registry_Verification_Set_Loaded_Status";
            this.cmdRegistryVerificationSetLoadedStatus.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdRegistryVerificationSetLoadedStatus.Connection = this.sqlPaymentConnection;
            this.cmdRegistryVerificationSetLoadedStatus.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Registry_Verification", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Registry_Loaded", System.Data.SqlDbType.Bit, 2)});
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "dbo.sp_Payment_Select";
            this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand1.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Payment_Begin_Date", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@Payment_End_Date", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@ID_Payment_Class", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ID_User", System.Data.SqlDbType.Int, 4)});
            // 
            // sqlInsertCommand1
            // 
            this.sqlInsertCommand1.CommandText = "dbo.sp_Payment_Insert";
            this.sqlInsertCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlInsertCommand1.Connection = this.sqlPaymentConnection;
            this.sqlInsertCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, false, ((byte)(0)), ((byte)(0)), "ID_Payment", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment_Class", System.Data.SqlDbType.Int, 4, "ID_Payment_Class"),
            new System.Data.SqlClient.SqlParameter("@Operation_Number", System.Data.SqlDbType.Int, 4, "Operation_Number"),
            new System.Data.SqlClient.SqlParameter("@Session_Number", System.Data.SqlDbType.Int, 4, "Session_Number"),
            new System.Data.SqlClient.SqlParameter("@Department_Number", System.Data.SqlDbType.Int, 4, "Department_Number"),
            new System.Data.SqlClient.SqlParameter("@Payment_Date", System.Data.SqlDbType.DateTime, 8, "Payment_Date"),
            new System.Data.SqlClient.SqlParameter("@Payment_Cash", System.Data.SqlDbType.Money, 8, "Payment_Cash"),
            new System.Data.SqlClient.SqlParameter("@Payment_Summ", System.Data.SqlDbType.Money, 8, "Payment_Summ"),
            new System.Data.SqlClient.SqlParameter("@Payment_Commission", System.Data.SqlDbType.Money, 8, "Payment_Commission"),
            new System.Data.SqlClient.SqlParameter("@IsStorno", System.Data.SqlDbType.Bit, 1, "IsStorno"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 500, "Comment"),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4, "CreatedBy"),
            new System.Data.SqlClient.SqlParameter("@ID_Cash_Type", System.Data.SqlDbType.Int, 4, "ID_Cash_Type"),
            new System.Data.SqlClient.SqlParameter("@DocumentNumber", System.Data.SqlDbType.Int, 4, "DocumentNumber")});
            // 
            // sqlUpdateCommand1
            // 
            this.sqlUpdateCommand1.CommandText = "dbo.sp_Payment_Update";
            this.sqlUpdateCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand1.Connection = this.sqlPaymentConnection;
            this.sqlUpdateCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4, "ID_Payment"),
            new System.Data.SqlClient.SqlParameter("@ID_Payment_Class", System.Data.SqlDbType.Int, 4, "ID_Payment_Class"),
            new System.Data.SqlClient.SqlParameter("@Operation_Number", System.Data.SqlDbType.Int, 4, "Operation_Number"),
            new System.Data.SqlClient.SqlParameter("@Department_Number", System.Data.SqlDbType.Int, 4, "Department_Number"),
            new System.Data.SqlClient.SqlParameter("@Payment_Date", System.Data.SqlDbType.DateTime, 8, "Payment_Date"),
            new System.Data.SqlClient.SqlParameter("@Payment_Cash", System.Data.SqlDbType.Money, 8, "Payment_Cash"),
            new System.Data.SqlClient.SqlParameter("@Payment_Summ", System.Data.SqlDbType.Money, 8, "Payment_Summ"),
            new System.Data.SqlClient.SqlParameter("@Payment_Commission", System.Data.SqlDbType.Money, 8, "Payment_Commission"),
            new System.Data.SqlClient.SqlParameter("@IsStorno", System.Data.SqlDbType.Bit, 1, "IsStorno"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 500, "Comment"),
            new System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.Int, 4, "CreatedBy"),
            new System.Data.SqlClient.SqlParameter("@ID_Cash_Type", System.Data.SqlDbType.Int, 4, "ID_Cash_Type"),
            new System.Data.SqlClient.SqlParameter("@ReturnDocumentNumber", System.Data.SqlDbType.Int, 4, "ReturnDocumentNumber")});
            // 
            // sqlDeleteCommand1
            // 
            this.sqlDeleteCommand1.CommandText = "dbo.sp_Payment_Storno";
            this.sqlDeleteCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand1.Connection = this.sqlPaymentConnection;
            this.sqlDeleteCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4, "ID_Payment")});
            // 
            // daPayment
            // 
            this.daPayment.DeleteCommand = this.sqlDeleteCommand1;
            this.daPayment.InsertCommand = this.sqlInsertCommand1;
            this.daPayment.SelectCommand = this.sqlSelectCommand1;
            this.daPayment.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_Payment_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Payment", "ID_Payment"),
                        new System.Data.Common.DataColumnMapping("Operation_Number", "Operation_Number"),
                        new System.Data.Common.DataColumnMapping("Session_Number", "Session_Number"),
                        new System.Data.Common.DataColumnMapping("Department_Number", "Department_Number"),
                        new System.Data.Common.DataColumnMapping("ID_Payment_Class", "ID_Payment_Class"),
                        new System.Data.Common.DataColumnMapping("Payment_Date", "Payment_Date"),
                        new System.Data.Common.DataColumnMapping("Payment_Cash", "Payment_Cash"),
                        new System.Data.Common.DataColumnMapping("Payment_Summ", "Payment_Summ"),
                        new System.Data.Common.DataColumnMapping("Payment_Commission", "Payment_Commission"),
                        new System.Data.Common.DataColumnMapping("Payment_SummTotal", "Payment_SummTotal"),
                        new System.Data.Common.DataColumnMapping("Payment_Charge", "Payment_Charge"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("IsStorno", "IsStorno"),
                        new System.Data.Common.DataColumnMapping("CreatedBy", "CreatedBy")})});
            this.daPayment.UpdateCommand = this.sqlUpdateCommand1;
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.ConnectionString = "Data Source=srv-mrivc6;Initial Catalog=Payments;Persist Security Info=True;User I" +
    "D=bobrovsky;Password=speaker";
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlSelectCommand11
            // 
            this.sqlSelectCommand11.CommandText = "dbo.sp_Payment_Unload_Sber_Reestr";
            this.sqlSelectCommand11.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand11.Connection = this.sqlPaymentConnection;
            this.sqlSelectCommand11.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Payment_Begin_Date", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@Payment_End_Date", System.Data.SqlDbType.DateTime, 8),
            new System.Data.SqlClient.SqlParameter("@ID_User", System.Data.SqlDbType.Int, 4)});
            // 
            // sqlUpdateCommand5
            // 
            this.sqlUpdateCommand5.CommandText = "dbo.sp_Payment_Loaded_Status_Update";
            this.sqlUpdateCommand5.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand5.Connection = this.sqlPaymentConnection;
            this.sqlUpdateCommand5.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Payment", System.Data.SqlDbType.Int, 4, "ID_Payment"),
            new System.Data.SqlClient.SqlParameter("@ID_Loaded_Status", System.Data.SqlDbType.Int, 4, "ID_Loaded_Status")});
            // 
            // daPaymentTxtReestr
            // 
            this.daPaymentTxtReestr.SelectCommand = this.sqlSelectCommand11;
            this.daPaymentTxtReestr.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "sp_Payment_Unload_Sber_Reestr", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Payment", "ID_Payment"),
                        new System.Data.Common.DataColumnMapping("ID_Payment_Class", "ID_Payment_Class"),
                        new System.Data.Common.DataColumnMapping("CLIENT_ID", "CLIENT_ID"),
                        new System.Data.Common.DataColumnMapping("CLIENT_NAME", "CLIENT_NAME"),
                        new System.Data.Common.DataColumnMapping("BARCODE", "BARCODE"),
                        new System.Data.Common.DataColumnMapping("ADDRESS", "ADDRESS"),
                        new System.Data.Common.DataColumnMapping("Payment_Date", "Payment_Date"),
                        new System.Data.Common.DataColumnMapping("Payment_Summ", "Payment_Summ"),
                        new System.Data.Common.DataColumnMapping("Payment_Commission", "Payment_Commission"),
                        new System.Data.Common.DataColumnMapping("TotalSumm", "TotalSumm"),
                        new System.Data.Common.DataColumnMapping("Line", "Line"),
                        new System.Data.Common.DataColumnMapping("ID_Loaded_Status", "ID_Loaded_Status")})});
            this.daPaymentTxtReestr.UpdateCommand = this.sqlUpdateCommand5;
            // 
            // sqlSelectCommand12
            // 
            this.sqlSelectCommand12.CommandText = "select *\r\nfrom [dbo].[Cash_Type]";
            this.sqlSelectCommand12.Connection = this.sqlPaymentConnection;
            // 
            // daCashType
            // 
            this.daCashType.SelectCommand = this.sqlSelectCommand12;
            this.daCashType.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Cash_Type", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Cash_Type", "ID_Cash_Type"),
                        new System.Data.Common.DataColumnMapping("Name", "Name")})});
            // 
            // sqlConnection3
            // 
            this.sqlConnection3.ConnectionString = "Data Source=SRV-MRIVC5-1\\INSTANCE2;Initial Catalog=Payments;Integrated Security=T" +
    "rue";
            this.sqlConnection3.FireInfoMessageEventOnUserErrors = false;

        }

        #endregion

        private System.Data.SqlClient.SqlConnection sqlPaymentConnection;
        private System.Data.SqlClient.SqlConnection sqlEllisConnection;
        private System.Data.SqlClient.SqlCommand cmdProgramSettingsSelectByCode;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand2;
        private System.Data.SqlClient.SqlDataAdapter daPaymentClass;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand4;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand3;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand3;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand3;
        private System.Data.SqlClient.SqlDataAdapter daPaymentParameterDetail;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand5;
        private System.Data.SqlClient.SqlDataAdapter daPaymentInfo;
        private System.Data.SqlClient.SqlConnection sqlConnection2;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand6;
        private System.Data.SqlClient.SqlDataAdapter daFilter_Client;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand7;
        private System.Data.SqlClient.SqlDataAdapter daPaymentEllisReestr;
        private System.Data.SqlClient.SqlCommand sqlCommand1;
        private System.Data.SqlClient.SqlCommand cmdClearDb;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand3;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand2;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand2;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand2;
        private System.Data.SqlClient.SqlDataAdapter daUser;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand8;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand4;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand4;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand4;
        private System.Data.SqlClient.SqlDataAdapter daPersonIdentification;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand9;
        private System.Data.SqlClient.SqlDataAdapter daPersonIdentificationSearch;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand10;
        private System.Data.SqlClient.SqlDataAdapter daPaymentVerification;
        private System.Data.SqlClient.SqlCommand cmdRegistryVerificationLineInsert;
        private System.Data.SqlClient.SqlCommand cmdRegistryVerification;
        private System.Data.SqlClient.SqlCommand cmdRegistryVerificationSetLoadedStatus;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand1;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand1;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand1;
        private System.Data.SqlClient.SqlDataAdapter daPayment;
        private System.Data.SqlClient.SqlConnection sqlConnection1;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand11;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand5;
        private System.Data.SqlClient.SqlDataAdapter daPaymentTxtReestr;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand12;
        private System.Data.SqlClient.SqlDataAdapter daCashType;
        private System.Data.SqlClient.SqlConnection sqlConnection3;
    }
}
