using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FastReport.Export;
using FastReport.Export.XAML;
using FastReport.Utils;

namespace FastReport.Forms
{
    internal partial class XAMLExportForm : BaseExportForm
    {
        public override void Init(ExportBase export)
        {
            base.Init(export);
            XAMLExport xamlExport = Export as XAMLExport;
            comboBox1.SelectedIndex = (int)xamlExport.ImageFormat;
        }
        
        protected override void Done()
        {
            base.Done();
            XAMLExport xamlExport = Export as XAMLExport;
            xamlExport.ImageFormat = (XamlImageFormat)comboBox1.SelectedIndex;
        }
        
        public override void Localize()
        {
            base.Localize();
            MyRes res = new MyRes("Export,Xaml");
            Text = res.Get("");
            res = new MyRes("Export,Misc");
            gbOptions.Text = res.Get("Options");
            lblImageFormat.Text = res.Get("Pictures");
        }        
        
        public XAMLExportForm()
        {
            InitializeComponent();
        }

    }
}

