﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Payments.LogicClasses
{
    public class PaymentParameterDatail
    {
        string displayName;
        string printName;
        string parameterCode;
        string parameterValue;
        int parameterQuantity;

        public PaymentParameterDatail(string displayName, string printName, string parameterCode, string parameterValue, int parameterpQuantity)
        {
            this.displayName = displayName;
            this.printName = printName;
            this.parameterCode = parameterCode;
            this.parameterValue = parameterValue;
            this.parameterQuantity = parameterpQuantity;
        }

        public string PrintName
        {
            get { return printName; }
        }

        public string DisplayName
        {
            get { return displayName; }
        }

        public string ParameterCode
        {
            get { return parameterCode; }
        }

        public string ParameterValue
        {
            get { return parameterValue; }
        }

        public int ParameterQuantity
        {
            get { return parameterQuantity; }
        }

    }
}
