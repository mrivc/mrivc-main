﻿namespace MassBillNew
{
    partial class Penetration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkedComboBoxEdit1 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.servicesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.penetrDataSet = new MassBillNew.PenetrDataSet();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.murmanskDataSet = new MassBillNew.MurmanskDataSet();
            this.qPenetrBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.qPenetrTableAdapter = new MassBillNew.PenetrDataSetTableAdapters.qPenetrTableAdapter();
            this.servicesTableAdapter = new MassBillNew.PenetrDataSetTableAdapters.servicesTableAdapter();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.penetrDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.murmanskDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qPenetrBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // checkedComboBoxEdit1
            // 
            this.checkedComboBoxEdit1.EditValue = "";
            this.checkedComboBoxEdit1.Location = new System.Drawing.Point(12, 12);
            this.checkedComboBoxEdit1.Name = "checkedComboBoxEdit1";
            this.checkedComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit1.Properties.DataSource = this.servicesBindingSource;
            this.checkedComboBoxEdit1.Properties.DisplayMember = "name";
            this.checkedComboBoxEdit1.Properties.ValueMember = "id";
            this.checkedComboBoxEdit1.Size = new System.Drawing.Size(199, 20);
            this.checkedComboBoxEdit1.TabIndex = 0;
            // 
            // servicesBindingSource
            // 
            this.servicesBindingSource.DataMember = "services";
            this.servicesBindingSource.DataSource = this.penetrDataSet;
            // 
            // penetrDataSet
            // 
            this.penetrDataSet.DataSetName = "PenetrDataSet";
            this.penetrDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 93);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(199, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Сформировать";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // murmanskDataSet
            // 
            this.murmanskDataSet.DataSetName = "MurmanskDataSet";
            this.murmanskDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // qPenetrBindingSource
            // 
            this.qPenetrBindingSource.DataMember = "qPenetr";
            this.qPenetrBindingSource.DataSource = this.penetrDataSet;
            // 
            // qPenetrTableAdapter
            // 
            this.qPenetrTableAdapter.ClearBeforeFill = true;
            // 
            // servicesTableAdapter
            // 
            this.servicesTableAdapter.ClearBeforeFill = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(12, 38);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(37, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "xls";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(12, 61);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(42, 17);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.Text = "xlsx";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // Penetration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 128);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.checkedComboBoxEdit1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Penetration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Penetration";
            this.Load += new System.EventHandler(this.Penetration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.penetrDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.murmanskDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qPenetrBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit1;
        private MurmanskDataSet murmanskDataSet;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.BindingSource qPenetrBindingSource;
        private PenetrDataSet penetrDataSet;
        private PenetrDataSetTableAdapters.qPenetrTableAdapter qPenetrTableAdapter;
        private System.Windows.Forms.BindingSource servicesBindingSource;
        private PenetrDataSetTableAdapters.servicesTableAdapter servicesTableAdapter;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
    }
}