﻿namespace EllisBillFilter.Forms
{
    partial class FrmLawsuitSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLawsuitSearch));
            this.inputsGroupBox = new System.Windows.Forms.GroupBox();
            this.resetButton = new System.Windows.Forms.Button();
            this.fullnameLabel = new System.Windows.Forms.Label();
            this.caseLabel = new System.Windows.Forms.Label();
            this.fullnameTextBox = new System.Windows.Forms.TextBox();
            this.caseTextBox = new System.Windows.Forms.TextBox();
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.inputsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // inputsGroupBox
            // 
            this.inputsGroupBox.Controls.Add(this.resetButton);
            this.inputsGroupBox.Controls.Add(this.fullnameLabel);
            this.inputsGroupBox.Controls.Add(this.caseLabel);
            this.inputsGroupBox.Controls.Add(this.fullnameTextBox);
            this.inputsGroupBox.Controls.Add(this.caseTextBox);
            this.inputsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.inputsGroupBox.Name = "inputsGroupBox";
            this.inputsGroupBox.Size = new System.Drawing.Size(370, 80);
            this.inputsGroupBox.TabIndex = 0;
            this.inputsGroupBox.TabStop = false;
            this.inputsGroupBox.Text = "Поиск";
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(314, 19);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(46, 46);
            this.resetButton.TabIndex = 3;
            this.resetButton.Text = "сброс";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // fullnameLabel
            // 
            this.fullnameLabel.AutoSize = true;
            this.fullnameLabel.Location = new System.Drawing.Point(6, 48);
            this.fullnameLabel.Name = "fullnameLabel";
            this.fullnameLabel.Size = new System.Drawing.Size(37, 13);
            this.fullnameLabel.TabIndex = 3;
            this.fullnameLabel.Text = "ФИО:";
            // 
            // caseLabel
            // 
            this.caseLabel.AutoSize = true;
            this.caseLabel.Location = new System.Drawing.Point(6, 22);
            this.caseLabel.Name = "caseLabel";
            this.caseLabel.Size = new System.Drawing.Size(71, 13);
            this.caseLabel.TabIndex = 2;
            this.caseLabel.Text = "Номер дела:";
            // 
            // fullnameTextBox
            // 
            this.fullnameTextBox.Location = new System.Drawing.Point(83, 45);
            this.fullnameTextBox.Name = "fullnameTextBox";
            this.fullnameTextBox.Size = new System.Drawing.Size(225, 20);
            this.fullnameTextBox.TabIndex = 2;
            this.fullnameTextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // caseTextBox
            // 
            this.caseTextBox.Location = new System.Drawing.Point(83, 19);
            this.caseTextBox.Name = "caseTextBox";
            this.caseTextBox.Size = new System.Drawing.Size(225, 20);
            this.caseTextBox.TabIndex = 1;
            this.caseTextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.EnableHeadersVisualStyles = false;
            this.dataGridView.Location = new System.Drawing.Point(12, 98);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.ShowEditingIcon = false;
            this.dataGridView.Size = new System.Drawing.Size(960, 401);
            this.dataGridView.TabIndex = 4;
            // 
            // FrmLawsuitSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 511);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.inputsGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(600, 550);
            this.Name = "FrmLawsuitSearch";
            this.Text = "Поиск иска";
            this.inputsGroupBox.ResumeLayout(false);
            this.inputsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox inputsGroupBox;
        private System.Windows.Forms.Label fullnameLabel;
        private System.Windows.Forms.Label caseLabel;
        private System.Windows.Forms.TextBox fullnameTextBox;
        private System.Windows.Forms.TextBox caseTextBox;
        private DS.MDataSet mDataSet;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button resetButton;
    }
}