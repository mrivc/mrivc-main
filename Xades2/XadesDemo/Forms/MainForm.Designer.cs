﻿namespace XadesDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.integrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportNsiMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportHouseDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportBillMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportMeterToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.exportMeasureToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.эспортДоговоровУправленияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортВБДToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортУслугToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортИнформацииОДомахToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.импортПлатежныхДокументовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортПриборовУчетаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортПоказанийПУToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отозватьПлатежныеДокументыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетОВыполненныхКомандахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LogSaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.integrationToolStripMenuItem,
            this.импортВБДToolStripMenuItem,
            this.serviceToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1458, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // integrationToolStripMenuItem
            // 
            this.integrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportNsiMenuItem,
            this.exportHouseDataToolStripMenuItem,
            this.exportBillMenuItem,
            this.exportMeterToolStrip,
            this.exportMeasureToolStrip,
            this.эспортДоговоровУправленияToolStripMenuItem});
            this.integrationToolStripMenuItem.Name = "integrationToolStripMenuItem";
            this.integrationToolStripMenuItem.Size = new System.Drawing.Size(133, 20);
            this.integrationToolStripMenuItem.Text = "Экспорт из ГИС ЖКХ";
            // 
            // exportNsiMenuItem
            // 
            this.exportNsiMenuItem.Name = "exportNsiMenuItem";
            this.exportNsiMenuItem.Size = new System.Drawing.Size(273, 22);
            this.exportNsiMenuItem.Text = "Экспорт справочников НСИ и услуг";
            this.exportNsiMenuItem.Click += new System.EventHandler(this.exportNsiMenuItem_Click);
            // 
            // exportHouseDataToolStripMenuItem
            // 
            this.exportHouseDataToolStripMenuItem.Name = "exportHouseDataToolStripMenuItem";
            this.exportHouseDataToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.exportHouseDataToolStripMenuItem.Text = "Экспорт информации о домах";
            this.exportHouseDataToolStripMenuItem.Click += new System.EventHandler(this.exportHouseDataToolStripMenuItem_Click);
            // 
            // exportBillMenuItem
            // 
            this.exportBillMenuItem.Name = "exportBillMenuItem";
            this.exportBillMenuItem.Size = new System.Drawing.Size(273, 22);
            this.exportBillMenuItem.Text = "Экспорт платежных документов";
            this.exportBillMenuItem.Click += new System.EventHandler(this.exportBillMenuItem_Click);
            // 
            // exportMeterToolStrip
            // 
            this.exportMeterToolStrip.Name = "exportMeterToolStrip";
            this.exportMeterToolStrip.Size = new System.Drawing.Size(273, 22);
            this.exportMeterToolStrip.Text = "Экспорт приборов учета";
            this.exportMeterToolStrip.Click += new System.EventHandler(this.exportMeterToolStrip_Click);
            // 
            // exportMeasureToolStrip
            // 
            this.exportMeasureToolStrip.Name = "exportMeasureToolStrip";
            this.exportMeasureToolStrip.Size = new System.Drawing.Size(273, 22);
            this.exportMeasureToolStrip.Text = "Экспорт показаний ПУ";
            this.exportMeasureToolStrip.Click += new System.EventHandler(this.exportMeasureToolStrip_Click);
            // 
            // эспортДоговоровУправленияToolStripMenuItem
            // 
            this.эспортДоговоровУправленияToolStripMenuItem.Name = "эспортДоговоровУправленияToolStripMenuItem";
            this.эспортДоговоровУправленияToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.эспортДоговоровУправленияToolStripMenuItem.Text = "Эспорт договоров управления";
            this.эспортДоговоровУправленияToolStripMenuItem.Click += new System.EventHandler(this.эспортДоговоровУправленияToolStripMenuItem_Click);
            // 
            // импортВБДToolStripMenuItem
            // 
            this.импортВБДToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.импортУслугToolStripMenuItem,
            this.импортИнформацииОДомахToolStripMenuItem1,
            this.импортПлатежныхДокументовToolStripMenuItem,
            this.импортПриборовУчетаToolStripMenuItem,
            this.импортПоказанийПУToolStripMenuItem,
            this.отозватьПлатежныеДокументыToolStripMenuItem});
            this.импортВБДToolStripMenuItem.Name = "импортВБДToolStripMenuItem";
            this.импортВБДToolStripMenuItem.Size = new System.Drawing.Size(105, 20);
            this.импортВБДToolStripMenuItem.Text = "Синхронизация";
            // 
            // импортУслугToolStripMenuItem
            // 
            this.импортУслугToolStripMenuItem.Name = "импортУслугToolStripMenuItem";
            this.импортУслугToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.импортУслугToolStripMenuItem.Text = "Синхронизация услуг";
            this.импортУслугToolStripMenuItem.Click += new System.EventHandler(this.importServicesToolStripMenuItem_Click);
            // 
            // импортИнформацииОДомахToolStripMenuItem1
            // 
            this.импортИнформацииОДомахToolStripMenuItem1.Name = "импортИнформацииОДомахToolStripMenuItem1";
            this.импортИнформацииОДомахToolStripMenuItem1.Size = new System.Drawing.Size(292, 22);
            this.импортИнформацииОДомахToolStripMenuItem1.Text = "Синхронизация информации о домах";
            this.импортИнформацииОДомахToolStripMenuItem1.Click += new System.EventHandler(this.импортИнформацииОДомахToolStripMenuItem_Click);
            // 
            // импортПлатежныхДокументовToolStripMenuItem
            // 
            this.импортПлатежныхДокументовToolStripMenuItem.Name = "импортПлатежныхДокументовToolStripMenuItem";
            this.импортПлатежныхДокументовToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.импортПлатежныхДокументовToolStripMenuItem.Text = "Синхронизация платежных документов";
            this.импортПлатежныхДокументовToolStripMenuItem.Click += new System.EventHandler(this.importBillsMenuItem_Click);
            // 
            // импортПриборовУчетаToolStripMenuItem
            // 
            this.импортПриборовУчетаToolStripMenuItem.Name = "импортПриборовУчетаToolStripMenuItem";
            this.импортПриборовУчетаToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.импортПриборовУчетаToolStripMenuItem.Text = "Синхронизация приборов учета";
            this.импортПриборовУчетаToolStripMenuItem.Click += new System.EventHandler(this.importMeterToolStrip_Click);
            // 
            // импортПоказанийПУToolStripMenuItem
            // 
            this.импортПоказанийПУToolStripMenuItem.Name = "импортПоказанийПУToolStripMenuItem";
            this.импортПоказанийПУToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.импортПоказанийПУToolStripMenuItem.Text = "Синхронизация показаний ПУ";
            this.импортПоказанийПУToolStripMenuItem.Click += new System.EventHandler(this.импортПоказанийПУToolStripMenuItem_Click);
            // 
            // отозватьПлатежныеДокументыToolStripMenuItem
            // 
            this.отозватьПлатежныеДокументыToolStripMenuItem.Name = "отозватьПлатежныеДокументыToolStripMenuItem";
            this.отозватьПлатежныеДокументыToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.отозватьПлатежныеДокументыToolStripMenuItem.Text = "Отозвать платежные документы";
            this.отозватьПлатежныеДокументыToolStripMenuItem.Click += new System.EventHandler(this.отозватьПлатежныеДокументыToolStripMenuItem_Click);
            // 
            // serviceToolStripMenuItem
            // 
            this.serviceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отчетОВыполненныхКомандахToolStripMenuItem,
            this.LogSaveToolStripMenuItem});
            this.serviceToolStripMenuItem.Name = "serviceToolStripMenuItem";
            this.serviceToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.serviceToolStripMenuItem.Text = "Сервис";
            // 
            // отчетОВыполненныхКомандахToolStripMenuItem
            // 
            this.отчетОВыполненныхКомандахToolStripMenuItem.Name = "отчетОВыполненныхКомандахToolStripMenuItem";
            this.отчетОВыполненныхКомандахToolStripMenuItem.Size = new System.Drawing.Size(439, 22);
            this.отчетОВыполненныхКомандахToolStripMenuItem.Text = "Отчет о выполненных командах";
            this.отчетОВыполненныхКомандахToolStripMenuItem.Click += new System.EventHandler(this.отчетОВыполненныхКомандахToolStripMenuItem_Click);
            // 
            // LogSaveToolStripMenuItem
            // 
            this.LogSaveToolStripMenuItem.Name = "LogSaveToolStripMenuItem";
            this.LogSaveToolStripMenuItem.Size = new System.Drawing.Size(439, 22);
            this.LogSaveToolStripMenuItem.Text = "Выгрузка текстов запроса и ответа по ошибкам для техподдержки";
            this.LogSaveToolStripMenuItem.Click += new System.EventHandler(this.LogSaveToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1458, 813);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "ГИС \"ЖКХ\" Интеграция";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem integrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportHouseDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportBillMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportMeterToolStrip;
        private System.Windows.Forms.ToolStripMenuItem exportMeasureToolStrip;
        private System.Windows.Forms.ToolStripMenuItem exportNsiMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортВБДToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортУслугToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортИнформацииОДомахToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem импортПлатежныхДокументовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортПриборовУчетаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортПоказанийПУToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отозватьПлатежныеДокументыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетОВыполненныхКомандахToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem эспортДоговоровУправленияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LogSaveToolStripMenuItem;
    }
}