﻿namespace EllisBillFilter
{
    partial class GridAgreement
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridAgreements = new EllisBillFilter.BaseGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAgreementAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miAgreementDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miAgreementDeleteCellValue = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miAgreementRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miAgreementExport = new System.Windows.Forms.ToolStripMenuItem();
            this.agreementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gridViewAgreement = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Agreement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreement_Number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreement_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Agreement_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riAgreementType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.agreementTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Agent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riAgent = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.payReceiverCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Agent2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Agent3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridAgreements)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.agreementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAgreement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riAgreementType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agreementTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riAgent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridAgreements
            // 
            this.gridAgreements.ContextMenuStrip = this.contextMenuStrip1;
            this.gridAgreements.DataSource = this.agreementBindingSource;
            this.gridAgreements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAgreements.Location = new System.Drawing.Point(0, 0);
            this.gridAgreements.MainView = this.gridViewAgreement;
            this.gridAgreements.Name = "gridAgreements";
            this.gridAgreements.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riAgent,
            this.riAgreementType});
            this.gridAgreements.Size = new System.Drawing.Size(997, 429);
            this.gridAgreements.TabIndex = 0;
            this.gridAgreements.TrimCellValueBeforeValidate = true;
            this.gridAgreements.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAgreement});
            this.gridAgreements.SaveData += new EllisBillFilter.BaseGrid.SaveDataEventHandler(this.gridAgreements_SaveData);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAgreementAdd,
            this.miAgreementDelete,
            this.toolStripSeparator2,
            this.miAgreementDeleteCellValue,
            this.toolStripSeparator3,
            this.miAgreementRefresh,
            this.toolStripSeparator1,
            this.miAgreementExport});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(215, 154);
            // 
            // miAgreementAdd
            // 
            this.miAgreementAdd.Image = global::EllisBillFilter.Properties.Resources.record_add;
            this.miAgreementAdd.Name = "miAgreementAdd";
            this.miAgreementAdd.Size = new System.Drawing.Size(214, 22);
            this.miAgreementAdd.Text = "Добавить запись";
            this.miAgreementAdd.Click += new System.EventHandler(this.miAgreementAdd_Click);
            // 
            // miAgreementDelete
            // 
            this.miAgreementDelete.Image = global::EllisBillFilter.Properties.Resources.record_delete;
            this.miAgreementDelete.Name = "miAgreementDelete";
            this.miAgreementDelete.Size = new System.Drawing.Size(214, 22);
            this.miAgreementDelete.Text = "Удалить запись";
            this.miAgreementDelete.Click += new System.EventHandler(this.miAgreementDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(211, 6);
            // 
            // miAgreementDeleteCellValue
            // 
            this.miAgreementDeleteCellValue.Name = "miAgreementDeleteCellValue";
            this.miAgreementDeleteCellValue.Size = new System.Drawing.Size(214, 22);
            this.miAgreementDeleteCellValue.Text = "Удалить значение ячейки";
            this.miAgreementDeleteCellValue.Click += new System.EventHandler(this.miAgreementDeleteCellValue_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(211, 6);
            // 
            // miAgreementRefresh
            // 
            this.miAgreementRefresh.Image = global::EllisBillFilter.Properties.Resources.refresh;
            this.miAgreementRefresh.Name = "miAgreementRefresh";
            this.miAgreementRefresh.Size = new System.Drawing.Size(214, 22);
            this.miAgreementRefresh.Text = "Обновить";
            this.miAgreementRefresh.Click += new System.EventHandler(this.miAgreementRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(211, 6);
            // 
            // miAgreementExport
            // 
            this.miAgreementExport.Image = global::EllisBillFilter.Properties.Resources.export;
            this.miAgreementExport.Name = "miAgreementExport";
            this.miAgreementExport.Size = new System.Drawing.Size(214, 22);
            this.miAgreementExport.Text = "Экспорт";
            this.miAgreementExport.Click += new System.EventHandler(this.miAgreementExport_Click);
            // 
            // agreementBindingSource
            // 
            this.agreementBindingSource.DataMember = "Agreement";
            this.agreementBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewAgreement
            // 
            this.gridViewAgreement.ColumnPanelRowHeight = 40;
            this.gridViewAgreement.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Agreement,
            this.colAgreement_Number,
            this.colAgreement_Date,
            this.colID_Agreement_Type,
            this.colDescription,
            this.colID_Agent1,
            this.colID_Agent2,
            this.colID_Agent3,
            this.colComment,
            this.colCreatedBy,
            this.colCreationDate});
            this.gridViewAgreement.CustomizationFormBounds = new System.Drawing.Rectangle(606, 387, 216, 318);
            this.gridViewAgreement.GridControl = this.gridAgreements;
            this.gridViewAgreement.Name = "gridViewAgreement";
            this.gridViewAgreement.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewAgreement.OptionsView.ColumnAutoWidth = false;
            this.gridViewAgreement.OptionsView.ShowDetailButtons = false;
            this.gridViewAgreement.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewAgreement_InitNewRow);
            this.gridViewAgreement.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewAgreement_FocusedRowChanged);
            this.gridViewAgreement.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewAgreement_CellValueChanging);
            this.gridViewAgreement.DoubleClick += new System.EventHandler(this.gridViewAgreement_DoubleClick);
            // 
            // colID_Agreement
            // 
            this.colID_Agreement.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Agreement.AppearanceCell.Options.UseBackColor = true;
            this.colID_Agreement.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Agreement.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID_Agreement.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Agreement.Caption = "ИД";
            this.colID_Agreement.FieldName = "ID_Agreement";
            this.colID_Agreement.Name = "colID_Agreement";
            this.colID_Agreement.OptionsColumn.AllowEdit = false;
            this.colID_Agreement.OptionsColumn.ReadOnly = true;
            this.colID_Agreement.Visible = true;
            this.colID_Agreement.VisibleIndex = 0;
            this.colID_Agreement.Width = 67;
            // 
            // colAgreement_Number
            // 
            this.colAgreement_Number.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgreement_Number.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgreement_Number.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgreement_Number.Caption = "Номер договора*";
            this.colAgreement_Number.FieldName = "Agreement_Number";
            this.colAgreement_Number.Name = "colAgreement_Number";
            this.colAgreement_Number.Visible = true;
            this.colAgreement_Number.VisibleIndex = 1;
            this.colAgreement_Number.Width = 103;
            // 
            // colAgreement_Date
            // 
            this.colAgreement_Date.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgreement_Date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgreement_Date.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgreement_Date.Caption = "Дата начала действия договора*";
            this.colAgreement_Date.FieldName = "Agreement_Date";
            this.colAgreement_Date.Name = "colAgreement_Date";
            this.colAgreement_Date.Visible = true;
            this.colAgreement_Date.VisibleIndex = 2;
            this.colAgreement_Date.Width = 132;
            // 
            // colID_Agreement_Type
            // 
            this.colID_Agreement_Type.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Agreement_Type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID_Agreement_Type.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Agreement_Type.Caption = "Тип договора*";
            this.colID_Agreement_Type.ColumnEdit = this.riAgreementType;
            this.colID_Agreement_Type.FieldName = "ID_Agreement_Type";
            this.colID_Agreement_Type.Name = "colID_Agreement_Type";
            this.colID_Agreement_Type.Visible = true;
            this.colID_Agreement_Type.VisibleIndex = 3;
            // 
            // riAgreementType
            // 
            this.riAgreementType.AutoHeight = false;
            this.riAgreementType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riAgreementType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Agreement_Type", "ID_Agreement_Type", 123, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Agreement_Type", "Agreement_Type", 93, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riAgreementType.DataSource = this.agreementTypeBindingSource;
            this.riAgreementType.DisplayMember = "Agreement_Type";
            this.riAgreementType.Name = "riAgreementType";
            this.riAgreementType.NullText = "";
            this.riAgreementType.ShowFooter = false;
            this.riAgreementType.ShowHeader = false;
            this.riAgreementType.ValueMember = "ID_Agreement_Type";
            // 
            // agreementTypeBindingSource
            // 
            this.agreementTypeBindingSource.DataMember = "Agreement_Type";
            this.agreementTypeBindingSource.DataSource = this.mDataSet;
            // 
            // colDescription
            // 
            this.colDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescription.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDescription.Caption = "Описание*";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 4;
            this.colDescription.Width = 266;
            // 
            // colID_Agent1
            // 
            this.colID_Agent1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Agent1.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Agent1.AppearanceCell.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Agent1.AppearanceCell.Options.UseBackColor = true;
            this.colID_Agent1.AppearanceCell.Options.UseBorderColor = true;
            this.colID_Agent1.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Agent1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID_Agent1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Agent1.Caption = "Агент*";
            this.colID_Agent1.ColumnEdit = this.riAgent;
            this.colID_Agent1.FieldName = "ID_Agent1";
            this.colID_Agent1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colID_Agent1.Name = "colID_Agent1";
            this.colID_Agent1.OptionsColumn.AllowEdit = false;
            this.colID_Agent1.OptionsColumn.ReadOnly = true;
            this.colID_Agent1.Visible = true;
            this.colID_Agent1.VisibleIndex = 6;
            this.colID_Agent1.Width = 151;
            // 
            // riAgent
            // 
            this.riAgent.AutoHeight = false;
            this.riAgent.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "-", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.riAgent.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Company", "ID_Company", 85, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Наименование", 250, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Provider", "Поставщик", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Derive_Company", "УК", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Pay_Receiver", "Получатель пл.", 80, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Код", "Code", 50, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Meter_Future_Period_Day", "Meter_Future_Period_Day", 137, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Load_Measure_In_Future", "Load_Measure_In_Future", 134, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far)});
            this.riAgent.DataSource = this.payReceiverCompanyBindingSource;
            this.riAgent.DisplayMember = "Name";
            this.riAgent.DropDownRows = 12;
            this.riAgent.Name = "riAgent";
            this.riAgent.NullText = "";
            this.riAgent.PopupWidth = 450;
            this.riAgent.ValueMember = "ID_Company";
            this.riAgent.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.riAgent_ButtonClick);
            // 
            // payReceiverCompanyBindingSource
            // 
            this.payReceiverCompanyBindingSource.DataMember = "PayReceiverCompany";
            this.payReceiverCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Agent2
            // 
            this.colID_Agent2.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Agent2.AppearanceCell.Options.UseBackColor = true;
            this.colID_Agent2.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Agent2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID_Agent2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Agent2.Caption = "Принципал*";
            this.colID_Agent2.ColumnEdit = this.riAgent;
            this.colID_Agent2.FieldName = "ID_Agent2";
            this.colID_Agent2.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colID_Agent2.Name = "colID_Agent2";
            this.colID_Agent2.OptionsColumn.AllowEdit = false;
            this.colID_Agent2.OptionsColumn.ReadOnly = true;
            this.colID_Agent2.Visible = true;
            this.colID_Agent2.VisibleIndex = 5;
            this.colID_Agent2.Width = 145;
            // 
            // colID_Agent3
            // 
            this.colID_Agent3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Agent3.AppearanceCell.Options.UseBackColor = true;
            this.colID_Agent3.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Agent3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID_Agent3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Agent3.Caption = "Третья сторона";
            this.colID_Agent3.ColumnEdit = this.riAgent;
            this.colID_Agent3.FieldName = "ID_Agent3";
            this.colID_Agent3.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colID_Agent3.Name = "colID_Agent3";
            this.colID_Agent3.OptionsColumn.AllowEdit = false;
            this.colID_Agent3.OptionsColumn.ReadOnly = true;
            this.colID_Agent3.Visible = true;
            this.colID_Agent3.VisibleIndex = 7;
            this.colID_Agent3.Width = 103;
            // 
            // colComment
            // 
            this.colComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComment.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComment.Caption = "Комментарий";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.Width = 141;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreatedBy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreatedBy.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCreatedBy.Caption = "Запись создана";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.Width = 85;
            // 
            // colCreationDate
            // 
            this.colCreationDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreationDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreationDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCreationDate.Caption = "Дата записи";
            this.colCreationDate.FieldName = "CreationDate";
            this.colCreationDate.Name = "colCreationDate";
            this.colCreationDate.Width = 89;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridAgreements);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(997, 609);
            this.splitContainerControl1.SplitterPosition = 429;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SizeChanged += new System.EventHandler(this.splitContainerControl1_SizeChanged);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.label1);
            this.splitContainerControl2.Panel1.Controls.Add(this.txtDescription);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.label2);
            this.splitContainerControl2.Panel2.Controls.Add(this.txtComment);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(997, 174);
            this.splitContainerControl2.SplitterPosition = 716;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Описание:";
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.Location = new System.Drawing.Point(0, 26);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(716, 148);
            this.txtDescription.TabIndex = 0;
            this.txtDescription.Leave += new System.EventHandler(this.txtDescription_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(8, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Комментарий:";
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(0, 26);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(275, 148);
            this.txtComment.TabIndex = 1;
            this.txtComment.Leave += new System.EventHandler(this.txtComment_Leave);
            // 
            // GridAgreement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "GridAgreement";
            this.Size = new System.Drawing.Size(997, 609);
            ((System.ComponentModel.ISupportInitialize)(this.gridAgreements)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.agreementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAgreement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riAgreementType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agreementTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riAgent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BaseGrid gridAgreements;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAgreement;
        private System.Windows.Forms.BindingSource agreementBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Agreement;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreement_Number;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreement_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Agent1;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Agent2;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Agent3;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colCreationDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riAgent;
        private System.Windows.Forms.BindingSource payReceiverCompanyBindingSource;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miAgreementAdd;
        private System.Windows.Forms.ToolStripMenuItem miAgreementDelete;
        private System.Windows.Forms.ToolStripMenuItem miAgreementRefresh;
        private System.Windows.Forms.ToolStripMenuItem miAgreementExport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miAgreementDeleteCellValue;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Agreement_Type;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riAgreementType;
        private System.Windows.Forms.BindingSource agreementTypeBindingSource;
    }
}
