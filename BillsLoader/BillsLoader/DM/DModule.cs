﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using BillsLoader.Logic;

namespace BillsLoader.DM
{
    using MrivcHelpers.Helpers;

    public partial class DModule : Component
    {
        public DModule()
        {
            InitializeComponent();
            //sqlSubConnection.ConnectionString = Properties.Settings.Default.MainConnection;
            //sqlMainConnection.ConnectionString = Properties.Settings.Default.MainConnection;
            //cmdAnyCommand.Connection.Close();
            //cmdAnyCommand.Connection.Open();
        }

        public void SetConnectionString(string connectionString)
        {
            SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();
            connBuilder.UserID = connectionString.GetSqlConnectionPart("UserID");
            connBuilder.Password = connectionString.GetSqlConnectionPart("Password");
            connBuilder.InitialCatalog = connectionString.GetSqlConnectionPart("InitialCatalog");
            connBuilder.DataSource = connectionString.GetSqlConnectionPart("DataSource");
            connBuilder.PersistSecurityInfo = true;

            cmdAnyCommand.Connection.Close();
            sqlSubConnection.Close();
            cmdAnyCommand.Connection.ConnectionString = connBuilder.ConnectionString;
            sqlSubConnection.ConnectionString = connBuilder.ConnectionString;
            sqlMainConnection.ConnectionString = connBuilder.ConnectionString;
            cmdAnyCommand.Connection.Open();
            sqlSubConnection.Open();
        }

        public string GetConnectionString()
        {
            return sqlMainConnection.ConnectionString;
        }

        public void SetSimpleConnectionString(string connectionString)
        {
            sqlMainConnection.ConnectionString = connectionString;
            sqlSubConnection.ConnectionString = connectionString;
            cmdAnyCommand.Connection.Close();
            sqlSubConnection.Close();
            cmdAnyCommand.Connection.Open();
            sqlSubConnection.Open();
        }

        public SqlConnection Connection {
            get {
                return sqlMainConnection;
            }
        }

        public void CreateTmpTables()
        {
            ExecuteNonQueryCommand(StaticQuery.TmpLoad);
        }

        public int WorkPeriod()
        {
            return (int)ExecuteScalarCommand("select dbo.Work_Period()");
        }

        public DateTime WorkPeriodBeginDate()
        {
            return (DateTime)ExecuteScalarCommand("select dbo.PeriodBeginDate(dbo.Work_Period())");
        }

        public string PrevPeriod()
        {
            string s = ExecuteScalarCommand("select convert(varchar,[year])+','+convert(varchar,[month]) from period where period = dbo.work_period()-1").ToString();

            return s;
        }

        public string WorkPeriodName()
        {
            return ExecuteScalarCommand("select dbo.Period_Name(dbo.Work_Period())").ToString();
        }

        public string CurrentDeriveCompanyFileName(string account)
        {
            cmdCurrentDeriveCompany.Parameters["@Account"].Value = account;
            
            if (cmdCurrentDeriveCompany.Connection.State == ConnectionState.Closed)
                cmdCurrentDeriveCompany.Connection.Open();

            return cmdCurrentDeriveCompany.ExecuteScalar().ToString();
        }

        public void ReestrSumPaySber(int number, int idPackType, DateTime date, string reestrNumber, bool Peni)
        {
            var name = "";

            if (!Peni)
            {
                name = "Reports.Reestr_Sum_Pay_Sberb";
            }
            else
            {
                name = "Reports.Reestr_Sum_Pay_Sberb_Only_Fine";
            }

            string sql =
                "declare @Sum_Payd Float, @Count_Payd Int " + Environment.NewLine +
                "exec " + name + " " + number.ToString() + ", " + idPackType.ToString() +
                            ", '" + date.Year.ToString() + StringHelper.ZeroCode(date.Month) + StringHelper.ZeroCode(date.Day) + "', @Sum_Payd output, @Count_Payd output, #ErrT, '"+ reestrNumber +"'"

            //Добавил Буторин Д.А. 3.07.14
                //Занести переменную с общей суммой всех платежей во временную таблицу #tempishe
                            + Environment.NewLine +
                            "IF OBJECT_ID('TEMPDB..#tempishe') is not null drop table #tempishe"
                            + Environment.NewLine +
                            "select @Sum_Payd as sum, @Count_Payd as pay_count into #tempishe";
            //End            

            ExecuteNonQueryCommand(sql);
        }

        //Добавил Буторин Д.А. 3.07.14
        //Функция вывода общей суммы
        public void SumPay(ReestrInfo r)
        {
            try
            {
                cmdAnyCommand.CommandText = "select pay_count from #tempishe";
                r.PayCountLoadedInEllis = (int)cmdAnyCommand.ExecuteScalar();

                cmdAnyCommand.CommandText = "select sum from #tempishe";
                r.PaySummLoadedInEllis = (double)cmdAnyCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка. Реестр не загружен. Проверьте файл с ошибками." + Environment.NewLine + ex.Message);
            }
        }
        //End

        public void LogBillLoader(ReestrInfo r, string comment)
        {
            cmdLogBillLoader.Connection.Close();
            cmdLogBillLoader.Parameters["@ID_Pack_Type"].Value = r.IdPackType;
            cmdLogBillLoader.Parameters["@Pack_Number"].Value = r.PackNumber;
            cmdLogBillLoader.Parameters["@Pack_Date"].Value = r.PackDate;
            cmdLogBillLoader.Parameters["@FileName"].Value = r.SourceFile.Name;
            cmdLogBillLoader.Parameters["@PayCount"].Value = r.PayCountLoadedInEllis;
            cmdLogBillLoader.Parameters["@PaySum"].Value = r.PaySummLoadedInEllis;
            cmdLogBillLoader.Parameters["@StartLoadTime"].Value = r.StartLoadTime;
            cmdLogBillLoader.Parameters["@EndLoadTime"].Value = r.EndLoadTime;
            cmdLogBillLoader.Parameters["@Comment"].Value = comment;
            cmdLogBillLoader.Connection.Open();
            cmdLogBillLoader.ExecuteNonQuery();
        }

        public void PackTypeSelect(MDataSet.Pack_TypeDataTable table)
        {
            daPackType.Fill(table);
        }

        public DataTable GetTmpLoadTable()
        {
            DataTable dt = new DataTable();
            daTmpLoad.Fill(dt);
            return dt;
        }

        public DataTable GetErrorTable()
        {
            DataTable dt = new DataTable();
            daErrorTable.Fill(dt);
            return dt;
        }

        public void SberSaldoReestrTypeSelect(MDataSet.Sber_Saldo_Reestr_TypeDataTable table)
        {
            daSberSaldoReestrType.Fill(table);
        }

        public void SberSaldoSelect(MDataSet.Sber_saldoDataTable table)
        {
            daSberSaldo.Fill(table);
        }

        public DataTable DetectBillFilterID(string account, int idClient)
        {
            DataTable dt = new DataTable();
            daDetectBillFilter.SelectCommand.Parameters["@account"].Value = account;
            daDetectBillFilter.SelectCommand.Parameters["@id_client"].Value = idClient;
            daDetectBillFilter.Fill(dt);
            return dt;
        }

        public void ExecuteNonQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.ExecuteNonQuery();
        }

        public object ExecuteScalarCommand(string sql)
        {
            cmdSubCommand.Connection.Close();
            cmdSubCommand.CommandText = sql;
            cmdSubCommand.Connection.Open();
            return cmdSubCommand.ExecuteScalar();
        }

        public object ExecuteScalarCommand1(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            return cmdAnyCommand.ExecuteScalar();
        }
    }
}
