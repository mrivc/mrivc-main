﻿using Payments.DS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Payments
{
    public partial class FrmOfdStatus : Form
    {
        public FrmOfdStatus()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtResult.Text = DAccess.CashTerminal.FNGetInfoExchangeStatus();
        }

        private void bntGetUnconfirmedDocCount_Click(object sender, EventArgs e)
        {
            txtResult.Text = string.Format("Кол-во неподтвержденных документов: {0}", DAccess.CashTerminal.FNGetUnconfirmedDocCount());
        }
    }
}
