﻿using MrivcControls.Data;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace MrivcControls
{
    public class EllisFilterSettings
    {
        string filterExtensions = "Фильтр Эллис ЖКХ|*.afl|Текстовый фильтр|*.txt";
        List<EllisAddress> addressList = new List<EllisAddress>();

        public List<EllisAddress> AddressList
        {
            get { return addressList; }
            set { addressList = value; }
        }

        public void AddAddress(int id, int type, string caption)
        {
            AddressList.Add(new EllisAddress() { ID = id, TypeID = type, Caption = caption });
        }

        void ReadXml(string fileName)
        {
            var root = XElement.Load(fileName);
            var adr = root.Element("Address");
            AddressList = (from a in adr.Elements()
                           select new EllisAddress()
                           {
                               ID = (int)a.Attribute("ID"),
                               TypeID = (int)a.Attribute("TypeID"),
                               DeriveCompany = (int)a.Attribute("DeriveCompany"),
                               District = (a.Attribute("District") == null ? -1 : (int)a.Attribute("District")),
                               Settlement = (a.Attribute("Settlement") == null ? -1 : (int)a.Attribute("Settlement")),
                               MaintainCompany = (a.Attribute("MaintainCompany") == null ? -1 : (int)a.Attribute("MaintainCompany")),
                               Caption = (string)a.Attribute("Caption"),
                           }
            ).ToList();
        }



        public DataTable AddressTable()
        {
            var root = new XElement("AddressSet");
            
            for (int i = 0; i < AddressList.Count; i++)
            {
                var adr = new XElement("Address");
                EllisAddress ea = AddressList[i];
                XElement[] a = {
                    new XElement("ID", ea.ID),
                    new XElement("TypeID", ea.TypeID),
                    new XElement("DeriveCompany", ea.DeriveCompany),
                }
                    ;
                adr.Add(a);
                root.Add(adr);
            }
            var ds = new DataSet();
            var xml = root.ToString();
            ds.ReadXml(new StringReader(xml));
            var dt = ds.Tables["Address"];
            return dt;
        }

        void WriteXml(string fileName)
        {
            var root = new XElement("EllisFilterSettings");
            var adr = new XElement("Address",
                    new XAttribute("Count", AddressList.Count));
            for (int i = 0; i < AddressList.Count; i++)
            {
                EllisAddress ea = AddressList[i];
                XElement a = new XElement($"A{i}",
                    new XAttribute("ID", ea.ID),
                    new XAttribute("TypeID", ea.TypeID),
                    new XAttribute("DeriveCompany", ea.DeriveCompany),
                    new XAttribute("District", ea.District),
                    new XAttribute("Settlement", ea.Settlement),
                    new XAttribute("MaintainCompany", ea.MaintainCompany),
                    new XAttribute("Caption", ea.Caption)
                    );
                adr.Add(a);
            }
            root.Add(adr);

            XmlWriterSettings settings = new XmlWriterSettings()
            {
                OmitXmlDeclaration = true,
                Indent = true
            };

            using (XmlWriter xw = XmlWriter.Create(fileName, settings))
                root.Save(xw);
        }

        public void LoadFromFile() => ShowFileDialog(new OpenFileDialog() { Filter =  filterExtensions});

        public void SaveToFile() => ShowFileDialog(new SaveFileDialog() { Filter = filterExtensions});

        void ShowFileDialog (FileDialog fileDialog)
        {
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = fileDialog.FileName;
                string fileExtension = Path.GetExtension(fileName);
                string dialogType = fileDialog.GetType().Name;
                switch (fileExtension.ToLower())
                {
                    case ".afl":
                        if (dialogType == "OpenFileDialog")
                            ReadXml(fileName);
                        else if (dialogType == "SaveFileDialog")
                            WriteXml(fileName);
                        break;
                    case ".txt":
                        if (dialogType == "OpenFileDialog")
                            ReadText(fileName);
                        else if (dialogType == "SaveFileDialog")
                            WriteText(fileName);
                        break;
                }
            }
        }

        void ReadText(string fileName)
        {
            string[] lines = File.ReadAllLines(fileName);
            foreach (string line in lines)
            {
                AddressList.Add(new EllisAddress()
                {
                    ID = int.Parse(line),
                    TypeID = 0,
                    Caption = $"ID={line}"
                });
            }
        }

        void WriteText(string fileName)
        {
            string[] ids =
                (from adr in AddressList
                where adr.TypeID == 0
                select adr.ID.ToString()).ToArray();

            File.WriteAllLines(fileName, ids);
        }

    }
}
