﻿namespace Payments
{
    partial class FrmOfdStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOfdStatus));
            this.txtResult = new System.Windows.Forms.RichTextBox();
            this.btnGetInfoExchangeStatus = new System.Windows.Forms.Button();
            this.bntGetUnconfirmedDocCount = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtResult
            // 
            this.txtResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtResult.Location = new System.Drawing.Point(12, 12);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(727, 318);
            this.txtResult.TabIndex = 0;
            this.txtResult.Text = "";
            // 
            // btnGetInfoExchangeStatus
            // 
            this.btnGetInfoExchangeStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGetInfoExchangeStatus.Location = new System.Drawing.Point(12, 341);
            this.btnGetInfoExchangeStatus.Name = "btnGetInfoExchangeStatus";
            this.btnGetInfoExchangeStatus.Size = new System.Drawing.Size(207, 23);
            this.btnGetInfoExchangeStatus.TabIndex = 1;
            this.btnGetInfoExchangeStatus.Text = "Получить статус инф. обмена";
            this.btnGetInfoExchangeStatus.UseVisualStyleBackColor = true;
            this.btnGetInfoExchangeStatus.Click += new System.EventHandler(this.button1_Click);
            // 
            // bntGetUnconfirmedDocCount
            // 
            this.bntGetUnconfirmedDocCount.Location = new System.Drawing.Point(225, 341);
            this.bntGetUnconfirmedDocCount.Name = "bntGetUnconfirmedDocCount";
            this.bntGetUnconfirmedDocCount.Size = new System.Drawing.Size(271, 23);
            this.bntGetUnconfirmedDocCount.TabIndex = 2;
            this.bntGetUnconfirmedDocCount.Text = "Запрос кол-ва неподтвержденных документов";
            this.bntGetUnconfirmedDocCount.UseVisualStyleBackColor = true;
            this.bntGetUnconfirmedDocCount.Click += new System.EventHandler(this.bntGetUnconfirmedDocCount_Click);
            // 
            // FrmOfdStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 376);
            this.Controls.Add(this.bntGetUnconfirmedDocCount);
            this.Controls.Add(this.btnGetInfoExchangeStatus);
            this.Controls.Add(this.txtResult);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmOfdStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Взаимодействие с ОФД";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtResult;
        private System.Windows.Forms.Button btnGetInfoExchangeStatus;
        private System.Windows.Forms.Button bntGetUnconfirmedDocCount;
    }
}