﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MrivcControls.Data;
using EllisBillFilter;
using DevExpress.XtraGrid.Views.Grid;

namespace MrivcControls.DataPickers
{
    public partial class TableColumnsPicker : UserControl
    {
        public TableColumnsPicker()
        {
            InitializeComponent();
        }

        public void LoadData(string databaseName, string tableName)
        {

            ddTableColumnsPicker.EditValue = string.Empty;
            ddTableColumnsPicker.Properties.DataSource = null;
            ddTableColumnsPicker.Properties.DisplayMember = "Name";
            ddTableColumnsPicker.Properties.ValueMember = "Name";
            ddTableColumnsPicker.Properties.DataSource = DAccess.DataModule.GetTableColumns(databaseName, tableName);
        }

        public string SelectedColumn
        {
            get
            {
                return ddTableColumnsPicker.EditValue.ToString();
            }
            set
            {
                ddTableColumnsPicker.EditValue = value;
            }
        }

        public BaseDropDown TablesDropDown
        {
            get
            {
                return ddTableColumnsPicker;
            }
        }

        public GridView TablesDropDownView
        {
            get
            {
                return ddTableColumnsPickerView;
            }
        }
    }
}
