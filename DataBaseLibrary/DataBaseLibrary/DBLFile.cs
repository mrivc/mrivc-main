﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataBaseLibrary
{
    public static class DBLFile
    {
        /// <summary>
        /// Метод выгружающий данные из таблицы в File
        /// </summary>
        public static void LoadFromTable(
            SqlConnection connection, 
            string commandText, 
            string fileName, 
            string expansion, 
            string encoding, 
            bool hasHeader = true)
        {
            SqlCommand command = connection.CreateCommand();
            command.CommandTimeout = 0;
            command.CommandText = commandText;

            SqlDataReader reader = command.ExecuteReader();

            var columns = new List<string>();
            for (int i = 0; i < reader.FieldCount; i++)
                columns.Add(reader.GetName(i));

            using (StreamWriter sw = new StreamWriter($@"{fileName}" + expansion, false, Encoding.GetEncoding(encoding)))
            {
                StringBuilder line = new StringBuilder();

                if (hasHeader)
                {
                    foreach (string column in columns)
                        line.Append($"{column};");

                    line.Remove(line.Length - 1, 1);

                    sw.WriteLine(line);

                    line = new StringBuilder("");
                }

                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                        line.Append($"{Config.CheckString(reader[columns[i]].ToString())};");

                    line.Remove(line.Length - 1, 1);

                    sw.WriteLine(line);

                    line = new StringBuilder("");
                }
            }
        }

        /// <summary>
        /// Метод выгружающий данные из объекта DataTable в File
        /// </summary>
        public static void LoadFromDataTable(
            DataTable dataTable,
            string fileName,
            string expansion,
            string encoding,
            bool hasHeader = true)
        {
            using (StreamWriter sw = new StreamWriter($@"{fileName}" + expansion, false, Encoding.GetEncoding(encoding)))
            {
                StringBuilder line = new StringBuilder();

                if (hasHeader)
                {
                    foreach (DataColumn column in dataTable.Columns)
                        line.Append($"{column.Caption.ToString()};");

                    line.Remove(line.Length - 1, 1);

                    sw.WriteLine(line);

                    line = new StringBuilder("");
                }

                foreach (DataRow row in dataTable.Rows)
                {
                    foreach (object obj in row.ItemArray)
                        line.Append($"{obj.ToString()};");

                    line.Remove(line.Length - 1, 1);

                    sw.WriteLine(line);

                    line = new StringBuilder("");
                }
            }
        }

        /// <summary>
        /// Метод выгружающий данные из File в таблицу
        /// Таблица должна уже быть создана и File должен под нее походить
        /// </summary>
        public static void LoadIntoTable(SqlConnection connection, string tableName, string fileName)
        {
            if (DataBase.IsTableExist(connection, tableName))
            {
                using (StreamReader reader = new StreamReader(fileName))
                {
                    string line = reader.ReadLine();
                    string[] columns = line.Split(';');
                    string[] parameters = null;

                    SqlCommand commandCheckIdInTable = connection.CreateCommand();
                    SqlCommand commandInsert = connection.CreateCommand();
                    SqlTransaction transaction;

                    while ((line = reader.ReadLine()) != null)
                    {
                        parameters = line.Split(';');

                        commandCheckIdInTable.CommandText =
                            $"select count(*) from {tableName} where {columns[0]} = {parameters[0]}";

                        if ((int)commandCheckIdInTable.ExecuteScalar() > 0)
                            continue;

                        commandInsert.CommandText = CreateCommand(tableName, columns, parameters);

                        transaction = connection.BeginTransaction();

                        commandInsert.Transaction = transaction;

                        try
                        {
                            commandInsert.ExecuteNonQuery();
                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                        }
                    }
                }
            }
        }

        public static string CreateCommand(string tableName, string[] columns, string[] parameters)
        {
            StringBuilder commandText = new StringBuilder();
            commandText.Append($"insert into {tableName} (");

            for (int i = 0; i < columns.Length - 1; i++)
                commandText.Append($"{columns[i]},");
            commandText.Append($"{columns[columns.Length - 1]}) \n");

            commandText.Append("values (");

            for (int i = 0; i < parameters.Length - 1; i++)
                commandText.Append($"{Config.ConvertNullStringInRealNull(parameters[i])},");
            commandText.Append($"{Config.ConvertNullStringInRealNull(parameters[parameters.Length - 1])})");

            return commandText.ToString();
        }
    }
}
