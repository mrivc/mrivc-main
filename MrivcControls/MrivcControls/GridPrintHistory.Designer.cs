﻿namespace MrivcControls
{
    partial class GridPrintHistory
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdPrintDetail_Checked = new System.Windows.Forms.ToolStripMenuItem();
            this.printHistoryBindingSource = new System.Windows.Forms.BindingSource();
            this.cDataSet = new MrivcControls.Data.CDataSet();
            this.periodBindingSource = new System.Windows.Forms.BindingSource();
            this.companyBindingSource = new System.Windows.Forms.BindingSource();
            this.billKindBindingSource = new System.Windows.Forms.BindingSource();
            this.employeeBindingSource = new System.Windows.Forms.BindingSource();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.cmdPrintHistory = new System.Windows.Forms.ContextMenuStrip();
            this.cmsPrintHistory_Checked = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cmsPrintHistory_Clear_IssuedDate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cmsPrintHistory_Filter = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsPrintHistory_Filter_CurrentDeriveCompany = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsPrintHistory_Filter_CurrentBillKind = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsPrintHistory_FilterClear = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cmsPrintHistory_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cmsPrintHistory_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsPrintHistory_ShowChanges = new System.Windows.Forms.ToolStripMenuItem();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Print = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrint_Count = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrint_Count_Without_Subscribers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colID_Bill_Kind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riBillKind = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colID_Debt_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssued_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeriveCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Delivery_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riDeliveryType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.printDeliveryTypeBindingSource = new System.Windows.Forms.BindingSource();
            this.colPrintedBackSide = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedDelay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintedBillsForSubscribers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageC2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridPrintHistoryDetail = new DevExpress.XtraGrid.GridControl();
            this.cmdPrintDetail = new System.Windows.Forms.ContextMenuStrip();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cmdPrintDetail_CheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdPrintDetail_UnCheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.printHistoryDetailBindingSource = new System.Windows.Forms.BindingSource();
            this.gridViewPrintHistoryDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Print1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_House = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Client_Detail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDerive_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBill_Filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouse_Address = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBill_Count = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouse_Balance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouse_Pays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouse_Tariff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouse_Recalculations = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouse_Benefit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChecked1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedBy1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBill_Count_Without_Subscribers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.cbAllUserHistory = new System.Windows.Forms.CheckBox();
            this.lblButtonFind = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDeriveCompany = new DevExpress.XtraEditors.TextEdit();
            this.cbIssueDelay = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.printHistoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billKindBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            this.cmdPrintHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillKind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeliveryType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printDeliveryTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageC2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPrintHistoryDetail)).BeginInit();
            this.cmdPrintDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printHistoryDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPrintHistoryDetail)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeriveCompany.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdPrintDetail_Checked
            // 
            this.cmdPrintDetail_Checked.Name = "cmdPrintDetail_Checked";
            this.cmdPrintDetail_Checked.Size = new System.Drawing.Size(294, 22);
            this.cmdPrintDetail_Checked.Text = "Проверено (установить/снять)";
            this.cmdPrintDetail_Checked.Click += new System.EventHandler(this.cmsPrintDetail_Checked_Click);
            // 
            // printHistoryBindingSource
            // 
            this.printHistoryBindingSource.DataMember = "Print_History";
            this.printHistoryBindingSource.DataSource = this.cDataSet;
            // 
            // cDataSet
            // 
            this.cDataSet.DataSetName = "CDataSet";
            this.cDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.cDataSet;
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataMember = "Company";
            this.companyBindingSource.DataSource = this.cDataSet;
            // 
            // billKindBindingSource
            // 
            this.billKindBindingSource.DataMember = "Bill_Kind";
            this.billKindBindingSource.DataSource = this.cDataSet;
            // 
            // employeeBindingSource
            // 
            this.employeeBindingSource.DataMember = "Employee";
            this.employeeBindingSource.DataSource = this.cDataSet;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(3, 46);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(972, 555);
            this.splitContainerControl1.SplitterPosition = 302;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SizeChanged += new System.EventHandler(this.splitContainerControl1_SizeChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.ContextMenuStrip = this.cmdPrintHistory;
            this.gridControl1.DataSource = this.printHistoryBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riEmployee,
            this.riCompany,
            this.riBillKind,
            this.riDeliveryType});
            this.gridControl1.Size = new System.Drawing.Size(972, 302);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // cmdPrintHistory
            // 
            this.cmdPrintHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmsPrintHistory_Checked,
            this.toolStripSeparator1,
            this.cmsPrintHistory_Clear_IssuedDate,
            this.toolStripSeparator5,
            this.cmsPrintHistory_Filter,
            this.cmsPrintHistory_FilterClear,
            this.toolStripSeparator3,
            this.cmsPrintHistory_Delete,
            this.toolStripSeparator2,
            this.cmsPrintHistory_Refresh,
            this.cmsPrintHistory_ShowChanges});
            this.cmdPrintHistory.Name = "cmdPrintHistory";
            this.cmdPrintHistory.Size = new System.Drawing.Size(244, 182);
            this.cmdPrintHistory.Opening += new System.ComponentModel.CancelEventHandler(this.cmdPrintHistory_Opening);
            // 
            // cmsPrintHistory_Checked
            // 
            this.cmsPrintHistory_Checked.Image = global::MrivcControls.Properties.Resources.check;
            this.cmsPrintHistory_Checked.Name = "cmsPrintHistory_Checked";
            this.cmsPrintHistory_Checked.Size = new System.Drawing.Size(243, 22);
            this.cmsPrintHistory_Checked.Text = "Проверено (установить/снять)";
            this.cmsPrintHistory_Checked.Click += new System.EventHandler(this.cmsPrintHistory_Checked_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(240, 6);
            // 
            // cmsPrintHistory_Clear_IssuedDate
            // 
            this.cmsPrintHistory_Clear_IssuedDate.Name = "cmsPrintHistory_Clear_IssuedDate";
            this.cmsPrintHistory_Clear_IssuedDate.Size = new System.Drawing.Size(243, 22);
            this.cmsPrintHistory_Clear_IssuedDate.Text = "Отозвать квитанции";
            this.cmsPrintHistory_Clear_IssuedDate.Click += new System.EventHandler(this.cmsPrintHistory_Clear_IssuedDate_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(240, 6);
            // 
            // cmsPrintHistory_Filter
            // 
            this.cmsPrintHistory_Filter.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter,
            this.cmsPrintHistory_Filter_CurrentDeriveCompany,
            this.cmsPrintHistory_Filter_CurrentBillKind});
            this.cmsPrintHistory_Filter.Image = global::MrivcControls.Properties.Resources.filter;
            this.cmsPrintHistory_Filter.Name = "cmsPrintHistory_Filter";
            this.cmsPrintHistory_Filter.Size = new System.Drawing.Size(243, 22);
            this.cmsPrintHistory_Filter.Text = "Фильтр по строке";
            // 
            // cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter
            // 
            this.cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter.Name = "cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter";
            this.cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter.Size = new System.Drawing.Size(393, 22);
            this.cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter.Text = "Фильтр записей по текущей УК и текущему Коду фильтра";
            this.cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter.Click += new System.EventHandler(this.cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter_Click);
            // 
            // cmsPrintHistory_Filter_CurrentDeriveCompany
            // 
            this.cmsPrintHistory_Filter_CurrentDeriveCompany.Name = "cmsPrintHistory_Filter_CurrentDeriveCompany";
            this.cmsPrintHistory_Filter_CurrentDeriveCompany.Size = new System.Drawing.Size(393, 22);
            this.cmsPrintHistory_Filter_CurrentDeriveCompany.Text = "Фильтр записей по текущей УК";
            this.cmsPrintHistory_Filter_CurrentDeriveCompany.Click += new System.EventHandler(this.cmsPrintHistory_Filter_CurrentDeriveCompany_Click);
            // 
            // cmsPrintHistory_Filter_CurrentBillKind
            // 
            this.cmsPrintHistory_Filter_CurrentBillKind.Name = "cmsPrintHistory_Filter_CurrentBillKind";
            this.cmsPrintHistory_Filter_CurrentBillKind.Size = new System.Drawing.Size(393, 22);
            this.cmsPrintHistory_Filter_CurrentBillKind.Text = "Фильтр записей по текущему Коду фильтра";
            this.cmsPrintHistory_Filter_CurrentBillKind.Click += new System.EventHandler(this.cmsPrintHistory_Filter_CurrentBillKind_Click);
            // 
            // cmsPrintHistory_FilterClear
            // 
            this.cmsPrintHistory_FilterClear.Image = global::MrivcControls.Properties.Resources.uncheck;
            this.cmsPrintHistory_FilterClear.Name = "cmsPrintHistory_FilterClear";
            this.cmsPrintHistory_FilterClear.Size = new System.Drawing.Size(243, 22);
            this.cmsPrintHistory_FilterClear.Text = "Снять все фильтры";
            this.cmsPrintHistory_FilterClear.Click += new System.EventHandler(this.cmsPrintHistory_FilterClear_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(240, 6);
            // 
            // cmsPrintHistory_Delete
            // 
            this.cmsPrintHistory_Delete.Image = global::MrivcControls.Properties.Resources.document_delete;
            this.cmsPrintHistory_Delete.Name = "cmsPrintHistory_Delete";
            this.cmsPrintHistory_Delete.Size = new System.Drawing.Size(243, 22);
            this.cmsPrintHistory_Delete.Text = "Удалить запись";
            this.cmsPrintHistory_Delete.Click += new System.EventHandler(this.cmsPrintHistory_Delete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(240, 6);
            // 
            // cmsPrintHistory_Refresh
            // 
            this.cmsPrintHistory_Refresh.Name = "cmsPrintHistory_Refresh";
            this.cmsPrintHistory_Refresh.Size = new System.Drawing.Size(243, 22);
            this.cmsPrintHistory_Refresh.Text = "Обновить";
            this.cmsPrintHistory_Refresh.Click += new System.EventHandler(this.cmsPrintHistory_Refresh_Click);
            // 
            // cmsPrintHistory_ShowChanges
            // 
            this.cmsPrintHistory_ShowChanges.Name = "cmsPrintHistory_ShowChanges";
            this.cmsPrintHistory_ShowChanges.Size = new System.Drawing.Size(243, 22);
            this.cmsPrintHistory_ShowChanges.Text = "Показать историю изменений";
            this.cmsPrintHistory_ShowChanges.Click += new System.EventHandler(this.показатьИсториюИзмененийToolStripMenuItem_Click);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.ColumnPanelRowHeight = 60;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Print,
            this.colPeriod,
            this.colPrintDate,
            this.colPrintBy,
            this.colTotalQuantity,
            this.colPrint_Count,
            this.colPrint_Count_Without_Subscribers,
            this.colID_Client,
            this.colID_Bill_Kind,
            this.colID_Debt_Company,
            this.colChecked,
            this.colCheckedBy,
            this.colIssued_Date,
            this.colIssuedBy,
            this.colComment,
            this.colDeriveCompany,
            this.colID_Delivery_Type,
            this.colPrintedBackSide,
            this.colIssuedDelay,
            this.colPrintedBillsForSubscribers});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1288, 324, 216, 458);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Перетащите сюда наименование столбцов по которым хотите сгруппировать записи";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colID_Print
            // 
            this.colID_Print.FieldName = "ID_Print";
            this.colID_Print.Name = "colID_Print";
            this.colID_Print.OptionsColumn.AllowEdit = false;
            // 
            // colPeriod
            // 
            this.colPeriod.FieldName = "Period";
            this.colPeriod.Name = "colPeriod";
            this.colPeriod.OptionsColumn.AllowEdit = false;
            // 
            // colPrintDate
            // 
            this.colPrintDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrintDate.AppearanceCell.Options.UseBackColor = true;
            this.colPrintDate.Caption = "Дата печати";
            this.colPrintDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm tt";
            this.colPrintDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPrintDate.FieldName = "PrintDate";
            this.colPrintDate.Name = "colPrintDate";
            this.colPrintDate.OptionsColumn.AllowEdit = false;
            this.colPrintDate.Visible = true;
            this.colPrintDate.VisibleIndex = 1;
            this.colPrintDate.Width = 96;
            // 
            // colPrintBy
            // 
            this.colPrintBy.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrintBy.AppearanceCell.Options.UseBackColor = true;
            this.colPrintBy.Caption = "Сотрудник";
            this.colPrintBy.ColumnEdit = this.riEmployee;
            this.colPrintBy.FieldName = "PrintBy";
            this.colPrintBy.Name = "colPrintBy";
            this.colPrintBy.OptionsColumn.AllowEdit = false;
            this.colPrintBy.SummaryItem.DisplayFormat = "{0}";
            this.colPrintBy.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.colPrintBy.Visible = true;
            this.colPrintBy.VisibleIndex = 0;
            this.colPrintBy.Width = 99;
            // 
            // riEmployee
            // 
            this.riEmployee.AutoHeight = false;
            this.riEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riEmployee.DataSource = this.employeeBindingSource;
            this.riEmployee.DisplayMember = "FIO";
            this.riEmployee.Name = "riEmployee";
            this.riEmployee.NullText = "";
            this.riEmployee.ValueMember = "ID_Employee";
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colTotalQuantity.AppearanceCell.Options.UseBackColor = true;
            this.colTotalQuantity.Caption = "Кол-во Обслуживаемых ЛС";
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.OptionsColumn.AllowEdit = false;
            this.colTotalQuantity.OptionsColumn.ReadOnly = true;
            this.colTotalQuantity.Visible = true;
            this.colTotalQuantity.VisibleIndex = 6;
            this.colTotalQuantity.Width = 106;
            // 
            // colPrint_Count
            // 
            this.colPrint_Count.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrint_Count.AppearanceCell.Options.UseBackColor = true;
            this.colPrint_Count.Caption = "Общее количество квитанций";
            this.colPrint_Count.FieldName = "Print_Count";
            this.colPrint_Count.Name = "colPrint_Count";
            this.colPrint_Count.OptionsColumn.AllowEdit = false;
            this.colPrint_Count.SummaryItem.DisplayFormat = "SUM={0:#.##}";
            this.colPrint_Count.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPrint_Count.Visible = true;
            this.colPrint_Count.VisibleIndex = 7;
            this.colPrint_Count.Width = 96;
            // 
            // colPrint_Count_Without_Subscribers
            // 
            this.colPrint_Count_Without_Subscribers.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrint_Count_Without_Subscribers.AppearanceCell.Options.UseBackColor = true;
            this.colPrint_Count_Without_Subscribers.Caption = "Кол-во квитанций исключая подписчиков на Email (для Курьера)";
            this.colPrint_Count_Without_Subscribers.FieldName = "Print_Count_Without_Subscribers";
            this.colPrint_Count_Without_Subscribers.Name = "colPrint_Count_Without_Subscribers";
            this.colPrint_Count_Without_Subscribers.OptionsColumn.AllowEdit = false;
            this.colPrint_Count_Without_Subscribers.Visible = true;
            this.colPrint_Count_Without_Subscribers.VisibleIndex = 8;
            this.colPrint_Count_Without_Subscribers.Width = 126;
            // 
            // colID_Client
            // 
            this.colID_Client.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colID_Client.AppearanceCell.Options.UseBackColor = true;
            this.colID_Client.Caption = "Получатель платежей";
            this.colID_Client.ColumnEdit = this.riCompany;
            this.colID_Client.FieldName = "ID_Client";
            this.colID_Client.Name = "colID_Client";
            this.colID_Client.OptionsColumn.AllowEdit = false;
            this.colID_Client.Visible = true;
            this.colID_Client.VisibleIndex = 3;
            this.colID_Client.Width = 196;
            // 
            // riCompany
            // 
            this.riCompany.AutoHeight = false;
            this.riCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riCompany.DataSource = this.companyBindingSource;
            this.riCompany.DisplayMember = "Name";
            this.riCompany.Name = "riCompany";
            this.riCompany.NullText = "";
            this.riCompany.ValueMember = "ID_Company";
            // 
            // colID_Bill_Kind
            // 
            this.colID_Bill_Kind.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colID_Bill_Kind.AppearanceCell.Options.UseBackColor = true;
            this.colID_Bill_Kind.Caption = "Вид квитанции";
            this.colID_Bill_Kind.ColumnEdit = this.riBillKind;
            this.colID_Bill_Kind.FieldName = "ID_Bill_Kind";
            this.colID_Bill_Kind.Name = "colID_Bill_Kind";
            this.colID_Bill_Kind.OptionsColumn.AllowEdit = false;
            this.colID_Bill_Kind.Visible = true;
            this.colID_Bill_Kind.VisibleIndex = 5;
            this.colID_Bill_Kind.Width = 135;
            // 
            // riBillKind
            // 
            this.riBillKind.AutoHeight = false;
            this.riBillKind.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riBillKind.DataSource = this.billKindBindingSource;
            this.riBillKind.DisplayMember = "Bill_Kind";
            this.riBillKind.Name = "riBillKind";
            this.riBillKind.ValueMember = "ID_Bill_Kind";
            // 
            // colID_Debt_Company
            // 
            this.colID_Debt_Company.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Debt_Company.AppearanceCell.Options.UseBackColor = true;
            this.colID_Debt_Company.Caption = "Долговая УК";
            this.colID_Debt_Company.ColumnEdit = this.riCompany;
            this.colID_Debt_Company.FieldName = "ID_Debt_Company";
            this.colID_Debt_Company.Name = "colID_Debt_Company";
            this.colID_Debt_Company.OptionsColumn.AllowEdit = false;
            this.colID_Debt_Company.Visible = true;
            this.colID_Debt_Company.VisibleIndex = 9;
            this.colID_Debt_Company.Width = 97;
            // 
            // colChecked
            // 
            this.colChecked.AppearanceCell.BackColor = System.Drawing.Color.Wheat;
            this.colChecked.AppearanceCell.Options.UseBackColor = true;
            this.colChecked.Caption = "Проверено";
            this.colChecked.FieldName = "Checked";
            this.colChecked.Name = "colChecked";
            this.colChecked.OptionsColumn.AllowEdit = false;
            this.colChecked.Visible = true;
            this.colChecked.VisibleIndex = 10;
            this.colChecked.Width = 76;
            // 
            // colCheckedBy
            // 
            this.colCheckedBy.Caption = "Проверил сотрудник";
            this.colCheckedBy.ColumnEdit = this.riEmployee;
            this.colCheckedBy.FieldName = "CheckedBy";
            this.colCheckedBy.Name = "colCheckedBy";
            this.colCheckedBy.OptionsColumn.AllowEdit = false;
            // 
            // colIssued_Date
            // 
            this.colIssued_Date.Caption = "Выданы клиенту";
            this.colIssued_Date.FieldName = "Issued_Date";
            this.colIssued_Date.Name = "colIssued_Date";
            this.colIssued_Date.Visible = true;
            this.colIssued_Date.VisibleIndex = 11;
            this.colIssued_Date.Width = 79;
            // 
            // colIssuedBy
            // 
            this.colIssuedBy.Caption = "Выдал сотрудник";
            this.colIssuedBy.ColumnEdit = this.riEmployee;
            this.colIssuedBy.FieldName = "IssuedBy";
            this.colIssuedBy.Name = "colIssuedBy";
            this.colIssuedBy.OptionsColumn.AllowEdit = false;
            // 
            // colComment
            // 
            this.colComment.Caption = "Комментарий";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            // 
            // colDeriveCompany
            // 
            this.colDeriveCompany.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colDeriveCompany.AppearanceCell.Options.UseBackColor = true;
            this.colDeriveCompany.Caption = "Управляющая компания";
            this.colDeriveCompany.FieldName = "DeriveCompany";
            this.colDeriveCompany.Name = "colDeriveCompany";
            this.colDeriveCompany.OptionsColumn.AllowEdit = false;
            this.colDeriveCompany.Visible = true;
            this.colDeriveCompany.VisibleIndex = 4;
            this.colDeriveCompany.Width = 113;
            // 
            // colID_Delivery_Type
            // 
            this.colID_Delivery_Type.Caption = "Способ доставки";
            this.colID_Delivery_Type.ColumnEdit = this.riDeliveryType;
            this.colID_Delivery_Type.FieldName = "ID_Delivery_Type";
            this.colID_Delivery_Type.Name = "colID_Delivery_Type";
            this.colID_Delivery_Type.Visible = true;
            this.colID_Delivery_Type.VisibleIndex = 12;
            this.colID_Delivery_Type.Width = 98;
            // 
            // riDeliveryType
            // 
            this.riDeliveryType.AutoHeight = false;
            this.riDeliveryType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riDeliveryType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Delivery_Type", "ID_Delivery_Type", 109, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riDeliveryType.DataSource = this.printDeliveryTypeBindingSource;
            this.riDeliveryType.DisplayMember = "Name";
            this.riDeliveryType.Name = "riDeliveryType";
            this.riDeliveryType.NullText = "";
            this.riDeliveryType.ShowFooter = false;
            this.riDeliveryType.ShowHeader = false;
            this.riDeliveryType.ValueMember = "ID_Delivery_Type";
            // 
            // printDeliveryTypeBindingSource
            // 
            this.printDeliveryTypeBindingSource.DataMember = "Print_Delivery_Type";
            this.printDeliveryTypeBindingSource.DataSource = this.cDataSet;
            // 
            // colPrintedBackSide
            // 
            this.colPrintedBackSide.AppearanceCell.BackColor = System.Drawing.Color.Beige;
            this.colPrintedBackSide.AppearanceCell.Options.UseBackColor = true;
            this.colPrintedBackSide.Caption = "Печать обратной стороны";
            this.colPrintedBackSide.FieldName = "PrintedBackSide";
            this.colPrintedBackSide.Name = "colPrintedBackSide";
            this.colPrintedBackSide.Visible = true;
            this.colPrintedBackSide.VisibleIndex = 2;
            this.colPrintedBackSide.Width = 110;
            // 
            // colIssuedDelay
            // 
            this.colIssuedDelay.Caption = "Задержка выдачи (дней)";
            this.colIssuedDelay.FieldName = "IssuedDelay";
            this.colIssuedDelay.Name = "colIssuedDelay";
            this.colIssuedDelay.OptionsColumn.AllowEdit = false;
            this.colIssuedDelay.OptionsColumn.ReadOnly = true;
            this.colIssuedDelay.Visible = true;
            this.colIssuedDelay.VisibleIndex = 13;
            // 
            // colPrintedBillsForSubscribers
            // 
            this.colPrintedBillsForSubscribers.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrintedBillsForSubscribers.AppearanceCell.Options.UseBackColor = true;
            this.colPrintedBillsForSubscribers.Caption = "Напечатаны квитанции подписчикам на Email ";
            this.colPrintedBillsForSubscribers.FieldName = "PrintedBillsForSubscribers";
            this.colPrintedBillsForSubscribers.Name = "colPrintedBillsForSubscribers";
            this.colPrintedBillsForSubscribers.OptionsColumn.AllowEdit = false;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageC2;
            this.xtraTabControl1.Size = new System.Drawing.Size(972, 247);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageC2,
            this.xtraTabPage2});
            // 
            // xtraTabPageC2
            // 
            this.xtraTabPageC2.Controls.Add(this.gridPrintHistoryDetail);
            this.xtraTabPageC2.Name = "xtraTabPageC2";
            this.xtraTabPageC2.Size = new System.Drawing.Size(965, 218);
            this.xtraTabPageC2.Text = "Детализация печати";
            // 
            // gridPrintHistoryDetail
            // 
            this.gridPrintHistoryDetail.ContextMenuStrip = this.cmdPrintDetail;
            this.gridPrintHistoryDetail.DataSource = this.printHistoryDetailBindingSource;
            this.gridPrintHistoryDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridPrintHistoryDetail.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridPrintHistoryDetail.Location = new System.Drawing.Point(0, 0);
            this.gridPrintHistoryDetail.MainView = this.gridViewPrintHistoryDetail;
            this.gridPrintHistoryDetail.Name = "gridPrintHistoryDetail";
            this.gridPrintHistoryDetail.Size = new System.Drawing.Size(965, 218);
            this.gridPrintHistoryDetail.TabIndex = 0;
            this.gridPrintHistoryDetail.UseEmbeddedNavigator = true;
            this.gridPrintHistoryDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPrintHistoryDetail});
            // 
            // cmdPrintDetail
            // 
            this.cmdPrintDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdPrintDetail_Checked,
            this.toolStripSeparator4,
            this.cmdPrintDetail_CheckAll,
            this.cmdPrintDetail_UnCheckAll});
            this.cmdPrintDetail.Name = "cmdPrintDetail";
            this.cmdPrintDetail.Size = new System.Drawing.Size(295, 76);
            this.cmdPrintDetail.Opening += new System.ComponentModel.CancelEventHandler(this.cmdPrintDetail_Opening);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(291, 6);
            // 
            // cmdPrintDetail_CheckAll
            // 
            this.cmdPrintDetail_CheckAll.Name = "cmdPrintDetail_CheckAll";
            this.cmdPrintDetail_CheckAll.Size = new System.Drawing.Size(294, 22);
            this.cmdPrintDetail_CheckAll.Text = "Установить статус Проверено (для всех)";
            this.cmdPrintDetail_CheckAll.Click += new System.EventHandler(this.cmdPrintDetail_CheckAll_Click);
            // 
            // cmdPrintDetail_UnCheckAll
            // 
            this.cmdPrintDetail_UnCheckAll.Name = "cmdPrintDetail_UnCheckAll";
            this.cmdPrintDetail_UnCheckAll.Size = new System.Drawing.Size(294, 22);
            this.cmdPrintDetail_UnCheckAll.Text = "Снять статус Проверки (для всех)";
            this.cmdPrintDetail_UnCheckAll.Click += new System.EventHandler(this.cmdPrintDetail_UnCheckAll_Click);
            // 
            // printHistoryDetailBindingSource
            // 
            this.printHistoryDetailBindingSource.DataMember = "Print_History_Detail";
            this.printHistoryDetailBindingSource.DataSource = this.cDataSet;
            // 
            // gridViewPrintHistoryDetail
            // 
            this.gridViewPrintHistoryDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewPrintHistoryDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewPrintHistoryDetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewPrintHistoryDetail.ColumnPanelRowHeight = 40;
            this.gridViewPrintHistoryDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Print1,
            this.colID_House,
            this.colID_Client_Detail,
            this.colDerive_Company,
            this.colBill_Filter,
            this.colHouse_Address,
            this.colBill_Count,
            this.colHouse_Balance,
            this.colHouse_Pays,
            this.colHouse_Tariff,
            this.colHouse_Recalculations,
            this.colHouse_Benefit,
            this.colChecked1,
            this.colCheckedBy1,
            this.colBill_Count_Without_Subscribers});
            this.gridViewPrintHistoryDetail.GridControl = this.gridPrintHistoryDetail;
            this.gridViewPrintHistoryDetail.GroupPanelText = "Перетащите сюда наименование столбцов по которым хотите сгруппировать записи";
            this.gridViewPrintHistoryDetail.Name = "gridViewPrintHistoryDetail";
            this.gridViewPrintHistoryDetail.OptionsView.ShowFooter = true;
            this.gridViewPrintHistoryDetail.OptionsView.ShowGroupPanel = false;
            // 
            // colID_Print1
            // 
            this.colID_Print1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Print1.AppearanceCell.Options.UseBackColor = true;
            this.colID_Print1.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Print1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Print1.FieldName = "ID_Print";
            this.colID_Print1.Name = "colID_Print1";
            this.colID_Print1.OptionsColumn.AllowEdit = false;
            // 
            // colID_House
            // 
            this.colID_House.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_House.AppearanceCell.Options.UseBackColor = true;
            this.colID_House.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_House.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_House.FieldName = "ID_House";
            this.colID_House.Name = "colID_House";
            this.colID_House.OptionsColumn.AllowEdit = false;
            // 
            // colID_Client_Detail
            // 
            this.colID_Client_Detail.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Client_Detail.AppearanceCell.Options.UseBackColor = true;
            this.colID_Client_Detail.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Client_Detail.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Client_Detail.FieldName = "ID_Client_Detail";
            this.colID_Client_Detail.Name = "colID_Client_Detail";
            this.colID_Client_Detail.OptionsColumn.AllowEdit = false;
            // 
            // colDerive_Company
            // 
            this.colDerive_Company.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colDerive_Company.AppearanceCell.Options.UseBackColor = true;
            this.colDerive_Company.AppearanceHeader.Options.UseTextOptions = true;
            this.colDerive_Company.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDerive_Company.Caption = "Управляющая компания";
            this.colDerive_Company.FieldName = "Derive_Company";
            this.colDerive_Company.Name = "colDerive_Company";
            this.colDerive_Company.OptionsColumn.AllowEdit = false;
            this.colDerive_Company.Visible = true;
            this.colDerive_Company.VisibleIndex = 0;
            this.colDerive_Company.Width = 163;
            // 
            // colBill_Filter
            // 
            this.colBill_Filter.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colBill_Filter.AppearanceCell.Options.UseBackColor = true;
            this.colBill_Filter.AppearanceHeader.Options.UseTextOptions = true;
            this.colBill_Filter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBill_Filter.Caption = "Код фильтра";
            this.colBill_Filter.FieldName = "Bill_Filter";
            this.colBill_Filter.Name = "colBill_Filter";
            this.colBill_Filter.OptionsColumn.AllowEdit = false;
            this.colBill_Filter.Visible = true;
            this.colBill_Filter.VisibleIndex = 1;
            this.colBill_Filter.Width = 108;
            // 
            // colHouse_Address
            // 
            this.colHouse_Address.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colHouse_Address.AppearanceCell.Options.UseBackColor = true;
            this.colHouse_Address.AppearanceHeader.Options.UseTextOptions = true;
            this.colHouse_Address.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHouse_Address.Caption = "Дом";
            this.colHouse_Address.FieldName = "House_Address";
            this.colHouse_Address.Name = "colHouse_Address";
            this.colHouse_Address.OptionsColumn.AllowEdit = false;
            this.colHouse_Address.Visible = true;
            this.colHouse_Address.VisibleIndex = 2;
            this.colHouse_Address.Width = 166;
            // 
            // colBill_Count
            // 
            this.colBill_Count.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colBill_Count.AppearanceCell.Options.UseBackColor = true;
            this.colBill_Count.AppearanceHeader.Options.UseTextOptions = true;
            this.colBill_Count.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBill_Count.Caption = "Общее количество квитанций";
            this.colBill_Count.FieldName = "Bill_Count";
            this.colBill_Count.Name = "colBill_Count";
            this.colBill_Count.OptionsColumn.AllowEdit = false;
            this.colBill_Count.Visible = true;
            this.colBill_Count.VisibleIndex = 3;
            this.colBill_Count.Width = 134;
            // 
            // colHouse_Balance
            // 
            this.colHouse_Balance.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colHouse_Balance.AppearanceCell.Options.UseBackColor = true;
            this.colHouse_Balance.AppearanceHeader.Options.UseTextOptions = true;
            this.colHouse_Balance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHouse_Balance.Caption = "Баланс";
            this.colHouse_Balance.FieldName = "House_Balance";
            this.colHouse_Balance.Name = "colHouse_Balance";
            this.colHouse_Balance.OptionsColumn.AllowEdit = false;
            // 
            // colHouse_Pays
            // 
            this.colHouse_Pays.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colHouse_Pays.AppearanceCell.Options.UseBackColor = true;
            this.colHouse_Pays.AppearanceHeader.Options.UseTextOptions = true;
            this.colHouse_Pays.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHouse_Pays.Caption = "Платежи";
            this.colHouse_Pays.FieldName = "House_Pays";
            this.colHouse_Pays.Name = "colHouse_Pays";
            this.colHouse_Pays.OptionsColumn.AllowEdit = false;
            // 
            // colHouse_Tariff
            // 
            this.colHouse_Tariff.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colHouse_Tariff.AppearanceCell.Options.UseBackColor = true;
            this.colHouse_Tariff.AppearanceHeader.Options.UseTextOptions = true;
            this.colHouse_Tariff.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHouse_Tariff.Caption = "Начисления";
            this.colHouse_Tariff.FieldName = "House_Tariff";
            this.colHouse_Tariff.Name = "colHouse_Tariff";
            this.colHouse_Tariff.OptionsColumn.AllowEdit = false;
            this.colHouse_Tariff.Visible = true;
            this.colHouse_Tariff.VisibleIndex = 5;
            this.colHouse_Tariff.Width = 101;
            // 
            // colHouse_Recalculations
            // 
            this.colHouse_Recalculations.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colHouse_Recalculations.AppearanceCell.Options.UseBackColor = true;
            this.colHouse_Recalculations.AppearanceHeader.Options.UseTextOptions = true;
            this.colHouse_Recalculations.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHouse_Recalculations.Caption = "Перерасчеты";
            this.colHouse_Recalculations.FieldName = "House_Recalculations";
            this.colHouse_Recalculations.Name = "colHouse_Recalculations";
            this.colHouse_Recalculations.OptionsColumn.AllowEdit = false;
            // 
            // colHouse_Benefit
            // 
            this.colHouse_Benefit.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colHouse_Benefit.AppearanceCell.Options.UseBackColor = true;
            this.colHouse_Benefit.AppearanceHeader.Options.UseTextOptions = true;
            this.colHouse_Benefit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHouse_Benefit.Caption = "Льготы";
            this.colHouse_Benefit.FieldName = "House_Benefit";
            this.colHouse_Benefit.Name = "colHouse_Benefit";
            this.colHouse_Benefit.OptionsColumn.AllowEdit = false;
            // 
            // colChecked1
            // 
            this.colChecked1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.colChecked1.AppearanceCell.Options.UseBackColor = true;
            this.colChecked1.AppearanceHeader.Options.UseTextOptions = true;
            this.colChecked1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colChecked1.Caption = "Проверено";
            this.colChecked1.FieldName = "Checked";
            this.colChecked1.Name = "colChecked1";
            this.colChecked1.OptionsColumn.AllowEdit = false;
            this.colChecked1.Visible = true;
            this.colChecked1.VisibleIndex = 6;
            this.colChecked1.Width = 138;
            // 
            // colCheckedBy1
            // 
            this.colCheckedBy1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colCheckedBy1.AppearanceCell.Options.UseBackColor = true;
            this.colCheckedBy1.AppearanceHeader.Options.UseTextOptions = true;
            this.colCheckedBy1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCheckedBy1.Caption = "Проверил сотрудник";
            this.colCheckedBy1.FieldName = "CheckedBy";
            this.colCheckedBy1.Name = "colCheckedBy1";
            this.colCheckedBy1.OptionsColumn.AllowEdit = false;
            // 
            // colBill_Count_Without_Subscribers
            // 
            this.colBill_Count_Without_Subscribers.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colBill_Count_Without_Subscribers.AppearanceCell.Options.UseBackColor = true;
            this.colBill_Count_Without_Subscribers.AppearanceHeader.Options.UseTextOptions = true;
            this.colBill_Count_Without_Subscribers.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBill_Count_Without_Subscribers.Caption = "Кол-во квитанций искл-я Email (для Курьера)";
            this.colBill_Count_Without_Subscribers.FieldName = "Bill_Count_Without_Subscribers";
            this.colBill_Count_Without_Subscribers.Name = "colBill_Count_Without_Subscribers";
            this.colBill_Count_Without_Subscribers.OptionsColumn.AllowEdit = false;
            this.colBill_Count_Without_Subscribers.OptionsColumn.ReadOnly = true;
            this.colBill_Count_Without_Subscribers.Visible = true;
            this.colBill_Count_Without_Subscribers.VisibleIndex = 4;
            this.colBill_Count_Without_Subscribers.Width = 149;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.txtComment);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(965, 218);
            this.xtraTabPage2.Text = "Комментарий";
            // 
            // txtComment
            // 
            this.txtComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtComment.Location = new System.Drawing.Point(0, 0);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(965, 218);
            this.txtComment.TabIndex = 0;
            this.txtComment.Leave += new System.EventHandler(this.txtComment_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Период:";
            // 
            // cbPeriod
            // 
            this.cbPeriod.Location = new System.Drawing.Point(67, 13);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPeriod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Period", "Period", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Month", "Month", 40, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Year", "Year", 32, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Work_Period", "Is_Work_Period", 86, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.cbPeriod.Properties.DataSource = this.periodBindingSource;
            this.cbPeriod.Properties.DisplayMember = "Name";
            this.cbPeriod.Properties.DropDownRows = 12;
            this.cbPeriod.Properties.NullText = "";
            this.cbPeriod.Properties.ShowFooter = false;
            this.cbPeriod.Properties.ShowHeader = false;
            this.cbPeriod.Properties.ShowLines = false;
            this.cbPeriod.Properties.ValueMember = "Period";
            this.cbPeriod.Size = new System.Drawing.Size(187, 20);
            this.cbPeriod.TabIndex = 4;
            this.cbPeriod.EditValueChanged += new System.EventHandler(this.cbPeriod_EditValueChanged);
            // 
            // cbAllUserHistory
            // 
            this.cbAllUserHistory.Location = new System.Drawing.Point(268, 6);
            this.cbAllUserHistory.Name = "cbAllUserHistory";
            this.cbAllUserHistory.Size = new System.Drawing.Size(171, 34);
            this.cbAllUserHistory.TabIndex = 5;
            this.cbAllUserHistory.Text = "показать историю печати по всем пользователям";
            this.cbAllUserHistory.UseVisualStyleBackColor = true;
            this.cbAllUserHistory.CheckedChanged += new System.EventHandler(this.cbAllUserHistory_CheckedChanged);
            // 
            // lblButtonFind
            // 
            this.lblButtonFind.AutoSize = true;
            this.lblButtonFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblButtonFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblButtonFind.ForeColor = System.Drawing.Color.Blue;
            this.lblButtonFind.Location = new System.Drawing.Point(721, 16);
            this.lblButtonFind.Name = "lblButtonFind";
            this.lblButtonFind.Size = new System.Drawing.Size(57, 13);
            this.lblButtonFind.TabIndex = 16;
            this.lblButtonFind.Text = "найти >>>";
            this.lblButtonFind.Click += new System.EventHandler(this.lblButtonFind_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(464, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Фильтр по УК:";
            // 
            // txtDeriveCompany
            // 
            this.txtDeriveCompany.Location = new System.Drawing.Point(550, 13);
            this.txtDeriveCompany.Name = "txtDeriveCompany";
            this.txtDeriveCompany.Size = new System.Drawing.Size(166, 20);
            this.txtDeriveCompany.TabIndex = 14;
            this.txtDeriveCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDeriveCompany_KeyDown);
            // 
            // cbIssueDelay
            // 
            this.cbIssueDelay.AutoSize = true;
            this.cbIssueDelay.BackColor = System.Drawing.Color.Red;
            this.cbIssueDelay.Location = new System.Drawing.Point(808, 16);
            this.cbIssueDelay.Name = "cbIssueDelay";
            this.cbIssueDelay.Size = new System.Drawing.Size(237, 17);
            this.cbIssueDelay.TabIndex = 17;
            this.cbIssueDelay.Text = "показать только не выданные квитанции";
            this.cbIssueDelay.UseVisualStyleBackColor = false;
            this.cbIssueDelay.CheckedChanged += new System.EventHandler(this.cbIssueDelay_CheckedChanged);
            // 
            // GridPrintHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbIssueDelay);
            this.Controls.Add(this.lblButtonFind);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDeriveCompany);
            this.Controls.Add(this.cbAllUserHistory);
            this.Controls.Add(this.cbPeriod);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "GridPrintHistory";
            this.Size = new System.Drawing.Size(978, 604);
            ((System.ComponentModel.ISupportInitialize)(this.printHistoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billKindBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            this.cmdPrintHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillKind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeliveryType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printDeliveryTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageC2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPrintHistoryDetail)).EndInit();
            this.cmdPrintDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printHistoryDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPrintHistoryDetail)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeriveCompany.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource printHistoryBindingSource;
        private MrivcControls.Data.CDataSet cDataSet;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private System.Windows.Forms.BindingSource employeeBindingSource;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private System.Windows.Forms.BindingSource billKindBindingSource;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageC2;
        private DevExpress.XtraGrid.GridControl gridPrintHistoryDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPrintHistoryDetail;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private System.Windows.Forms.BindingSource printHistoryDetailBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Print1;
        private DevExpress.XtraGrid.Columns.GridColumn colID_House;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client_Detail;
        private DevExpress.XtraGrid.Columns.GridColumn colBill_Count;
        private DevExpress.XtraGrid.Columns.GridColumn colHouse_Balance;
        private DevExpress.XtraGrid.Columns.GridColumn colHouse_Pays;
        private DevExpress.XtraGrid.Columns.GridColumn colHouse_Tariff;
        private DevExpress.XtraGrid.Columns.GridColumn colHouse_Recalculations;
        private DevExpress.XtraGrid.Columns.GridColumn colHouse_Benefit;
        private DevExpress.XtraGrid.Columns.GridColumn colChecked1;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedBy1;
        private DevExpress.XtraGrid.Columns.GridColumn colHouse_Address;
        private DevExpress.XtraGrid.Columns.GridColumn colDerive_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colBill_Filter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Print;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintBy;
        private DevExpress.XtraGrid.Columns.GridColumn colPrint_Count;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Bill_Kind;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Debt_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colChecked;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colIssued_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedBy;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riCompany;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riBillKind;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit cbPeriod;
        private System.Windows.Forms.CheckBox cbAllUserHistory;
        private System.Windows.Forms.ContextMenuStrip cmdPrintHistory;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_Refresh;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_Checked;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_Clear_IssuedDate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_Delete;
        private System.Windows.Forms.ContextMenuStrip cmdPrintDetail;
        private System.Windows.Forms.ToolStripMenuItem cmdPrintDetail_CheckAll;
        private System.Windows.Forms.ToolStripMenuItem cmdPrintDetail_UnCheckAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem cmdPrintDetail_Checked;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private System.Windows.Forms.TextBox txtComment;
        private DevExpress.XtraGrid.Columns.GridColumn colDeriveCompany;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_Filter;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_Filter_CurrentDeriveCompany;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_Filter_CurrentBillKind;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_FilterClear;
        private System.Windows.Forms.Label lblButtonFind;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtDeriveCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Delivery_Type;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintedBackSide;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riDeliveryType;
        private System.Windows.Forms.BindingSource printDeliveryTypeBindingSource;
        private System.Windows.Forms.ToolStripMenuItem cmsPrintHistory_ShowChanges;
        private System.Windows.Forms.CheckBox cbIssueDelay;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedDelay;
        private DevExpress.XtraGrid.Columns.GridColumn colPrint_Count_Without_Subscribers;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintedBillsForSubscribers;
        private DevExpress.XtraGrid.Columns.GridColumn colBill_Count_Without_Subscribers;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;

    }
}
