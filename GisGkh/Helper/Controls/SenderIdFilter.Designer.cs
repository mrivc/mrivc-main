﻿namespace Helper.Controls
{
    partial class SenderIdFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbSender = new System.Windows.Forms.GroupBox();
            this.cbSender = new System.Windows.Forms.ComboBox();
            this.cbHouse = new Helper.Controls.FiasHouseGuidFilter();
            this.gbSender.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSender
            // 
            this.gbSender.Controls.Add(this.cbSender);
            this.gbSender.Location = new System.Drawing.Point(3, 3);
            this.gbSender.Name = "gbSender";
            this.gbSender.Size = new System.Drawing.Size(503, 50);
            this.gbSender.TabIndex = 2;
            this.gbSender.TabStop = false;
            this.gbSender.Text = "Поставщик информации";
            this.gbSender.Enter += new System.EventHandler(this.gbSender_Enter);
            // 
            // cbSender
            // 
            this.cbSender.FormattingEnabled = true;
            this.cbSender.Location = new System.Drawing.Point(63, 19);
            this.cbSender.Name = "cbSender";
            this.cbSender.Size = new System.Drawing.Size(434, 21);
            this.cbSender.TabIndex = 3;
            this.cbSender.SelectionChangeCommitted += new System.EventHandler(this.cbSender_SelectionChangeCommitted);
            // 
            // cbHouse
            // 
            this.cbHouse.Location = new System.Drawing.Point(3, 62);
            this.cbHouse.Name = "cbHouse";
            this.cbHouse.SenderID = null;
            this.cbHouse.Size = new System.Drawing.Size(506, 59);
            this.cbHouse.TabIndex = 3;
            this.cbHouse.Load += new System.EventHandler(this.cbHouse_Load);
            // 
            // SenderIdFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbHouse);
            this.Controls.Add(this.gbSender);
            this.Name = "SenderIdFilter";
            this.Size = new System.Drawing.Size(509, 121);
            this.gbSender.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSender;
        private System.Windows.Forms.ComboBox cbSender;
        private FiasHouseGuidFilter cbHouse;
    }
}
