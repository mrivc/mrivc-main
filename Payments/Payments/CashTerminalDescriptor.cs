﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Payments
{
    public static class CashTerminalDescriptor
    {
        public static string GetInfoExchangeStatusFN(int value, string descriptorName = "Статус информационного обмена")
        {
            string descriptorText = "";
            switch (value)
            {
                case 0:
                    descriptorText = "транспортное соединение установлено";
                    break;
                case 1:
                    descriptorText = "есть сообщение для передачи в ОФД";
                    break;
                case 2:
                    descriptorText = "ожидание ответного сообщения (квитанции) от ОФД";
                    break;
                case 3:
                    descriptorText = "есть команда от ОФД Бит";
                    break;
                case 4:
                    descriptorText = "изменились настройки соединения с ОФД";
                    break;
                case 5:
                    descriptorText = "ожидание ответа на команду от ОФД";
                    break;
            }
            return string.Format("{0}: {1}", descriptorName, descriptorText);
        }

        public static string GetMessageStateFN(int value, string descriptorName = "Состояние чтения сообщения")
        {
            string descriptorText = "";
            switch (value)
            {
                case 0:
                    descriptorText = "Нет";
                    break;
                case 1:
                    descriptorText = "Да";
                    break;
            }
            return string.Format("{0}: {1}", descriptorName, descriptorText);
        }
    }
}
