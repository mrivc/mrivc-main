﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Helper.Data;

namespace Helper.Controls
{
    public partial class GisErrorFilter : UserControl
    {
        public int Period { get; set; }

        public string ErrorCode
        {
            get { return cbGisError.SelectedValue.ToString(); }
        }

        public GisErrorFilter()
        {
            InitializeComponent();
        }

        public void Fill()
        {
            cbGisError.DataSource = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.SelectErrorByPeriod", new object[,] { {"period" , Period } });
            cbGisError.ValueMember = "errorcode";
            cbGisError.DisplayMember = "ErrorMessageFull";
        }
    }
}
