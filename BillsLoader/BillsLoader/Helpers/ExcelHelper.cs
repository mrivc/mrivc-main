﻿using System;
using System.Collections.Generic;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Windows.Forms;
//using Microsoft.Office.Interop.Excel;

namespace BillsLoader
{
    public static class ExcelHelper
    {
        // Export DataTable into an excel file with field names in the header line
        // - Save excel file without ever making it visible if filepath is given
        // - Don't save excel file, just make it visible if no filepath is given
        public static void ExportToExcel(DataTable Tbl, string ExcelFilePath)
        {
            try
            {
                if (Tbl == null || Tbl.Columns.Count == 0)
                    throw new Exception("ExportToExcel: Null or empty input table!\n");

                // load excel, and create a new workbook
                Excel.Application excelApp = new Excel.Application();

                excelApp.Workbooks.Add(null);

                //System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                //excelApp.Workbooks.GetType().InvokeMember("Add", System.Reflection.BindingFlags.InvokeMethod, null, excelApp.Workbooks, null, ci);

                // single worksheet
                Excel._Worksheet workSheet = (Excel._Worksheet) excelApp.ActiveSheet;

                // column headings
                for (int i = 0; i < Tbl.Columns.Count; i++)
                {
                    workSheet.Cells[1, (i + 1)] = Tbl.Columns[i].ColumnName;
                }

                // rows
                for (int i = 0; i < Tbl.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (int j = 0; j < Tbl.Columns.Count; j++)
                    {
                        workSheet.Cells[(i + 2), (j + 1)] = "'"+Tbl.Rows[i][j];
                    }
                }

                // check fielpath
                if (ExcelFilePath != null && ExcelFilePath != "")
                {
                    try
                    {
                        workSheet.SaveAs(ExcelFilePath, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);
                        //workSheet.SaveAs(ExcelFilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlExcel8);
                        excelApp.Quit();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("ExportToExcel: Excel file could not be saved! Check filepath.\n"
                            + ex.Message);
                    }
                }
                else    // no filepath is given
                {
                    excelApp.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ExportToExcel: \n" + ex.Message);
            }
        }
    }
}
