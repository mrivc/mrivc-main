﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FastReport.Web
{
    internal class WebLog
    {
        private StringBuilder log = new StringBuilder();
        private bool showStackTrace = false;

        public string Text
        {
            get { return log.ToString(); }
        }        

        public void Add(string line)
        {
            log.Append(line).Append("<br />");
        }

        public void Clear()
        {
            log = new StringBuilder();
        }

        public void AddError(Exception e)
        {
            Add(String.Format("<span style=\"color:red\"><b>ERROR:</b><br /> {0}</span>", e.Message.Replace("\n", "<br />")));
            if (showStackTrace)
                Add(e.StackTrace);
        }

        public WebLog(bool trace)
        {
            showStackTrace = trace;
        }
    }
}
