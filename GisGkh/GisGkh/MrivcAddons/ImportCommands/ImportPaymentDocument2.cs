﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Xml;
using Xades.Helpers;
using System.IO;

namespace GisGkh
{
    public class ImportPaymentDocument2 : MrivcImportCommand, ISingleHouseCommand, IMrivcImportCommandAsync
    {
        bool _preview = true;
        string _description = "Синхронизация платежных документов. ";
        bool empty;

        int _period;
        public int Period
        {
            get { return _period; }
            set
            {
                _period = value;
                var periodName = DAccess.DataModule.ExecuteScalarQueryCommand1("select period_name from dbo.vperiod where period = " + _period.ToString());
                Description = _description + periodName;
            }

        }

        public bool NoAccounts { get; private set; } = false;

        ExportPaymentDocument exportPaymentDocument;

        public string FiasHouseGuid { get; set; }

        public ImportPaymentDocument2(string senderID, int period) : base("importPaymentDocumentData", "BillsAsync", senderID)
        {

            var periodName = DAccess.DataModule.ExecuteScalarQueryCommand1("select period_name from dbo.vperiod where period = " + period.ToString());

            _maxImportValue = 1000;
            _importValueName = "PaymentDocument";
            Description = _description + periodName;
            Period = period;

            exportPaymentDocument = new ExportPaymentDocument(SenderID, Period.ToString());
        }

        public void ExportData()
        {
            bool execute = true;

            //if (CheckBeforeExport) //ПРоверка на то, надо ли выполнять экспорт, иначе выполнять экспорт всегда, без проверки
            //{
                execute = !this.IsEmpty && !(
                 //HasErrors || 
                 NoAccounts);
            //}

            if (execute)
            {
                exportPaymentDocument.FiasHouseGuid = FiasHouseGuid;
                exportPaymentDocument.Description = Description;
                exportPaymentDocument.AddToReport = AddToReport;
                exportPaymentDocument.CommandType = CommandType;
                exportPaymentDocument.Execute();

                HasErrors = exportPaymentDocument.HasErrors;
                NoAccounts = exportPaymentDocument.NoAccounts;

                if (NoAccounts) Console.WriteLine("Отсутсвуют загруженные  в ГИС ЖКХ лицевые счета");

                empty = HasErrors || NoAccounts;

                
            }
        }

        public void ImportData()
        {
            //bool check = (bool)(DAccess.DataModule.ExecuteScalarStoredProcedure1("gis.CheckPaymentDocumentData", new object[,] { { "@senderID", SenderID }, { "@fiasHouseGuid", FiasHouseGuid }, { "@period", Period } }));
            bool check = true;
            base.Execute();
        }

        public void Execute()
        {
            IsEmpty = false;
            CheckBeforeExport = false;
            ExportData();

            ImportData();

            CheckBeforeExport = true;
            ExportData();
        }

        public override object Clone()
        {
            var exportPaymentDocumentClone = (ExportPaymentDocument)exportPaymentDocument.Clone();
            var baseClone = (MrivcCommand)base.Clone();
            ((ImportPaymentDocument2)baseClone).exportPaymentDocument = exportPaymentDocumentClone;
            return baseClone;
        }

        public DataSet PrepareToDataSet()
        {
            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.Prepare_Import_Bill_Full3", new object[,] { { "senderid", SenderID }, { "fiasHouseGuid", FiasHouseGuid }, { "period", Period }, { "preview", _preview }, { "empty", empty } }, "paymentInformation", "Bills", "Services", "CapitalRepairBills", "CapitalRepairDetail", "sumCheck");
        }

        protected override void CreateXmlBody()
        {
            _preview = false;
            var _preparedDataSet = PrepareToDataSet();
            _preview = true;

            if (_preparedDataSet.Tables["sumCheck"].Rows.Count != 0)
            {
                var importSum = _preparedDataSet.Tables["sumCheck"].Rows[0]["importSum"].ToString();
                var dbSum = _preparedDataSet.Tables["sumCheck"].Rows[0]["dbSum"].ToString();

                var importDebt = _preparedDataSet.Tables["sumCheck"].Rows[0]["importDebt"].ToString();
                var dbDebt = _preparedDataSet.Tables["sumCheck"].Rows[0]["dbDebt"].ToString();

                var importPays = _preparedDataSet.Tables["sumCheck"].Rows[0]["importPays"].ToString();
                var dbPays = _preparedDataSet.Tables["sumCheck"].Rows[0]["dbPays"].ToString();

                if (importSum != dbSum || importDebt != dbDebt || importPays != dbPays)
                {
                    throw new MrivcCommandException("Сумма в БД не совпадает с суммой для загрузки в ГИС \"ЖКХ\"", SenderID, Service, Method);
                }
            }

            var month = DAccess.DataModule.GetDataTableByQueryCommand1("select month, year from dbo.period where period = @period", new[,] { { "period", Period.ToString() } });

            var values = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "Month" },month.Rows[0][0].ToString()),
                    new Tuple<string[], string>( new [] { "Year" },month.Rows[0][1].ToString())
                };

            Values = values;

            mrivcXmlFormatter.RemoveNodesByName(new[] { "PaymentInformation" });

            foreach (DataRow payInfo in _preparedDataSet.Tables["paymentInformation"].Rows)
            {
                var values1 = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "BankBIK" },payInfo["BankBIK"].ToString()),
                    new Tuple<string[], string>( new [] { "operatingAccountNumber" },payInfo["operatingAccountNumber"].ToString()),
                    new Tuple<string[], string>( new [] { "TransportGUID" },payInfo["TransportGUID"].ToString())
                };

                AddChildElement(MrivcStatic.GisConfig.Services["BillsAsync"].Methods["importPaymentInformation_add"].Template, values1, "importPaymentDocumentRequest");
            }

            foreach (DataRow bill in _preparedDataSet.Tables["Bills"].Rows)
            {
                var values1 = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "AccountGuid" },"AccountGuid"),
                    new Tuple<string[], string>( new [] { "PaymentDocumentNumber" },"PaymentDocumentNumber"),
                };

                var addedBill = AddChildElement( bill , Service, "importPaymentDocument_add", "importPaymentDocument_update",values1,null,null,"importPaymentDocumentRequest");

                var valuesLivingServices = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new[] { "ServiceType", "Code" },"ServiceType_Code"),
                            new Tuple<string[], string>( new [] { "ServiceType", "GUID" },"ServiceType_GUID"),
                            new Tuple<string[], string>( new [] { "ServiceType", "Name" },"ServiceType_Name"),
                            new Tuple<string[], string>( new [] { "Rate" },"Rate"),
                            new Tuple<string[], string>( new [] { "TotalPayable" },"TotalPayable"),
                            new Tuple<string[], string>( new [] { "AccountingPeriodTotal" },"AccountingPeriodTotal"),
                            new Tuple<string[], string>( new [] { "ServiceCharge", "MoneyRecalculation" },"RECALC"),
                            new Tuple<string[], string>( new [] { "ServiceCharge", "MoneyDiscount" },"LGOTA")
                        };

                values1 = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new[] { "ServiceType", "Code" },"ServiceType_Code"),
                            new Tuple<string[], string>( new [] { "ServiceType", "GUID" },"ServiceType_GUID"),
                            new Tuple<string[], string>( new [] { "ServiceType", "Name" },"ServiceType_Name"),
                            new Tuple<string[], string>( new [] { "Rate" },"Rate"),
                            new Tuple<string[], string>( new [] { "TotalPayable" },"TotalPayable"),
                            new Tuple<string[], string>( new [] { "AccountingPeriodTotal" },"AccountingPeriodTotal"),
                            new Tuple<string[], string>( new [] { "ServiceCharge", "MoneyRecalculation" },"RECALC"),
                            new Tuple<string[], string>( new [] { "ServiceCharge", "MoneyDiscount" },"LGOTA"),
                            new Tuple<string[], string>( new [] { "Consumption", "Volume" },"Volume")
                        };


                var serviceRows = _preparedDataSet.Tables["Services"].Select($@"IsCapitalRepair = 0 and RegistryNumber = 50 and PaymentDocumentNumber = '{bill["PaymentDocumentNumber"].ToString()}'");
                foreach (DataRow row in serviceRows)
                {
                    AddChildElement(row, "BillsAsync", "importPaymentDocumentData_ls_add", null, valuesLivingServices, null, null, addedBill);
                }

                serviceRows = _preparedDataSet.Tables["Services"].Select($@"RegistryNumber = 1 and PaymentDocumentNumber = '{bill["PaymentDocumentNumber"].ToString()}'");
                foreach (DataRow row in serviceRows)
                {
                    AddChildElement(row, "BillsAsync", "importPaymentDocumentData_as_add", null, values1, null, null, addedBill);
                }

                serviceRows = _preparedDataSet.Tables["Services"].Select($@"RegistryNumber = 51 and PaymentDocumentNumber = '{bill["PaymentDocumentNumber"].ToString()}'");
                foreach (DataRow row in serviceRows)
                {
                    AddChildElement(row, "BillsAsync", "importPaymentDocumentData_ms_add", null, values1, null, null, addedBill);
                }

                //BDA 14-12-17 Добавлен кап.ремонт в составе ЕПД
                values1 = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "AccountingPeriodTotal" },"AccountingPeriodTotal"),
                            new Tuple<string[], string>( new [] { "TotalPayable" },"TotalPayable"),
                            new Tuple<string[], string>( new [] { "Contribution" },"Rate"),
                            new Tuple<string[], string>( new [] { "MoneyRecalculation" },"RECALC"),
                            new Tuple<string[], string>( new [] { "MoneyDiscount" },"LGOTA")
                        };

                serviceRows = _preparedDataSet.Tables["Services"].Select($@"IsCapitalRepair = 1 and PaymentDocumentNumber = '{bill["PaymentDocumentNumber"].ToString()}'");
                foreach (DataRow row in serviceRows)
                {
                    AddChildElement(row, "BillsAsync", "importPaymentDocumentData_cr_add", null, values1, null, null, addedBill);
                }

                var values2 = new List<Tuple<string[], string>>
                {
                     new Tuple<string[], string>( new string[] { "DebtPreviousPeriods" },bill["ДОЛГ"].ToString()),
                     new Tuple<string[], string>( new string[] { "PaidCash" },bill["ОПЛАТА_ТЕКСТ"].ToString()),
                     new Tuple<string[], string>( new string[] { "PaymentInformationKey" },bill["PaymentInformationKey"].ToString())
                };

                AddChildElement(MrivcStatic.GisConfig.Services["BillsAsync"].Methods["importPaymentDocumentData_add_end"].Template, values2, addedBill);

                CounterCheck();
            }

            if (_preparedDataSet.Tables["CapitalRepairBills"].Rows.Count != 0)
            {
                foreach (DataRow bill in _preparedDataSet.Tables["CapitalRepairBills"].Rows)
                {
                    var values1 = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "AccountGuid" },"AccountGuid"),
                    new Tuple<string[], string>( new [] { "PaymentDocumentNumber" },"PaymentDocumentNumber"),
                };

                    var addedBill = AddChildElement(bill, Service, "importPaymentDocument_add", "importPaymentDocument_update", values1, null, null, "importPaymentDocumentRequest");

                    values1 = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "AccountingPeriodTotal" },"AccountingPeriodTotal"),
                            new Tuple<string[], string>( new [] { "TotalPayable" },"TotalPayable"),
                            new Tuple<string[], string>( new [] { "Contribution" },"Contribution"),
                            new Tuple<string[], string>( new [] { "MoneyRecalculation" },"MoneyRecalculation"),
                            new Tuple<string[], string>( new [] { "MoneyDiscount" },"MoneyDiscount")
                        };

                    var serviceRows = _preparedDataSet.Tables["CapitalRepairDetail"].Select($"PaymentDocumentNumber = '{bill["PaymentDocumentNumber"].ToString()}'");

                    foreach (DataRow row in serviceRows)
                    {
                        AddChildElement(row, "BillsAsync", "importPaymentDocumentData_cr_add", null, values1, null, null, addedBill);
                    }

                    var values2 = new List<Tuple<string[], string>>
                {
                     new Tuple<string[], string>( new string[] { "DebtPreviousPeriods" },bill["ДОЛГ"].ToString()),
                     new Tuple<string[], string>( new string[] { "PaidCash" },bill["ОПЛАТА_ТЕКСТ"].ToString()),
                     new Tuple<string[], string>( new [] { "PaymentInformationKey" }, bill["PaymentInformationKey"].ToString())
                };

                    AddChildElement(MrivcStatic.GisConfig.Services["BillsAsync"].Methods["importPaymentDocumentData_add_end"].Template, values2, addedBill);

                    CounterCheck();
                }
            }
        }

       
    }
}
