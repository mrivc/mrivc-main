﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;

namespace MrivcControls
{
    public partial class MultiValueDataPicker : UserControl, IDataPicker
    {
        public MultiValueDataPicker()
        {
            InitializeComponent();
        }

        public object DataSource
        {
            get
            {
                return customEdit.Properties.DataSource;
            }

            set
            {
                customEdit.Properties.DataSource = value;
            }
        }

        public string DisplayMember
        {
            get
            {
                return customEdit.Properties.DisplayMember;
            }

            set
            {
                customEdit.Properties.DisplayMember = value;
            }
        }

        public string ParameterName
        {
            get { return parameterGroupBox.Text; }
            set { parameterGroupBox.Text = value; }
        }

        public string SelectedName
        {
            get
            {
                return customEdit.Text;
            }
        }

        public object SelectedValue
        {
            get
            {
                return customEdit.EditValue;
            }
        }

        public string ValueMember
        {
            get
            {
                return customEdit.Properties.ValueMember;
            }

            set
            {
                customEdit.Properties.ValueMember = value;
            }
        }

        private void customEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (customEdit.EditValue.ToString() != "")
                selectAllCheckBox.Checked = false;
            else
                selectAllCheckBox.Checked = true;
        }

        private void selectAllCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (customEdit.EditValue.ToString() != "")
                selectAllCheckBox.Checked = false;
            else
                selectAllCheckBox.Checked = true;
        }

        private void selectAllCheckBox_Click(object sender, EventArgs e)
        {
            int checkedCount = 0;

            foreach (CheckedListBoxItem item in customEdit.Properties.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    checkedCount += 1;
                }
            }

            if (checkedCount != 0)
                if (customEdit.Properties.Items.Count != checkedCount)
                {
                    object all = string.Empty;
                    foreach (CheckedListBoxItem item in customEdit.Properties.Items)
                    {
                        all += item.Value + ",";
                    }

                    customEdit.EditValue = all;
                    customEdit.RefreshEditValue();
                }
                else
                {
                    customEdit.EditValue = string.Empty;
                    customEdit.RefreshEditValue();
                }
        }
    }
}
