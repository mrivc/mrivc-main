﻿using System;
using System.Collections.Generic;
using System.Text;
using Payments.DS;

namespace Payments.LogicClasses
{
    public class ProgramSettings
    {
        public string ELLIS_DB_SERVER
        {
            get {
                return DAccess.DataModule.GetProgramSettingsByCode("#ELLIS_DB_SERVER");
            }
        }

        public string ELLIS_DB_NAME
        {
            get
            {
                return DAccess.DataModule.GetProgramSettingsByCode("#ELLIS_DB_NAME");
            }
        }

        /// <summary>
        /// Разрешено открывать смену при не отправленных данных в ОФД
        /// </summary>
        public bool OFD_ALLOW_OPEN_SESSION
        {
            get
            {
                return bool.Parse(DAccess.DataModule.GetProgramSettingsByCode("#OFD_ALLOW_OPEN_SESSION"));
            }
        }

        /// <summary>
        /// Максимальная сумма платежа
        /// </summary>
        public decimal MAX_SUM_PAY
        {
            get
            {
                return decimal.Parse(DAccess.DataModule.GetProgramSettingsByCode("#MAX_SUM_PAY"));
            }
        }
    }
}
