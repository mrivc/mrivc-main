﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Payments
{
    public class StringHelper
    {
        public static string GetSubstring(string str, string startChar, string endChar)
        {
            int _ind1 = str.IndexOf(startChar);
            int _ind2 = str.LastIndexOf(endChar);

            if (_ind1 > 0 && _ind2 > _ind1)
                return str.Substring(str.IndexOf(startChar)+1, str.LastIndexOf(endChar) - str.IndexOf(startChar)-1);
            else
                return "";
        }

        public static string ReplaceEscape(string str)
        {
            str = str.Replace("'", "''");
            return str;
        }

        public static string ZeroCode(int value)
        {
            if (value < 10)
                return "0" + value.ToString();
            return value.ToString();
        }

        public static string DayCode(int day)
        {
            if (day < 10)
                return "0" + day.ToString();
            return day.ToString();
        }
    }
}
