using System;
using System.Collections.Generic;
using System.Text;
using FastReport.Cloud.OAuth;
using System.Net;
using System.IO;
using FastReport.Export;
using System.Windows.Forms;

namespace FastReport.Cloud.StorageClient.Dropbox
{
    /// <summary>
    /// Dropbox cloud storage client.
    /// </summary>
    public class DropboxStorageClient : CloudStorageClient
    {
        #region Constants

        /// <summary>
        /// The base URL for Dropbox API.
        /// </summary>
        public const string DropboxUrlBase = "https://api.dropbox.com";
        
        /// <summary>
        /// The base URL for request token.
        /// </summary>
        public const string RequestTokenUrlBase = "https://api.dropbox.com/1/oauth/request_token";
        
        /// <summary>
        /// The base URL for access token.
        /// </summary>
        public const string AccessTokenUrlBase = "https://api.dropbox.com/1/oauth/access_token";
        
        /// <summary>
        /// The base URL for user authorization.
        /// </summary>
        public const string UserAuthorizationUrlBase = "https://www.dropbox.com/1/oauth/authorize";
        
        /// <summary>
        /// The base URL for files_put command.
        /// </summary>
        public const string FilesPutUrlBase = "https://api-content.dropbox.com/1/files_put";

        #endregion // Constants

        #region Fields

        private AppInfo appInfo;
        private DropboxStorageConfig storageConfig;
        private ConsumerContext consumer;
        private Token requestToken;
        private Token accessToken;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Gets or sets the application info.
        /// </summary>
        public AppInfo AppInfo
        {
            get { return appInfo; }
            set { appInfo = value; }
        }

        /// <summary>
        /// Gets or sets the storage configuration.
        /// </summary>
        public DropboxStorageConfig StorageConfig
        {
            get { return storageConfig; }
            set { storageConfig = value; }
        }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        public Token AccessToken
        {
            get { return accessToken; }
            set { accessToken = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DropboxStorageClient"/> class.
        /// </summary>
        public DropboxStorageClient() : base()
        {
            this.appInfo = new AppInfo("", "", "", "");
            this.storageConfig = new DropboxStorageConfig("");
            consumer = new ConsumerContext(appInfo.Key, appInfo.Secret);
            requestToken = new Token("", "");
            accessToken = new Token("", "");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DropboxStorageClient"/> class.
        /// </summary>
        /// <param name="appInfo">The Dropbox application info.</param>
        /// <param name="storageConfig">The Dropbox storage configuration.</param>
        public DropboxStorageClient(AppInfo appInfo, DropboxStorageConfig storageConfig) : base()
        {
            this.AppInfo = appInfo;
            this.storageConfig = storageConfig;
            consumer = new ConsumerContext(appInfo.Key, appInfo.Secret);
            requestToken = new Token("", "");
            accessToken = new Token("", "");
        }

        #endregion // Constructors

        #region Public Methods

        /// <summary>
        /// Gets the authorization URL.
        /// </summary>
        /// <returns>The authorization URL string.</returns>
        public string GetAuthorizationUrl()
        {
            Session session = new Session(ProxySettings);
            requestToken = session.ObtainDropboxRequestToken(storageConfig, consumer);
            return session.ObtainAuthorizationUrl(storageConfig, requestToken);
        }

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <returns>The access token.</returns>
        public Token GetAccessToken()
        {
            Session session = new Session(ProxySettings);
            accessToken = session.ObtainDropboxAccessToken(storageConfig, consumer, requestToken);
            return accessToken;
        }

        /// <inheritdoc/>
        public override bool SaveReport(Report report, ExportBase export)
        {
            bool result = true;
            using (MemoryStream ms = PrepareToSave(report, export))
            {
                Session session = new Session(ProxySettings);
                string filesPutUrl = session.ObtainFilesPutUrl(FilesPutUrlBase, appInfo.AccessType, Filename, consumer, accessToken);
                try
                {
                    Uri uri = new Uri(filesPutUrl);
                    WebRequest request = WebRequest.Create(uri);
                    request.Method = HttpMethod.Put;
                    RequestUtils.SetProxySettings(request, ProxySettings);

                    int length = Convert.ToInt32(ms.Length);
                    byte[] buffer = new byte[length];
                    ms.Read(buffer, 0, length);
                    request.ContentLength = buffer.Length;
                    using (Stream rs = request.GetRequestStream())
                    {
                        rs.Write(buffer, 0, length);
                    }

                    WebResponse response = request.GetResponse();
                    Stream stream = response.GetResponseStream();
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
            }
            return result;
        }

        #endregion // Public Methods
    }
}
