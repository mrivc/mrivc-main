﻿using System;
using System.Configuration;
using System.Text;
using System.Xml;
using GisGkh.Configurations.Sections;
using GisGkh.Infrastructure;
using System.Data;
using System.IO;
using Xades.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Helper.Data;
using Helper;
using FastReport;

namespace GisGkh
{
    public static class MrivcStatic
    {
        public static int GetStateWaitMin { get; set; } = GetStateWaitMinDefault; //значение "-1" - до отмены пользователем
        public const int GetStateWaitMinDefault = 5;
        public static bool WithoutExport { get; set; } = false;
        public static bool Check { get; set; }
        public static bool Await { get; set; }

        static GisServiceConfiguration gisConfig = (GisServiceConfiguration)ConfigurationManager.GetSection(Constants.GisServicesConfigSectionName);
        static string output = "";
        public static string Output
        {
            get
            {
                var _output = output;
                output = "";
                return _output;
            }
        }

        public static string CurrentSoapRequest { get; set; }

        public static GisServiceConfiguration GisConfig {
            get
            {
                return gisConfig;
            }
        }

        public static bool isRSO(string senderID)
        {
            return (bool)DAccess.DataModule.ExecuteScalarQueryCommand1("select isRso from gis.Provider where senderID = @senderID", new[,] { { "senderID", senderID } });
        }

        public static void WriteLogToConsole(int ID)
        {
            Console.ForegroundColor = ConsoleColor.Gray;

            var logTable = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.LogPrint", new object[,] { { "ID", ID } });

            foreach (DataRow row in logTable.Rows)
            {
                if (int.Parse(row[1].ToString()) == 0)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                }

                var message = row[0].ToString();

                Console.WriteLine(message);

                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        public static void WriteLogBase(object[,] prms)
        {
            //try
            //{
                var ID = DAccess.DataModule.ExecuteScalarStoredProcedure1("GIS.WriteLog", prms);
                WriteLogToConsole(Convert.ToInt32(ID));
            //}
            //catch (Exception)
            //{}
        }

        public static void WriteLog(MrivcCommand command,MrivcCommandException mcex,bool addToReport, string houseGuid)
        {
            var prms = new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@houseGUID", houseGuid},
                            { "@message", mcex.Message },
                            { "@code", mcex.Code },
                            { "@sendRequest", mcex.SendRequest },
                            { "@messageGUID", mcex.MessageGUID },
                            { "@description", command.Description },
                            { "@addToReport", addToReport },
                            { "@response", mcex.Response },
                            { "@IdAsyncSession", command.CommandType }
                       };

            WriteLogBase(prms);
        }

        public static void WriteLog(MrivcCommand command, MrivcCommandException mcex, bool addToReport)
        {
            var prms = new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@message", mcex.Message },
                            { "@code", mcex.Code },
                            { "@sendRequest", mcex.SendRequest },
                            { "@messageGUID", mcex.MessageGUID },
                            { "@description", command.Description },
                            { "@addToReport", addToReport },
                            { "@response", mcex.Response },
                            { "@IdAsyncSession", command.CommandType }
                       };

            WriteLogBase(prms);
        }

        public static void ClearConsole()
        {
            Console.Clear();
        }

        public static void WriteLog(MrivcCommand command)
        {
            var prms = new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@description", command.Description },
                            { "@addToReport", command.AddToReport },
                            { "@IdAsyncSession", command.CommandType }

                       };

            WriteLogBase(prms);
        }

        public static void WriteLog(MrivcCommand command, Exception ex)
        {
            var prms = new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@description", command.Description },
                            { "@addToReport", command.AddToReport },
                            { "@message", ex.Message },
                            { "@IdAsyncSession", command.CommandType }
                       };

            WriteLogBase(prms);
        }

        public static void WriteLog(MrivcCommand command, Exception ex, string houseGuid)
        {
            var prms = new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@houseGUID", houseGuid},
                            { "@description", command.Description },
                            { "@addToReport", false },
                           { "@message", ex.Message },
                            { "@IdAsyncSession", command.CommandType }
                       };

            WriteLogBase(prms);
        }

        public static void WriteLog(Exception ex)
        {
            var prms = new object[,] {
                            { "@senderID", null },
                            { "@serviceName", null },
                            { "@methodName", null },
                            { "@addToReport", false },
                           { "@message", ex.Message }
                       };

            WriteLogBase(prms);
        }

        public static void WriteLog(MrivcCommand command, string houseGuid)
        {
            var prms = new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@houseGUID", houseGuid},
                            { "@description", command.Description },
                            { "@addToReport", command.AddToReport },
                            { "@IdAsyncSession", command.CommandType }
                       };

            WriteLogBase(prms);
        }


        public static void WriteLogWithMessageGuid(MrivcCommand command, string messageGuid)
        {


            var prms = new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@description", command.Description },
                            { "@addToReport", command.AddToReport },
                            { "@messageGuid", messageGuid },
                            { "@IdAsyncSession", command.CommandType }
                       };

            WriteLogBase(prms);
        }

        public static void WriteLogWithMessageGuid(MrivcCommand command, string houseGuid, string messageGuid)
        {
            var prms = new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@houseGUID", houseGuid},
                            { "@description", command.Description },
                            { "@addToReport", command.AddToReport },
                            { "@messageGuid", messageGuid },
                            { "@IdAsyncSession", command.CommandType }
                       };

            WriteLogBase(prms);
        }

        public static Tuple<string,string> GetErrorFromResponse(string response)
        {
            var errCode = MrivcStatic.SelectNodeByName(MrivcStatic.ResultNodeName("ErrorCode"), response);

            if (errCode != null)
            {
                var err = MrivcStatic.SelectNodeByName(MrivcStatic.ResultNodeName("Description"), response);

                if (err == null)
                err = MrivcStatic.SelectNodeByName(MrivcStatic.ResultNodeName("ErrorMessage"), response);

                return new Tuple<string, string>(errCode.InnerText, err.InnerText);
            }
               
            else return null;
        }

        public static void AddToOutput (params string[] lines)
        {
            foreach (var _string in lines)
            {
                output += _string;
                output += Environment.NewLine;
            } 
        }

        public static void ClearOutput()
        {
            output = "";
        }

        public static string[] PrepareSendCommandArgs(string service, string methodName)
        {
            return new string[]{"send","-s",service,"-m",methodName,"-c",$"{methodName} request.csv","-o",$"{methodName} response.csv","-a", "lanit:tv,n8!Ya" };
        }

        public static string[] PrepareGetStateCommandArgs(string service, string methodName, string messageGuid)
        {
            return new string[] { "get-state", "-s", service,"-g", messageGuid , "-o", $"{methodName} response.csv", "-a", "lanit:tv,n8!Ya" };
        }

        public static bool IsLastElement(XmlNode node)
        {
            if (node.ChildNodes.Count == 0 || (node.ChildNodes.Count == 1 && node.ChildNodes[0].NodeType == XmlNodeType.Text))
                return true;
            else
                return false;
        }

        public static string FindXPath(XmlNode node)
        {
            StringBuilder builder = new StringBuilder();
            while (node != null)
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Attribute:
                        builder.Insert(0, "/@" + node.LocalName);
                        node = ((XmlAttribute)node).OwnerElement;
                        break;
                    case XmlNodeType.Element:
                        builder.Insert(0, "/" + node.LocalName);
                        node = node.ParentNode;
                        break;
                    case XmlNodeType.Document:
                        return builder.ToString();
                    default:
                        throw new ArgumentException("Only elements and attributes are supported");
                }
            }
            throw new ArgumentException("Node was not in a document");
        }

        public static string GetMessageGuidBySendResponse(string sendResponse)
        {
            try
            {
                var ackGuidNode = SelectNodeByName("//*[local-name()='AckRequest']/*[local-name()='Ack']", sendResponse);
                var messageGuidNode = SelectNodeByName("//*[local-name()='MessageGUID']", ackGuidNode.OuterXml);
                return messageGuidNode.InnerText;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        public static string GetMessageGuidByGetStateResponse(string getStateResponse)
        {
            try
            {
                var gsNode = SelectNodeByName("//*[local-name()='getStateRequest']", getStateResponse);
                var messageGuidNode = SelectNodeByName("//*[local-name()='MessageGUID']", gsNode.OuterXml);
                return messageGuidNode.InnerText;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public static string ReplaceTransportGUID(string xmlBody)
        {
            var xd = new XmlDocument();
            xd.InnerXml = xmlBody;
            var tg = xd.SelectNodes(NodePathWithoutPrefix("TransportGUID"));
            foreach (XmlNode item in tg)
            {
                if (item.ParentNode.LocalName != "PaymentInformation")
                    item.InnerText = Guid.NewGuid().ToString("D");
            }
            return xd.InnerXml;
        }

        public static void RemoveNodesByName(string nodeName, ref string xmlText)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlText);
            var xm = xd.CreateNamespaceManager();
            XmlNodeList xnl = xd.SelectNodes(NodePathWithoutPrefix(nodeName), xm);

            foreach (XmlNode item in xnl)
            {
                var pn = item.ParentNode;
                //item.RemoveAll();
                pn.RemoveChild(item);
            }

            xmlText = xd.InnerXml;
        }

        public static XmlNode SelectNodeByName(string nodeName, string xmlText)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlText);
            var xm = xd.CreateNamespaceManager();
            return xd.SelectSingleNode(nodeName, xm);
        }

        public static XmlNodeList SelectNodesByName(string nodeName, string xmlText)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlText);
            var xm = xd.CreateNamespaceManager();
            return xd.SelectNodes(nodeName, xm);
        }

        public static string ResultNodeName(string nodeName)
        {
            return $"//*[local-name()='{nodeName}']"; 
        }

        public static string NodePathWithoutPrefix(params string[] nodeNames)
        {
            var res = "/";
            foreach (var nodeName in nodeNames)
            {
                res += $"/*[local-name()='{nodeName}']";
            }
            return res;
        }

        public static string PrepareXmlBody(string service, string method, IEnumerable<Tuple<string[],string>> valuePairs)
        {
            var templatePath = GisConfig.Services[service].Methods[method].Template;
            var template = File.ReadAllText(templatePath);

            var xd = new XmlDocument();
            xd.LoadXml(template);

            foreach (Tuple<string[],string> value in valuePairs)
            {
                var node = xd.SelectSingleNode(NodePathWithoutPrefix(value.Item1));
                node.InnerText = value.Item2;
            }

            return xd.InnerXml;
        }

        public static List<DataSet> HouseIteration(IEnumerable<ISingleHouseCommand> singleHouseCommandSet, string senderID, bool preview)
        {
            var resultSet = new List<DataSet>();

            var houseGuidList = MrivcData.HouseFiasGuidList(senderID);

            resultSet.Add(new DataSet());
            var ds = resultSet.Last();

            foreach (string houseGuid in houseGuidList)
            {
                foreach (ISingleHouseCommand singleHouseCommand in singleHouseCommandSet)
                {
                    try
                    {
                            singleHouseCommand.FiasHouseGuid = houseGuid;
                            if (preview)
                            {
                                if (ds.Tables.Count == 0)
                                {
                                    ds = singleHouseCommand.PrepareToDataSet();
                                }
                                else
                                {
                                    ds.Merge(singleHouseCommand.PrepareToDataSet());
                                }
                            }
                            else
                                singleHouseCommand.Execute();
                    }
                    catch (MrivcCommandException mcex)
                    {
                        MrivcStatic.WriteLog(((MrivcCommand)singleHouseCommand), mcex,false,singleHouseCommand.FiasHouseGuid);
                    }
                    catch (Exception ex)
                    {
                        MrivcStatic.WriteLog(((MrivcCommand)singleHouseCommand),ex,houseGuid);
                    }
                }
            }
            return resultSet;

        }

        public static string IndentXml (string xml)
        {
            XmlWriterSettings ws = new XmlWriterSettings() { Indent = true , OmitXmlDeclaration = true};
            var xe = XElement.Parse(xml);

            var sb = new StringBuilder();
            using (XmlWriter xw = XmlWriter.Create(sb, ws))
                xe.Save(xw);

            return sb.ToString();
        }


        public static DataSet HouseIteration(ISingleHouseCommand singleHouseCommand, bool preview)
        {
            var houseGuidList = MrivcData.HouseFiasGuidList(singleHouseCommand.SenderID);

            var ds = new DataSet();

            foreach (string houseGuid in houseGuidList)
            {
               singleHouseCommand.FiasHouseGuid = houseGuid;
               if (preview)
               {
                    if (ds.Tables.Count == 0)
                    {
                        ds = singleHouseCommand.PrepareToDataSet();
                    }
                    else
                    {
                        ds.Merge(singleHouseCommand.PrepareToDataSet());
                    }
                 }
                 else
                 singleHouseCommand.Execute();
              }

            return ds;
        }

        public static void HouseIterationAsync(MrivcCommandType singleHouseCommand, string senderID, int period, int startPeriod, List<MrivcCommand> asyncCommandList, IEnumerable<string> houseGuidList)
        {
            IEnumerable<string> _houseGuidList;
            IEnumerable<string> houseGuidListPD;
            IEnumerable<string> houseGuidListMeasure;
            IEnumerable<string> houseGuidListMeter;

            if (houseGuidList != null)
            {
                var newHouseGuidList = new List<string>();

                foreach (var item in houseGuidList)
                {
                    var _item = item.Trim();
                    newHouseGuidList.Add(_item); 
                }

                _houseGuidList = newHouseGuidList;
                houseGuidListPD = newHouseGuidList;
                houseGuidListMeter = newHouseGuidList;
                houseGuidListMeasure = newHouseGuidList;
            }
            else
            {
                if (Check)
                {
                    _houseGuidList = MrivcData.HouseFiasGuidListForHouseData(senderID, period);
                    houseGuidListPD = MrivcData.HouseFiasGuidListForPaymentDocument(senderID, period);
                    houseGuidListMeasure = MrivcData.HouseFiasGuidListForMeasure(senderID);
                    houseGuidListMeter = MrivcData.HouseFiasGuidListForMeter(senderID);
                }
                else
                {
                    _houseGuidList = MrivcData.HouseFiasGuidList(senderID);
                    houseGuidListPD = MrivcData.HouseFiasGuidList(senderID);
                    houseGuidListMeter = MrivcData.HouseFiasGuidList(senderID);
                    houseGuidListMeasure = MrivcData.HouseFiasGuidListForMeasure(senderID);
                }
            }

            foreach (string houseGuid in _houseGuidList)
            {
                switch (singleHouseCommand)
                {
                    case MrivcCommandType.ImportAccountData:
                        asyncCommandList.Add(new ImportAccountData(senderID) { FiasHouseGuid = houseGuid , AddToReport = true });
                        break;
                    case MrivcCommandType.ImportHouseData:
                        asyncCommandList.Add(new ImportHouseData(senderID) { FiasHouseGuid = houseGuid , AddToReport = true });
                        break;
                    
                }

                //singleHouseCommand.FiasHouseGuid = houseGuid;
                //MrivcCommand commandClone = (MrivcCommand)((MrivcCommand)singleHouseCommand).Clone();
                //asyncCommandList.Add(commandClone);
            }

            foreach (string houseGuid in _houseGuidList)
            {
                    if (singleHouseCommand == MrivcCommandType.ImportMeteringDevice)
                        asyncCommandList.Add(new ImportMeteringDevice(senderID) { FiasHouseGuid = houseGuid, AddToReport = true });
                        break;
            }

            foreach (string houseGuid in houseGuidListMeasure)
            {
                switch (singleHouseCommand)
                {
                    case MrivcCommandType.ImportMeasureData:
                        asyncCommandList.Add(new ImportMeasureData(senderID) { FiasHouseGuid = houseGuid, AddToReport = true });
                        break;
                }
            }

            var periods = DAccess.DataModule.GetDataTableByQueryCommand1("select period from dbo.period where period between "+startPeriod+" and "+period+"" );

            foreach (DataRow periodRow in periods.Rows)
            {
                foreach (string houseGuid in houseGuidListPD)
                {
                    switch (singleHouseCommand)
                    {
                        case MrivcCommandType.ImportPaymentDocument2:
                            asyncCommandList.Add(new ImportPaymentDocument2(senderID, int.Parse(periodRow[0].ToString())) { FiasHouseGuid = houseGuid, AddToReport = true });
                            break;
                    }
                }
            }
            
        }

        public static Tuple<DateTime, DateTime> CurrentDates()
        {
            var dt = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.CurrentPeriodDatesSelect");

            var beginDate = (DateTime)dt.Rows[0][0];
            var endDate = (DateTime)dt.Rows[0][1];

            return new Tuple<DateTime, DateTime>(beginDate, endDate);
        }

        public static void PrepareExcelDescriptionReport(string senderID, int period, string savePath)
        {
            var es = new EnvironmentSettings();
            es.ReportSettings.ShowProgress = false;

            //var path = Directory.GetCurrentDirectory() + "//_GISCommandReport.FRX";
            var report = new Report();

            var sql = string.Format("select convert(varbinary(max),[file]) from dbo.module where name ='{0}' ", "_GISCommandReport");

            byte[] module = (byte[])DAccess.DataModule.ExecuteScalarQueryCommand1(sql);

            report.LoadFromString(Encoding.UTF8.GetString(module));

            report.SetParameterValue("MainConnection", MrivcData.ConnectionString);
            report.SetParameterValue("id_company", senderID);
            report.SetParameterValue("period", period);
            report.Prepare();

            var export = new FastReport.Export.OoXML.Excel2007Export();

            report.Export(export, savePath);
        }

        public static string CreateMailText(bool intermediate, DateTime beginDate, DateTime endDate, string senderID)
        {
            var date = DateTime.Today.ToShortDateString();

            var state = int.Parse(DAccess.DataModule.ExecuteScalarStoredProcedure1("GIS.CheckLogState", new object[,] { { "date", beginDate }, { "enddate", endDate }, { "senderID", senderID } }).ToString());

            var type = 0;

            if (state == 0)
            {
                if (intermediate)
                {
                    type = 2; // Пром. очтет с ошибками
                }
                else
                {
                    type = 0; // Отчет с ошибками
                }
            }
            else if (state == 2)
            {
                type = 3; // Без ошибок
            }
            else if (state == 1)
            {
                return ""; //Не выполнялась
            }

            return DAccess.DataModule.ExecuteScalarQueryCommand1("select GIS.CreateMailText(@type, @date)", new object[,] { { "type", type }, { "date", date } }).ToString();
        }

        public static void SendCommandReport(string senderId, bool intermediate)
        {
            //Формирование периодов для последующих отчетов
            var dates = CurrentDates();
            var beginDate = dates.Item1; //для txt отчета по ошибкам
            var endDate = dates.Item2;
            var period = Convert.ToInt32(DAccess.DataModule.ExecuteScalarQueryCommand1("select dbo.Work_Period() - 1")); //для xls отчета по ошибкам (там используется период, предыдущий от текущего)

            var dt = DAccess.DataModule.GetDataTableByQueryCommand1("select distinct senderID, senderName, email from GIS.VContacts where senderid = @senderId", new[,] { { "senderId", senderId } });

            var emailSender = new EmailSender();
            emailSender.SmtpServerHost = "srv-proxy";
            emailSender.SmtpServerSSL = false;
            emailSender.SmtpServerMailFrom = "gis@mrivc.ru";
            emailSender.SmtpServerMailLogin = "gis@mrivc.ru";
            emailSender.SmtpServerPswd = "gg11GG";
            emailSender.SmtpServerPort = 25;

            var date = DateTime.Today.ToShortDateString();

            foreach (DataRow row in dt.Rows)
            {
                var senderID = row["senderID"].ToString();
                var senderName = row["senderName"].ToString();
                var email = row["email"].ToString();

                //Текстовый отчет об ошибках
                var eReport = DAccess.DataModule.GisCommandReport(senderID, beginDate, false, endDate);
                var epath = $"test\\Как исправить ошибки.txt";

                var now = DateTime.Now;

                //Excel описание ошибок
                var eExcelPath = $"test\\Excel Описание ошибок {FileHelper.CreateCorrectFileName($"{date} {senderName}")}.xlsx";
                PrepareExcelDescriptionReport(senderID, period, eExcelPath);

                var subject = "";

                if (intermediate)
                {
                    subject = "АО \"МРИВЦ\" Промежуточный отчет по интеграции с ГИС \"ЖКХ\" ";
                }
                else
                {
                    subject = "АО \"МРИВЦ\" Отчет по интеграции с ГИС \"ЖКХ\" ";
                }

                var paths = new List<string>();
                var mailBody = CreateMailText(intermediate, beginDate, endDate, senderID);

                if (eReport != "")
                {
                    File.WriteAllText(epath, eReport);
                    paths.Add(epath);
                    paths.Add(eExcelPath);
                }

                if (paths.Count != 0)
                    emailSender.Send(email, paths.ToArray(), subject + senderName + " за " + date, mailBody);
                else if (mailBody != "")
                    emailSender.Send(email, null, subject + senderName + " за " + date, mailBody);

                //Список услуг

                var dt1 = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.ServicesReport", new[,] { { "senderID", senderID } });
                string s = "";
                foreach (DataRow row1 in dt1.Rows)
                {
                    s += row1[0].ToString() + " - " + row1[1].ToString() + Environment.NewLine;
                }

                var srpath = $"test/Список услуг {FileHelper.CreateCorrectFileName($"{date} {senderName}")}.txt";

                paths = new List<string>();
                mailBody = DAccess.DataModule.ExecuteScalarQueryCommand1("select GIS.CreateMailText(1, NULL)").ToString();

                if (s != "")
                {
                    File.WriteAllText(srpath, s);
                    paths.Add(srpath);
                }

                if (paths.Count != 0)
                    emailSender.Send(email, paths.ToArray(), "АО \"МРИВЦ\" Список услуг для интеграции с ГИС \"ЖКХ\" " + senderName + " за " + date, mailBody);

                //Конец. Список услуг
            }
        }

        static public void RunConsole(string[] args, List<string> senderID, int startPeriod, int period, List<string> houseGuidList)
        {
            bool syncHouseData = false;
            bool syncAccountData = false;
            bool syncNsi = false;
            bool syncServices = false;
            bool syncPaymentDocument = false;
            bool syncMeterData = false;
            bool syncMeterMeasure = false;
            bool syncSenderData = false;
            bool withdrawPaymentDocument = false;
            bool exportSupplyContracts = false;

            foreach (var arg in args)
            {
                switch (arg)
                {
                    case "-woexp":
                        WithoutExport = true;
                        break;
                    case "-si":
                        syncSenderData = true;
                        break;
                    case "-hd":
                        syncHouseData = true;
                        break;
                    case "-ad":
                        syncAccountData = true;
                        break;
                    case "-nsi":
                        syncNsi = true;
                        break;
                    case "-s":
                        syncServices = true;
                        break;
                    case "-pd":
                        syncPaymentDocument = true;
                        break;
                    case "-m":
                        syncMeterData = true;
                        break;
                    case "-mm":
                        syncMeterMeasure = true;
                        break;
                    case "-wpd":
                        withdrawPaymentDocument = true;
                        break;
                    case "-sc":
                        exportSupplyContracts = true;
                        break;
                }
            }

            if (syncSenderData || syncNsi || syncServices || syncHouseData || syncAccountData || syncMeterData || syncMeterMeasure || syncPaymentDocument || withdrawPaymentDocument || exportSupplyContracts)
            {
                //var beginDate = DateTime.Now;

                ////DAccess.DataModule.ExecuteNonQueryCommand1("insert GIS.SyncBeginDateLog (SyncBeginDate,SyncSenderData,SyncNsi,SyncServices,SyncHouseData,SyncAccountData,SyncMeterData,SyncMeterMeasure,SyncPaymentDocument) values (@bd,@si,@n,@s,@hd,@ad,@md,@mm,@pd)", new object[,] { { "@bd", beginDate }, { "@si", syncSenderData }, { "@n", syncNsi }, { "@s", syncServices }, { "@hd", syncHouseData }, { "@ad", syncAccountData }, { "@md", syncMeterData }, { "@mm", syncMeterMeasure }, { "@pd", syncPaymentDocument } });

                //Console.WriteLine($"Начата синхронизация за {beginDate}");

                var siError = false;
                var nsiError = false;

                try
                {
                    var exportSenderID = new ExportOrgRegistry();
                    if (syncSenderData)
                    {
                        exportSenderID.Execute();
                    }

                    siError = exportSenderID.HasErrors;

                }
                catch (Exception ex)
                {
                    siError = true;
                    MrivcStatic.WriteLog(ex);

                }

                if (!siError)
                {
                    try
                    {
                        var exportNsiList = new ExportNsiList();
                        if (syncNsi)
                        {
                            var nsiRegNumbers = MrivcStatic.GisConfig.ExportNsiRegistryNumbers;
                            exportNsiList.NsiRegistryNumbers = nsiRegNumbers;
                            exportNsiList.Execute();
                        }

                        nsiError = exportNsiList.HasErrors;

                    }
                    catch (Exception ex)
                    {
                        nsiError = true;
                        MrivcStatic.WriteLog(ex);

                    }
                }

                if (nsiError || siError)
                {
                    return;
                }

                var SenderList = new List<string>();

                if (senderID != null)
                {
                    //SenderList = new List<string> { { senderID } };
                    SenderList = senderID;
                }
                else
                {
                    SenderList = MrivcData.SenderList();
                }

                if (period == -1)
                    period = (int)DAccess.DataModule.ExecuteScalarQueryCommand1("select dbo.work_period()-1");

                if (startPeriod == -1)
                    startPeriod = (int)DAccess.DataModule.ExecuteScalarQueryCommand1("select dbo.work_period()-1");

                var asyncCommandSet = new List<MrivcCommand>();

                var singleCommandList = new List<MrivcCommandType>();

                if (syncServices)
                {
                    singleCommandList.Add(MrivcCommandType.ImportMunicipalServices);
                    singleCommandList.Add(MrivcCommandType.ImportAdditionalServices);
                }

                if (syncHouseData)
                {
                    singleCommandList.Add(MrivcCommandType.ImportHouseData);
                }

                if (syncAccountData)
                {
                    singleCommandList.Add(MrivcCommandType.ImportAccountData);
                }

                if (syncPaymentDocument)
                {
                    singleCommandList.Add(MrivcCommandType.ImportPaymentDocument2);
                }

                if (syncMeterData)
                {
                    singleCommandList.Add(MrivcCommandType.ImportMeteringDevice);
                }

                if (syncMeterMeasure)
                    singleCommandList.Add(MrivcCommandType.ImportMeasureData);
                
                ////////singleCommandList.Add(new ImportMeasureData(senderID) { AddToReport = true });

                MrivcStatic.AsyncCommandIteration(singleCommandList, SenderList, period, startPeriod, asyncCommandSet, houseGuidList);

                var n = 2000;

                while (asyncCommandSet.Count != 0)
                {
                    var asyncCommandSetPart = asyncCommandSet.Take(n);

                    DoIntegration(asyncCommandSetPart.OfType<ImportAdditionalServices>());
                    DoIntegration(asyncCommandSetPart.OfType<ImportMunicipalServices>());
                    DoIntegration(asyncCommandSetPart.OfType<ImportHouseData>());
                    DoIntegration(asyncCommandSetPart.OfType<ImportAccountData>());
                    DoIntegration(asyncCommandSetPart.OfType<ImportPaymentDocument2>());
                    DoIntegration(asyncCommandSetPart.OfType<ImportMeteringDevice>());
                    DoIntegration(asyncCommandSetPart.OfType<ImportMeasureData>());

                    if (asyncCommandSet.Count < n)
                    {
                        n = asyncCommandSet.Count;
                    }

                    asyncCommandSet.RemoveRange(0,n);
                }

                //Console.WriteLine("Синхронизация завершена");
                //if (Await)
                //{
                //    Console.ReadLine();
                //}
       

            }
        }

        static void DoIntegration(IEnumerable<MrivcCommand> asyncCommandSet)
        {
            if (WithoutExport == false)
            {
                ////Первый экспорт
                foreach (MrivcCommand command in asyncCommandSet)
                {
                    command.CommandType = MrivcCommand.AsyncCommandType.Send;

                    if (command is MrivcImportCommand)
                        ((MrivcImportCommand)command).CheckBeforeExport = false;

                    if (command is IMrivcImportCommandAsync)
                    {
                        try
                        {
                            ((IMrivcImportCommandAsync)command).ExportData();
                        }
                        catch (MrivcCommandException mcex)
                        {
                            if (command is ISingleHouseCommand)
                            {
                                MrivcStatic.WriteLog(command, mcex, false, ((ISingleHouseCommand)command).FiasHouseGuid);
                            }
                            else
                            {
                                MrivcStatic.WriteLog(command, mcex, false);
                            }

                        }
                        catch (Exception ex)
                        {
                            if (command is ISingleHouseCommand)
                            {
                                MrivcStatic.WriteLog(command, ex, ((ISingleHouseCommand)command).FiasHouseGuid);
                            }
                            else
                            {
                                MrivcStatic.WriteLog(command, ex);
                            }
                        }

                    }
                }

                foreach (MrivcCommand command in asyncCommandSet)
                {
                    command.CommandType = MrivcCommand.AsyncCommandType.GetState;

                    if (command is IMrivcImportCommandAsync)
                    {
                        try
                        {
                            ((IMrivcImportCommandAsync)command).ExportData();
                        }
                        catch (MrivcCommandException mcex)
                        {
                            if (command is ISingleHouseCommand)
                            {
                                MrivcStatic.WriteLog(command, mcex, false, ((ISingleHouseCommand)command).FiasHouseGuid);
                            }
                            else
                            {
                                MrivcStatic.WriteLog(command, mcex, false);
                            }

                        }
                        catch (Exception ex)
                        {
                            if (command is ISingleHouseCommand)
                            {
                                MrivcStatic.WriteLog(command, ex, ((ISingleHouseCommand)command).FiasHouseGuid);
                            }
                            else
                            {
                                MrivcStatic.WriteLog(command, ex);
                            }
                        }
                    }
                }
            }

            //Импорт
            foreach (MrivcCommand command in asyncCommandSet)
            {
                command.CommandType = MrivcCommand.AsyncCommandType.Send;

                if (command is IMrivcImportCommandAsync)
                {
                    try
                    {
                        ((IMrivcImportCommandAsync)command).ImportData();
                    }
                    catch (MrivcCommandException mcex)
                    {
                        if (command is ISingleHouseCommand)
                        {
                            MrivcStatic.WriteLog(command, mcex, false, ((ISingleHouseCommand)command).FiasHouseGuid);
                        }
                        else
                        {
                            MrivcStatic.WriteLog(command, mcex, false);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (command is ISingleHouseCommand)
                        {
                            MrivcStatic.WriteLog(command, ex, ((ISingleHouseCommand)command).FiasHouseGuid);
                        }
                        else
                        {
                            MrivcStatic.WriteLog(command, ex);
                        }
                    }
                }
            }

            foreach (MrivcCommand command in asyncCommandSet)
            {
                command.CommandType = MrivcCommand.AsyncCommandType.GetState;

                if (command is IMrivcImportCommandAsync)
                {
                    try
                    {
                        ((IMrivcImportCommandAsync)command).ImportData();
                    }
                    catch (MrivcCommandException mcex)
                    {
                        if (command is ISingleHouseCommand)
                        {
                            MrivcStatic.WriteLog(command, mcex, false, ((ISingleHouseCommand)command).FiasHouseGuid);
                        }
                        else
                        {
                            MrivcStatic.WriteLog(command, mcex, false);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (command is ISingleHouseCommand)
                        {
                            MrivcStatic.WriteLog(command, ex, ((ISingleHouseCommand)command).FiasHouseGuid);
                        }
                        else
                        {
                            MrivcStatic.WriteLog(command, ex);
                        }
                    }
                }
            }

            //Второй экспорт
            foreach (MrivcCommand command in asyncCommandSet)
            {
                command.CommandType = MrivcCommand.AsyncCommandType.Send;

                if (command is MrivcImportCommand)
                    ((MrivcImportCommand)command).CheckBeforeExport = false;
                else
                    return; //Если команда экспорт, то не выполняем второй экспорт

                if (command is IMrivcImportCommandAsync)
                {
                    try
                    {
                        ((IMrivcImportCommandAsync)command).ExportData();
                    }
                    catch (MrivcCommandException mcex)
                    {
                        if (command is ISingleHouseCommand)
                        {
                            MrivcStatic.WriteLog(command, mcex, false, ((ISingleHouseCommand)command).FiasHouseGuid);
                        }
                        else
                        {
                            MrivcStatic.WriteLog(command, mcex, false);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (command is ISingleHouseCommand)
                        {
                            MrivcStatic.WriteLog(command, ex, ((ISingleHouseCommand)command).FiasHouseGuid);
                        }
                        else
                        {
                            MrivcStatic.WriteLog(command, ex);
                        }
                    }
                }
            }

            foreach (MrivcCommand command in asyncCommandSet)
            {
                command.CommandType = MrivcCommand.AsyncCommandType.GetState;

                if (command is IMrivcImportCommandAsync)
                {
                    try
                    {
                        ((IMrivcImportCommandAsync)command).ExportData();
                    }
                    catch (MrivcCommandException mcex)
                    {
                        if (command is ISingleHouseCommand)
                        {
                            MrivcStatic.WriteLog(command, mcex, false, ((ISingleHouseCommand)command).FiasHouseGuid);
                        }
                        else
                        {
                            MrivcStatic.WriteLog(command, mcex, false);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (command is ISingleHouseCommand)
                        {
                            MrivcStatic.WriteLog(command, ex, ((ISingleHouseCommand)command).FiasHouseGuid);
                        }
                        else
                        {
                            MrivcStatic.WriteLog(command, ex);
                        }
                    }
                }

               
            }

        }

        static public void AsyncCommandIteration(List<MrivcCommandType> commandTypeSet, List<string> senderIdSet, int period,int startPeriod, List<MrivcCommand> asyncCommandSet, List<string> houseGuidList)
        {
            //var AsyncSessionID = int.Parse(DAccess.DataModule.ExecuteScalarStoredProcedure1("GIS.AsyncSessionLogAdd").ToString());

            //foreach (MrivcCommand command in commandTypeSet)
            //{
            //    command.IdAsyncSession = AsyncSessionID;
            //}

            foreach (string _senderID in senderIdSet)
            {
                var senderID = _senderID.Trim();

                foreach (MrivcCommandType command in commandTypeSet)
                {
                    if (command == MrivcCommandType.ImportAdditionalServices || command == MrivcCommandType.ImportMunicipalServices)
                    {
                        //bool serv = Convert.ToBoolean(DAccess.DataModule.ExecuteScalarStoredProcedure1("[GIS].[ForService]", new object[,] { { "SenderID", senderID }, { "period", period } }));

                        //if (serv)
                        //{
                            switch (command)
                            {
                                case MrivcCommandType.ImportAdditionalServices:
                                    asyncCommandSet.Add(new ImportAdditionalServices(senderID) { AddToReport = true });
                                    break;
                                case MrivcCommandType.ImportMunicipalServices:
                                    asyncCommandSet.Add(new ImportMunicipalServices(senderID) { AddToReport = true });
                                    break;
                            }
                        //}
                    }
                    else
                    {
                        HouseIterationAsync(command,senderID, period,startPeriod, asyncCommandSet, houseGuidList);
                    }
                }
            }
        }
    }
}
