﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class ImportMeasureForm : Form
    {
        public string SenderID
        {
            get { return ""; }
        }

        public ImportMeasureForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var importMeasureData = new ImportMeasureData(SenderID);
            var ds = MrivcStatic.HouseIteration(importMeasureData, true);

            importMeasureDataGridView.DataSource = ds.Tables[0];
        }

        private void ImportMeasureForm_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var importMeasureData = new ImportMeasureData(SenderID);
            MrivcStatic.HouseIteration(importMeasureData, false);
        }
    }
}
