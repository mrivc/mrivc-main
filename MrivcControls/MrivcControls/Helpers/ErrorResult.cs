﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MrivcControls.Helpers
{
    public class ErrorResult
    {
        public bool IsError { get; private set; } = false;
        public string Description { get; private set; }

        public ErrorResult(bool isError, string description = "")
        {
            this.IsError = isError;
            this.Description = description;
        }

        public void ShowErrorMessage(bool showOkMessageIfNoError = false, string okMessage = "Ошибок не найдено.")
        {
            if (IsError)
                MessageBox.Show(Description, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (showOkMessageIfNoError)
            {
                MessageBox.Show(okMessage, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
