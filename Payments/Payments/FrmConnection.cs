﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Payments.DS;

namespace Payments
{
    public partial class FrmConnection : Form
    {
        public FrmConnection()
        {
            InitializeComponent();
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            txtResult.Text = DAccess.CashTerminal.Connect2((int)nudComNumber.Value, (int)nudTimeout.Value, (int)nudBaudRate.Value, SystemInformation.ComputerName);
            if (DAccess.CashTerminal.Terminal.ResultCode == 0)
            {
                DAccess.CashTerminal.UpdateECRTerminalStatus(true);
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            DAccess.CashTerminal.Terminal.FindDevice();
            txtResult.Text = DAccess.CashTerminal.Terminal.ResultCode.ToString() + " - " + DAccess.CashTerminal.Terminal.ResultCodeDescription;
            nudComNumber.Value = DAccess.CashTerminal.Terminal.PortNumber+1;
            nudBaudRate.Value = DAccess.CashTerminal.Terminal.BaudRate;
            Cursor = Cursors.Default;
        }

        private void btnState_Click(object sender, EventArgs e)
        {
            MessageBox.Show(DAccess.CashTerminal.Terminal.Timeout.ToString());
            MessageBox.Show(DAccess.CashTerminal.ReadTableFieldValue(1, 0, 28).ToString());
            MessageBox.Show(DAccess.CashTerminal.ReadTableFieldValue(1, 1, 28).ToString());
            MessageBox.Show(DAccess.CashTerminal.ReadTableFieldValue(1, 2, 28).ToString());
            MessageBox.Show(DAccess.CashTerminal.ReadTableFieldValue(1, 3, 28).ToString());
            MessageBox.Show(DAccess.CashTerminal.ReadTableFieldValue(1, 28, 28).ToString());
        }

        private void btnPrintOn_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.WriteTableFieldValue(17, 1, 7, "0");
        }

        private void btnPrintOff_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.WriteTableFieldValue(17, 1, 7, "2");
        }

        private void btnStateCheck_Click(object sender, EventArgs e)
        {
            MessageBox.Show(DAccess.CashTerminal.ReadTableFieldValue(17, 1, 7).ToString());
        }
    }
}
