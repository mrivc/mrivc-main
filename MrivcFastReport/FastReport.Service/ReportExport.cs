﻿using FastReport.Utils;
using System;
using System.IO;
using System.ServiceModel.Web;

namespace FastReport.Service
{
    public partial class ReportService : IFastReportService
    {
        private Stream Export(Stream preparedReport, string format)
        {
            MemoryStream preparedStream = PreparePostStream(preparedReport);
            MemoryStream stream = new MemoryStream();
            if (preparedReport != null && preparedStream.Length > 0)
            {
                using (Report r = new Report())
                {
                    Config.WebMode = true;
                    try
                    {
                        r.LoadPrepared(preparedStream);
                        GearItem gear = new GearItem();
                        gear.Name = format.ToUpper();
                        gear.Properties = GetDefaultProp();
                        PrepareOutput(gear, r, stream);
                    }
                    catch (Exception e)
                    {
                        throw new WebFaultException<ErrorHandler>(new ErrorHandler { Cause = e.Message, ErrorCode = 111 },
                            System.Net.HttpStatusCode.InternalServerError);
                    }
                    stream.Position = 0;
                }
            }
            else
                throw new WebFaultException<ErrorHandler>(new ErrorHandler { Cause = "Bad input data", ErrorCode = 108 },
                    System.Net.HttpStatusCode.BadRequest);
            return stream;
        }

        public Stream GetFPX(Stream preparedReport)
        {
            return Export(preparedReport, "FPX");
        }

        public Stream GetPDF(Stream preparedReport)
        {
            return Export(preparedReport, "PDF");
        }

        public Stream GetXLSX(Stream preparedReport)
        {
            return Export(preparedReport, "XLSX");
        }

        public Stream GetDOCX(Stream preparedReport)
        {
            return Export(preparedReport, "DOCX");
        }

        public Stream GetPPTX(Stream preparedReport)
        {
            return Export(preparedReport, "PPTX");
        }

        public Stream GetODS(Stream preparedReport)
        {
            return Export(preparedReport, "ODS");
        }

        public Stream GetODT(Stream preparedReport)
        {
            return Export(preparedReport, "ODT");
        }

        public Stream GetMHT(Stream preparedReport)
        {
            return Export(preparedReport, "MHT");
        }

        public Stream GetCSV(Stream preparedReport)
        {
            return Export(preparedReport, "CSV");
        }

        public Stream GetDBF(Stream preparedReport)
        {
            return Export(preparedReport, "DBF");
        }

        public Stream GetXML(Stream preparedReport)
        {
            return Export(preparedReport, "XML");
        }

        public Stream GetTXT(Stream preparedReport)
        {
            return Export(preparedReport, "TXT");
        }

        public Stream GetRTF(Stream preparedReport)
        {
            return Export(preparedReport, "RTF");
        }

        public Stream GetHTML(Stream preparedReport)
        {
            return Export(preparedReport, "HTML");
        }

        public Stream GetFPXByUUID(string uuid)
        {
            return GetFPX(GetPreparedByUUID(uuid));
        }

        public Stream GetPDFByUUID(string uuid)
        {
            return GetPDF(GetPreparedByUUID(uuid));
        }

        public Stream GetXLSXByUUID(string uuid)
        {
            return GetXLSX(GetPreparedByUUID(uuid));
        }

        public Stream GetDOCXByUUID(string uuid)
        {
            return GetDOCX(GetPreparedByUUID(uuid));
        }

        public Stream GetPPTXByUUID(string uuid)
        {
            return GetPPTX(GetPreparedByUUID(uuid));
        }

        public Stream GetODSByUUID(string uuid)
        {
            return GetODS(GetPreparedByUUID(uuid));
        }

        public Stream GetODTByUUID(string uuid)
        {
            return GetODT(GetPreparedByUUID(uuid));
        }

        public Stream GetMHTByUUID(string uuid)
        {
            return GetMHT(GetPreparedByUUID(uuid));
        }

        public Stream GetCSVByUUID(string uuid)
        {
            return GetCSV(GetPreparedByUUID(uuid));
        }

        public Stream GetDBFByUUID(string uuid)
        {
            return GetDBF(GetPreparedByUUID(uuid));
        }

        public Stream GetXMLByUUID(string uuid)
        {
            return GetXML(GetPreparedByUUID(uuid));
        }

        public Stream GetTXTByUUID(string uuid)
        {
            return GetTXT(GetPreparedByUUID(uuid));
        }

        public Stream GetRTFByUUID(string uuid)
        {
            return GetRTF(GetPreparedByUUID(uuid));
        }

        public Stream GetHTMLByUUID(string uuid)
        {
            return GetHTML(GetPreparedByUUID(uuid));
        }
    }
}