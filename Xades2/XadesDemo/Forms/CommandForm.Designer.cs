﻿namespace XadesDemo
{
    partial class CommandForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommandForm));
            this.executeBtn = new System.Windows.Forms.Button();
            this.commandComBox = new System.Windows.Forms.ComboBox();
            this.serviceComBox = new System.Windows.Forms.ComboBox();
            this.outTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.paramGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.waitDelayLabel = new System.Windows.Forms.Label();
            this.waitDelayTextBox = new System.Windows.Forms.TextBox();
            this.gisSenderProvider1 = new MrivcControls.GisSenderProvider();
            this.waitCountLabel = new System.Windows.Forms.Label();
            this.waitCountTextBox = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.openBtn = new System.Windows.Forms.Button();
            this.wordWrapCheckBox = new System.Windows.Forms.CheckBox();
            this.paramsOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.paramsSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.paramGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // executeBtn
            // 
            this.executeBtn.Location = new System.Drawing.Point(8, 138);
            this.executeBtn.Name = "executeBtn";
            this.executeBtn.Size = new System.Drawing.Size(107, 23);
            this.executeBtn.TabIndex = 0;
            this.executeBtn.Text = "Выполнить";
            this.executeBtn.UseVisualStyleBackColor = true;
            this.executeBtn.Click += new System.EventHandler(this.executeBtn_Click);
            // 
            // commandComBox
            // 
            this.commandComBox.FormattingEnabled = true;
            this.commandComBox.Location = new System.Drawing.Point(57, 111);
            this.commandComBox.Name = "commandComBox";
            this.commandComBox.Size = new System.Drawing.Size(219, 21);
            this.commandComBox.TabIndex = 1;
            this.commandComBox.SelectedIndexChanged += new System.EventHandler(this.commandComBox_SelectedIndexChanged);
            // 
            // serviceComBox
            // 
            this.serviceComBox.FormattingEnabled = true;
            this.serviceComBox.Location = new System.Drawing.Point(57, 80);
            this.serviceComBox.Name = "serviceComBox";
            this.serviceComBox.Size = new System.Drawing.Size(219, 21);
            this.serviceComBox.TabIndex = 2;
            this.serviceComBox.SelectedIndexChanged += new System.EventHandler(this.serviceComBox_SelectedIndexChanged);
            // 
            // outTextBox
            // 
            this.outTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outTextBox.Location = new System.Drawing.Point(3, 38);
            this.outTextBox.Multiline = true;
            this.outTextBox.Name = "outTextBox";
            this.outTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.outTextBox.Size = new System.Drawing.Size(651, 339);
            this.outTextBox.TabIndex = 3;
            this.outTextBox.WordWrap = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Service";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Method";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Output";
            // 
            // paramGrid
            // 
            this.paramGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paramGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.paramGrid.Location = new System.Drawing.Point(3, 177);
            this.paramGrid.Name = "paramGrid";
            this.paramGrid.Size = new System.Drawing.Size(648, 198);
            this.paramGrid.TabIndex = 9;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.refreshBtn);
            this.splitContainer1.Panel1.Controls.Add(this.waitDelayLabel);
            this.splitContainer1.Panel1.Controls.Add(this.waitDelayTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.gisSenderProvider1);
            this.splitContainer1.Panel1.Controls.Add(this.waitCountLabel);
            this.splitContainer1.Panel1.Controls.Add(this.waitCountTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.paramGrid);
            this.splitContainer1.Panel1.Controls.Add(this.saveBtn);
            this.splitContainer1.Panel1.Controls.Add(this.openBtn);
            this.splitContainer1.Panel1.Controls.Add(this.serviceComBox);
            this.splitContainer1.Panel1.Controls.Add(this.executeBtn);
            this.splitContainer1.Panel1.Controls.Add(this.commandComBox);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.wordWrapCheckBox);
            this.splitContainer1.Panel2.Controls.Add(this.outTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Size = new System.Drawing.Size(654, 762);
            this.splitContainer1.SplitterDistance = 378;
            this.splitContainer1.TabIndex = 10;
            // 
            // refreshBtn
            // 
            this.refreshBtn.Image = global::XadesDemo.Properties.Resources._1410521507_Refresh;
            this.refreshBtn.Location = new System.Drawing.Point(121, 138);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(23, 23);
            this.refreshBtn.TabIndex = 19;
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // waitDelayLabel
            // 
            this.waitDelayLabel.Location = new System.Drawing.Point(350, 45);
            this.waitDelayLabel.Name = "waitDelayLabel";
            this.waitDelayLabel.Size = new System.Drawing.Size(200, 17);
            this.waitDelayLabel.TabIndex = 15;
            this.waitDelayLabel.Text = "Время в секундах между попытками";
            // 
            // waitDelayTextBox
            // 
            this.waitDelayTextBox.Location = new System.Drawing.Point(320, 42);
            this.waitDelayTextBox.Name = "waitDelayTextBox";
            this.waitDelayTextBox.Size = new System.Drawing.Size(24, 20);
            this.waitDelayTextBox.TabIndex = 14;
            this.waitDelayTextBox.Text = "2";
            // 
            // gisSenderProvider1
            // 
            this.gisSenderProvider1.DataSource = null;
            this.gisSenderProvider1.DisplayMember = "SenderName";
            this.gisSenderProvider1.Location = new System.Drawing.Point(9, 12);
            this.gisSenderProvider1.Name = "gisSenderProvider1";
            this.gisSenderProvider1.ParameterName = "Поставщик информации ГИС ЖКХ";
            this.gisSenderProvider1.Size = new System.Drawing.Size(304, 55);
            this.gisSenderProvider1.TabIndex = 13;
            this.gisSenderProvider1.ValueMember = "SenderID";
            // 
            // waitCountLabel
            // 
            this.waitCountLabel.Location = new System.Drawing.Point(372, 12);
            this.waitCountLabel.Name = "waitCountLabel";
            this.waitCountLabel.Size = new System.Drawing.Size(270, 29);
            this.waitCountLabel.TabIndex = 11;
            this.waitCountLabel.Text = "Количество попыток выполнения команды get-state до получения статуса \"обработан\"";
            // 
            // waitCountTextBox
            // 
            this.waitCountTextBox.Location = new System.Drawing.Point(320, 16);
            this.waitCountTextBox.MaxLength = 3;
            this.waitCountTextBox.Name = "waitCountTextBox";
            this.waitCountTextBox.Size = new System.Drawing.Size(46, 20);
            this.waitCountTextBox.TabIndex = 10;
            this.waitCountTextBox.Text = "10";
            // 
            // saveBtn
            // 
            this.saveBtn.Image = global::XadesDemo.Properties.Resources.Save;
            this.saveBtn.Location = new System.Drawing.Point(253, 138);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(23, 23);
            this.saveBtn.TabIndex = 9;
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // openBtn
            // 
            this.openBtn.Image = global::XadesDemo.Properties.Resources.Open;
            this.openBtn.Location = new System.Drawing.Point(224, 138);
            this.openBtn.Name = "openBtn";
            this.openBtn.Size = new System.Drawing.Size(23, 23);
            this.openBtn.TabIndex = 8;
            this.openBtn.UseVisualStyleBackColor = true;
            this.openBtn.Click += new System.EventHandler(this.openBtn_Click);
            // 
            // wordWrapCheckBox
            // 
            this.wordWrapCheckBox.AutoSize = true;
            this.wordWrapCheckBox.Location = new System.Drawing.Point(57, 13);
            this.wordWrapCheckBox.Name = "wordWrapCheckBox";
            this.wordWrapCheckBox.Size = new System.Drawing.Size(102, 17);
            this.wordWrapCheckBox.TabIndex = 8;
            this.wordWrapCheckBox.Text = "Перенос строк";
            this.wordWrapCheckBox.UseVisualStyleBackColor = true;
            this.wordWrapCheckBox.CheckedChanged += new System.EventHandler(this.wordWrapCheckBox_CheckedChanged);
            // 
            // paramsOpenFileDialog
            // 
            this.paramsOpenFileDialog.DefaultExt = "txt";
            this.paramsOpenFileDialog.Filter = "Text|*.txt";
            // 
            // paramsSaveFileDialog
            // 
            this.paramsSaveFileDialog.DefaultExt = "txt";
            this.paramsSaveFileDialog.Filter = "Text|*.txt";
            // 
            // CommandForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 762);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CommandForm";
            this.Text = "CommandForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.paramGrid)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button executeBtn;
        private System.Windows.Forms.ComboBox commandComBox;
        private System.Windows.Forms.ComboBox serviceComBox;
        private System.Windows.Forms.TextBox outTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView paramGrid;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button openBtn;
        private System.Windows.Forms.OpenFileDialog paramsOpenFileDialog;
        private System.Windows.Forms.SaveFileDialog paramsSaveFileDialog;
        private System.Windows.Forms.CheckBox wordWrapCheckBox;
        private System.Windows.Forms.Label waitCountLabel;
        private System.Windows.Forms.TextBox waitCountTextBox;
        private MrivcControls.GisSenderProvider gisSenderProvider1;
        private System.Windows.Forms.Label waitDelayLabel;
        private System.Windows.Forms.TextBox waitDelayTextBox;
        private System.Windows.Forms.Button refreshBtn;
    }
}