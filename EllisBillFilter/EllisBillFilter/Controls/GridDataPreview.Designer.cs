﻿namespace EllisBillFilter.Controls
{
    partial class GridDataPreview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridData = new EllisBillFilter.BaseGrid();
            this.gridDataView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dabaBaseTablePicker1 = new MrivcControls.DataPickers.DabaBaseTablePicker();
            this.btnTableListReload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataView)).BeginInit();
            this.SuspendLayout();
            // 
            // gridData
            // 
            this.gridData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridData.Location = new System.Drawing.Point(0, 41);
            this.gridData.MainView = this.gridDataView;
            this.gridData.Name = "gridData";
            this.gridData.Size = new System.Drawing.Size(808, 419);
            this.gridData.TabIndex = 0;
            this.gridData.TrimCellValueBeforeValidate = true;
            this.gridData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridDataView});
            // 
            // gridDataView
            // 
            this.gridDataView.GridControl = this.gridData;
            this.gridDataView.Name = "gridDataView";
            this.gridDataView.OptionsView.ShowGroupPanel = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Таблица данных:";
            // 
            // dabaBaseTablePicker1
            // 
            this.dabaBaseTablePicker1.Location = new System.Drawing.Point(102, 11);
            this.dabaBaseTablePicker1.Name = "dabaBaseTablePicker1";
            this.dabaBaseTablePicker1.SelectedTable = "";
            this.dabaBaseTablePicker1.Size = new System.Drawing.Size(253, 21);
            this.dabaBaseTablePicker1.TabIndex = 2;
            // 
            // btnTableListReload
            // 
            this.btnTableListReload.Image = global::EllisBillFilter.Properties.Resources.refresh;
            this.btnTableListReload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTableListReload.Location = new System.Drawing.Point(362, 10);
            this.btnTableListReload.Name = "btnTableListReload";
            this.btnTableListReload.Size = new System.Drawing.Size(78, 23);
            this.btnTableListReload.TabIndex = 3;
            this.btnTableListReload.Text = "обновить";
            this.btnTableListReload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTableListReload.UseVisualStyleBackColor = true;
            this.btnTableListReload.Click += new System.EventHandler(this.btnTableListReload_Click);
            // 
            // GridDataPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnTableListReload);
            this.Controls.Add(this.dabaBaseTablePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gridData);
            this.Name = "GridDataPreview";
            this.Size = new System.Drawing.Size(808, 460);
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseGrid gridData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridDataView;
        private System.Windows.Forms.Label label1;
        private MrivcControls.DataPickers.DabaBaseTablePicker dabaBaseTablePicker1;
        private System.Windows.Forms.Button btnTableListReload;
    }
}
