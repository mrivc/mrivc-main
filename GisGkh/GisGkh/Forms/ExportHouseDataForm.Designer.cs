﻿namespace GisGkh
{
    partial class ExportHouseDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportHouseDataForm));
            this.exportDataGridView = new System.Windows.Forms.DataGridView();
            this.exportBtn = new System.Windows.Forms.Button();
            this.entranceDataGridView = new System.Windows.Forms.DataGridView();
            this.premisesDataGridView = new System.Windows.Forms.DataGridView();
            this.accountDataGridView = new System.Windows.Forms.DataGridView();
            this.exportToDbBtn = new System.Windows.Forms.Button();
            this.exportAccountBtn = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.exportAccountToDbBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.exportDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entranceDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.premisesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountDataGridView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // exportDataGridView
            // 
            this.exportDataGridView.AllowUserToAddRows = false;
            this.exportDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.exportDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exportDataGridView.Location = new System.Drawing.Point(3, 3);
            this.exportDataGridView.Name = "exportDataGridView";
            this.exportDataGridView.Size = new System.Drawing.Size(1165, 436);
            this.exportDataGridView.TabIndex = 1;
            this.exportDataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.exportDataGridView_RowsAdded);
            this.exportDataGridView.SelectionChanged += new System.EventHandler(this.exportDataGridView_SelectionChanged);
            // 
            // exportBtn
            // 
            this.exportBtn.Location = new System.Drawing.Point(707, 12);
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(162, 23);
            this.exportBtn.TabIndex = 3;
            this.exportBtn.Text = "Экспорт";
            this.exportBtn.UseVisualStyleBackColor = true;
            this.exportBtn.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // entranceDataGridView
            // 
            this.entranceDataGridView.AllowUserToAddRows = false;
            this.entranceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.entranceDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.entranceDataGridView.Location = new System.Drawing.Point(3, 3);
            this.entranceDataGridView.Name = "entranceDataGridView";
            this.entranceDataGridView.Size = new System.Drawing.Size(1165, 436);
            this.entranceDataGridView.TabIndex = 0;
            this.entranceDataGridView.SelectionChanged += new System.EventHandler(this.entranceDataGridView_SelectionChanged);
            // 
            // premisesDataGridView
            // 
            this.premisesDataGridView.AllowUserToAddRows = false;
            this.premisesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.premisesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.premisesDataGridView.Location = new System.Drawing.Point(3, 3);
            this.premisesDataGridView.Name = "premisesDataGridView";
            this.premisesDataGridView.Size = new System.Drawing.Size(1165, 436);
            this.premisesDataGridView.TabIndex = 0;
            // 
            // accountDataGridView
            // 
            this.accountDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.accountDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.accountDataGridView.Location = new System.Drawing.Point(3, 3);
            this.accountDataGridView.Name = "accountDataGridView";
            this.accountDataGridView.Size = new System.Drawing.Size(1165, 436);
            this.accountDataGridView.TabIndex = 0;
            this.accountDataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.accountDataGridView_RowsAdded);
            // 
            // exportToDbBtn
            // 
            this.exportToDbBtn.Location = new System.Drawing.Point(875, 12);
            this.exportToDbBtn.Name = "exportToDbBtn";
            this.exportToDbBtn.Size = new System.Drawing.Size(170, 23);
            this.exportToDbBtn.TabIndex = 8;
            this.exportToDbBtn.Text = "Экспорт в БД";
            this.exportToDbBtn.UseVisualStyleBackColor = true;
            this.exportToDbBtn.Click += new System.EventHandler(this.exportToDbBtn_Click);
            // 
            // exportAccountBtn
            // 
            this.exportAccountBtn.Location = new System.Drawing.Point(707, 41);
            this.exportAccountBtn.Name = "exportAccountBtn";
            this.exportAccountBtn.Size = new System.Drawing.Size(162, 23);
            this.exportAccountBtn.TabIndex = 9;
            this.exportAccountBtn.Text = "Экспорт ЛС";
            this.exportAccountBtn.UseVisualStyleBackColor = true;
            this.exportAccountBtn.Click += new System.EventHandler(this.exportAccountBtn_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(2, 73);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1179, 468);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.exportDataGridView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1171, 442);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Дома";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.entranceDataGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1171, 442);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Подъезды";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.premisesDataGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1171, 442);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Квартиры";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.accountDataGridView);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1171, 442);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Лицевые счета";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // exportAccountToDbBtn
            // 
            this.exportAccountToDbBtn.Location = new System.Drawing.Point(875, 41);
            this.exportAccountToDbBtn.Name = "exportAccountToDbBtn";
            this.exportAccountToDbBtn.Size = new System.Drawing.Size(170, 23);
            this.exportAccountToDbBtn.TabIndex = 11;
            this.exportAccountToDbBtn.Text = "Экспорт ЛС в БД";
            this.exportAccountToDbBtn.UseVisualStyleBackColor = true;
            this.exportAccountToDbBtn.Click += new System.EventHandler(this.exportAccountToDbBtn_Click);
            // 
            // ExportHouseDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 539);
            this.Controls.Add(this.exportAccountToDbBtn);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.exportAccountBtn);
            this.Controls.Add(this.exportToDbBtn);
            this.Controls.Add(this.exportBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportHouseDataForm";
            this.Text = "Экспорт информации о домах";
            this.Load += new System.EventHandler(this.ExportHouseDataForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.exportDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entranceDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.premisesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountDataGridView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView exportDataGridView;
        private System.Windows.Forms.Button exportBtn;
        private System.Windows.Forms.DataGridView entranceDataGridView;
        private System.Windows.Forms.DataGridView premisesDataGridView;
        private System.Windows.Forms.DataGridView accountDataGridView;
        private System.Windows.Forms.Button exportToDbBtn;
        private System.Windows.Forms.Button exportAccountBtn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button exportAccountToDbBtn;
    }
}