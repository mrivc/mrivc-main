﻿using System;
using System.Collections.Generic;
using System.Text;
using Word=Microsoft.Office.Interop.Word;

namespace Payments.Helpers
{
    // Word запускается ОТДЕЛЬНЫМ ПРОЦЕССОМ, класс просто управляет им через библиотеку Word Interoperability, на компьютере должен быть установлен офис, в проекте должна быть ссылка на Microsoft.Office.Interop.Word, соотвествующая библиотека .dll должна быть в папке с программой
    // документ MS WORD, позволяет создать новый документ по шаблону, произвести поиск и замену строк (одно вхождение или все), изменить видимость документа, закрыть документ
    //
    class WordDocument
    {
	    // фиксированные параметры для передачи приложению Word
	    Object wordMissing = System.Reflection.Missing.Value;
        Object wordTrue = true;
        Object wordFalse = false;
 
	    //рабочие параметры
	    // если использовать Word.Application и Word.Document получим предупреждение от компиллятора
	    Word._Application wordApplication;
	    Word._Document wordDocument;
	    Object templatePathObj;
	 
	    // определяет поведение в случае ошибки при поиске и замене строки, по умолчанию открытый документ и приложенеи Word закрываются
	    public bool CloseIFReplaceFailed = true;
	 
	    // видимость на экране приложения Word
	    // по умолчанию false
	    public bool Visible
	    {
	        get
	        {
	            if (documentClosed()) { throw new Exception("Ошибка при попытке изменить видимость Microsoft Word. Программа или документ уже закрыты.");  }
	            return wordApplication.Visible; 
	 
	        }
	        set
	        {
	            if (documentClosed()) { throw new Exception("Ошибка при попытке изменить видимость Microsoft Word. Программа или документ уже закрыты."); }
	            wordApplication.Visible = value;
	            }
	        // завершение public bool Visible
	    }
	 
	    // конструктор, создаем по шаблону, потом возможно расширение другими вариантами
	    public WordDocument(string templatePath)
	    {
	        createFromTemplate(templatePath);
	    }
	 
	    // создаем приложение Word и открывает новый документ по заданному файлу шаблона
	    public void createFromTemplate(string templatePath)
	    {
	        //создаем обьект приложения word
	        wordApplication = new Word.Application();
	 
	        // создаем путь к файлу используя имя файла
	        templatePathObj = templatePath;
	 
	        //создаем ноый документ методом приложения Word по поути к шаблону документа
	        try
	        {
	            wordDocument = wordApplication.Documents.Add(ref  templatePathObj, ref wordMissing, ref wordMissing, ref wordMissing);
	        }
	        // если произошла ошибка, то приложение Word надо закрыть
	        catch (Exception error)
	        {
	            wordApplication.Quit(ref wordMissing, ref  wordMissing, ref wordMissing);
	            wordApplication = null;
	            throw new Exception("Ошибка. Не удалось открыть шаблон документа MS Word. " + error.Message);
	        }
	        // завершение createFromTemplate(string templatePath)
	    }
	 
	    // ПОИСК И ЗАМЕНА ЗАДАННОЙ СТРОКИ
	    public void ReplaceString(string strToFind, string replaceStr, string replaceTypeStr)
	    {
	        if (documentClosed()) { throw new Exception("Ошибка при выполнении поиска и замены в Microsoft Word. Программа или документ уже закрыты."); } 
	 
	        // обьектные строки для Word
	        object strToFindObj = strToFind;
	        object replaceStrObj = replaceStr;
	        // диапазон документа Word
	        Word.Range wordRange;
	        //тип поиска и замены
	        object replaceTypeObj;
	 
	        if (replaceTypeStr == "all")
	        {
	            // заменять все найденные вхождения
	            replaceTypeObj = Word.WdReplace.wdReplaceAll;
	        }
	        else if (replaceTypeStr == "one")
	        {
	            // заменять только первое найденное вхождение
	            replaceTypeObj = Word.WdReplace.wdReplaceOne;
	        }
	        else
	        {
	            this.Close();
	            throw new Exception("Неизвестный тип поиска и замены в документе Word.");
	        }
	 
	        try
	        {
	            // обходим все разделы документа
	            for (int i = 1; i <= wordDocument.Sections.Count; i++)
	            {
	                // берем всю секцию диапазоном
	                wordRange = wordDocument.Sections[i].Range;
	 
	                // выполняем метод поискаи  замены обьекта диапазона ворд
	                wordRange.Find.Execute(ref strToFindObj, ref wordMissing, ref wordMissing, ref wordMissing,
	                ref wordMissing, ref wordMissing, ref wordMissing, ref wordMissing, ref wordMissing, ref replaceStrObj,
	                ref replaceTypeObj, ref wordMissing, ref wordMissing, ref wordMissing, ref wordMissing);
	            }
	        }
	        catch (Exception error)
	        {
	            if (CloseIFReplaceFailed)
	            {
	                this.Close();
	            }
	            throw new Exception("Ошибка при выполнении поиска и замены в документе Word.  " + error.Message);
	        }
	        // завершение функции поиска и замены SearchAndReplace
	    }
	 
	    // закрытие открытого документа и приложения
	    public void Close()
	    {
	        if (documentClosed()) { throw new Exception("Ошибка при попытке закрыть Microsoft Word. Программа или документ уже закрыты."); }

            try
            {
                wordDocument.Close(ref wordFalse, ref  wordMissing, ref wordMissing);
                wordApplication.Quit(ref wordMissing, ref  wordMissing, ref wordMissing);
                wordDocument = null;
                wordApplication = null;
            }
            catch { }
	    }
	 
	    // если документ уже закрыт, true
	    private bool documentClosed()
	    {
	        if (this.wordApplication == null || this.wordDocument == null) { return true; }
	        else { return false; }
	    }
	 
	    // завершение class WordDocument
	}
}
