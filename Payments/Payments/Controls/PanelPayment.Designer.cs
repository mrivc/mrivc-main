﻿namespace Payments.Controls
{
    partial class PanelPayment
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.txtCash = new DevExpress.XtraEditors.SpinEdit();
            this.txtSumm = new DevExpress.XtraEditors.SpinEdit();
            this.lblPaymentDate = new System.Windows.Forms.Label();
            this.lblPaymentCash = new System.Windows.Forms.Label();
            this.lblPaymentSumm = new System.Windows.Forms.Label();
            this.txtComment = new DevExpress.XtraEditors.TextEdit();
            this.lblPaymentComment = new System.Windows.Forms.Label();
            this.txtCharge = new DevExpress.XtraEditors.TextEdit();
            this.txtPaymentDate = new DevExpress.XtraEditors.DateEdit();
            this.txtSummCommission = new DevExpress.XtraEditors.SpinEdit();
            this.lblPaymentCommission = new System.Windows.Forms.Label();
            this.txtSummTotal = new DevExpress.XtraEditors.SpinEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCommissionPersent = new System.Windows.Forms.Label();
            this.btnMakePayVisa = new DevExpress.XtraEditors.SimpleButton();
            this.btnMakePay = new DevExpress.XtraEditors.SimpleButton();
            this.txtPhone = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtCash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummCommission.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCash
            // 
            this.txtCash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCash.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCash.Location = new System.Drawing.Point(103, 110);
            this.txtCash.Name = "txtCash";
            this.txtCash.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtCash.Properties.Mask.EditMask = "c";
            this.txtCash.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCash.Size = new System.Drawing.Size(151, 20);
            this.txtCash.TabIndex = 1;
            this.txtCash.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtCash_EditValueChanging);
            // 
            // txtSumm
            // 
            this.txtSumm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSumm.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSumm.Location = new System.Drawing.Point(103, 32);
            this.txtSumm.Name = "txtSumm";
            this.txtSumm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtSumm.Properties.Mask.EditMask = "c";
            this.txtSumm.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSumm.Size = new System.Drawing.Size(151, 20);
            this.txtSumm.TabIndex = 0;
            this.txtSumm.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtSumm_EditValueChanging);
            // 
            // lblPaymentDate
            // 
            this.lblPaymentDate.AutoSize = true;
            this.lblPaymentDate.Location = new System.Drawing.Point(2, 9);
            this.lblPaymentDate.Name = "lblPaymentDate";
            this.lblPaymentDate.Size = new System.Drawing.Size(82, 13);
            this.lblPaymentDate.TabIndex = 3;
            this.lblPaymentDate.Text = "Дата платежа:";
            // 
            // lblPaymentCash
            // 
            this.lblPaymentCash.AutoSize = true;
            this.lblPaymentCash.Location = new System.Drawing.Point(2, 113);
            this.lblPaymentCash.Name = "lblPaymentCash";
            this.lblPaymentCash.Size = new System.Drawing.Size(61, 13);
            this.lblPaymentCash.TabIndex = 4;
            this.lblPaymentCash.Text = "Наличные:";
            // 
            // lblPaymentSumm
            // 
            this.lblPaymentSumm.AutoSize = true;
            this.lblPaymentSumm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPaymentSumm.ForeColor = System.Drawing.Color.Blue;
            this.lblPaymentSumm.Location = new System.Drawing.Point(2, 35);
            this.lblPaymentSumm.Name = "lblPaymentSumm";
            this.lblPaymentSumm.Size = new System.Drawing.Size(90, 13);
            this.lblPaymentSumm.TabIndex = 5;
            this.lblPaymentSumm.Text = "Сумма платежа:";
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(103, 136);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(151, 20);
            this.txtComment.TabIndex = 2;
            this.txtComment.Leave += new System.EventHandler(this.txtComment_Leave);
            // 
            // lblPaymentComment
            // 
            this.lblPaymentComment.AutoSize = true;
            this.lblPaymentComment.Location = new System.Drawing.Point(2, 139);
            this.lblPaymentComment.Name = "lblPaymentComment";
            this.lblPaymentComment.Size = new System.Drawing.Size(80, 13);
            this.lblPaymentComment.TabIndex = 7;
            this.lblPaymentComment.Text = "Комментарий:";
            // 
            // txtCharge
            // 
            this.txtCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCharge.EditValue = "0 000,00";
            this.txtCharge.Location = new System.Drawing.Point(268, 136);
            this.txtCharge.Name = "txtCharge";
            this.txtCharge.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtCharge.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtCharge.Properties.Appearance.Options.UseBackColor = true;
            this.txtCharge.Properties.Appearance.Options.UseFont = true;
            this.txtCharge.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCharge.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCharge.Properties.Mask.EditMask = "c";
            this.txtCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCharge.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCharge.Properties.ReadOnly = true;
            this.txtCharge.Size = new System.Drawing.Size(76, 20);
            this.txtCharge.TabIndex = 4;
            // 
            // txtPaymentDate
            // 
            this.txtPaymentDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentDate.EditValue = null;
            this.txtPaymentDate.Location = new System.Drawing.Point(103, 6);
            this.txtPaymentDate.Name = "txtPaymentDate";
            this.txtPaymentDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtPaymentDate.Properties.DisplayFormat.FormatString = "G";
            this.txtPaymentDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtPaymentDate.Properties.EditFormat.FormatString = "G";
            this.txtPaymentDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtPaymentDate.Properties.Mask.EditMask = "G";
            this.txtPaymentDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPaymentDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPaymentDate.Size = new System.Drawing.Size(151, 20);
            this.txtPaymentDate.TabIndex = 17;
            // 
            // txtSummCommission
            // 
            this.txtSummCommission.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSummCommission.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSummCommission.Location = new System.Drawing.Point(157, 58);
            this.txtSummCommission.Name = "txtSummCommission";
            this.txtSummCommission.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtSummCommission.Properties.Appearance.Options.UseBackColor = true;
            this.txtSummCommission.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.txtSummCommission.Properties.Mask.EditMask = "c";
            this.txtSummCommission.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSummCommission.Properties.ReadOnly = true;
            this.txtSummCommission.Size = new System.Drawing.Size(97, 20);
            this.txtSummCommission.TabIndex = 18;
            // 
            // lblPaymentCommission
            // 
            this.lblPaymentCommission.AutoSize = true;
            this.lblPaymentCommission.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPaymentCommission.ForeColor = System.Drawing.Color.Blue;
            this.lblPaymentCommission.Location = new System.Drawing.Point(2, 61);
            this.lblPaymentCommission.Name = "lblPaymentCommission";
            this.lblPaymentCommission.Size = new System.Drawing.Size(61, 13);
            this.lblPaymentCommission.TabIndex = 19;
            this.lblPaymentCommission.Text = "Комиссия:";
            // 
            // txtSummTotal
            // 
            this.txtSummTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSummTotal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSummTotal.Location = new System.Drawing.Point(103, 84);
            this.txtSummTotal.Name = "txtSummTotal";
            this.txtSummTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtSummTotal.Properties.Appearance.Options.UseBackColor = true;
            this.txtSummTotal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.txtSummTotal.Properties.Mask.EditMask = "c";
            this.txtSummTotal.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSummTotal.Properties.ReadOnly = true;
            this.txtSummTotal.Size = new System.Drawing.Size(151, 20);
            this.txtSummTotal.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(2, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "ИТОГО:";
            // 
            // lblCommissionPersent
            // 
            this.lblCommissionPersent.AutoSize = true;
            this.lblCommissionPersent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCommissionPersent.ForeColor = System.Drawing.Color.Gray;
            this.lblCommissionPersent.Location = new System.Drawing.Point(100, 61);
            this.lblCommissionPersent.Name = "lblCommissionPersent";
            this.lblCommissionPersent.Size = new System.Drawing.Size(33, 13);
            this.lblCommissionPersent.TabIndex = 22;
            this.lblCommissionPersent.Text = "0% = ";
            // 
            // btnMakePayVisa
            // 
            this.btnMakePayVisa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMakePayVisa.Image = global::Payments.Properties.Resources.visacard;
            this.btnMakePayVisa.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnMakePayVisa.Location = new System.Drawing.Point(268, 61);
            this.btnMakePayVisa.Name = "btnMakePayVisa";
            this.btnMakePayVisa.Size = new System.Drawing.Size(76, 48);
            this.btnMakePayVisa.TabIndex = 23;
            this.btnMakePayVisa.ToolTip = "Платеж кредитной картой";
            this.btnMakePayVisa.Click += new System.EventHandler(this.btnMakePayVisa_Click);
            // 
            // btnMakePay
            // 
            this.btnMakePay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMakePay.Image = global::Payments.Properties.Resources.cash_register32;
            this.btnMakePay.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMakePay.Location = new System.Drawing.Point(268, 4);
            this.btnMakePay.Name = "btnMakePay";
            this.btnMakePay.Size = new System.Drawing.Size(76, 48);
            this.btnMakePay.TabIndex = 3;
            this.btnMakePay.ToolTip = "Быстрый платеж <Ctrl+Enter>";
            this.btnMakePay.Click += new System.EventHandler(this.btnMakePay_Click);
            // 
            // txtPhone
            // 
            this.txtPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPhone.Location = new System.Drawing.Point(103, 162);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(241, 20);
            this.txtPhone.TabIndex = 25;
            this.txtPhone.Leave += new System.EventHandler(this.txtPhone_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "E-mail:";
            // 
            // PanelPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.btnMakePayVisa);
            this.Controls.Add(this.lblCommissionPersent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSummTotal);
            this.Controls.Add(this.lblPaymentCommission);
            this.Controls.Add(this.txtSummCommission);
            this.Controls.Add(this.txtPaymentDate);
            this.Controls.Add(this.btnMakePay);
            this.Controls.Add(this.txtCharge);
            this.Controls.Add(this.lblPaymentComment);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.lblPaymentSumm);
            this.Controls.Add(this.lblPaymentCash);
            this.Controls.Add(this.lblPaymentDate);
            this.Controls.Add(this.txtSumm);
            this.Controls.Add(this.txtCash);
            this.Name = "PanelPayment";
            this.Size = new System.Drawing.Size(349, 188);
            this.Load += new System.EventHandler(this.PanelPayment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtCash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummCommission.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SpinEdit txtCash;
        private DevExpress.XtraEditors.SpinEdit txtSumm;
        private System.Windows.Forms.Label lblPaymentDate;
        private System.Windows.Forms.Label lblPaymentCash;
        private System.Windows.Forms.Label lblPaymentSumm;
        private DevExpress.XtraEditors.TextEdit txtComment;
        private System.Windows.Forms.Label lblPaymentComment;
        private DevExpress.XtraEditors.TextEdit txtCharge;
        private DevExpress.XtraEditors.SimpleButton btnMakePay;
        private DevExpress.XtraEditors.DateEdit txtPaymentDate;
        private DevExpress.XtraEditors.SpinEdit txtSummCommission;
        private System.Windows.Forms.Label lblPaymentCommission;
        private DevExpress.XtraEditors.SpinEdit txtSummTotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCommissionPersent;
        private DevExpress.XtraEditors.SimpleButton btnMakePayVisa;
        private DevExpress.XtraEditors.TextEdit txtPhone;
        private System.Windows.Forms.Label label2;
    }
}
