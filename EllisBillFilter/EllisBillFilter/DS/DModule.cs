﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;

namespace EllisBillFilter.DS
{
    using MrivcHelpers.Helpers;

    public partial class DModule : Component
    {
        public DModule()
        {
            InitializeComponent();
        }

        public DModule(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public SqlConnectionStringBuilder ConnectionInfo
        {
            get
            {
                return new SqlConnectionStringBuilder(ConnectionString);
            }
        }

        public string ConnectionString
        {
            set {
                SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();

                //Буторин 27-04-16 Добавлена возможность входа с логином Windows
                if (isWinLogin(value))
                    connBuilder.IntegratedSecurity = true;
                else
                {
                    connBuilder.UserID = value.GetSqlConnectionPart("UserID");
                    connBuilder.Password = value.GetSqlConnectionPart("Password");
                }

                connBuilder.InitialCatalog = value.GetSqlConnectionPart("InitialCatalog");
                connBuilder.DataSource = value.GetSqlConnectionPart("DataSource");
                connBuilder.PersistSecurityInfo = true;
                connBuilder.ConnectTimeout = 200;
                sqlMainConnection.Close();
                sqlMainConnection.ConnectionString = connBuilder.ConnectionString;
                sqlMainConnection.Open();
            }
            get
            {
                return sqlMainConnection.ConnectionString;
            }
        }

        bool isWinLogin(string connectionString)
        {
            string value = string.Empty;

            try
            {
                value = connectionString.GetSqlConnectionPart("IntegratedSecurity");
            }
            catch { }

            if (value.ToLower() == "true" || value.ToLower() == "sspi")
                return true;
            else
                return false;
        }

        public void SetConnection(string connectionString)
        {
            sqlMainConnection.Close();
            sqlMainConnection.ConnectionString = connectionString;
            sqlMainConnection.Open();
        }

        public SqlConnection Connection
        {
            get
            {
                return sqlMainConnection;
            }
        }

        public int GetCurrentUserId()
        {
            object userId = ExecuteScalarQueryCommand("select dbo.get_user_id()");
            if (userId == null || userId == DBNull.Value)
                userId = 1265;
            return (int)userId;
        }

        public void ClientDetailSelect(MDataSet.ClientDetailDataTable table, int period, bool showActive)
        {
            daClientDetail.SelectCommand.Connection.Close();
            daClientDetail.SelectCommand.Parameters["@Period"].Value = period;
            daClientDetail.SelectCommand.Parameters["@ShowActive"].Value = showActive;
            daClientDetail.SelectCommand.Connection.Open();
            daClientDetail.Fill(table);
        }

        public void ClientDetailUpdate(MDataSet.ClientDetailDataTable table)
        {
            daClientDetail.UpdateCommand.Connection.Close();
            daClientDetail.UpdateCommand.Connection.Open();
            daClientDetail.Update(table);
        }

        public void ClientDetailCopy(MDataSet.ClientDetailDataTable table, int idClientDetailSource)
        {
            daClientDetailCopy.InsertCommand.Connection.Close();
            daClientDetailCopy.InsertCommand.Parameters["@ID_Client_Detail_Source"].Value = idClientDetailSource;
            daClientDetailCopy.InsertCommand.Connection.Open();
            daClientDetailCopy.Update(table);
        }

        public void DeriveCompanySelect(MDataSet.DeriveCompanyDataTable table)
        {
            daDeriveCompany.Fill(table);
        }

        public void HouseHierarhySelect(MDataSet.HouseHierarhyDataTable table)
        {
            daHouseHierarhy.Fill(table);
        }

        public void PayReceiverSelect(MDataSet.PayReceiverCompanyDataTable table)
        {
            daPayReceiver.Fill(table);
        }

        public void PeriodSelect(MDataSet.PeriodDataTable table)
        {
            daPeriod.Fill(table);
        }

        public void BillFilterSelect(MDataSet.BillFilterDataTable table)
        {
            daBillFilter.Fill(table);
        }

        public void FilterTypeSelect(MDataSet.FilterTypeDataTable table)
        {
            daFilterType.Fill(table);
        }

        /// <summary>
        /// Выбор списка домов по которым действует или нет тот или иной код фильтра
        /// </summary>
        /// <param name="tableSelect">Таблица для заполнения</param>
        /// <param name="idClientDetail">idClientDetail из таблицы Reports.Client_Detail (код фильтра для УК)</param>
        /// <param name="period">Период в котором дома принадлежали УК</param>
        /// <param name="show_all_houses">False - показывать только текущие дома по УК, по заданному периоду. True - показывать все дома которые когда либо были в УК.</param>
        public void ClientDetailHouseSelect(MDataSet.ClientDetailHouseDataTable tableSelect, int idClientDetail, int period, bool show_all_houses)
        {
            daClientDetailHouse.SelectCommand.Connection.Close();
            daClientDetailHouse.SelectCommand.Parameters["@ID_Client_Detail"].Value = idClientDetail;
            daClientDetailHouse.SelectCommand.Parameters["@Period"].Value = period;
            daClientDetailHouse.SelectCommand.Parameters["@ID_Derive_Company"].Value = 0; //этот параметро нужно удалить из хранимки [Reports].[ClientDetailHouseLinked_Select]
            daClientDetailHouse.SelectCommand.Parameters["@Show_All_Houses"].Value = show_all_houses;
            daClientDetailHouse.SelectCommand.Connection.Open();
            daClientDetailHouse.Fill(tableSelect);
        }

        public void BillFilterDetailProviderFilterSelect(MDataSet.BillFilterDetailProviderFilterDataTable table, int idBillFilter)
        {
            daBillFilterDetailProviderFilter.SelectCommand.Connection.Close();
            daBillFilterDetailProviderFilter.SelectCommand.Parameters["@ID_Bill_Filter"].Value = idBillFilter;
            daBillFilterDetailProviderFilter.SelectCommand.Connection.Open();
            daBillFilterDetailProviderFilter.Fill(table);
        }

        public void BillFilterDetailProviderFilterUpdate(MDataSet.BillFilterDetailProviderFilterDataTable table)
        {
            daBillFilterDetailProviderFilter.Update(table);
        }

        public void FilterHouseSelect(MDataSet.ClientDetailDataTable table, int period, long idHouse)
        {
            daFilterHouse.SelectCommand.Connection.Close();
            daFilterHouse.SelectCommand.Parameters["@Period"].Value = period;
            daFilterHouse.SelectCommand.Parameters["@ID_House"].Value = idHouse;
            daFilterHouse.SelectCommand.Connection.Open();
            daFilterHouse.Fill(table);
        }

        public void HouseWithoutBillFilterSelect(MDataSet.HouseWithoutBillFilterDataTable dt)
        {
            daHouseWithoutBillFilter.Fill(dt);
        }

        public string GetHouseAddress(int idHouse)
        {
            return ExecuteScalarQueryCommand("select House_Address from dbo.vhouse_address_all where id_house = " + idHouse.ToString()).ToString();
        }

        public void ServiceSelect(MDataSet.ServiceDataTable table)
        {
            daServiceList.Fill(table);
        }

        public void BillKindSelect(MDataSet.Bill_KindDataTable table)
        {
            daBillKind.Fill(table);
        }

        public int GetWorkPeriod()
        {
            return (int)ExecuteScalarQueryCommand("select dbo.Work_Period()");
        }

        public string GetPeriodName(int period)
        {
            return
                ExecuteScalarQueryCommand(
                    "select (Month_Name + ' ' + convert(varchar,[Year])) Period from period where period="
                    + period).ToString();
        }

        public string GetPeriodMY(int period)
        {
            return
                ExecuteScalarQueryCommand(
                    "select right('0000000000'+convert(varchar,[month]),2)+substring(convert(varchar,[year]),3,2) as Period from period where period="
                    + period).ToString();
        }

        public int GetPeriodYear(int period)
        {
            string val = ExecuteScalarQueryCommand(
                    "select [Year] from period where period="
                    + period).ToString();
            return Convert.ToInt32(val);
        }

        public int GetPeriodMonth(int period)
        {
            string val = ExecuteScalarQueryCommand(
                    "select [Month] from period where period="
                    + period).ToString();
            return Convert.ToInt32(val);
        }

        /// <summary>
        /// Добавить удалить запись из таблицы Reports.Client_Detail_House.
        /// Добавление записи означает что по данному дому печатается квитанция.
        /// </summary>
        /// <param name="idClientDetail">idClientDetail из таблицы Reports.Client_Detail (код фильтра для УК)</param>
        /// <param name="idHouse">ИД дома</param>
        /// <param name="checking">1-печатается квитанция, 0-не печатается квитанция</param>
        /// <param name="isDebt">0-печатается текущая квитанция,1-печатается долговая квитанция</param>
        public void ClientDetailHouseUpdate(int idClientDetail, int idHouse, int checking, int isDebt)
        {
            ExecuteNonQueryCommand("exec [Reports].[ClientDetailHouseLinked_Check] 	@ID_Client_Detail = " + idClientDetail.ToString() +
                                          ", @ID_House = " + idHouse.ToString() +
                                          ", @Cheking = " + checking.ToString() +
                                          ", @IsDebt = " + isDebt.ToString());
        }

        public void ExecuteNonQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.ExecuteNonQuery();
        }

        public object ExecuteScalarQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            object res = cmdAnyCommand.ExecuteScalar();
            return res;
        }

        private void FillDataTable(string commandSelect, DataTable table)
        {
            SqlDataAdapter da = new SqlDataAdapter(commandSelect, sqlMainConnection);
            da.Fill(table);
        }

        public void ClaimStatusSelect(MDataSet.PaperStatusDataTable table)
        {
            this.FillDataTable("exec [dbo].[Claim_Status_Select]", table);
        }

        public void FillCaseTable(MDataSet.LawsuitCaseDataTable table, int idAccount)
        {
            FillDataTable($"select ID_Lawsuit, case_number from [dbo].[Lawsuit] where ID_Account={idAccount}", table);
        }

        public void FillCourtTable(MDataSet.CourtDataTable table)
        {
            FillDataTable("select ID_Court, Name from [dbo].[Court]", table);
        }

        public void FillLawsuitStadyTable(MDataSet.LawsuitStadyDataTable table)
        {
            FillDataTable("select * from [dbo].[lawsuit_stady]", table);
        }

        public void FillLawsuitInnerIndexTable(MDataSet.LawsuitInnerIndexDataTable table)
        {
            FillDataTable("select * from [dbo].[lawsuit_inner_index]", table);
        }

        public void FillFullNameTable(MDataSet.FullNameDataTable table, int idAccount, int period)
        {
            FillDataTable($"select Owner_Name from [dbo].[VAccount_Owner_All] where ID_Account={idAccount} and Period={period}", table);
        }

        public void FillLawsuitTable(MDataSet.LawsuitDataTable table, int idLawsuit)
        {
            FillDataTable($@"select FIO, 
                                    ID_Derive_Company, 
                                    Date_Debt_Begin, 
                                    Date_Debt_End, 
                                    Date_Proceedings_Begin,
                                    Date_Proceedings_End,
                                    ID_Court,
                                    Sum,
                                    Sum_Fine,
                                    Legal_Costs,
                                    id_stady,
                                    id_inner_index,
                                    ratio,
                                    list_number,
                                    contract_number,
                                    provider_group from [dbo].[Lawsuit] where ID_Lawsuit={idLawsuit}", table);
        }

        public void FillDebtTable(MDataSet.DebtDataTable table, 
                                  int idAccount,
                                  int beginPeriod,
                                  int endPeriod,
                                  int idDeriveCompany,
                                  string ratio,
                                  string providers,
                                  int beginDay,
                                  int endDay,
                                  int x)
        {
            FillDataTable($@"EXEC [User_Reports].[lawsuit_find_dolg]
                                    {beginPeriod},
                                    {endPeriod},
                                    {idAccount},
                                    {idDeriveCompany},
                                    {checkStringForNull(providers)},
                                    {checkStringForNull(ratio)},
                                    {beginDay},
                                    {endDay},
                                    {x};", table);
        }

        public void FillLegalCostsTable(MDataSet.LegalCostsDataTable table,
                          decimal money,
                          int x)
        {
            FillDataTable($@"EXEC [User_Reports].[lawsuit_legal_costs]
                                    {convertDecimal(money)},
                                    {x};", table);
        }

        public void FillCompanyTable(MDataSet.CompanyDataTable table,
                              int beginPeriod,
                              int endPeriod,
                              int idAccount)
        {
            const string idCompany = "NULL";
            const int x = 1;

            FillDataTable($@"EXEC [User_Reports].[lawsuit_company_provider]
                                    {beginPeriod},
                                    {endPeriod},
                                    {idAccount},
                                    {idCompany},
                                    {x};", table);
        }

        public void FillProviderTable(MDataSet.ProviderDataTable table,
                                      int beginPeriod,
                                      int endPeriod,
                                      int idAccount,
                                      int idCompany)
        {
            const int x = 2;

            FillDataTable($@"EXEC [User_Reports].[lawsuit_company_provider]
                                    {beginPeriod},
                                    {endPeriod},
                                    {idAccount},
                                    {idCompany},
                                    {x};", table);
        }

        public void FillLawsuitSearchTable(MDataSet.LawsuitSearchDataTable table)
        {
            FillDataTable($@"select ID_Lawsuit,
                                    [dbo].[Lawsuit].ID_Account,
                                    account,
                                    [dbo].[VAccount_Address_All].Address,
                                    case_number,
                                    list_number, 
                                    [dbo].[lawsuit_stady].stady as stady,
                                    FIO, 
                                    ratio, 
                                    [dbo].[VDerive_Company].Name as company,
                                    Date_Debt_Begin, 
                                    Date_Debt_End, 
                                    Sum, 
                                    Sum_Fine, 
                                    Legal_Costs, 
                                    Date_Proceedings_Begin, 
                                    Date_Proceedings_End, 
                                    [dbo].[court].Name as court
                             from [dbo].[Lawsuit]
                             left join [dbo].[lawsuit_stady] on 
                                    [dbo].[Lawsuit].id_stady = [dbo].[lawsuit_stady].id_stady
                             left join [dbo].[VDerive_Company] on
                                    [dbo].[Lawsuit].ID_Derive_Company = [dbo].[VDerive_Company].ID_Company
							 left join [dbo].[VAccount_Address_All] on
									[dbo].[Lawsuit].ID_Account = [dbo].[VAccount_Address_All].ID_Account
                             left join [dbo].[court] on
                                    [dbo].[Lawsuit].ID_Court = [dbo].[court].ID_Court;", table);
        }

        public void FillLawsuitDolgDetail(MDataSet.lawsuitDolgDetailDataTable table,
                                          int beginPeriod,
                                          int endPeriod,
                                          int idAccount,
                                          int idCompany,
                                          string providers)
        {
            FillDataTable($@"EXEC [User_Reports].[lawsuit_dolg_detail]
                                    {beginPeriod},
                                    {endPeriod},
                                    {idAccount},
                                    {idCompany},
                                    {checkStringForNull(providers)};", table);
        }

        string checkDateForNull(object date)
        {
            return date == null ? "NULL" : $"'{((DateTime)date).ToString().Replace('.', '/')}'";
        }

        string checkDateForNull(string date)
        {
            return date == "  .  ." ? "NULL" : $"'{date.ToString().Replace('.', '/')}'";
        }

        string convertDecimal(decimal num)
        {
            return num.ToString().Replace(',', '.');
        }

        string checkStringForNull(string str)
        {
            return str == "" ? "NULL" : $"'{str}'";
        }

        public void LawsuitAdd(int idAccount, int idDeriveCompany, string datedebtBegin, string dateDebtEnd,
                   object dateProcBegin, object dateProcEnd, int idCourt,
                   decimal sum, decimal sumFine, decimal legalCost, string comm, string caseNumber, string fullName,
                   int idStady, string innerIndex, string ratio, string listNumber, string providers)
        {
            string dateNow = DateTime.Now.ToString().Replace('.', '/');

            string query =
                ($@"INSERT INTO [dbo].[Lawsuit]
                    VALUES ({idAccount}, {idDeriveCompany}, 
                            '{dateNow}', 
                            '{dateNow}', 
                            '{dateNow}', 
                            {checkDateForNull(datedebtBegin)}, {checkDateForNull(dateDebtEnd)},
                            {checkDateForNull(dateProcBegin)}, {checkDateForNull(dateProcEnd)},
                            {idCourt}, {1}, {convertDecimal(sum)}, {convertDecimal(sumFine)}, 
                            {convertDecimal(legalCost)}, '', {GetCurrentUserId()},
                            {GetCurrentUserId()}, {0}, NULL, NULL, NULL, {checkStringForNull(comm)}, 
                            {checkStringForNull(caseNumber)}, {checkStringForNull(fullName)}, 
                            {idStady}, {innerIndex}, NULL, NULL, NULL,
                            {checkStringForNull(ratio)}, {checkStringForNull(listNumber)}, {checkStringForNull(providers)});");

            ExecuteNonQueryCommand(query);
        }

        public void LawsuitEdit(int idLawsuit, int idDeriveCompany, string datedebtBegin,
                                string dateDebtEnd, object dateProcBegin, object dateProcEnd, int idCourt,
                                decimal sum, decimal sumFine, decimal legalCost, string fullName,
                                int idStady, string innerIndex, string comm, string caseNumber, string ratio, string listNumber, string providers)
        {
            string dateNow = DateTime.Now.ToString().Replace('.', '/');

            string query = 
                $@"UPDATE [dbo].[Lawsuit]
                   SET Date_Change = '{dateNow}',
                       FIO = {checkStringForNull(fullName)},
                       ID_Derive_Company = {idDeriveCompany},
                       Date_Debt_Begin = {checkDateForNull(datedebtBegin)},
                       Date_Debt_End = {checkDateForNull(dateDebtEnd)},
                       Date_Proceedings_Begin = {checkDateForNull(dateProcBegin)},
                       Date_Proceedings_End = {checkDateForNull(dateProcEnd)},
                       ID_Court = {idCourt},
                       Sum = {convertDecimal(sum)},
                       Sum_Fine = {convertDecimal(sumFine)},
                       Legal_Costs = {convertDecimal(legalCost)},
                       id_stady = {idStady},
                       id_inner_index = {innerIndex},
                       contract_number = {checkStringForNull(comm)},
                       case_number = {checkStringForNull(caseNumber)},
                       ratio = {checkStringForNull(ratio)},
                       list_number = {checkStringForNull(listNumber)},
                       provider_group = {checkStringForNull(providers)}
                   WHERE ID_Lawsuit = {idLawsuit};";

            ExecuteNonQueryCommand(query);
        }

        public void LawsuitDelete(int idLawsuit)
        {
            string query = $@"DELETE FROM [dbo].[Lawsuit] WHERE ID_Lawsuit = {idLawsuit};";

            ExecuteNonQueryCommand(query);
        }

        public DataTable ClaimCalcDebt(int idAccount, int period, int idDeriveCompany, int idProviderGroup, decimal sumTariffPrevPeriodExclude, decimal sumTariffCurrPeriodInclude, string idAccountTable, string deriveCompanyList, string balanceTable)
        {
            string cmd1 = "if object_id('tempdb..#" + balanceTable + "') is not null \n" +
                         "drop table [#" + balanceTable + "] \n" +
                         "Create Table [#" + balanceTable + "] (ID_Account int,ID_Service int,ID_Provider int,Debt money,Debt_Fine money,primary key(ID_Account,ID_Service,ID_Provider))";
            string cmd2 =
                string.Format(
                    "exec [dbo].[Claim_Debt_Count2] @ID_Account={0}, @Period={1}, @ID_Derive_Company={2}, @ID_Provider_Group={3}, @Sum_Tariff_Prev_Period_Exclude={4}, @Sum_Tariff_Curr_Period_Include={5}, @ID_Account_Table={6}, @ID_Derive_Company_List={7}, @BalanceTable='{8}'",
                    idAccount, period, idDeriveCompany, (idProviderGroup == -1 ? "NULL" : idProviderGroup.ToString()), sumTariffPrevPeriodExclude, sumTariffCurrPeriodInclude, idAccountTable, deriveCompanyList, balanceTable);
            
            DataTable t = new DataTable();
            this.FillDataTable(cmd1 + "\n" + cmd2, t);

            return t;
        }

        public void ClaimAddEdit(int idAccount, int idDeriveCompany, int idProviderGroup, decimal sum, decimal sumFine, DateTime datePlanRepayment,
            int idPaperStatus, string note, DateTime dateClaim, DateTime? dateRepayment, int debtPeriodStart, bool includeWorkSumTariff)
        {
            cmdClaimAddEdit.Parameters["@ID_Account"].Value = idAccount;
            cmdClaimAddEdit.Parameters["@ID_Derive_Company"].Value = idDeriveCompany;
            cmdClaimAddEdit.Parameters["@ID_Provider_Group"].Value = (idProviderGroup == -1 ? (object)DBNull.Value : idProviderGroup);
            cmdClaimAddEdit.Parameters["@Sum"].Value = sum;
            cmdClaimAddEdit.Parameters["@Sum_Fine"].Value = sumFine;
            cmdClaimAddEdit.Parameters["@Date_Plan_Repayment"].Value = datePlanRepayment;
            cmdClaimAddEdit.Parameters["@ID_Paper_Status"].Value = idPaperStatus;
            cmdClaimAddEdit.Parameters["@Note"].Value = note;
            cmdClaimAddEdit.Parameters["@Date_Claim"].Value = dateClaim;
            cmdClaimAddEdit.Parameters["@Date_Repayment"].Value = dateRepayment;
            cmdClaimAddEdit.Parameters["@Anul_Reason"].Value = string.Empty;
            cmdClaimAddEdit.Parameters["@BalanceTable"].Value = DBNull.Value;
            cmdClaimAddEdit.Parameters["@Debt_Period_Start"].Value = debtPeriodStart;
            cmdClaimAddEdit.Parameters["@Include_Work_Sum_Tariff"].Value = includeWorkSumTariff;
            cmdClaimAddEdit.ExecuteNonQuery();
        }

        public void LawsuitAddEdit(int idAccount, int idDeriveCompany, int idProviderGroup, decimal sum, decimal sumFine, DateTime datePlanRepayment,
            int idPaperStatus, string note, DateTime dateClaim, DateTime? dateRepayment, int debtPeriodStart, bool includeWorkSumTariff)
        {
            cmdClaimAddEdit.Parameters["@ID_Account"].Value = idAccount;
            cmdClaimAddEdit.Parameters["@ID_Derive_Company"].Value = idDeriveCompany;
            cmdClaimAddEdit.Parameters["@ID_Provider_Group"].Value = (idProviderGroup == -1 ? (object)DBNull.Value : idProviderGroup);
            cmdClaimAddEdit.Parameters["@Sum"].Value = sum;
            cmdClaimAddEdit.Parameters["@Sum_Fine"].Value = sumFine;
            cmdClaimAddEdit.Parameters["@Date_Plan_Repayment"].Value = datePlanRepayment;
            cmdClaimAddEdit.Parameters["@ID_Paper_Status"].Value = idPaperStatus;
            cmdClaimAddEdit.Parameters["@Note"].Value = note;
            cmdClaimAddEdit.Parameters["@Date_Claim"].Value = dateClaim;
            cmdClaimAddEdit.Parameters["@Date_Repayment"].Value = dateRepayment;
            cmdClaimAddEdit.Parameters["@Anul_Reason"].Value = string.Empty;
            cmdClaimAddEdit.Parameters["@BalanceTable"].Value = DBNull.Value;
            cmdClaimAddEdit.Parameters["@Debt_Period_Start"].Value = debtPeriodStart;
            cmdClaimAddEdit.Parameters["@Include_Work_Sum_Tariff"].Value = includeWorkSumTariff;
            cmdClaimAddEdit.ExecuteNonQuery();
        }

        public int ClaimCanAdd(int idDeriveCompany, int idProviderGroup, int idAccount)
        {
            cmdCanAddClaim.Parameters["@ID_Derive_Company"].Value = idDeriveCompany;
            cmdCanAddClaim.Parameters["@ID_Account"].Value = idAccount;
            cmdCanAddClaim.ExecuteScalar();
            return Convert.ToInt32(cmdCanAddClaim.Parameters["@Answer"].Value);
        }

        public object LoadEllisReportTemplate(string moduleName)
        {
            return ExecuteScalarQueryCommand("select convert(varbinary(max),[File]) from dbo.Module where name like '%" + moduleName + "'");
        }

        public object GetSystemParameter(string parameter, bool isFloatParameter)
        {
            string column = isFloatParameter ? "Float_Value" : "String_Value";

            string cmd = string.Format("select {0} from dbo.system_parameter where parameter like '%{1}%'", column, parameter);

            return this.ExecuteScalarQueryCommand(cmd);
        }

        public object LoadBillStdTemplate()
        {
            return ExecuteScalarQueryCommand("select convert(varbinary(max),[File]) from dbo.Module where name like '%_BillStd_Full%'");
        }

        public int GetIdBillKind(int idBillFilter, int isDebt)
        { 
            string sql = "select bk.ID_Bill_Kind " + 
                         "from reports.bill_kind bk " +
                         "where is_debt=" + isDebt.ToString() + " and filter_codes=(select convert(varchar,code) from dbo.bill_filter where id_bill_filter=" + idBillFilter.ToString() + ")";
            return int.Parse(ExecuteScalarQueryCommand(sql).ToString());
        }

        public string GetAccountAddress(int idAccount)
        {
            string sql = "select ('(' + convert(varchar,id_account)+') ' + Address) info from dbo.vaccount_address_all where id_account=" + idAccount.ToString();
            return ExecuteScalarQueryCommand(sql).ToString();
        }

        public string GetAccount(int idAccount)
        {
            string sql = "select account from dbo.account where id_account=" + idAccount.ToString();
            return ExecuteScalarQueryCommand(sql).ToString();
        }

        public string GetCompanyName(int idCompany)
        {
            string sql = "select Name from dbo.Company where id_company=" + idCompany.ToString();
            return ExecuteScalarQueryCommand(sql).ToString();
        }

        public DataTable HouseBillFilterSelect(int idClientDetail, int period)
        {
            DataTable dt = new DataTable();
            daBillHouseList.SelectCommand.Parameters["@id_client_detail"].Value = idClientDetail;
            daBillHouseList.SelectCommand.Parameters["@period"].Value = period;
            daBillHouseList.Fill(dt);
            return dt;
        }


        public void PrepareBillReportAccounts(string accounts)
        {
            string sql = "IF OBJECT_ID('TempDB..#__Account') IS NOT NULL DROP TABLE #__Account " + Environment.NewLine +
                         "select id " + Environment.NewLine +
                         "into #__Account " + Environment.NewLine +
                         "from dbo.list_to_table('" + accounts + "',default)";
            ExecuteNonQueryCommand(sql);
        }
        
        public void PrepareBillReportAccounts(int idClientDetail, int period)
        {
            string sql = "IF OBJECT_ID('TempDB..#__Account') IS NOT NULL DROP TABLE #__Account " + Environment.NewLine +
                         "select aaa.id_account as ID " +
                         "into #__Account " +
                         "from reports.client_detail cd inner join reports.vclient_detail_house cdh on cd.id_client_detail=cdh.id_client_detail " +
                                                       "inner join dbo.vaccount_address_all aaa on cdh.id_house=aaa.id_house " +
                         "where cdh.period=" + period.ToString() + " and cd.id_client_detail=" + idClientDetail.ToString();
            ExecuteNonQueryCommand(sql);
        }

        public void PrepareBillReportAccounts(int idClientDetail, int period, string houses)
        {
            string sql = "IF OBJECT_ID('TempDB..#__Account') IS NOT NULL DROP TABLE #__Account " + Environment.NewLine +
                         "select aaa.id_account as ID " +
                         "into #__Account " +
                         "from reports.client_detail cd inner join reports.vclient_detail_house cdh on cd.id_client_detail=cdh.id_client_detail " +
                                                       "inner join dbo.vaccount_address_all aaa on cdh.id_house=aaa.id_house " +
                         "where cdh.period=" + period.ToString() + " and cd.id_client_detail=" + idClientDetail.ToString() +
                                " and aaa.id_house in (" + houses + ")";
            ExecuteNonQueryCommand(sql);
        }

        public void PrepareBillReportData(long idPrint, int idBillFilter, int period, bool printZeroParam, bool printEmptyFlat, int idCompany, int isDebt)
        {
            cmdBillPrepareReportData.Parameters["@ID_Print"].Value = idPrint;
            cmdBillPrepareReportData.Parameters["@ID_Bill_Kind"].Value = GetIdBillKind(idBillFilter, isDebt);
            cmdBillPrepareReportData.Parameters["@Period"].Value = period;
            cmdBillPrepareReportData.Parameters["@Print_Zero_Param"].Value = printZeroParam;
            cmdBillPrepareReportData.Parameters["@Print_Empty_Flat"].Value = printEmptyFlat;
            cmdBillPrepareReportData.Parameters["@ID_Company"].Value = (idCompany >= 0 ? (object)idCompany : (object)DBNull.Value);
            cmdBillPrepareReportData.Parameters["@SavePrintHistory"].Value = 0;
            cmdBillPrepareReportData.Parameters["@PrintedBackSide"].Value = 0;
            cmdBillPrepareReportData.Parameters["@PrintBillsForSubscribers"].Value = 1;
            cmdBillPrepareReportData.ExecuteNonQuery();
        }

        public void SberReestrLog(int idDelivery, DateTime sendDate, int sendBy)
        {
            cmdClientDetailDeliveryLastSend.Parameters["@ID_Delivery"].Value = idDelivery;
            cmdClientDetailDeliveryLastSend.Parameters["@Last_Send"].Value = sendDate;
            cmdClientDetailDeliveryLastSend.Parameters["@Last_Send_By"].Value = sendBy;
            cmdClientDetailDeliveryLastSend.ExecuteNonQuery();
        }

        public void CreateClientDetail(int idClientDetail,
	                                   int idClient,
	                                   int idDeriveCompany,
	                                   int idBillFilter,
	                                   int beginPeriod,
	                                   int endPeriod,
	                                   float advance,
	                                   int roundTo,
	                                   string prefix,
	                                   string showMeterForServices,
	                                   int idFilterType,
	                                   string comment,
	                                   string billName)
        {
            cmdCreateClientDetail.Parameters["@ID_Client_Detail"].Value = idClientDetail;
            cmdCreateClientDetail.Parameters["@ID_Client"].Value = idClient;
            cmdCreateClientDetail.Parameters["@ID_Derive_Company"].Value = idDeriveCompany;
            cmdCreateClientDetail.Parameters["@ID_Bill_Filter"].Value = idBillFilter;
            cmdCreateClientDetail.Parameters["@Begin_Period"].Value = beginPeriod;
            cmdCreateClientDetail.Parameters["@End_Period"].Value = endPeriod;
            cmdCreateClientDetail.Parameters["@Advance"].Value = advance;
            cmdCreateClientDetail.Parameters["@RoundTo"].Value = roundTo;
            cmdCreateClientDetail.Parameters["@Prefix"].Value = prefix;
            cmdCreateClientDetail.Parameters["@Show_Meter_For_Services"].Value = showMeterForServices;
            cmdCreateClientDetail.Parameters["@ID_Filter_Type"].Value = idFilterType;
            cmdCreateClientDetail.Parameters["@Comment"].Value = comment;
            cmdCreateClientDetail.Parameters["@Bill_Name"].Value = billName;
            cmdCreateClientDetail.ExecuteNonQuery();
        }

        public string ClientDetailRecordInfo(int idClientDetail)
        {
            return ExecuteScalarQueryCommand("[Reports].[ClientDetail_RecordInfo] " + idClientDetail.ToString()).ToString();
        }

        public void AgreementSelect(MDataSet.AgreementDataTable table, DateTime beginDate, DateTime endDate)
        {
            daAgreement.SelectCommand.Parameters["@begin_date"].Value = beginDate;
            daAgreement.SelectCommand.Parameters["@end_date"].Value = endDate;
            daAgreement.Fill(table);
        }

        public void ProviderGroupSelect(MDataSet.ProviderGroupDataTable table)
        {
            daProviderGroup.SelectCommand.CommandText =
                "SELECT ID_Provider_Group, Provider_Group, ID_Company, Code_Group FROM Provider_Group";
            daProviderGroup.Fill(table);
        }

        public void ProviderGroupSelect(MDataSet.ProviderGroupDataTable table, string whereClause)
        {
            daProviderGroup.SelectCommand.CommandText = "SELECT ID_Provider_Group, Provider_Group, ID_Company, Code_Group FROM Provider_Group " + Environment.NewLine + whereClause;
            daProviderGroup.Fill(table);
        }

        public void BillFilterServiceProviderUpdate(int idProviderGroupFilter, int idBillFilter, int idService, string condition)
        {
            cmdBillFilterProviderUpdate.Parameters["@ID_Provider_Group_Filter"].Value = idProviderGroupFilter;
            cmdBillFilterProviderUpdate.Parameters["@ID_Bill_Filter"].Value = idBillFilter;
            cmdBillFilterProviderUpdate.Parameters["@ID_Service"].Value = idService;
            cmdBillFilterProviderUpdate.Parameters["@Condition"].Value = condition;
            cmdBillFilterProviderUpdate.ExecuteNonQuery();
        }

        public void ProviderGroupFilterSelect(MDataSet.ProviderGroupFilterDataTable table)
        {
            daProviderGroupFilter.Fill(table);
        }

        public void FillHouseTableForSaldoFilter(MDataSet.SaldoHouseFilterDataTable table, int period, string clients)
        {
            daRSaldoHouseFilter.SelectCommand.Parameters["@period"].Value = period;
            daRSaldoHouseFilter.SelectCommand.Parameters["@Clients"].Value = clients;
            daRSaldoHouseFilter.Fill(table);
        }

        public void AgreementUpdate(MDataSet.AgreementDataTable table)
        {
            daAgreement.Update(table);
        }

        public void ClientDetailAgreementSelect(MDataSet.VClient_Detail_AgreementDataTable table, int idClientDetail)
        {
            daClientDetailAgreement.SelectCommand.Connection.Close();
            daClientDetailAgreement.SelectCommand.Connection.Open();
            daClientDetailAgreement.SelectCommand.Parameters["@ID_Client_Detail"].Value = idClientDetail;
            daClientDetailAgreement.Fill(table);
        }

        public void ClientDetailAgreementUpdate(MDataSet.VClient_Detail_AgreementDataTable table)
        {
            daClientDetailAgreement.Update(table);
        }

        public void ClearPrintDataTables(long idPrint)
        {
            ExecuteNonQueryCommand("execute [Reports].[Clear_Print_Data] " + idPrint.ToString());
        }

        public void ClientDetailAccountSelect(MDataSet.ClientDetailDataTable table, int idAccount, int period)
        {
            daClientDetailAccount.SelectCommand.Connection.Close();
            daClientDetailAccount.SelectCommand.Parameters["@id_account"].Value = idAccount;
            daClientDetailAccount.SelectCommand.Parameters["@period"].Value = period;
            daClientDetailAccount.SelectCommand.Connection.Open();
            daClientDetailAccount.Fill(table);
        }

        public void ClientDetailDeliverySettingsSelect(MDataSet.ClientDetailDeliverySettingsDataTable table, int period, bool sendToday)
        {
            daClientDetailDeliverySettings.SelectCommand.Parameters["@Period"].Value = period;
            daClientDetailDeliverySettings.SelectCommand.Parameters["@Today"].Value = sendToday;
            daClientDetailDeliverySettings.Fill(table);
        }

        public void ClientDetailDeliverySettingsUpdate(MDataSet.ClientDetailDeliverySettingsDataTable table)
        {
         //   daClientDetailDeliverySettings.UpdateCommand.Connection.Close();
            daClientDetailDeliverySettings.Update(table);
        }

        public void AgreementTypeSelect(MDataSet.Agreement_TypeDataTable table)
        {
            daAgreementType.Fill(table);
        }

        public void ClientSelect(MDataSet.ClientDataTable table, int period, bool showOnlyPrinted)
        {
            daClients.SelectCommand.Connection.Close();
            daClients.SelectCommand.Parameters["@period"].Value = period;
            daClients.SelectCommand.Parameters["@printed"].Value = showOnlyPrinted;
            daClients.SelectCommand.Connection.Open();
            daClients.Fill(table);
        }

        public void DebtAccountBalanceSelect(MDataSet.DebtAccountBalanceDataTable table, int idAccount, int period, int idDeriveCompany, int idBillFilter)
        {
            daDebtBillSaldo.SelectCommand.Connection.Close();
            daDebtBillSaldo.SelectCommand.Parameters["@id_account"].Value = idAccount;
            daDebtBillSaldo.SelectCommand.Parameters["@period"].Value = period;
            daDebtBillSaldo.SelectCommand.Parameters["@id_derive_company"].Value = idDeriveCompany;
            daDebtBillSaldo.SelectCommand.Parameters["@id_bill_filter"].Value = idBillFilter;
            daDebtBillSaldo.SelectCommand.Connection.Open();
            daDebtBillSaldo.Fill(table);
        }

        public void DebtBillClientDetailSelect(MDataSet.DebtClientDetailDataTable table, int idDeriveCompany)
        {
            daDebtBillClientDetail.SelectCommand.Connection.Close();
            daDebtBillClientDetail.SelectCommand.Parameters["@id_derive_company"].Value = idDeriveCompany;
            daDebtBillClientDetail.SelectCommand.Connection.Open();
            daDebtBillClientDetail.Fill(table);
        }

        public void ClaimDeriveCompany(MDataSet.DeriveCompanyDataTable table, int period, int idAccount)
        {
            daClaimDeriveCompany.SelectCommand.Parameters["@id_account"].Value = idAccount;
            daClaimDeriveCompany.SelectCommand.Parameters["@period"].Value = period;
            daClaimDeriveCompany.Fill(table);
        }

        public void ClaimProviderGroup(MDataSet.ProviderGroupDataTable table, int period, int idAccount)
        {
            daClaimProviderGroup.SelectCommand.Parameters["@id_account"].Value = idAccount;
            daClaimProviderGroup.SelectCommand.Parameters["@period"].Value = period;
            daClaimProviderGroup.Fill(table);
        }

        public string DebtBillPrintCheck(int idAccount, int idClient, int idDeriveCompany, int idBillFilter)
        {
            return this.ExecuteScalarQueryCommand(
                string.Format(
                    "[Reports].[Debt_Bill_Client_Detail_Check] @ID_Account ={0}, @ID_Client = {1}, @ID_Derive_Company = {2}, @ID_Bill_Filter = {3}",
                    idAccount,
                    idClient,
                    idDeriveCompany,
                    idBillFilter)).ToString();
        }

        public void ClientDetailDeliveryEmailSelect(MDataSet.ClientDetailDeliveryEmailDataTable table)
        {
            daClientDetailDeliveryEmail.Fill(table);
        }

        public void ClientDetailDeliveryEmailUpdate(MDataSet.ClientDetailDeliveryEmailDataTable table)
        {
            daClientDetailDeliveryEmail.Update(table);
        }

        public DataTable RSaldo(int period, string clients, int idDeriveCompany, int idBillFilter, string housefilterString, bool isDebt, bool onlyUninhabitedAccounts)
        {
            DataTable t = new DataTable();
            daRSaldo.SelectCommand.Parameters["@Period"].Value = period;
            daRSaldo.SelectCommand.Parameters["@Clients"].Value = clients;
            daRSaldo.SelectCommand.Parameters["@DC"].Value = idDeriveCompany;
            daRSaldo.SelectCommand.Parameters["@ID_Kvit"].Value = idBillFilter;
            daRSaldo.SelectCommand.Parameters["@house_filter"].Value = housefilterString;
            daRSaldo.SelectCommand.Parameters["@isdebt"].Value = isDebt;
            daRSaldo.SelectCommand.Parameters["@onlyUninhabitedAccounts"].Value = onlyUninhabitedAccounts;      
            daRSaldo.Fill(t);
            return t;
        }

        // Загрузка данных в таблицу для Сбера
        public DataTable RSaldoSber(int period, string clients, int idDeriveCompany, int idBillFilter, string housefilterString, bool isDebt, bool onlyUninhabitedAccounts)
        {
            DataTable t = new DataTable();
            daRSaldoSber.SelectCommand.Parameters["@Period"].Value = period;
            daRSaldoSber.SelectCommand.Parameters["@Clients"].Value = clients;
            daRSaldoSber.SelectCommand.Parameters["@DC"].Value = idDeriveCompany;
            daRSaldoSber.SelectCommand.Parameters["@ID_Kvit"].Value = idBillFilter;
            daRSaldoSber.SelectCommand.Parameters["@house_filter"].Value = housefilterString;
            daRSaldoSber.SelectCommand.Parameters["@isdebt"].Value = isDebt;
            daRSaldoSber.SelectCommand.Parameters["@onlyUninhabitedAccounts"].Value = onlyUninhabitedAccounts;
            daRSaldoSber.Fill(t);
            return t;
        }

        // Загрузка данных в таблицу для Сбера с показаниями
        public DataTable RSaldoSberMeasure(int period, string clients, int idDeriveCompany, int idBillFilter, string housefilterString, bool isDebt, bool onlyUninhabitedAccounts)
        {
            DataTable t = new DataTable();
            daRSaldoSberMeasure.SelectCommand.Parameters["@Period"].Value = period;
            daRSaldoSberMeasure.SelectCommand.Parameters["@Clients"].Value = clients;
            daRSaldoSberMeasure.SelectCommand.Parameters["@DC"].Value = idDeriveCompany;
            daRSaldoSberMeasure.SelectCommand.Parameters["@ID_Kvit"].Value = idBillFilter;
            daRSaldoSberMeasure.SelectCommand.Parameters["@house_filter"].Value = housefilterString;
            daRSaldoSberMeasure.SelectCommand.Parameters["@isdebt"].Value = isDebt;
            daRSaldoSberMeasure.SelectCommand.Parameters["@onlyUninhabitedAccounts"].Value = onlyUninhabitedAccounts;
            daRSaldoSberMeasure.Fill(t);
            return t;
        }

        public void CompanyServiceNumberSelect(MDataSet.CompanyServiceNumberDataTable table, int idCompany)
        {
            daCompanyServiceNumber.SelectCommand.Parameters["@ID_Company"].Value = idCompany;
            daCompanyServiceNumber.Fill(table);
        }

        public void PrintBillDeliverySelect(MDataSet.PrintDeliveryBillDataTable table, int idAccount)
        {
            daPrintBillDeliveryAccount.SelectCommand.Parameters["@ID_Account"].Value = idAccount;
            daPrintBillDeliveryAccount.Fill(table);
        }

        public void InsertPrintDeliveryInfo(long idPrint, int period, int idAccount, int idClientDetail, string email, string fileName, int success, string error)
        {
            string sql = string.Format(
                "exec [Reports].[Print_Delivery_Bill_Insert] @ID_Print={0}, @Period={1}, @ID_Account={2}, @ID_Client_Detail={3}, @Email='{4}', @File_Name='{5}', @Send_Success={6}, @Error='{7}'",
                idPrint, period, idAccount, idClientDetail, email, fileName, success, error);
            this.ExecuteNonQueryCommand(sql);
        }

        public string GetAccountSubscriptionEmail(int idAccount)
        {
            string sql = string.Format("select top 1 [e-mail] from [town].[VAccount_Property_Email] where id_account={0} order by period desc", idAccount);
            object obj = this.ExecuteScalarQueryCommand(sql);
            return obj == null || obj == DBNull.Value ? string.Empty : obj.ToString();
        }

        public void BulkCopyData(DataTable importTable, string destinationTableName, string dataBaseName = "")
        {
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder(ConnectionString);
            conn.InitialCatalog = string.IsNullOrEmpty(dataBaseName) ? Connection.Database : dataBaseName;

            SqlServerWriter sqlServerWriter = new SqlServerWriter(conn.ConnectionString) { CreateAddressColumnsAsVarcharType = true };
            importTable.TableName = destinationTableName;
            sqlServerWriter.WriteDataTable(importTable, true);
        }

        public string GetDynamicSQL(string name)
        {
            return ExecuteScalarQueryCommand($"select [SQL] from [town].[DynamicSql] where name = '{name}'").ToString();
        }

        public void AddLinkAccountColumns(string tableName)
        {
            string sql = GetDynamicSQL("AddLinkAccountColumns");
            sql = sql.Replace("?TableName", tableName);
            ExecuteNonQueryCommand(sql);
        }

        public void TryBindAccountsByAddress(string tableName, int address_part_number, string street_separator,
            string house_separator, string building_separator, string block_separator, string flat_separator, string room_separator)
        {
            string sql = GetDynamicSQL("BindAccountsByAddress");
            sql = sql.Replace("?TableName", tableName);
            sql = sql.Replace("?address_part_number", address_part_number.ToString());
            sql = sql.Replace("?street_separator", $"'{street_separator}'");
            sql = sql.Replace("?house_separator", $"'{house_separator}'");
            sql = sql.Replace("?building_separator", $"'{building_separator}'");
            sql = sql.Replace("?block_separator", $"'{block_separator}'");
            sql = sql.Replace("?flat_separator", $"'{flat_separator}'");
            sql = sql.Replace("?room_separator", $"'{room_separator}'");
            ExecuteNonQueryCommand(sql);
        }

        public void TryBindAccountsByAccount(string tableName, string sourceColumn, string destColumn)
        {
            string sql = GetDynamicSQL("BindAccountsByAccount");
            sql = sql.Replace("?TableName", tableName);
            sql = sql.Replace("?SourceColumn", sourceColumn);
            sql = sql.Replace("?DestColumn", destColumn);
            ExecuteNonQueryCommand(sql);
        }

        public DataTable SelectDataTable(string tableName, string dataBaseName)
        {
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder(ConnectionString);
            conn.InitialCatalog = string.IsNullOrEmpty(dataBaseName) ? Connection.Database : dataBaseName;

            DataTable dt = new DataTable();

            using (SqlConnection connection = new SqlConnection(conn.ConnectionString))
            {
                SqlDataAdapter da = new SqlDataAdapter($"select * from {tableName}", connection);
                da.Fill(dt);
            }

            return dt;
        }

        public string CheckBindAddressAccounts(string account, string street_separator, string house_separator, string building_separator, 
            string block_separator, string flat_separator, string room_separator)
        {
            cmdCheckBindAddress.Parameters["@account"].Value = account;
            cmdCheckBindAddress.Parameters["@street_separator"].Value = street_separator;
            cmdCheckBindAddress.Parameters["@house_separator"].Value = house_separator;
            cmdCheckBindAddress.Parameters["@building_separator"].Value = building_separator;
            cmdCheckBindAddress.Parameters["@block_separator"].Value = block_separator;
            cmdCheckBindAddress.Parameters["@flat_separator"].Value = flat_separator;
            cmdCheckBindAddress.Parameters["@room_separator"].Value = room_separator;
            return cmdCheckBindAddress.ExecuteScalar().ToString();
        }

        public DataTable SelectDynamicDataImport()
        {
            DataTable dt = new DataTable();
            FillDataTable("select * from town.DynamicDataImport where [SQL] is not null", dt);
            return dt;
        }

        public DataTable GetDynamicParameter(string parameterName)
        {
            DataTable dt = new DataTable();
            FillDataTable($"select * from town.DynamicParameter where [Parameter] = '{parameterName}'", dt);
            return dt;
        }
    }
}
