using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList;
using System.Xml;
using System.IO;

namespace MassBillNew
{
    public partial class AccountFilter : DevExpress.XtraEditors.XtraUserControl
    {
        static string connectionString = Properties.Settings.Default.MurmanskConnectionString;
        
        DataSet dataSet = new DataSet();

        public AccountFilter()
        {
            InitializeComponent();
        }

        public void FillDataSet(DataTable dataTable)
        {

            DataTableReader reader = new DataTableReader(dataTable);

            dataSet.Tables["Adress"].Load(reader);
        }

        private void AccountFilter_Load(object sender, EventArgs e)
        {
            //dataSet.Tables.Add("Adress");
            //dataSet.Tables["Adress"].Columns.Add("id");
            //dataSet.Tables["Adress"].Columns.Add("id_parent");
            //dataSet.Tables["Adress"].Columns.Add("type");
            //dataSet.Tables["Adress"].Columns.Add("name");
            //dataSet.Tables["Adress"].Columns.Add("company");
            //dataSet.Tables["Adress"].Columns.Add("caption");

            //this.listBoxControl1.DataSource = dataSet.Tables["Adress"];
            //this.listBoxControl1.DisplayMember = "caption";
        }

        public void Init(FilterType filterType, bool selectAllEnabled)
        {
            dataSet.Tables.Add("Adress");
            dataSet.Tables["Adress"].Columns.Add("id");
            dataSet.Tables["Adress"].Columns.Add("id_parent");
            dataSet.Tables["Adress"].Columns.Add("type");
            dataSet.Tables["Adress"].Columns.Add("name");
            dataSet.Tables["Adress"].Columns.Add("company");
            dataSet.Tables["Adress"].Columns.Add("caption");

            this.listBoxControl1.DataSource = dataSet.Tables["Adress"];
            this.listBoxControl1.DisplayMember = "caption";

            this.filterType = filterType;
            this.simpleButton3.Visible = selectAllEnabled;

            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "select * from [town].[VFrx_Account_Tree] where id_parent is null order by sort_name_varchar";
            cmd.CommandType = CommandType.Text;
            
            SqlConnection sqlConn = new SqlConnection();

            sqlConn = new SqlConnection();
            sqlConn.ConnectionString = connectionString;

            cmd.Connection = sqlConn;

            sqlConn.Open();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                treeList1.BeginUnboundLoad();
                TreeListNode parentForRootNodes = null;
                TreeListNode rootNode = treeList1.AppendNode(new object[] { reader[0], reader[1], reader[2], reader[3],reader[4],reader[5] }, parentForRootNodes);
                rootNode.StateImageIndex = (int)reader[2];
                TreeListNode Node = treeList1.AppendNode(new object[] { "", "", "", "", "","" }, rootNode);
                treeList1.EndUnboundLoad();
            }
            sqlConn.Close();
        }

        private void treeList1_BeforeExpand(object sender, DevExpress.XtraTreeList.BeforeExpandEventArgs e)
        {
            e.Node.Nodes.Clear();

            string item = e.Node[0].ToString();
            string type = e.Node[2].ToString();
            string company = e.Node[4].ToString();

            SqlCommand cmd = new SqlCommand();


            cmd.CommandText = string.Format("select * from [town].[VFrx_Account_Tree] where id_parent = '{0}' and id_company = '{1}'", item, company);

            if (type == "2" | type == "3")
                cmd.CommandText += " order by sort_name_int";
            else
                cmd.CommandText += " order by sort_name_varchar";

            cmd.CommandType = CommandType.Text;


            SqlConnection sqlConn = new SqlConnection();


            sqlConn = new SqlConnection();
            sqlConn.ConnectionString = connectionString;

            cmd.Connection = sqlConn;

            sqlConn.Open();


            SqlDataReader reader = cmd.ExecuteReader();

            int level = 0;

            while (reader.Read())
            {
                treeList1.BeginUnboundLoad();


                TreeListNode rootNode = treeList1.AppendNode(new object[] { reader[0], reader[1], reader[2], reader[3], reader[4], reader[5] }, e.Node);
                rootNode.StateImageIndex = (int)reader[2];


                TreeListNode Node;




                if (filterType == FilterType.Account)
                {
                    level = 4;
                }

                if (filterType == FilterType.House)
                {
                    level = 2;
                }

                if (e.Node.Level != level)
                    Node = treeList1.AppendNode(new object[] { "", "", "", "", "", "" }, rootNode);

                treeList1.EndUnboundLoad();

            }






            sqlConn.Close();
        }

        private string GetStringByNode(DevExpress.XtraTreeList.Nodes.TreeListNode node)
        {
            string ret = string.Empty;
            for (int i = 0; i < treeList1.Columns.Count; i++)
                ret += node.GetDisplayText(i) + (i < treeList1.Columns.Count - 1 ? "; " : ".");
            return ret;
        }

        private object[] GetRowByNode(DevExpress.XtraTreeList.Nodes.TreeListNode node)
        {
            object[] ret = new object[treeList1.Columns.Count];
            for (int i = 0; i < treeList1.Columns.Count; i++)
                ret[i] = node.GetValue(i);
            return ret;
        }

        private TreeListHitInfo dragStartHitInfo; 
        private void treeList1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.None)
            {
                DevExpress.XtraTreeList.TreeList tl = sender as DevExpress.XtraTreeList.TreeList;
                dragStartHitInfo = tl.CalcHitInfo(e.Location);
            }

        }

        private void listBox1_DragDrop(object sender, DragEventArgs e)
        {
            DevExpress.XtraEditors.ListBoxControl lb = sender as DevExpress.XtraEditors.ListBoxControl;
            DevExpress.XtraTreeList.TreeListMultiSelection nodes = GetDragNodes(e.Data);
            if (nodes != null)
            {
                int ind = lb.IndexFromPoint(lb.PointToClient(new Point(e.X, e.Y)));
                for (int i = 0; i < nodes.Count; i++)
                {
                    //string dragString = GetStringByNode(nodes[i]);

                    if (ind == -1)
                        dataSet.Tables["Adress"].Rows.Add(GetRowByNode(nodes[i]));
                        //lb.Items.Add(dragString);
                    else
                        dataSet.Tables["Adress"].Rows.Add(GetRowByNode(nodes[i]));
                        //lb.Items.Insert(ind, dragString);
                }
            }
        }

        private DevExpress.XtraTreeList.TreeListMultiSelection GetDragNodes(IDataObject data)
        {
            return data.GetData(typeof(DevExpress.XtraTreeList.TreeListMultiSelection)) as DevExpress.XtraTreeList.TreeListMultiSelection;
        }

        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (GetDragNodes(e.Data) != null)
                e.Effect = DragDropEffects.Copy;
        }

        private void treeList1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && dragStartHitInfo != null && dragStartHitInfo.Node != null)
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(dragStartHitInfo.MousePoint.X - dragSize.Width / 2,
                    dragStartHitInfo.MousePoint.Y - dragSize.Height / 2), dragSize);
                if (!dragRect.Contains(e.Location))
                    ((TreeList)sender).DoDragDrop(treeList1.Selection, DragDropEffects.Copy);
            }
        }

        public DataTable CreateDataTable()
        {
            return (DataTable)listBoxControl1.DataSource;
        }

        public string CreateSqlQuery()
        {
            string query = string.Empty;

            switch (this.filterType)
            {
                case FilterType.Account:
                    query = " select aa.id_account";
                    break;
                case FilterType.House:
                    query = " select distinct aa.id_house";
                    break;
            }

            query += " from vhouse_derive_company hdc" +
            " inner join vaccount_address_all aa on aa.id_house = hdc.id_house" +
            " where period = dbo.work_period() ";

            if (dataSet.Tables["Adress"].Rows.Count != 0)
            {
                query += " and (";

                bool first = true;

                foreach (DataRow row in dataSet.Tables["Adress"].Rows)
                {
                    if (Convert.ToInt32(row["type"]) == 0)
                    {
                        if (first) query += " id_company in (";
                        query += row["id"].ToString().Replace("c", "") + ",";
                        first = false;
                    }
                }

                if (!first)
                {
                    query = query.TrimEnd(',');
                    query += ")";
                }

                foreach (DataRow row in dataSet.Tables["Adress"].Rows)
                {
                    if (Convert.ToInt32(row["type"]) == 1)
                    {
                        if (!first) query += " or ";
                        query += " (id_settlement = " + row["id"].ToString().Replace("set", "") + " and id_company = " + row["company"].ToString() + ") ";
                    }

                    if (Convert.ToInt32(row["type"]) == 2)
                    {
                        if (!first) query += " or ";
                        query += " (id_street = " + row["id"].ToString().Replace("str", "") + " and id_company = " + row["company"].ToString() + ") ";
                    }

                    if (Convert.ToInt32(row["type"]) == 3)
                    {
                        if (!first) query += " or ";
                        query += " aa.id_house = " + row["id"].ToString().Replace("h", "");
                    }

                    if (Convert.ToInt32(row["type"]) == 4)
                    {
                        if (!first) query += " or ";
                        query += " id_flat = " + row["id"].ToString().Replace("f", "");
                    }

                    if (Convert.ToInt32(row["type"]) == 5)
                    {
                        if (!first) query += " or ";
                        query += " id_account = " + row["id"].ToString().Replace("a", "");
                    }

                    first = false;
                }

                query = query.TrimEnd(',');
                query += ")";

            }        

            return query;
        }

        public string CreateStringOfItems()
        {

            string items = "";

            SqlConnection conn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 0;
            cmd.Connection = conn;

            foreach (DataRow row in dataSet.Tables["Adress"].Rows)
            {
                switch (this.filterType)
                {
                    case FilterType.Account: 
                        cmd.CommandText = " select aa.id_account";
                        break;
                    case FilterType.House: 
                        cmd.CommandText = " select distinct aa.id_house";
                        break;
                }
                
                
                
                cmd.CommandText += " from vhouse_derive_company hdc" +
                " inner join vaccount_address_all aa on aa.id_house = hdc.id_house" +
                " where period = dbo.work_period() ";


                switch (Convert.ToInt32(row["type"]))
                {
                    case 0:
                        cmd.CommandText += " and id_company =" +  row["id"].ToString().Replace("c","");
                        break;
                    case 1:
                        cmd.CommandText += " and id_settlement =" + row["id"].ToString().Replace("set", "");
                        break;
                    case 2:
                        cmd.CommandText += " and id_street =" + row["id"].ToString().Replace("str", "");
                        break;
                    case 3:
                        cmd.CommandText += " and aa.id_house =" + row["id"].ToString().Replace("h", "");
                        break;
                    case 4:
                        cmd.CommandText += " and id_flat =" + row["id"].ToString().Replace("f", "");
                        break;
                    case 5:
                        cmd.CommandText += " and id_account =" + row["id"].ToString().Replace("a", "");
                        break;
                   
                }
                conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    items += reader[0].ToString() + ',';
                }

                conn.Close();
            }

            items = items.TrimEnd(',');

            return items;

        }

        private FilterType filterType;

        public enum FilterType
        {
            Account,
            House
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < treeList1.Nodes.Count; i++)
            {
                
                        dataSet.Tables["Adress"].Rows.Add(GetRowByNode(treeList1.Nodes[i]));

                
            
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string ext = Path.GetExtension(openFileDialog1.SafeFileName);

                    if (ext == ".txt")
                    {
                        string[] lines = File.ReadAllLines(openFileDialog1.FileName);

                        foreach (string line in lines)
                        {
                                dataSet.Tables["Adress"].Rows.Add(line, null, 3, null, null, "ID="+line);
                        }
                    }
                    else
                    {
                        if (ext == ".afl")
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(openFileDialog1.FileName);
                            foreach (XmlNode item in xmlDoc)
                            {
                                foreach (XmlNode item1 in item.ChildNodes)
                                {
                                    switch (item1.Name)
                                    {
                                        case "Address":
                                            foreach (XmlNode addr in item1.ChildNodes)
                                            {
                                                int TypeID = -1;
                                                switch (addr.Attributes["TypeID"].Value)
                                                {
                                                    case "5":
                                                        TypeID = 0;
                                                        break;
                                                    case "4":
                                                        TypeID = 1;
                                                        break;
                                                    case "3":
                                                        TypeID = 2;
                                                        break;
                                                    case "2":
                                                        TypeID = 3;
                                                        break;
                                                    case "1":
                                                        if (this.filterType == FilterType.Account)//���� ��� ������ ������ - �� �������, �� ������������ ��������
                                                            TypeID = 4;
                                                        else
                                                            return;
                                                        break;
                                                    case "0":
                                                        if (this.filterType == FilterType.Account)//���� ��� ������ ������ - �� �������, �� ������������ �������
                                                            TypeID = 5;
                                                        else
                                                            return;
                                                        break;
                                                }
                                                dataSet.Tables["Adress"].Rows.Add(addr.Attributes["ID"].Value, null, TypeID, null, addr.Attributes["DeriveCompany"].Value, addr.Attributes["Caption"].Value);
                                            };
                                            break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                else
                {
                    return;
                }
            }
            catch { return; }
        }

        private void listBoxControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (dataSet.Tables.Contains("Adress"))
                    if (dataSet.Tables["Adress"].Rows.Count != 0)
                    {

                        listBoxControl1.SelectedIndex = listBoxControl1.IndexFromPoint(e.Location);

                        if (listBoxControl1.SelectedItems.Count == 0)
                            popupMenu2.ShowPopup(Control.MousePosition);
                        else
                            popupMenu1.ShowPopup(Control.MousePosition);

                    }

            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dataSet.Tables["Adress"].Rows.Remove(((DataRowView)listBoxControl1.SelectedItem).Row);
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dataSet.Tables["Adress"].Clear();
        }

        private void listBoxControl1_DoubleClick(object sender, EventArgs e)
        {
            if (dataSet.Tables["Adress"].Rows.Count != 0)
            dataSet.Tables["Adress"].Rows.Remove(((DataRowView)listBoxControl1.SelectedItem).Row);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }
    }
}
