﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MrivcControls.Data;
using System.IO;
using MrivcControls.Helpers;
using DevExpress.XtraPivotGrid;

namespace MrivcControls
{
    public partial class PivotSataticticByServicedAccounts : UserControl
    {
        private bool initialized = false;

        private const string PivotLayoutName = "PivotSataticticByServicedAccountsLayout.xml";

        public PivotSataticticByServicedAccounts()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            cDataSet.Period.Merge(DAccess.Instance.Periods);
            DAccess.DataModule.ClientsSelect(cDataSet.Company);

            //cbClients.SetEditValue("12798, 11842, 11908, 1063");
            DAccess.DataModule.StatisticByServicedAccountsSelect(nDataSet.Statistic_By_Serviced_Accounts, DAccess.Instance.WorkPeriod, cbClients.EditValue.ToString());

            cbPeriod.EditValue = DAccess.Instance.WorkPeriod;

            pivotGridControl2.RefreshData();
            pivotGridControl2.CollapseAll();

            initialized = true;
        }

        public void ReLoadData()
        {
            if (!initialized)
                return;

            this.Cursor = Cursors.WaitCursor;

            cDataSet.PrintPivotData.Clear();
            DAccess.DataModule.StatisticByServicedAccountsSelect(nDataSet.Statistic_By_Serviced_Accounts, (int)cbPeriod.EditValue, cbClients.EditValue.ToString());

            pivotGridControl2.RefreshData();
            pivotGridControl2.CollapseAll();

            this.Cursor = Cursors.Default;
        }

        private void cbPeriod_EditValueChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.ReLoadData();
            this.Cursor = Cursors.Default;
        }

        private void btnUpdateDate_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            DAccess.DataModule.StatisticByServicedAccountsUpdate((int)cbPeriod.EditValue);
            this.Cursor = Cursors.Default;
            ReLoadData();
            MessageBox.Show("Обновление данных завершено.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void SaveLayout()
        {
            if (pivotGridControl2 == null)
                return;

            pivotGridControl2.SaveLayoutToXml(PivotLayoutName);
        }

        public void LoadLayout()
        {
            if (pivotGridControl2 == null)
                return;

            if (File.Exists(PivotLayoutName))
                pivotGridControl2.RestoreLayoutFromXml(PivotLayoutName);
        }

        private void miExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.RestoreDirectory = true;
            sfd.Filter = "xls files (*.xls)|*.xls|xlsx files (*.xlsx)|*.xlsx";
            sfd.AddExtension = true;
            sfd.DefaultExt = "xls";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if (FileHelper.GetFileExt(sfd.FileName).ToLower() == "xls")
                    pivotGridControl2.ExportToXls(sfd.FileName);
                if (FileHelper.GetFileExt(sfd.FileName).ToLower() == "xlsx")
                    pivotGridControl2.ExportToXlsx(sfd.FileName);
            }
        }

        private void miExpandAllGroups_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (menuItem.Tag.ToString().Equals("All", StringComparison.InvariantCultureIgnoreCase))
            {
                pivotGridControl2.ExpandAll();
                return;
            }

            (menuItem.Tag as PivotGridField).ExpandAll();
        }

        private void miCollapseAllGroups_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (menuItem.Tag.ToString().Equals("All", StringComparison.InvariantCultureIgnoreCase))
            {
                pivotGridControl2.CollapseAll();
                return;
            }

            (menuItem.Tag as PivotGridField).CollapseAll();
        }

        private void customEdit1_Properties_EditValueChanged(object sender, EventArgs e)
        {
            if (!initialized)
                return;

            ReLoadData();
        }

        private void pivotGridControl2_FieldValueExpanding(object sender, PivotFieldValueCancelEventArgs e)
        {
        }

        private void pivotGridControl2_EndRefresh(object sender, EventArgs e)
        {
            pivotGridControl2.CollapseAll();
        }

        private void miCopyAccount_Click(object sender, EventArgs e)
        {
            object value = pivotGridControl2.Cells.GetFocusedCellInfo().GetFieldValue(fieldAccount);
            if (value != null)
            {
                Clipboard.SetText(value.ToString());
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            miCopyAccount.Enabled = true;

            object value = pivotGridControl2.Cells.GetFocusedCellInfo().GetFieldValue(fieldAccount);
            if (value == null || value.ToString().Length != 12)
            {
                miCopyAccount.Enabled = false;
            }
        }
    }
}
