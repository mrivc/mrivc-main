﻿using System.Windows.Forms;

namespace MrivcHelpers.Helpers
{
    public class Messages
    {
        static public void Warning(string text, string caption)
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        static public bool Question(string text, string caption)
        {
            if (MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.OK)
                return true;
            else
                return false;
        }
    }
}
