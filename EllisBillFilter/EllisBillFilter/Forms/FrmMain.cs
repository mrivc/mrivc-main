﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using System.Collections;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using FastReport;
using System.Threading;

namespace EllisBillFilter
{
    public partial class FrmMain : Form
    {
        //private string connectionString;
        private GridView billFilterCurrentGridView;
        private bool formShown;

        public FrmMain()
        {
            formShown = false;
            InitializeComponent();

            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;

            this.gridBillFilterCurrent.GridView.Name = "gridBillFilterCurrent";
            tsStatusLabel.Text = "";

            billFilterCurrentGridView = this.gridBillFilterCurrent.GridView;

            houseClientDetailFilter1.GoToActiveClientDetailActiveted += new GoToActiveClientDetailEventHandler(houseClientDetailFilter1_GoToActiveClientDetailActiveted);
            gridBillFilterCurrent.ClientDetailEditing += new ClientDetailEditingEventHandler(gridBillFilterCurrent_ClientDetailEditing);
            gridBillFilterCurrent.ClientDetailPrint += new ClientDetailPrintEventHandler(gridBillFilterCurrent_ClientDetailPrint);
            gridBillFilterCurrent.GridView.ColumnFilterChanged += new EventHandler(GridView_ColumnFilterChanged);
            gridBillFilterCurrent.GridView.EndGrouping += new EventHandler(GridView_EndGrouping);
            billFilterCurrentGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(billFilterCurrentGridView_FocusedRowChanged);
        }

        void gridBillFilterCurrent_ClientDetailPrint(DataRow data)
        {
            long idPrint = DAccess.Instance.GenerateIdPrint();

            FrmBillPrintHouseFilter f = new FrmBillPrintHouseFilter(idPrint, data, DAccess.Instance.WorkPeriod);
            
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                ShowBillReport(idPrint);
        }

        private void ShowBillReport(long idPrint)
        {
            //if (report1.Preview == null)
            //{
                SetStatusText("загрузка отчета...");
                report1.Preview = previewControl1;
                report1.LoadFromString(DAccess.Instance.GetBillStdReport());
                report1.DoublePass = false;
                report1.UseFileCache = false;
            //}

            report1.SetParameterValue("MainConnection", DAccess.DataModule.Connection.ConnectionString);
            report1.SetParameterValue("IDPrint", idPrint);
            report1.SetParameterValue("IS_DEBT", false);

            report1.Show();
            xtraTabControl1.SelectedTabPage = tabPrint;
            DAccess.DataModule.ClearPrintDataTables(idPrint);
        }

        private void fastReportEnvironmentSettings_StartProgress(object sender, EventArgs e)
        {
            SetStatusText("подготовка к выводу...");
        }

        private void fastReportEnvironmentSettings_Progress(object sender, ProgressEventArgs e)
        {
            SetStatusText(e.Message);
        }

        private void fastReportEnvironmentSettings_FinishProgress(object sender, EventArgs e)
        {
            SetStatusText("");
        }

        private void SetStatusText(string text)
        {
            tsStatusLabel.Text = text;
            statusStrip1.Refresh();
        }

        void GridView_EndGrouping(object sender, EventArgs e)
        {
            ReLoadCurrentHouseList();
        }

        void GridView_ColumnFilterChanged(object sender, EventArgs e)
        {
            ReLoadCurrentHouseList();
        }

        void houseClientDetailFilter1_GoToActiveClientDetailActiveted(int idClientDetail)
        {
            xtraTabControl1.SelectedTabPage = tabActiveClientDetail;

            ColumnView View = gridBillFilterCurrent.GridView as ColumnView;
            try
            {
                int rowHandle = 0;

                txtDeriveCompany.Text = "";
                billFilterCurrentGridView.ClearColumnsFilter();

                DevExpress.XtraGrid.Columns.GridColumn col = View.Columns["ID_Client_Detail"];
                rowHandle = View.LocateByValue(rowHandle, col, idClientDetail);
                gridBillFilterCurrent.GridView.FocusedRowHandle = rowHandle;
            }
            finally { 
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //DAccess.DataModule.ConnectionString = connectionString;
            mDataSet.Period.Merge(DAccess.Instance.Periods);
            
            cbPeriod.EditValue = DAccess.Instance.WorkPeriod;
        }

        private void cbPeriod_Properties_EditValueChanged(object sender, EventArgs e)
        {
           // billFilterCurrentGridView.ClearColumnsFilter();
            ReloadData((int)cbPeriod.EditValue);
        }

        void billFilterCurrentGridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ReLoadCurrentHouseList();
        }

        private void ReLoadCurrentHouseList()
        {
            if (!formShown)
                return;

            houseListCurrent.ClearHouseList();
            houseListDebt.ClearHouseList();

            if (billFilterCurrentGridView.FocusedRowHandle < 0 || billFilterCurrentGridView.GetDataRow(billFilterCurrentGridView.FocusedRowHandle) == null ||
                billFilterCurrentGridView.GetDataRow(billFilterCurrentGridView.FocusedRowHandle).RowState != DataRowState.Unchanged)
                return;

            UpdateHouseLists(billFilterCurrentGridView.GetDataRow(billFilterCurrentGridView.FocusedRowHandle));
        }

        void gridBillFilterCurrent_ClientDetailEditing(DataRow data)
        {
            UpdateHouseLists(data);
        }

        private void UpdateHouseLists(DataRow data)
        {
            if (!formShown)
                return;

            switch ((int)data["ID_Filter_Type"])
            { 
                case 1: // текущий код фильтра
                    tabCompanyHouses.PageEnabled = true;
                    tabAllHouses.PageEnabled = false;
                    houseListCurrent.ReLoadHouseListForDeriveCompany((int)data["ID_Client_Detail"], (int)cbPeriod.EditValue, false);
                    break;
                case 2: // долговой код фильтра
                    tabCompanyHouses.PageEnabled = false;
                    tabAllHouses.PageEnabled = true;
                    houseListDebt.ReLoadHouseListForDeriveCompany((int)data["ID_Client_Detail"], (int)cbPeriod.EditValue, true);
                    break;
                case 4: // смешанный код фильтра
                    tabCompanyHouses.PageEnabled = true;
                    tabAllHouses.PageEnabled = true;
                    houseListCurrent.ReLoadHouseListForDeriveCompany((int)data["ID_Client_Detail"], (int)cbPeriod.EditValue, false);
                    houseListDebt.ReLoadHouseListForDeriveCompany((int)data["ID_Client_Detail"], (int)cbPeriod.EditValue, true);
                    break;
                case 3: //Переплата
                    tabCompanyHouses.PageEnabled = false;
                    tabAllHouses.PageEnabled = false;
                    break;
            }
        }

        //фильтр по наименованию УК
        private void txtDeriveCompany_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            //убираем данную функциональность иначе тормозит, много запросов
            //FilterClientDetailGridByTxtDeriveCompany();
        }

        private void xtraTabControl1_Click(object sender, EventArgs e)
        {
            LoadData((int)cbPeriod.EditValue);
        }

        private void LoadData(int period)
        {
            Cursor = Cursors.WaitCursor;
            txtDeriveCompany.Text = "";
            if (xtraTabControl1.SelectedTabPage == tabActiveClientDetail)
            {
                //billFilterCurrentGridView.ClearColumnsFilter();
                if (!gridBillFilterCurrent.Initialized)
                    gridBillFilterCurrent.LoadData(period, true);
            }
            else
            {
                if (!houseClientDetailFilter1.Initialized)
                    houseClientDetailFilter1.LoadData(period);
            }
            Cursor = Cursors.Default;
        }

        private void ReloadData(int period)
        {
            Cursor = Cursors.WaitCursor;
            txtDeriveCompany.Text = "";
            if (xtraTabControl1.SelectedTabPage == tabActiveClientDetail)
            {
                //billFilterCurrentGridView.ClearColumnsFilter();
                if (gridBillFilterCurrent.Initialized)
                    gridBillFilterCurrent.ReLoadData(period, true);
                else
                    gridBillFilterCurrent.LoadData(period, true);

                gridBillFilterCurrent.GridView.FocusedRowHandle = 0;
                ReLoadCurrentHouseList();
            }
            else
            {
                if (houseClientDetailFilter1.Initialized)
                    houseClientDetailFilter1.ReLoadData(period);
                else
                    houseClientDetailFilter1.LoadData(period);
            }
            Cursor = Cursors.Default;
        }

        private void clientDetailHouseList1_ShowAllBillFiltersByHouseIDEvent(int idHouse)
        {
            Cursor = Cursors.WaitCursor;
            if (!houseClientDetailFilter1.Initialized)
                houseClientDetailFilter1.LoadData((int)cbPeriod.EditValue);

            houseClientDetailFilter1.FindAndSelectHouse(idHouse);
            Cursor = Cursors.Default;
            xtraTabControl1.SelectedTabPage = tabHouseClientDetail;
        }

        private void houseListCurrent_RequestReloadHouseListEvent()
        {
            ReLoadCurrentHouseList();
        }

        private void FrmMain_Shown(object sender, EventArgs e)
        {
            formShown = true;
            ReLoadCurrentHouseList();
        }

        private void txtDeriveCompany_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                FilterClientDetailGridByTxtDeriveCompany();
        }

        private void FilterClientDetailGridByTxtDeriveCompany()
        {
            if (xtraTabControl1.SelectedTabPage == tabActiveClientDetail)
            {
                mDataSet.ClientDetailHouse.Clear();
                if (txtDeriveCompany.Text.Length > 1)
                {
                    billFilterCurrentGridView.Columns["ID_Derive_Company"].ClearFilter();
                    billFilterCurrentGridView.ActiveFilter.Add(new ViewColumnFilterInfo(billFilterCurrentGridView.Columns["ID_Derive_Company"], new DevExpress.XtraGrid.Columns.ColumnFilterInfo("[ID_Derive_Company] like '%" + txtDeriveCompany.Text + "%'")));
                    ReLoadCurrentHouseList();
                }
                else
                    billFilterCurrentGridView.Columns["ID_Derive_Company"].ClearFilter();
                billFilterCurrentGridView.ApplyColumnsFilter();
            }
        }

        private void lblButtonFind_Click(object sender, EventArgs e)
        {
            FilterClientDetailGridByTxtDeriveCompany();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //FrmAgreement f = new FrmAgreement(true);
            //f.Show();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.D | Keys.Control))
            {
                FrmAgreement f = new FrmAgreement(true);
                f.Show();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
