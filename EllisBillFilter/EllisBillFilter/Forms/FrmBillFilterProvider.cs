﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using EllisBillFilter.DS;

    using MrivcHelpers.Helpers;

    public partial class FrmBillFilterProvider : Form
    {
        private int idBillFilter;

        public FrmBillFilterProvider()
        {
            InitializeComponent();
        }

        public FrmBillFilterProvider(int idBillFilter)
        {
            InitializeComponent();
            this.Text += ": " + DAccess.Instance.GetBillFilterName(idBillFilter);
            this.idBillFilter = idBillFilter;
        }

        public void LoadData()
        {
            mDataSet.BillFilterDetailProviderFilter.Clear();
            mDataSet.ProviderGroupFilter.Clear();

            DAccess.DataModule.BillFilterDetailProviderFilterSelect(mDataSet.BillFilterDetailProviderFilter, this.idBillFilter);
            mDataSet.ProviderGroupFilter.Merge(DAccess.Instance.ProviderGroupFilter);

            LoadLayout();
            gridViewBillFilterProvider.Layout += gridLayoutSave;
        }

        public void ReLoadData()
        {
            mDataSet.BillFilterDetailProviderFilter.Clear();
            DAccess.DataModule.BillFilterDetailProviderFilterSelect(mDataSet.BillFilterDetailProviderFilter, this.idBillFilter);
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle < 0)
                return;

            DataRow row = gridViewBillFilterProvider.GetDataRow(e.FocusedRowHandle);
            DataRowView filter = null;

            if (row["ID_Provider_Group_Filter"] != DBNull.Value)
            {
                filter = (DataRowView) riProviderGroupFilter.GetDataSourceRowByKeyValue((int)row["ID_Provider_Group_Filter"]);
                txtCondition.Text = filter["Condition"].ToString();
            }
            else txtCondition.Text = "";

            if (row["ID_Provider_Group_Filter"] != DBNull.Value && filter["Condition"].ToString().ToLower().Contains("not"))
            {
                ReloadProviderGroup(filter["Condition"].ToString());
            }
            else if (row["ID_Provider_Group_Filter"] != DBNull.Value && filter["Condition"].ToString().ToLower().Contains("in"))
            {
                ReloadProviderGroup(filter["Condition"].ToString());
            }
            else
            {
                ReloadProviderGroup(string.Empty);
            }
        }

        private void ReloadProviderGroup(string condition)
        {
            mDataSet.ProviderGroup.Clear();

            if (condition.ToLower().Contains("not"))
            {
                condition = condition.Replace("not", "");
                rbConditionNotIn.Checked = true;
                gridViewProvider.ViewCaption = "ВСЕ КРОМЕ";
            }
            else if (condition.ToLower().Contains("in"))
            {
                rbConditionIn.Checked = true;
                gridViewProvider.ViewCaption = "ТОЛЬКО";
            }
            else
            {
                rbConditionNotIn.Checked = false;
                rbConditionIn.Checked = false;
                gridViewProvider.ViewCaption = "НЕТ УСЛОВИЙ (ВСЕ ПОСТАВЩИКИ)";
            }

            if (condition.Length > 0)
                DAccess.DataModule.ProviderGroupSelect(mDataSet.ProviderGroup, "where ID_Provider_Group " + condition);
        }

        #region Layout Save/Load
        void gridLayoutSave(object sender, EventArgs e)
        {
            SaveLayout();
        }

        public void SaveLayout()
        {
            gridViewBillFilterProvider.SaveLayoutToXml("gridViewBillFilterProvider.xml");
        }

        public void LoadLayout()
        {
            if (FileHelper.ExistFile("gridViewBillFilterProvider.xml"))
                gridViewBillFilterProvider.RestoreLayoutFromXml("gridViewBillFilterProvider.xml");
        }
        #endregion

        private void miProviderGroupAdd_Click(object sender, EventArgs e)
        {
            FrmProviderGroup f = new FrmProviderGroup();
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                mDataSet.ProviderGroup.ImportRow(f.SelectedProviderGroupRow);
                mDataSet.ProviderGroup.AcceptChanges();
            }
        }

        private void miProviderGroupRemove_Click(object sender, EventArgs e)
        {
            if (gridViewProvider.FocusedRowHandle < 0)
                return;

            gridViewProvider.DeleteRow(gridViewProvider.FocusedRowHandle);
            mDataSet.ProviderGroup.AcceptChanges();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.SaveData();
        }

        private void SaveData()
        {
            if (!rbConditionIn.Checked && !rbConditionNotIn.Checked)
            {
                MessageBox.Show(
                    "Не указано условие включения поставщиков. Сохранение отменено.",
                    "Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            if (gridViewBillFilterProvider.FocusedRowHandle < 0)
            {
                MessageBox.Show("Нет изменений.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (MessageBox.Show("Вы уверены, что хотите применить изменения?",
                    "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Cursor = Cursors.WaitCursor;
                string condition = this.mDataSet.ProviderGroup.Rows.Cast<MDataSet.ProviderGroupRow>().Aggregate("", (current, row) => current + ("," + row.ID_Provider_Group.ToString()));

                if (condition.Length > 1)
                {
                    condition = condition.Substring(1);
                }

                MDataSet.BillFilterDetailProviderFilterRow r = (MDataSet.BillFilterDetailProviderFilterRow)gridViewBillFilterProvider.GetDataRow(gridViewBillFilterProvider.FocusedRowHandle);
                condition = this.SetActiveCondition(condition);

                DAccess.DataModule.BillFilterServiceProviderUpdate(
                    r.ID_Provider_Group_Filter,
                    r.ID_Bill_Filter,
                    r.ID_Service,
                    condition);

                this.Cursor = Cursors.Default;

                MessageBox.Show(
                    "Код фильтра изменен успешно.",
                    "Информация",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private string SetActiveCondition(string condition)
        {
            DataRow r = gridViewBillFilterProvider.GetDataRow(gridViewBillFilterProvider.FocusedRowHandle);
            DataRowView filter = (DataRowView) riProviderGroupFilter.GetDataSourceRowByKeyValue((int)r["ID_Provider_Group_Filter"]);
            if (r != null)
            {
                string preCond = "";

                if (rbConditionNotIn.Checked) preCond = "not in (<c>)";
                if (rbConditionIn.Checked) preCond = "in (<c>)";

                if (condition.Length > 0)
                {
                    filter["Condition"] = preCond.Replace("<c>", condition);
                    txtCondition.Text = preCond.Replace("<c>", condition);
                }
                else
                {
                    filter["Condition"] = "";
                    txtCondition.Text = "";
                }
            }
            else
            {
                throw new Exception("Не удалось определить активный код фильтра. (SetActiveCondition)");
            }

            return txtCondition.Text;
        }

        private void rbConditionNotIn_CheckedChanged(object sender, EventArgs e)
        {
            if (rbConditionNotIn.Checked)
                gridViewProvider.ViewCaption = "ВСЕ КРОМЕ";
        }

        private void rbConditionIn_CheckedChanged(object sender, EventArgs e)
        {
            if (rbConditionIn.Checked)
                gridViewProvider.ViewCaption = "ТОЛЬКО";
        }

        private void miProviderGroupRemoveAll_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                "Вы уверены, что хотите удалить всех поставщиков из фильтра?",
                "Вопрос",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                mDataSet.ProviderGroup.Clear();
            }
        }

        private void riProviderGroupFilter_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            DataRow filter = DAccess.Instance.GetProviderGroupFilter((int)e.NewValue);
            this.ReloadProviderGroup(filter["Condition"].ToString());
            txtCondition.Text = filter["Condition"].ToString();

            DataRow r = gridViewBillFilterProvider.GetDataRow(gridViewBillFilterProvider.FocusedRowHandle);
            r["ID_Provider_Group_Filter"] = (int)e.NewValue;

            DAccess.DataModule.BillFilterDetailProviderFilterUpdate(mDataSet.BillFilterDetailProviderFilter);
        }

        private void miBillFilterDetail_Refresh_Click(object sender, EventArgs e)
        {
            this.ReLoadData();
        }

        private void miBillFilterDetail_DeleteProviderFilter_Click(object sender, EventArgs e)
        {
            DataRow r = gridViewBillFilterProvider.GetDataRow(gridViewBillFilterProvider.FocusedRowHandle);

            if (r == null || r["ID_Provider_Group_Filter"] == DBNull.Value)
            {
                MessageBox.Show(
                    "Вы не выбрали услугу или по данной услуги уже фильтра по поставщикам.",
                    "Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            if (MessageBox.Show(
                    "Вы уверены, что хотите удалить фильтр по поставщикам по текущей услуги?",
                    "Вопрос",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                r["ID_Provider_Group_Filter"] = DBNull.Value;
                txtCondition.Text = "";
                this.ReloadProviderGroup("");
                DAccess.DataModule.BillFilterDetailProviderFilterUpdate(mDataSet.BillFilterDetailProviderFilter);
            }
        }
    }
}
