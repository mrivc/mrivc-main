using System;
using System.Collections.Generic;
using System.Text;

namespace FastReport.Cloud
{
    /// <summary>
    /// Represents an HTTP request method.
    /// </summary>
    public static class HttpMethod
    {
        #region Constants

        /// <summary>
        /// GET request method.
        /// </summary>
        public const string Get = "GET";
        
        /// <summary>
        /// POST request method.
        /// </summary>
        public const string Post = "POST";

        /// <summary>
        /// PUT request method.
        /// </summary>
        public const string Put = "PUT";

        #endregion // Constants
    }
}
