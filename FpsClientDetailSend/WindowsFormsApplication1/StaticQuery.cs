﻿using System;

namespace WindowsFormsApplication1
{
    public static class StaticQuery
    {
        public static readonly string ExcelTable =
                                "create table ExcelTable([Код клиента] varchar(250), [Наименование клиента] varchar(250), [Город] varchar(250), [Управляющая компания] varchar(250), [Код УК (1-4)] varchar(250)," + Environment.NewLine +
                                "[Код вида ПД (5-6)] varchar(250), [Вид платежного документа] varchar(250));";
    }
}
