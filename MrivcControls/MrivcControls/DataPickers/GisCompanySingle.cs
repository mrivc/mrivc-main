﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MrivcControls
{
    public class GisCompanySingle : SingleValueDataPicker
    {
        public void Fill(string connectionString, int companyType)
        {
            DAccess.DataModule.Connection1 = new SqlConnection(connectionString);
            var dt = DAccess.DataModule.GetDataTableByQueryCommand1("select id_company,SenderName from GIS.Provider where isRSO = @type", new[,] { { "@type", companyType.ToString() } });
            DataSource = dt;
        }

        public GisCompanySingle()
        {
            ParameterName = "Зарегистрированные компании";
            ValueMember = "id_company";
            DisplayMember = "SenderName";
        }
    }
}
