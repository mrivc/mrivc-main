﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MrivcHelpers.Helpers
{
    internal class ExcelWrapper : IDisposable
    {
        private class Window
        {
            [DllImport("user32.dll", SetLastError = true)]
            static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

            [DllImport("user32.dll")]
            private static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd,
                out IntPtr ProcessID);

            public static IntPtr GetWindowThreadProcessId(IntPtr hWnd)
            {
                IntPtr processID;
                IntPtr returnResult = GetWindowThreadProcessId(hWnd, out processID);
                return processID;
            }

            public static IntPtr FindExcel(string caption)
            {
                IntPtr hWnd = FindWindow("XLMAIN", caption);
                return hWnd;
            }
        }

        private Application excel;
        private IntPtr windowHandle;
        private IntPtr processID;
        private const string ExcelWindowCaption = "MyUniqueExcelCaption";

        public ExcelWrapper()
        {
            excel = CreateExcelApplication();
            windowHandle = Window.FindExcel(ExcelWindowCaption);
            processID = Window.GetWindowThreadProcessId(windowHandle);
        }

        private Application CreateExcelApplication()
        {
            Application excel = new Application();
            excel.Caption = ExcelWindowCaption;
            excel.Visible = false;
            excel.DisplayAlerts = false;
            excel.AlertBeforeOverwriting = false;
            excel.AskToUpdateLinks = false;
            return excel;
        }

        public Application Excel
        {
            get { return this.excel; }
        }

        public int ProcessID
        {
            get { return this.processID.ToInt32(); }
        }

        public int WindowHandle
        {
            get { return this.windowHandle.ToInt32(); }
        }

        public void Dispose()
        {
            if (excel != null)
            {
                excel.Workbooks.Close();
                excel.Quit();
                Marshal.ReleaseComObject(excel);
                excel = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                try
                {
                    Process process = Process.GetProcessById(this.ProcessID);

                    if (process != null)
                    {
                        process.Kill();
                    }
                }
                catch
                {
                    throw;
                }
            }
        }
    }
}
