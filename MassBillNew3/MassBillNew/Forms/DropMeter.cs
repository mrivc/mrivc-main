﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;

namespace MassBillNew
{
    using MrivcControls.Data;

    public partial class DropMeter : Form
    {
        #region var

        string id = "";

        string name = "";

        int people0 = 0;

        int pustMunKvart = 0;

        ArrayList AccountList = new ArrayList();
        ArrayList TagList = new ArrayList();

        string idTag = "";

        string IdCompany = "";

        string ParamType = "";

        string ParamNormativ = "";

        SqlConnection sqlConnection1 = StaticConfig.MainConnection;

        #endregion

        DModule dModule = new DModule();

        public DropMeter()
        {
            InitializeComponent();
            this.Text = this.Text + (" (" + StaticConfig.MainConnection.DataSource + " - " + StaticConfig.MainConnection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;

            dModule.SetConnection(StaticConfig.MainConnection.ConnectionString);
        }

        private void FillTreeView()
        {
            treeView1.Nodes.Clear();

            SqlConnection sqlConnection1;

            sqlConnection1 = StaticConfig.MainConnection;

            SqlCommand cmd = new SqlCommand();

            cmd.CommandText =

                    " select distinct c.id_company,c.name from  house_derive_company hdc " +
                    " inner join company c on c.id_company = hdc.id_company" +
                    " where hdc.period = dbo.work_period() order by c.name";

            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;
            sqlConnection1.Open();

            SqlDataReader reader;
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {

                TreeNode treenode = new TreeNode();
                treenode.Tag = "id_company = " + reader["id_company"].ToString();
                treenode.Text = reader["name"].ToString();

                treeView1.Nodes.Add(treenode);
                treenode.ImageIndex = 0;
                treenode.Nodes.Add("*");
            }

            TreeNode last_tn = new TreeNode();
            last_tn.ImageIndex = 7;
            last_tn.Text = "";
            treeView1.Nodes.Add(last_tn);

            sqlConnection1.Close(); 
        }


        private void DropMeter_Load(object sender, EventArgs e)
        {
            DateTime dt = new DateTime();

            dt = DateTime.Today;

            dt = dt.AddMonths(-1);
                 
            int lastDayMonth = DateTime.DaysInMonth(dt.Year, dt.Month);

            DateTime dt1 = new DateTime(dt.Year, dt.Month, lastDayMonth);

            dateTimePicker1.Value = dt1;

            FillTreeView();
        }

        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {


            try
            {
                MultiSelectTreeview lb = ((MultiSelectTreeview)sender);

                foreach (TreeNode node in lb.SelectedNodes)
                {
                    idTag = node.Tag.ToString();
                    lb.DoDragDrop(node.FullPath, DragDropEffects.Copy);

                }

            }
            catch (Exception)
            {


            }
        }

        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("Text"))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void listBox1_DragDrop(object sender, DragEventArgs e)
        {
            TagList.Add(idTag);

            string ind = e.Data.GetData("Text").ToString();

            listBox1.Items.Add(ind);
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            ArrayList list = new ArrayList();
            ArrayList listTag = new ArrayList();

            if (e.KeyCode == Keys.Delete)
                foreach (object item in listBox1.SelectedItems)
                {
                    list.Add(item);
                    listTag.Add(TagList[(listBox1.Items.IndexOf(item))]);

                }

            foreach (object item in list)
            {
                listBox1.Items.Remove(item);

            }

            foreach (object item in listTag)
            {
                TagList.Remove(item);

            }
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                Point pt = new Point(e.X, e.Y);

                int i = listBox1.IndexFromPoint(pt);

                listBox1.Items.RemoveAt(i);
                TagList.RemoveAt(i);
            }
            catch (Exception)
            {

            }
        }

        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {

            TreeNode treenode = e.Node;
            treenode.Nodes.Clear();


            if (treenode.Level == 0)
            {
                IdCompany = treenode.Tag.ToString();
            }

            SqlConnection sqlConnection1 = StaticConfig.MainConnection;

            SqlCommand cmd = new SqlCommand();

            int imgIndex = 0;

            switch (treenode.Level)
            {
                case 0:
                    id = "id_settlement";
                    name = "Settlement";
                    imgIndex = 1;
                    break;
                case 1:
                    id = "id_street";
                    name = "street";
                    imgIndex = 2;
                    break;
                case 2:
                    id = "id_house";
                    name = "Full_house_name";
                    imgIndex = 3;
                    break;
                case 3:
                    id = "id_flat";
                    name = "Full_Flat_Name";
                    imgIndex = 4;
                    break;
                case 4:
                    id = "id_account";
                    name = "Account";
                    imgIndex = 5;
                    break;
            }

            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            cmd.CommandText =
            "select distinct aa." + id + "," + name + " from  house_derive_company hdc " +
            "inner join vaccount_address_all aa on aa.id_house = hdc.id_house " +
            "where hdc.period = dbo.work_period() and aa." + treenode.Tag.ToString();

            cmd.CommandText = cmd.CommandText.Replace("aa.id_company", "id_company");

            if (treenode.Level == 1 | treenode.Level == 4)
            {
                cmd.CommandText += " order by " + name;
            }
            if (treenode.Level == 3)
            {
                cmd.CommandText += " order by flat_number";
                cmd.CommandText = cmd.CommandText.Replace("distinct", " ");
            }

            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;
            sqlConnection1.Open();

            SqlDataReader reader;
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {

                TreeNode new_treenode = new TreeNode();
                new_treenode.Tag = id + " = " + reader[id].ToString() + " and " + IdCompany;
                new_treenode.Text = reader[name].ToString();
                new_treenode.ImageIndex = imgIndex;
                treenode.Nodes.Add(new_treenode);

                if (treenode.Level != 4)
                {
                    new_treenode.Nodes.Add("*");
                }

            }

            sqlConnection1.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //try
            //{

           
            if (TagList.Count == 0)
            {
                MessageBox.Show("Не выбран адрес");
                return;
            }


            string MType = "(";

            ParamType = "(";

            ParamNormativ = "(";

            if (checkBox1.Checked) { MType += "1,4,"; ParamType += "'VHOL','VHOL_KV',"; ParamNormativ += "'НА_НОРМАТИВ_ХВОДА',"; };
            if (checkBox2.Checked) { MType += "2,5,"; ParamType += "'VGOR','VGOR_KV',"; ParamNormativ += "'НА_НОРМАТИВ_ГВОДА',"; };
            if (checkBox3.Checked) { MType += "3,6,"; ParamType += "'GAZ','GAZ_KV',"; ParamNormativ += "'НА_НОРМАТИВ_ГАЗ',"; };
            if (checkBox4.Checked) { MType += "7,8,"; ParamType += "'METER_EL','METER_EL_KV',"; ParamNormativ += "'НА_НОРМАТИВ_ЭЛЭН',"; };
            if (checkBox5.Checked) { MType += "10,"; ParamType += "'METER_EL_NIGHT',"; ParamNormativ += "'НА_НОРМАТИВ_ЭЛЭН_НОЧЬ',"; };

            MType = MType.TrimEnd(',');

            MType += ")";

            ParamType = ParamType.TrimEnd(',');

            ParamType += ")";

            ParamNormativ = ParamNormativ.TrimEnd(',');

            ParamNormativ += ")";

            if (MType == "()")
            {
                MessageBox.Show("Не выбран тип счетчика");
                return;
            }

            dataGridView1.Rows.Clear();

            this.Cursor = Cursors.WaitCursor;

            

          

            SqlConnection sqlConnection1 = StaticConfig.MainConnection;

            SqlCommand cmd = new SqlCommand();

            cmd.CommandText =
            "select distinct aa.settlement,aa.street,aa.house_number,aa.house_block,aa.house_building,aa.house_liter,aa.flat_number,aa.flat_liter,aa.rooms,aa.account,aa.address,am.ID_Meter,mt.meter_type,map.install_date,map.remove_date from  house_derive_company hdc " +
            "inner join vaccount_address_all aa on aa.id_house = hdc.id_house " +
            "inner join account_Meter am on am.ID_account = aa.ID_account " +

            "inner join Meter m on am.ID_Meter = m.ID_Meter " +
                  " and m.ID_Meter_Type in  " + MType +
            " inner join Meter_Type mt on m.ID_Meter_type = mt.ID_Meter_type " +
            "inner join Meter_Activity_Period map on map.ID_Meter = m.ID_Meter " +
            "and (map.Remove_Date > GetDate() " +
                    "or map.Remove_Date is null) " +
             " where hdc.period = dbo.work_period() and ( ";

            for (int i = 0; i < TagList.Count; i++)
            {

                if (i == (TagList.Count - 1))
                {
                    cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") ) ";
                }
                else
                {
                    cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") or ";
                }

            }

            cmd.CommandText = cmd.CommandText.Replace("aa.id_company", "id_company");

            cmd.CommandText += " union  select distinct aa.settlement,aa.street,aa.house_number,aa.house_block,aa.house_building,aa.house_liter,aa.flat_number,aa.flat_liter,aa.rooms,aa.account,aa.address,am.ID_Meter,mt.meter_type,map.install_date,map.remove_date from  house_derive_company hdc " +
              "inner join vaccount_address_all aa on aa.id_house = hdc.id_house " +
              "inner join Flat_Meter am on am.ID_Flat = aa.ID_Flat " +

              "inner join Meter m on am.ID_Meter = m.ID_Meter " +
                    " and m.ID_Meter_Type in  " + MType +
              " inner join Meter_Type mt on m.ID_Meter_type = mt.ID_Meter_type " +
              "inner join Meter_Activity_Period map on map.ID_Meter = m.ID_Meter " +
              "and (map.Remove_Date > GetDate() " +
                      "or map.Remove_Date is null) " +
               " where hdc.period = dbo.work_period() and ( ";

            for (int i = 0; i < TagList.Count; i++)
            {

                if (i == (TagList.Count - 1))
                {
                    cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") ) ";
                }
                else
                {
                    cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") or ";
                }

            }

            cmd.CommandText = cmd.CommandText.Replace("aa.id_company", "id_company");

            cmd.CommandText += " order by aa.settlement,aa.street,aa.house_number,aa.house_block,aa.house_building,aa.house_liter,aa.flat_number,aa.flat_liter,aa.rooms";

            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;
            sqlConnection1.Open();

            SqlDataReader reader;
            reader = cmd.ExecuteReader();


            while (reader.Read())
            {
                dataGridView1.Rows.Add(reader["account"], reader["address"], reader["id_meter"], reader["meter_type"], reader["install_date"], reader["remove_date"]);
            }

            sqlConnection1.Close();

            
            this.Cursor = Cursors.Arrow;


            //}
            //catch (Exception exp)
            //{
            //    MessageBox.Show(exp.Message);
            //    this.Cursor = Cursors.Arrow;
                
            //}
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
                if (dataGridView1.Rows.Count == 0)
                {
                    MessageBox.Show("Не выбрано ни одного счетчика");
                    return;
                }
            
            //if (textBox1.Text=="")
            //{
            //    MessageBox.Show("Введите примечание");
            //    return;
            //}

          

            

            SqlCommand cmd = new SqlCommand();

            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            SqlCommand cmd2 = new SqlCommand();

            cmd2.CommandTimeout = 0;
            cmd2.CommandType = CommandType.Text;
            cmd2.Connection = sqlConnection1;

            sqlConnection1.Open();


            string login = "";

            cmd2.CommandText = "select case when isnull(employee_login,'') = '' then win_login else employee_login end as login from employee where id_employee = dbo.get_user_id()";

            DateTime date = dateTimePicker1.Value;
            int idMeter = 0;

            login = cmd2.ExecuteScalar().ToString();

            foreach (DataGridViewRow row in dataGridView1.Rows)
	            {
                    idMeter = (int)row.Cells[2].Value;
                    
                    dModule.RemoveMeter(idMeter, date);
	            }

            sqlConnection1.Close();

            
            MessageBox.Show("Готово");
          
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
      
    }
}
