Unit uMDIChild;

Interface
Uses
  Windows,
  Messages,
  uMessages,
  Classes,
  Controls,
  Forms,
  EllisGKH_TLB,
  Dialogs,
  uPropertyCollection,
  uProperties,
  ADODB,
  DB,
  ImgList
//  ,cxLookAndFeels
//  ,cxPC
  ;

Type
  TMDIChildForm = Class(TForm)
    Procedure FormActivate(Sender: TObject);
    Procedure FormCreate(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
  Private
    { Private declarations }
    FComObj: TObject;
    FEllisSrv: IEllisCOMServer;
    FEllisSearch: IEllisCOMSearch;
    FEllisFilter: IEllisCOMFilter;
    FEllisAccountSubService: IEllisAccountSubSystem;
    Procedure CreateIntf;
    Procedure Update_TabIndex(Var MSG: TMessage); Message WM_MDIChildForm_Update_TabIndex;
    Procedure Update_Params(Var MSG: TMessage); Message WM_MDIChildForm_ParamsChange;
    function GetOptions(AIndex: String): Variant;
  Protected
    FMainFormTabIndex: Integer;
    FOptions: TPropertyCollection;
    FConnection: TADOConnection;
    FImageList: TCustomImageList;
    FDLLHandle: THandle;
//    FPCStyle: TcxPCStyleID;
    Procedure UpdateParams; Virtual;
    Procedure SetCaption(Value: TCaption); Virtual;
    Function GetCaption: TCaption; Virtual;
  Public
    { Public declarations }
    Constructor Create(AOwner: TComponent; AOptions: TPropertyCollection); Reintroduce; Virtual;
    Constructor CreateSecondary(AOwner: TComponent; AOptions: TPropertyCollection); Reintroduce; Virtual;
    Destructor Destroy; Override;
    Procedure ShowMessage(AMsg: WideString; ATime: Integer = 0);
    Function ShowQuery(AMsg: WideString): Boolean;
    Procedure ShowError(AMsg: WideString; ATime: Integer = 0);
    Procedure GoToAccount(Account: String);
    Procedure StartOperation;
    Procedure FinishOperation(Const ADescr: WideString);
    Property ComObj: TObject Read FComObj;
    Property EllisComSrv: IEllisCOMServer Read FEllisSrv;
    Property EllisComSearch: IEllisCOMSearch Read FEllisSearch;
    Property EllisComFilter: IEllisCOMFilter Read FEllisFilter;
    Property EllisComAccountSubService: IEllisAccountSubSystem Read FEllisAccountSubService;
    Property Caption: TCaption Read GetCaption Write SetCaption;
    Property Options[AIndex: String]: Variant Read GetOptions;
  End;

TMDIChildFormClass = Class Of TMDIChildForm;

Implementation

Uses SysUtils, uReportUtils;

{$R *.dfm}
Resourcestring
  ErrorMsgInterfaceNotSupported =
    '������������� COM-��������� �� ��������������. �������� ������ ���������';
  ErrorMsgNoComObject =
    '������������� COM-������ �� ������. �������� ������ ���������';

Constructor TMDIChildForm.Create(AOwner: TComponent; AOptions: TPropertyCollection);
Begin
  FDLLHandle := 0;
  FMainFormTabIndex := -1;
  {����� �� ������������ ����� �������, ���� � ������������ ���������� Exception}
  Inherited Create(AOwner);
  FOptions := AOptions;
  FConnection := TADOConnection(Integer(FOptions[pConnection]));
  FImageList := TCustomImageList(Integer(FOptions[pImageList]));
  Caption := CaptionByDescription(HInstance);
  FComObj := TObject(Integer(FOptions[pEllisCOMServer]));
  CreateIntf;
  Try
    FDLLHandle := THandle(FOptions[pDLLHandle]);
  Except
    FDLLHandle := 0;
  End;
  UpdateParams;
End;

constructor TMDIChildForm.CreateSecondary(AOwner: TComponent;
  AOptions: TPropertyCollection);
begin
  FDLLHandle := 0;
  FMainFormTabIndex := -1;
  Inherited Create(AOwner);
  FOptions := AOptions;
  FConnection := TADOConnection(Integer(FOptions[pConnection]));
  FImageList := TCustomImageList(Integer(FOptions[pImageList]));
  FComObj := TObject(Integer(FOptions[pEllisCOMServer]));
  CreateIntf;
  UpdateParams;
end;

Procedure TMDIChildForm.CreateIntf;
Var
  B: Boolean;
Begin
  B := True;
  If Assigned(FComObj) Then
    Begin
      B := B And FComObj.GetInterface(IEllisCOMServer, FEllisSrv);
      B := B And FComObj.GetInterface(IEllisCOMSearch, FEllisSearch);
      B := B And FComObj.GetInterface(IID_IEllisCOMFilter, FEllisFilter);
      B := B And FComObj.GetInterface(IID_IEllisAccountSubSystem, FEllisAccountSubService);
      If Not B Then
        Begin
          Dialogs.ShowMessage(ErrorMsgInterfaceNotSupported);
        End;
    End
  Else
    Begin
      Dialogs.ShowMessage(ErrorMsgNoComObject);
    End;
End;

Destructor TMDIChildForm.Destroy;
Var
  H: HWND;
Begin
  If Assigned(Application.MainForm) And (FMainFormTabIndex >= 0) Then
    Begin
      H := Application.MainForm.Handle;
      SendMessage(H, WM_MDIChildForm_Destroyed, FMainFormTabIndex, FDLLHandle);
    End;
  FEllisSrv := Nil;
  FEllisSearch := Nil;
  FEllisFilter := Nil;
  FEllisAccountSubService := Nil;
  Inherited;
End;

Procedure TMDIChildForm.FinishOperation(Const ADescr: WideString);
Begin
  FEllisSrv.FinishOperation(ADescr);
End;

Procedure TMDIChildForm.FormActivate(Sender: TObject);
Begin
  If Assigned(Application.MainForm) Then
    SendMessage(Application.MainForm.Handle, WM_MDIChildForm_Activated, FMainFormTabIndex, 0);
End;

Procedure TMDIChildForm.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  Action := caFree;
End;

Procedure TMDIChildForm.FormCreate(Sender: TObject);
Begin
  FMainFormTabIndex := SendMessage(Application.MainForm.Handle, WM_MDIChildForm_Created, Integer(Self), 0);
  UpdateParams;
End;

Function TMDIChildForm.GetCaption: TCaption;
Begin
  Result := Inherited Caption;
End;

function TMDIChildForm.GetOptions(AIndex: String): Variant;
begin
  Result := FOptions[AIndex];
end;

Procedure TMDIChildForm.GoToAccount(Account: String);
Begin
  FEllisSrv._goAccount(Account);
End;

Procedure TMDIChildForm.SetCaption(Value: TCaption);
Begin
  Inherited Caption := Value;
  If FMainFormTabIndex >= 0 Then
    SendMessage(Application.MainForm.Handle, WM_MDIChildForm_Update_Caption, FMainFormTabIndex, 0);
End;

Procedure TMDIChildForm.ShowError(AMsg: WideString; ATime: Integer);
Begin
  If Assigned(FEllisSrv) Then
    FEllisSrv._dlgError(AMsg, ATime)
  Else
    Dialogs.ShowMessage(AMsg);
End;

Procedure TMDIChildForm.ShowMessage(AMsg: WideString; ATime: Integer);
Begin
  If Assigned(FEllisSrv) Then
    FEllisSrv._dlgInfo(AMsg, ATime)
  Else
    Dialogs.ShowMessage(AMsg);
End;

Function TMDIChildForm.ShowQuery(AMsg: WideString): Boolean;
Begin
  If Assigned(FEllisSrv) Then
    Begin
      Result := FEllisSrv._dlgQueryYN(AMsg, 0) <> 0;
    End
  Else
    Result := Dialogs.MessageDlg(AMsg, mtConfirmation, mbYesNo, 0) = ID_YES;
End;

Procedure TMDIChildForm.StartOperation;
Begin
  FEllisSrv.StartOperation;
End;

Procedure TMDIChildForm.UpdateParams;
Var
  I: Integer;
Begin
//  If Assigned(FOptions) Then
//    Begin
//      RootLookAndFeel.NativeStyle := Boolean(Options[pNativeStyle]);
//      RootLookAndFeel.Kind := TcxLookAndFeelKind(Integer(Options[pStyleKind]));
//      FPCStyle := Options[pPCStyle];
//      For I := 0 To ComponentCount - 1 Do
//        If Components[I] Is TcxPageControl Then
//          TcxPageControl(Components[I]).Style := FPCStyle;
//    End;
End;

Procedure TMDIChildForm.Update_Params(Var MSG: TMessage);
Begin
  UpdateParams;
End;

Procedure TMDIChildForm.Update_TabIndex(Var MSG: TMessage);
Begin
  FMainFormTabIndex := MSG.WParam;
End;

End.

