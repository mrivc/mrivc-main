object SelectEditFilter: TSelectEditFilter
  Left = 0
  Top = 0
  Width = 225
  Height = 57
  AutoSize = True
  TabOrder = 0
  object GB: TcxGroupBox
    Left = 0
    Top = 0
    Caption = ' '
    TabOrder = 0
    Transparent = True
    DesignSize = (
      225
      57)
    Height = 57
    Width = 225
    object CB: TcxComboBox
      Left = 8
      Top = 24
      Anchors = [akLeft, akTop, akRight]
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 16
      Properties.DropDownSizeable = True
      Properties.OnCloseUp = CBCloseUp
      TabOrder = 0
      OnKeyPress = CBKeyPress
      Width = 209
    end
  end
  object adsList: TADODataSet
    Parameters = <>
    Top = 56
  end
end
