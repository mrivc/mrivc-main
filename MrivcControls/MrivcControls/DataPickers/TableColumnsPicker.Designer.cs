﻿namespace MrivcControls.DataPickers
{
    partial class TableColumnsPicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddTableColumnsPicker = new EllisBillFilter.BaseDropDown();
            this.ddTableColumnsPickerView = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.ddTableColumnsPicker.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddTableColumnsPickerView)).BeginInit();
            this.SuspendLayout();
            // 
            // ddTableColumnsPicker
            // 
            this.ddTableColumnsPicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddTableColumnsPicker.EditValue = "";
            this.ddTableColumnsPicker.Location = new System.Drawing.Point(0, 0);
            this.ddTableColumnsPicker.Name = "ddTableColumnsPicker";
            this.ddTableColumnsPicker.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddTableColumnsPicker.Properties.ImmediatePopup = true;
            this.ddTableColumnsPicker.Properties.NullText = "";
            this.ddTableColumnsPicker.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.ddTableColumnsPicker.Properties.PopupFormSize = new System.Drawing.Size(0, 200);
            this.ddTableColumnsPicker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.ddTableColumnsPicker.Properties.View = this.ddTableColumnsPickerView;
            this.ddTableColumnsPicker.Size = new System.Drawing.Size(234, 20);
            this.ddTableColumnsPicker.TabIndex = 0;
            // 
            // ddTableColumnsPickerView
            // 
            this.ddTableColumnsPickerView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.ddTableColumnsPickerView.Name = "ddTableColumnsPickerView";
            this.ddTableColumnsPickerView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.ddTableColumnsPickerView.OptionsView.ShowColumnHeaders = false;
            this.ddTableColumnsPickerView.OptionsView.ShowGroupPanel = false;
            this.ddTableColumnsPickerView.OptionsView.ShowIndicator = false;
            // 
            // TableColumnsPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ddTableColumnsPicker);
            this.Name = "TableColumnsPicker";
            this.Size = new System.Drawing.Size(234, 21);
            ((System.ComponentModel.ISupportInitialize)(this.ddTableColumnsPicker.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddTableColumnsPickerView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private EllisBillFilter.BaseDropDown ddTableColumnsPicker;
        private DevExpress.XtraGrid.Views.Grid.GridView ddTableColumnsPickerView;
    }
}
