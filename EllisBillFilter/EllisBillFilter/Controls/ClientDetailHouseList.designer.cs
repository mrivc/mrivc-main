﻿namespace EllisBillFilter
{
    partial class ClientDetailHouseList
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.houseList = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.cmsHouseList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miHouseCheckUncheckSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miHouseCheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.miHouseUnCheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miHouseCheck_SelectedHouses = new System.Windows.Forms.ToolStripMenuItem();
            this.miHouseUnCheck_SelectedHouses = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.miHouse_ShowAllClientDetailByHouse = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miHouse_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.miHouse_CopyIdHouseToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.clientDetailHouseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.txtHouseSearch = new DevExpress.XtraEditors.TextEdit();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.houseList)).BeginInit();
            this.cmsHouseList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailHouseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHouseSearch.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // houseList
            // 
            this.houseList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.houseList.ContextMenuStrip = this.cmsHouseList;
            this.houseList.DataSource = this.clientDetailHouseBindingSource;
            this.houseList.DisplayMember = "House_Address";
            this.houseList.Location = new System.Drawing.Point(0, 29);
            this.houseList.Name = "houseList";
            this.houseList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.houseList.Size = new System.Drawing.Size(217, 417);
            this.houseList.TabIndex = 0;
            this.houseList.ValueMember = "ID_House";
            this.houseList.ItemChecking += new DevExpress.XtraEditors.Controls.ItemCheckingEventHandler(this.houseList_ItemChecking);
            this.houseList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.houseList_MouseDoubleClick);
            // 
            // cmsHouseList
            // 
            this.cmsHouseList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miHouseCheckUncheckSelected,
            this.toolStripSeparator2,
            this.miHouseCheckAll,
            this.miHouseUnCheckAll,
            this.toolStripSeparator3,
            this.miHouseCheck_SelectedHouses,
            this.miHouseUnCheck_SelectedHouses,
            this.toolStripSeparator7,
            this.miHouse_ShowAllClientDetailByHouse,
            this.toolStripSeparator1,
            this.miHouse_Refresh,
            this.miHouse_CopyIdHouseToClipboard});
            this.cmsHouseList.Name = "cmsHouseList";
            this.cmsHouseList.Size = new System.Drawing.Size(278, 204);
            // 
            // miHouseCheckUncheckSelected
            // 
            this.miHouseCheckUncheckSelected.Image = global::EllisBillFilter.Properties.Resources.checkbox_yes;
            this.miHouseCheckUncheckSelected.Name = "miHouseCheckUncheckSelected";
            this.miHouseCheckUncheckSelected.Size = new System.Drawing.Size(277, 22);
            this.miHouseCheckUncheckSelected.Text = "Установить/Снять код фильтра";
            this.miHouseCheckUncheckSelected.Click += new System.EventHandler(this.miHouseCheckUncheckSelected_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(274, 6);
            // 
            // miHouseCheckAll
            // 
            this.miHouseCheckAll.Image = global::EllisBillFilter.Properties.Resources.check;
            this.miHouseCheckAll.Name = "miHouseCheckAll";
            this.miHouseCheckAll.Size = new System.Drawing.Size(277, 22);
            this.miHouseCheckAll.Text = "Отметить все дома";
            this.miHouseCheckAll.Click += new System.EventHandler(this.miHouseCheckAll_Click);
            // 
            // miHouseUnCheckAll
            // 
            this.miHouseUnCheckAll.Image = global::EllisBillFilter.Properties.Resources.uncheck;
            this.miHouseUnCheckAll.Name = "miHouseUnCheckAll";
            this.miHouseUnCheckAll.Size = new System.Drawing.Size(277, 22);
            this.miHouseUnCheckAll.Text = "Снять все дома";
            this.miHouseUnCheckAll.Click += new System.EventHandler(this.miHouseUnCheckAll_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(274, 6);
            // 
            // miHouseCheck_SelectedHouses
            // 
            this.miHouseCheck_SelectedHouses.Name = "miHouseCheck_SelectedHouses";
            this.miHouseCheck_SelectedHouses.Size = new System.Drawing.Size(277, 22);
            this.miHouseCheck_SelectedHouses.Text = "Отметить выделенные дома";
            this.miHouseCheck_SelectedHouses.Click += new System.EventHandler(this.miHouseCheck_SelectedHouses_Click);
            // 
            // miHouseUnCheck_SelectedHouses
            // 
            this.miHouseUnCheck_SelectedHouses.Name = "miHouseUnCheck_SelectedHouses";
            this.miHouseUnCheck_SelectedHouses.Size = new System.Drawing.Size(277, 22);
            this.miHouseUnCheck_SelectedHouses.Text = "Снять выделенные дома";
            this.miHouseUnCheck_SelectedHouses.Click += new System.EventHandler(this.miHouseUnCheck_SelectedHouses_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(274, 6);
            // 
            // miHouse_ShowAllClientDetailByHouse
            // 
            this.miHouse_ShowAllClientDetailByHouse.Image = global::EllisBillFilter.Properties.Resources.house_5840;
            this.miHouse_ShowAllClientDetailByHouse.Name = "miHouse_ShowAllClientDetailByHouse";
            this.miHouse_ShowAllClientDetailByHouse.Size = new System.Drawing.Size(277, 22);
            this.miHouse_ShowAllClientDetailByHouse.Text = "Показать все коды фильтры по дому";
            this.miHouse_ShowAllClientDetailByHouse.Click += new System.EventHandler(this.miHouse_ShowAllClientDetailByHouse_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(274, 6);
            // 
            // miHouse_Refresh
            // 
            this.miHouse_Refresh.Image = global::EllisBillFilter.Properties.Resources.refresh;
            this.miHouse_Refresh.Name = "miHouse_Refresh";
            this.miHouse_Refresh.Size = new System.Drawing.Size(277, 22);
            this.miHouse_Refresh.Text = "Обновить список домов";
            this.miHouse_Refresh.Click += new System.EventHandler(this.miHouse_Refresh_Click);
            // 
            // miHouse_CopyIdHouseToClipboard
            // 
            this.miHouse_CopyIdHouseToClipboard.Name = "miHouse_CopyIdHouseToClipboard";
            this.miHouse_CopyIdHouseToClipboard.Size = new System.Drawing.Size(277, 22);
            this.miHouse_CopyIdHouseToClipboard.Text = "Скопировать ID дома в буфер";
            this.miHouse_CopyIdHouseToClipboard.Click += new System.EventHandler(this.miHouse_CopyIdHouseToClipboard_Click);
            // 
            // clientDetailHouseBindingSource
            // 
            this.clientDetailHouseBindingSource.DataMember = "ClientDetailHouse";
            this.clientDetailHouseBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txtHouseSearch
            // 
            this.txtHouseSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHouseSearch.Location = new System.Drawing.Point(3, 3);
            this.txtHouseSearch.Name = "txtHouseSearch";
            this.txtHouseSearch.Size = new System.Drawing.Size(177, 20);
            this.txtHouseSearch.TabIndex = 1;
            this.txtHouseSearch.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtHouseSearch_EditValueChanging);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(186, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 20);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ClientDetailHouseList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtHouseSearch);
            this.Controls.Add(this.houseList);
            this.Name = "ClientDetailHouseList";
            this.Size = new System.Drawing.Size(217, 446);
            ((System.ComponentModel.ISupportInitialize)(this.houseList)).EndInit();
            this.cmsHouseList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailHouseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHouseSearch.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckedListBoxControl houseList;
        private System.Windows.Forms.BindingSource clientDetailHouseBindingSource;
        private DS.MDataSet mDataSet;
        private System.Windows.Forms.ContextMenuStrip cmsHouseList;
        private System.Windows.Forms.ToolStripMenuItem miHouseCheckAll;
        private System.Windows.Forms.ToolStripMenuItem miHouseUnCheckAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem miHouseCheck_SelectedHouses;
        private System.Windows.Forms.ToolStripMenuItem miHouseUnCheck_SelectedHouses;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem miHouse_ShowAllClientDetailByHouse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miHouse_Refresh;
        private System.Windows.Forms.ToolStripMenuItem miHouse_CopyIdHouseToClipboard;
        private DevExpress.XtraEditors.TextEdit txtHouseSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem miHouseCheckUncheckSelected;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}
