﻿namespace XadesDemo
{
    partial class ImportHouseDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportHouseDataForm));
            this.importBtn = new System.Windows.Forms.Button();
            this.entranceDataGridView = new System.Windows.Forms.DataGridView();
            this.premisesDataGridView = new System.Windows.Forms.DataGridView();
            this.accountDataGridView = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.houseDataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.previewImportBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.entranceDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.premisesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountDataGridView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.houseDataGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // importBtn
            // 
            this.importBtn.Location = new System.Drawing.Point(573, 12);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(159, 23);
            this.importBtn.TabIndex = 1;
            this.importBtn.Text = "Импорт";
            this.importBtn.UseVisualStyleBackColor = true;
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // entranceDataGridView
            // 
            this.entranceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.entranceDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.entranceDataGridView.Location = new System.Drawing.Point(3, 3);
            this.entranceDataGridView.Name = "entranceDataGridView";
            this.entranceDataGridView.Size = new System.Drawing.Size(906, 449);
            this.entranceDataGridView.TabIndex = 3;
            this.entranceDataGridView.SelectionChanged += new System.EventHandler(this.entranceDataGridView_SelectionChanged);
            // 
            // premisesDataGridView
            // 
            this.premisesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.premisesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.premisesDataGridView.Location = new System.Drawing.Point(3, 3);
            this.premisesDataGridView.Name = "premisesDataGridView";
            this.premisesDataGridView.Size = new System.Drawing.Size(906, 449);
            this.premisesDataGridView.TabIndex = 4;
            // 
            // accountDataGridView
            // 
            this.accountDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.accountDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accountDataGridView.Location = new System.Drawing.Point(3, 3);
            this.accountDataGridView.Name = "accountDataGridView";
            this.accountDataGridView.Size = new System.Drawing.Size(906, 449);
            this.accountDataGridView.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(1, 73);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(920, 481);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.houseDataGridView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(912, 455);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Дома";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // houseDataGridView
            // 
            this.houseDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.houseDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.houseDataGridView.Location = new System.Drawing.Point(3, 3);
            this.houseDataGridView.Name = "houseDataGridView";
            this.houseDataGridView.Size = new System.Drawing.Size(906, 449);
            this.houseDataGridView.TabIndex = 2;
            this.houseDataGridView.SelectionChanged += new System.EventHandler(this.houseDataGridView_SelectionChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.entranceDataGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(912, 455);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Подъезды";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.premisesDataGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(912, 455);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Квартиры";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.accountDataGridView);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(912, 455);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Лицевые счета";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // previewImportBtn
            // 
            this.previewImportBtn.Location = new System.Drawing.Point(412, 12);
            this.previewImportBtn.Name = "previewImportBtn";
            this.previewImportBtn.Size = new System.Drawing.Size(155, 23);
            this.previewImportBtn.TabIndex = 9;
            this.previewImportBtn.Text = "Просмотр";
            this.previewImportBtn.UseVisualStyleBackColor = true;
            this.previewImportBtn.Click += new System.EventHandler(this.previewImportBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(412, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Просмотр ЛС";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(573, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Импорт ЛС";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ImportHouseDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 554);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.previewImportBtn);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.importBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportHouseDataForm";
            this.Text = "Синхронизация информации о домах";
            this.Load += new System.EventHandler(this.ImportHouseDataForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.entranceDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.premisesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountDataGridView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.houseDataGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button importBtn;
        private System.Windows.Forms.DataGridView entranceDataGridView;
        private System.Windows.Forms.DataGridView premisesDataGridView;
        private System.Windows.Forms.DataGridView accountDataGridView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button previewImportBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView houseDataGridView;
    }
}