﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Helper.Data;

namespace Helper.Controls
{
    public partial class SenderIdFilter : UserControl
    {
        public string SenderID
        {
            get { return cbSender.SelectedValue.ToString(); }
        }

        public bool HouseAll
        {
            get { return cbHouse.All; }
        }

        public string FiasHouseGuid
        {
            get { return cbHouse.FiasHouseGuid; }
        }

        public SenderIdFilter()
        {
            InitializeComponent();
        }

        public void Fill()
        {
            cbSender.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("select senderid,sendername from gis.provider order by 2");
            cbSender.ValueMember = "senderid";
            cbSender.DisplayMember = "sendername";

            FillHouseDataSource();
        }

        void FillHouseDataSource()
        {
            cbHouse.SenderID = SenderID;
            cbHouse.Fill();
        }

        private void cbSender_SelectionChangeCommitted(object sender, EventArgs e)
        {
            FillHouseDataSource();
        }

        private void gbSender_Enter(object sender, EventArgs e)
        {

        }

        private void cbHouse_Load(object sender, EventArgs e)
        {

        }
    }
}
