﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace mrivcupdate
{
    class Program
    {
        static void Main()
        {
            MySqlConnection mcon = new MySqlConnection(Properties.Settings.Default.Connection);

            mcon.Open();
            mcon.Close();

            SqlConnection sqlConnection1 = new SqlConnection(ConfigurationManager.ConnectionStrings["EllisConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = "exec town.Address_for_site";
            Console.WriteLine(cmd.CommandText);
            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;
            sqlConnection1.Open();
            reader = cmd.ExecuteReader();


            MySqlCommand mcmd = new MySqlCommand();
            mcon.Open();
            mcmd.Connection = mcon;
            mcmd.CommandType = CommandType.Text;
            mcmd.CommandTimeout = 0;

            mcmd.CommandText = "TRUNCATE TABLE `address_prepared`";
            mcmd.ExecuteNonQuery();


            while (reader.Read())
            {

                string IdM = reader[0].ToString();
                string Val = reader[7].ToString();
                string comp = reader[8].ToString();
                string street = reader[4].ToString();
                street = street.Replace("\"", "");

                if (IdM == "")
                    IdM = "0";
                if (Val == "")
                    Val = "0";
                if (comp == "")
                    comp = "0";

                Val = Val.Replace(",", ".");

                mcmd.CommandText = "insert into address_prepared  values(\"" + reader[2].ToString() + "\"," + IdM + ",\"" + reader[1] + "\",\"" + reader[3] + "\",\"" + street + "\",\"" + reader[5] + "\",\"" + reader[6] + "\"," + Val + "," + comp + ")";
                mcmd.ExecuteNonQuery();

            }

            mcmd.CommandText = "TRUNCATE TABLE `address`; INSERT INTO `address`(`settlement`, `id_meter`, `meter_type`, `district`, `street`, `Full_house_name`, `flat_number`, `measure_value`, `id_company`) SELECT * FROM `address_prepared`";
            mcmd.ExecuteNonQuery();

            mcon.Close();
            sqlConnection1.Close();

            //Загрузка лицевых счетов для ГИС "ЖКХ"

            cmd.CommandText = "exec GIS.SelectAccountsForSite";
            Console.WriteLine(cmd.CommandText);
            sqlConnection1.Open();
            reader = cmd.ExecuteReader();

            mcon.Open();

            mcmd.CommandText = "TRUNCATE TABLE `gis_account`";
            mcmd.ExecuteNonQuery();

            while (reader.Read())
            {
                string senderName = reader[0].ToString();
                string accountMrivc = reader[1].ToString();
                string accountGis = reader[2].ToString();

                mcmd.CommandText = "insert into gis_account values('" + senderName + "','" + accountMrivc + "','" + accountGis + "')";
                mcmd.ExecuteNonQuery();
            }

            //Конец. Загрузка лицевых счетов для ГИС "ЖКХ"


        }

        //   static void Main1()
        //    {

        //        MySqlConnection mcon = new MySqlConnection(Properties.Settings.Default.Connection);

        //                mcon.Open();
        //            mcon.Close();

        //            SqlConnection sqlConnection1 = new SqlConnection("Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;integrated security=true;Persist Security Info=false;User ID=;password=");
        //            SqlCommand cmd = new SqlCommand();
        //            SqlDataReader reader;
        //            cmd.CommandText = "exec town.Address_for_site";
        //            Console.WriteLine(cmd.CommandText);
        //            cmd.CommandTimeout = 0;
        //            cmd.CommandType = CommandType.Text;
        //            cmd.Connection = sqlConnection1;
        //            sqlConnection1.Open();
        //            reader = cmd.ExecuteReader();


        //            MySqlCommand mcmd = new MySqlCommand();
        //            mcon.Open();
        //            mcmd.Connection = mcon;
        //            mcmd.CommandType = CommandType.Text;
        //            mcmd.CommandText =  " delete from murmansk;" +
        //                                " delete from kola;" +
        //                                " delete from olenegorsk;" +
        //                                " delete from kildinstroy;" +
        //                                " delete from zverosovhoz;" +
        //                                " delete from shonguy;" +
        //                                " delete from murmashi;" +
        //                                " delete from nikel;" +
        //                                " delete from apatity;" +
        //                                " delete from zelenoborskiy;" +
        //                                " delete from kandalaksha;" +
        //                                " delete from prichalniy;" +
        //                                " delete from knyazhya_guba;" +
        //                                " delete from molochniy;" +
        //                                " delete from visokiy;" +
        //                                " delete from les;" +
        //                                " delete from vih;" +
        //                                " delete from les;" +
        //                                " delete from pes;" +
        //                                " delete from gad;" +
        //                                " delete from umb;" +
        //                                " delete from ter;" +
        //                                " delete from vertul;" +
        //                                " delete from push;" +
        //                                " delete from tuloma;"+
        //                                 " delete from ensk;";
        //            Console.WriteLine(mcmd.CommandText);
        //            mcmd.CommandTimeout = 0;
        //            mcmd.ExecuteNonQuery();
        //            while (reader.Read())
        //            {
        //                string settlement = "";
        //                switch (reader[2].ToString())
        //                {
        //                    case "Мурманск":
        //                        settlement = "murmansk";
        //                        break;
        //                    case "Кола":
        //                        settlement = "kola";
        //                        break;
        //                    //case "Оленегорск":
        //                    //    settlement = "olenegorsk";
        //                    //    break;
        //                    case "Кильдинстрой":
        //                        settlement = "kildinstroy";
        //                        break;
        //                    case "Зверосовхоз":
        //                        settlement = "zverosovhoz";
        //                        break;
        //                    case "Шонгуй":
        //                        settlement = "shonguy";
        //                        break;
        //                    case "Мурмаши":
        //                        settlement = "murmashi";
        //                        break;
        //                    case "Никель":
        //                        settlement = "nikel";
        //                        break;
        //                    //case "Апатиты":
        //                    //    settlement = "apatity";
        //                    //    break;
        //                    case "Зеленоборский":
        //                        settlement = "zelenoborskiy";
        //                        break;
        //                    case "Кандалакша":
        //                        settlement = "kandalaksha";
        //                        break;
        //                    //case "Причальный":
        //                    //    settlement = "prichalniy";
        //                    //    break;
        //                    //case "Княжья губа":
        //                    //    settlement = "knyazhya_guba";
        //                    //    break;
        //                    case "Молочный":
        //                        settlement = "molochniy";
        //                        break;
        //                    case "Высокий":
        //                        settlement = "visokiy";
        //                        break;
        //                    //case "Лесозаводский":
        //                    //    settlement = "les";
        //                    //    break;
        //                    //case "Выходной":
        //                    //    settlement = "vih";
        //                    //    break;
        //                    case "Песчаный":
        //                        settlement = "pes";
        //                        break;
        //                    case "Гаджиево":
        //                        settlement = "gad";
        //                        break;
        //                    case "Умба":
        //                        settlement = "umb";
        //                        break;
        //                    //case "Териберка":
        //                    //    settlement = "ter";
        //                    //    break;
        //                    case "Верхнетуломский":
        //                        settlement = "vertul";
        //                        break;
        //                    case "Пушной":
        //                        settlement = "push";
        //                        break;
        //                    case "Тулома":
        //                        settlement = "tuloma";
        //                        break;
        //                    case "Енский":
        //                        settlement = "ensk";
        //                        break;
        //                }
        //                if (settlement != "")
        //                {
        //                    string IdM = reader[0].ToString();
        //                    string Val = reader[7].ToString();
        //                    string comp = reader[8].ToString();
        //                    string street = reader[4].ToString();
        //                    street = street.Replace("\"", "");

        //                    if (IdM == "")
        //                        IdM = "0";
        //                    if (Val == "")
        //                        Val = "0";
        //                    if (comp == "")
        //                        comp = "0";

        //                    Val = Val.Replace(",", ".");

        //                    mcmd.CommandText = "insert into " + settlement + " values(" + IdM + ",\"" + reader[1] + "\",\"" + reader[3] + "\",\"" + street + "\",\"" + reader[5] + "\",\"" + reader[6] + "\"," + Val + "," + comp + ")";
        //                    mcmd.ExecuteNonQuery();
        //                    Console.WriteLine(mcmd.CommandText);
        //                }

        //            }
        //            mcon.Close();
        //            sqlConnection1.Close();
        //            cmd.CommandText = "Select id_company, name  from company";
        //            cmd.CommandType = CommandType.Text;
        //            cmd.Connection = sqlConnection1;
        //            sqlConnection1.Open();
        //            reader = cmd.ExecuteReader();
        //            mcon.Open();
        //            mcmd.Connection = mcon;
        //            mcmd.CommandType = CommandType.Text;
        //            while (reader.Read())
        //            {
        //                mcmd.CommandText = "insert into company values (" + reader[0] + ",\"" + reader[1].ToString().Replace("\"", "\\\"") + "\")";
        //                mcmd.ExecuteNonQuery();
        //            }
        //            sqlConnection1.Close();
        //            mcon.Close();
        //        //}

        //        //catch (Exception e)
        //        //{
        //        //    Console.WriteLine(e);
        //        //    Console.ReadLine();
        //        //}

        //    }
    }
}
