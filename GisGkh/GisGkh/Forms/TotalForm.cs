﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh.Forms
{
    public partial class TotalForm : Form
    {
        public TotalForm()
        {
            InitializeComponent();
        }

        public string SenderID
        {
            //get { return cbSender.SelectedValue.ToString(); }
            get { return customEdit1.EditValue.ToString(); }
        }

        public string FiasHouseGuid
        {
            //get { return cbHouse.SelectedValue.ToString(); }
            get { return customEdit2.EditValue.ToString(); }
        }

        void FillHouseDataSource()
        {
            //var SenderID1 = "5BD341B6-9AA1-4265-974B-034FC48ECC12,FF752088-3F72-4440-97F1-056F13626F35";

            var SIDpar =
                //"'" + 
                SenderID.Replace(" ", "")
                //+ "'"
                ;

            //cbHouse.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.SelectHouseBySenderID @senderID", new object[,] { { "@senderID", SIDpar } });
            //cbHouse.ValueMember = "FiasHouseGuid";
            //cbHouse.DisplayMember = "House_Address";

            customEdit2.Properties.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.SelectHouseBySenderID @senderID", new object[,] { { "@senderID", SIDpar } });
            customEdit2.Properties.ValueMember = "FiasHouseGuid";
            customEdit2.Properties.DisplayMember = "House_Address";
        }


        private void TotalForm_Load(object sender, EventArgs e)
        {
            //cbSender.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("select senderid,sendername from gis.provider order by 2");
            //cbSender.ValueMember = "senderid";
            //cbSender.DisplayMember = "sendername";

            customEdit1.Properties.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("select SenderID,sendername from gis.provider order by 2");
            customEdit1.Properties.ValueMember = "SenderID";
            customEdit1.Properties.DisplayMember = "sendername";


            FillHouseDataSource();           

            cbPeriod.Fill();
            periodFilter1.Fill();
        }


        List<string> ListByParameter(string list)
        {
            string[] s1 = list.Split(',');
            List<string> _list = s1.ToList<string>();
            return _list;
        }

        void PrepareIntegration()
        {
            var senderAll = chbSenderAll.Checked;

            if (chbTimer.Checked)
            {
                MrivcStatic.GetStateWaitMin = int.Parse(tbTimer.Text);
            }
            else
            {
                MrivcStatic.GetStateWaitMin = -1;
            }

            if (chbDorRes.Checked)
            {
                if (senderAll)
                {
                    var senderListRso = MrivcData.SenderListRso();

                    foreach (string senderID in senderListRso)
                    {
                        var exportSupplyContracts = new ExportSupplyContracts(senderID);
                        exportSupplyContracts.Execute();
                    }
                }
                else
                {
                    var exportSupplyContracts = new ExportSupplyContracts(SenderID.ToString());
                    exportSupplyContracts.Execute();
                }
            }

            var senderList = ListByParameter(SenderID);
            var houseGuidList = ListByParameter(FiasHouseGuid);

            if (chbPredPodDan.Checked)
            {
                Console.WriteLine("Предварительная подготовка данных");
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.PrepareData", new object[,] { { "sPeriod", periodFilter1.Period }, { "period", cbPeriod.Period } });
            }

            if (senderAll)
            {
                DoIntegration(cbPeriod.Period, periodFilter1.Period);
            }
            else if (cbAllHouse.Checked)
            {
                DoIntegration(senderList,null, cbPeriod.Period, periodFilter1.Period);
            }
            else
            {
                DoIntegration(senderList, houseGuidList, cbPeriod.Period, periodFilter1.Period);
            };

            //MrivcStatic.GetStateWaitMin = MrivcStatic.GetStateWaitMinDefault;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            var beginDate = DateTime.Now;
            Console.WriteLine($"Начата синхронизация за {beginDate}");

            PrepareIntegration();

            Console.WriteLine("Синхронизация завершена");
        }

        void DoIntegration( int period, int stratPeriod)
        {
            var args = new List<string>();

            if (chbHouse.Checked)
            {
                args.Add("-hd");
            }

            if (chbAccount.Checked)
            {
                args.Add("-ad");
            }

            if (chbPD.Checked)
            {
                args.Add("-pd");
            }

            if (chbMeter.Checked)
            {
                args.Add("-m");
            }

            if (chbMeasure.Checked)
            {
                args.Add("-mm");
            }

            if (chbService.Checked)
            {
                args.Add("-s");
            }

            var args1 = args.ToArray();

            MrivcStatic.Check = true;
            MrivcStatic.Await = false;
            MrivcStatic.RunConsole(args1, null, stratPeriod, period,null);

            //MrivcStatic.HouseIteration(commandList, senderID, false);
        }

        void DoIntegration(List<string> senderID, List<string> houseGuidList, int period, int stratPeriod)
        {
            var commandList = new List<ISingleHouseCommand>();

            var importMServices = new ImportMunicipalServices(senderID[0]);
            var importAServices = new ImportAdditionalServices(senderID[0]);
            var importHouse = new ImportHouseData(senderID[0]);
            var importAccount = new ImportAccountData(senderID[0]);
            //var importPaymentDocument = new ImportPaymentDocument(senderID, period);
            var importPaymentDocument2 = new ImportPaymentDocument2(senderID[0], period);
            var importMeter = new ImportMeteringDevice(senderID[0]);
            var importMeasure = new ImportMeasureData(senderID[0]);

            var args = new List<string>();

            if (chbHouse.Checked)
            {
                commandList.Add(importHouse);
                args.Add("-hd");
            }

            if (chbAccount.Checked)
            {
                commandList.Add(importAccount);
                args.Add("-ad");
            }

            if (chbPD.Checked)
            {
                commandList.Add(importPaymentDocument2);
                args.Add("-pd");
            }

            if (chbMeter.Checked)
            {
                commandList.Add(importMeter);
                args.Add("-m");
            }

            if (chbMeasure.Checked)
            {
                commandList.Add(importMeasure);
                args.Add("-mm");
            }

            if (chbService.Checked)
            {
                //importMServices.Execute();
                //importAServices.Execute();
                args.Add("-s");
            }

            var args1 = args.ToArray();

            MrivcStatic.Check = true;
            MrivcStatic.Await = false;
            MrivcStatic.RunConsole(args1, senderID, stratPeriod, period, houseGuidList);

            //MrivcStatic.HouseIteration(commandList, senderID, false);
        }

        //void DoIntegration(List<string> senderID, string fiasHouseGuid, int period, int stratPeriod)
        //{
            
        //        //var exportSupplyContracts = new ExportSupplyContracts(senderID);
        //        var importMServices = new ImportMunicipalServices(senderID[0]);
        //        var importAServices = new ImportAdditionalServices(senderID[0]);
        //        var importHouse = new ImportHouseData(senderID[0]) { FiasHouseGuid = fiasHouseGuid };
        //        var importAccount = new ImportAccountData(senderID[0]) { FiasHouseGuid = fiasHouseGuid };
        //        //var importPaymentDocument = new ImportPaymentDocument(senderID, period) { FiasHouseGuid = fiasHouseGuid };
        //        var importPaymentDocument2 = new ImportPaymentDocument2(senderID[0], period) { FiasHouseGuid = fiasHouseGuid };
        //        var importMeter = new ImportMeteringDevice(senderID[0]) { FiasHouseGuid = fiasHouseGuid };
        //        var importMeasure = new ImportMeasureData(senderID[0]) { FiasHouseGuid = fiasHouseGuid };


        //        //if (chbDorRes.Checked)
        //        //{
        //        //    exportSupplyContracts.Execute();
        //        //}

        //        //if (chbPredPodDan.Checked)
        //        //{
        //        //    DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.PrepareData", new object[,] { { "speriod", stratPeriod }, { "period", period } });
        //        //}

        //        if (chbService.Checked)
        //        {
        //            importMServices.Execute();
        //            importAServices.Execute();
        //        }

        //        if (chbHouse.Checked)
        //        {
        //            importHouse.Execute();
        //        }

        //        if (chbAccount.Checked)
        //        {
        //            importAccount.Execute();
        //        }

        //        if (chbPD.Checked)
        //        {
        //            var periods = DAccess.DataModule.GetDataTableByQueryCommand1("select period from dbo.period where period between " + stratPeriod + " and " + period + "");

        //            foreach (DataRow periodRow in periods.Rows)
        //            {
        //                importPaymentDocument2.Period = int.Parse(periodRow[0].ToString());
        //                importPaymentDocument2.Execute();
        //            }


        //        }


        //        if (chbMeter.Checked)
        //        {
        //            importMeter.Execute();
        //        }

        //        if (chbMeasure.Checked)
        //        {
        //            importMeasure.Execute();
        //        }

         
        //}

        private void chbPdOnly_CheckedChanged(object sender, EventArgs e)
        {
            
            
        }

        private void btnUnselectAll_Click(object sender, EventArgs e)
        {
            var chBoxes = new List<CheckBox>() { chbService, chbHouse, chbAccount, chbPD, chbMeter, chbMeasure };
            foreach (CheckBox item in chBoxes)
            {
                item.CheckState = CheckState.Unchecked;
            }
        }

        private void chbTimer_CheckedChanged(object sender, EventArgs e)
        {
            tbTimer.Enabled = chbTimer.Checked;
        }

        private void chbSenderAll_CheckedChanged(object sender, EventArgs e)
        {
            chbTimer.Checked = chbSenderAll.Checked;
            //cbSender.Enabled = !chbSenderAll.Checked;
            //gbHouse.Enabled = !chbSenderAll.Checked;
            customEdit1.Enabled = !chbSenderAll.Checked;
            if (chbSenderAll.Checked) { cbAllHouse.Checked = true; };
            groupBox1.Enabled = !chbSenderAll.Checked;
            
        }

        private void cbAllHouse_CheckedChanged(object sender, EventArgs e)
        {
            //cbHouse.Enabled = !cbAllHouse.Checked;
            customEdit2.Enabled = !cbAllHouse.Checked;
        }

        private void cbSender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbSender_SelectionChangeCommitted(object sender, EventArgs e)
        {
            FillHouseDataSource();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(customEdit1.EditValue.ToString());



        }

        private void customEdit1_EditValueChanged(object sender, EventArgs e)
        {
            FillHouseDataSource();
        }
    }
}
