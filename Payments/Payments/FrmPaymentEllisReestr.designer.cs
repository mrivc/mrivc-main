﻿namespace Payments
{
    partial class FrmPaymentEllisReestr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPaymentEllisReestr));
            this.gridPaymentEllisReestr1 = new Payments.Controls.GridPaymentEllisReestr();
            this.SuspendLayout();
            // 
            // gridPaymentEllisReestr1
            // 
            this.gridPaymentEllisReestr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPaymentEllisReestr1.Location = new System.Drawing.Point(0, 0);
            this.gridPaymentEllisReestr1.Name = "gridPaymentEllisReestr1";
            this.gridPaymentEllisReestr1.Size = new System.Drawing.Size(925, 685);
            this.gridPaymentEllisReestr1.TabIndex = 0;
            // 
            // FrmPaymentEllisReestr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 685);
            this.Controls.Add(this.gridPaymentEllisReestr1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPaymentEllisReestr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбрузка реестра платежей";
            this.Load += new System.EventHandler(this.FrmPaymentEllisReestr_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.GridPaymentEllisReestr gridPaymentEllisReestr1;
    }
}