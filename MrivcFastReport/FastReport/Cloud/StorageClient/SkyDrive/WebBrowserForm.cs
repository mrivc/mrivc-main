using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using FastReport.Forms;

namespace FastReport.Cloud.StorageClient.SkyDrive
{
    /// <summary>
    /// Represents form of the web browser.
    /// </summary>
    public partial class WebBrowserForm : BaseDialogForm
    {
        #region Fields

        private string url;
        private string authCode;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Gets obtained authorization code.
        /// </summary>
        public string AuthCode
        {
            get { return authCode; }
        }

        #endregion // Properties

        #region Constructors

        /// <inheritdoc/>
        public WebBrowserForm(string url)
        {
            InitializeComponent();
            this.url = url;
            authCode = "";
            wbBrowser.Navigated += new WebBrowserNavigatedEventHandler(wbBrowser_Navigated);
        }

        #endregion // Constructors

        #region Events Handlers

        private void WebBrowserForm_Shown(object sender, EventArgs e)
        {
            wbBrowser.Navigate(url);
        }

        private void wbBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (e.Url.AbsoluteUri.Contains("code="))
            {
                authCode = Regex.Split(Regex.Split(e.Url.AbsoluteUri, "code=")[1], "&")[0];
                this.Close();
            }
        }

        #endregion // Events Handlers
    }
}