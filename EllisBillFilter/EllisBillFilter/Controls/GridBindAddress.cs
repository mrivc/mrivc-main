﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using System.Text.RegularExpressions;
using System.Threading;

namespace EllisBillFilter.Controls
{
    public partial class GridBindAddress : UserControl
    {
        private bool addLinkColumns;
        private string databaseName = "Hlam";

        public GridBindAddress()
        {
            InitializeComponent();

            cbBindAccountType.SelectedIndex = 0;
            dabaBaseTablePicker1.TablesDropDown.EditValueChanged += TablesDropDown_EditValueChanged;
        }

        public string TableName
        {
            get;
            private set;
        }

        public DataTable TableData
        {
            get
            {
                return baseGrid1.DataSource as DataTable;
            }
        }

        public void LoadDataTablePicker()
        {
            dabaBaseTablePicker1.LoadData(databaseName);
            tableColumnsDestinationPicker1.LoadData(DAccess.DataModule.ConnectionInfo.InitialCatalog, "Account");
        }

        public void LoadData(string tableName, bool addLinkColumns = false)
        {
            this.TableName = tableName;
            this.addLinkColumns = addLinkColumns;

            dabaBaseTablePicker1.LoadData(databaseName, $"[dbo].[{tableName}]");            
            //загрузка данных в грид происходит в событии cbBindAccountType_SelectedIndexChanged => FillGrid()
        }

        public void FillGrid()
        {
            this.TableName = dabaBaseTablePicker1.SelectedTable;

            if (string.IsNullOrEmpty(TableName))
                return;

            if(addLinkColumns)
                DAccess.DataModule.AddLinkAccountColumns($"[{databaseName}].{TableName}");

            gridBindAddressView.Columns.Clear();
            baseGrid1.DataSource = null;
            baseGrid1.DataSource = DAccess.DataModule.SelectDataTable(TableName, databaseName);
        }

        public void BindAccounts()
        {
            switch (cbBindAccountType.SelectedIndex)
            {
                case 0:
                    BindAccountsByAddress();
                    break;
                case 2:
                    BindAccountsByAccount();
                    break;
                default:
                    throw new ApplicationException("Данный метод соединения не реализован.");
                    break;
            }

            FillGrid();
        }

        public void BindAccountsByAddress()
        {
            int numAddressCountParts = GetAddressColumnMaxIndex();
            if (numAddressCountParts == -1)
            {
                throw new ApplicationException("Не найдено столбцов содержащих адресную информацию. Столбцы с адресом должны иметь наименования: Address1, Address2, Address3, Address4. Максимально может быть 4 столбца, минимально 1. Каждый столбец должен содержать адресную информацию по порядку, например Address1 = 'Мурманск', Address2 = 'Старостина', Address3 = '61к2', Address4 = '57' или весь адрес может быть в одном столбце, например Address1 = 'Мурманск, Старостина, 61к2, 57'.");
            }

            DAccess.DataModule.TryBindAccountsByAddress($"[{databaseName}].{TableName}", numAddressCountParts, txtStreetSeparator.Text, txtHouseSeparator.Text, txtBuildingSeparator.Text,
                txtBlockSeparator.Text, txtFlatSeparator.Text, txtRoomSeparator.Text);
        }

        public void BindAccountsByAccount()
        {
            if (string.IsNullOrEmpty(tableColumnsSourcePicker1.SelectedColumn) || string.IsNullOrEmpty(tableColumnsDestinationPicker1.SelectedColumn))
            {
                throw new ApplicationException("Необходимо выбрать столбцы для соединения.");
            }

            DAccess.DataModule.TryBindAccountsByAccount($"[{databaseName}].{TableName}", tableColumnsSourcePicker1.SelectedColumn, tableColumnsDestinationPicker1.SelectedColumn);
        }

        private void btnCheckAddress_Click(object sender, EventArgs e)
        {
            string address = DAccess.DataModule.CheckBindAddressAccounts(txtAccount.Text, txtStreetSeparator.Text, txtHouseSeparator.Text, txtBuildingSeparator.Text,
                txtBlockSeparator.Text, txtFlatSeparator.Text, txtRoomSeparator.Text);
            MessageBox.Show(address, "Проверка адреса", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private int GetAddressColumnMaxIndex()
        {
            var columns = this.TableData.Columns.OfType<DataColumn>().Where(c => c.ColumnName.ToLower().Contains("address") && Regex.Match(c.ColumnName, @"\d+$").Length > 0);
            if (columns == null || columns.Count() == 0)
                return -1;

            var col = columns.OrderByDescending(c => c.ColumnName).Take(1).Single();

            return int.Parse(Regex.Match(col.ColumnName, @"\d+$").Value);
        }

        private void TablesDropDown_EditValueChanged(object sender, EventArgs e)
        {
            FillGrid();
            tableColumnsSourcePicker1.LoadData(databaseName, dabaBaseTablePicker1.SelectedTable.Replace("[dbo].[","").Replace("]",""));
        }

        private void cbBindAccountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbBindAccountType.SelectedIndex)
            {
                case 0:
                    panelBindByAddressSettings.Enabled = true;
                    panelBindByAccountSettings.Enabled = false;
                    break;
                case 1:
                    panelBindByAddressSettings.Enabled = true;
                    panelBindByAccountSettings.Enabled = false;
                    break;
                case 2:
                    panelBindByAddressSettings.Enabled = false;
                    panelBindByAccountSettings.Enabled = true;
                    break;
                case 3:
                    panelBindByAddressSettings.Enabled = false;
                    panelBindByAccountSettings.Enabled = true;
                    break;
                default:
                    panelBindByAddressSettings.Enabled = false;
                    panelBindByAccountSettings.Enabled = true;
                    break;
            }
        }
    }
}
