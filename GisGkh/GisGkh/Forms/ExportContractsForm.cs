﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class ExportContractsForm : Form
    {
        public string SenderID
        {
            get { return senderIdOnlyFilter.SenderID; }
        }

        public ExportContractsForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var beginDate = DateTime.Now;
            Console.WriteLine($"Начата синхронизация за {beginDate}");

            MrivcStatic.GetStateWaitMin = -1;

            var exportContracts = new ExportContracts(SenderID);
            var ds = MrivcStatic.HouseIteration(exportContracts, false);

            var exportContractsStatus = new ExportContractsStatus(SenderID);
            exportContractsStatus.Execute();

            MrivcStatic.GetStateWaitMin = MrivcStatic.GetStateWaitMinDefault;

            Console.WriteLine("Синхронизация завершена");
        }

        private void ExportContractsForm_Load(object sender, EventArgs e)
        {
            senderIdOnlyFilter.Fill();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var beginDate = DateTime.Now;
            Console.WriteLine($"Начата синхронизация за {beginDate}");

            MrivcStatic.GetStateWaitMin = -1;

            var exportSupplyContracts = new ExportSupplyContracts(SenderID);
            //MrivcStatic.HouseIteration(exportSupplyContracts, false);
            exportSupplyContracts.Execute();

            MrivcStatic.GetStateWaitMin = MrivcStatic.GetStateWaitMinDefault;

            Console.WriteLine("Синхронизация завершена");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var importCharter = new ImportCharterData(SenderID);
            importCharter.Execute();
        }
    }
}
