﻿using MrivcControls.Data;
using MrivcControls.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XadesDemo;
using XadesDemo.Configurations.Sections;

namespace gis
{
    class Program
    {
        

        static void Main(string[] args)
        {
            DAccess.DataModule.Connection1 = new SqlConnection(MrivcData.ConnectionString);

            bool syncHouseData = false;
            bool syncAccountData = false;
            bool syncNsi = false;
            bool syncServices = false;
            bool sendServices = false;
            bool syncPaymentDocument = false;
            bool syncMeterData = false;
            bool syncMeterMeasure = false;
            bool syncSenderData = false;
            bool withdrawPaymentDocument = false;

            bool sendAdminReport = false;
            bool sendReport = false;

            foreach (var arg in args)
            {
                switch (arg)
                {
                    case "-si":
                        syncSenderData = true;
                        break;
                    case "-hd":
                        syncHouseData = true;
                        break;
                    case "-ad":
                        syncAccountData = true;
                        break;
                    case "-nsi":
                        syncNsi = true;
                        break;
                    case "-s":
                        syncServices = true;
                        break;
                    case "-ss":
                        sendServices = true;
                        break;
                    case "-pd":
                        syncPaymentDocument = true;
                        break;
                    case "-m":
                        syncMeterData = true;
                        break;
                    case "-mm":
                        syncMeterMeasure = true;
                        break;
                    case "-sar":
                        sendAdminReport = true;
                        break;
                    case "-sr":
                        sendReport = true;
                        break;
                    case "-wpd":
                        withdrawPaymentDocument = true;
                        break;
                }
            }

            if (syncSenderData || syncNsi || syncServices || sendServices || syncHouseData || syncAccountData || syncMeterData || syncMeterMeasure || syncPaymentDocument || withdrawPaymentDocument)
            {
                var beginDate = DateTime.Now;

                DAccess.DataModule.ExecuteNonQueryCommand1("insert GIS.SyncBeginDateLog (SyncBeginDate,SyncSenderData,SyncNsi,SyncServices,SyncHouseData,SyncAccountData,SyncMeterData,SyncMeterMeasure,SyncPaymentDocument) values (@bd,@si,@n,@s,@hd,@ad,@md,@mm,@pd)", new object[,] { { "@bd", beginDate }, { "@si", syncSenderData }, { "@n", syncNsi }, { "@s", syncServices }, { "@hd", syncHouseData }, { "@ad", syncAccountData }, { "@md", syncMeterData }, { "@mm", syncMeterMeasure }, { "@pd", syncPaymentDocument } });

                Console.WriteLine($"Начата синхронизация за {beginDate}");

                //ConfigurationManager.OpenExeConfiguration(@"C:\mrivcprojects\Xades2\gis\bin\Debug\gis.exe");

                var siError = false;
                var nsiError = false;

                beginDate = DateTime.Now;

                try
                {
                    var exportSenderID = new ExportOrgRegistry();
                    if (syncSenderData)
                    {
                        exportSenderID.Execute();
                    }

                    siError = exportSenderID.HasErrors;

                 
                }
                catch (Exception ex)
                {
                    siError = true;
                    MrivcStatic.WriteLog(ex);

                }

                var SenderList = MrivcData.SenderList();

                beginDate = DateTime.Now;

                if (!siError)
                {
                    try
                    {
                        var exportNsiList = new ExportNsiList();
                        if (syncNsi)
                        {
                            var nsiRegNumbers = MrivcStatic.GisConfig.ExportNsiRegistryNumbers;
                            exportNsiList.NsiRegistryNumbers = nsiRegNumbers;
                            exportNsiList.Execute();
                        }

                        nsiError = exportNsiList.HasErrors;

                    }
                    catch (Exception ex)
                    {
                        nsiError = true;
                        MrivcStatic.WriteLog(ex);

                    }

                    MrivcStatic.WriteLogToConsole(beginDate);
                }

                if (syncServices)
                {
                    DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.FillServices2");
                }

                foreach (string senderID in SenderList)
                {

                    if (!nsiError && !siError)
                    {
                        var serviceError = false;

                        try
                        {
                            var importService = new ImportMunicipalServices(senderID);
                            var importAddService = new ImportAdditionalServices(senderID);
                            if (syncServices)
                            {
                                importService.AddToReport = true;
                                importService.Execute();


                                importAddService.AddToReport = true;
                                importAddService.Execute();
                            }
                            serviceError = importService.HasErrors || importAddService.HasErrors;
                        }
                        catch (Exception ex)
                        {
                            serviceError = true;
                            MrivcStatic.WriteLog(ex);
                        }

                        var singleCommandList = new List<ISingleHouseCommand>();

                        if (syncHouseData)
                            singleCommandList.Add(new ImportHouseData(senderID) { AddToReport = true });

                        if (syncAccountData)
                            singleCommandList.Add(new ImportAccountData(senderID) { AddToReport = true });

                        if (syncMeterData && !serviceError)
                            singleCommandList.Add(new ImportMeteringDevice(senderID) { AddToReport = true });

                        if (syncMeterMeasure && !serviceError)
                            singleCommandList.Add(new ImportMeasureData(senderID) { AddToReport = true });

                        int period = (int)DAccess.DataModule.ExecuteScalarQueryCommand1("select dbo.work_period()-1");

                        if (syncPaymentDocument)
                        {
                            singleCommandList.Add(new ImportPaymentDocument(senderID, period) { AddToReport = true });
                        }

                        if (withdrawPaymentDocument)
                        {
                            singleCommandList.Add(new WithdrawPaymentDocument(senderID, period) { AddToReport = true });
                        }

                        MrivcStatic.HouseIteration(singleCommandList, senderID, false);
                    }

                    //if (sendReport)
                    //{
                    //    try
                    //    {
                    //        MrivcStatic.SendCommandReport(senderID, beginDate);

                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        FileHelper.WriteLog("Ошибка при отправке отчетов по email: " + ex.Message);
                    //    }

                    //    //if (syncMeterData || syncPaymentDocument)
                    //    //{
                    //    //    MrivcStatic.SendServiceList(senderID);
                    //    //}
                    //}


                }

                if (sendAdminReport)
                    MrivcStatic.SendCommandReportToAdmin(beginDate);

                Console.WriteLine("Синхронизация завершена");
                Console.ReadLine();
            }
            else
            {
                var argsHelp = @"-si -- па
- s--
- hd--
- ad--
- pd--
- m--
- mm--



gis.exe - si - s - hd - ad--

gis.exe - pd--

gis.exe - m--mm--
";
                //byte[] bytes = Encoding.Default.GetBytes();
                //string argsHelp = Encoding.GetEncoding(866).GetString(bytes);

                Console.WriteLine(argsHelp);
                Console.ReadLine();
            }
        
    }
    }
}
