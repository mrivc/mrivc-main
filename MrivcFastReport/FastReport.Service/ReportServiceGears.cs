﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Configuration;
using FastReport;
using FastReport.Utils;
using System.Collections;
using FastReport.Data;

namespace FastReport.Service
{
    public partial class ReportService : IFastReportService
    {
        public List<GearItem> GetGearList()
        {
            List<GearItem> list = new List<GearItem>();

            string gearsConf = ConfigurationManager.AppSettings["FastReport.Gear"];
            string[] listGears = gearsConf.Split(',');

            foreach (string gearName in listGears)
            {
                switch (gearName)
                {
                    case "FPX":
                        list.Add(RegisterFPX());
                        break;
                    case "PDF":
                        list.Add(RegisterPDF());
                        break;
                    case "DOCX":
                        list.Add(RegisterDOCX());
                        break;
                    case "XLSX":
                        list.Add(RegisterXLSX());
                        break;
                    case "PPTX":
                        list.Add(RegisterPPTX());
                        break;
                    case "ODS":
                        list.Add(RegisterODS());
                        break;
                    case "ODT":
                        list.Add(RegisterODT());
                        break;
                    case "MHT":
                        list.Add(RegisterMHT());
                        break;
                    case "DBF":
                        list.Add(RegisterDBF());
                        break;
                    case "XML":
                        list.Add(RegisterXML());
                        break;
                    case "TXT":
                        list.Add(RegisterTXT());
                        break;
                    case "RTF":
                        list.Add(RegisterRTF());
                        break;
                    case "HTML":
                        list.Add(RegisterHTML());
                        break;
                }
            }
            return list;
        }

        private Dictionary<string, string> GetDefaultProp()
        {
            Dictionary<string, string> prop = new Dictionary<string, string>();
            prop.Add("Zoom", "1.0");
            prop.Add("PageRange", "All");
            prop.Add("PageNumbers", "");
            return prop;
        }

        private GearItem RegisterFPX()
        {
            GearItem item = new GearItem();
            item.Name = "FPX";
            item.Extension = ".fpx";
            item.MimeType = "application/fpx";
            return item;
        }

        private GearItem RegisterPDF()
        {
            GearItem item = new GearItem();
            item.Name = "PDF";
            item.Extension = ".pdf";
            item.MimeType = "application/pdf";
            item.Properties = GetDefaultProp();
            // add properties 
            item.Properties.Add("EmbeddingFonts", "true");
            item.Properties.Add("PdfA", "false");
            item.Properties.Add("Background", "true");
            item.Properties.Add("PrintOptimized", "true");
            item.Properties.Add("Title", "");
            item.Properties.Add("Author", "");
            item.Properties.Add("Subject", "");
            item.Properties.Add("Keywords", "");
            item.Properties.Add("Creator", "FastReport");
            item.Properties.Add("Producer", "FastReport.NET");
            return item;
        }

        private GearItem RegisterXLSX()
        {
            GearItem item = new GearItem();
            item.Name = "XLSX";
            item.Extension = ".xlsx";
            item.MimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterDOCX()
        {
            GearItem item = new GearItem();
            item.Name = "DOCX";
            item.Extension = ".docx";
            item.MimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterPPTX()
        {
            GearItem item = new GearItem();
            item.Name = "PPTX";
            item.Extension = ".pptx";
            item.MimeType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterODS()
        {
            GearItem item = new GearItem();
            item.Name = "ODS";
            item.Extension = ".ods";
            item.MimeType = "application/vnd.oasis.opendocument.spreadsheet";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterODT()
        {
            GearItem item = new GearItem();
            item.Name = "ODT";
            item.Extension = ".odt";
            item.MimeType = "application/vnd.oasis.opendocument.text";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterMHT()
        {
            GearItem item = new GearItem();
            item.Name = "MHT";
            item.Extension = ".mht";
            item.MimeType = "multipart/related application/x-mimearchive";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterCSV()
        {
            GearItem item = new GearItem();
            item.Name = "CSV";
            item.Extension = ".csv";
            item.MimeType = "text/csv";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterDBF()
        {
            GearItem item = new GearItem();
            item.Name = "DBF";
            item.Extension = ".dbf";
            item.MimeType = "application/dbf";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterXML()
        {
            GearItem item = new GearItem();
            item.Name = "XML";
            item.Extension = ".xml";
            item.MimeType = "text/xml";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterTXT()
        {
            GearItem item = new GearItem();
            item.Name = "TXT";
            item.Extension = ".txt";
            item.MimeType = "text/plain";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterRTF()
        {
            GearItem item = new GearItem();
            item.Name = "RTF";
            item.Extension = ".rtf";
            item.MimeType = "text/rtf";
            item.Properties = GetDefaultProp();
            // add properties 
            return item;
        }

        private GearItem RegisterHTML()
        {
            GearItem item = new GearItem();
            item.Name = "HTML";
            item.Extension = ".zip";
            item.MimeType = "application/zip";
            item.Properties = GetDefaultProp();
            item.Properties.Add("Layers", "false");
            item.Properties.Add("SinglePage", "false");
            // add properties 
            return item;
        }
    }
}
