Unit uTypes;

Interface

Uses Controls, uPropertyCollection, ADODB,SyncObjs,classes,Messages;
type
 TByteSet=set of byte;
Const
  WM_MDICHILD_CLOSE_COMMAND=WM_CLOSE; // �� CM_RELEASE;

  Min_SQL_Year=1753; //����������� ����, ������� ������������ MS SQL
  Max_SQL_Year=2999; //������������ ����, ������� ������������ MS SQL

  // ���� �����  sql �������
  SQL_system_Type_id_Virtual=-1;
  SQL_system_Type_id_Datetime=61;
  SQL_system_Type_id_Varchar=167;
  SQL_system_Type_id_Int=56;
  SQL_system_Type_id_BigInt=127;
  SQL_system_Type_id_Money=60;
  SQL_system_Type_id_Bit=104;


  WORKGROUP_DEVELOPER='ELLIS';// ������� ������ ���������� 

  SFilterSettings = 'EllisFilterSettings';
  SFilterDialog = '��������� ������� (*.afl)|*.afl';
  SFilterDialogTxt = '������ ��������������� (*.txt)|*.txt';
  SFilterDialogDefaultExt = 'afl';

  SModule = '������';
  SSubMenu = '�������';
  SSubMenuFiller = '����������� �������';
  SStaticSubMenu = '����������� �������';

// ������ � ������ ������, ����������� � ���� // ����������
  iMaxThumbWidth = 100;
  iMaxThumbHeight = 75;

  // ����� ����������� ���������, ����������� ��������� ������� � ������ ��
  // ��������, �� �������� � ����� �� ������� �����,
  // � ���� ������ ����� �� �� �������� � �������� ����������� ��� ������
  Account_RegExp_Format='[A-Z,0-9,-]';// ��� �������� ����� ���������� {0,25}

  fr3EngineDLL='fr3reporting.dll';
  // ���� �� Task
  ID_Task_TechPassport    =1;//'����������';
  ID_Task_Rent            =3;//'����������';
  ID_Task_Housing         =5;//'�������';

  // ���� �� Parameter_Owner
  ID_Parameter_Owner_Common     =21;
  ID_Parameter_Owner_Company    =13;
  ID_Parameter_Owner_Provider   =23;
  ID_Parameter_Owner_House      =4;
  ID_Parameter_Owner_Flat       =2;
  ID_Parameter_Owner_Account    =1;

  ID_Parameter_Owner_House_Agreement=29;
  ID_Parameter_Owner_House_Agreement_Attachment=30;

  // ���� �� Action_Category
  Action_Category_Default         =0;
  Action_Category_MassOperation   =1;
  Action_Category_Outer_Module    =2;
  Action_Category_Report          =3;


  // ���� �������� �����������
  DA_AccountMain_DocOwnerParameter  =39;
  DA_Registration_DocOwnerParameter =40;
  DA_LegalCard_DocOwnerParameter    =41;
  DA_TPCard_DocOwnerParameter       =43;
  DA_TPCard_Top_Area                =44;
//  DA_TPCard_Constructive_Element_Parameters=45;// �������
  DA_TPCard_Flat_Parameters         =46;
  DA_TPCard_Common_Parameters       =47;
  DA_TPCard_Other_Parameters        =48;
  DA_TPCard_Flat_Common_Parameters  =49;
  DA_TPCard_Flat_Common_Account_Parameters  =50;
  DA_Charge_House_Parameter         =51;
  DA_TPCard_Flat_And_Account_Parameters  =53;

  //������ �����, ������� ������ ��������� � ������ ������
  ADS_Except_Filter_Field_List = '''ID_Request'',''ID_Person'',''ID_Legal_Face'',''ID_Settlement'',''ID_Street'',''ID_Flat'',''ID_House''';

  //�� ������ "����� ����� ��������" ����������� ����� ����� ����
//  flg_New_Split_Form = True;
  //id_bill_type ��� ����������� �� ����������� ���� 
  cBillTypeSplitSumPriority = 300;

  //�������� ������ ���������� �������� �����
  flg_Account_Division_Master = true;

  //������ ������ ����� �� ���������
  default_Footer_Format = ',0.00';
  // ����  �������� ������ ���, ��� ������� �������� ������ ������
  ADS_Status_Cancellable:TByteSet=[0,1,2];
  // ���� �������� ������ ��� , ��� ������� ����� ��������� ������ "���������"
  ADS_Status_Completable:TByteSet=[3];

{$REGION '<������ ��������> ����� ������ ������ ����� ���������� "��������"'}
    // ������ � �������� �����������
    flg_ProviderGroups = true;
    // ������ ���������� ����
    flg_Fine_Enabled = true;
    // ��������� �������
    flg_Period_Parameter_Enabled = True;
    // ��������� ���� ���� ��� ����� ��������
    flg_FineManual = True;
    //������ � �������� ����� � �������
    flg_HouseFlatGroups = True;
    // ���������� ����������� ���������� ����� �������� ����������
    flg_Calculation_Recalc_Derived_NoDependence = true;
    // ���������� ����� ����� ������������� � ���������� ���  ���������� ��
    flg_NewAccountOwnersInterface = True;
    // �������������� ���������� ��������
    flg_CompanyParameters = True;
    // ���������� ����������� �����
    flg_KladrUpdate=true;
    // ��������� �������� �����-�����
    flg_Barcode =true;
    // ������� ������ �� ����������
    flg_ProviderSaldoMoving=true;
    {$IFDEF PASSPORT}
    flg_PassportCard=true;
    {$ENDIF}
    // �������/���. ������ �� ����� (����� ��������)
//    flg_New_House_Charge_Income = True;
    // ������/�������
    flg_Districts = True;
//    // ���������� ������� �����
//    flg_Benefit_Disable = True;
    // ������� ��������� ������������ ������
    flg_Person_Change_History=true;
    // ������ � �� ������
    flg_Legal=true;
    // ������ � ������������ �� ���������������� �����
    flg_DebtAgreements=true;
    // ���������� ��������� ��� ����� ������������ �������� ����������� �����
    flg_NewPassportCardInterface =true;


{$ENDREGION}
  // ����� � ��������� ���� "�������� �� ��������"
  flg_MultiplyNormative=false;

  // ���� ������ ������������ �������
  flg_FlatCardReports =false;//����� ���� ����������
  //��������� ������ ���������
  flg_ForeignPays=false;
  // ������ ��� �������� - ��� ��������� ����� ����������� ������� ������ ������� ����
  flg_NewRegistCurrDate = True;
  // ��������� ����������� �� ����������� ��� �������� � �������
  flg_Provider_Group_Splitting=true;


  // ��������� ����������� ����� �����������
  flg_SplitParams=false;

  //����������� ������
  flg_LawService_Enable=0=0;
  // ������� �������� � �������� ������
  flg_OverPay_Transfer=true;

  //�����, ������������� �������� ������
  //flg_New_Address_Tree = true;
  // ���������� ��� ��� ����� ���� ��������� ������
    flg_Show_System_Log = true; // ����� � ���������� 2.3
    //������� � �������
    flg_Modules_Signs = true;


  // �������� ���������� �������� � ��������
  flg_Charge_Mass_Balance_Prepare = true;
  //������� ������ � ��������
  flg_Charge_Balance_Transfer =true;

  // �������� �������� �������� ������� ���������� ���������
  flg_BadFormulaEditor=true;

  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
  //����� ����������� ������, �� �������� ����� ��������, ����� ����������
  //���������� TCxGrid ��� ���. �� ������ ������ ����������� ���� ������
  //��������� ���� ������. kaa 2010-09-24
  //��. TSystemSubSystem.Get_Action_Input_Parameters - � ��� ����� ����������!
  flg_Do_Not_Log_TCxGrid = false;
  //������������ ������ ������������ ����� � ������� BLOB_VALUE
  Max_Blob_File_Size = 104857600;//100 ��

  flg_Auto_EXE_Update = true;

  // ����������� ��������� ������ � ����� �������������� ������ ���
  flg_ADS_EDIT_MODULES=false;

  //�����������. �������� � ���������� 6.30
  flg_Scheduler = true;
  //������ ������
  flg_Schudule_Only_Admin = true;

  // �������� ����������, �� ������������
  flg_USE_MASS_RECALCULATE=false;

  //��������� �������������� �������� �� ���������
  // ��� ��������� �� ������ ������������� _acMeterTypeEdit � uDIctionaryAdministration
  flg_MeterTypeActionVisible=True;


Type
  TDateDynArray = Array Of TDate;
  TADODatasetDynArray = Array Of TADODataSet;
  TProcedureOfObject = Procedure Of Object;
  TBoolFunctionOfObject = Function: boolean Of Object;
  TStringFunctionOfObject = Function: String Of Object;
  TExecuteProcedure = Procedure(Options: TPropertyCollection);
  TExecuteEventHandlerFunction =function (var AOptions: TPropertyCollection):Integer;
  TGetVariablesProcedure=procedure(AReportFileName:ShortString; AVarList:TMemoryStream);
  TExecuteCOMProcedure = Procedure(AEllisCOMServer : IUnknown);
  TThreadExecuteProcedure = Procedure(Sender: TObject; Params: Pointer) Of Object;
  TThreadFinishProcedure = Procedure(Sender: TObject) Of Object;

  TParameterType = (ptUnknown, ptInteger, ptFloat, ptString, ptDate, ptDictionary, ptFileName, ptOneString);
  // ��� ��� ���������� ������ ������ � ���� �� ���� ��� ����������, ��������������� � ���������
  TDlgFormMode = (dfmAdd, dfmEdit, dfmView);

  // ��� �������(������� Calculation_Type)
  TCalculation_type=(ctNone=-1, ctAll,ctTariff,ctBenefit);

  //------------------------------------------------------------------------------
  // ������ ��������
Type
  TFilterObject = (fo_Account, fo_Flat, fo_House, fo_Street, fo_Settlement,
    fo_Derive_Company, fo_District, fo_Maintain_Company);

  //������� ����������
  TTPJournalObject = (jo_charge, jo_repair, jo_check);

  TAddressTreeEvent  = procedure (ACode, ALevel: Integer; ANew: Boolean = true) of object;
  TAddressTreeLevel = TFilterObject;
  TAddressTreeLevelNames = Array[TAddressTreeLevel] Of String;
  TAddressTreeLevelCodes = Array[TAddressTreeLevel] Of Integer;
  //��������� �������� ����� ����������, � ������ ���� ��� �� ������ �������
  //ADirection 1 = ���������, -1 - ����������
  TNextPrevControlProc = procedure(Sender: TObject; ADirection: Integer) of object;
Const
  atl_Account = fo_Account;
  atl_Flat = fo_Flat;
  atl_House = fo_House;
  atl_Street = fo_Street;
  atl_Settlement = fo_Settlement;
  //Deprecated, ������������ ������� �������� � ��
  AddressTreeLevelNames: TAddressTreeLevelNames = ('������� ����', '��������',
    '���', '�����', '��������� �����', '����������� ��������', '�������', '������������� ��������');
  // ������������  ����� ID_Parameter_Owner �� ������� Parameter_Owner
  AddressTreeLevelCodes: TAddressTreeLevelCodes = (1, 2, 4, 16, 17, 13, 22, 13);


  // ��������������
Function ID_Parameter_Owner_To_TAddressTreeLevel(AValue: Integer): TAddressTreeLevel;
Function FilterObjectToAddressTreeLevel(FilterObject: TFilterObject): TAddressTreeLevel;
// ��������� ������ ����� ���� 1,2,4,16 ��� ������� � ������ ���� where ID_Parameter_Owner in (1,2,4,17)
Function AddressTreeLevelCodesDelimitedStr: String;
//------------------------------------------------------------------------------
// �������,� ������� ������� ������� ���� �������� ��������
Type
  TMeterCheckPeriodUnitType = (utYear, utMonth, utDay);
  TMeterCheckPeriodUnitTypeNames = Array[1..3] Of String;
  TMeterCheckPeriodUnitTypeNameArray = Array[TMeterCheckPeriodUnitType] Of TMeterCheckPeriodUnitTypeNames;
Const
  DateUnitNames: TMeterCheckPeriodUnitTypeNameArray = (('���', '����', '���'),
    ('�����', '������', '�������'), ('����', '���', '����'));
  //------------------------------------------------------------------------------
Type
  TFormulaMacrosType = (fmt_Simple, fmt_System);
  TADSItemType=(ads_Request{������},ads_Complaint{������});
  TADSItemTypeNames=array[TADSItemType] of string;
const
  ADSItemTypeGenitiveNameMass:TADSItemTypeNames=('������','�����');
  ADSItemTypeGenitiveName:TADSItemTypeNames=('������','������');

  // ����� ����� ��� Account_Action
  Acc_Edit_Parameters ='1';
  Acc_Edit_Documents  ='3';
  Acc_Edit_Meters     ='4';
  Acc_Edit_Registration='5';
  Acc_Edit_Comments   ='6';
  Acc_Edit_Benefit    ='7';
  Acc_Edit_DebtAgreement='8';
  Acc_Edit_Rooms      ='9';

var
 //����������� ������ ��� �������������  ������� � ������ , ����������� ����������� ��������
 MainCS:TCriticalSection;

Implementation

Uses SysUtils, uMessages;

Function FilterObjectToAddressTreeLevel(FilterObject: TFilterObject): TAddressTreeLevel;
Begin
  Result := atl_Account;
  Case FilterObject Of
    fo_Account: Result := atl_Account;
    fo_Flat: Result := atl_Flat;
    fo_House: Result := atl_House;
    fo_Street: Result := atl_Street;
    fo_Settlement: Result := atl_Settlement;
  End;
End;

Function ID_Parameter_Owner_To_TAddressTreeLevel(AValue: Integer): TAddressTreeLevel;
Var
  flg_Exists: Boolean;
  a: TAddressTreeLevel;
Begin
  flg_Exists := false;
  Result := atl_Account;
  For a := Low(a) To High(a) Do
    If AValue = AddressTreeLevelCodes[a] Then
    Begin
      flg_Exists := True;
      Result := a;
      Break;
    End;
  If Not flg_Exists Then
    Raise EMessage.Create('������',
      format('ID_Parameter_Owner_To_TAddressTreeLevel:����������� ������� �������� %d !', [AValue]));
End;

Function AddressTreeLevelCodesDelimitedStr: String;
Var
  a: TAddressTreeLevel;
Begin
  Result := '';
  For a := Low(a) To High(a) Do
    If Result = '' Then Result := inttostr(AddressTreeLevelCodes[a])
    Else Result := Result + ',' + inttostr(AddressTreeLevelCodes[a]);
End;

initialization
 MainCS:=TCriticalSection.Create;
finalization
 FreeAndNil(MainCS);
End.
