﻿namespace GisGkh.Forms
{
    partial class IndividualServicesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.senderIdOnlyFilter1 = new Helper.Controls.SenderIdOnlyFilter();
            this.periodFilter1 = new Helper.Controls.PeriodFilter();
            this.tbIdBillfilter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // senderIdOnlyFilter1
            // 
            this.senderIdOnlyFilter1.Location = new System.Drawing.Point(12, 12);
            this.senderIdOnlyFilter1.Name = "senderIdOnlyFilter1";
            this.senderIdOnlyFilter1.Size = new System.Drawing.Size(395, 59);
            this.senderIdOnlyFilter1.TabIndex = 0;
            // 
            // periodFilter1
            // 
            this.periodFilter1.Location = new System.Drawing.Point(12, 77);
            this.periodFilter1.Name = "periodFilter1";
            this.periodFilter1.Size = new System.Drawing.Size(279, 61);
            this.periodFilter1.TabIndex = 1;
            // 
            // tbIdBillfilter
            // 
            this.tbIdBillfilter.Location = new System.Drawing.Point(301, 104);
            this.tbIdBillfilter.Name = "tbIdBillfilter";
            this.tbIdBillfilter.Size = new System.Drawing.Size(106, 20);
            this.tbIdBillfilter.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(298, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "id_bill_filter";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(207, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Загрузить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // IndividualServicesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 207);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbIdBillfilter);
            this.Controls.Add(this.periodFilter1);
            this.Controls.Add(this.senderIdOnlyFilter1);
            this.Name = "IndividualServicesForm";
            this.Text = "IndividualServicesForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Helper.Controls.SenderIdOnlyFilter senderIdOnlyFilter1;
        private Helper.Controls.PeriodFilter periodFilter1;
        private System.Windows.Forms.TextBox tbIdBillfilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}