﻿namespace EllisBillFilter
{
    partial class FrmClientDetailDeliveryEmail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridEmail = new EllisBillFilter.BaseGrid();
            this.clientDetailDeliveryEmailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gridViewEMail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Email = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbGridEditable = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAddRecord = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeleteRecord = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailDeliveryEmailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridEmail
            // 
            this.gridEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridEmail.ContextMenuStrip = this.contextMenuStrip1;
            this.gridEmail.DataSource = this.clientDetailDeliveryEmailBindingSource;
            this.gridEmail.Location = new System.Drawing.Point(0, 0);
            this.gridEmail.MainView = this.gridViewEMail;
            this.gridEmail.Name = "gridEmail";
            this.gridEmail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.gridEmail.Size = new System.Drawing.Size(680, 359);
            this.gridEmail.TabIndex = 0;
            this.gridEmail.TrimCellValueBeforeValidate = true;
            this.gridEmail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEMail});
            this.gridEmail.SaveData += new EllisBillFilter.BaseGrid.SaveDataEventHandler(this.gridEmail_SaveData);
            // 
            // clientDetailDeliveryEmailBindingSource
            // 
            this.clientDetailDeliveryEmailBindingSource.DataMember = "ClientDetailDeliveryEmail";
            this.clientDetailDeliveryEmailBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewEMail
            // 
            this.gridViewEMail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewEMail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewEMail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewEMail.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewEMail.Appearance.Row.Options.UseTextOptions = true;
            this.gridViewEMail.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewEMail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colChecked,
            this.colID_Email,
            this.colName,
            this.colEmails,
            this.colComment});
            this.gridViewEMail.GridControl = this.gridEmail;
            this.gridViewEMail.Name = "gridViewEMail";
            this.gridViewEMail.OptionsView.RowAutoHeight = true;
            this.gridViewEMail.OptionsView.ShowGroupPanel = false;
            this.gridViewEMail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewEMail_CellValueChanged);
            // 
            // colChecked
            // 
            this.colChecked.Caption = "Д";
            this.colChecked.FieldName = "Checked";
            this.colChecked.Name = "colChecked";
            this.colChecked.OptionsColumn.AllowSize = false;
            this.colChecked.OptionsColumn.FixedWidth = true;
            this.colChecked.Visible = true;
            this.colChecked.VisibleIndex = 0;
            this.colChecked.Width = 33;
            // 
            // colID_Email
            // 
            this.colID_Email.FieldName = "ID_Email";
            this.colID_Email.Name = "colID_Email";
            this.colID_Email.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.Caption = "Адресат";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 144;
            // 
            // colEmails
            // 
            this.colEmails.Caption = "Email";
            this.colEmails.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colEmails.FieldName = "Emails";
            this.colEmails.Name = "colEmails";
            this.colEmails.Visible = true;
            this.colEmails.VisibleIndex = 2;
            this.colEmails.Width = 348;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // colComment
            // 
            this.colComment.Caption = "Комментарий";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 3;
            this.colComment.Width = 134;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(462, 370);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(98, 34);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(570, 370);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(98, 34);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // cbGridEditable
            // 
            this.cbGridEditable.AutoSize = true;
            this.cbGridEditable.Location = new System.Drawing.Point(12, 380);
            this.cbGridEditable.Name = "cbGridEditable";
            this.cbGridEditable.Size = new System.Drawing.Size(229, 17);
            this.cbGridEditable.TabIndex = 3;
            this.cbGridEditable.Text = "Включить редактирование справочника";
            this.cbGridEditable.UseVisualStyleBackColor = true;
            this.cbGridEditable.CheckStateChanged += new System.EventHandler(this.cbGridEditable_CheckStateChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAddRecord,
            this.miDeleteRecord});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(167, 48);
            // 
            // miAddRecord
            // 
            this.miAddRecord.Enabled = false;
            this.miAddRecord.Name = "miAddRecord";
            this.miAddRecord.Size = new System.Drawing.Size(166, 22);
            this.miAddRecord.Text = "Добавить запись";
            this.miAddRecord.Click += new System.EventHandler(this.miAddRecord_Click);
            // 
            // miDeleteRecord
            // 
            this.miDeleteRecord.Enabled = false;
            this.miDeleteRecord.Name = "miDeleteRecord";
            this.miDeleteRecord.Size = new System.Drawing.Size(166, 22);
            this.miDeleteRecord.Text = "Удалить запись";
            this.miDeleteRecord.Click += new System.EventHandler(this.miDeleteRecord_Click);
            // 
            // FrmClientDetailDeliveryEmail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 411);
            this.Controls.Add(this.cbGridEditable);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gridEmail);
            this.Name = "FrmClientDetailDeliveryEmail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Справочник адресатов";
            this.Load += new System.EventHandler(this.FrmClientDetailDeliveryEmail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailDeliveryEmailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseGrid gridEmail;
        private System.Windows.Forms.BindingSource clientDetailDeliveryEmailBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEMail;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Email;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmails;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colChecked;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private System.Windows.Forms.CheckBox cbGridEditable;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miAddRecord;
        private System.Windows.Forms.ToolStripMenuItem miDeleteRecord;
    }
}