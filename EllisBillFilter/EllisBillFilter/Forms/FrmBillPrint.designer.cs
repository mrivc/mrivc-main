﻿namespace EllisBillFilter
{
    partial class FrmBillPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.txtAccount = new System.Windows.Forms.TextBox();
            this.lbAccountList = new System.Windows.Forms.ListBox();
            this.btnAddAccount = new System.Windows.Forms.Button();
            this.cbBillKind = new System.Windows.Forms.ComboBox();
            this.billKindBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            ((System.ComponentModel.ISupportInitialize)(this.billKindBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(455, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtAccount
            // 
            this.txtAccount.Location = new System.Drawing.Point(12, 35);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(374, 20);
            this.txtAccount.TabIndex = 1;
            // 
            // lbAccountList
            // 
            this.lbAccountList.FormattingEnabled = true;
            this.lbAccountList.Location = new System.Drawing.Point(12, 63);
            this.lbAccountList.Name = "lbAccountList";
            this.lbAccountList.Size = new System.Drawing.Size(244, 108);
            this.lbAccountList.TabIndex = 2;
            // 
            // btnAddAccount
            // 
            this.btnAddAccount.Location = new System.Drawing.Point(392, 34);
            this.btnAddAccount.Name = "btnAddAccount";
            this.btnAddAccount.Size = new System.Drawing.Size(70, 23);
            this.btnAddAccount.TabIndex = 3;
            this.btnAddAccount.Text = "Добавить";
            this.btnAddAccount.UseVisualStyleBackColor = true;
            this.btnAddAccount.Click += new System.EventHandler(this.btnAddAccount_Click);
            // 
            // cbBillKind
            // 
            this.cbBillKind.DataSource = this.billKindBindingSource;
            this.cbBillKind.DisplayMember = "Bill_Kind";
            this.cbBillKind.FormattingEnabled = true;
            this.cbBillKind.Location = new System.Drawing.Point(12, 8);
            this.cbBillKind.Name = "cbBillKind";
            this.cbBillKind.Size = new System.Drawing.Size(450, 21);
            this.cbBillKind.TabIndex = 4;
            this.cbBillKind.ValueMember = "ID_Bill_Kind";
            // 
            // billKindBindingSource
            // 
            this.billKindBindingSource.DataMember = "Bill_Kind";
            this.billKindBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // FrmBillPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 204);
            this.Controls.Add(this.cbBillKind);
            this.Controls.Add(this.btnAddAccount);
            this.Controls.Add(this.lbAccountList);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.button1);
            this.Name = "FrmBillPrint";
            this.Text = "FrmBillPrint";
            this.Shown += new System.EventHandler(this.FrmBillPrint_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.billKindBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtAccount;
        private System.Windows.Forms.ListBox lbAccountList;
        private System.Windows.Forms.Button btnAddAccount;
        private System.Windows.Forms.ComboBox cbBillKind;
        private System.Windows.Forms.BindingSource billKindBindingSource;
        private DS.MDataSet mDataSet;
    }
}