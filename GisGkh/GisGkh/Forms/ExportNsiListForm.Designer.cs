﻿namespace GisGkh
{
    partial class ExportNsiListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportNsiListForm));
            this.exportNsiListButton = new System.Windows.Forms.Button();
            this.exportNsiProviderBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pfStart = new Helper.Controls.PeriodFilter();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pfEnd = new Helper.Controls.PeriodFilter();
            this.lbStartPeriod = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // exportNsiListButton
            // 
            this.exportNsiListButton.Location = new System.Drawing.Point(12, 12);
            this.exportNsiListButton.Name = "exportNsiListButton";
            this.exportNsiListButton.Size = new System.Drawing.Size(194, 23);
            this.exportNsiListButton.TabIndex = 19;
            this.exportNsiListButton.Text = "Экспорт справочников NSI";
            this.exportNsiListButton.UseVisualStyleBackColor = true;
            this.exportNsiListButton.Click += new System.EventHandler(this.exportNsiListButton_Click);
            // 
            // exportNsiProviderBtn
            // 
            this.exportNsiProviderBtn.Location = new System.Drawing.Point(307, 47);
            this.exportNsiProviderBtn.Name = "exportNsiProviderBtn";
            this.exportNsiProviderBtn.Size = new System.Drawing.Size(194, 23);
            this.exportNsiProviderBtn.TabIndex = 24;
            this.exportNsiProviderBtn.Text = "Экспорт услуг";
            this.exportNsiProviderBtn.UseVisualStyleBackColor = true;
            this.exportNsiProviderBtn.Click += new System.EventHandler(this.exportNsiProviderBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(212, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(289, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "Экпорт ИД поставщиков информации";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pfStart
            // 
            this.pfStart.Location = new System.Drawing.Point(6, 19);
            this.pfStart.Name = "pfStart";
            this.pfStart.Size = new System.Drawing.Size(279, 61);
            this.pfStart.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(330, 112);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(153, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Запустить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbStartPeriod);
            this.groupBox1.Controls.Add(this.pfEnd);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.pfStart);
            this.groupBox1.Location = new System.Drawing.Point(12, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(489, 143);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Предварительная подготовка данных";
            // 
            // pfEnd
            // 
            this.pfEnd.Location = new System.Drawing.Point(6, 74);
            this.pfEnd.Name = "pfEnd";
            this.pfEnd.Size = new System.Drawing.Size(279, 61);
            this.pfEnd.TabIndex = 2;
            // 
            // lbStartPeriod
            // 
            this.lbStartPeriod.AutoSize = true;
            this.lbStartPeriod.Location = new System.Drawing.Point(59, 22);
            this.lbStartPeriod.Name = "lbStartPeriod";
            this.lbStartPeriod.Size = new System.Drawing.Size(68, 13);
            this.lbStartPeriod.TabIndex = 3;
            this.lbStartPeriod.Text = "(начальный)";
            // 
            // ExportNsiListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 233);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.exportNsiProviderBtn);
            this.Controls.Add(this.exportNsiListButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportNsiListForm";
            this.Text = "Экспорт справочников и услуг";
            this.Load += new System.EventHandler(this.ExportNsiListForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button exportNsiListButton;
        private System.Windows.Forms.Button exportNsiProviderBtn;
        private System.Windows.Forms.Button button1;
        private Helper.Controls.PeriodFilter pfStart;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbStartPeriod;
        private Helper.Controls.PeriodFilter pfEnd;
    }
}