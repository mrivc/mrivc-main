﻿using EllisBillFilter.DS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace EllisBillFilter
{
    public class DynamicParameter
    {
        public string Caption;

        public string LinkSqlParameter;

        public string ParameterName;

        public string Type;

        public string SQL;

        public DynamicParameter(string parameterInfo)
        {
            Parse(parameterInfo);
        }

        public static DynamicParameter Create(string parameterInfo)
        {
            return new DynamicParameter(parameterInfo);
        }

        private void Parse(string parameterInfo)
        {
            string[] descriptors = parameterInfo.Split('|');

            if (descriptors.Length != 3)
                throw new ApplicationException("Дескриптор параметра должен соответствовать шаблону: <Caption>|?<LinkSqlParametrName>|<ParameterType>, например: Дата прописки|?OnDate|date_picker");

            this.Caption = descriptors[0];
            this.LinkSqlParameter = descriptors[1];
            this.ParameterName = descriptors[2];

            DataTable pInfo = DAccess.DataModule.GetDynamicParameter(ParameterName);
            if (pInfo != null && pInfo.Rows.Count > 0)
            {
                this.Type = pInfo.Rows[0]["Type"].ToString();
                this.SQL = pInfo.Rows[0]["SQL"].ToString();
            }
        }
    }
}
