﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ExportNsiList : MrivcExportCommand
    {
        public string NsiRegistryNumbers { get; set; }

        public ExportNsiList() : base("exportNsiList", "NsiCommonAsync", "NsiList")
        {
            Description = "Экспорт справочников ГИС \"ЖКХ\"";
        }

        public new void Execute()
        {
            var nsiListDs = ExecuteToDataSet(new[] { new Tuple<string[], string>(new[] { "ListGroup" }, "NSI") }, _exportNodeName);

            DAccess.DataModule.CopyDataSetToTempDB(nsiListDs);

            var exportNsiItemCommand = new ExportNsiItem(SenderID);
            exportNsiItemCommand.AddToReport = this.AddToReport;
            exportNsiItemCommand.Description = this.Description;

            foreach (var regNumber in NsiRegistryNumbers.Split(','))
            {
                exportNsiItemCommand.ExecuteToTempDB(regNumber);
                var sql = "GIS.ExportNsiItems";
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1(sql, new object[,] {  { "@registryNumber", regNumber } });
            }
        }

        public void ExecuteToTempDB()
        {

        }

        public DataSet SaveToDataSet()
        {
            return new DataSet();
        }
    }
}
