Unit uPropertyCollection;

Interface

Uses Classes, INIFiles;

Type
  TProperty = Class(TCollectionItem)
  Private
    FName: String;
    FValue: Variant;
  Published
    Property Name: String Read FName Write FName;
    Property Value: Variant Read FValue Write FValue;
  Public
    //    constructor Create(ACollection: TCollection; AName: String); reintroduce;
    Procedure Assign(Source: TPersistent); Override;
  End;

  TPropertyCollection = Class(TCollection)
  Private
    FSection: String;
    Function GetValue(Name: String): Variant;
    Procedure SetValue(Name: String; Value: Variant);

    Function GetValueByIndex(Index: Integer): Variant;
    Procedure SetValueByIndex(Index: Integer; Value: Variant);

    Function GetNameByIndex(Index: Integer): String;

    Function Parse_String(Source: String; Var Name, Value: String): Boolean;
  Public
    Function GetPropertyByIndex(Index: Integer): TProperty;
    Function GetPropertyByName(Name: String): TProperty;
    Property Value[Name: String]: Variant Read GetValue Write SetValue; Default;
    Property ValueByIndex[Index: Integer]: Variant Read GetValueByIndex Write SetValueByIndex;
    Property Name[Index: Integer]: String Read GetNameByIndex;
    Function Add(AName: String): TProperty;
    Constructor Create; Overload;
    Constructor Create(FileName: String; Section: String = ''); Overload;
    Function LoadFromFile(Const FileName: String; Section: String = ''): Boolean;
    Function SaveToFile(Const FileName: String): Boolean;
    Procedure Assign(Source: TPropertyCollection); Reintroduce;
  End;

Implementation

Uses Variants, SysUtils, Windows;

{ TPropertyCollection }

Function TPropertyCollection.Add(AName: String): TProperty;
Begin
  Result := GetPropertyByName(AName);
  If Not Assigned(Result) Then
  Begin
    Result := (Inherited Add) As TProperty;
    Result.Name := AName;
  End;

End;

Constructor TPropertyCollection.Create;
Begin
  Inherited Create(TProperty);
End;

Procedure TPropertyCollection.Assign(Source: TPropertyCollection);
Var
  I: Integer;
Begin
  Clear;
  For I := 0 To Source.Count - 1 Do
    Value[Source.GetNameByIndex(I)] := Source.GetValueByIndex(I);
End;

Constructor TPropertyCollection.Create(FileName: String; Section: String = '');
Begin
  Create;
  LoadFromFile(FileName, Section);
End;

Function TPropertyCollection.GetNameByIndex(Index: Integer): String;
Var
  ResultProperty: TProperty;
Begin
  ResultProperty := GetPropertyByIndex(Index);
  If Assigned(ResultProperty) Then Result := ResultProperty.Name Else Result := '';
End;

Function TPropertyCollection.GetPropertyByIndex(Index: Integer): TProperty;
Begin
  Result := TProperty(Inherited Items[Index]);
End;

Function TPropertyCollection.GetPropertyByName(Name: String): TProperty;
Var
  I: Integer;
Begin
  I := 0;
  Result := Nil;
  While (Not Assigned(Result)) And (I < Count) Do
  Begin
    If Assigned(GetPropertyByIndex(I))
     and AnsiSameText(GetPropertyByIndex(I).Name, Name) Then
      Result := GetPropertyByIndex(I);
    Inc(I);
  End;
End;

Function TPropertyCollection.GetValue(Name: String): Variant;
Var
  ResultProperty: TProperty;
Begin
  ResultProperty := GetPropertyByName(Name);
  If Assigned(ResultProperty) Then
    Result := ResultProperty.Value
  Else
    Result := Unassigned;
End;

Function TPropertyCollection.GetValueByIndex(Index: Integer): Variant;
Var
  ResultProperty: TProperty;
Begin
  ResultProperty := GetPropertyByIndex(Index);
  If Assigned(ResultProperty) Then Result := ResultProperty.Value Else Result := Unassigned;
End;

Function TPropertyCollection.LoadFromFile(Const FileName: String; Section: String = ''): Boolean;
Var
  Source, Name, Value: String;
  SL: TStringList;
  INI: TINIFile;
  I: Integer;

  Procedure ReadOldINI;
  Var
    F: Text;
  Begin
    AssignFile(F, FileName);
    Reset(F);
    While Not EOF(F) Do
    Begin
      Readln(F, Source);
      If Parse_String(Source, Name, Value) Then
        Add(Name).Value := Value;
    End;
    CloseFile(F);
  End;

Begin
  Result := FileExists(FileName);
  If Result Then
  Begin
    INI := TINIFile.Create(FileName);
    SL := TStringList.Create;
    Try
      INI.ReadSection(Section, SL);
      If (Trim(Section) <> '') And (SL.Count > 0) Then
      Begin
        For I := 0 To SL.Count - 1 Do
        Begin
          //          if Parse_String(INI.ReadString('', SL[I], ''), Name, Value) then
          Value := INI.ReadString('', SL[I], '');
          Add(SL[I]).Value := Value;
        End;
      End Else
      Begin
        ReadOldINI;
      End;
      If Trim(Section) = '' Then FSection := ''
      Else FSection := Section;
    Finally
      FreeAndNiL(SL);
      FreeAndNiL(INI);
    End;
  End;
End;

Function TPropertyCollection.Parse_String(Source: String; Var Name, Value: String): Boolean;
Begin
  Result := Pos('[', Source) <> 1;
  If Not Result Then Exit;
  Result := Pos('=', Source) <> 0;
  If Result Then
  Begin
    Name := Trim(Copy(Source, 1, Pos('=', Source) - 1));
    Value := Trim(Copy(Source, Pos('=', Source) + 1, MaxInt));
    Result := (Name <> '') And (Value <> '');
  End;
End;

Function TPropertyCollection.SaveToFile(Const FileName: String): Boolean;
Var
  I: Integer;
  INI: TINIFile;
  V: Variant;
  N: String;
Begin
  INI := TINIFile.Create(FileName);
  Try
    For I := 0 To Count - 1 Do
    Begin
      N := GetPropertyByIndex(I).Name;
      V := GetPropertyByIndex(I).Value;
      If Not VarIsNull(V) Then
        INI.WriteString(FSection, N, VarToStr(V));
    End;
    Result := True;
  Finally
    FreeAndNiL(INI);
  End;
  //  AssignFile(F,FileName);
  //  try
  //    Rewrite(F);
  //    for I := 0 to Count - 1 do
  //      if not VarIsNull(GetPropertyByIndex(i).Value) then
  //        Writeln(F,GetPropertyByIndex(I).Name+' = '+GetPropertyByIndex(I).Value);
  //    Result := True;
  //  except on E: Exception do Result := False;
  //  end;
  //  CloseFile(F);
End;

Procedure TPropertyCollection.SetValue(Name: String; Value: Variant);
Var
  ResultProperty: TProperty;
Begin
  ResultProperty := GetPropertyByName(Name);
  If Assigned(ResultProperty) Then ResultProperty.Value := Value
  Else Add(Name).Value := Value;
End;

Procedure TPropertyCollection.SetValueByIndex(Index: Integer; Value: Variant);
Var
  ResultProperty: TProperty;
Begin
  ResultProperty := GetPropertyByIndex(Index);
  If Assigned(ResultProperty) Then ResultProperty.Value := Value;
End;

{ TProperty }

Procedure TProperty.Assign(Source: TPersistent);
Begin
  If Source Is TProperty Then
    With TProperty(Source) Do
    Begin
      Self.FName := FName;
      Self.FValue := FValue;
    End
  Else Inherited;
End;

//constructor TProperty.Create(ACollection: TCollection; AName: String);
//begin
//  inherited Create(ACollection);
//  FName := AName;
//  FValue := Null;
//end;

End.
