﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MassBillNew
{
    public partial class PrintHistoryDialog : Form
    {
        public PrintHistoryDialog()
        {
            InitializeComponent();


            SqlClass.Open();
            if (Convert.ToInt32(SqlClass.ExecuteScalar("select float_value from system_parameter sp where parameter = 'PRINT_BACKSIDE'")) == 1)
            {
                checkBox1.Checked = true;
            }
            if (Convert.ToInt32(SqlClass.ExecuteScalar("select float_value from system_parameter sp where parameter = 'PRINT_SAVE_HISTORY'")) == 1)
            {
                checkBox2.Checked = true;
            }
            if (Convert.ToInt32(SqlClass.ExecuteScalar("select float_value from system_parameter sp where parameter = 'PRINT_BILL_WITH_SUBSCRIPTION'")) == 1)
            {
                checkBox3.Checked = true;
            }
            SqlClass.Close();

        }

        private bool backSide = false;
        public bool BackSide
        {
            get
            {
                return backSide;
            }
            set
            {
                backSide = value;
            }
        }

        private bool saveHistory = false;
        public bool SaveHistory
        {
            get
            {
                return saveHistory;
            }
            set
            {
                saveHistory = value;
            }
        }

        private bool printBillsWithSubscription = false;
        public bool PrintBillsWithSubscription
        {
            get
            {
                return printBillsWithSubscription;
            }
            set
            {

                printBillsWithSubscription = value;
            }
        }

        private bool printDoubleKvit = false;
        public bool PrintDoubleKvit
        {
            get
            {
                return printDoubleKvit;
            }
            set
            {

                printDoubleKvit = value;
                chBxDoubledKvit.Checked = printDoubleKvit;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            backSide = checkBox1.Checked;
            saveHistory = checkBox2.Checked;
            printBillsWithSubscription = checkBox3.Checked;
            PrintDoubleKvit = chBxDoubledKvit.Checked;
        }

        private void PrintHistoryDialog_Load(object sender, EventArgs e)
        {

            
        }
    }
}
