﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;

namespace MtecExcelConverter
{
    using System.Data;

    using MtecExcelConverter.DataBase;

    public class ImportData
    {
        private MDataSet.MtecAccountImportDataTable dataTable;

        private string sourceDirectory;

        private Guid importGuid;

        public ImportData(string sourceDirectory, Guid importGuid)
        {
            this.dataTable = new MDataSet.MtecAccountImportDataTable();
            this.importGuid = importGuid;
            this.sourceDirectory = sourceDirectory;
        }

        public MDataSet.MtecAccountImportDataTable DataTable
        {
            get
            {
                return this.dataTable;
            }
        }

        public string SourceDirectory
        {
            get
            {
                return this.sourceDirectory;
            }
        }

        public void AddData(CellDataItem dataItem)
        {
            this.CreateRowForHeatingService(dataItem);
            this.CreateHotWaterMopService(dataItem);
            this.CreateHotWaterService(dataItem);
        }

        /// <summary>
        /// Отопление РСО
        /// </summary>
        /// <param name="dataItem"></param>
        /// <returns></returns>
        private MDataSet.MtecAccountImportRow CreateRowForHeatingService(CellDataItem dataItem)
        {
            MDataSet.MtecAccountImportRow row = GetDataRow(dataItem, 2);

            switch (dataItem.SumTypeValue)
            {
                case SummType.Balance:
                    row.Balance = dataItem.SumHeatingValue;
                    break;
                case SummType.Bill:
                    row.Bill = dataItem.SumHeatingValue;
                    break;
                case SummType.Tariff:
                    row.Tariff = dataItem.SumHeatingValue;
                    break;
            }

            return row;
        }

        /// <summary>
        /// Подогрев воды МОП РСО
        /// </summary>
        /// <param name="dataItem"></param>
        /// <returns></returns>
        private MDataSet.MtecAccountImportRow CreateHotWaterMopService(CellDataItem dataItem)
        {
            MDataSet.MtecAccountImportRow row = GetDataRow(dataItem, 46);

            switch (dataItem.SumTypeValue)
            {
                case SummType.Balance:
                    row.Balance = dataItem.SumHotWaterMopValue;
                    break;
                case SummType.Bill:
                    row.Bill = dataItem.SumHotWaterMopValue;
                    break;
                case SummType.Tariff:
                    row.Tariff = dataItem.SumHotWaterMopValue;
                    break;
            }

            return row;
        }

        /// <summary>
        /// Подогрев воды РСО
        /// </summary>
        /// <param name="dataItem"></param>
        /// <returns></returns>
        private MDataSet.MtecAccountImportRow CreateHotWaterService(CellDataItem dataItem)
        {
            MDataSet.MtecAccountImportRow row = GetDataRow(dataItem, 4);

            switch (dataItem.SumTypeValue)
            {
                case SummType.Balance:
                    row.Balance = dataItem.SumHotWaterValue;
                    break;
                case SummType.Bill:
                    row.Bill = dataItem.SumHotWaterValue;
                    break;
                case SummType.Tariff:
                    row.Tariff = dataItem.SumHotWaterValue;
                    break;
            }

            return row;
        }


        //2 - Отопление РСО
        //46 - Подогрев воды МОП РСО
        //4 - Подогрев воды РСО
        private MDataSet.MtecAccountImportRow GetDataRow(CellDataItem dataItem, int serviceId)
        {
            string account = dataItem.AccountValue;
            DateTime period1 = dataItem.PeriodValue;

            MDataSet.MtecAccountImportRow row = dataTable.FirstOrDefault(r => r.FileName == dataItem.FileName && r.Account == account && r.PeriodDate == period1 && r.ID_Service == serviceId);

            if (row == null)
            {
                row = dataTable.NewMtecAccountImportRow();
                row.FileName = dataItem.FileName;
                row.ImportId = importGuid;
                row.ImportDate = DateTime.Now;
                row.Account = dataItem.AccountValue;
                row.Address = dataItem.AddressValue;
                row.Owner = dataItem.OwnerValue;
                row.PeriodDate = dataItem.PeriodValue;
                row.ID_Service = serviceId;
                this.dataTable.Rows.Add(row);
            }

            return row;
        }

        public void SaveDataToDataBase()
        {
            DBManager.InserData((DataTable)this.dataTable, "town.MtecAccountImport");
        }
    }
}
