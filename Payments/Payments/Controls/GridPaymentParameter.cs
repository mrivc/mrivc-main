﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Payments.DS;
using Payments.LogicClasses;
using DevExpress.XtraGrid.Views.Grid;

namespace Payments.Controls
{
    public partial class GridPaymentParameter : UserControl
    {
        PaymentClass paymentClass;
        string layoutName;

        public GridPaymentParameter()
        {
            InitializeComponent();
            gridViewPaymentParameterDetail.GroupFormat = "[#image]{1} {2}";
        }

        public GridView PaymentDetailGridView
        {
            get {
                return gridViewPaymentParameterDetail;
            }
        }

        public void SaveLayout()
        {

            if (gridViewPaymentParameterDetail == null)
                return;

            gridViewPaymentParameterDetail.SaveLayoutToXml(layoutName + ".xml");
        }

        public void LoadLayout(string layoutName)
        {
            this.layoutName = layoutName;
            if (gridViewPaymentParameterDetail == null)
                return;

            if (FileHelper.ExistFile(this.layoutName + ".xml"))
                gridViewPaymentParameterDetail.RestoreLayoutFromXml(this.layoutName + ".xml");
        }

        public void SetColumnWidth(string columnName, int value)
        {
            gridViewPaymentParameterDetail.Columns[columnName].Width = value;
        }

        public void LoadData(PaymentClass paymentClass)
        {
            this.paymentClass = paymentClass;
            paymentsDataSet.PaymentParameterDetail.Clear();
            DAccess.DataModule.PaymentParameterDetailSelect(paymentsDataSet.PaymentParameterDetail, paymentClass);
            gridViewPaymentParameterDetail.ExpandAllGroups();

            //if (paymentClass.ShowParameterQuantity == 1) // для коммунальных платежей скрываем колонку количества
                colDefault_Quantity.Visible = paymentClass.ShowParameterQuantity;
            //else
              //  colDefault_Quantity.Visible = true;
        }

        public void SetValue(string parameterCode, string parameterValue)
        {
            foreach (PaymentsDataSet.PaymentParameterDetailRow row in paymentsDataSet.PaymentParameterDetail.Rows)
            {
                if (row.Code.ToLower() == parameterCode.ToLower())
                    row["Parameter_Value"] = parameterValue;
            }
            paymentsDataSet.PaymentParameterDetail.AcceptChanges();
        }

        public object GetValue(string parameterCode)
        {
            foreach (PaymentsDataSet.PaymentParameterDetailRow row in paymentsDataSet.PaymentParameterDetail.Rows)
            {
                if (row.Code.ToLower() == parameterCode.ToLower())
                    if (row["Parameter_Value"] != DBNull.Value)
                        return row["Parameter_Value"];
                    else
                        return string.Empty;
            }
            return string.Empty;
        }

        /// <summary>
        /// Получает записи у конторых количество (Parameter_Quantity) больше нуля и не пустое.
        /// </summary>
        /// <returns></returns>
        public List<PaymentParameterDatail> GetParameterWithQuantity()
        {
            List<PaymentParameterDatail> list = new List<PaymentParameterDatail>();
            foreach (PaymentsDataSet.PaymentParameterDetailRow row in paymentsDataSet.PaymentParameterDetail.Rows)
            {
                if (row["Parameter_Quantity"] != DBNull.Value && row.Parameter_Quantity > 0)
                    list.Add(new PaymentParameterDatail(row.Display_Name, row.Print_Name, row.Code, row.Parameter_Value, row.Parameter_Quantity));
            }
            return list;
        }


        public void SaveData()
        {
            //удаляем не заполненные параметры
            foreach (PaymentsDataSet.PaymentParameterDetailRow row in paymentsDataSet.PaymentParameterDetail.Rows)
            {
                row.ID_Payment = paymentClass.IdPayment;

                if (row["Parameter_Quantity"] == DBNull.Value && paymentClass.ShowParameterQuantity == true)
                    row.Delete();

                if (row.RowState == DataRowState.Deleted)
                    continue;

                if (row["Parameter_Value"] == DBNull.Value && paymentClass.ShowParameterQuantity == false)
                    row.Delete();
            }

            paymentsDataSet.PaymentParameterDetail.AcceptChanges();
            foreach (PaymentsDataSet.PaymentParameterDetailRow row in paymentsDataSet.PaymentParameterDetail.Rows)
                row.SetAdded();

            DAccess.DataModule.PaymentParameterDetailUpdate(paymentsDataSet.PaymentParameterDetail);
        }

        public void DisableGrid()
        {
            gridViewPaymentParameterDetail.OptionsBehavior.Editable = false;
            foreach (DevExpress.XtraGrid.Columns.GridColumn _col in gridViewPaymentParameterDetail.Columns)
                _col.AppearanceCell.BackColor = Color.LightGray;
        }

        public void EnableGrid()
        {
            gridViewPaymentParameterDetail.OptionsBehavior.Editable = true;
            foreach (DevExpress.XtraGrid.Columns.GridColumn _col in gridViewPaymentParameterDetail.Columns)
                _col.AppearanceCell.BackColor = Color.White;
        }

        private void gridViewPaymentParameterDetail_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

        }

        private void gridViewPaymentParameterDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Value == null ||  e.Value.ToString().Trim() == "")
                return;

            switch ((int)gridViewPaymentParameterDetail.GetDataRow(gridViewPaymentParameterDetail.FocusedRowHandle)["ID_Parameter_Type"])
            {
                case 1: //int
                    int intValue;
                    if (!int.TryParse(e.Value.ToString(), out intValue))
                        ShowError(e);
                    break;
                case 2: //string
                    break;
                case 3: //float
                    float floatValue;
                    if (!float.TryParse(e.Value.ToString().Replace(",","."), out floatValue))
                        ShowError(e);
                    else if (e.Value.ToString().IndexOf(".")>-1)
                        gridViewPaymentParameterDetail.GetDataRow(gridViewPaymentParameterDetail.FocusedRowHandle)[e.Column.FieldName] = e.Value.ToString().Replace(".", ",");
                    break;
                case 4: //DateTime
                    DateTime dateValue;
                    if (!DateTime.TryParse(e.Value.ToString(), out dateValue))
                        ShowError(e);
                    break;
            }
        }

        public void ShowError(DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            MessageBox.Show("Неправильный формат данных.");
            gridViewPaymentParameterDetail.GetDataRow(gridViewPaymentParameterDetail.FocusedRowHandle)[e.Column.FieldName] = DBNull.Value;
        }

        private void gridPaymentParameterDetail_Enter(object sender, EventArgs e)
        {
            gridViewPaymentParameterDetail.FocusedColumn = gridViewPaymentParameterDetail.VisibleColumns[1];
            gridViewPaymentParameterDetail.FocusedRowHandle = 0;
        }
    }
}
