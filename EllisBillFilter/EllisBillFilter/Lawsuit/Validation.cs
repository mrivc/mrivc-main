﻿using System;
using System.Windows.Forms;

namespace EllisBillFilter.Lawsuit
{
    static public class Validation
    {
        static public void Validate(TextBox tBox, string message)
        {
            if (tBox.Text == "") throw new Exception(message);
        }

        static public void Validate(ComboBox cBox, string message)
        {
            if (cBox.Text == "") throw new Exception(message);
        }

        static public void Validate(CheckedListBox cListBox, string message)
        {
            if (cListBox.CheckedItems.Count == 0) throw new Exception(message);
        }

        static public void Validate(NumericUpDown right, NumericUpDown left, string message)
        {
            if (right.Value > left.Value) throw new Exception(message);
        }

        static public void Validate(MaskedTextBox mTextBox, string message)
        {
            if (mTextBox.Text == "  .  ." || mTextBox.Text.Length < 10) throw new Exception(message);
        }
    }
}
