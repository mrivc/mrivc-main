﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using FastReport;

namespace EllisBillFilter
{
    public delegate void ClientDetailEditingEventHandler(DataRow data);
    public delegate void ClientDetailPrintEventHandler(DataRow data);

    public partial class GridBillFilterCurrent : UserControl
    {
        public event ClientDetailEditingEventHandler ClientDetailEditing;
        public event ClientDetailPrintEventHandler ClientDetailPrint;

        private bool initialized = false;
        private int period;
        private bool showActive;

        public GridBillFilterCurrent()
        {
            InitializeComponent();
        }

        public bool Initialized
        {
            get { return initialized; }
        }

        public GridView GridView
        {
            get { return (GridView)gridViewClientDetail; }
        }

        private bool UpdateData()
        {
            try
            {
                DAccess.DataModule.ClientDetailUpdate(mDataSet.ClientDetail);
                return true;
            }
            catch (Exception ex)
            {
                DataRow [] rows = mDataSet.ClientDetail.Select("ID_CLient_Detail=-1");

                if (rows.Length == 1)
                    mDataSet.ClientDetail.RemoveClientDetailRow((MDataSet.ClientDetailRow) rows[0]);

                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void LoadData(int period, bool showActive)
        {
            //if (!initialized)
            //    return;

            if (!showActive)
                miClientDetail_Add.Enabled = false;
            else
                miClientDetail_Add.Enabled = true;

            this.period = period;
            this.showActive = showActive;
            mDataSet.Period.Merge(DAccess.Instance.Periods);
            mDataSet.BillFilter.Merge(DAccess.Instance.BillFilter);
            mDataSet.DeriveCompany.Merge(DAccess.Instance.DeriveCompany);
            mDataSet.PayReceiverCompany.Merge(DAccess.Instance.PayReceiverCompany);
            mDataSet.FilterType.Merge(DAccess.Instance.FilterType);
            DAccess.DataModule.ClientDetailSelect(mDataSet.ClientDetail, period, showActive);

            gridClientDetail.InitEx();
            gridViewClientDetail.Columns["ID_Derive_Company"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            //gridViewClientDetail.ClearColumnsFilter();
            initialized = true;

            ReloadAgreementGrid();
        }

        public void ReLoadData(int period, bool showActive)
        {
            if (!initialized)
                return;
            mDataSet.ClientDetail.Clear();
            DAccess.DataModule.ClientDetailSelect(mDataSet.ClientDetail, period, showActive);
        }

        private void gridViewClientDetail_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridViewClientDetail.GetDataRow(e.RowHandle)["Begin_Period"] = period;
        }

        private void EditRecord()
        {
            EditRecord(true);
        }

        /// <summary>
        /// Редактирование записи кода фильтра 
        /// </summary>
        /// <param name="withAllowEditCheck">Проверять можно ли редактировать или нет. Если False то запись можно редактировать в любом случае, даже если относиться к прошлому периоду.</param>
        private void EditRecord(bool withAllowEditCheck)
        {
            DataRow currRow = gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle);

            int recordPeriod = (int)currRow["Begin_Period"];
            DateTime recordCreationDate = DateTime.Parse(currRow["CreationDate"].ToString());

            if (withAllowEditCheck)
                if (recordPeriod < DAccess.Instance.WorkPeriod && (DateTime.Now - recordCreationDate).TotalDays > 31)
                {
                    MessageBox.Show("Редактирование записи запрещено, т.к. запись создана в прошлом периоде.\nЧтобы внести изменения Вам необходимо закрыть данный код фильтра и открыть новый.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            FrmClientDetailEdit f = new FrmClientDetailEdit((MDataSet.ClientDetailRow)currRow, false);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                currRow["ID_Client"] = f.ActiveData["ID_Client"];

                //if ((int)currRow["ID_Derive_Company"] != (int)f.ActiveData["ID_Derive_Company"])
                //{
                //    if (ClientDetailEditing != null)
                //        ClientDetailEditing(f.ActiveData);
                //}

                currRow["ID_Derive_Company"] = f.ActiveData["ID_Derive_Company"];
                currRow["ID_Bill_Filter"] = f.ActiveData["ID_Bill_Filter"];
                currRow["Begin_Period"] = f.ActiveData["Begin_Period"];
                currRow["End_Period"] = f.ActiveData["End_Period"];
                currRow["Advance"] = f.ActiveData["Advance"];
                currRow["RoundTo"] = f.ActiveData["RoundTo"];
                currRow["Prefix"] = f.ActiveData["Prefix"];
                currRow["Show_Meter_For_Services"] = f.ActiveData["Show_Meter_For_Services"];
                currRow["ID_Filter_Type"] = f.ActiveData["ID_Filter_Type"];
               // currRow["Comment"] = "";
                currRow["Bill_Name"] = f.ActiveData["Bill_Name"];
                

                if (UpdateData() && ClientDetailEditing != null)
                    ClientDetailEditing(f.ActiveData);
            }
        }

        private void miClientDetail_Add_Click(object sender, EventArgs e)
        {
            DataRow currRow = mDataSet.ClientDetail.NewRow();

            FrmClientDetailEdit f = new FrmClientDetailEdit((MDataSet.ClientDetailRow)currRow, true);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                currRow["ID_Client_Detail"] = -1;
                currRow["ID_Client"] = f.ActiveData["ID_Client"];
                currRow["ID_Derive_Company"] = f.ActiveData["ID_Derive_Company"];
                currRow["ID_Bill_Filter"] = f.ActiveData["ID_Bill_Filter"];
                currRow["Begin_Period"] = f.ActiveData["Begin_Period"];
                currRow["End_Period"] = f.ActiveData["End_Period"];
                currRow["Advance"] = f.ActiveData["Advance"];
                currRow["RoundTo"] = f.ActiveData["RoundTo"];
                currRow["Prefix"] = f.ActiveData["Prefix"];
                currRow["Show_Meter_For_Services"] = f.ActiveData["Show_Meter_For_Services"];
                currRow["ID_Filter_Type"] = f.ActiveData["ID_Filter_Type"];
                currRow["Bill_Name"] = f.ActiveData["Bill_Name"];
                currRow["Comment"] = "";
                currRow["CreationDate"] = DateTime.Now;
                mDataSet.ClientDetail.AddClientDetailRow((MDataSet.ClientDetailRow)currRow);
                if (UpdateData() && ClientDetailEditing != null)
                    ClientDetailEditing(currRow);
            }
        }

        private void miClientDetail_Delete_Click(object sender, EventArgs e)
        {
            int period = (int)gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle)["Begin_Period"];
            if (period != this.period)
            {
                MessageBox.Show("Разрешено удалять только те записи, у которых «Начальный период» равен текущему выбранному периоду.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                gridClientDetail.DeleteActiveRow(false);
                UpdateData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void miClientDetail_Open_Click(object sender, EventArgs e)
        {
            gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle)["End_Period"] = DBNull.Value;
            UpdateData();
        }

        private void miClientDetail_Close_Click(object sender, EventArgs e)
        {
            ClosePeriodActiveBillFilter("Вы уверены что хотите закрыть период?");
        }

        private void ClosePeriodActiveBillFilter(string questionBeforeClose)
        {
            if (gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle)["End_Period"] != DBNull.Value)
                return;

            if (MessageBox.Show(questionBeforeClose, "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle)["End_Period"] = DAccess.Instance.WorkPeriod-1;
                UpdateData();
            }
        }

        private void miClientDetail_Refresh_Click(object sender, EventArgs e)
        {
            ReLoadData(period, showActive);
        }

        private void miClientDetail_Export_Click(object sender, EventArgs e)
        {
            gridClientDetail.MyExport();
        }

        private void miClientDetail_ServiceInfo_Click(object sender, EventArgs e)
        {
            DataRow currRow = gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle);
            MessageBox.Show("Служебная информация:"
                    + "\nID_Client_Detail = " + currRow["ID_Client_Detail"].ToString()
                    + "\nID_Client = " + currRow["ID_Client"].ToString()
                    + "\nID_Derive_Company = " + currRow["ID_Derive_Company"].ToString()
                    + "\nID_Bill_Filter = " + currRow["ID_Bill_Filter"].ToString()
                    + "\nBegin_Period = " + currRow["Begin_Period"].ToString()
                    + "\nEnd_Period = " + currRow["End_Period"].ToString()
                , "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void miClientDetail_OpenMeasureForService_Click(object sender, EventArgs e)
        {
            DataRow currRow = gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle);
            FrmServiceList f = new FrmServiceList(currRow["Show_Meter_For_Services"].ToString());
            if (f.ShowDialog() == DialogResult.OK)
            {
                currRow["Show_Meter_For_Services"] = f.SelectedServices;
	            this.UpdateData();
            }
        }

        private void miClientDetail_Edit_Click(object sender, EventArgs e)
        {
            EditRecord();
        }

        private void gridViewClientDetail_DoubleClick(object sender, EventArgs e)
        {
            EditRecord();
        }

        private void gridViewClientDetail_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle) == null ||
                gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle).RowState != DataRowState.Unchanged)
                return;

            ReloadAgreementGrid();
        }

        private void ReloadAgreementGrid() 
        {
            if (gridViewClientDetail.FocusedRowHandle >= 0)
            {
                txtComment.Text = gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle)["Comment"].ToString();
                gridClientDetailAgreement1.LoadData((int)gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle)["ID_Client_Detail"]);
            }
        }

        private void txtComment_Leave(object sender, EventArgs e)
        {
            if (gridViewClientDetail.FocusedRowHandle >= 0)
            {
                gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle)["Comment"] = txtComment.Text;
                UpdateData();
            }
        }

        private void splitContainerControl1_SizeChanged(object sender, EventArgs e)
        {
            splitContainerControl1.SplitterPosition = (int) (splitContainerControl1.Size.Height / 1.3);
        }

        private void gridViewClientDetail_RowStyle(object sender, RowStyleEventArgs e)
        {
            e.Appearance.Options.UseBackColor = true;

            if (e.RowHandle > -1 && gridViewClientDetail.GetDataRow(e.RowHandle)["Empty"] == DBNull.Value &&
                gridViewClientDetail.GetDataRow(e.RowHandle)["DeriveCompanyHasHouses"] != DBNull.Value &&
                (int)gridViewClientDetail.GetDataRow(e.RowHandle)["ID_Filter_Type"] == 1)
                e.Appearance.BackColor = Color.Red;
        }

        private void miClientDetail_Copy_Click(object sender, EventArgs e)
        {
            CopyClientDetailRecord(true);
        }

        private void miClientDetail_SimpleCopy_Click(object sender, EventArgs e)
        {
            CopyClientDetailRecord(false);
        }

        /// <summary>
        /// CopyClientDetailRecord
        /// </summary>
        /// <param name="deepCopy">Если true то копирует отмеченные дома</param>
        private void CopyClientDetailRecord(bool deepCopy)
        {
            if (gridViewClientDetail.FocusedRowHandle < 0)
            {
                MessageBox.Show("Для того чтобы скопировать текущую запись, необходимо выделить «запись-источник».", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            ClosePeriodActiveBillFilter("Закрыть период у копируемой записи по текущему коду фильтра?");

            DataRow currRow = mDataSet.ClientDetail.NewRow();
            DataRow sourceRow = gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle);

            FrmClientDetailEdit f = new FrmClientDetailEdit((MDataSet.ClientDetailRow)currRow, true, sourceRow, deepCopy);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    currRow["ID_Client_Detail"] = -1;
                    currRow["ID_Client"] = f.ActiveData["ID_Client"];
                    currRow["ID_Derive_Company"] = f.ActiveData["ID_Derive_Company"];
                    currRow["ID_Bill_Filter"] = f.ActiveData["ID_Bill_Filter"];
                    currRow["Begin_Period"] = f.ActiveData["Begin_Period"];
                    currRow["End_Period"] = f.ActiveData["End_Period"];
                    currRow["Advance"] = f.ActiveData["Advance"];
                    currRow["RoundTo"] = f.ActiveData["RoundTo"];
                    currRow["Prefix"] = f.ActiveData["Prefix"];
                    currRow["Show_Meter_For_Services"] = f.ActiveData["Show_Meter_For_Services"];
                    currRow["ID_Filter_Type"] = f.ActiveData["ID_Filter_Type"];
                    currRow["Comment"] = f.ActiveData["Comment"];
                    currRow["Bill_Name"] = f.ActiveData["Bill_Name"];
                    currRow["CreationDate"] = DateTime.Now;
                    mDataSet.ClientDetail.AddClientDetailRow((MDataSet.ClientDetailRow)currRow);

                    if (deepCopy)
                    {
                        DAccess.DataModule.ClientDetailCopy(mDataSet.ClientDetail, (int)sourceRow["ID_Client_Detail"]);
                    }

                    if (ClientDetailEditing != null)
                        ClientDetailEditing(f.ActiveData);

                    UpdateData();
                    MessageBox.Show("Запись скопирована успешно.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    mDataSet.ClientDetail.RemoveClientDetailRow((MDataSet.ClientDetailRow)currRow);
                    MessageBox.Show("Копирование записи завершилось ошибкой.  Проверьте уникальность записей, скорее всего копирование приводит к дублированию." + Environment.NewLine + Environment.NewLine + "Внутренняя ошибка:" + Environment.NewLine + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void miClientDetail_RecordHistory_Click(object sender, EventArgs e)
        {
            DataRow currRow = gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle);

            string info = DAccess.DataModule.ClientDetailRecordInfo((int)currRow["ID_Client_Detail"]);
            MessageBox.Show(info, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.E | Keys.Control))
            {
                EditRecord(false);
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void miClientDetail_PrintBills_Click(object sender, EventArgs e)
        {
            if (gridViewClientDetail.FocusedRowHandle < 0)
            {
                MessageBox.Show("Для печати квитанций необходимо выбрать код фильтра!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DataRow currRow = gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle);

            if (ClientDetailPrint != null)
                ClientDetailPrint(currRow);
        }

        private void gridViewClientDetail_ColumnFilterChanged(object sender, EventArgs e)
        {
            ReloadAgreementGrid();
        }

        private void gridViewClientDetail_EndGrouping(object sender, EventArgs e)
        {
            ReloadAgreementGrid();
        }

        private void miClientDetail_ProviderSettings_Click(object sender, EventArgs e)
        {
            DataRow currRow = gridViewClientDetail.GetDataRow(gridViewClientDetail.FocusedRowHandle);

            FrmBillFilterProvider f = new FrmBillFilterProvider((int)currRow["ID_Bill_Filter"]);
            f.LoadData();
            f.ShowDialog(this);
        }
    }
}
