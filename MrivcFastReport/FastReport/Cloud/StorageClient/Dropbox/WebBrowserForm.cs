using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using FastReport.Forms;

namespace FastReport.Cloud.StorageClient.Dropbox
{
    /// <summary>
    /// Represents form of the web browser.
    /// </summary>
    public partial class WebBrowserForm : BaseDialogForm
    {
        #region Fields

        private string url;

        #endregion // Fields

        #region Constructors

        /// <inheritdoc/>
        public WebBrowserForm(string url)
        {
            InitializeComponent();
            this.url = url;
        }

        #endregion // Constructors

        #region Events Handlers

        private void WebBrowserForm_Shown(object sender, EventArgs e)
        {
            wbBrowser.Navigate(url);
        }

        #endregion // Events Handlers
    }
}