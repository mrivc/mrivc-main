﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using XadesDemo.Configurations.Sections;

namespace XadesDemo
{
    public partial class CommandForm : Form
    {
        public string SenderID
        {
            get { return gisSenderProvider1.SenderID().ToString(); }
        }

        public CommandForm()
        {
            InitializeComponent();
        }

        List<Tuple<string[], string>> SetParameterValues()
        {
            var paramValues = new List<Tuple<string[], string>>();

            foreach (DataGridViewRow item in paramGrid.Rows)
            {
                if (!item.IsNewRow)
                {
                    var header = item.Cells[0].Value.ToString().Split('/');
                    header = (from h in header where h != "" select h).ToArray();
                    var value = "";
                    if (item.Cells[1].Value != null)
                        value = item.Cells[1].Value.ToString();
                    paramValues.Add(new Tuple<string[], string>(header, value));
                }
            }

            return paramValues;
        }

        private void executeBtn_Click(object sender, EventArgs e)
        {
            var mrivcCommand = new MrivcCommand(commandComBox.Text, serviceComBox.Text, SenderID);

            mrivcCommand.GetStateCommandWaitCount = int.Parse(waitCountTextBox.Text);
            mrivcCommand.GetStateCommandWaitDelay = int.Parse(waitDelayTextBox.Text);

            mrivcCommand.Execute(SetParameterValues());

            outTextBox.Text = MrivcStatic.Output;
        }

        void FillParamGrid()
        {
            paramGrid.Rows.Clear();
            var x = "";
            try
            {
                x = MrivcStatic.GisConfig.Services[serviceComBox.Text].Methods[commandComBox.Text].Template;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show($"Шаблон не указан в App.config для связки сервиса {serviceComBox.Text} и метода {commandComBox.Text}");
                return;
            }

            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(x);
            }
            catch (XmlException e)
            {
                //MrivcStatic.AddToOutput(e.Message);
            }

            var nodes = document.SelectNodes("//*");

            var values = new List<Tuple<string, string>>();

            foreach (XmlNode item in nodes)
            {
                if (MrivcStatic.IsLastElement(item))
                {
                    var value = "";
                    if (item.ChildNodes.Count != 0)
                        value = item.ChildNodes[0].InnerText;
                    values.Add(new Tuple<string, string>(MrivcStatic.FindXPath(item), value));
                }
            }

            foreach (var item in values)
            {
                paramGrid.Rows.Add(item.Item1, item.Item2);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            gisSenderProvider1.Fill(MrivcData.ConnectionString);
            foreach (ServiceElement item in MrivcStatic.GisConfig.Services)
            {
                serviceComBox.Items.Add(item.ServiceName);
            }
        }

        private void serviceComBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            commandComBox.Items.Clear();
            commandComBox.Text = "";
            foreach (MethodElement item in MrivcStatic.GisConfig.Services[serviceComBox.Text].Methods)
            {
                commandComBox.Items.Add(item.MethodName);
            }
        }

        private void commandComBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillParamGrid();
        }

        private void openBtn_Click(object sender, EventArgs e)
        {
            var path = "";

            if (paramsOpenFileDialog.ShowDialog() == DialogResult.OK)
                path = paramsOpenFileDialog.FileName;
            else
                return;

            string[] lines = File.ReadAllLines(path);


            paramGrid.Rows.Clear();
            foreach (var _string in lines)
            {
                var words = _string.Split(';');
                paramGrid.Rows.Add(words[0], words[1]);
            }

        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            var path = "";
            var _string = "";

            if (paramsSaveFileDialog.ShowDialog() == DialogResult.OK)
                path = paramsSaveFileDialog.FileName;
            else
                return;

            foreach (DataGridViewRow item in paramGrid.Rows)
            {
                if (!item.IsNewRow)
                {
                    _string += $"{item.Cells[0].Value};{item.Cells[1].Value}";
                    _string += Environment.NewLine;
                }
            }

            File.WriteAllText(path, _string);
        }

        private void wordWrapCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            outTextBox.WordWrap = ((CheckBox)sender).Checked;
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            FillParamGrid();
        }
    }
}
