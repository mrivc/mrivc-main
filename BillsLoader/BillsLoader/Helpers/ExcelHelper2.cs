﻿#region Using directives 
using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Data; 
using System.Data.OleDb; 
using System.Data.SqlClient; 
using System.Configuration;
using BillsLoader.DM; 
#endregion Using directives 
 

public class ExcelHelper2
{ 
    // Export data to an Excel spreadsheet via ADO.NET 
    public static void ExportToExcel(DataTable dtSQL, string excelFilePath) 
    {
        if (dtSQL.Rows.Count == 0)
            return;

        string strDownloadFileName = "";
        string strConn = "";

        //if (rblExtension.SelectedValue == "2003") 
        //{ 
        // Excel 97-2003 
        strDownloadFileName = excelFilePath;
        strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strDownloadFileName + ";Extended Properties='Excel 8.0;HDR=Yes'";
        //} 
        //else 
        //{ 
        //    // Excel 2007 
        //    strDownloadFileName = "~/DownloadFiles/" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx"; 
        //    strExcelConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Server.MapPath(strDownloadFileName) + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'"; 
        //} 

        using (OleDbConnection conn = new OleDbConnection(strConn)) 
        { 
            // Create a new sheet in the Excel spreadsheet. 
            OleDbCommand cmd = new OleDbCommand(StaticQuery.TmpErr, conn); 
 
            // Open the connection. 
            conn.Open(); 
 
            // Execute the OleDbCommand. 
            cmd.ExecuteNonQuery();

            cmd.CommandText = "INSERT INTO ErrT (NPP, DATPLAT, ADRES, AUTHCOD, SUMPLAT, OP1, OP2, OP3, OP4, NP1, NP2, NP3, NP4, COMMENT, Processed) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 

            // Add the parameters. 
            cmd.Parameters.Add("NPP", OleDbType.VarChar, 150, "NPP");
            cmd.Parameters.Add("DATPLAT", OleDbType.VarChar, 150, "DATPLAT");
            cmd.Parameters.Add("ADRES", OleDbType.VarChar, 150, "ADRES");
            cmd.Parameters.Add("AUTHCOD", OleDbType.VarChar, 150, "AUTHCOD");
            cmd.Parameters.Add("SUMPLAT", OleDbType.VarChar, 150, "SUMPLAT");
            cmd.Parameters.Add("OP1", OleDbType.VarChar, 150, "OP1");
            cmd.Parameters.Add("OP2", OleDbType.VarChar, 150, "OP2");
            cmd.Parameters.Add("OP3", OleDbType.VarChar, 150, "OP3");
            cmd.Parameters.Add("OP4", OleDbType.VarChar, 150, "OP4");
            cmd.Parameters.Add("NP1", OleDbType.VarChar, 150, "NP1");
            cmd.Parameters.Add("NP2", OleDbType.VarChar, 150, "NP2");
            cmd.Parameters.Add("NP3", OleDbType.VarChar, 150, "NP3");
            cmd.Parameters.Add("NP4", OleDbType.VarChar, 150, "NP4");
            cmd.Parameters.Add("COMMENT", OleDbType.VarChar, 150, "COMMENT");
            cmd.Parameters.Add("Processed", OleDbType.VarChar, 150, "Processed");

            // Initialize an OleDBDataAdapter object. 
            OleDbDataAdapter da = new OleDbDataAdapter("select * from ErrT", conn);

            // Set the InsertCommand of OleDbDataAdapter,  
            // which is used to insert data. 
            da.InsertCommand = cmd;

            foreach (DataRow dr in dtSQL.Rows)
            {
                dr.SetAdded();
            }

            // Insert the data into the Excel spreadsheet. 
            da.Update(dtSQL); 
 
        } 
    }

    public static DataTable ImportExcelData(string sheetName, string excelFilePath)
    {
        DataTable dt = new DataTable();
        string strDownloadFileName = "";
        string strConn = "";

        strDownloadFileName = excelFilePath;
        strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strDownloadFileName + ";Extended Properties='Excel 8.0;HDR=Yes'";

        using (OleDbConnection conn = new OleDbConnection(strConn))
        {
            using (OleDbDataAdapter da = new OleDbDataAdapter())
            {
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                if (sheetName.Length == 0) sheetName = schemaTable.Rows[0]["TABLE_NAME"].ToString();

                da.SelectCommand = new OleDbCommand("select * from [" + sheetName + "]", conn);
                da.Fill(dt);
            }
        }
        return dt;
    }
} 
