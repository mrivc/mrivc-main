﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MrivcHelpers.Helpers;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace GisTemplateDesigner
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            DAccess.DataModule.Connection1 = new System.Data.SqlClient.SqlConnection("Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Persist Security Info=false;Integrated security=true;MultipleActiveResultSets=true");
        }

        private void prepareBtn_Click(object sender, EventArgs e)
        {
            switch (templatesTabControl.SelectedTab.Name)
            {
                case "mkdTemplateTabPage":
                    {
                        if (uoRadioButton.Checked)
                        {
                            var ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_House @id_company", new[,] { { "@id_company", gisCompanySingle1.SelectedValue } }, "Характеристики МКД", "Подъезды", "Жилые помещения");

                            characteristicsDataGridView.DataSource = ds.Tables["Характеристики МКД"];
                            entrancesDataGridView.DataSource = ds.Tables["Подъезды"];
                            premisesDataGridView.DataSource = ds.Tables["Жилые помещения"];
                        }
                        else
                        {
                            var ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_House_RSO @id_company", new[,] { { "@id_company", gisCompanySingle1.SelectedValue } }, "Характеристики МКД", "Жилые помещения");

                            characteristicsDataGridView.DataSource = ds.Tables["Характеристики МКД"];
                            premisesDataGridView.DataSource = ds.Tables["Жилые помещения"];
                        }
                    }
                    break;
                case "accountTemplateTabPage":
                    {
                        var ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Account @id_company", new[,] { { "@id_company", gisCompanySingle1.SelectedValue } }, "Основные сведения", "Помещение");

                        mainInfoDataGridView.DataSource = ds.Tables["Основные сведения"];
                        accountPremiseDataGridView.DataSource = ds.Tables["Помещение"];
                    }
                    break;
                case "billTemplateTabPage":
                    {
                        var ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Bill @period, @id_company", new[,] { { "@period", periodPicker1.SelectedValue }, { "@id_company", gisCompanySingle1.SelectedValue } }, "Разделы 1-2", "Разделы 3-6", "ДПД");

                        bill1DataGridView.DataSource = ds.Tables["Разделы 1-2"];
                        bill2DataGridView.DataSource = ds.Tables["Разделы 3-6"];
                        dpdDataGridView.DataSource = ds.Tables["ДПД"];
                    }
                    break;
                case "meterTabPage":
                    {
                        var ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Meter @id_company", new[,] { { "@id_company", gisCompanySingle1.SelectedValue } }, "Сведения о ПУ");

                        infoMeterDataGridView.DataSource = ds.Tables["Сведения о ПУ"];
                    }
                    break;
                case "measureTemplateTabPage":
                    {
                        var ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Measure @id_company,@period,@type", new[,] { { "@period", measurePeriodPicker.SelectedValue }, { "@id_company", gisCompanySingle1.SelectedValue }, { "@type", 0 } }, "Импорт показаний ИПУ");

                        measureDataGridView.DataSource = ds.Tables["Импорт показаний ИПУ"];
                    }
                    break;
                case "measureOdpuTemplateTabPage":
                    {
                        var ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Measure @id_company,@period,@type", new[,] { { "@period", measurePeriodPicker.SelectedValue }, { "@id_company", gisCompanySingle1.SelectedValue }, { "@type", 1 } }, "Импорт показаний ОДПУ");

                        measureOdpuDataGridView.DataSource = ds.Tables["Импорт показаний ОДПУ"];
                    }
                    break;
            }
        }

        private void MkdForm_Load(object sender, EventArgs e)
        {
            gisCompanySingle1.Fill(DAccess.DataModule.Connection1.ConnectionString, 0);
            gisCompany1.Fill(DAccess.DataModule.Connection1.ConnectionString,0);
            periodPicker1.Fill(DAccess.DataModule.Connection1.ConnectionString);
            measurePeriodPicker.Fill(DAccess.DataModule.Connection1.ConnectionString);
            measureOdpuPeriodPicker.Fill(DAccess.DataModule.Connection1.ConnectionString);
        }

        private void exportBtn_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                DataSet ds = null;
                var openFilePath = "";

                var templateName = "";

                var companyTable = DAccess.DataModule.GetDataTableByQueryCommand1($"select id_company,name from dbo.company where id_company in ({gisCompany1.SelectedValue})");

                foreach (DataRow row in companyTable.Rows)
                {
                    var company = row[0].ToString();
                    var companyName = row[1].ToString();
                    var headerRomNumber = 2;

                    switch (templatesTabControl.SelectedTab.Name)
                    {
                        case "mkdTemplateTabPage":
                            {
                                if (uoRadioButton.Checked)
                                {
                                    ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_House @id_company", new[,] { { "@id_company", company } }, "Характеристики МКД", "Подъезды", "Жилые помещения");
                                    openFilePath = $@"\Templates\Шаблон импорт сведений о МКД-УО-10.0.2.1.xlsx";
                                    templateName = "МКД УО";
                                }
                                else
                                {
                                    ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_House_RSO @id_company", new[,] { { "@id_company", company } }, "Характеристики МКД", "Жилые помещения");
                                    openFilePath = $@"\Templates\Шаблон импорт сведений о МКД-РСО-РОКР-10.0.1.1.xlsx";
                                    templateName = "МКД РСО";
                                }
                            }
                            break;
                        case "accountTemplateTabPage":
                            {
                                ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Account @id_company", new[,] { { "@id_company", company } }, "Основные сведения", "Помещение");
                                openFilePath = $@"\Templates\Шаблон импорта ЛС-10.0.2.1.xlsx";
                                templateName = "ЛС";
                            }
                            break;
                        case "billTemplateTabPage":
                            {
                                ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Bill @period, @id_company", new[,] { { "@period", periodPicker1.SelectedValue }, { "@id_company", company } }, "Разделы 1-2", "Разделы 3-6", "ДПД");
                                openFilePath = DAccess.DataModule.ExecuteScalarQueryCommand1("select '\\Templates\\Bills\\Шаблон импорта платежных документов ТСЖ Карла Маркса 61.xlsx'", new[,] { { "@id_company", company } }).ToString();
                                templateName = "ПД";
                                headerRomNumber = 4;
                            }
                            break;
                        case "meterTabPage":
                            {
                                ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Meter @id_company", new[,] { { "@id_company", company } }, "Сведения о ПУ");
                                openFilePath = $@"\Templates\Шаблон импорта сведений о ПУ-10.0.0.3.xlsx";
                                templateName = "ПУ";
                                headerRomNumber = 1;
                            }
                            break;
                        case "measureTemplateTabPage":
                            {
                                ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Measure @id_company,@period,@type", new[,] { { "@period", measurePeriodPicker.SelectedValue }, { "@id_company", gisCompanySingle1.SelectedValue }, { "@type", 0 } }, "Импорт показаний ИПУ");
                                openFilePath = $@"\Templates\Шаблон импорта показаний ИПУ-9.0.0.2.xlsx";
                                templateName = "ИПУ";
                            }
                            break;
                        case "measureOdpuTemplateTabPage":
                            {
                                ds = DAccess.DataModule.GetDataSetByQueryCommand1("exec GIS.Prepare_Template_Measure @id_company,@period,@type", new[,] { { "@period", measurePeriodPicker.SelectedValue }, { "@id_company", gisCompanySingle1.SelectedValue }, { "@type", 1 } }, "Импорт показаний ОДПУ");
                                openFilePath = $@"\Templates\Шаблон импорта показаний ОДПУ-9.0.0.2.xlsx";
                                templateName = "ОДПУ";
                            }
                            break;
                    }

                    var fileName = FileHelper.CreateCorrectFileName(companyName);

                    var saveFilePath = $@"{folderBrowserDialog1.SelectedPath}\{templateName} {fileName}.xlsx";

                    string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    var filePath = executableLocation + openFilePath;

                    //File.Copy(filePath, saveFilePath, true);

                    var export = new ExcelExport(filePath);
                    export.LoadDataSet(ds, headerRomNumber);
                    export.SaveAs(saveFilePath);
                }
            }
        }

        private void uoRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (uoRadioButton.Checked)
            {
                gisCompany1.Fill(DAccess.DataModule.Connection1.ConnectionString, 0);
                gisCompanySingle1.Fill(DAccess.DataModule.Connection1.ConnectionString, 0);
            }
            else
            {
                gisCompany1.Fill(DAccess.DataModule.Connection1.ConnectionString, 1);
                gisCompanySingle1.Fill(DAccess.DataModule.Connection1.ConnectionString, 1);
            }
        }

        private void openImportTemplateBtn_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ExcelImport ei = new ExcelImport(openFileDialog1.FileName);

                switch (importTabControl.SelectedTab.Name)
                {
                    case "accountImportTabPage":
                        {
                            var dt = ei.SaveToDataTable("ЕЛС", 2);
                            dt.Columns[0].ColumnName = "UniqueNumber";
                            dt.Columns[1].ColumnName = "Account";
                            dt.Columns[2].ColumnName = "UnifiedAccountNumber";
                            dt.Columns[3].ColumnName = "IDGKU";
                            accountImportDataGridView.DataSource = dt;
                            var dt1 = ei.SaveToDataTable("Идентификаторы помещений ГИСЖКХ", 2);
                            dt1.Columns[10].ColumnName = "UniqueNumber";
                            premisesImportDataGridView.DataSource = dt1;
                            break;
                        }
                    case "meterImportTabPage":
                        {
                            var dt = ei.SaveToDataTable("Сведения о ПУ", 1);
                            dt.Columns[4].ColumnName = "ID_Object";
                            dt.Columns[1].ColumnName = "ID_Meter";
                            meterImportDataGridView.DataSource = dt;
                            break;
                        }
                }
            }  
        }

        private void importTemplateBtn_Click(object sender, EventArgs e)
        {
            switch (importTabControl.SelectedTab.Name)
            {
                case "accountImportTabPage":
                    {
                        var dt = (DataTable)accountImportDataGridView.DataSource;
                        dt.TableName = "ImportTemplateAccount";
                        DAccess.DataModule.CopyDataTableToTempDB(dt);
                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.Import_Template_Account");
                        dt = (DataTable)premisesImportDataGridView.DataSource;
                        dt.TableName = "ImportTemplatePremises";
                        DAccess.DataModule.CopyDataTableToTempDB(dt);
                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.Import_Template_Premises");
                        break;
                    }
                case "meterImportTabPage":
                    {
                        var dt = (DataTable)meterImportDataGridView.DataSource;
                        dt.TableName = "ImportTemplateMeter";
                        DAccess.DataModule.CopyDataTableToTempDB(dt);
                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.Import_Template_Meter");
                        break;
                    }
            }
        }
    }
}
