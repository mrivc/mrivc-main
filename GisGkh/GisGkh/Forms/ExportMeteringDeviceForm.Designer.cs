﻿namespace GisGkh
{
    partial class ExportMeteringDeviceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportMeteringDeviceForm));
            this.meterDataGrid = new System.Windows.Forms.DataGridView();
            this.exportBtn = new System.Windows.Forms.Button();
            this.exportToDbBtn = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.meterDataGrid)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // meterDataGrid
            // 
            this.meterDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.meterDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meterDataGrid.Location = new System.Drawing.Point(3, 3);
            this.meterDataGrid.Name = "meterDataGrid";
            this.meterDataGrid.Size = new System.Drawing.Size(1191, 479);
            this.meterDataGrid.TabIndex = 2;
            // 
            // exportBtn
            // 
            this.exportBtn.Location = new System.Drawing.Point(299, 12);
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(216, 23);
            this.exportBtn.TabIndex = 3;
            this.exportBtn.Text = "Экспортировать";
            this.exportBtn.UseVisualStyleBackColor = true;
            this.exportBtn.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // exportToDbBtn
            // 
            this.exportToDbBtn.Location = new System.Drawing.Point(299, 41);
            this.exportToDbBtn.Name = "exportToDbBtn";
            this.exportToDbBtn.Size = new System.Drawing.Size(216, 23);
            this.exportToDbBtn.TabIndex = 4;
            this.exportToDbBtn.Text = "Экспортировать в базу данных";
            this.exportToDbBtn.UseVisualStyleBackColor = true;
            this.exportToDbBtn.Click += new System.EventHandler(this.exportToDbBtn_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 73);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1205, 511);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.meterDataGrid);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1197, 485);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Счетчики";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ExportMeteringDeviceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1208, 586);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.exportToDbBtn);
            this.Controls.Add(this.exportBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportMeteringDeviceForm";
            this.Text = "Экспорт ПУ";
            this.Load += new System.EventHandler(this.ExportMeteringDeviceForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.meterDataGrid)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView meterDataGrid;
        private System.Windows.Forms.Button exportBtn;
        private System.Windows.Forms.Button exportToDbBtn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}