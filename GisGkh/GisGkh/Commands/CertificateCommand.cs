﻿using System;
using System.Collections.Generic;
using Xades.Helpers;
using GisGkh.Configurations.Options;

namespace GisGkh.Commands
{
    public class CertificateCommand : CommandBase<CertificateOptions>
    {
        public CertificateCommand(CertificateOptions option) : base(option)
        {
        }

        protected override string OnExecute(CertificateOptions option, string xmlBody, string senderID)
        {
            Option.Verbose = true;
            var certificates = CertificateHelper.GetCertificates();
            Info("Информация о сертификатах");
            foreach (var cert in certificates)
            {
                Info(string.Format("Субъект: {0}", cert.Subject));
                Info(string.Format("Отпечаток: {0}", cert.Thumbprint));
            }
            return "";
        }
    }
}