﻿namespace PaymentStatementParser
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.filePathTextBox = new System.Windows.Forms.TextBox();
            this.parseButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.providersComboBox = new System.Windows.Forms.ComboBox();
            this.referenceButton = new System.Windows.Forms.Button();
            this.isCompanyGroupCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // filePathTextBox
            // 
            this.filePathTextBox.AllowDrop = true;
            this.filePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filePathTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.filePathTextBox.Location = new System.Drawing.Point(12, 12);
            this.filePathTextBox.Name = "filePathTextBox";
            this.filePathTextBox.Size = new System.Drawing.Size(354, 26);
            this.filePathTextBox.TabIndex = 0;
            this.filePathTextBox.TextChanged += new System.EventHandler(this.filePathTextBox_TextChanged);
            this.filePathTextBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.filePathTextBox_DragDrop);
            this.filePathTextBox.DragOver += new System.Windows.Forms.DragEventHandler(this.filePathTextBox_DragOver);
            // 
            // parseButton
            // 
            this.parseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.parseButton.Location = new System.Drawing.Point(175, 123);
            this.parseButton.Name = "parseButton";
            this.parseButton.Size = new System.Drawing.Size(150, 50);
            this.parseButton.TabIndex = 1;
            this.parseButton.Text = "Обработать";
            this.parseButton.UseVisualStyleBackColor = true;
            this.parseButton.Click += new System.EventHandler(this.parseButton_Click);
            // 
            // addButton
            // 
            this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addButton.Location = new System.Drawing.Point(372, 12);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(100, 26);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Добавить";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // providersComboBox
            // 
            this.providersComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.providersComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.providersComboBox.FormattingEnabled = true;
            this.providersComboBox.Items.AddRange(new object[] {
            "МРИВЦ",
            "МЭС",
            "МТЭЦ_1",
            "МТЭЦ_2"});
            this.providersComboBox.Location = new System.Drawing.Point(175, 70);
            this.providersComboBox.Name = "providersComboBox";
            this.providersComboBox.Size = new System.Drawing.Size(150, 24);
            this.providersComboBox.TabIndex = 3;
            this.providersComboBox.SelectedIndexChanged += new System.EventHandler(this.providersComboBox_SelectedIndexChanged);
            // 
            // referenceButton
            // 
            this.referenceButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.referenceButton.BackgroundImage = global::PaymentStatementParser.Properties.Resources.work;
            this.referenceButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.referenceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.referenceButton.Location = new System.Drawing.Point(447, 148);
            this.referenceButton.Name = "referenceButton";
            this.referenceButton.Size = new System.Drawing.Size(25, 25);
            this.referenceButton.TabIndex = 4;
            this.referenceButton.UseVisualStyleBackColor = false;
            this.referenceButton.Click += new System.EventHandler(this.referenceButton_Click);
            // 
            // isCompanyGroupCheckBox
            // 
            this.isCompanyGroupCheckBox.AutoSize = true;
            this.isCompanyGroupCheckBox.Location = new System.Drawing.Point(175, 100);
            this.isCompanyGroupCheckBox.Name = "isCompanyGroupCheckBox";
            this.isCompanyGroupCheckBox.Size = new System.Drawing.Size(172, 17);
            this.isCompanyGroupCheckBox.TabIndex = 5;
            this.isCompanyGroupCheckBox.Text = "Группировать по компаниям";
            this.isCompanyGroupCheckBox.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 186);
            this.Controls.Add(this.isCompanyGroupCheckBox);
            this.Controls.Add(this.referenceButton);
            this.Controls.Add(this.providersComboBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.parseButton);
            this.Controls.Add(this.filePathTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "Обработка платежных ведомостей";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox filePathTextBox;
        private System.Windows.Forms.Button parseButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ComboBox providersComboBox;
        private System.Windows.Forms.Button referenceButton;
        private System.Windows.Forms.CheckBox isCompanyGroupCheckBox;
    }
}

