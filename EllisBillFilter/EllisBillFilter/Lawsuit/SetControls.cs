﻿using System;
using System.Data;
using System.Windows.Forms;

namespace EllisBillFilter.Lawsuit
{
    static public class SetControls
    {
        static public void SetCBoxSelectedValue(ComboBox cBox, string id)
        {
            for (int i = 0; i < cBox.Items.Count; i++)
            {
                if ((cBox.Items[i] as DataRowView)[0].ToString() == id)
                {
                    cBox.SelectedIndex = i;
                    break;
                }
            }
        }

        static public void SetDatePickerValue(DateTimePicker datePicker, string date)
        {
            if (date == "")
            {
                datePicker.Checked = false;
            }
            else
            {
                datePicker.Value = DateTime.Parse(date);
            }
        }

        static public void SetCheckedListBoxValues(CheckedListBox cListBox, string[] values)
        {
            for (int i = 0; i < cListBox.Items.Count; i++)
            {
                cListBox.SetItemCheckState(i, CheckState.Unchecked);
                foreach (string value in values)
                {
                    if ((cListBox.Items[i] as DataRowView)[0].ToString() == value)
                    {
                        cListBox.SetItemCheckState(i, CheckState.Checked);
                        break;
                    }
                }
            }
        }
    }
}
