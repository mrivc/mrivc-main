﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class WithdrawPaymentDocument : MrivcImportCommand, ISingleHouseCommand
    {
     
        public string FiasHouseGuid { get; set; }

        public int Period { get; set; }

        public string AccountGuid { get; set; }

        public WithdrawPaymentDocument(string senderID, int period) : base("withdrawPaymentDocument_root", "BillsAsync", senderID)
        {
            _maxImportValue = 1000;
            _importValueName = "WithdrawPaymentDocument";
            Period = period;
        }

        protected override void CreateXmlBody()
        {
            var billsToWithdraw = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.Prepare_Withdraw_Bill @SenderID,@FiasHouseGuid,@period", new[,] { { "@SenderID", SenderID }, { "@FiasHouseGuid", FiasHouseGuid }, { "@period", Period.ToString() } });

            foreach (DataRow bill in billsToWithdraw.Rows)
            {
                var values1 = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new string[] { "PaymentDocumentID" },"paymentDocumentID")
                    };

                var addedBill = AddChildElement(bill, Service, "withdrawPaymentDocument", null, values1, null, null, "importPaymentDocumentRequest");

                CounterCheck();
            }
        }

        public void Execute()
        {
            var exportPaymentDocument = new ExportPaymentDocument(SenderID, Period.ToString());
            exportPaymentDocument.FiasHouseGuid = FiasHouseGuid;
            exportPaymentDocument.Description = Description;
            exportPaymentDocument.AddToReport = AddToReport;
            exportPaymentDocument.Execute();

            base.Execute();

            exportPaymentDocument.Execute();
        }

        public DataSet PrepareToDataSet()
        {
            return new DataSet();
        }
    }
}
