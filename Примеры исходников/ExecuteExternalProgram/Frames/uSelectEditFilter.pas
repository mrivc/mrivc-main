Unit uSelectEditFilter;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, cxLookAndFeelPainters, cxGraphics, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxControls, cxContainer, cxEdit, cxGroupBox;

Const
  Version = 1.1;

Type
  TSelectEditFilter = Class(TFrame)
    adsList: TADODataSet;
    GB: TcxGroupBox;
    CB: TcxComboBox;
    Procedure CBCloseUp(Sender: TObject);
    Procedure CBKeyPress(Sender: TObject; Var Key: Char);
  Private
    { Private declarations }
    FConnection: TADOConnection;
    FMyOnChange: TNotifyEvent;
    Procedure SetConnection(AConnection: TADOConnection);
    Procedure SetCaption(ACaption: TCaption);
    Procedure Enable;
    Procedure Disable;
  Public
    { Public declarations }
    Procedure Initialize(AConnection: TADOConnection; AQuery: String; ACaption: TCaption = '');
    Procedure InitializeByAccountOwner(AConnection: TADOConnection; AID_Account: Int64; APeriod: Integer; ACaption:
      TCaption = '');
    Procedure LoadListFromQuery(AQuery: String);
    Function GetName: String;
    Function GetID: String; Overload;
    Procedure SetID(Value: String);
    Function GetCount: Integer;
    Property MyOnChange: TNotifyEvent Read FMyOnChange Write FMyOnChange;
    Property ID: String Read GetID Write SetID;
  End;

Implementation

{$R *.dfm}

Uses uReportUtils, Math;

{ TSelectEditFilter }

Procedure TSelectEditFilter.CBCloseUp(Sender: TObject);
Begin
  If Assigned(FMyOnChange) Then FMyOnChange(Sender);
End;

Procedure TSelectEditFilter.CBKeyPress(Sender: TObject; Var Key: Char);
Begin
  If Key = #13 Then
    If Assigned(FMyOnChange) Then FMyOnChange(Sender);
End;

Procedure TSelectEditFilter.Disable;
Begin
  //CB.Text := '����������� ������!';
  //GB.Enabled := False;
End;

Procedure TSelectEditFilter.Enable;
Begin
  //GB.Enabled := True;
End;

Function TSelectEditFilter.GetCount: Integer;
Begin
  Result := CB.Properties.Items.Count;
End;

Function TSelectEditFilter.GetID: String;
Begin
  If CB.Properties.Items.Count > 0 Then
    Result := TStringObj(CB.Properties.Items.Objects[CB.ItemIndex]).Text
  Else
    Result := '';
End;

Function TSelectEditFilter.GetName: String;
Begin
  Result := CB.Text;
End;

Procedure TSelectEditFilter.Initialize(AConnection: TADOConnection;
  AQuery: String; ACaption: TCaption);
Begin
  SetConnection(AConnection);
  LoadListFromQuery(AQuery);
  SetCaption(ACaption);
End;

Procedure TSelectEditFilter.InitializeByAccountOwner(AConnection: TADOConnection; AID_Account: Int64; APeriod: Integer;
  ACaption: TCaption = '');
Begin
  SetConnection(AConnection);
  LoadListFromQuery(
    'SELECT ID_Person, RTRIM(Last_Name + '' '' + First_Name + '' '' + Second_name) AS [���] ' +
    'FROM dbo.VAccount_Owner WHERE ID_Account = ' + IntToStr(AID_Account) + ' AND Period = ' + IntToStr(APeriod));
  SetCaption(ACaption);
End;

Procedure TSelectEditFilter.LoadListFromQuery(AQuery: String);
Var
  I: Integer;
Begin
  CB.Properties.Items.BeginUpdate;
  For I := 0 To CB.Properties.Items.Count - 1 Do
    Begin
      If Assigned(CB.Properties.Items.Objects[I]) Then CB.Properties.Items.Objects[I].Free;
    End;
  CB.Properties.Items.Clear;

  I := 0;
  If AQuery <> '' Then
    With adsList Do
      Begin
        Close;
        CommandText := AQuery;
        Try
          Open;
          While Not Eof Do
            Begin
              CB.Properties.Items.AddObject(Fields[1].AsString, TStringObj.Create(Fields[0].AsString));
              Inc(I);
              Next;
            End;
          CB.Properties.DropDownRows := Min(32, I + 1);
        Except
          I := 0;
        End;
      End;
  If I = 0 Then
    Begin
      Disable
    End
  Else
    Begin
      Enable;
      CB.ItemIndex := 0;
    End;
  CB.Properties.Items.EndUpdate;
End;

Procedure TSelectEditFilter.SetCaption(ACaption: TCaption);
Begin
  If ACaption <> '' Then
    GB.Caption := ACaption;
End;

Procedure TSelectEditFilter.SetConnection(AConnection: TADOConnection);
Begin
  If FConnection <> AConnection Then
    Begin
      FConnection := AConnection;
      adsList.Connection := FConnection;
    End;
End;

Procedure TSelectEditFilter.SetID(Value: String);
Var
  I: Integer;
Begin
  For I := 0 To CB.Properties.Items.Count - 1 Do
    If TStringObj(CB.Properties.Items.Objects[I]).Text = Value Then
      Begin
        CB.ItemIndex := I;
        Break;
      End;
End;

End.

