Unit uLibrary;
{$HINTS OFF}
Interface
Uses
  Controls,
  Forms,
  Windows,
  Classes,
  Variants,
  uMDIChild,
  uPropertyCollection,
  uProperties;

Var
  OldApplication: TApplication;
  OldScreen: TScreen;
  OldHintWindowClass: THintWindowClass;

Procedure ActionProc(Reason: Integer);
Function GetOwnerAndPrepare(AOptions: TPropertyCollection): TComponent;
Procedure ExecuteMDIChild(AOptions: TPropertyCollection; AFormClass: TMDIChildFormClass);

Type TPropertyCollection = uPropertyCollection.TPropertyCollection;

Implementation

Procedure ActionProc(Reason: Integer);
Begin
  If (Reason = DLL_PROCESS_DETACH) Then
    Begin
      If Assigned(OldApplication) Then Application := OldApplication;
      If Assigned(OldScreen) Then Screen := OldScreen;
      If Assigned(OldHintWindowClass) Then HintWindowClass := OldHintWindowClass;
    End;
End;

Function GetOwnerAndPrepare(AOptions: TPropertyCollection): TComponent;
Var
  S: Variant;
Begin
  S := AOptions[pApplication];
  If (Not Assigned(OldApplication)) Then OldApplication := Application;
  If (Not VarIsNull(S)) Then Integer(Application) := S;

  S := AOptions[pScreen];
  If (Not Assigned(OldScreen)) Then OldScreen := Screen;
  If (Not VarIsNull(S)) Then Integer(Screen) := S;

  S := AOptions[pHintWindowClass];
  If (Not Assigned(OldHintWindowClass)) Then OldHintWindowClass := HintWindowClass;
  If (Not VarIsNull(S)) Then Integer(HintWindowClass) := S;

  If Assigned(Application) Then
    Result := Application.MainForm
  Else
    Result := Nil;
End;

Procedure ExecuteMDIChild(AOptions: TPropertyCollection; AFormClass: TMDIChildFormClass);
Var
  ResultForm: TMDIChildForm;
Begin
  Try
    ResultForm := AFormClass.Create(GetOwnerAndPrepare(AOptions), AOptions);
    ResultForm.Show;
  Except
    ResultForm := Nil;
    Raise;
  End;
  AOptions[pResultForm] := Integer(ResultForm);
End;

Begin
  DllProc := @ActionProc;
{$HINTS ON}
End.
