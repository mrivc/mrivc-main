﻿namespace EllisBillFilter
{
    partial class GridBillFilterCurrent
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GridBillFilterCurrent));
            this.gridClientDetail = new EllisBillFilter.BaseGrid();
            this.cmsClientDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miClientDetail_PrintBills = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miClientDetail_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.miClientDetail_Add = new System.Windows.Forms.ToolStripMenuItem();
            this.miClientDetail_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miClientDetail_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.miClientDetail_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.miClientDetail_Close = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miClientDetail_OpenMeasureForService = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.miClientDetail_ProviderSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.miClientDetail_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.miClientDetail_Export = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.miClientDetail_ServiceInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.miClientDetail_RecordHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.clientDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gridViewClientDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Client_Detail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riClientPayReceiver = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.payReceiverCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Derive_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riDeriveCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.deriveCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Bill_Filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riBillFilter = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.billFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Filter_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riFilterType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.filterTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colBegin_Period = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colEnd_Period = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdvance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoundTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrefix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShow_Meter_For_Services = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmpty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDerive_Company_Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeriveCompanyHasHouses = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBill_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.clientDetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tabControlSub = new DevExpress.XtraTab.XtraTabControl();
            this.tabAgreement = new DevExpress.XtraTab.XtraTabPage();
            this.gridClientDetailAgreement1 = new EllisBillFilter.GridClientDetailAgreement();
            this.tabComment = new DevExpress.XtraTab.XtraTabPage();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.miClientDetail_SimpleCopy = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridClientDetail)).BeginInit();
            this.cmsClientDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riClientPayReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeriveCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFilterType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlSub)).BeginInit();
            this.tabControlSub.SuspendLayout();
            this.tabAgreement.SuspendLayout();
            this.tabComment.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridClientDetail
            // 
            this.gridClientDetail.ContextMenuStrip = this.cmsClientDetail;
            this.gridClientDetail.DataSource = this.clientDetailBindingSource;
            this.gridClientDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridClientDetail.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridClientDetail.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridClientDetail.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridClientDetail.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridClientDetail.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridClientDetail.Location = new System.Drawing.Point(0, 0);
            this.gridClientDetail.MainView = this.gridViewClientDetail;
            this.gridClientDetail.Name = "gridClientDetail";
            this.gridClientDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riClientPayReceiver,
            this.riDeriveCompany,
            this.riPeriod,
            this.riBillFilter,
            this.riFilterType});
            this.gridClientDetail.Size = new System.Drawing.Size(887, 449);
            this.gridClientDetail.TabIndex = 2;
            this.gridClientDetail.TrimCellValueBeforeValidate = true;
            this.gridClientDetail.UseEmbeddedNavigator = true;
            this.gridClientDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewClientDetail});
            // 
            // cmsClientDetail
            // 
            this.cmsClientDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miClientDetail_PrintBills,
            this.toolStripSeparator1,
            this.miClientDetail_Edit,
            this.toolStripSeparator4,
            this.miClientDetail_Add,
            this.miClientDetail_Delete,
            this.toolStripSeparator3,
            this.miClientDetail_Copy,
            this.miClientDetail_SimpleCopy,
            this.toolStripSeparator7,
            this.miClientDetail_Open,
            this.miClientDetail_Close,
            this.toolStripSeparator2,
            this.miClientDetail_OpenMeasureForService,
            this.toolStripSeparator5,
            this.miClientDetail_ProviderSettings,
            this.toolStripSeparator8,
            this.miClientDetail_Refresh,
            this.miClientDetail_Export,
            this.toolStripSeparator6,
            this.miClientDetail_ServiceInfo,
            this.miClientDetail_RecordHistory});
            this.cmsClientDetail.Name = "cmsClientDetail";
            this.cmsClientDetail.Size = new System.Drawing.Size(266, 382);
            // 
            // miClientDetail_PrintBills
            // 
            this.miClientDetail_PrintBills.Image = global::EllisBillFilter.Properties.Resources.print;
            this.miClientDetail_PrintBills.Name = "miClientDetail_PrintBills";
            this.miClientDetail_PrintBills.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_PrintBills.Text = "Печать квитанций";
            this.miClientDetail_PrintBills.Click += new System.EventHandler(this.miClientDetail_PrintBills_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(262, 6);
            // 
            // miClientDetail_Edit
            // 
            this.miClientDetail_Edit.Image = global::EllisBillFilter.Properties.Resources.record_edit;
            this.miClientDetail_Edit.Name = "miClientDetail_Edit";
            this.miClientDetail_Edit.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_Edit.Text = "Редактировать запись";
            this.miClientDetail_Edit.Click += new System.EventHandler(this.miClientDetail_Edit_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(262, 6);
            // 
            // miClientDetail_Add
            // 
            this.miClientDetail_Add.Image = global::EllisBillFilter.Properties.Resources.record_add;
            this.miClientDetail_Add.Name = "miClientDetail_Add";
            this.miClientDetail_Add.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_Add.Text = "Добавить запись";
            this.miClientDetail_Add.Click += new System.EventHandler(this.miClientDetail_Add_Click);
            // 
            // miClientDetail_Delete
            // 
            this.miClientDetail_Delete.Image = global::EllisBillFilter.Properties.Resources.record_delete;
            this.miClientDetail_Delete.Name = "miClientDetail_Delete";
            this.miClientDetail_Delete.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_Delete.Text = "Удалить запись";
            this.miClientDetail_Delete.Click += new System.EventHandler(this.miClientDetail_Delete_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(262, 6);
            // 
            // miClientDetail_Copy
            // 
            this.miClientDetail_Copy.Name = "miClientDetail_Copy";
            this.miClientDetail_Copy.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_Copy.Text = "Скопировать запись (с домами)";
            this.miClientDetail_Copy.Click += new System.EventHandler(this.miClientDetail_Copy_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(262, 6);
            // 
            // miClientDetail_Open
            // 
            this.miClientDetail_Open.Image = global::EllisBillFilter.Properties.Resources.period_open;
            this.miClientDetail_Open.Name = "miClientDetail_Open";
            this.miClientDetail_Open.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_Open.Text = "Открыть период кода фильтра";
            this.miClientDetail_Open.Click += new System.EventHandler(this.miClientDetail_Open_Click);
            // 
            // miClientDetail_Close
            // 
            this.miClientDetail_Close.Image = global::EllisBillFilter.Properties.Resources.period_close;
            this.miClientDetail_Close.Name = "miClientDetail_Close";
            this.miClientDetail_Close.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_Close.Text = "Закрыть период кода фильтра";
            this.miClientDetail_Close.Click += new System.EventHandler(this.miClientDetail_Close_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(262, 6);
            // 
            // miClientDetail_OpenMeasureForService
            // 
            this.miClientDetail_OpenMeasureForService.Image = global::EllisBillFilter.Properties.Resources.services;
            this.miClientDetail_OpenMeasureForService.Name = "miClientDetail_OpenMeasureForService";
            this.miClientDetail_OpenMeasureForService.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_OpenMeasureForService.Text = "Открыть ввод показаний для услуг";
            this.miClientDetail_OpenMeasureForService.Click += new System.EventHandler(this.miClientDetail_OpenMeasureForService_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(262, 6);
            // 
            // miClientDetail_ProviderSettings
            // 
            this.miClientDetail_ProviderSettings.Name = "miClientDetail_ProviderSettings";
            this.miClientDetail_ProviderSettings.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_ProviderSettings.Text = "Настроить поставщиков";
            this.miClientDetail_ProviderSettings.Click += new System.EventHandler(this.miClientDetail_ProviderSettings_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(262, 6);
            // 
            // miClientDetail_Refresh
            // 
            this.miClientDetail_Refresh.Image = global::EllisBillFilter.Properties.Resources.refresh;
            this.miClientDetail_Refresh.Name = "miClientDetail_Refresh";
            this.miClientDetail_Refresh.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_Refresh.Text = "Обновить";
            this.miClientDetail_Refresh.Click += new System.EventHandler(this.miClientDetail_Refresh_Click);
            // 
            // miClientDetail_Export
            // 
            this.miClientDetail_Export.Image = global::EllisBillFilter.Properties.Resources.export;
            this.miClientDetail_Export.Name = "miClientDetail_Export";
            this.miClientDetail_Export.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_Export.Text = "Экспорт в Excel";
            this.miClientDetail_Export.Click += new System.EventHandler(this.miClientDetail_Export_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(262, 6);
            // 
            // miClientDetail_ServiceInfo
            // 
            this.miClientDetail_ServiceInfo.Name = "miClientDetail_ServiceInfo";
            this.miClientDetail_ServiceInfo.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_ServiceInfo.Text = "Служебная информация";
            this.miClientDetail_ServiceInfo.Click += new System.EventHandler(this.miClientDetail_ServiceInfo_Click);
            // 
            // miClientDetail_RecordHistory
            // 
            this.miClientDetail_RecordHistory.Name = "miClientDetail_RecordHistory";
            this.miClientDetail_RecordHistory.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_RecordHistory.Text = "История изменений";
            this.miClientDetail_RecordHistory.Click += new System.EventHandler(this.miClientDetail_RecordHistory_Click);
            // 
            // clientDetailBindingSource
            // 
            this.clientDetailBindingSource.DataMember = "ClientDetail";
            this.clientDetailBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewClientDetail
            // 
            this.gridViewClientDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewClientDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewClientDetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewClientDetail.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewClientDetail.ColumnPanelRowHeight = 40;
            this.gridViewClientDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Client_Detail,
            this.colID_Client,
            this.colID_Derive_Company,
            this.colID_Bill_Filter,
            this.colID_Filter_Type,
            this.colBegin_Period,
            this.colEnd_Period,
            this.colAdvance,
            this.colRoundTo,
            this.colPrefix,
            this.colShow_Meter_For_Services,
            this.colComment,
            this.colEmpty,
            this.colDerive_Company_Code,
            this.colDeriveCompanyHasHouses,
            this.colBill_Name});
            this.gridViewClientDetail.CustomizationFormBounds = new System.Drawing.Rectangle(1055, 529, 216, 318);
            this.gridViewClientDetail.GridControl = this.gridClientDetail;
            this.gridViewClientDetail.Name = "gridViewClientDetail";
            this.gridViewClientDetail.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewClientDetail.OptionsBehavior.Editable = false;
            this.gridViewClientDetail.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewClientDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewClientDetail.OptionsView.ColumnAutoWidth = false;
            this.gridViewClientDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colID_Client_Detail, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewClientDetail.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewClientDetail_RowStyle);
            this.gridViewClientDetail.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewClientDetail_InitNewRow);
            this.gridViewClientDetail.EndGrouping += new System.EventHandler(this.gridViewClientDetail_EndGrouping);
            this.gridViewClientDetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewClientDetail_FocusedRowChanged);
            this.gridViewClientDetail.ColumnFilterChanged += new System.EventHandler(this.gridViewClientDetail_ColumnFilterChanged);
            this.gridViewClientDetail.DoubleClick += new System.EventHandler(this.gridViewClientDetail_DoubleClick);
            // 
            // colID_Client_Detail
            // 
            this.colID_Client_Detail.Caption = "ИД";
            this.colID_Client_Detail.FieldName = "ID_Client_Detail";
            this.colID_Client_Detail.Name = "colID_Client_Detail";
            // 
            // colID_Client
            // 
            this.colID_Client.Caption = "Получатель платежа*";
            this.colID_Client.ColumnEdit = this.riClientPayReceiver;
            this.colID_Client.FieldName = "ID_Client";
            this.colID_Client.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colID_Client.Name = "colID_Client";
            this.colID_Client.Visible = true;
            this.colID_Client.VisibleIndex = 0;
            this.colID_Client.Width = 146;
            // 
            // riClientPayReceiver
            // 
            this.riClientPayReceiver.AutoHeight = false;
            this.riClientPayReceiver.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riClientPayReceiver.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Наименование", 250, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Company", "ID_Company", 85, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Provider", "Is_Provider", 65, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Derive_Company", "Is_Derive_Company", 107, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Pay_Receiver", "Is_Pay_Receiver", 91, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Meter_Future_Period_Day", "Meter_Future_Period_Day", 137, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Load_Measure_In_Future", "Load_Measure_In_Future", 134, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far)});
            this.riClientPayReceiver.DataSource = this.payReceiverCompanyBindingSource;
            this.riClientPayReceiver.DisplayMember = "Name";
            this.riClientPayReceiver.Name = "riClientPayReceiver";
            this.riClientPayReceiver.NullText = "";
            this.riClientPayReceiver.PopupFormMinSize = new System.Drawing.Size(300, 270);
            this.riClientPayReceiver.ValueMember = "ID_Company";
            // 
            // payReceiverCompanyBindingSource
            // 
            this.payReceiverCompanyBindingSource.DataMember = "PayReceiverCompany";
            this.payReceiverCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Derive_Company
            // 
            this.colID_Derive_Company.Caption = "Управляющая компания*";
            this.colID_Derive_Company.ColumnEdit = this.riDeriveCompany;
            this.colID_Derive_Company.FieldName = "ID_Derive_Company";
            this.colID_Derive_Company.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colID_Derive_Company.Name = "colID_Derive_Company";
            this.colID_Derive_Company.Visible = true;
            this.colID_Derive_Company.VisibleIndex = 1;
            this.colID_Derive_Company.Width = 159;
            // 
            // riDeriveCompany
            // 
            this.riDeriveCompany.AutoHeight = false;
            this.riDeriveCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riDeriveCompany.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Наименование", 250, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Company", "ID_Company", 85, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Provider", "Is_Provider", 65, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Derive_Company", "Is_Derive_Company", 107, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Pay_Receiver", "Is_Pay_Receiver", 91, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Meter_Future_Period_Day", "Meter_Future_Period_Day", 137, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Load_Measure_In_Future", "Load_Measure_In_Future", 134, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far)});
            this.riDeriveCompany.DataSource = this.deriveCompanyBindingSource;
            this.riDeriveCompany.DisplayMember = "Name";
            this.riDeriveCompany.Name = "riDeriveCompany";
            this.riDeriveCompany.NullText = "";
            this.riDeriveCompany.PopupFormMinSize = new System.Drawing.Size(300, 270);
            this.riDeriveCompany.ValueMember = "ID_Company";
            // 
            // deriveCompanyBindingSource
            // 
            this.deriveCompanyBindingSource.DataMember = "DeriveCompany";
            this.deriveCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Bill_Filter
            // 
            this.colID_Bill_Filter.Caption = "Код фильтра*";
            this.colID_Bill_Filter.ColumnEdit = this.riBillFilter;
            this.colID_Bill_Filter.FieldName = "ID_Bill_Filter";
            this.colID_Bill_Filter.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colID_Bill_Filter.Name = "colID_Bill_Filter";
            this.colID_Bill_Filter.Visible = true;
            this.colID_Bill_Filter.VisibleIndex = 2;
            this.colID_Bill_Filter.Width = 134;
            // 
            // riBillFilter
            // 
            this.riBillFilter.AutoHeight = false;
            this.riBillFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riBillFilter.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Bill_Filter", "ID_Bill_Filter", 82, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Bill_Filter", "Bill_Filter", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tag", "Tag", 28, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "Info", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("IsActivate", "Is Activate", 62, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Account_Type", "Account_Type", 79, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Position", "Position", 47, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far)});
            this.riBillFilter.DataSource = this.billFilterBindingSource;
            this.riBillFilter.DisplayMember = "Bill_Filter";
            this.riBillFilter.Name = "riBillFilter";
            this.riBillFilter.NullText = "";
            this.riBillFilter.PopupFormMinSize = new System.Drawing.Size(250, 200);
            this.riBillFilter.PopupWidth = 250;
            this.riBillFilter.ShowFooter = false;
            this.riBillFilter.ShowHeader = false;
            this.riBillFilter.ValueMember = "ID_Bill_Filter";
            // 
            // billFilterBindingSource
            // 
            this.billFilterBindingSource.DataMember = "BillFilter";
            this.billFilterBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Filter_Type
            // 
            this.colID_Filter_Type.Caption = "Тип кода фильтра";
            this.colID_Filter_Type.ColumnEdit = this.riFilterType;
            this.colID_Filter_Type.FieldName = "ID_Filter_Type";
            this.colID_Filter_Type.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colID_Filter_Type.Name = "colID_Filter_Type";
            this.colID_Filter_Type.Visible = true;
            this.colID_Filter_Type.VisibleIndex = 3;
            // 
            // riFilterType
            // 
            this.riFilterType.AutoHeight = false;
            this.riFilterType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riFilterType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Filter_Type", "ID_Filter_Type", 94, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riFilterType.DataSource = this.filterTypeBindingSource;
            this.riFilterType.DisplayMember = "Name";
            this.riFilterType.Name = "riFilterType";
            this.riFilterType.NullText = "";
            this.riFilterType.PopupFormMinSize = new System.Drawing.Size(150, 50);
            this.riFilterType.PopupWidth = 150;
            this.riFilterType.ShowFooter = false;
            this.riFilterType.ShowHeader = false;
            this.riFilterType.ValueMember = "ID_Filter_Type";
            // 
            // filterTypeBindingSource
            // 
            this.filterTypeBindingSource.DataMember = "FilterType";
            this.filterTypeBindingSource.DataSource = this.mDataSet;
            // 
            // colBegin_Period
            // 
            this.colBegin_Period.Caption = "Действует С*";
            this.colBegin_Period.ColumnEdit = this.riPeriod;
            this.colBegin_Period.FieldName = "Begin_Period";
            this.colBegin_Period.Name = "colBegin_Period";
            this.colBegin_Period.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.colBegin_Period.Visible = true;
            this.colBegin_Period.VisibleIndex = 4;
            this.colBegin_Period.Width = 82;
            // 
            // riPeriod
            // 
            this.riPeriod.AutoHeight = false;
            this.riPeriod.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riPeriod.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Period", "Period", 120, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Work_Period", "Is_Work_Period", 86, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.riPeriod.DataSource = this.periodBindingSource;
            this.riPeriod.DisplayMember = "Name";
            this.riPeriod.Name = "riPeriod";
            this.riPeriod.NullText = "";
            this.riPeriod.PopupFormMinSize = new System.Drawing.Size(130, 250);
            this.riPeriod.ShowFooter = false;
            this.riPeriod.ShowHeader = false;
            this.riPeriod.ValueMember = "Period";
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // colEnd_Period
            // 
            this.colEnd_Period.Caption = "Действует ПО";
            this.colEnd_Period.ColumnEdit = this.riPeriod;
            this.colEnd_Period.FieldName = "End_Period";
            this.colEnd_Period.Name = "colEnd_Period";
            this.colEnd_Period.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.colEnd_Period.Visible = true;
            this.colEnd_Period.VisibleIndex = 5;
            this.colEnd_Period.Width = 76;
            // 
            // colAdvance
            // 
            this.colAdvance.Caption = "Аванс*";
            this.colAdvance.FieldName = "Advance";
            this.colAdvance.Name = "colAdvance";
            this.colAdvance.Visible = true;
            this.colAdvance.VisibleIndex = 6;
            this.colAdvance.Width = 56;
            // 
            // colRoundTo
            // 
            this.colRoundTo.Caption = "Тип округления*";
            this.colRoundTo.FieldName = "RoundTo";
            this.colRoundTo.Name = "colRoundTo";
            this.colRoundTo.Visible = true;
            this.colRoundTo.VisibleIndex = 7;
            this.colRoundTo.Width = 90;
            // 
            // colPrefix
            // 
            this.colPrefix.Caption = "Префикс реестров";
            this.colPrefix.FieldName = "Prefix";
            this.colPrefix.Name = "colPrefix";
            this.colPrefix.Visible = true;
            this.colPrefix.VisibleIndex = 8;
            this.colPrefix.Width = 88;
            // 
            // colShow_Meter_For_Services
            // 
            this.colShow_Meter_For_Services.Caption = "Открыть ввод показаний для услуг";
            this.colShow_Meter_For_Services.FieldName = "Show_Meter_For_Services";
            this.colShow_Meter_For_Services.Name = "colShow_Meter_For_Services";
            this.colShow_Meter_For_Services.Visible = true;
            this.colShow_Meter_For_Services.VisibleIndex = 9;
            this.colShow_Meter_For_Services.Width = 156;
            // 
            // colComment
            // 
            this.colComment.Caption = "Комментарий";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            // 
            // colEmpty
            // 
            this.colEmpty.Caption = "Пустой";
            this.colEmpty.FieldName = "Empty";
            this.colEmpty.Name = "colEmpty";
            this.colEmpty.Visible = true;
            this.colEmpty.VisibleIndex = 10;
            // 
            // colDerive_Company_Code
            // 
            this.colDerive_Company_Code.Caption = "Код УК";
            this.colDerive_Company_Code.FieldName = "Derive_Company_Code";
            this.colDerive_Company_Code.Name = "colDerive_Company_Code";
            this.colDerive_Company_Code.Visible = true;
            this.colDerive_Company_Code.VisibleIndex = 11;
            // 
            // colDeriveCompanyHasHouses
            // 
            this.colDeriveCompanyHasHouses.Caption = "В УК есть дома по адресной программе";
            this.colDeriveCompanyHasHouses.FieldName = "DeriveCompanyHasHouses";
            this.colDeriveCompanyHasHouses.Name = "colDeriveCompanyHasHouses";
            // 
            // colBill_Name
            // 
            this.colBill_Name.Caption = "Наименование квитанции";
            this.colBill_Name.FieldName = "Bill_Name";
            this.colBill_Name.Name = "colBill_Name";
            this.colBill_Name.Visible = true;
            this.colBill_Name.VisibleIndex = 12;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "house_5840.png");
            this.imageList1.Images.SetKeyName(1, "house_link_7100.png");
            // 
            // clientDetailBindingSource1
            // 
            this.clientDetailBindingSource1.DataMember = "ClientDetail";
            this.clientDetailBindingSource1.DataSource = this.mDataSet;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridClientDetail);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.tabControlSub);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(887, 578);
            this.splitContainerControl1.SplitterPosition = 449;
            this.splitContainerControl1.TabIndex = 3;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SizeChanged += new System.EventHandler(this.splitContainerControl1_SizeChanged);
            // 
            // tabControlSub
            // 
            this.tabControlSub.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSub.Location = new System.Drawing.Point(0, 0);
            this.tabControlSub.Name = "tabControlSub";
            this.tabControlSub.SelectedTabPage = this.tabAgreement;
            this.tabControlSub.Size = new System.Drawing.Size(887, 123);
            this.tabControlSub.TabIndex = 1;
            this.tabControlSub.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabAgreement,
            this.tabComment});
            // 
            // tabAgreement
            // 
            this.tabAgreement.Controls.Add(this.gridClientDetailAgreement1);
            this.tabAgreement.Name = "tabAgreement";
            this.tabAgreement.Size = new System.Drawing.Size(880, 94);
            this.tabAgreement.Text = "Договора";
            // 
            // gridClientDetailAgreement1
            // 
            this.gridClientDetailAgreement1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridClientDetailAgreement1.Location = new System.Drawing.Point(0, 0);
            this.gridClientDetailAgreement1.Name = "gridClientDetailAgreement1";
            this.gridClientDetailAgreement1.Size = new System.Drawing.Size(880, 94);
            this.gridClientDetailAgreement1.TabIndex = 0;
            // 
            // tabComment
            // 
            this.tabComment.Controls.Add(this.txtComment);
            this.tabComment.Name = "tabComment";
            this.tabComment.Size = new System.Drawing.Size(880, 94);
            this.tabComment.Text = "Комментарий";
            // 
            // txtComment
            // 
            this.txtComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtComment.Location = new System.Drawing.Point(0, 0);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(880, 94);
            this.txtComment.TabIndex = 0;
            this.txtComment.Leave += new System.EventHandler(this.txtComment_Leave);
            // 
            // miClientDetail_SimpleCopy
            // 
            this.miClientDetail_SimpleCopy.Name = "miClientDetail_SimpleCopy";
            this.miClientDetail_SimpleCopy.Size = new System.Drawing.Size(265, 22);
            this.miClientDetail_SimpleCopy.Text = "Скопировать запись (без домов)";
            this.miClientDetail_SimpleCopy.Click += new System.EventHandler(this.miClientDetail_SimpleCopy_Click);
            // 
            // GridBillFilterCurrent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "GridBillFilterCurrent";
            this.Size = new System.Drawing.Size(887, 578);
            ((System.ComponentModel.ISupportInitialize)(this.gridClientDetail)).EndInit();
            this.cmsClientDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riClientPayReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeriveCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFilterType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControlSub)).EndInit();
            this.tabControlSub.ResumeLayout(false);
            this.tabAgreement.ResumeLayout(false);
            this.tabComment.ResumeLayout(false);
            this.tabComment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private BaseGrid gridClientDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewClientDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client_Detail;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riClientPayReceiver;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Derive_Company;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riDeriveCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Bill_Filter;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riBillFilter;
        private DevExpress.XtraGrid.Columns.GridColumn colBegin_Period;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colEnd_Period;
        private DevExpress.XtraGrid.Columns.GridColumn colAdvance;
        private DevExpress.XtraGrid.Columns.GridColumn colRoundTo;
        private DevExpress.XtraGrid.Columns.GridColumn colPrefix;
        private DevExpress.XtraGrid.Columns.GridColumn colShow_Meter_For_Services;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private DS.MDataSet mDataSet;
        private System.Windows.Forms.BindingSource clientDetailBindingSource;
        private System.Windows.Forms.BindingSource billFilterBindingSource;
        private System.Windows.Forms.BindingSource deriveCompanyBindingSource;
        private System.Windows.Forms.BindingSource payReceiverCompanyBindingSource;
        private System.Windows.Forms.ContextMenuStrip cmsClientDetail;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_Edit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_Add;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_Delete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_Open;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_Close;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_OpenMeasureForService;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_Refresh;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_Export;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_ServiceInfo;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.BindingSource clientDetailBindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.TextBox txtComment;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Filter_Type;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riFilterType;
        private System.Windows.Forms.BindingSource filterTypeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEmpty;
        private DevExpress.XtraGrid.Columns.GridColumn colDerive_Company_Code;
        private DevExpress.XtraGrid.Columns.GridColumn colDeriveCompanyHasHouses;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_Copy;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_RecordHistory;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_PrintBills;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private DevExpress.XtraTab.XtraTabControl tabControlSub;
        private DevExpress.XtraTab.XtraTabPage tabComment;
        private DevExpress.XtraTab.XtraTabPage tabAgreement;
        private GridClientDetailAgreement gridClientDetailAgreement1;
        private DevExpress.XtraGrid.Columns.GridColumn colBill_Name;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_ProviderSettings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem miClientDetail_SimpleCopy;
    }
}
