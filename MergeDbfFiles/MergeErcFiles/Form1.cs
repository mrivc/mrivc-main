﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Payments;

namespace MergeErcFiles
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Text = this.Text + " " + System.Windows.Forms.Application.ProductVersion;
        }

        string path;
        private void button1_Click(object sender, EventArgs e)
        {
            path = textBox1.Text;
            System.IO.DirectoryInfo root = new System.IO.DirectoryInfo(path);
            List<FileInfo> fileList = new List<FileInfo>();
            FileHelper.GetDirectoryTree(root, fileList);

            label1.Text = "Всего файлов: " + fileList.Count.ToString();
            label1.Refresh();

            DbfReestr reestr = new DbfReestr();
            foreach (FileInfo fi in fileList)
            {
                checkedListBox1.Items.Add(fi.DirectoryName + "\\" + fi.Name);
                checkedListBox1.Refresh();
                
                reestr.AddFile(fi.DirectoryName, fi.Name);
                
                checkedListBox1.SetItemChecked(checkedListBox1.Items.Count-1, true);
                checkedListBox1.Refresh();
                label2.Text = "Обработано: " + checkedListBox1.Items.Count.ToString();
                label2.Refresh();
            }

            //reestr.MergedFile.WriteXml(Properties.Settings.Default.Path + "\\" + "AllFiles.xml", XmlWriteMode.WriteSchema);

            gridControl1.DataSource = reestr.MergedFile;
            
            MessageBox.Show("Объединение файлов завершено. Для экспорта нажмите кнопку \"Экспорт в Excel\".", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            gridControl1.ExportToXlsx(path + "\\export1.xlsx");

            MessageBox.Show($"Экспорт закончен. {Environment.NewLine + path}", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // textBox1.Text = "d:\\Работа\\Temp\\ООО МРИВЦ Газ\\Платежи\\";
        }
    }
}
