﻿namespace MrivcControls
{
    partial class PivotPrintHistory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fieldclientname = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldderivecompany = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldcity = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldhouseaddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldfio = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miExpandAllGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.miCollapseAllGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miExpandColumnGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.miCollapseColumnGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.miSaveLayout = new System.Windows.Forms.ToolStripMenuItem();
            this.miSaveNewLayout = new System.Windows.Forms.ToolStripMenuItem();
            this.printPivotDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cDataSet = new MrivcControls.Data.CDataSet();
            this.fieldbillfilter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTotalQuantity = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldbillcount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldBillCountWithoutSubscribers = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIssuedDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPrintDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDeliveryType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccountStatus0 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccountStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccountStatus2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccountStatus3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccountStatus4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccountStatus5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccountStatus6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccountStatus7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHouse_Contract_MTEC = new DevExpress.XtraPivotGrid.PivotGridField();
            this.cbPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cbShowAllBill = new System.Windows.Forms.CheckBox();
            this.cbLayout = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printPivotDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // fieldclientname
            // 
            this.fieldclientname.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldclientname.AreaIndex = 0;
            this.fieldclientname.Caption = "Клиент";
            this.fieldclientname.FieldName = "client_name";
            this.fieldclientname.MinWidth = 100;
            this.fieldclientname.Name = "fieldclientname";
            this.fieldclientname.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldclientname.Width = 250;
            // 
            // fieldderivecompany
            // 
            this.fieldderivecompany.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldderivecompany.AreaIndex = 1;
            this.fieldderivecompany.Caption = "УК";
            this.fieldderivecompany.FieldName = "derive_company";
            this.fieldderivecompany.MinWidth = 100;
            this.fieldderivecompany.Name = "fieldderivecompany";
            this.fieldderivecompany.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.True;
            this.fieldderivecompany.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldderivecompany.Width = 250;
            // 
            // fieldcity
            // 
            this.fieldcity.AreaIndex = 1;
            this.fieldcity.Caption = "Город";
            this.fieldcity.FieldName = "city";
            this.fieldcity.Name = "fieldcity";
            this.fieldcity.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldhouseaddress
            // 
            this.fieldhouseaddress.AreaIndex = 2;
            this.fieldhouseaddress.Caption = "Дома";
            this.fieldhouseaddress.FieldName = "house_address";
            this.fieldhouseaddress.Name = "fieldhouseaddress";
            this.fieldhouseaddress.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldhouseaddress.Width = 80;
            // 
            // fieldfio
            // 
            this.fieldfio.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldfio.AreaIndex = 2;
            this.fieldfio.Caption = "Сотрудник";
            this.fieldfio.FieldName = "fio";
            this.fieldfio.Name = "fieldfio";
            this.fieldfio.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pivotGridControl1.Appearance.ColumnHeaderArea.Options.UseTextOptions = true;
            this.pivotGridControl1.Appearance.ColumnHeaderArea.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridControl1.Appearance.ColumnHeaderArea.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridControl1.Appearance.FieldHeader.Options.UseTextOptions = true;
            this.pivotGridControl1.Appearance.FieldHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridControl1.Appearance.FieldHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridControl1.Appearance.HeaderArea.Options.UseTextOptions = true;
            this.pivotGridControl1.Appearance.HeaderArea.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridControl1.Appearance.HeaderArea.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridControl1.Appearance.Lines.BackColor = System.Drawing.Color.Silver;
            this.pivotGridControl1.Appearance.Lines.Options.UseBackColor = true;
            this.pivotGridControl1.Appearance.TotalCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pivotGridControl1.Appearance.TotalCell.Options.UseBackColor = true;
            this.pivotGridControl1.ContextMenuStrip = this.contextMenuStrip1;
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.printPivotDataBindingSource;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldfio,
            this.fieldderivecompany,
            this.fieldbillfilter,
            this.fieldcity,
            this.fieldhouseaddress,
            this.fieldTotalQuantity,
            this.fieldbillcount,
            this.fieldBillCountWithoutSubscribers,
            this.fieldIssuedDate,
            this.fieldPrintDate,
            this.fieldDeliveryType,
            this.fieldclientname,
            this.fieldAccountStatus0,
            this.fieldAccountStatus1,
            this.fieldAccountStatus2,
            this.fieldAccountStatus3,
            this.fieldAccountStatus4,
            this.fieldAccountStatus5,
            this.fieldAccountStatus6,
            this.fieldAccountStatus7,
            this.fieldHouse_Contract_MTEC});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 42);
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.AllowFilter = false;
            this.pivotGridControl1.Size = new System.Drawing.Size(1223, 532);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.FieldFilterChanged += new DevExpress.XtraPivotGrid.PivotFieldEventHandler(this.pivotGridControl1_FieldFilterChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExpandAllGroups,
            this.miCollapseAllGroups,
            this.toolStripSeparator3,
            this.miExpandColumnGroups,
            this.miCollapseColumnGroups,
            this.toolStripSeparator1,
            this.miExportExcel,
            this.toolStripSeparator2,
            this.miRefresh,
            this.toolStripSeparator4,
            this.miSaveLayout,
            this.miSaveNewLayout});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(230, 204);
            // 
            // miExpandAllGroups
            // 
            this.miExpandAllGroups.Image = global::MrivcControls.Properties.Resources.expand1;
            this.miExpandAllGroups.Name = "miExpandAllGroups";
            this.miExpandAllGroups.Size = new System.Drawing.Size(229, 22);
            this.miExpandAllGroups.Text = "Развернуть все группы";
            this.miExpandAllGroups.Click += new System.EventHandler(this.miExpandAllGroups_Click);
            // 
            // miCollapseAllGroups
            // 
            this.miCollapseAllGroups.Image = global::MrivcControls.Properties.Resources.collapse_all1;
            this.miCollapseAllGroups.Name = "miCollapseAllGroups";
            this.miCollapseAllGroups.Size = new System.Drawing.Size(229, 22);
            this.miCollapseAllGroups.Text = "Свернуть все группы";
            this.miCollapseAllGroups.Click += new System.EventHandler(this.miCollapseAllGroups_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(226, 6);
            // 
            // miExpandColumnGroups
            // 
            this.miExpandColumnGroups.Name = "miExpandColumnGroups";
            this.miExpandColumnGroups.Size = new System.Drawing.Size(229, 22);
            this.miExpandColumnGroups.Text = "Развернуть группы колонок";
            this.miExpandColumnGroups.Click += new System.EventHandler(this.miExpandColumnGroups_Click);
            // 
            // miCollapseColumnGroups
            // 
            this.miCollapseColumnGroups.Name = "miCollapseColumnGroups";
            this.miCollapseColumnGroups.Size = new System.Drawing.Size(229, 22);
            this.miCollapseColumnGroups.Text = "Свернуть группы колонок";
            this.miCollapseColumnGroups.Click += new System.EventHandler(this.miCollapseColumnGroups_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(226, 6);
            // 
            // miExportExcel
            // 
            this.miExportExcel.Image = global::MrivcControls.Properties.Resources.filter_enable;
            this.miExportExcel.Name = "miExportExcel";
            this.miExportExcel.Size = new System.Drawing.Size(229, 22);
            this.miExportExcel.Text = "Экспорт в Excel";
            this.miExportExcel.Click += new System.EventHandler(this.miExportExcel_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(226, 6);
            // 
            // miRefresh
            // 
            this.miRefresh.Image = global::MrivcControls.Properties.Resources.refresh;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(229, 22);
            this.miRefresh.Text = "Обновить";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(226, 6);
            // 
            // miSaveLayout
            // 
            this.miSaveLayout.Image = global::MrivcControls.Properties.Resources.Save;
            this.miSaveLayout.Name = "miSaveLayout";
            this.miSaveLayout.Size = new System.Drawing.Size(229, 22);
            this.miSaveLayout.Text = "Сохранить разметку";
            this.miSaveLayout.Click += new System.EventHandler(this.miSaveLayout_Click);
            // 
            // miSaveNewLayout
            // 
            this.miSaveNewLayout.Image = global::MrivcControls.Properties.Resources.Save_As;
            this.miSaveNewLayout.Name = "miSaveNewLayout";
            this.miSaveNewLayout.Size = new System.Drawing.Size(229, 22);
            this.miSaveNewLayout.Text = "Сохранить новую разметку";
            this.miSaveNewLayout.Click += new System.EventHandler(this.miSaveNewLayout_Click);
            // 
            // printPivotDataBindingSource
            // 
            this.printPivotDataBindingSource.DataMember = "PrintPivotData";
            this.printPivotDataBindingSource.DataSource = this.cDataSet;
            this.printPivotDataBindingSource.CurrentChanged += new System.EventHandler(this.printPivotDataBindingSource_CurrentChanged);
            // 
            // cDataSet
            // 
            this.cDataSet.DataSetName = "CDataSet";
            this.cDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fieldbillfilter
            // 
            this.fieldbillfilter.Appearance.Cell.Options.UseTextOptions = true;
            this.fieldbillfilter.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fieldbillfilter.Appearance.Cell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.fieldbillfilter.Appearance.Header.Options.UseTextOptions = true;
            this.fieldbillfilter.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fieldbillfilter.Appearance.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.fieldbillfilter.Appearance.Value.Options.UseTextOptions = true;
            this.fieldbillfilter.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fieldbillfilter.Appearance.Value.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.fieldbillfilter.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldbillfilter.AreaIndex = 0;
            this.fieldbillfilter.Caption = "Код фильтра";
            this.fieldbillfilter.ColumnValueLineCount = 4;
            this.fieldbillfilter.FieldName = "bill_filter";
            this.fieldbillfilter.Name = "fieldbillfilter";
            this.fieldbillfilter.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldbillfilter.Visible = false;
            this.fieldbillfilter.Width = 90;
            // 
            // fieldTotalQuantity
            // 
            this.fieldTotalQuantity.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldTotalQuantity.AreaIndex = 0;
            this.fieldTotalQuantity.Caption = "Кол-во Обслуживаемых ЛС";
            this.fieldTotalQuantity.FieldName = "TotalQuantity";
            this.fieldTotalQuantity.Name = "fieldTotalQuantity";
            this.fieldTotalQuantity.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldTotalQuantity.Visible = false;
            // 
            // fieldbillcount
            // 
            this.fieldbillcount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldbillcount.AreaIndex = 0;
            this.fieldbillcount.Caption = "Кол-во квитанций";
            this.fieldbillcount.FieldName = "bill_count";
            this.fieldbillcount.Name = "fieldbillcount";
            this.fieldbillcount.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldBillCountWithoutSubscribers
            // 
            this.fieldBillCountWithoutSubscribers.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldBillCountWithoutSubscribers.AreaIndex = 2;
            this.fieldBillCountWithoutSubscribers.Caption = "Кол-во (Курьер)";
            this.fieldBillCountWithoutSubscribers.FieldName = "Bill_Count_Without_Subscribers";
            this.fieldBillCountWithoutSubscribers.Name = "fieldBillCountWithoutSubscribers";
            this.fieldBillCountWithoutSubscribers.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldBillCountWithoutSubscribers.Visible = false;
            // 
            // fieldIssuedDate
            // 
            this.fieldIssuedDate.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldIssuedDate.AreaIndex = 1;
            this.fieldIssuedDate.Caption = "Дата выдачи клиенту";
            this.fieldIssuedDate.CellFormat.FormatString = "dd.MM.yyyy";
            this.fieldIssuedDate.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldIssuedDate.FieldName = "Issued_Date";
            this.fieldIssuedDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.Date;
            this.fieldIssuedDate.Name = "fieldIssuedDate";
            this.fieldIssuedDate.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldIssuedDate.Options.ShowCustomTotals = false;
            this.fieldIssuedDate.Options.ShowGrandTotal = false;
            this.fieldIssuedDate.Options.ShowTotals = false;
            this.fieldIssuedDate.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Max;
            this.fieldIssuedDate.UnboundFieldName = "fieldIssuedDate";
            this.fieldIssuedDate.Width = 291;
            // 
            // fieldPrintDate
            // 
            this.fieldPrintDate.AreaIndex = 0;
            this.fieldPrintDate.Caption = "Дата печати квитанций";
            this.fieldPrintDate.FieldName = "PrintDate";
            this.fieldPrintDate.Name = "fieldPrintDate";
            this.fieldPrintDate.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.True;
            this.fieldPrintDate.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldDeliveryType
            // 
            this.fieldDeliveryType.AreaIndex = 1;
            this.fieldDeliveryType.Caption = "Доставка";
            this.fieldDeliveryType.FieldName = "Delivery_Type";
            this.fieldDeliveryType.Name = "fieldDeliveryType";
            this.fieldDeliveryType.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldDeliveryType.Visible = false;
            // 
            // fieldAccountStatus0
            // 
            this.fieldAccountStatus0.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccountStatus0.AreaIndex = 1;
            this.fieldAccountStatus0.Caption = "Кол-во открытых ЛС";
            this.fieldAccountStatus0.FieldName = "AccountStatus0";
            this.fieldAccountStatus0.Name = "fieldAccountStatus0";
            this.fieldAccountStatus0.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccountStatus0.Visible = false;
            // 
            // fieldAccountStatus1
            // 
            this.fieldAccountStatus1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccountStatus1.AreaIndex = 1;
            this.fieldAccountStatus1.Caption = "Кол-во закрытых ЛС";
            this.fieldAccountStatus1.FieldName = "AccountStatus1";
            this.fieldAccountStatus1.Name = "fieldAccountStatus1";
            this.fieldAccountStatus1.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccountStatus1.Visible = false;
            // 
            // fieldAccountStatus2
            // 
            this.fieldAccountStatus2.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccountStatus2.AreaIndex = 1;
            this.fieldAccountStatus2.Caption = "Кол-во пустующих ЛС";
            this.fieldAccountStatus2.FieldName = "AccountStatus2";
            this.fieldAccountStatus2.Name = "fieldAccountStatus2";
            this.fieldAccountStatus2.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccountStatus2.Visible = false;
            // 
            // fieldAccountStatus3
            // 
            this.fieldAccountStatus3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccountStatus3.AreaIndex = 1;
            this.fieldAccountStatus3.Caption = "Кол-во ЛС выведенных из ЖФ";
            this.fieldAccountStatus3.FieldName = "AccountStatus3";
            this.fieldAccountStatus3.Name = "fieldAccountStatus3";
            this.fieldAccountStatus3.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccountStatus3.Visible = false;
            // 
            // fieldAccountStatus4
            // 
            this.fieldAccountStatus4.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccountStatus4.AreaIndex = 1;
            this.fieldAccountStatus4.Caption = "Кол-во ЛС (Общий счетчик)";
            this.fieldAccountStatus4.FieldName = "AccountStatus4";
            this.fieldAccountStatus4.Name = "fieldAccountStatus4";
            this.fieldAccountStatus4.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccountStatus4.Visible = false;
            // 
            // fieldAccountStatus5
            // 
            this.fieldAccountStatus5.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccountStatus5.AreaIndex = 0;
            this.fieldAccountStatus5.Caption = "Кол-во нежилых ЛС";
            this.fieldAccountStatus5.FieldName = "AccountStatus5";
            this.fieldAccountStatus5.Name = "fieldAccountStatus5";
            this.fieldAccountStatus5.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccountStatus5.Visible = false;
            // 
            // fieldAccountStatus6
            // 
            this.fieldAccountStatus6.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccountStatus6.AreaIndex = 1;
            this.fieldAccountStatus6.Caption = "Кол-во долговых ЛС";
            this.fieldAccountStatus6.FieldName = "AccountStatus6";
            this.fieldAccountStatus6.Name = "fieldAccountStatus6";
            this.fieldAccountStatus6.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccountStatus6.Visible = false;
            // 
            // fieldAccountStatus7
            // 
            this.fieldAccountStatus7.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccountStatus7.AreaIndex = 1;
            this.fieldAccountStatus7.Caption = "Кол-во долгововых ЛС (для нежилых)";
            this.fieldAccountStatus7.FieldName = "AccountStatus7";
            this.fieldAccountStatus7.Name = "fieldAccountStatus7";
            this.fieldAccountStatus7.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccountStatus7.Visible = false;
            // 
            // fieldHouse_Contract_MTEC
            // 
            this.fieldHouse_Contract_MTEC.AreaIndex = 3;
            this.fieldHouse_Contract_MTEC.Caption = "Текущий договор МТЭЦ";
            this.fieldHouse_Contract_MTEC.FieldName = "House_Contract_MTEC";
            this.fieldHouse_Contract_MTEC.Name = "fieldHouse_Contract_MTEC";
            this.fieldHouse_Contract_MTEC.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldHouse_Contract_MTEC.Visible = false;
            // 
            // cbPeriod
            // 
            this.cbPeriod.Location = new System.Drawing.Point(148, 8);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPeriod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Period", "Period", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Month", "Month", 40, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Year", "Year", 32, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Work_Period", "Is_Work_Period", 86, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.cbPeriod.Properties.DataSource = this.periodBindingSource;
            this.cbPeriod.Properties.DisplayMember = "Name";
            this.cbPeriod.Properties.DropDownRows = 12;
            this.cbPeriod.Properties.NullText = "";
            this.cbPeriod.Properties.ShowFooter = false;
            this.cbPeriod.Properties.ShowHeader = false;
            this.cbPeriod.Properties.ShowLines = false;
            this.cbPeriod.Properties.ValueMember = "Period";
            this.cbPeriod.Size = new System.Drawing.Size(187, 20);
            this.cbPeriod.TabIndex = 5;
            this.cbPeriod.EditValueChanged += new System.EventHandler(this.cbPeriod_EditValueChanged);
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.cDataSet;
            this.periodBindingSource.CurrentChanged += new System.EventHandler(this.periodBindingSource_CurrentChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Период печати квитанций:";
            // 
            // cbShowAllBill
            // 
            this.cbShowAllBill.Checked = true;
            this.cbShowAllBill.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowAllBill.Location = new System.Drawing.Point(349, -2);
            this.cbShowAllBill.Name = "cbShowAllBill";
            this.cbShowAllBill.Size = new System.Drawing.Size(384, 40);
            this.cbShowAllBill.TabIndex = 7;
            this.cbShowAllBill.Text = "Учитывать только выданные квитанции клиенту. \r\n(Если галочки не стоит, показывают" +
    "ся все напечатанные квитанции)";
            this.cbShowAllBill.UseVisualStyleBackColor = true;
            this.cbShowAllBill.CheckStateChanged += new System.EventHandler(this.cbShowAllBill_CheckStateChanged);
            // 
            // cbLayout
            // 
            this.cbLayout.FormattingEnabled = true;
            this.cbLayout.Location = new System.Drawing.Point(826, 8);
            this.cbLayout.Name = "cbLayout";
            this.cbLayout.Size = new System.Drawing.Size(217, 21);
            this.cbLayout.TabIndex = 8;
            this.cbLayout.SelectedIndexChanged += new System.EventHandler(this.cbLayout_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(744, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Тип отчета:";
            // 
            // PivotPrintHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbLayout);
            this.Controls.Add(this.cbShowAllBill);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPeriod);
            this.Controls.Add(this.pivotGridControl1);
            this.Name = "PivotPrintHistory";
            this.Size = new System.Drawing.Size(1223, 574);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printPivotDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private System.Windows.Forms.BindingSource printPivotDataBindingSource;
        private Data.CDataSet cDataSet;
        private DevExpress.XtraPivotGrid.PivotGridField fieldfio;
        private DevExpress.XtraPivotGrid.PivotGridField fieldderivecompany;
        private DevExpress.XtraPivotGrid.PivotGridField fieldbillfilter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldhouseaddress;
        private DevExpress.XtraPivotGrid.PivotGridField fieldbillcount;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miExportExcel;
        private DevExpress.XtraEditors.LookUpEdit cbPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbShowAllBill;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIssuedDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPrintDate;
        private System.Windows.Forms.ToolStripMenuItem miRefresh;
        private System.Windows.Forms.ToolStripMenuItem miCollapseAllGroups;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private DevExpress.XtraPivotGrid.PivotGridField fieldcity;
		private DevExpress.XtraPivotGrid.PivotGridField fieldDeliveryType;
		private System.Windows.Forms.ToolStripMenuItem miExpandAllGroups;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem miExpandColumnGroups;
        private System.Windows.Forms.ToolStripMenuItem miCollapseColumnGroups;
        private System.Windows.Forms.ComboBox cbLayout;
        private DevExpress.XtraPivotGrid.PivotGridField fieldclientname;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldBillCountWithoutSubscribers;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccountStatus0;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccountStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccountStatus2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccountStatus3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccountStatus4;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccountStatus5;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccountStatus6;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccountStatus7;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTotalQuantity;
        private System.Windows.Forms.ToolStripMenuItem miSaveLayout;
        private System.Windows.Forms.ToolStripMenuItem miSaveNewLayout;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHouse_Contract_MTEC;
    }
}
