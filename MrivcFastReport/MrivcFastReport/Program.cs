﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MrivcFastReport
{
    using FastReport;
    using FastReport.Design.StandardDesigner;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if ( args.Length != 0 )
                fileName = args[0];

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DesignerForm designerForm = new DesignerForm();
            designerForm.Load += designerForm_Load;
            
            Application.Run(designerForm);
        }

        static string fileName = string.Empty;

        static void designerForm_Load(object sender, EventArgs e)
        {
            DesignerForm form = sender as DesignerForm;
            
            form.Designer.MdiMode = true;
            //form.Designer.cmdNew.Enabled = true;

            if (fileName != string.Empty)
                form.Designer.cmdOpen.LoadFile(fileName);

            form.UpdateContent();
        }
    }
}
