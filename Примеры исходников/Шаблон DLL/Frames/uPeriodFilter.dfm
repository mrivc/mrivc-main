object PeriodFilter: TPeriodFilter
  Left = 0
  Top = 0
  Width = 225
  Height = 57
  AutoSize = True
  TabOrder = 0
  TabStop = True
  object gbPeriod: TcxGroupBox
    Left = 0
    Top = 0
    Caption = #1055#1077#1088#1080#1086#1076
    TabOrder = 0
    Transparent = True
    DesignSize = (
      225
      57)
    Height = 57
    Width = 225
    object jvdbcbPeriod: TcxLookupComboBox
      Left = 8
      Top = 24
      Anchors = [akLeft, akTop, akRight]
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 16
      Properties.KeyFieldNames = 'Period'
      Properties.ListColumns = <
        item
          FieldName = 'Period_Name'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = JvdsPeriod
      Properties.OnChange = jvdbcbPeriodChange
      TabOrder = 0
      Width = 209
    end
  end
  object adsPeriod: TADODataSet
    Tag = 1
    CursorType = ctStatic
    LockType = ltReadOnly
    CommandText = 
      'SELECT Period, Year, Month, Month_Name + '#39' '#39' + CAST(Year AS VARC' +
      'HAR) AS Period_Name, Begin_Date, End_Date, Day_Count, Is_Work_Pe' +
      'riod'#13#10'FROM VPeriod'#13#10'ORDER BY '#13#10'Year DESC, Month DESC'
    CommandTimeout = 0
    Parameters = <>
    Left = 8
    Top = 56
    object dfPeriod: TIntegerField
      FieldName = 'Period'
    end
    object dfYear: TIntegerField
      FieldName = 'Year'
    end
    object dfMonth: TIntegerField
      FieldName = 'Month'
    end
    object dfPeriod_Name: TStringField
      FieldName = 'Period_Name'
      ReadOnly = True
      Size = 56
    end
    object dfBegin_Date: TDateTimeField
      FieldName = 'Begin_Date'
    end
    object dfEnd_Date: TDateTimeField
      FieldName = 'End_Date'
    end
    object dfDay_Count: TIntegerField
      FieldName = 'Day_Count'
      ReadOnly = True
    end
    object dfIs_Work_Period: TBooleanField
      FieldName = 'Is_Work_Period'
    end
  end
  object JvdsPeriod: TDataSource
    DataSet = adsPeriod
    Left = 44
    Top = 56
  end
end
