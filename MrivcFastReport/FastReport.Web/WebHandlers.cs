﻿using FastReport.Utils;
using FastReport.Web;
using FastReport.Export.Html;
using FastReport.Data;
using System;
using System.Web;
using System.IO;
using System.Web.Caching;
using System.Text;
using System.Net;
using System.Web.UI;

namespace FastReport.Web.Handlers
{
    /// <summary>
    /// Web handler class
    /// </summary>
    public partial class WebExport: IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        private bool WebFarmMode = false;
        private string FileStoragePath;
        private WebReport webReport;
        private WebLog log;

        private Report Report
        {
            get { return webReport.Report; }
        }

        private WebReportProperties Properties
        {
            get { return webReport.Prop; }
        }


        /// <summary>
        /// 
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Process Request
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            FileStoragePath = WebReportCache.GetStoragePath(context);
            WebFarmMode = !String.IsNullOrEmpty(FileStoragePath);
            // pictures
            if (context.Request.Params[WebUtils.PicsPrefix] != null)
                SendPicture(context);
            // export
            else if (context.Request.QueryString[WebUtils.ConstID] != null)
                SendExport(context);
            // print
            else if (context.Request.Params[WebUtils.PrintPrefix] != null && context.Request.Params[WebUtils.ReportPrefix] != null)
                SendPrint(context);
            // report
            else if (context.Request.Params["object"] != null)
                SendObjectResponse(context);
            else if (context.Request.Params["previewobject"] != null)
                SendPreviewObjectResponse(context);
            else if (context.Request.Params["respic"] != null)
                SendResourcePic(context);
            else if (context.Request.Params["getReport"] != null)
                SendReportTemplate(context);
            else if (context.Request.Params["putReport"] != null)
                SetReportTemplate(context);
            else if (context.Request.Params["getPreview"] != null)
                SendReportPreview(context);
            else if (context.Request.Params["makePreview"] != null)
                MakeReportPreview(context);
            else
                SendDebug(context);
        }

        // preview for Designer
        private void MakeReportPreview(HttpContext context)
        {
            AddNoCacheHeaders(context);
            string guid = context.Request.Params["makePreview"];
            SetUpWebReport(guid);
            if (webReport != null)
            {            
                string receivedReportString = GetPOSTReport(context);
                try
                {
                    WebReport previewReport = webReport;

                    previewReport.Width = System.Web.UI.WebControls.Unit.Percentage(100);
                    previewReport.Height = System.Web.UI.WebControls.Unit.Percentage(100);
                    previewReport.Toolbar.EnableFit = true;
                    previewReport.Toolbar.Width = "100%";
                    previewReport.Toolbar.Height = "100%";

                    string reportString = PasteRestricted(webReport, receivedReportString);                    

                    previewReport.Report.ReportResourceString = reportString;
                    previewReport.ReportFile = String.Empty;
                    previewReport.Report.LoadFromString(reportString);

                    previewReport.Prepare();

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<script src=\"").Append(GetResourceTemplateUrl(context, "fr_util.js")).AppendLine("\" type=\"text/javascript\"></script>");

                    HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(sb, System.Globalization.CultureInfo.InvariantCulture));

                    bool oldDesingMode = previewReport.DesignReport;
                    previewReport.ShowZoomButton = false;
                    previewReport.ShowRefreshButton = false;
                    previewReport.PreviewMode = true;
                    previewReport.DesignReport = false;

                    previewReport.Page = null;

                    previewReport.RenderControl(writer);

                    previewReport.PreviewMode = false;
                    previewReport.DesignReport = oldDesingMode;

                    string responseText = previewReport.Toolbar.GetCss() + sb.ToString();
                    context.Response.Write(responseText);
                }
                catch (Exception e)
                {                    
                    if (webReport.Debug)
                        context.Response.Write(e.Message);  
                    context.Response.StatusCode = 500;
                }
            }
            else
            {
                context.Response.StatusCode = 404;
            }
            context.Response.End();
        }

        private string GetResourceTemplateUrl(HttpContext context, string resName)
        {
            return new System.Web.UI.Page().ClientScript.
                GetWebResourceUrl(this.GetType(), string.Format("FastReport.Web.Resources.Templates.{0}", resName));
        }

        private void SendReportPreview(HttpContext context)
        {
            // to-do
            // not implemented
            AddNoCacheHeaders(context);
            string guid = context.Request.Params["getPreview"];
            context.Response.StatusCode = 501;
            context.Response.End();
        }

        // save report
        private void SetReportTemplate(HttpContext context)
        {            
            string guid = context.Request.Params["putReport"];
            SetUpWebReport(guid);

            if (webReport != null)
            {
                string reportString = GetPOSTReport(context);
                try
                {
                    // paste restricted back in report before save
                    webReport.Report.LoadFromString(PasteRestricted(webReport, reportString));

                    // save report in reports folder
                    if (!String.IsNullOrEmpty(webReport.DesignerSavePath))
                    {
                        string savePath = context.Request.MapPath(webReport.DesignerSavePath);
                        string reportFileName = String.Concat(webReport.ReportGuid, ".frx");
                        if (Directory.Exists(savePath))
                        {
                            string report = webReport.Report.SaveToString();
                            File.WriteAllText(Path.Combine(savePath, reportFileName), report, Encoding.UTF8);
                        }
                        else
                        {
                            context.Response.Write("DesignerSavePath does not exists");
                            context.Response.StatusCode = 404;
                        }

                        // call call-back page with notification
                        if (!String.IsNullOrEmpty(webReport.DesignerSaveCallBack))
                        {
                            UriBuilder uri = new UriBuilder();
                            uri.Scheme = context.Request.Url.Scheme;
                            uri.Host = context.Request.Url.Host;
                            uri.Port = context.Request.Url.Port;
                            uri.Path = webReport.ResolveUrl(webReport.DesignerSaveCallBack); 
                            string queryToAppend = String.Format("reportID={0}&reportUUID={1}", webReport.ID != null ? webReport.ID : "", reportFileName);
                            if (uri.Query != null && uri.Query.Length > 1)
                                uri.Query = uri.Query.Substring(1) + "&" + queryToAppend;
                            else
                                uri.Query = queryToAppend;
                            string callBackURL = uri.ToString();
                            WebRequest request = WebRequest.Create(callBackURL);
                            request.Method = "GET";
                            request.GetResponse();                            
                        }
                    }
                    else
                    {
                        context.Response.Write("DesignerSavePath does not set");
                        context.Response.StatusCode = 404;
                    }
                }
                catch (Exception e)
                {
                    if (webReport.Debug)
                        context.Response.Write(e.Message);
                    context.Response.StatusCode = 500;
                }
            }
            else
                context.Response.StatusCode = 404;

            context.Response.End();
        }

        private string PasteRestricted(WebReport webReport, string xmlString)
        {
            using (MemoryStream xmlStream1 = new MemoryStream())
            using (MemoryStream xmlStream2 = new MemoryStream())
            {
                Write(xmlStream1, webReport.Report.SaveToString());
                Write(xmlStream2, xmlString);
                xmlStream1.Position = 0;
                xmlStream2.Position = 0;
                FastReport.Utils.XmlDocument xml1 = new FastReport.Utils.XmlDocument();
                FastReport.Utils.XmlDocument xml2 = new FastReport.Utils.XmlDocument();
                xml1.Load(xmlStream1);
                xml2.Load(xmlStream2);

                if (!webReport.DesignScriptCode)
                {
                    xml2.Root.SetProp("CodeRestricted", "");
                    // clean received script
                    FastReport.Utils.XmlItem scriptItem2 = xml2.Root.FindItem("ScriptText");
                    if (scriptItem2 != null)
                        scriptItem2.Value = "";
                    // paste old script
                    FastReport.Utils.XmlItem scriptItem1 = xml1.Root.FindItem("ScriptText");
                    if (scriptItem1 != null)
                    {
                        if (String.IsNullOrEmpty(scriptItem1.Value))
                        {
                            scriptItem2.Dispose();
                            scriptItem2 = null;
                        }
                        else
                        if (scriptItem2 != null)
                            scriptItem2.Value = scriptItem1.Value;
                        else
                            xml2.Root.AddItem(scriptItem1);
                    }

                }
                // paste saved connection strings
                FastReport.Utils.XmlItem dictionary1 = xml1.Root.FindItem("Dictionary");
                FastReport.Utils.XmlItem dictionary2 = xml2.Root.FindItem("Dictionary");
                if (dictionary1 != null && dictionary2 != null)
                {
                    for (int i = 0; i < dictionary1.Items.Count; i++)
                    {
                        FastReport.Utils.XmlItem item1 = dictionary1.Items[i];
                        string connectionString = item1.GetProp("ConnectionString");
                        if (!String.IsNullOrEmpty(connectionString))
                        {
                            FastReport.Utils.XmlItem item2 = dictionary2.FindItem(item1.Name);
                            if (item2 != null)
                                item2.SetProp("ConnectionString", connectionString);
                        }
                    }
                }

                // save prepared xml
                using (MemoryStream secondXmlStream = new MemoryStream())
                {
                    xml2.Save(secondXmlStream);
                    secondXmlStream.Position = 0;
                    byte[] buff = new byte[secondXmlStream.Length];
                    secondXmlStream.Read(buff, 0, buff.Length);
                    xmlString = Encoding.UTF8.GetString(buff);
                }
            }
            return xmlString;
        }

        private string GetPOSTReport(HttpContext context)
        {
            string requestString = ""; 
            using (TextReader textReader = new StreamReader(context.Request.InputStream))
                requestString = textReader.ReadToEnd().
                    Replace("&amp;", "&").
                    Replace("&gt;", ">").
                    Replace("&lt;", "<");

            string xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            StringBuilder result = new StringBuilder(xmlHeader.Length + requestString.Length + 100);
            result.Append(xmlHeader);
            bool quoteOpen = false;
            foreach (char c in requestString)
            {
                switch (c)
                {
                    case '"':
                        quoteOpen = !quoteOpen;
                        result.Append(c);
                        break;
                    case '<':
                        if (quoteOpen)
                            result.Append("&lt;");
                        else
                            result.Append(c);
                        break;
                    case '>':
                        if (quoteOpen)
                            result.Append("&gt;");
                        else
                            result.Append(c);
                        break;
                    case '&':
                        if (quoteOpen)
                            result.Append("&amp;");
                        else
                            result.Append(c);
                        break;
                    default:
                        result.Append(c);
                        break;
                }
            }
            return result.ToString().
                Replace("&amp;#10;", "&#10;").
                Replace("&amp;#13;", "&#13;").
                Replace("&amp;quot;", "&quot;");
        }

        // send report to the designer
        private void SendReportTemplate(HttpContext context)
        {
            AddNoCacheHeaders(context);
            string guid = context.Request.Params["getReport"];
            SetUpWebReport(guid);
            if (webReport != null)
            {
                string report = CutRestricted(webReport, webReport.Report.SaveToString());
                context.Response.Write(report);
            }
            else
                context.Response.StatusCode = 404;

            context.Response.End();
        }

        private string CutRestricted(WebReport webReport, string xmlString)
        {
            using (MemoryStream xmlStream = new MemoryStream())
            {
                Write(xmlStream, xmlString);
                xmlStream.Position = 0;
                FastReport.Utils.XmlDocument xml = new FastReport.Utils.XmlDocument();
                xml.Load(xmlStream);

                if (!webReport.DesignScriptCode)
                {
                    xml.Root.SetProp("CodeRestricted", "true");
                    // cut script
                    FastReport.Utils.XmlItem scriptItem = xml.Root.FindItem("ScriptText");
                    if (scriptItem != null && !String.IsNullOrEmpty(scriptItem.Value))
                        scriptItem.Value = String.Empty;                    
                }
                // cut connection strings
                FastReport.Utils.XmlItem dictionary = xml.Root.FindItem("Dictionary");
                if (dictionary != null)
                {
                    for (int i = 0; i < dictionary.Items.Count; i++)
                    {
                        FastReport.Utils.XmlItem item = dictionary.Items[i];
                        if (!String.IsNullOrEmpty(item.GetProp("ConnectionString")))
                            item.SetProp("ConnectionString", String.Empty);
                    }
                }

                // save prepared xml
                using (MemoryStream secondXmlStream = new MemoryStream())
                {
                    xml.Save(secondXmlStream);
                    secondXmlStream.Position = 0;
                    byte[] buff = new byte[secondXmlStream.Length];
                    secondXmlStream.Read(buff, 0, buff.Length);
                    xmlString = Encoding.UTF8.GetString(buff);
                }
            }
            return xmlString;
        }

        private void Write(Stream stream, string value)
        {
            byte[] buf = Encoding.UTF8.GetBytes(value);
            stream.Write(buf, 0, buf.Length);
        }

        private void SendResourcePic(HttpContext context)
        {            
            string file = context.Request.Params["respic"];
            context.Response.AddHeader("Content-Type", string.Concat("image/", Path.GetExtension(file)));
            string resname = file.Replace('/', '.');
            using (Stream stream = this.GetType().Assembly.GetManifestResourceStream(string.Concat("FastReport.Web.Resources.jquery.", resname)))            
            using (BinaryReader reader = new BinaryReader(stream))
            {
                byte[] res = reader.ReadBytes((int)stream.Length);
                context.Response.OutputStream.Write(res, 0, res.Length);
            }
            context.Response.End();
        }

        private void SendObjectResponse(HttpContext context)
        {
            AddNoCacheHeaders(context);
            string uuid = context.Request.Params["object"];
            SetUpWebReport(uuid); 

            if (!NeedExport(context) && !NeedPrint(context))
            {
                if (webReport.DesignReport)
                {
                    ReportLoad(context);
                    ReportRegisterDataAndRunEvent();
                    SendDesigner(context, uuid);
                }
                else
                {
                    SendReport(context);
                }
            }
            context.Response.End();
        }

        private void SendPreviewObjectResponse(HttpContext context)
        {
            AddNoCacheHeaders(context);
            string uuid = context.Request.Params["previewobject"];
            SetUpWebReport(uuid);

            if (!NeedExport(context) && !NeedPrint(context))
            {
                SendReport(context);
            }
            context.Response.End();
        }

        // On-line Designer
        private void SendDesigner(HttpContext context, string uuid)
        {
            StringBuilder sb = new StringBuilder();
            context.Response.AddHeader("Content-Type", "html/text");
            try
            {
                string designerPath = WebUtils.GetAppRoot(context, webReport.DesignerPath);
                sb.Append(String.Format("<iframe src=\"{0}?uuid={1}\" style=\"border:none;\" width=\"{2}\" height=\"{3}\" >", // min-width:930px;position:absolute;bottom:0px
                    designerPath, uuid, webReport.Width.ToString(), webReport.Height.ToString()));
                //sb.Append(String.Format("<iframe src=\"{0}?uuid={1}\" style=\"border:none;min-width:930px;min-height:600px\" width=\"{2}\" height=\"{3}\" >",
                //    designerPath, uuid, webReport.Width.ToString(), webReport.Height.ToString()));
                sb.Append("<p style=\"color:red\">ERROR: Browser does not support IFRAME!</p>");
                sb.AppendLine("</iframe>");

                // add resize here
                if (webReport.Height == System.Web.UI.WebControls.Unit.Percentage(100))
                    sb.Append(GetFitScript(uuid));
            }
            catch (Exception e)
            {
                log.AddError(e);
            }

            if (log.Text.Length > 0)
            {
                context.Response.Write(log.Text);
                log.Clear();
            }

            SetContainer(context, Properties.ControlID);
            context.Response.Write(sb.ToString());            
        }

        private string GetFitScript(string ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script>");
            sb.AppendLine("(function() {");
            sb.AppendLine(String.Format("var div = document.querySelector('#{0}'),", ID));
            sb.AppendLine("iframe,");
            sb.AppendLine("rect,");
            sb.AppendLine("e = document.documentElement,");
            sb.AppendLine("g = document.getElementsByTagName('body')[0],");
            //sb.AppendLine("x = window.innerWidth || e.clientWidth || g.clientWidth,");
            sb.AppendLine("y = window.innerHeight|| e.clientHeight|| g.clientHeight;");
            sb.AppendLine("if (div) {");
            sb.AppendLine("iframe = div.querySelector('iframe');");
            sb.AppendLine("if (iframe) {");
            sb.AppendLine("rect = iframe.getBoundingClientRect();");
            //sb.AppendLine("iframe.setAttribute('width', x - rect.left);");
            sb.AppendLine("iframe.setAttribute('height', y - rect.top - 11);");
            sb.AppendLine("}}}());");
            sb.AppendLine("</script>");
            return sb.ToString();
        }

        private void SendReport(HttpContext context)
        {
            StringBuilder sb = new StringBuilder();
            if (context.Request.Params["settab"] != null)
            {
                int i = 0;
                if (int.TryParse(context.Request.Params["settab"], out i))
                    if (i >= 0 && i < webReport.Tabs.Count)
                        webReport.CurrentTabIndex = i;
            }
            Properties.ControlID = context.Request.Params["object"];
            if (String.IsNullOrEmpty(Properties.ControlID))
                Properties.ControlID = context.Request.Params["previewobject"];
            context.Response.AddHeader("Content-Type", "html/text");
            try
            {
                if (context.Request.Params["refresh"] != null)
                {
                    webReport.Refresh();
                    sb.Append(webReport.Toolbar.GetHtmlProgress(context.Response.ApplyAppPathModifier(WebUtils.HandlerFileName), Properties.ControlID, false));
                }
                else
                {
                    if (context.Request.Params["dialog"] != null)
                        SetUpDialogs(webReport, context);
                    SetReportPage(context);
                    StartReport(sb, context);
                    StringBuilder sb_rep = new StringBuilder();
                    GetReportHTML(sb_rep, webReport, context);
                    if (!dialogRefresh)
                        GetReportTopToolbar(sb);
                    sb.Append(sb_rep);
                    GetReportBottomToolbar(sb);
                    EndReport(sb);
                }
            }
            catch (Exception e)
            {
                log.AddError(e);
            }

            if (log.Text.Length > 0)
            {
                context.Response.Write(log.Text);
                log.Clear();
            }

            SetContainer(context, dialogRefresh ? String.Concat(Properties.ControlID, "Dialog") : Properties.ControlID);

            context.Response.Write(sb.ToString());            
        }

        private void SetContainer(HttpContext context, string p)
        {
            context.Response.AddHeader("FastReport-container", p);
        }
        
        private bool NeedPrint(HttpContext context)
        {
            if (context.Request.Params["print_browser"] != null)
                webReport.ExportHtml(context, true, true);
            else if (context.Request.Params["print_pdf"] != null)
                webReport.ExportPdf(context, true);
            else
                return  false;
            return true;
        }

        private bool NeedExport(HttpContext context)
        {
            bool result = true;
            if (context.Request.Params["export_pdf"] != null)
                webReport.ExportPdf(context);
            else if (context.Request.Params["export_excel2007"] != null)
                webReport.ExportExcel2007(context);
            else if (context.Request.Params["export_word2007"] != null)
                webReport.ExportWord2007(context);
            else if (context.Request.Params["export_pp2007"] != null)
                webReport.ExportPowerPoint2007(context);
            else if (context.Request.Params["export_text"] != null)
                webReport.ExportText(context);
            else if (context.Request.Params["export_rtf"] != null)
                webReport.ExportRtf(context);
            else if (context.Request.Params["export_xps"] != null)
                webReport.ExportXps(context);
            else if (context.Request.Params["export_ods"] != null)
                webReport.ExportOds(context);
            else if (context.Request.Params["export_odt"] != null)
                webReport.ExportOdt(context);
            else if (context.Request.Params["export_mht"] != null)
                webReport.ExportMht(context);
            else if (context.Request.Params["export_xml"] != null)
                webReport.ExportXmlExcel(context);
            else if (context.Request.Params["export_dbf"] != null)
                webReport.ExportDbf(context);
            else if (context.Request.Params["export_csv"] != null)
                webReport.ExportCsv(context);
            else if (context.Request.Params["export_fpx"] != null)
                webReport.ExportPrepared(context);
            else
                result = false;
            return result;
        }

        private void SetReportPage(HttpContext context)
        {
            if (context.Request.Params["next"] != null)
                webReport.NextPage();
            else if (context.Request.Params["prev"] != null)
                webReport.PrevPage();
            else if (context.Request.Params["first"] != null)
                webReport.FirstPage();
            else if (context.Request.Params["last"] != null)
                webReport.LastPage();
            else if (context.Request.Params["goto"] != null)
            {
                int i = 0;
                if (int.TryParse(context.Request.Params["goto"], out i))
                    webReport.SetPage(i - 1);
            }
            else if (context.Request.Params["zoom_width"] != null)
                webReport.Prop.ZoomMode = ZoomMode.Width;
            else if (context.Request.Params["zoom_page"] != null)
                webReport.Prop.ZoomMode = ZoomMode.Page;
            else if (context.Request.Params["zoom_300"] != null)
                webReport.Prop.Zoom = 3;
            else if (context.Request.Params["zoom_200"] != null)
                webReport.Prop.Zoom = 2;
            else if (context.Request.Params["zoom_150"] != null)
                webReport.Prop.Zoom = 1.5f;
            else if (context.Request.Params["zoom_100"] != null)
                webReport.Prop.Zoom = 1;
            else if (context.Request.Params["zoom_90"] != null)
                webReport.Prop.Zoom = 0.9f;
            else if (context.Request.Params["zoom_75"] != null)
                webReport.Prop.Zoom = 0.75f;
            else if (context.Request.Params["zoom_50"] != null)
                webReport.Prop.Zoom = 0.5f;
            else if (context.Request.Params["zoom_25"] != null)
                webReport.Prop.Zoom = 0.25f;
        }

        private void SendDebug(HttpContext context)
        {
            int count = 0;
            if (WebFarmMode)
                count = WebReportCache.CleanStorage(FileStoragePath, WebReportCache.GetStorageTimeout(), WebReportCache.GetStorageCleanup());
            AddNoCacheHeaders(context);
            context.Response.ContentType = "text/html";
            context.Response.Write("FastReport.Web.WebExport handler: FastReport.NET");
            context.Response.Write(String.Concat("<b>v", FastReport.Utils.Config.Version, "</b><br/>",
                "Current server time: <b>", DateTime.Now.ToString(), "</b><br/>",
                "Cluster mode: <b>", (WebFarmMode ? "ON" : "OFF"), "</b><br/>",
                "Files in storage: <b>", count.ToString(), "</b>"));
            context.Response.End();
        }

        private void SendPrint(HttpContext context)
        {
            AddNoCacheHeaders(context);
            try
            {
                SetUpWebReport(context.Request.Params[WebUtils.ReportPrefix]);
                if (context.Request.Params[WebUtils.PrintPrefix] == "pdf")
                    webReport.PrintPdf(context);
                else
                    webReport.PrintHtml(context);
            }
            catch (Exception e)
            {
                log.AddError(e);
            }
            if (log.Text.Length > 0)
            {
                context.Response.Write(log.Text);
                log.Clear();
            }
            context.Response.End();
        }

        private void SendExport(HttpContext context)
        {
            log = new WebLog(false);
            string cacheKeyName = String.Concat(WebUtils.EXPORT, context.Request.QueryString[WebUtils.ConstID]);
            WebExportItem exportItem;
            if (WebFarmMode)
                exportItem = WebReportCache.GetFileStorage(String.Empty, cacheKeyName, FileStoragePath) as WebExportItem;
            else
                exportItem = HttpRuntime.Cache[cacheKeyName] as WebExportItem;
            if (exportItem != null)
            {
                try
                {
                    if (!WebFarmMode)
                        HttpRuntime.Cache.Remove(cacheKeyName);
                    context.Response.ClearContent();
                    context.Response.ClearHeaders();
                    if (string.IsNullOrEmpty(exportItem.ContentType))
                        context.Response.ContentType = "application/unknown";
                    else
                        context.Response.ContentType = exportItem.ContentType;
                    context.Response.AddHeader("Content-Type", context.Response.ContentType);
                    AddNoCacheHeaders(context);

                    string disposition = "attachment";
                    if (context.Request.QueryString["displayinline"].Equals("True", StringComparison.OrdinalIgnoreCase))
                        disposition = "inline";

                    string s = context.Server.UrlPathEncode(exportItem.FileName);
                    context.Response.AddHeader("Content-Disposition", string.Format("{0}; filename*=UTF-8''{1}", disposition, s));
                    context.Response.Flush();

                    WebUtils.ResponseChunked(context.Response, exportItem.File);
                }
                catch (Exception e)
                {
                    log.AddError(e);
                }

                if (log.Text.Length > 0)
                {
                    context.Response.Write(log.Text);
                    log.Clear();
                }
                context.Response.End();
            }
        }

        private void AddNoCacheHeaders(HttpContext context)
        {
            context.Response.AddHeader("Expires", "May, 3 Jul 1997 05:00:00 GMT");
            context.Response.AddHeader("Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0");
            context.Response.AddHeader("Pragma", "no-cache");
        }

        private void SendPicture(HttpContext context)
        {
            try
            {
                string imageKeyName = Convert.ToString(context.Request.Params[WebUtils.PicsPrefix]);
                context.Response.ContentType = "image/png";
                context.Response.Flush();
                byte[] image;
                if (WebFarmMode)
                    image = WebReportCache.GetFileStorage(String.Empty, imageKeyName, FileStoragePath) as byte[];
                else
                    image = HttpRuntime.Cache[imageKeyName] as byte[];
                if (image != null)
                    WebUtils.ResponseChunked(context.Response, image);
            }
            catch
            {
                // nothing
            }
            context.Response.End();
        }

        private void PutStorage(string prefix, string suffix, object value, int CacheDelay, System.Web.Caching.CacheItemPriority CachePriority)
        {
            if (value != null)
            {
                if (WebFarmMode)
                    WebReportCache.PutFileStorage(prefix, suffix, value, FileStoragePath);
                else
                {
                    WebReportCache.CacheAdd(String.Concat(prefix, suffix), value,
                        new CacheItemRemovedCallback(this.RemovedCallback),                        
                        CacheDelay, CachePriority);
                }
            }
        }

        private void RemovedCallback(String k, Object v, CacheItemRemovedReason r)
        {
            if (v is WebReport)
            {
                try
                {
                    if (((WebReport)v).Report.PreparedPages != null)
                        ((WebReport)v).Report.PreparedPages.Clear();
                    ((WebReport)v).Report.Dispose();
                    ((WebReport)v).Report = null;
                }
                catch
                {
                    // nothing
                }
            }
        }

        private void SetUpWebReport(string ID)
        {
            if (WebFarmMode)
                webReport =  WebReportCache.GetFileStorage(WebUtils.REPORT, ID, FileStoragePath) as WebReport;
            else
                webReport = WebReportCache.CacheGet(String.Concat(WebUtils.REPORT, ID)) as WebReport;
            if (webReport == null)
                webReport = new WebReport();
            log = new WebLog(webReport.Debug);
        }


        #endregion
    }
}
