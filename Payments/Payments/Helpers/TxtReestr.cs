﻿namespace Payments.Helpers
{
    using System.Text;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Ionic.Zip;
    using Payments.DS;

    public class TxtReestr
    {
        public static readonly string UnloadedPath = Application.StartupPath + "\\Выгрузки\\";
        public static DateTime LastUnloadedPaymentsDate = DateTime.Today;
        private static SortedList<string, string> unloadedFiles = new SortedList<string, string>();

        public static void UnloadToTxt(string fileName, PaymentsDataSet.TxtReestrDataTable table)
        {
            string filePath = UnloadedPath + fileName;
            int count = 0;
            decimal summ = 0;

            foreach (PaymentsDataSet.TxtReestrRow r in table)
            {
                FileHelper.WriteTextFile(filePath, r.Line, true, Encoding.Default);
                FileHelper.WriteTextFile(filePath, Environment.NewLine, true, Encoding.Default);
                r.ID_Loaded_Status = 1;

                count++;
                summ += r.Payment_Summ;
            }

            DAccess.DataModule.PaymentLoadedStatusUpdate(table);
            unloadedFiles.Add(
                filePath,
                "Файл " + fileName + " - количество: " + count.ToString() + ", сумма: " + Math.Round(summ, 2).ToString() + "руб. ");
        }

        public static string CompressLastUploadedFiles(bool deleteOriginalFiles)
        {
            List<string> files = new List<string>();
            string info = "";

            using (ZipFile zip = new ZipFile())
            {
                foreach (KeyValuePair<string, string> kvp in unloadedFiles)
                {
                    files.Add(kvp.Key);
                    info += kvp.Value + "\n";
                }

                FileHelper.WriteTextFile(UnloadedPath + "Information.txt", info, Encoding.UTF8);

                string saveZippedFile = UnloadedPath + "pays_" + LastUnloadedPaymentsDate.Year.ToString() +
                                            StringHelper.ZeroCode(LastUnloadedPaymentsDate.Month) +
                                            StringHelper.ZeroCode(LastUnloadedPaymentsDate.Day) + "_" +
                                            StringHelper.ZeroCode(DateTime.Now.Hour) +
                                            StringHelper.ZeroCode(DateTime.Now.Minute) + "_" +
                                            DAccess.Instance.CurrentUser.Name + ".zip";

                zip.AddFiles(files, false, "");
                zip.AddFile(UnloadedPath + "Information.txt", "");
                zip.Save(saveZippedFile);

                if (deleteOriginalFiles)
                    FileHelper.DeleteFiles(files);

                FileHelper.DeleteFileIfExist(UnloadedPath + "Information.txt");
                return saveZippedFile;
            }
        }

        public static void ClearUnloadedFiles()
        {
            unloadedFiles.Clear();
        }
    }
}
