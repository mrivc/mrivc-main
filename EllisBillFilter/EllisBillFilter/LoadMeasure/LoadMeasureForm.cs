﻿using System;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Text;

namespace EllisBillFilter.LoadMeasure
{
    public partial class LoadMeasureForm : Form
    {
        #region Constants

        private const string INVALID_FILE_TYPE = "Недопустимый тип файла";

        private const string VALIDATION_COMMAND = "SELECT [dbo].[Meter_In_Verification](@ID_Meter);";
        private const string LOAD_MEASURE_COMMAND = "[dbo].[Meter_Measure_Add]";
        private const string GET_LAST_MEASURE_VALUE_COMMAND = (@"
            SELECT Measure_Value FROM [dbo].[Meter_Measure] 
            WHERE
	            ID_Meter = @ID_Meter AND
	            CreationDate = (SELECT MAX(CreationDate) FROM [dbo].[Meter_Measure] WHERE ID_Meter = @ID_Meter)
        ");
        private const string GET_WORK_PERIOD_DATE_COMMAND = "SELECT [dbo].[Work_Period]();";
        private const string GET_PERIOD_BEGIN_DATE_COMMAND = "SELECT [dbo].[PeriodBeginDate](@Period)";

        private const string FILE_NAME = "Text file";
        private const string FILTER = "Text file (*.txt;*.csv)|*.txt;*.csv";
        private const string MATCH_PATTERN = @"\w*.(txt|csv)$";

        private const int PROGRESS_START = 0;

        const double ACCUMULATION_CEIL = 1.0;
        const double ACCUMULATION_FLOOR = 0.0;

        #endregion

        private bool _allFiles;

        private bool _isLoad;
        private SqlDatabase _sqlDatabase;

        public LoadMeasureForm(string connectionString)
        {
            InitializeComponent();

            Init(connectionString);
        }

        private void Init(string connectionString)
        {
            _allFiles = false;
            _isLoad = false;

            try { _sqlDatabase = SqlDatabase.Create(connectionString); }
            catch (Exception ex) { ExceptionHandling(ex); }

            openFileDialog.FileName = FILE_NAME;
            openFileDialog.Filter = FILTER;
        }

        private void ExceptionHandling(Exception ex)
        {
            MessageBox.Show(ex.Message);
        }

        private void ToggleBootMode()
        {
            _isLoad = !_isLoad;

            Cursor = _isLoad ? Cursors.WaitCursor : Cursors.Default;

            toolStripStatusLabel.Visible = _isLoad;
            toolStripProgressBar.Visible = _isLoad;
            toolStripProgressBar.Value = PROGRESS_START;

            Enabled = !_isLoad;
        }

        private void AsyncLoad()
        {
            if (backgroundWorker.IsBusy == true) return;

            if (!_isLoad) ToggleBootMode();

            backgroundWorker.RunWorkerAsync();
        }

        private bool IsValidString(string text, string pattern)
        {
            return new Regex(pattern).IsMatch(text);
        }

        private int CalculateProgress(ref double progressAccumulation, long fileSize, int lineSize)
        {
            progressAccumulation = progressAccumulation > ACCUMULATION_CEIL
                ? ACCUMULATION_FLOOR
                : progressAccumulation;
            progressAccumulation += lineSize / (double)fileSize * toolStripProgressBar.Maximum;

            return (int)progressAccumulation;
        }

        private string MarkFileAsLoaded(string fileName)
        {
            int lastDotIndex = fileName.LastIndexOf('.');
            string fileNameWithoutExtention = fileName.Remove(lastDotIndex, fileName.Length - lastDotIndex);
            string fileNameExtention = fileName.Remove(0, lastDotIndex);

            return $"{fileNameWithoutExtention}_LOADED{fileNameExtention}";
        }

        private void RenameFile(string oldName, string newName)
        {
            File.Move(oldName, newName);
        }

        #region Events

        private void loadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidString(filePathTextBox.Text, MATCH_PATTERN)) throw new Exception(INVALID_FILE_TYPE);

                AsyncLoad();
            }
            catch (Exception ex)
            {
                ExceptionHandling(ex);
            }
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel) return;

            filePathTextBox.Text = openFileDialog.FileName;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker worker = sender as BackgroundWorker;

                _sqlDatabase.Open();

                SqlCommand loadCommand = _sqlDatabase
                    .CreateCommand(LOAD_MEASURE_COMMAND, CommandType.StoredProcedure);
                SqlCommand validationCommand = _sqlDatabase
                    .CreateCommand(VALIDATION_COMMAND, CommandType.Text);
                SqlCommand getLastMeasureValueCommand = _sqlDatabase
                    .CreateCommand(GET_LAST_MEASURE_VALUE_COMMAND, CommandType.Text);
                SqlCommand getWorkPeriodCommand = _sqlDatabase
                    .CreateCommand(GET_WORK_PERIOD_DATE_COMMAND, CommandType.Text);
                SqlCommand getPeriodBeginDateCommand = _sqlDatabase
                    .CreateCommand(GET_PERIOD_BEGIN_DATE_COMMAND, CommandType.Text);

                string filePath = filePathTextBox.Text;

                using (StreamReader streamReader = new StreamReader(filePath))
                {
                    long fileSize = streamReader.BaseStream.Length;
                    string line = string.Empty;

                    double progressAccumulation = ACCUMULATION_FLOOR;

                    while ((line = streamReader.ReadLine()) != null)
                    {
                        try
                        {
                            new PersonalAccount(line, getWorkPeriodCommand, getPeriodBeginDateCommand)
                                .Load(loadCommand, validationCommand, getLastMeasureValueCommand);
                        }
                        catch (Exception ex) { ExceptionHandling(ex); }

                        int lineSize = Encoding.UTF8.GetByteCount(line);
                        int progress = CalculateProgress(ref progressAccumulation, fileSize, lineSize);

                        worker.ReportProgress(progress);
                    }
                }

                RenameFile(filePath, MarkFileAsLoaded(filePath));
                MessageBox.Show("Загрузка завершена!");
            }
            catch (Exception ex) { ExceptionHandling(ex); }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _sqlDatabase.Close();
            if (_isLoad) ToggleBootMode();
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int progress = toolStripProgressBar.Value + e.ProgressPercentage;

            if (progress > toolStripProgressBar.Maximum) progress = toolStripProgressBar.Maximum;

            toolStripProgressBar.Value = progress;
        }

        private void LoadMeasureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isLoad) e.Cancel = true;
        }

        private void filePathTextBox_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Link;
            else
                e.Effect = DragDropEffects.None;
        }

        private void filePathTextBox_DragDrop(object sender, DragEventArgs e)
        {
            TextBox target = (sender as TextBox);
            string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (files == null) return;

            if (_allFiles)
            {
                string separator = target.Multiline ? Environment.NewLine : ";";
                target.Text = string.Join(separator, files);
            }
            else { target.Text = files[0]; }
        }

        #endregion
    }
}
