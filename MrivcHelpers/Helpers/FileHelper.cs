﻿namespace MrivcHelpers.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    public class FileHelper
    {
        public static void WriteLogEmptyLine()
        {
            string filepath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "NLog.txt");
            FileHelper.WriteTextFile(filepath, Environment.NewLine, Encoding.Default, true);
        }

        public static void WriteLog(string message)
        {
            string filepath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "NLog.txt");
            FileHelper.WriteTextFile(filepath, ">>>" + DateTime.Now.ToString() + " - " + message + Environment.NewLine, Encoding.Default, true);
        }

        public static void WriteTextFile(string fileName, string fileContent, Encoding encoding, bool append)
        {
            CreateDirectory(GetFilePath(fileName));
            StreamWriter _sr = new StreamWriter(fileName, append, encoding);
            _sr.Write(fileContent);
            _sr.Close();
        }

        public static string RenameFile(string fileFullName, string newFileName)
        {
            int ind = fileFullName.LastIndexOf("\\");
            
            string path = fileFullName.Substring(0, ind);
            
            return path + "\\" + newFileName;
        }

        public static string GetFileExt(string file)
        {
            return file.Substring(file.LastIndexOf(".") + 1);
        }

        public static bool ExistFile(string fileName)
        {
            FileInfo _fi = new FileInfo(fileName);
            return _fi.Exists;
        }

        public static void DeleteFileIfExist(string fileName)
        {
            FileInfo _fi = new FileInfo(fileName);
            if (_fi.Exists)
                _fi.Delete();
        }

        public static void DeleteFiles(IEnumerable<string> collection)
        {
            FileInfo _fi;
            IEnumerator<string> en = collection.GetEnumerator();

            while (en.MoveNext())
            {
                _fi = new FileInfo(en.Current);
                if (_fi.Exists)
                    _fi.Delete();
            }
        }

        public static string ReadTextFile(string fileName)
        {
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(fileName))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            return string.Empty;
        }

        public static void WriteTextFileWithAppend(string fileName, string fileContent)
        {
            CreateDirectory(GetFilePath(fileName));
            StreamWriter _sr = new StreamWriter(fileName, true, Encoding.Default);
            _sr.Write(fileContent);
            _sr.Close();
        }

        public static void WriteTextFile(string fileName, string fileContent)
        {
            CreateDirectory(GetFilePath(fileName));
            StreamWriter _sr = new StreamWriter(fileName, false, Encoding.Default);
            _sr.Write(fileContent);
            _sr.Close();
        }

        public static void WriteTextFile(string fileName, string fileContent, Encoding encoding)
        {
            CreateDirectory(GetFilePath(fileName));
            StreamWriter _sr = new StreamWriter(fileName, false, encoding);
            _sr.Write(fileContent);
            _sr.Close();
        }

        public static string CreateDirectory(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);
            return directoryPath;
        }

        public static bool DirectoryExists(string directoryPath)
        {
            return Directory.Exists(directoryPath);
        }

        public static string GetFilePath(string filePath)
        {
            return Path.GetDirectoryName(filePath) + "\\";
        }

        public static string GetFileName(string filePath)
        {
            return Path.GetFileName(filePath);
        }

        public static FileInfo CopyFile(string copyFile, string destFilePath)
        {
            FileInfo _fi = new FileInfo(copyFile);
            _fi = _fi.CopyTo(destFilePath, true);
            return _fi;
        }

        //public static void CopyFile(string copyFile, string destFilePath)
        //{
        //    FileInfo _fi = new FileInfo(copyFile);
        //    _fi.CopyTo(destFilePath, true);
        //}

        public static void RewriteFileWithRename(string copyFile, string destFilePath)
        {
            FileInfo _fi = new FileInfo(copyFile);
            _fi.CopyTo(destFilePath, true);
            _fi.Delete();
        }

        public static List<FileInfo> GetDirectoryTree(
            string directory,
            string fileExtension)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(directory);
            List<FileInfo> files = new List<FileInfo>();

            FileHelper.GetDirectoryTree(directoryInfo, files, fileExtension);

            return files;
        }

        public static void GetDirectoryTree(System.IO.DirectoryInfo root, List<System.IO.FileInfo> resultFilesList, string fileExtension)
        {
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            // First, process all the files directly under this folder
            // files = root.GetFiles("*.txt");
            files = root.GetFiles(fileExtension);

            if (files != null)
            {
                foreach (System.IO.FileInfo fi in files)
                    resultFilesList.Add(fi);

                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    GetDirectoryTree(dirInfo, resultFilesList, fileExtension);
                }
            }
        }

        public static string CreateCorrectFileName (string fileName)
        {
            fileName = fileName.Trim();

            string[] symbols = new string[] {"/","\\",":","*","?", "\"", "<",">","|","«","»" };

            foreach (var symbol in symbols)
                fileName = fileName.Replace(symbol, "");

            return fileName;
        }
    }
}
