﻿using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace PaymentStatementParser
{
    public class FileHelper
    {
        public static void CreateDirectory(string path)
        {
            if (Directory.Exists(path)) return;

            Directory.CreateDirectory(path);
        }

        public static void CreateDirectory(string path, bool withCleaning = false)
        {
            if (Directory.Exists(path) && withCleaning)
            {
                Directory.Delete(path, true);
            }
            else if (Directory.Exists(path)) return;

            Thread.Sleep(500);

            Directory.CreateDirectory(path);
        }

        public static string GetFilePath(string filePath)
        {
            int length = filePath.Length;

            if (filePath[length - 1] == '\\') return filePath;

            int lastSlashIndex = filePath.LastIndexOf('\\');

            return filePath.Substring(0, lastSlashIndex + 1);
        }

        public static bool CheckFileType(string file, string type)
        {
            Regex regex = new Regex($@"{type}$");
            Match match = regex.Match(file);

            return match.ToString() == type;
        }

        public static string FilenameWithoutType(string file)
        {
            StringBuilder filenameCopy = new StringBuilder(file);

            filenameCopy.Remove(file.LastIndexOf('.'), filenameCopy.Length - file.LastIndexOf('.'));

            return filenameCopy.ToString();
        }
    }
}
