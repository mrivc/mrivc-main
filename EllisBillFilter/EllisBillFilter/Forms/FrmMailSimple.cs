﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using EllisBillFilter.Helpers;

    using MrivcHelpers.Helpers;

    public partial class FrmMailSimple : Form
    {
        public FrmMailSimple()
        {
            InitializeComponent();
        }

        public FrmMailSimple(string mailTo, string copyTo, string subject, string bodyText)
        {
            InitializeComponent();
            this.MailTo = mailTo;
            this.CopyTo = copyTo;
            this.Subject = subject;
            this.BodyText = bodyText;
            this.btbOk.Focus();
        }

        public string MailTo {
            get
            {
                return this.txtMailTo.Text;
            }
            set
            {
                MailSettings.MailTo = value;
                this.txtMailTo.Text = value;
            }
        }

        public string CopyTo
        {
            get
            {
                return this.txtCopyTo.Text;
            }
            set
            {
                MailSettings.CopyTo = value;
                this.txtCopyTo.Text = value;
            }
        }

        public string Subject
        {
            get
            {
                return this.txtSubject.Text;
            }
            set
            {
                MailSettings.Subject = value;
                this.txtSubject.Text = value;
            }
        }

        public string BodyText
        {
            get
            {
                return this.txtBodyText.Text;
            }
            set
            {
                MailSettings.BodyText = value;
                this.txtBodyText.Text = value;
            }
        }

        private void btbOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            SetMailSettings();
            this.Close();
        }

        private void SetMailSettings()
        {
            MailSettings.MailTo = txtMailTo.Text;
            MailSettings.CopyTo = txtCopyTo.Text;
            MailSettings.Subject = txtSubject.Text;
            MailSettings.BodyText = txtBodyText.Text;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
