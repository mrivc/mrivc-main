﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadingMeasureFromFile
{
    public class User
    {
        private string _login;
        private string _password;

        public string Login
        {
            get { return _login; }
        }
        public string Password
        {
            get { return _password; }
        }

        public User(string login, string password)
        {
            _login = login;
            _password = password;
        }
    }
}
