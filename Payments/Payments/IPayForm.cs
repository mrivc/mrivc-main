﻿using System;
using System.Collections.Generic;
using System.Text;
using Payments.Controls;
using System.Windows.Forms;
using Payments.LogicClasses;

namespace Payments
{
    interface IPayForm
    {
        PaymentClass PaymentClass { get; }
        PanelPayment PaymentInfo { get; }
        Form Window { get; }
        void SavePaymentDetail();
    }
}
