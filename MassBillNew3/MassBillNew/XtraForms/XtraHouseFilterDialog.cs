using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace MassBillNew
{
    public partial class XtraHouseFilterDialog : DevExpress.XtraEditors.XtraForm
    {
        private string stringOfQuery = string.Empty;
        public string StringOfQuery
        {
            get
            {
                return stringOfQuery;
            }
            set
            {
                stringOfQuery = value;
            }
        }

        private DataTable addressFilterSet;
        public DataTable AddressFilterSet
        {
            get
            {
                return addressFilterSet;
            }
            set
            {
                addressFilterSet = value;
            }
        }


        public XtraHouseFilterDialog(AccountFilter.FilterType Type, bool SelectAllEnabled, DataTable AddressSet)
        {
            InitializeComponent();

            accountFilter2.Init(Type,SelectAllEnabled);

            if (AddressSet.Rows.Count != 0)
                accountFilter2.FillDataSet(AddressSet);
        }

        public XtraHouseFilterDialog(AccountFilter.FilterType Type, bool SelectAllEnabled)
        {
            InitializeComponent();

            accountFilter2.Init(Type, SelectAllEnabled);

        }



        private void simpleButton1_Click(object sender, EventArgs e)
        {
            stringOfQuery = accountFilter2.CreateSqlQuery();
            addressFilterSet = accountFilter2.CreateDataTable();
        }


    }
}