﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ImportMunicipalServices : MrivcImportCommand, IMrivcImportCommandAsync
    {
        ExportDataProvider exportDataProvider;

        public ImportMunicipalServices(string senderID) : base("importMunicipalServices", "NsiAsync", senderID)
        {
            _importValueName = "ImportMainMunicipalService";
            Description = "Синхронизация услуг";

            exportDataProvider = new ExportDataProvider(SenderID);
        }

        public new void Execute()
        {
            ExportData();
            ImportData();
            ExportData();
        }

        public void ExportData()
        {
            if (!this.IsEmpty && !HasErrors)
            {
                exportDataProvider.AddToReport = this.AddToReport;
                exportDataProvider.Description = this.Description;
                exportDataProvider.CommandType = CommandType;
                var dataProviderRegNumbers = "51";
                exportDataProvider.ExecuteToDataBase(dataProviderRegNumbers.Split(','));

                HasErrors = exportDataProvider.HasErrors;
            }
        }

        public void ImportData()
        {
            if (!HasErrors)
                base.Execute();
        }

        public override object Clone()
        {
            var exportDataProviderClone = (ExportDataProvider)exportDataProvider.Clone();
            var baseClone = (MrivcCommand)base.Clone();
            ((ImportMunicipalServices)baseClone).exportDataProvider = exportDataProviderClone;
            return baseClone;
        }

        protected override void CreateXmlBody()
        {
            var munServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.Prepare_Import_Municipal_Services @senderID", new[,] { { "@senderID", SenderID } });

            var commonValues = new[]
            {
                new Tuple<string[],string>(new [] { "MunicipalServiceRef", "Code" },"MunicipalServiceRefCode"),
                new Tuple<string[],string>(new [] { "MunicipalServiceRef", "GUID" },"MunicipalServiceRefGUID"),
                new Tuple<string[],string>(new [] { "MunicipalServiceRef", "Name" },"MunicipalServiceRefName"),
                new Tuple<string[],string>(new [] { "MainMunicipalServiceName" },"MainMunicipalServiceName"),
                new Tuple<string[],string>(new [] { "MunicipalResourceRef", "Code" },"MunicipalResourceRefCode"),
                new Tuple<string[],string>(new [] { "MunicipalResourceRef", "GUID" },"MunicipalResourceRefGUID"),
                new Tuple<string[],string>(new [] { "MunicipalResourceRef", "Name" },"MunicipalResourceRefName")
                //new Tuple<string[],string>(new [] { "OKEI" },"OKEI")
                //new Tuple<string[],string>(new [] { "SortOrder" },"SortOrder")
            };

            var updateValues = new[]
            {
                new Tuple<string[],string>(new [] { "ElementGuid" },"ElementGUID")
            };

            foreach (DataRow row in munServices.Select())
            {
                AddChildElement(row, "NsiAsync", "importMunicipalServices_add", "importMunicipalServices_update", commonValues, null, updateValues, "importMunicipalServicesRequest");
            }
        }
    }
}
