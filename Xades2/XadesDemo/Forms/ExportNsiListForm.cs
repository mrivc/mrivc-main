﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo
{
    public partial class ExportNsiListForm : Form
    {
        public ExportNsiListForm()
        {
            InitializeComponent();
        }

        private void exportNsiListButton_Click(object sender, EventArgs e)
        {
                var exportNsiList = new ExportNsiList();
                exportNsiList.SenderID = "";
                var nsiRegNumbers = MrivcStatic.GisConfig.ExportNsiRegistryNumbers;
                exportNsiList.NsiRegistryNumbers = nsiRegNumbers;
                exportNsiList.Execute();
        }

        private void ExportNsiListForm_Load(object sender, EventArgs e)
        {

        }

        private void exportNsiProviderBtn_Click(object sender, EventArgs e)
        {
                var exportDataProvider = new ExportDataProvider("");
                var dataProviderRegNumbers = "1,51";
                exportDataProvider.ExecuteToDataBase(dataProviderRegNumbers.Split(','));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var exportOrgRegistry = new ExportOrgRegistry();
            exportOrgRegistry.Execute();
        }
    }
}
