﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo
{
    public partial class ExportContractsForm : Form
    {
        public string SenderID
        {
            get { return ""; }
        }

        public ExportContractsForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var exportContracts = new ExportContracts(SenderID);
            var ds = MrivcStatic.HouseIteration(exportContracts, false);

            var exportContractsStatus = new ExportContractsStatus(SenderID);
            exportContractsStatus.Execute();
        }

        private void ExportContractsForm_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var exportSupplyContracts = new ExportSupplyContracts(SenderID);
            exportSupplyContracts.Execute();
        }
    }
}
