using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FastReport.Forms;
using FastReport.Utils;

namespace FastReport.Cloud.StorageClient.Dropbox
{
    /// <summary>
    /// Represents the Application Info diabolg form.
    /// </summary>
    public partial class ApplicationInfoForm : BaseDialogForm
    {
        #region Fields

        private string key;
        private string secret;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Gets the application key.
        /// </summary>
        public string Key
        {
            get { return key; }
        }

        /// <summary>
        /// Gets the application secret.
        /// </summary>
        public string Secret
        {
            get { return secret; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationInfoForm"/> class.
        /// </summary>
        public ApplicationInfoForm()
        {
            this.key = "";
            this.secret = "";
            InitializeComponent();
            Localize();
        }

        #endregion // Constructors

        #region Public Methods

        /// <inheritdoc/>
        public override void Localize()
        {
            base.Localize();

            MyRes res = new MyRes("Cloud,Dropbox");
            this.Text = res.Get("ApplicationInfoDialog");
            labelAppKey.Text = res.Get("ApplicationKey");
            labelAppSecret.Text = res.Get("ApplicationSecret");
        }

        #endregion // Public Methods

        #region Events Handlers

        private void btnOk_Click(object sender, EventArgs e)
        {
            key = tbAppKey.Text;
            secret = tbAppSecret.Text;
            this.Close();
        }

        #endregion // Events Handlers
    }
}