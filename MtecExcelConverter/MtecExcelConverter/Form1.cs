﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MtecExcelConverter
{
    using System.IO;

    using MtecExcelConverter.Helpers;
    using MtecExcelConverter.Properties;

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Text = this.Text + " (" + DBManager.Connection.DataSource + " - " + DBManager.Connection.Database + ")" + " v:" + System.Windows.Forms.Application.ProductVersion;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string directory = ChooseDirectory();

            txtDirectory.Text = directory;

            List<FileInfo> files = FileHelper.GetDirectoryTree(directory, "*.xls");
            files.ForEach(fi => lbFiles.Items.Add(fi.FullName));
            lblFilesCount.Text = string.Format("Всего файлов: {0}", files.Count);
        }

        private void btnLoadData_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (txtDirectory.Text.Length == 0)
            {
                MessageBox.Show("Необходимо указать папку с файлами.", "Загрузка отмена", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            ImportData importData = new ImportData(txtDirectory.Text, Guid.NewGuid());
            int i = 1;
            foreach (var fileItem in lbFiles.Items)
            {
                ExcelHelper.ReadExcelFile(importData, fileItem.ToString());
                lblFilesLoaded.Text = string.Format("Обработано файлов: {0}", i);
                Console.WriteLine(string.Format("{0} - {1}", i, fileItem));
                i++;
            }

            importData.SaveDataToDataBase();

            if (cbShowDataAfterLoad.Checked)
            {
                dataGridView1.DataSource = importData.DataTable;
                tabControl1.SelectedTab = tabPage2;
            }

            this.Cursor = Cursors.Default;
            MessageBox.Show("Данные были загружены успешно.", "Загрузка закончена", MessageBoxButtons.OK, MessageBoxIcon.Information);
            FlashWindow.Flash(this);
        }

        private string ChooseDirectory()
        {
            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                dlg.SelectedPath = Settings.Default.LastDirectory;
                dlg.Description = "Укажите папку с файлами";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    Settings.Default.LastDirectory = dlg.SelectedPath;
                    Settings.Default.Save();
                    return dlg.SelectedPath;
                }
            }
            return string.Empty;
        }
    }
}
