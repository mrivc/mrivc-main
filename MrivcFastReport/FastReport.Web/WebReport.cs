using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Caching;
using System.IO;
using FastReport;
using FastReport.Export;
using FastReport.Export.Html;
using FastReport.Export.RichText;
using FastReport.Export.OoXML;
using FastReport.Utils;
using System.Threading;
using System.Drawing;
using System.Drawing.Design;
using System.Collections;
using System.Data;
using FastReport.Data;
using System.Web.UI.Design;

namespace FastReport.Web
{
    /// <summary>
    /// Represents the Web Report.
    /// </summary>        
    [Designer("FastReport.VSDesign.WebReportComponentDesigner, FastReport.VSDesign, Version=1.0.0.0, Culture=neutral, PublicKeyToken=db7e5ce63278458c, processorArchitecture=MSIL")]
    [ToolboxBitmap(typeof(WebReport), "Resources.Report.bmp")]

    public
    partial class WebReport : WebControl, INamingContainer
    {
        #region Private fields

        private string fReportGuid;
        private WebToolbar fToolbar;
        private bool WebFarmMode = false;        
        private string FileStoragePath;
        private int FileStorageTimeout;
        private int FileStorageCleanup;
        private int CurrentReportIndex = -1;
        private List<ReportTab> tabs;
        private bool fPreviewMode = false;
      
        #endregion

        #region Public properties

        #region Layout

        /// <summary>
        /// Get or sets auto width of report
        /// </summary>
        [DefaultValue(false)]
        [Category("Layout")]
        [Browsable(true)]
        public bool AutoWidth
        {
            get { return Prop.AutoWidth; }
            set { Prop.AutoWidth = value; }
        }

        /// <summary>
        /// Enable or disable inline object registration
        /// </summary>
        [DefaultValue(false)]
        [Category("Layout")]
        [Browsable(true)]
        public bool InlineRegistration
        {
            get { return Prop.InlineRegistration; }
            set { Prop.InlineRegistration = value; }
        }

        /// <summary>
        /// Enable or disable using of external jQuery library
        /// </summary>
        [DefaultValue(false)]
        [Category("Layout")]
        [Browsable(true)]
        public bool ExternalJquery
        {
            get { return Prop.ExternalJquery; }
            set { Prop.ExternalJquery = value; }
        }

        /// <summary>
        /// Get or sets auto height of report
        /// </summary>
        [DefaultValue(false)]
        [Category("Layout")]
        [Browsable(true)]
        public bool AutoHeight
        {
            get { return Prop.AutoHeight; }
            set { Prop.AutoHeight = value; }
        }

        /// <summary>
        /// Enable or disable layers mode visualisation
        /// </summary>
        [DefaultValue(false)]
        [Category("Layout")]
        [Browsable(true)]
        public bool Layers
        {
            get { return Prop.Layers; }
            set { Prop.Layers = value; }
        }

        /// <summary>
        /// Gets or sets Padding of Report section
        /// </summary>
        [Category("Layout")]
        [Browsable(true)]
        public System.Windows.Forms.Padding Padding
        {
            get { return Prop.Padding; }
            set { Prop.Padding = value; }
        }
        #endregion

        #region Network
        /// <summary>
        /// Delay in cache in minutes
        /// </summary>
        [Category("Network")]
        [DefaultValue(60)]
        [Browsable(true)]
        public int CacheDelay
        {
            get { return Prop.CacheDelay; }
            set { Prop.CacheDelay = value; }
        }

        /// <summary>
        /// Priority of items in cache
        /// </summary>
        [Category("Network")]
        [DefaultValue(CacheItemPriority.Normal)]
        [Browsable(true)]
        public CacheItemPriority CachePriority
        {
            get { return Prop.CachePriority; }
            set { Prop.CachePriority = value; }
        }

        #endregion

        #region Designer
        /// <summary>
        /// Enable the Report Designer
        /// </summary>
        [DefaultValue(false)]
        [Category("Designer")]
        [Browsable(true)]
        public bool DesignReport
        {
            get { return Prop.DesignReport; }
            set { Prop.DesignReport = value; }
        }

        /// <summary>
        /// Enable code editor in the Report Designer
        /// </summary>
        [DefaultValue(false)]
        [Category("Designer")]
        [Browsable(true)]
        public bool DesignScriptCode
        {
            get { return Prop.DesignScriptCode; }
            set { Prop.DesignScriptCode = value; }
        }

        /// <summary>
        /// Gets or sets path to the Report Designer
        /// </summary>
        [DefaultValue("~/WebReportDesigner/index.html")]
        [Category("Designer")]
        [Browsable(true)]
        public string DesignerPath
        {
            get { return Prop.DesignerPath; }
            set { Prop.DesignerPath = value; }
        }

        /// <summary>
        /// Gets or sets path to a folder for save designed reports
        /// </summary>
        [DefaultValue("")]
        [Category("Designer")]
        [Browsable(true)]
        public string DesignerSavePath
        {
            get { return Prop.DesignerSavePath; }
            set { Prop.DesignerSavePath = value; }
        }

        /// <summary>
        /// Gets or sets path to callback page after Save from Designer
        /// </summary>
        [DefaultValue("")]
        [Category("Designer")]
        [Browsable(true)]
        public string DesignerSaveCallBack
        {
            get { return Prop.DesignerSaveCallBack; }
            set { Prop.DesignerSaveCallBack = value; }
        }

        #endregion Designer

        #region Report
        /// <summary>
        /// Report Resource String
        /// </summary>
        [DefaultValue("")]
        [Category("Report")]
        [Browsable(true)]
        public string ReportResourceString
        {
            get { return Prop.ReportResourceString; }
            set { Prop.ReportResourceString = value; }
        }

        /// <summary>
        /// Gets or sets report data source(s).
        /// </summary>
        /// <remarks>
        /// To pass several datasources, use ';' delimiter, for example: 
        /// "sqlDataSource1;sqlDataSource2"
        /// </remarks>
        [DefaultValue("")]
        [Category("Report")]
        [Browsable(true)]
        public string ReportDataSources
        {
            get { return Prop.ReportDataSources; }
            set { Prop.ReportDataSources = value; }
        }
        /// <summary>
        /// Switch the pictures visibility in report
        /// </summary>
        [DefaultValue(true)]
        [Category("Report")]
        [Browsable(true)]
        public bool Pictures
        {
            get { return Prop.Pictures; }
            set { Prop.Pictures = value; }
        }
        /// <summary>
        /// Switch the pages layout between multiple pages and single page
        /// </summary>
        [DefaultValue(false)]
        [Category("Report")]
        [Browsable(true)]
        public bool SinglePage
        {
            get { return Prop.SinglePage; }
            set { Prop.SinglePage = value; }
        }
        /// <summary>
        /// Gets or sets the name of report file.
        /// </summary>
        [DefaultValue("")]
        [Category("Report")]
        [Browsable(true)]
        [Editor("FastReport.VSDesign.ReportFileEditor, FastReport.VSDesign, Version=1.0.0.0, Culture=neutral, PublicKeyToken=db7e5ce63278458c, processorArchitecture=MSIL", typeof(UITypeEditor))]
        public string ReportFile
        {
            get { return Prop.ReportFile; }
            set { Prop.ReportFile = value; }
        }

        /// <summary>
        /// Gets or sets the name of localization file.
        /// </summary>
        [DefaultValue("")]
        [Category("Report")]
        [Browsable(true)]
        [Editor("FastReport.VSDesign.LocalizationFileEditor, FastReport.VSDesign, Version=1.0.0.0, Culture=neutral, PublicKeyToken=db7e5ce63278458c, processorArchitecture=MSIL", typeof(UITypeEditor))]
        public string LocalizationFile
        {
            get { return Prop.LocalizationFile; }
            set { Prop.LocalizationFile = value; }
        }

        /// <summary>
        /// Set the zoom factor of previewed page between 0..1
        /// </summary>
        [DefaultValue(1f)]
        [Category("Report")]
        [Browsable(true)]
        public float Zoom
        {
            get { return Prop.Zoom; }
            set { Prop.Zoom = value; }
        }

        /// <summary>
        /// Enable or disable of showing the report after call Prepare
        /// </summary>
        [DefaultValue(false)]
        [Category("Report")]
        [Browsable(true)]
        public bool ShowAfterPrepare
        {
            get { return Prop.ShowAfterPrepare; }
            set { Prop.ShowAfterPrepare = value; }
        }

        /// <summary>
        /// Enable or disable of showing the debug information on errors
        /// </summary>
        [DefaultValue(false)]
        [Category("Report")]
        [Browsable(true)]
        public bool Debug
        {
            get { return Prop.Debug; }
            set { Prop.Debug = value; }
        }
        #endregion

        #region Toolbar

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(ToolbarStyle.Large)]
        [Category("Toolbar")]
        [Browsable(true)]
        public ToolbarStyle ToolbarStyle
        {
            get { return Prop.ToolbarStyle; }
            set { Prop.ToolbarStyle = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(ToolbarIconsStyle.Red)]
        [Category("Toolbar")]
        [Browsable(true)]
        public ToolbarIconsStyle ToolbarIconsStyle
        {
            get { return Prop.ToolbarIconsStyle; }
            set { Prop.ToolbarIconsStyle = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(ToolbarBackgroundStyle.Light)]
        [Category("Toolbar")]
        [Browsable(true)]
        public ToolbarBackgroundStyle ToolbarBackgroundStyle
        {
            get { return Prop.ToolbarBackgroundStyle; }
            set { Prop.ToolbarBackgroundStyle = value; }
        }

        /// <summary>
        ///  Switch toolbar visibility
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowToolbar
        {
            get { return Prop.ShowToolbar; }
            set { Prop.ShowToolbar = value; }
        }

        /// <summary>
        /// Sets the path to custom buttons on site. 
        /// </summary>
        /// <remarks>
        /// Pictures should be named:
        /// Checkbox.gif, Progress.gif, toolbar.png, toolbar_background.png, toolbar_big.png, toolbar_disabled.png, toolbar_disabled.png
        /// </remarks>
        [DefaultValue("")]
        [Category("Toolbar")]
        [Browsable(true)]
        //[UrlProperty]
        [Editor("FastReport.VSDesign.ReportFileEditor, FastReport.VSDesign, Version=1.0.0.0, Culture=neutral, PublicKeyToken=db7e5ce63278458c, processorArchitecture=MSIL", typeof(UITypeEditor))]
        public string ButtonsPath
        {
            get { return Prop.ButtonsPath; }
            set { Prop.ButtonsPath = value; }
        }

        /// <summary>
        ///  Switch visibility of Exports in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowExports
        {
            get { return Prop.ShowExports; }
            set { Prop.ShowExports = value; }
        }

        /// <summary>
        ///  Switch visibility of Print button in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowPrint
        {
            get { return Prop.ShowPrint; }
            set { Prop.ShowPrint = value; }
        }

        /// <summary>
        ///  Switch visibility of First Button in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowFirstButton
        {
            get { return Prop.ShowFirstButton; }
            set { Prop.ShowFirstButton = value; }
        }

        /// <summary>
        ///  Switch visibility of Previous Button in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowPrevButton
        {
            get { return Prop.ShowPrevButton; }
            set { Prop.ShowPrevButton = value; }
        }

        /// <summary>
        ///  Switch visibility of Next Button in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowNextButton
        {
            get { return Prop.ShowNextButton; }
            set { Prop.ShowNextButton = value; }
        }

        /// <summary>
        ///  Switch visibility of Last Button in toolbar
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowLastButton
        {
            get { return Prop.ShowLastButton; }
            set { Prop.ShowLastButton = value; }
        }

        /// <summary>
        /// Switch visibility of Zoom in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowZoomButton
        {
            get { return Prop.ShowZoomButton; }
            set { Prop.ShowZoomButton = value; }
        }

        /// <summary>
        /// Switch visibility of Refresh in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowRefreshButton
        {
            get { return Prop.ShowRefreshButton; }
            set { Prop.ShowRefreshButton = value; }
        }

        /// <summary>
        /// Switch visibility of Page Number in toolbar.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Browsable(true)]
        public bool ShowPageNumber
        {
            get { return Prop.ShowPageNumber; }
            set { Prop.ShowPageNumber = value; }
        }

        /// <summary>
        /// Set the Toolbar color.
        /// </summary>
        [DefaultValue(0xECE9D8)]
        [Category("Toolbar")]
        [Browsable(true)]
        public System.Drawing.Color ToolbarColor
        {
            get { return Prop.ToolbarColor; }
            set { Prop.ToolbarColor = value; }
        }
        #endregion

        #region Print

        /// <summary>
        /// Enable or disable print in PDF
        /// </summary>
        [DefaultValue(true)]
        [Category("Print")]
        [Browsable(true)]
        public bool PrintInPdf
        {
            get { return Prop.PrintInPdf; }
            set { Prop.PrintInPdf = value; }
        }

        /// <summary>
        /// Enable or disable print in browser
        /// </summary>
        [DefaultValue(true)]
        [Category("Print")]
        [Browsable(true)]
        public bool PrintInBrowser
        {
            get { return Prop.PrintInBrowser; }
            set { Prop.PrintInBrowser = value; }
        }

        /// <summary>
        /// Sets the width of print window
        /// </summary>
        [DefaultValue("700px")]
        [Category("Print")]
        [Browsable(true)]
        public string PrintWindowWidth
        {
            get { return Prop.PrintWindowWidth; }
            set { Prop.PrintWindowWidth = value; }
        }

        /// <summary>
        /// Sets the height of print window
        /// </summary>
        [DefaultValue("500px")]
        [Category("Print")]
        [Browsable(true)]
        public string PrintWindowHeight
        {
            get { return Prop.PrintWindowHeight; }
            set { Prop.PrintWindowWidth = value; }
        }

        #endregion

        #region Hidden Properties

        [Browsable(false)]
        internal bool ControlVisible
        {
            get { return Prop.ControlVisible; }
            set { Prop.ControlVisible = value; }
        }

        [Browsable(false)]
        internal string StoragePath
        {
            get { return FileStoragePath; }
            set { FileStoragePath = value; }
        }

        [Browsable(false)]
        internal bool WebFarm
        {
            get { return WebFarmMode; }
            set { WebFarmMode = value; }
        }


        [Browsable(false)]
        internal List<ReportTab> ReportTabs
        {
            get
            {
                InitTabs();
                return tabs;
            }
            set
            {
                tabs = value;
                InitTabs();
            }
        }

        /// <summary>
        /// Direct access to Properties of report object
        /// </summary>
        [Browsable(false)]
        internal WebReportProperties Prop
        {
            get
            {
                return ReportTabs[CurrentReportIndex].Properties;
            }
            set
            {
                ReportTabs[CurrentReportIndex].Properties = value;
            }
        }

        /// <summary>
        /// Direct access to Tabs
        /// </summary>
        [Browsable(false)]
        public List<ReportTab> Tabs
        {
            get
            {
                return ReportTabs;
            }
        }

        /// <summary>
        /// Gets or sets current tab index
        /// </summary>
        [Browsable(false)]
        public int CurrentTabIndex
        {
            get
            {
                return CurrentReportIndex;
            }

            set
            {
                CurrentReportIndex = value;
                Toolbar.CurrentTabIndex = CurrentReportIndex;
            }
        }

        /// <summary>
        /// Gets current tab
        /// </summary>
        [Browsable(false)]
        public ReportTab CurrentTab
        {
            get 
            {
                return ReportTabs[CurrentReportIndex]; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        internal WebToolbar Toolbar
        {
            get
            {
                if (fToolbar == null)
                    fToolbar = new WebToolbar(
                        String.Concat(this.Width.Value.ToString(), GetHtmlUnitName(this.Width)), 
                        String.Concat(this.Height.Value.ToString(), GetHtmlUnitName(this.Height)),
                        ReportGuid, ReportTabs,
                        (this.Width.Type != UnitType.Percentage) && (this.Height.Type != UnitType.Percentage));
                return fToolbar;
            }
            set
            {
                fToolbar = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public bool PreviewMode
        {
            get { return fPreviewMode; }
            set { fPreviewMode = value; }
        }

        private string GetHtmlUnitName(Unit unit)
        {
            switch (unit.Type)
            {
                case UnitType.Pixel:
                    return "px";
                case UnitType.Percentage:
                    return "%";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Direct access to Report object
        /// </summary>
        [Browsable(false)]
        public Report Report
        {
            get
            {
                return ReportTabs[CurrentReportIndex].Report;
            }
            set
            {
                ReportTabs[CurrentReportIndex].Report = value;
            }
        }

        /// <summary>
        /// Gets total pages of current report
        /// </summary>
        [Browsable(false)]
        public int TotalPages
        {
            get
            {
                if (Report.PreparedPages != null)
                    return Report.PreparedPages.Count;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Gets or sets current state of report
        /// </summary>
        [Browsable(false)]
        public ReportState State
        {
            get { return Prop.State; }
            set { Prop.State = value; }
        }

        /// <summary>
        /// Return true if report done
        /// </summary>
        [Browsable(false)]
        public bool ReportDone
        {
            get { return Prop.ReportDone; }
            set { Prop.ReportDone = value; }
        }

        /// <summary>
        /// Gets or sets number of current page
        /// </summary>
        [Browsable(false)]
        public int CurrentPage
        {
            get { return Prop.CurrentPage; }
            set { Prop.CurrentPage = value; }
        }

        /// <summary>
        /// Gets or sets guid of report
        /// </summary>
        [Browsable(false)]
        public string ReportGuid
        {
            get 
            { 
                if (String.IsNullOrEmpty(fReportGuid))
                    fReportGuid = WebUtils.GetGUID();
                return fReportGuid; 
            }
            set { fReportGuid = value; }
        }

        #endregion

        #endregion

        #region Events

        /// <summary>
        /// Occurs when report execution is started.
        /// </summary>
        [Browsable(true)]
        public event EventHandler StartReport;
        #endregion

        private object GetStorage(string prefix, string suffix)
        {
            string uid = String.Concat(prefix, suffix);
            object obj;

            if (WebFarmMode)
            {
                obj = WebReportCache.GetFileStorage(prefix, suffix, FileStoragePath);
            }
            else
            {
                obj = WebReportCache.CacheGet(uid);
            }            
            return obj;
        }

        private void PutStorage(string prefix, string suffix, object value)
        {
            if (value != null)
            {
                if (WebFarmMode)
                {
                    WebReportCache.PutFileStorage(prefix, suffix, value, FileStoragePath);
                }
                else
                {
                    WebReportCache.CacheAdd(String.Concat(prefix, suffix), value, //null,
                        new CacheItemRemovedCallback(this.RemovedCallback),
                        CacheDelay, CachePriority);
                }
            }
        }

        /// <summary>
        /// Runs on report start
        /// </summary>
        /// <param name="e"></param>
        public void OnStartReport(EventArgs e)
        {
            if (StartReport != null)
            {
                StartReport(this, e);
            }
        }

        #region Navigation

        /// <summary>
        /// Force go to next report page
        /// </summary>
        public void NextPage()
        {
            if (CurrentPage < TotalPages - 1)
                CurrentPage++;
        }

        /// <summary>
        /// Force go to previous report page
        /// </summary>
        public void PrevPage()
        {
            if (CurrentPage > 0)
                CurrentPage--;
        }

        /// <summary>
        /// Force go to first report page
        /// </summary>
        public void FirstPage()
        {
            CurrentPage = 0;
        }

        /// <summary>
        /// Force go to last report page
        /// </summary>
        public void LastPage()
        {
            CurrentPage = TotalPages - 1;
        }

        /// <summary>
        /// Force go to "value" report page
        /// </summary>
        public void SetPage(int value)
        {
            if (value >= 0 && value < TotalPages)
                CurrentPage = value;
        }

        #endregion

        /// <summary>
        /// Prepare the report
        /// </summary>
        public void Prepare()
        {
            //this.Controls.Clear();
            Refresh();
        }

        /// <summary>
        /// Force refresh of report
        /// </summary>
        public void Refresh()
        {
            CurrentPage = 0;
            Prop.CurrentForm = 0;
            State = ReportState.Empty;
            ControlVisible = true;
            ShowAfterPrepare = false;
        }

        /// <summary>
        /// Adds new report tab
        /// </summary>
        /// <param name="report"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public ReportTab AddTab(Report report, string name)
        {
            ReportTab tab = new ReportTab(report);
            tab.Properties.ControlID = ReportGuid;
            tab.Properties.HandlerURL = WebUtils.GetBasePath(this.Context) + WebUtils.HandlerFileName;
            tab.Name = name;
            ReportTabs.Add(tab);
            return tab;
        }

        /// <summary>
        /// Adds new report tab
        /// </summary>
        /// <param name="report"></param>
        /// <returns></returns>
        public ReportTab AddTab(Report report)
        {
            ReportTab tab = new ReportTab(report);
            tab.Properties.ControlID = ReportGuid;
            tab.Properties.HandlerURL = WebUtils.GetBasePath(this.Context) + WebUtils.HandlerFileName;
            ReportTabs.Add(tab);
            return tab;
        }

        /// <summary>
        /// Adds new report tab
        /// </summary>
        /// <param name="report"></param>
        /// <param name="name"></param>
        /// <param name="reportDone"></param>
        /// <returns></returns>
        public ReportTab AddTab(Report report, string name, bool reportDone)
        {
            ReportTab tab = new ReportTab(report);
            tab.Properties.ControlID = ReportGuid;
            tab.Properties.HandlerURL = WebUtils.GetBasePath(this.Context) + WebUtils.HandlerFileName;
            tab.Name = name;
            tab.Properties.ReportDone = reportDone;
            tab.Properties.ShowRefreshButton = !reportDone;
            ReportTabs.Add(tab);
            return tab;
        }

        #region Protected methods

        private void RemovedCallback(String k, Object v, CacheItemRemovedReason r)
        {
            if (v is WebReport)
            {
                try
                {
                    if (((WebReport)v).Report.PreparedPages != null)
                        ((WebReport)v).Report.PreparedPages.Clear();
                    ((WebReport)v).Report.Dispose();
                    ((WebReport)v).Report = null;
                }
                catch
                {
                    // nothing
                }
            }
        }

        private Control FindControlRecursive(Control root, string id)
        {
          if (root.ID == id)
            return root;

          foreach (Control ctl in root.Controls)
          {
            Control foundCtl = FindControlRecursive(ctl, id);
            if (foundCtl != null)
              return foundCtl;
          }

          return null;
        }
        
        internal void RegisterData(Report report)
        {
            if (Page != null)
            {
                string[] dataSources = ReportDataSources.Split(new char[] { ';' });
                foreach (string dataSource in dataSources)
                {
                    IDataSource ds = FindControlRecursive(Page, dataSource) as IDataSource;
                    if (ds == null)
                        continue;
                    string dataName = (ds as Control).ID;
                    RegisterDataAsp(report, ds, dataName);
                }
            }
        }

        /// <summary>
        /// Registers the ASP.NET application data to use it in the report.
        /// </summary>
        /// <param name="report">The <b>Report</b> object.</param>
        /// <param name="data">The application data.</param>
        /// <param name="name">The name of the data.</param>
        public void RegisterDataAsp(Report report, IDataSource data, string name)
        {
          FAspDataName = name;
          FReport = report;
          DataSourceView view = data.GetView("");
          if (view != null)
            view.Select(DataSourceSelectArguments.Empty, new DataSourceViewSelectCallback(RegisterDataAsp));
        }

        private string FAspDataName;
        private Report FReport;
        private void RegisterDataAsp(IEnumerable data)
        {
          if (data != null)
            RegisterDataAsp(FReport, data, FAspDataName);
        }

        /// <summary>
        /// Registers the ASP.NET application data to use it in the report.
        /// </summary>
        /// <param name="report">The <b>Report</b> object.</param>
        /// <param name="data">The application data.</param>
        /// <param name="name">The name of the data.</param>
        public void RegisterDataAsp(Report report, IEnumerable data, string name)
        {
          DataComponentBase existingDs = report.Dictionary.FindDataComponent(name);
          if (existingDs is ViewDataSource && data is DataView)
          {
            // compatibility with old FR versions (earlier than 1.1.45)
            report.Dictionary.RegisterDataView(data as DataView, name, true);
          }
          else
          {
            // in a new versions, always register the business object
            report.Dictionary.RegisterBusinessObject(data, name, 1, true);
          }
        }

        /// <inheritdoc/>
        protected override void OnLoad(EventArgs e)        
        {
            this.EnsureChildControls();
            base.OnLoad(e);
        }

        /// <inheritdoc/>
        protected override void OnUnload(EventArgs e)
        {
            //PutStorage(WebUtils.REPORT, ReportGuid, this);
            if (WebFarmMode)
                WebReportCache.CleanStorage(FileStoragePath, FileStorageTimeout, FileStorageCleanup);

            base.OnUnload(e);
        }

        private void InitTabs()
        {
            if (tabs == null)
                tabs = new List<ReportTab>();
            if (tabs.Count == 0)
            {
                ReportTab item = new ReportTab();
                item.Properties.ControlID = ReportGuid;
                item.Properties.HandlerURL = WebUtils.GetBasePath(this.Context) + WebUtils.HandlerFileName;
                tabs.Add(item);
                CurrentReportIndex = 0;
            }
        }

        private void InitReport()
        {
            if (!DesignMode)
                Config.WebMode = true;

            FileStoragePath = WebReportCache.GetStoragePath(HttpContext.Current);
            WebFarmMode = !String.IsNullOrEmpty(FileStoragePath);

            if (WebFarmMode)
            {
                FileStorageTimeout = WebReportCache.GetStorageTimeout();
                FileStorageCleanup = WebReportCache.GetStorageCleanup();
            }
             
            Prop.ControlID = ReportGuid;

            if (!fPreviewMode)
            {
                // load report
                WebReport oldweb = (WebReport)GetStorage(WebUtils.REPORT, ReportGuid);
                if (oldweb != null)
                {
                    Report = oldweb.Report;
                    WebReportProperties storedProp = oldweb.Prop;
                    if (storedProp != null)
                        Prop = storedProp;
                }
            }
            else
            {
                this.Width = Unit.Percentage(100);
                this.Height = Unit.Percentage(100);
                this.Toolbar.EnableFit = true;
                this.Toolbar.Width = "100%";
                this.Toolbar.Height = "100%";
            }

            if (!String.IsNullOrEmpty(LocalizationFile))
            {
                string fileName = LocalizationFile;
                if (!WebUtils.IsAbsolutePhysicalPath(fileName))
                    fileName = this.Context.Request.MapPath(fileName, base.AppRelativeTemplateSourceDirectory, true);
                Res.LoadLocale(fileName);
            }
            if (ShowAfterPrepare)
                ControlVisible = false;
        }

        /// <inheritdoc/>
        protected override void OnInit(EventArgs e)
        {
            if (HttpContext.Current != null)
                InitReport();
            base.OnInit(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
//        protected override void OnPreRender(EventArgs e)
//        {
//            base.OnPreRender(e);
//        }

        /// <inheritdoc/>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (HttpContext.Current != null && !InlineRegistration && Page != null)
            {
                Toolbar.RegisterGlobals(Prop.ControlID, Page.ClientScript, this.GetType(), Prop.ExternalJquery);
                if (!Page.ClientScript.IsClientScriptIncludeRegistered("fr_util"))
                    this.InlineRegistration = true;
            }
            
            // check for design time
            if (HttpContext.Current == null)            
                RenderDesignModeNavigatorControls(writer);
            else
            {  // Run-time
                Prop.HandlerURL = WebUtils.GetBasePath(this.Context) + WebUtils.HandlerFileName;
                if (Config.FullTrust)
                {
                    WebUtils.CheckHandlersRuntime();
                }

                StringBuilder container = new StringBuilder();

                if (Page == null || PreviewMode) // Razor
                {
                    InitReport();
                }

                if (ControlVisible)
                {
                    if (InlineRegistration)
                    {
                        container.AppendLine(Toolbar.GetInlineRegistration(Prop.ExternalJquery));
                    }

                    container.AppendLine(Toolbar.GetHtmlProgress(Prop.HandlerURL, Prop.ControlID, !DesignReport));

                    writer.WriteLine(container.ToString());
                    base.RenderContents(writer);

                    // save all objects
                    PutStorage(WebUtils.REPORT, ReportGuid, this);
                }
            }
        }

        #endregion

        #region DotNet 4 specific
#if DOTNET_4
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HtmlString GetHtml()
        {
            StringBuilder sb = new StringBuilder();            
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(sb, System.Globalization.CultureInfo.InvariantCulture));
            this.RenderControl(writer);
            return new HtmlString(Toolbar.GetCss() + sb.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HtmlString Scripts()
        {
            return WebReportGlobals.Scripts();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HtmlString Styles()
        {
            return WebReportGlobals.Styles();
        }
#endif
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public WebReport SetSize(Unit width, Unit height)
        {
            this.Width = width;
            this.Height = height;            
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <returns></returns>
        public WebReport SetStartEvent(EventHandler start)
        {
            StartReport += start;
            return this;
        }

        #region Register Data

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public WebReport RegisterData(DataSet data, string name)
        {
            this.Report.Dictionary.RegisterData(data, name, true);            
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public WebReport RegisterData(DataRelation data, string name)
        {
            this.Report.Dictionary.RegisterData(data, name, true);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public WebReport RegisterData(DataSet data)
        {
            this.Report.Dictionary.RegisterData(data, "Data", true);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public WebReport RegisterData(DataTable data, string name)
        {
            this.Report.Dictionary.RegisterData(data, name, true);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public WebReport RegisterData(DataView data, string name)
        {
            this.Report.Dictionary.RegisterData(data, name, true);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public WebReport RegisterData(IEnumerable data, string name)
        {
            this.Report.Dictionary.RegisterData(data, name, true);
            return this;
        }

        #endregion 

        /// <summary>
        /// Initializes a new instance of the <see cref="WebReport"/> class.
        /// </summary>
        public WebReport()
        {
            this.Width = Unit.Pixel(550);
            this.Height = Unit.Pixel(500);
            this.ForeColor = Color.Black;
            this.BackColor = Color.White;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class WebReportGlobals
    {
#if DOTNET_4
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static HtmlString Scripts()
        {
            return new HtmlString(ScriptsAsString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static HtmlString ScriptsWOjQuery()
        {
            return new HtmlString(ScriptsWOjQueryAsString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static HtmlString Styles()
        {
            return new HtmlString(StylesAsString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static HtmlString StylesWOjQuery()
        {
            // reserved for future
            return new HtmlString(String.Empty);
        }
#endif
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string ScriptsAsString()
        {
            StringBuilder raw = new StringBuilder();
            raw.Append("<script src=\"").Append(GetResourceTemplateUrl("fr_util.js")).AppendLine("\" type=\"text/javascript\"></script>");
            raw.Append("<script src=\"").Append(GetResourceJqueryUrl("jquery.min.js")).AppendLine("\" type=\"text/javascript\"></script>");
            raw.Append("<script src=\"").Append(GetResourceJqueryUrl("jquery-ui.custom.min.js")).AppendLine("\" type=\"text/javascript\"></script>");
            return raw.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string ScriptsWOjQueryAsString()
        {
            StringBuilder raw = new StringBuilder();
            raw.Append("<script src=\"").Append(GetResourceTemplateUrl("fr_util.js")).AppendLine("\" type=\"text/javascript\"></script>");
            return raw.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string StylesAsString()
        {
            StringBuilder raw = new StringBuilder();
            raw.Append("<link rel=\"stylesheet\" href=\"").Append(GetResourceJqueryUrl("jquery-ui.min.css")).AppendLine("\">");
            return raw.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string StylesWOjQueryAsString()
        {
            // reserved for future
            return String.Empty;
        }

        private static string GetResourceJqueryUrl(string resName)
        {
            return new Page().ClientScript.GetWebResourceUrl(typeof(WebReport), string.Format("FastReport.Web.Resources.jquery.{0}", resName));
        }

        private static string GetResourceTemplateUrl(string resName)
        {
            return new Page().ClientScript.GetWebResourceUrl(typeof(WebReport), string.Format("FastReport.Web.Resources.Templates.{0}", resName));
        }

    }
/*  
    /// <summary>
    /// 
    /// </summary>
    public static class FastReportWebHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static WebReport FastReport(this HtmlHelper html)
        {
            WebReport report = new WebReport();            
            return report;
        }
    }    
 */
}
