﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ErcTxtConvert
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = System.Environment.CurrentDirectory;
            
            List<FileInfo> fileList = new List<FileInfo>();
            FileHelper.GetDirectoryTree(new DirectoryInfo(path), fileList);

            foreach (FileInfo file in fileList)
                if (file.Name.IndexOf("_new") == -1)
                        ConvertFile(file);

            Console.WriteLine(path);
           // Console.ReadKey();
        }

        public static void ConvertFile(FileInfo file)
        {
            string[] lines = System.IO.File.ReadAllLines(file.Name, Encoding.Default);
            //000000007253;МУРМАНСК,СОФЬИ ПЕРОВСКОЙ УЛ,0000000072,53;000000007253;27.06;;;;100093:МУРМАНСК,АСКОЛЬДОВЦЕВ, Д.7 КВ.109:ООО "УК "НЕДВИЖИМОСТЬ-СЕРВИС":000903122013000000007253::СЧЕТЧИК Х/ВОДЫ:4.00:СЧЕТЧИК Г/ВОДЫ:5.00::0::0;20/01/2014;-27.06;2306648568
            string fc = string.Empty;
            foreach (string line in lines)
            {
                if (!line.StartsWith("#"))
                {
                    string[] s = line.Split(new char[] { ';', ':' }, StringSplitOptions.None);
                    //000300701235;МУРМАНСК,СОФЬИ ПЕРОВСКОЙ УЛ,0003007012,35;000300701235;64.28;;;;100093:МУРМАНСК,АЛЕКСАНДРА НЕВСКОГО, Д.69/51, КВ.35:ООО "МУРМАНЖИЛСЕРВИС":021503122013000300701235::СЧЕТЧИК Х/ВОДЫ:271.00:СЧЕТЧИК Г/ВОДЫ:103.00::0::0;20/01/2014;-64.28;2306647987
                    
                    fc += s[0] + ";" + s[1] + ";" + s[0] + ";" + s[3] + ";;;;000000:" + s[1] + ":" + "UK:" + s[8] + ":" +
                        (s[11].Trim() == "" ? "::":":СЧЕТЧИК Х/ВОДЫ:") + s[11] +
                        (s[12].Trim() == "" ? "::" : ":СЧЕТЧИК Г/ВОДЫ:") + s[12] +
                        (s[13].Trim() == "" ? "::" : ":ГАЗ:") + s[13] +
                        (s[14].Trim() == "" ? "::" : ":ЭЛЭН:") + s[14] +
                        ";" + s[16].Replace(".", "/") + ";-" + s[3] + ";" + s[15] + "\n";
                    
                }
            }
            FileHelper.WriteTextFile(file.Name.Replace(".txt", "_new.txt"), fc);
        }
    }
}
