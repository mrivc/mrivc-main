﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helper.Data
{
    public class DModule
    {
        SqlConnection connection1;
        public SqlConnection Connection1
        {
            get
            {
                return connection1;
            }
            set
            {
                connection1 = value;
                connection1.Open();
            }
        }

        void CheckConnection(SqlConnection connection)
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();
        }

        public void ExecuteNonQueryCommand1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;

            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();
        }

        public void ExecuteNonQueryCommand1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;


            for (int i = 0; i < prms.GetLength(0); i++)
            {
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }

            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();

            sqlCommand.Dispose();
        }


        public void ExecuteNonQueryStoredProcedure1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }
            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();

            sqlCommand.Dispose();
        }

        public void ExecuteNonQueryStoredProcedure1(string sql, object[,] prms, int timeout)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = timeout;

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }
            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();

            sqlCommand.Dispose();
        }

        public void ExecuteNonQueryStoredProcedure1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;
            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();

            sqlCommand.Dispose();
        }

        public object ExecuteScalarStoredProcedure1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }
            CheckConnection(sqlCommand.Connection);

            return sqlCommand.ExecuteScalar();

            sqlCommand.Dispose();
        }

        public object ExecuteScalarStoredProcedure1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            CheckConnection(sqlCommand.Connection);

            return sqlCommand.ExecuteScalar();

            sqlCommand.Dispose();
        }


        public object ExecuteScalarQueryCommand1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;
            CheckConnection(sqlCommand.Connection);

            object res = sqlCommand.ExecuteScalar();

            sqlCommand.Dispose();

            return res;

        }

        public object ExecuteScalarQueryCommand1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            CheckConnection(sqlCommand.Connection);

            object res = sqlCommand.ExecuteScalar();

            sqlCommand.Dispose();

            return res;
        }


        public DataTable GetDataTableByQueryCommand1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);


            res.Load(sqlCommand.ExecuteReader());

            sqlCommand.Dispose();

            return res;
        }

        public DataTable GetDataTableByStoredProcedure1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);

            var reader = sqlCommand.ExecuteReader();

            sqlCommand.Dispose();

            res.Load(reader);
            reader.Close();
            return res;
        }

        public DataTable GetDataTableByStoredProcedure1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;
            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);

            var reader = sqlCommand.ExecuteReader();

            sqlCommand.Dispose();

            res.Load(reader);
            reader.Close();
            return res;
        }

        public DataSet GetDataSetByStoredProcedure1(string sql, object[,] prms, params string[] tableNames)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var tables = new DataTable[tableNames.Count()];
            var res = new DataSet();

            for (int i = 0; i < tableNames.Count(); i++)
            {
                tables[i] = new DataTable(tableNames[i]);
            }
            CheckConnection(sqlCommand.Connection);

            res.Tables.AddRange(tables);
            res.Load(sqlCommand.ExecuteReader(), LoadOption.OverwriteChanges, tables);

            sqlCommand.Dispose();

            return res;
        }

        public DataSet GetDataSetByQueryCommand1(string sql, object[,] prms, params string[] tableNames)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var tables = new DataTable[tableNames.Count()];
            var res = new DataSet();

            for (int i = 0; i < tableNames.Count(); i++)
            {
                tables[i] = new DataTable(tableNames[i]);
            }
            CheckConnection(sqlCommand.Connection);

            res.Tables.AddRange(tables);
            res.Load(sqlCommand.ExecuteReader(), LoadOption.OverwriteChanges, tables);

            sqlCommand.Dispose();

            return res;
        }

        public DataSet GetDataSetByQueryCommand1(string sql, params string[] tableNames)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);

            var tables = new DataTable[tableNames.Count()];
            var res = new DataSet();

            for (int i = 0; i < tableNames.Count(); i++)
            {
                tables[i] = new DataTable(tableNames[i]);
            }
            CheckConnection(sqlCommand.Connection);

            res.Tables.AddRange(tables);
            res.Load(sqlCommand.ExecuteReader(), LoadOption.OverwriteChanges, tables);

            sqlCommand.Dispose();

            return res;
        }

        public DataTable GetDataTableByQueryCommand1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);

            res.Load(sqlCommand.ExecuteReader());

            sqlCommand.Dispose();

            return res;
        }


        public void BulkCopy(string destinationTableName, DataTable sourceTable, string[,] columnMappings)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(Connection1))
            {
                bulkCopy.DestinationTableName = destinationTableName;

                for (int i = 0; i < columnMappings.GetLength(0); i++)
                    bulkCopy.ColumnMappings.Add(columnMappings[i, 0].ToString(), columnMappings[i, 1].ToString());

                bulkCopy.WriteToServer(sourceTable);
            }
        }

        public void BulkCopy(string destinationTableName, DataTable sourceTable)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(Connection1))
            {
                bulkCopy.DestinationTableName = destinationTableName;
                bulkCopy.WriteToServer(sourceTable);
            }
        }

        public void TruncateTable(params string[] tableNames)
        {
            foreach (var tableName in tableNames)
            {
                ExecuteNonQueryCommand1($"Truncate table {tableName}");
            }
        }

        public void CopyDataTableToTempDB(DataTable table)
        {
            var sql = "";
            var tempTable = $"#{table.TableName}";
            sql += $"if object_id('tempdb..{tempTable}') is not null drop table {tempTable}";
            sql += Environment.NewLine;
            sql += $"create table {tempTable} (";
            sql += Environment.NewLine;
            foreach (DataColumn column in table.Columns)
            {
                sql += $"[{column.ColumnName}] varchar(1000),";
                sql += Environment.NewLine;
            }
            sql.TrimEnd(',');
            sql += Environment.NewLine;
            sql += ")";
            sql += Environment.NewLine;

            DAccess.DataModule.ExecuteNonQueryCommand1(sql);
            DAccess.DataModule.BulkCopy($"#{table.TableName}", table);
        }

        public void CopyDataSetToTempDB(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                CopyDataTableToTempDB(dt);
            }
        }

        public string GisCommandReport(string senderID, DateTime date, bool succesful, DateTime endDate)
        {
            var dt = GetDataTableByStoredProcedure1("GIS.PrepareCommandReport", new object[,] { { "senderID", senderID }, { "date", date }, { "succesful", succesful }, { "order", 1 }, { "endDate", endDate } });
            string text = "";

            if (dt.Rows.Count != 0)
            {
                text += @"ПОЖАЛУЙСТА ВНИМАТЕЛЬНО ПРОЧТИТЕ ЭТОТ ФАЙЛ ЦЕЛИКОМ. СПАСИБО

Этот отчет(файл) состоит из двух частей:

1.Как исправить ошибки ГИС ЖКХ
2.На каком доме вышли ошибки

Просьба при возникновении вопросов писать письмо на электронный почтовый ящик gis@mrivc.ru

----------------------------------------
----1.КАК ИСПРАВИТЬ ОШИБКИ ГИС ЖКХ-----
----------------------------------------

";

                text += GisErrorDescriptionReport(senderID, date, endDate);

                text += @"----------------------------------------
----2. НА КАКОМ ДОМЕ ВЫШЛИ ОШИБКИ-----
----------------------------------------

";
            }

            foreach (DataRow row in dt.Rows)
            {


                text += row["house_address"] + " | " + row["command"] + Environment.NewLine;

                if (row["errormessage"] == DBNull.Value)
                {
                    text += "Успешно";
                }
                else
                {
                    text += "Код ошибки: " + row["errorcode"] + " Ошибка: " + row["errormessage"];
                }

                text += Environment.NewLine + Environment.NewLine;
            }
            return text;
        }

        public string GisErrorDescriptionReport(string senderID, DateTime date, DateTime endDate)
        {
            var dt = GetDataTableByStoredProcedure1("GIS.PrepareCommandReport", new object[,] { { "senderID", senderID }, { "date", date }, { "succesful", false }, { "order", 2 }, { "endDate", endDate } });
            string text = "";
            foreach (DataRow row in dt.Rows)
            {
                text += "Ошибка: " + row["errormessage"] + Environment.NewLine + "Код ошибки: " + row["errorcode"] +  Environment.NewLine + "КАК ИСПРАВИТЬ ОШИБКУ:" + Environment.NewLine + row["description"];
                text += Environment.NewLine + Environment.NewLine;
            }
            return text;
        }
    }
}
