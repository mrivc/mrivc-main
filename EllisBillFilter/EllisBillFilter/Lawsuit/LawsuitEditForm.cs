﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

using Microsoft.VisualBasic;
using ClosedXML.Excel;

namespace EllisBillFilter.Lawsuit
{
    using DS;

    public partial class LawsuitEditForm : Form
    {
        int idAccount;
        int period;

        int idLawsuit;

        public LawsuitEditForm(int idAccount, int period, int parIdLawsuit = -1)
        {
            InitializeComponent();

            this.idAccount = idAccount;
            this.period = period;

            idLawsuit = -1;

            try
            {
                init();
                SetControls.SetCBoxSelectedValue(caseNumberComboBox, parIdLawsuit > 0 ?
                    parIdLawsuit.ToString() : idLawsuit.ToString());
            }
            catch (NoLawsuitsException ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void init()
        {
            Text += $" (ЛС: {DAccess.DataModule.GetAccount(idAccount)})";

            ContextMenuStrip cMenuStrip = new ContextMenuStrip();

            ToolStripMenuItem addFullname = new ToolStripMenuItem("Добавить");
            ToolStripMenuItem updateFullname = new ToolStripMenuItem("Редактировать");
            ToolStripMenuItem removeFullname = new ToolStripMenuItem("Удалить");

            cMenuStrip.Items.AddRange(new[] { addFullname, updateFullname, removeFullname });

            accCheckedListBox.ContextMenuStrip = cMenuStrip;

            addFullname.Click += addFullname_Click;
            updateFullname.Click += updateFullname_Click;
            removeFullname.Click += removeFullname_Click;

            summNumUpDown.Minimum = decimal.MinValue;
            summFineNumUpDown.Minimum = decimal.MinValue;
            legalCostsNumUpDown.Minimum = decimal.MinValue;

            summNumUpDown.Maximum = decimal.MaxValue;
            summFineNumUpDown.Maximum = decimal.MaxValue;
            legalCostsNumUpDown.Maximum = decimal.MaxValue;

            summNumUpDown.Controls.RemoveAt(0);
            summFineNumUpDown.Controls.RemoveAt(0);
            legalCostsNumUpDown.Controls.RemoveAt(0);

            Utils.EnableControlToggle(new List<object> { dCompanyComboBox, providerCheckedListBox }, false);

            DAccess.DataModule.FillCaseTable(mDataSet.LawsuitCase, idAccount);

            if (mDataSet.LawsuitCase.Rows.Count == 0) throw new NoLawsuitsException("На этом ЛС нет исков!");

            DAccess.DataModule.FillCourtTable(mDataSet.Court);
            DAccess.DataModule.FillLawsuitStadyTable(mDataSet.LawsuitStady);

            DAccess.DataModule.FillLawsuitInnerIndexTable(mDataSet.LawsuitInnerIndex);
            mDataSet.LawsuitInnerIndex.AddLawsuitInnerIndexRow("", "Пусто");

            DAccess.DataModule.FillFullNameTable(mDataSet.FullName, idAccount, period);

            caseNumberComboBox.DataSource = mDataSet.LawsuitCase;
            caseNumberComboBox.ValueMember = "ID_Lawsuit";
            caseNumberComboBox.DisplayMember = "case_number";

            dCompanyComboBox.DataSource = mDataSet.Company;
            dCompanyComboBox.ValueMember = "ID_Company";
            dCompanyComboBox.DisplayMember = "Name";

            providerCheckedListBox.DataSource = mDataSet.Provider;
            providerCheckedListBox.ValueMember = "ID_Provider_Group";
            providerCheckedListBox.DisplayMember = "Provider_Group";

            courtComboBox.DataSource = mDataSet.Court;
            courtComboBox.ValueMember = "ID_Court";
            courtComboBox.DisplayMember = "Name";

            stadyComboBox.DataSource = mDataSet.LawsuitStady;
            stadyComboBox.ValueMember = "id_stady";
            stadyComboBox.DisplayMember = "stady";

            innerIndexComboBox.DataSource = mDataSet.LawsuitInnerIndex;
            innerIndexComboBox.ValueMember = "id_inner_index";
            innerIndexComboBox.DisplayMember = "inner_index";

            accCheckedListBox.DataSource = mDataSet.FullName;
            accCheckedListBox.ValueMember = "Owner_Name";
            accCheckedListBox.DisplayMember = "Owner_Name";

            updateForm();
        }

        void updateForm()
        {
            mDataSet.Lawsuit.Clear();

            if (caseNumberComboBox.SelectedValue is DataRowView) return;

            string caseNumber = caseNumberComboBox.SelectedValue.ToString();

            if (caseNumber == "") return;

            mDataSet.FullName.Clear();
            DAccess.DataModule.FillFullNameTable(mDataSet.FullName, idAccount, period);

            DAccess.DataModule.FillLawsuitTable(mDataSet.Lawsuit,
                int.Parse(caseNumber));

            if (mDataSet.Lawsuit.Rows.Count == 0) return;

            idLawsuit = int.Parse(caseNumber);

            receivingOrderTextBox.Text = mDataSet.Lawsuit[0]["list_number"].ToString();

            string[] fullnames = Utils.DeParseString(mDataSet.Lawsuit[0]["FIO"].ToString(), ',');
            foreach (string name in fullnames)
            {
                int rowIndex = Utils.RowsToStringsList(mDataSet.FullName.Rows, "Owner_Name").IndexOf(name); ;

                if (rowIndex < 0) mDataSet.FullName.AddFullNameRow(name);
            }

            SetControls.SetCheckedListBoxValues(accCheckedListBox, fullnames);

            SetControls.SetDatePickerValue(dateDebtBeginPicker, mDataSet.Lawsuit[0]["Date_Debt_Begin"].ToString());
            SetControls.SetDatePickerValue(dateDebtEndPicker, mDataSet.Lawsuit[0]["Date_Debt_End"].ToString());

            SetControls.SetCBoxSelectedValue(dCompanyComboBox, mDataSet.Lawsuit[0]["ID_Derive_Company"].ToString());

            string[] providers = Utils.DeParseString(mDataSet.Lawsuit[0]["provider_group"].ToString(), ',');

            if (providers.Length > 0)
            {
                SetControls.SetCheckedListBoxValues(providerCheckedListBox, providers);
            }

            SetControls.SetDatePickerValue(dateProceedingsBeginPicker,
                mDataSet.Lawsuit[0]["Date_Proceedings_Begin"].ToString());
            SetControls.SetDatePickerValue(dateProceedingsEndPicker,
                mDataSet.Lawsuit[0]["Date_Proceedings_End"].ToString());

            SetControls.SetCBoxSelectedValue(courtComboBox, mDataSet.Lawsuit[0]["ID_Court"].ToString());

            summNumUpDown.Value = decimal.Parse(mDataSet.Lawsuit[0]["Sum"].ToString());

            commTextBox.Text = mDataSet.Lawsuit[0]["contract_number"].ToString();

            string ratio = mDataSet.Lawsuit[0]["ratio"].ToString() == "" ?
                "1/1" : mDataSet.Lawsuit[0]["ratio"].ToString();

            decimal numerator = decimal.Parse(ratio.Split('/')[0]);
            decimal denominator = decimal.Parse(ratio.Split('/')[1]);

            numeratorNumUpDown.Value = numerator;
            denominatorNumUpDown.Value = denominator;

            summFineNumUpDown.Value = decimal.Parse(mDataSet.Lawsuit[0]["Sum_Fine"].ToString());
            legalCostsNumUpDown.Value = decimal.Parse(mDataSet.Lawsuit[0]["Legal_Costs"].ToString());

            SetControls.SetCBoxSelectedValue(stadyComboBox, mDataSet.Lawsuit[0]["id_stady"].ToString());
            SetControls.SetCBoxSelectedValue(innerIndexComboBox, mDataSet.Lawsuit[0]["id_inner_index"].ToString());
        }

        private void validationMain()
        {
            Validation.Validate(accCheckedListBox, "Отметьте хотя бы одно имя!");
            Validation.Validate(dateDebtBeginTextBox, "Введите дату начала периода взыскания!");
            Validation.Validate(dateDebtEndTextBox, "Введите дату окончания периода взыскания!");
        }

        private void validationCalc()
        {
            Validation.Validate(providerCheckedListBox, "Отметьте хотя бы одного поставщика!");
            Validation.Validate(numeratorNumUpDown, denominatorNumUpDown,
                "В доле должна быть либо правильная дробь, либо целое число!");
        }

        private void ifDatesCorrect()
        {
            DateTime outDate = new DateTime();

            mDataSet.Company.Clear();

            Utils.EnableControlToggle(dCompanyComboBox, false);

            if (DateTime.TryParse(dateDebtBeginTextBox.Text, out outDate) &&
                DateTime.TryParse(dateDebtEndTextBox.Text, out outDate) &&
                dateDebtBeginTextBox.Text.Length == 10 &&
                dateDebtEndTextBox.Text.Length == 10)
            {
                int beginPeriod = Utils.GetPeriodByDate(dateDebtBeginTextBox.Text);
                int endPeriod = Utils.GetPeriodByDate(dateDebtEndTextBox.Text);

                DAccess.DataModule.FillCompanyTable(mDataSet.Company, beginPeriod, endPeriod, idAccount);

                if (mDataSet.Company.Count <= 0) return;

                Utils.EnableControlToggle(dCompanyComboBox, true);

                ifCompanyCorrect();
            }
        }

        private void ifCompanyCorrect()
        {
            mDataSet.Provider.Clear();

            Utils.EnableControlToggle(providerCheckedListBox, false);

            if (dCompanyComboBox.SelectedValue.ToString() != "")
            {
                int beginPeriod = Utils.GetPeriodByDate(dateDebtBeginTextBox.Text);
                int endPeriod = Utils.GetPeriodByDate(dateDebtEndTextBox.Text);
                int idCompany = int.Parse(dCompanyComboBox.SelectedValue.ToString());

                DAccess.DataModule.FillProviderTable(mDataSet.Provider, beginPeriod, endPeriod, idAccount, idCompany);

                if (mDataSet.Provider.Count <= 0) return;

                Utils.EnableControlToggle(providerCheckedListBox, true);

                for (int i = 0; i < providerCheckedListBox.Items.Count; i++)
                {
                    providerCheckedListBox.SetItemChecked(i, true);
                }
            }
        }

        void success(string action)
        {
            DataTable temp = mDataSet.LawsuitCase.Copy();

            int tempId = idLawsuit;

            caseNumberComboBox.DataSource = temp;

            mDataSet.LawsuitCase.Clear();
            DAccess.DataModule.FillCaseTable(mDataSet.LawsuitCase, idAccount);

            caseNumberComboBox.DataSource = mDataSet.LawsuitCase;

            MessageBox.Show($"Данные успешно {action}!");

            if (mDataSet.LawsuitCase.Rows.Count == 0) throw new NoLawsuitsException("На этом ЛС нет исков!");

            SetControls.SetCBoxSelectedValue(caseNumberComboBox, tempId.ToString());

            updateForm();
        }

        private void addFullname_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Добавление нового поля с ФИО", "Добавить ФИО");

            if (input == string.Empty) return;

            mDataSet.FullName.AddFullNameRow(input);

            string[] fullnames = Utils.DeParseString(mDataSet.Lawsuit[0]["FIO"].ToString(), ',');

            SetControls.SetCheckedListBoxValues(accCheckedListBox, fullnames);
        }

        private void updateFullname_Click(object sender, EventArgs e)
        {
            if (accCheckedListBox.SelectedItems.Count == 0) return;

            DataTable dataTableFullName = mDataSet.FullName;

            int index = accCheckedListBox.SelectedIndex;
            string value = accCheckedListBox.SelectedValue.ToString();

            string input = Interaction.InputBox("Редактирование поля с ФИО", "Редактирование ФИО", value);

            if (input == string.Empty) return;

            dataTableFullName.Rows[index]["Owner_Name"] = input;
        }

        private void removeFullname_Click(object sender, EventArgs e)
        {
            if (accCheckedListBox.SelectedItems.Count == 0) return;

            DataTable dataTableFullName = mDataSet.FullName;

            int index = accCheckedListBox.SelectedIndex;
            string value = accCheckedListBox.SelectedValue.ToString();

            dataTableFullName.Rows.Remove(dataTableFullName.Rows[index]);

            string[] fullnames = Utils.DeParseString(mDataSet.Lawsuit[0]["FIO"].ToString(), ',');

            SetControls.SetCheckedListBoxValues(accCheckedListBox, fullnames);
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            try
            {
                validationMain();

                int idDeriveCompany = int.Parse(dCompanyComboBox.SelectedValue.ToString());
                string providers = Utils.JoinCheckedListBox(", ", providerCheckedListBox, true);

                string dateDebtBegin = dateDebtBeginTextBox.Text;
                string dateDebtEnd = dateDebtEndTextBox.Text;

                object dateProceedingsBegin = dateProceedingsBeginPicker.Checked ?
                    (object)dateProceedingsBeginPicker.Value : null;
                object dateProceedingsEnd = dateProceedingsEndPicker.Checked ?
                    (object)dateProceedingsEndPicker.Value : null;

                int idCourt = int.Parse(courtComboBox.SelectedValue.ToString());

                decimal summ = summNumUpDown.Value;
                decimal summFine = summFineNumUpDown.Value;
                decimal legalCosts = legalCostsNumUpDown.Value;

                string comment = commTextBox.Text;
                string fullName = Utils.JoinCheckedListBox(", ", accCheckedListBox, true);

                int idStady = int.Parse(stadyComboBox.SelectedValue.ToString());

                string idInnerIndex =
                    innerIndexComboBox.SelectedValue.ToString() == "" ?
                    "NULL" : innerIndexComboBox.SelectedValue.ToString();

                string caseNumber = caseNumberComboBox.Text;
                string ratio = $"{numeratorNumUpDown.Value}/{denominatorNumUpDown.Value}";
                string listNumber = receivingOrderTextBox.Text;

                DAccess.DataModule.LawsuitEdit(
                    idLawsuit,
                    idDeriveCompany,
                    dateDebtBegin,
                    dateDebtEnd,
                    dateProceedingsBegin,
                    dateProceedingsEnd,
                    idCourt,
                    summ,
                    summFine,
                    legalCosts,
                    fullName,
                    idStady,
                    idInnerIndex,
                    comment,
                    caseNumber,
                    ratio,
                    listNumber,
                    providers
                );

                success("обновлены");
            }
            catch (NoLawsuitsException ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                const string message = "Уверены что хотите удалить иск?";
                const string caption = "Удаление";

                var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Warning);

                if (result == DialogResult.No) return;

                DAccess.DataModule.LawsuitDelete(idLawsuit);

                success("удалены");
            }
            catch (NoLawsuitsException ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void caseNumberComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                updateForm();
            }
            catch (NoLawsuitsException ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void calcSumButton_Click(object sender, EventArgs e)
        {
            try
            {
                validationCalc();

                mDataSet.Debt.Clear();

                int beginPeriod = Utils.GetPeriodByDate(dateDebtBeginTextBox.Text);
                int endPeriod = Utils.GetPeriodByDate(dateDebtEndTextBox.Text);

                int idDeriveCompany = int.Parse(dCompanyComboBox.SelectedValue.ToString());

                string ratio = $"{numeratorNumUpDown.Value}/{denominatorNumUpDown.Value}";
                string providers = Utils.JoinCheckedListBox(", ", providerCheckedListBox, true);

                int beginDay = Utils.GetDayFromDate(dateDebtBeginTextBox.Text, '.');
                int endDay = Utils.GetDayFromDate(dateDebtEndTextBox.Text, '.'); ;
                int x = (dateProceedingsBeginPicker.Checked || dateProceedingsEndPicker.Checked) ? 1 : 0;

                DAccess.DataModule.FillDebtTable(mDataSet.Debt,
                    idAccount,
                    beginPeriod,
                    endPeriod,
                    idDeriveCompany,
                    ratio,
                    providers,
                    beginDay,
                    endDay,
                    x);

                if (mDataSet.Debt.Rows.Count == 0) return;

                summNumUpDown.Value = decimal.Parse(mDataSet.Debt[0]["Sum_Tariff"].ToString());
                legalCostsNumUpDown.Value = decimal.Parse(mDataSet.Debt[0]["legal_costs"].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void calcLegalCostsButton_Click(object sender, EventArgs e)
        {
            try
            {
                validationCalc();

                mDataSet.LegalCosts.Clear();

                decimal money = summNumUpDown.Value;
                int x = (dateProceedingsBeginPicker.Checked || dateProceedingsEndPicker.Checked) ? 1 : 0;

                DAccess.DataModule.FillLegalCostsTable(mDataSet.LegalCosts,
                    money,
                    x);

                if (mDataSet.LegalCosts.Rows.Count == 0) return;

                legalCostsNumUpDown.Value = decimal.Parse(mDataSet.LegalCosts[0]["legal_costs"].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dateDebtBeginPicker_ValueChanged(object sender, EventArgs e)
        {
            dateDebtBeginTextBox.Text = string.Join("", (sender as DateTimePicker).Value.ToString().Split(' ')[0].Split('.'));
        }

        private void dateDebtEndPicker_ValueChanged(object sender, EventArgs e)
        {
            dateDebtEndTextBox.Text = string.Join("", (sender as DateTimePicker).Value.ToString().Split(' ')[0].Split('.'));
        }

        private void dateDebtBeginTextBox_TextChanged(object sender, EventArgs e)
        {
            ifDatesCorrect();
        }

        private void dateDebtEndTextBox_TextChanged(object sender, EventArgs e)
        {
            ifDatesCorrect();
        }

        private void dCompanyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ifCompanyCorrect();
        }

        private void detailButton_Click(object sender, EventArgs e)
        {
            const string templateFileName = "template.xlsx";

            Process process = new Process();

            try
            {
                Validation.Validate(dateDebtBeginTextBox, "Введите дату начала периода взыскания!");
                Validation.Validate(dateDebtEndTextBox, "Введите дату окончания периода взыскания!");
                Validation.Validate(providerCheckedListBox, "Отметьте хотя бы одногго поставщика!");

                Enabled = false;

                int beginPeriod = Utils.GetPeriodByDate(dateDebtBeginTextBox.Text);
                int endPeriod = Utils.GetPeriodByDate(dateDebtEndTextBox.Text);
                int idDeriveCompany = int.Parse(dCompanyComboBox.SelectedValue.ToString());

                string providers = Utils.JoinCheckedListBox(", ", providerCheckedListBox, true);

                mDataSet.lawsuitDolgDetail.Clear();
                DAccess.DataModule.FillLawsuitDolgDetail(mDataSet.lawsuitDolgDetail,
                                                         beginPeriod,
                                                         endPeriod,
                                                         idAccount,
                                                         idDeriveCompany,
                                                         providers);

                DataTable dataTableLawsuitDolgDetail = mDataSet.lawsuitDolgDetail.Copy();
                dataTableLawsuitDolgDetail.TableName = "Детализация";

                var workBook = new XLWorkbook();

                workBook.Worksheets.Add(dataTableLawsuitDolgDetail);

                workBook.SaveAs(templateFileName);

                process.StartInfo.FileName = templateFileName;
                process.Start();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (File.Exists(templateFileName)) File.Delete(templateFileName);
                process.Close();

                Enabled = true;
            }
        }
    }
}
