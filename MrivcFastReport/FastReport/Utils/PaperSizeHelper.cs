﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;

namespace FastReport.Utils
{
    public static class PaperSizeHelper
    {
        public static void UpdatePage(ReportPage page, bool landscape, float width, float height, int rawPaperSize)
        {
            PrinterSettings printerSettings = new PrinterSettings();

            string savePrinterName = printerSettings.PrinterName;
            printerSettings.PrinterName = page.Report.PrintSettings.Printer;
            if (!printerSettings.IsValid)
                printerSettings.PrinterName = savePrinterName;

            page.Landscape = landscape;
            page.PaperWidth = width;
            page.PaperHeight = height;

            page.RawPaperSize = rawPaperSize == 0 ? 0 : printerSettings.PaperSizes[rawPaperSize].RawKind;
        }
    }
}
