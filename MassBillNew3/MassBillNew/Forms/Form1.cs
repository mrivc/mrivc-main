﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using MrivcControls;
using MrivcHelpers.Helpers;
using FastReport;
using FastReport.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;


namespace MassBillNew
{
    public partial class Form1 : Form
    {
        List<DebtCompanyAccount> debtCompanyAccountList = new List<DebtCompanyAccount>();

        public Form1()
        {
            InitializeComponent();
            this.Text = StaticConfig.Title(this.Text);
        }

        public bool CoBxSelVal { get; set; } = false;

        #region Var

        string id = ""; 
        
        string name = "";

        int people0 = 0;

        int pustMunKvart = 0;

        ArrayList AccountList = new ArrayList();
        ArrayList TagList = new ArrayList();

        string idTag = "";

        string IdCompany = "";
        
        #endregion

        private void FillTreeView()
        {
            if (radioButton1.Checked)
            {
                treeView1.Nodes.Clear();

                SqlConnection sqlConnection1;

                sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();


                cmd.CommandText =

                  " SELECT distinct id_client,name" +
                  " FROM dbo.Bill_Filter F" +
                  " INNER JOIN dbo.List_To_Table((SELECT Filter_Codes FROM Reports.Bill_Kind WHERE ID_Bill_Kind = " + comboBox1.SelectedValue + "), DEFAULT) List" +
                  " ON List.ID = F.Code" +
                  " inner join reports.client_detail cd on cd.id_bill_filter = f.id_bill_filter and " + comboBox2.SelectedValue + " between begin_period and isnull(end_period," + comboBox2.SelectedValue + ")" +
                  " inner join reports.client_detail_house cdh on cd.id_client_detail = cdh.id_client_detail " +
                  " inner join house_derive_company hdc on hdc.id_house = cdh.id_house and hdc.period = " + comboBox2.SelectedValue + " and hdc.id_company = cd.id_derive_company" +
                  " inner join vaccount_address_all aa on aa.id_house = cdh.id_house" +
                  " inner join company c on c.id_company = cd.id_client" +
                  " order by name";



                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;
                sqlConnection1.Open();

                SqlDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    TreeNode treenode = new TreeNode();
                    treenode.Tag = "id_client = " + reader["id_client"].ToString();
                    treenode.Text = reader["name"].ToString();

                    treeView1.Nodes.Add(treenode);
                    treenode.ImageIndex = 0;
                    treenode.Nodes.Add("*");
                }

                TreeNode last_tn = new TreeNode();
                last_tn.ImageIndex = 7;
                last_tn.Text = "";
                treeView1.Nodes.Add(last_tn);

                sqlConnection1.Close();
            }
            else
            {
                treeView1.Nodes.Clear();

                SqlConnection sqlConnection1;

                sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();

                cmd.CommandText =

                        " select distinct c.id_company,c.name from  house_derive_company hdc " +
                        " inner join company c on c.id_company = hdc.id_company" +
                        " where hdc.period = dbo.work_period() order by c.name";

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;
                sqlConnection1.Open();

                SqlDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    TreeNode treenode = new TreeNode();
                    treenode.Tag = "id_company = " + reader["id_company"].ToString();
                    treenode.Text = reader["name"].ToString();

                    treeView1.Nodes.Add(treenode);
                    treenode.ImageIndex = 0;
                    treenode.Nodes.Add("*");
                }

                TreeNode last_tn = new TreeNode();
                last_tn.ImageIndex = 7;
                last_tn.Text = "";
                treeView1.Nodes.Add(last_tn);

                sqlConnection1.Close(); 
            }
        }


        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if (radioButton1.Checked)
            {

                TreeNode treenode = e.Node;
                treenode.Nodes.Clear();

                if (treenode.Level == 0)
                {
                    IdCompany = "";
                }

                if (treenode.Level == 1)
                {
                    IdCompany = " and " + treenode.Tag.ToString();
                }



                SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();

                int imgIndex = 0;

                switch (treenode.Level)
                {
                    case 0:
                        id = "id_derive_company";
                        name = "name";
                        imgIndex = 0;
                        break;
                    case 1:
                        id = "id_settlement";
                        name = "Settlement";
                        imgIndex = 1;
                        break;
                    case 2:
                        id = "id_street";
                        name = "street";
                        imgIndex = 2;
                        break;
                    case 3:
                        id = "aa.id_house";
                        name = "Full_house_name";
                        imgIndex = 3;
                        break;
                    case 4:
                        id = "id_flat";
                        name = "Flat_Number";
                        imgIndex = 4;
                        break;
                    case 5:
                        id = "id_account";
                        name = "Account";
                        imgIndex = 5;
                        break;
                }


                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                string where = "";

                if (treenode.Level != 4)
                {
                    where = " where " + treenode.Tag;
                }
                else
                {
                    where = " where aa." + treenode.Tag;
                }



                cmd.CommandText =
               " SELECT distinct " + id + "," + name +
               " FROM dbo.Bill_Filter F" +
               " INNER JOIN dbo.List_To_Table((SELECT Filter_Codes FROM Reports.Bill_Kind WHERE ID_Bill_Kind = " + comboBox1.SelectedValue + "), DEFAULT) List" +
               " ON List.ID = F.Code" +
               " inner join reports.client_detail cd on cd.id_bill_filter = f.id_bill_filter and " + comboBox2.SelectedValue + " between begin_period and isnull(end_period," + comboBox2.SelectedValue + ")" +
               " inner join reports.client_detail_house cdh on cd.id_client_detail = cdh.id_client_detail " +
               " inner join house_derive_company hdc on hdc.id_house = cdh.id_house and hdc.period = " + comboBox2.SelectedValue + " and hdc.id_company = cd.id_derive_company" +
               " inner join vaccount_address_all aa on aa.id_house = cdh.id_house" +
               " inner join company c on c.id_company = cd.id_derive_company" +
               where;


                //cmd.CommandText = cmd.CommandText.Replace("aa.id_house", "id_company");

                id = id.Replace("aa.id_house", "id_house");

                if (treenode.Level == 0 | treenode.Level == 2 | treenode.Level == 5 | treenode.Level == 4)
                {
                    cmd.CommandText += " order by " + name;
                }
                if (treenode.Level == 4)
                {
                    //cmd.CommandText += " order by full_flat_name";
                    //cmd.CommandText = cmd.CommandText.Replace("distinct", " ");
                }

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;
                sqlConnection1.Open();

                SqlDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    TreeNode new_treenode = new TreeNode();
                    new_treenode.Tag = id + " = " + reader[id].ToString() + IdCompany;
                    new_treenode.Text = reader[name].ToString();
                    new_treenode.ImageIndex = imgIndex;
                    treenode.Nodes.Add(new_treenode);

                    if (treenode.Level != 5)
                    {
                        new_treenode.Nodes.Add("*");
                    }

                }

                sqlConnection1.Close();
            }
            else
            {

                TreeNode treenode = e.Node;
                treenode.Nodes.Clear();


                if (treenode.Level == 0)
                {
                    IdCompany = treenode.Tag.ToString();
                }

                SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();

                int imgIndex = 0;

                switch (treenode.Level)
                {
                    case 0:
                        id = "id_settlement";
                        name = "Settlement";
                        imgIndex = 1;
                        break;
                    case 1:
                        id = "id_street";
                        name = "street";
                        imgIndex = 2;
                        break;
                    case 2:
                        id = "id_house";
                        name = "Full_house_name";
                        imgIndex = 3;
                        break;
                    case 3:
                        id = "id_flat";
                        name = "Full_Flat_Name";
                        imgIndex = 4;
                        break;
                    case 4:
                        id = "id_account";
                        name = "Account";
                        imgIndex = 5;
                        break;
                }

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                cmd.CommandText =
                "select distinct aa." + id + "," + name + " from  house_derive_company hdc " +
                "inner join vaccount_address_all aa on aa.id_house = hdc.id_house " +
                "where hdc.period = dbo.work_period() and aa." + treenode.Tag.ToString();

                cmd.CommandText = cmd.CommandText.Replace("aa.id_company", "id_company");

                if (treenode.Level == 1 | treenode.Level == 4)
                {
                    cmd.CommandText += " order by " + name;
                }
                if (treenode.Level == 3)
                {
                    cmd.CommandText += " order by flat_number";
                    cmd.CommandText = cmd.CommandText.Replace("distinct", " ");
                }

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;
                sqlConnection1.Open();

                SqlDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    TreeNode new_treenode = new TreeNode();
                    new_treenode.Tag = id + " = " + reader[id].ToString() + " and " + IdCompany;
                    new_treenode.Text = reader[name].ToString();
                    new_treenode.ImageIndex = imgIndex;
                    treenode.Nodes.Add(new_treenode);

                    if (treenode.Level != 4)
                    {
                        new_treenode.Nodes.Add("*");
                    }

                }

                sqlConnection1.Close();
            }
        }

        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            this.bill_KindTableAdapter.Fill(this.murmanskDataSet.Bill_Kind);
            comboBox3.SelectedValue = -1;
            comboBox3.Enabled = false;
            button3.Enabled = true;
            button8.Enabled = false;

            chBxTwoDebtComp.Enabled = false;

            TagList.Clear();
            listBox1.Items.Clear();

            FillTreeView();
        }

        private void rb2_CheckedChanged(object sender, EventArgs e)
        {
            this.bill_KindTableAdapter.FillBy(this.murmanskDataSet.Bill_Kind);
            comboBox3.Enabled = true;
            button3.Enabled = false;
            button8.Enabled = true;

            chBxTwoDebtComp.Enabled = true;

            TagList.Clear();
            listBox1.Items.Clear();

            FillTreeView();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                people0 = 1;
            }
            else
            {
                people0 = 0;
            }

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {


            if (checkBox2.Checked)
            {
                pustMunKvart = 1;
            }
            else
            {
                pustMunKvart = 0;
            }
        }

        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {


            try
            {
            MultiSelectTreeview lb = ((MultiSelectTreeview)sender);
           
            foreach (TreeNode node in lb.SelectedNodes)
            {
                idTag = node.Tag.ToString();
                lb.DoDragDrop(node.FullPath, DragDropEffects.Copy);
                
            }

            }
            catch (Exception)
            {


            }
        }

        private void listBox1_DragDrop(object sender, DragEventArgs e)
        {
            TagList.Add(idTag);

            object ind = e.Data.GetData("Text");
            
            listBox1.Items.Add(ind);
        }

        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("Text"))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        //Группа с двойными квитанциями для МЭС
        string ShowDoubleKvitDialog(bool printDoubleKvit, string fileName)
        {
            if (printDoubleKvit)
            {
                fileName = "";
                if (saveFileDialog2.ShowDialog() == DialogResult.OK)
                {
                    fileName = saveFileDialog2.FileName;
                    return fileName;
                }
                else
                {
                    return null; 
                }
            }
            return null;
        }
        //Конец. Группа с двойными квитанциями для МЭС

        void PrintDoubleKvit(Report report1, Report report2, string fileName)
        {
            //if (fileName.Contains(".fpx"))
            //{
            //    fileName = Path.GetFileNameWithoutExtension(fileName);
            //}

            report1.Prepare();
            report1.SavePrepared(fileName
                //+ ".fpx"
                );

            //report1.Clear();
            report2.SetParameterValue("PrintBillsWithSubscription", true);
            report2.Prepare();
            report2.SavePrepared(fileName.Replace(".fpx", "(копия для МЭС).fpx")
                //.fpx"
                );
        }

        private void PrintKvit(bool backSide, bool saveHistory, bool printBillsWithSubscription, bool printDoubleKvit)
        {
            var fileName = "";

            report1.Dispose();
            report2.Dispose();

            report1 = new Report();
            report2 = new Report();

            if (printDoubleKvit)
                fileName = ShowDoubleKvitDialog(printDoubleKvit, fileName);

                if (radioButton1.Checked)
            {
                //try
                //{


                label1.Text = "Выполняется формирование отчета...";
                label1.Refresh();

                this.Cursor = Cursors.WaitCursor;
                SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();

                cmd.CommandText =
                cmd.CommandText =
                   " SELECT distinct id_account " +
                   " FROM dbo.Bill_Filter F" +
                   " INNER JOIN dbo.List_To_Table((SELECT Filter_Codes FROM Reports.Bill_Kind WHERE ID_Bill_Kind = " + comboBox1.SelectedValue + "), DEFAULT) List" +
                   " ON List.ID = F.Code" +
                   " inner join reports.client_detail cd on cd.id_bill_filter = f.id_bill_filter and " + comboBox2.SelectedValue + " between begin_period and isnull(end_period," + comboBox2.SelectedValue + ")" +
                   " inner join reports.client_detail_house cdh on cd.id_client_detail = cdh.id_client_detail " +
                   " inner join house_derive_company hdc on hdc.id_house = cdh.id_house and hdc.period = " + comboBox2.SelectedValue + " and hdc.id_company = cd.id_derive_company" +
                   " inner join vaccount_address_all aa on aa.id_house = cdh.id_house" +
                   " where (";

                for (int i = 0; i < TagList.Count; i++)
                {

                    if (i == (TagList.Count - 1))
                    {
                        cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") ) ";
                    }
                    else
                    {
                        cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") or ";
                    }

                }

                cmd.CommandText = cmd.CommandText.Replace("aa.id_derive_company", "id_derive_company");

                cmd.CommandText = cmd.CommandText.Replace("aa.id_client", "id_client");

                foreach (DataGridViewRow item in parameterFilter2.dataGridView2.Rows)
                {
                    string owner = "";
                    switch (item.Cells["id_owner"].Value.ToString())
                    {
                        case "1":
                            owner = "account";
                            break;
                        case "2":
                            owner = "flat";
                            break;
                        case "4":
                            owner = "house";
                            break;
                    }

                    if (item.Index == 0)
                        item.Cells["condition"].Value = "and ("; //добавить открывающую скобку и заменить условие на "и", так как при "или" запрос будет выводить все лицевые с первым парметром

                    if (item.Cells["value"].Value == "")
                    {
                        item.Cells["value"].Value = " null ";
                        if (item.Cells["operator_"].Value == "=")
                        {
                            item.Cells["operator_"].Value = "is";
                        }
                        if (item.Cells["operator_"].Value == "<>")
                        {
                            item.Cells["operator_"].Value = "is not";
                        }
                    }

                    //добавляем выбор ЛС по параметрам, расположенным в parameterFilter2.dataGridView2

                    cmd.CommandText = cmd.CommandText + String.Format(" {0} aa.id_{5} in (select id_{5} from v{5}_parameter where period = {1} and parameter = '{2}' and {6}_value {3} {4} )", item.Cells["condition"].Value, item.Cells["period"].Value, item.Cells["parameter"].Value, item.Cells["operator_"].Value, item.Cells["value"].Value, owner, item.Cells["type"].Value);
                }

                if (parameterFilter2.dataGridView2.Rows.Count != 0) //если есть параметры - закрываем скобку 
                {
                    cmd.CommandText += " ) ";
                }


                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;
                sqlConnection1.Open();

                SqlDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AccountList.Add(reader[0]);
                }

                sqlConnection1.Close();

                if (AccountList.Count == 0)
                {
                    saveHistory = false;
                }


                report1.Preview = previewControl1;

                sqlConnection1.Open();

                cmd.CommandText = "exec Reports.Bill_Binary_Convert  " + comboBox2.SelectedValue.ToString();

                byte[] module = (byte[])cmd.ExecuteScalar();

                sqlConnection1.Close();

                report1.LoadFromString(Encoding.UTF8.GetString(module));
                report1.SetParameterValue("PageA5", false);

                report2.LoadFromString(Encoding.UTF8.GetString(module));
                report2.SetParameterValue("PageA5", false);

                long idPrint = PrepareRepotData(saveHistory, backSide, printBillsWithSubscription);

                textBox3.Text = idPrint.ToString();

                var rowKind = (billKindBindingSource.Current as System.Data.DataRowView).Row as MurmanskDataSet.Bill_KindRow;
                if (rowKind.PaperSize.Contains("A5"))
                {
                    report1.SetParameterValue("PageA5", true);
                    report2.SetParameterValue("PageA5", true);
                    foreach (ReportPage p in report1.Pages)
                    {
                        if (p.Name.Contains("o_O"))
                        {
                            p.SetPaperSize(FastReport.Enums.FPaperSize.A5, true);
                        }
                    }
                }

                report1.SetParameterValue("PrintFio", true);
                report1.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);
                report1.SetParameterValue("IDPrint", idPrint);
                report1.SetParameterValue("PrintBillsWithSubscription", printBillsWithSubscription);

                report1.SetParameterValue("IS_DEBT", radioButton2.Checked);

                report2.SetParameterValue("PrintFio", true);
                report2.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);
                report2.SetParameterValue("IDPrint", idPrint);
                report2.SetParameterValue("PrintBillsWithSubscription", printBillsWithSubscription);

                report2.SetParameterValue("IS_DEBT", radioButton2.Checked);

                if (printDoubleKvit)
                {
                    PrintDoubleKvit(report1, report2, fileName);
                }
                else
                {
                    report1.Show();
                }

                sqlConnection1.Open();

                //cmd.CommandText = " exec [Reports].[Clear_Print_Data] @ID_Print=" + idPrint;
                //cmd.ExecuteNonQuery();

                sqlConnection1.Close();

                tabControl1.SelectTab(1);
                this.Cursor = Cursors.Arrow;
                label1.Text = "";

                AccountList.Clear();

                if (printDoubleKvit)
                {
                    MessageBox.Show("Отчеты сформированы");
                }



                //}
                //catch (Exception exp)
                //{

                //    MessageBox.Show(exp.ToString());
                //    this.Cursor = Cursors.Arrow;
                //    label1.Text = "";

                //    AccountList.Clear();
                //}
            } //if (radioButton1.Checked)
            else
            {
                //try
                //{


                label1.Text = "Выполняется формирование отчета...";
                label1.Refresh();

                this.Cursor = Cursors.WaitCursor;
                SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();

                sqlConnection1.Open();

                var param = "";

                foreach (DataGridViewRow item in parameterFilter2.dataGridView2.Rows)
                {
                    string owner = "";
                    switch (item.Cells["id_owner"].Value.ToString())
                    {
                        case "1":
                            owner = "account";
                            break;
                        case "2":
                            owner = "flat";
                            break;
                        case "4":
                            owner = "house";
                            break;
                    }

                    if (item.Index == 0)
                        item.Cells["condition"].Value = "and ("; //добавить открывающую скобку и заменить условие на "и", так как при "или" запрос будет выводить все лицевые с первым парметром

                    if (item.Cells["value"].Value == "")
                    {
                        item.Cells["value"].Value = " null ";
                        if (item.Cells["operator_"].Value == "=")
                        {
                            item.Cells["operator_"].Value = "is";
                        }
                        if (item.Cells["operator_"].Value == "<>")
                        {
                            item.Cells["operator_"].Value = "is not";
                        }
                    }


                    //добавляем выбор ЛС по параметрам, расположенным в parameterFilter2.dataGridView2

                    param += String.Format(" {0} aa.id_{5} in (select id_{5} from v{5}_parameter where period = {1} and parameter = '{2}' and {6}_value {3} {4} )", item.Cells["condition"].Value, item.Cells["period"].Value, item.Cells["parameter"].Value, item.Cells["operator_"].Value, item.Cells["value"].Value, owner, item.Cells["type"].Value);
                }

                if (parameterFilter2.dataGridView2.Rows.Count != 0) //если есть параметры - закрываем скобку 
                {
                    param += " ) ";
                }

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                //07-02-17
                var objs = TagList.ToArray();
                var dObjs = objs.Distinct().ToArray();

                TagList.Clear();
                TagList.AddRange(dObjs);

                cmd.CommandText =
                    "select distinct aa.id_account from  house_derive_company hdc " +
                    "inner join vaccount_address_all aa on aa.id_house = hdc.id_house " +
                     " where hdc.period = dbo.work_period() and (";

                var n = 0;

                for (int i = 0; i < TagList.Count; i++)
                {

                    //if (i == (TagList.Count - 1))
                    //{
                    //cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") ) ";
                    //}
                    //else
                    //{
                    cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") ";
                    //

                    cmd.CommandText = cmd.CommandText.Replace("id_derive_company", "id_company");
                    cmd.CommandText = cmd.CommandText.Replace("aa.id_company", "id_company");

                    if (n == 1000 || i == TagList.Count - 1)
                    {
                        n = 0;
                        cmd.CommandText += " ) ";
                        cmd.CommandText = cmd.CommandText + param;

                        SqlDataReader reader;
                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            AccountList.Add(reader[0]);
                        }

                        reader.Close();

                        cmd.CommandText =
                       "select distinct aa.id_account from  house_derive_company hdc " +
                       "inner join vaccount_address_all aa on aa.id_house = hdc.id_house " +
                        " where hdc.period = dbo.work_period() and (";
                    }
                    else
                    {
                        cmd.CommandText += " or ";
                    }

                    n++;
                }

                sqlConnection1.Close();

                report1.Preview = previewControl1;

                sqlConnection1.Open();

                cmd.CommandText = "exec Reports.Bill_Binary_Convert  " + comboBox2.SelectedValue.ToString();

                byte[] module = (byte[])cmd.ExecuteScalar();

                sqlConnection1.Close();

                report1.LoadFromString(Encoding.UTF8.GetString(module));
                report2.LoadFromString(Encoding.UTF8.GetString(module));

                long idPrint = PrepareRepotData(saveHistory, backSide, printBillsWithSubscription);

                textBox3.Text = idPrint.ToString();

                var rowKind = (billKindBindingSource.Current as System.Data.DataRowView).Row as MurmanskDataSet.Bill_KindRow;
                if (rowKind.PaperSize.Contains("A5"))
                {
                    report1.SetParameterValue("PageA5", true);
                    foreach (ReportPage p in report1.Pages)
                    {
                        if (p.Name.Contains("o_O"))
                        {
                            p.SetPaperSize(FastReport.Enums.FPaperSize.A5, true);
                        }
                    }
                }

                report1.SetParameterValue("PrintFio", true);

                report1.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);
                report1.SetParameterValue("IDPrint", idPrint);
                report1.SetParameterValue("PrintBillsWithSubscription", printBillsWithSubscription);

                report1.SetParameterValue("IS_DEBT", radioButton2.Checked);

                report1.Show();

                sqlConnection1.Open();

                cmd.CommandText = " exec [Reports].[Clear_Print_Data] @ID_Print=" + idPrint;
                cmd.ExecuteNonQuery();

                sqlConnection1.Close();

                tabControl1.SelectTab(1);
                this.Cursor = Cursors.Arrow;
                label1.Text = "";

                AccountList.Clear();
                //}
                //catch (Exception exp)
                //{

                //    MessageBox.Show(exp.ToString());
                //    this.Cursor = Cursors.Arrow;
                //    label1.Text = "";

                //    AccountList.Clear();
                //}
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
             

           

        }

        GridPrintHistory gridPrintHistory;
        private void Form1_Load_1(object sender, EventArgs e)
        {
            gridPrintHistory = new GridPrintHistory();
            MrivcControls.Data.DAccess.DataModule.SetConnection(StaticConfig.MainConnection.ConnectionString);
            gridPrintHistory.LoadData();
            gridPrintHistory.Dock = DockStyle.Fill;
            tabPage5.Controls.Add(gridPrintHistory);

            this.bill_KindTableAdapter.Fill(this.murmanskDataSet.Bill_Kind);
            this.companyTableAdapter.Fill(this.murmanskDataSet.Company);
            this.vPeriodTableAdapter.Fill(this.murmanskDataSet.VPeriod);

            label1.Text = "";
            FillTreeView();
            parameterFilter2.Init();
            checkBox1.Visible = false;
        }

        public long PrepareRepotData(bool saveHistory, bool backSide, bool printBillsForSubscribers, int idDebtCompany = -1)
        {
            string idPrint = comboBox2.SelectedValue.ToString() + StringHelper.ZeroCode(DateTime.Now.Month) + StringHelper.ZeroCode(DateTime.Now.Day) +
                StringHelper.ZeroCode(DateTime.Now.Hour) + StringHelper.ZeroCode(DateTime.Now.Minute) + StringHelper.ZeroCode(DateTime.Now.Millisecond);
            
            string company = "";

            if (((int)comboBox3.SelectedValue != -1 || customEdit1.EditValue.ToString() != "") && radioButton2.Checked)
            {
                if (chBxTwoDebtComp.Checked)
                {
                    if (idDebtCompany != -1)
                    {
                        company = idDebtCompany.ToString();
                    }
                }
                else
                {
                    company = comboBox3.SelectedValue.ToString();
                }
            }
            else
	        {
                company = "null";
	        } ;

            if (chbDebtFilterCodePeriod.Checked == true) //БДА 10-10-17 Не учитывать выбор компаний при выборе долга по коду фильтра
            {
                company = "null";
            }

            string sql = "execute [Reports].[Prepare_Bill_Report_Data] " +
                         " @ID_Print = " + idPrint +
                         ", @ID_Bill_Kind = " + comboBox1.SelectedValue.ToString() +
                         ", @Period = " + comboBox2.SelectedValue.ToString() +
                         ", @Print_Zero_Param = " + people0.ToString() +
                         ", @Print_Empty_Flat = " + pustMunKvart.ToString() +
                         ", @Id_company = " + company +
                         ", @saveprinthistory = " + saveHistory +
                         ", @PrintedBackSide = " + backSide +
                        ", @PrintBillsForSubscribers = " + printBillsForSubscribers;
	 
            string finalSql = CreateSqlFillAccounttable() + Environment.NewLine + sql;

            SqlConnection sqlConnection1 = StaticConfig.MainConnection;

            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = finalSql;
            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;
            sqlConnection1.Open();

            cmd.ExecuteNonQuery();

            sqlConnection1.Close();

            return long.Parse(idPrint);
        }

        public string CreateSqlFillAccounttable()
        {
            string sqlTableAccountCreate = "create table #__Account(ID int)";
            string sqlAddAccTemplate = "insert into #__Account(ID) values (@account)";
            string sqlAddAcc = "";


            foreach (var item in AccountList)
                sqlAddAcc += sqlAddAccTemplate.Replace("@account", item.ToString()
                    //.Split("-".ToCharArray())[0].Trim()
                    ) + ";" + Environment.NewLine;
            sqlTableAccountCreate += Environment.NewLine + sqlAddAcc;

            return sqlTableAccountCreate;
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                //if (chBxTwoDebtComp.Checked)
                //{
                //    MessageBox.Show("При выборе нескольких долговых УК данное поле только для чтения. Редактирование запрещено");
                //}
                //else
                //{
                    Point pt = new Point(e.X, e.Y);

                    int i = listBox1.IndexFromPoint(pt);

                    listBox1.Items.RemoveAt(i);
                    TagList.RemoveAt(i);
                //}
            }
            catch (Exception)
            {

            }
        }

        private void environmentSettings1_Progress(object sender, FastReport.ProgressEventArgs e)
        {
           
            label1.Text = e.Message.ToString();
            label1.Refresh();
            
        }

        private void environmentSettings1_StartProgress(object sender, EventArgs e)
        {
         
        }

        private void comboBox5_DropDown(object sender, EventArgs e)
        {
            this.vPeriod1TableAdapter.Fill(this.murmanskDataSet.VPeriod1);
        }

 

      

    
        //private void parameterFilter2.dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    parameterFilter2.dataGridView2.Rows.RemoveAt(e.RowIndex);
        //}

      

      

       

      

      
        

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection sqlconn = StaticConfig.MainConnection;

            try
            {
                
                sqlconn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlconn;
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = string.Format(" select c.name +', '+aa.district+', '+aa.street+', '+aa.full_house_name+', '+aa.full_flat_name+', '+account" +
                " from house_derive_company hdc" +
                " inner join vaccount_address_all aa on hdc.id_house = aa.id_house" +
                " inner join company c on c.id_company = hdc.id_company" +
                " where hdc.period = dbo.work_period() and  aa.account = '{0}'", textBox2.Text);

                listBox1.Items.Add(cmd.ExecuteScalar());

                cmd.CommandText = string.Format(" select id_account from account where account = '{0}'", textBox2.Text);

                sqlconn.Close();
                sqlconn.Open();

                TagList.Add(" id_account =" + cmd.ExecuteScalar());

                sqlconn.Close();
            }
            catch (Exception)
            {
                sqlconn.Close();
                
            }
            


        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar != 8 & !Char.IsDigit(e.KeyChar)) //ввод только цифр и backspace
            //    e.Handled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection sqlconn = StaticConfig.MainConnection;
            sqlconn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlconn;
            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = " select distinct c.id_company,c.name" +
            " from house_derive_company hdc" +
            " inner join company c on c.id_company = hdc.id_company" +
            " where hdc.period = dbo.work_period() " +
            " order by name";

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                listBox1.Items.Add(reader["name"]); //добавляем название всех компаний в listbox
                TagList.Add("id_company = " + reader["id_company"]); // ИД всех компаний в taglist
            }

            sqlconn.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            TagList.Clear();
        }

        private void button5_Click(object sender, EventArgs e)
        {


            try
            {
                


                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //Очищаем список адресов после загрузки фильтра
                    TagList.Clear();
                    listBox1.Items.Clear();

                    string ext = Path.GetExtension(openFileDialog1.SafeFileName);

                    if (ext == ".txt")
                    {
                        string[] lines = File.ReadAllLines(openFileDialog1.FileName);

                        SqlConnection sqlconn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);



                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = sqlconn;



                        foreach (string line in lines)
                        {
                            
                            cmd.CommandText = "select id_account,c.id_company,c.name+', '+aa.settlement+', '+aa.street+', '+aa.full_house_name+', '+aa.full_flat_name+', '+aa.account as caption from house_derive_company hdc inner join company c on hdc.id_company = c.id_company inner join vaccount_address_all aa on aa.id_house = hdc.id_house where hdc.period = dbo.work_period() and (aa.id_account = "
                                + line + " or  aa.account = '" + line + "')";

                            sqlconn.Open();

                            SqlDataReader reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                TagList.Add(string.Format("id_account = {0} and id_company = {1}", reader[0], reader[1]));
                                listBox1.Items.Add(reader[2]);

                            }

                            sqlconn.Close();
                        }


                    }
                    else
                    {
                        if (ext == ".afl")
                        {
                            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                            xmlDoc.Load(openFileDialog1.FileName);

                            

                            foreach (XmlNode item in xmlDoc)
                            {

                                foreach (XmlNode item1 in item.ChildNodes)
                                {
                                    switch (item1.Name)
                                    {
                                        case "Address":

                                            foreach (XmlNode addr in item1.ChildNodes)
                                            {
                                                string TypeID = "";
                                                switch (addr.Attributes["TypeID"].Value)
                                                {


                                                    case "5":
                                                        TypeID = "";
                                                        break;
                                                    case "4":
                                                        TypeID = string.Format("id_settlement = {0} and ", addr.Attributes["ID"].Value);
                                                        break;
                                                    case "3":
                                                        TypeID = string.Format("id_street = {0} and ", addr.Attributes["ID"].Value);
                                                        break;
                                                    case "2":
                                                        TypeID = string.Format("id_house = {0} and ", addr.Attributes["ID"].Value);
                                                        break;
                                                    case "1":
                                                        TypeID = string.Format("id_flat = {0} and ", addr.Attributes["ID"].Value);
                                                        break;
                                                    case "0":
                                                        TypeID = string.Format("id_account = {0} and ", addr.Attributes["ID"].Value);
                                                        break;


                                                }

                                                TagList.Add(string.Format("{0}id_derive_company = {1}", TypeID, addr.Attributes["DeriveCompany"].Value));
                                                listBox1.Items.Add(addr.Attributes["Caption"].Value);

                                            };

                                            break;

                                        case "Parameter":

                                            foreach (XmlNode param in item1.ChildNodes)
                                            {
                                                SqlConnection sqlconn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);

                                                sqlconn.Open();

                                                SqlCommand cmd = new SqlCommand();
                                                cmd.CommandTimeout = 0;
                                                cmd.CommandType = CommandType.Text;
                                                cmd.Connection = sqlconn;

                                                cmd.CommandText = "select period_name from vperiod where period = " + param.Attributes["Period"].Value;
                                                string per = cmd.ExecuteScalar().ToString();

                                                cmd.CommandText = string.Format("select display_name from parameter where code = '{0}'", param.Attributes["Parameter"].Value);
                                                string par = cmd.ExecuteScalar().ToString();

                                                string operat = "";
                                                switch (param.Attributes["Operation"].Value)
                                                {
                                                    case "=":
                                                        operat = "равно";
                                                        break;
                                                    case "<>":
                                                        operat = "не равно";
                                                        break;
                                                    case "<":
                                                        operat = "меньше";
                                                        break;
                                                    case "<=":
                                                        operat = "меньше или равно";
                                                        break;
                                                    case ">":
                                                        operat = "больше";
                                                        break;
                                                    case ">=":
                                                        operat = "больше или равно";
                                                        break;
                                                }

                                                cmd.CommandText = string.Format("select id_parameter_type from parameter where code = '{0}'", param.Attributes["Parameter"].Value);
                                                int type = (int)cmd.ExecuteScalar();

                                                string value = "";

                                                if (param.Attributes["Value"].Value != "")
                                                    if (type == 5)
                                                    {
                                                        cmd.CommandText = string.Format("select value from parameter_dictionary where parameter = '{0}' and number = {1}", param.Attributes["Parameter"].Value, param.Attributes["Value"].Value);
                                                        value = cmd.ExecuteScalar().ToString();
                                                    }
                                                    else
                                                    {
                                                        value = param.Attributes["Value"].Value;
                                                    }
                                                
                                                    

                                                string cond = "";

                                                switch (param.Attributes["Connection"].Value)
                                                {
                                                    case "и":
                                                        cond = "and";
                                                        break;
                                                    case "или":
                                                        cond = "or";
                                                        break;
                                                }

                                                cmd.CommandText = string.Format("select id_parameter_owner from parameter where code = '{0}'", param.Attributes["Parameter"].Value);
                                                int own = (int)cmd.ExecuteScalar();

                                                string type_name = "";

                                                if (type == 3)
                                                {
                                                    type_name = "string";
                                                }
                                                else
                                                {
                                                    type_name = "float";
                                                }


                                                object[] o = new object[] { param.Attributes["Connection"].Value, per, par, operat, value, cond, param.Attributes["Period"].Value, param.Attributes["Parameter"].Value, own, param.Attributes["Operation"].Value, param.Attributes["Value"].Value, type_name };
                                                parameterFilter2.dataGridView2.Rows.Add(o);

                                                sqlconn.Close();
                                            };

                                            break;
                                    }


                                }




                            }
                        }
                        else
                        {
                            return;
                        }
                    }


                }
                else
                {
                    return;
                }
            }
            catch { return; }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string ext = Path.GetExtension(saveFileDialog1.FileName);

                if (ext == ".txt")
                {
                    int n = 0;

                    foreach (string tag in TagList)
                    {
                        //if (tag.Contains("id_account"))
                            n += 1;
                    }

                    List<string> list = new List<string>(); 

                    SqlConnection sqlconn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);

                   

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = sqlconn;

                    n = 0;

                    SqlDataReader reader;

                    foreach (string tag in TagList)
                    {
                        if (tag.Contains("id_account"))
                        {
                        n = 0;

                            cmd.CommandText = " SELECT distinct aa.id_account" +
                           " FROM house_derive_company hdc " +
                           " inner join vaccount_address_all aa on aa.id_house = hdc.id_house" +
                           " where  hdc.period = " + comboBox2.SelectedValue + " and (aa." + tag + ")";

                            cmd.CommandText = cmd.CommandText.Replace("aa.id_derive_company", "id_derive_company");

                            

                            sqlconn.Open();

                            reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                list.Add(reader[0].ToString());

                                //n += 1;
                            }

                            sqlconn.Close();
                        }
                        
                    }

                    string[] a = list.ToArray();

                    File.WriteAllLines(saveFileDialog1.FileName,a);

                }
                else
                {
                    if (ext == ".afl")
                    {
                        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                        XmlElement element1 = doc.CreateElement(string.Empty, "EllisFilterSettings", string.Empty);
                        doc.AppendChild(element1);

                        XmlElement element2 = doc.CreateElement(string.Empty, "Address", string.Empty);
                        element1.AppendChild(element2);

                        

                        SqlConnection conn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);

                        SqlCommand cmd = new SqlCommand();

                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = conn;

                        string id = "";

                        int type = 0;

                        int n = 0;

                        foreach (string item in TagList)
                        {


                            if (item.ToLower().Contains("id_settlement"))
                            { type = 2; id = "aa.id_house"; }
                            else
                                if (item.ToLower().Contains("id_street"))
                                { type = 2; id = "aa.id_house"; }
                                else
                                    if (item.ToLower().Contains("id_house"))
                                    { type = 2; id = "aa.id_house"; }
                                    else
                                        if (item.ToLower().Contains("id_flat"))
                                        { type = 1; id = "aa.id_flat"; }
                                        else
                                            if (item.ToLower().Contains("id_account"))
                                            { type = 0; id = "aa.id_account"; }
                                             else
                                            { type = 2; id = "aa.id_house"; }

                          
                            

                            cmd.CommandText =

                            " SELECT distinct " + id + ", id_derive_company as comp, c.name+', '+aa.settlement+', '+aa.street+', '+aa.full_house_name as caption " +
                           " FROM dbo.Bill_Filter F" +
                           " INNER JOIN dbo.List_To_Table((SELECT Filter_Codes FROM Reports.Bill_Kind WHERE ID_Bill_Kind = " + comboBox1.SelectedValue + "), DEFAULT) List" +
                           " ON List.ID = F.Code" +
                           " inner join reports.client_detail cd on cd.id_bill_filter = f.id_bill_filter and " + comboBox2.SelectedValue + " between begin_period and isnull(end_period," + comboBox2.SelectedValue + ")" +
                           " inner join reports.client_detail_house cdh on cd.id_client_detail = cdh.id_client_detail " +
                           " inner join house_derive_company hdc on hdc.id_house = cdh.id_house and hdc.period = " + comboBox2.SelectedValue + " and hdc.id_company = cd.id_derive_company" +
                           " inner join vaccount_address_all aa on aa.id_house = cdh.id_house" +
                           " inner join company c on c.id_company = cd.id_derive_company where aa." + item;

                            //cmd.CommandText = cmd.CommandText.Replace("id_derive_company", "id_company");

                            cmd.CommandText = cmd.CommandText.Replace("aa.id_derive_company", "id_derive_company");

                            conn.Open();

                            

                            SqlDataReader res = cmd.ExecuteReader();

                            


                            while (res.Read())
                            {

                                XmlElement element3 = doc.CreateElement(string.Empty, "A" + n, string.Empty);
                                element3.SetAttribute("ID", res[0].ToString());
                                element3.SetAttribute("TypeID", type.ToString());
                                element3.SetAttribute("DeriveCompany", res["comp"].ToString());

                                if (type == 2)
                                    element3.SetAttribute("Caption", res["caption"].ToString());
                                else
                                    element3.SetAttribute("Caption", listBox1.Items[TagList.IndexOf(item)].ToString());

                                element2.AppendChild(element3);

                                n += 1;
                            }

                            

                            conn.Close();

                            if (TagList.Count != 0)
                                element2.SetAttribute("Count", n.ToString());

                        }








                        XmlElement element5 = doc.CreateElement(string.Empty, "Parameter", string.Empty);
                        element1.AppendChild(element5);

                        if (parameterFilter2.dataGridView2.Rows.Count != 0)
                            element5.SetAttribute("Count", parameterFilter2.dataGridView2.Rows.Count.ToString());






                        foreach (DataGridViewRow row in parameterFilter2.dataGridView2.Rows)
                        {
                            XmlElement element4 = doc.CreateElement(string.Empty, "Pr" + (parameterFilter2.dataGridView2.Rows.IndexOf(row)+1), string.Empty);


                            element4.SetAttribute("Connection", row.Cells[0].Value.ToString());
                            element4.SetAttribute("Period", row.Cells["period"].Value.ToString());
                            element4.SetAttribute("Parameter", row.Cells["parameter"].Value.ToString());
                            element4.SetAttribute("Operation", row.Cells["operator_"].Value.ToString());
                            element4.SetAttribute("Value", row.Cells["value"].Value.ToString());

                            element5.AppendChild(element4);


                        }


                        conn.Open();







                        XmlElement element6 = doc.CreateElement(string.Empty, "Provider", string.Empty);
                        element1.AppendChild(element6);


                        doc.Save(saveFileDialog1.FileName);
                    }
                    else
                    {
                        return;
                    }
                }
            
            }
            else
                return;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
           
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
                FillTreeView();
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        void PrepareDebtAccounts()
        {
            if ((int)comboBox3.SelectedValue == -1 && customEdit1.EditValue == ""
                )
            {
                if (chbDebtFilterCodePeriod.Checked == false)
                {
                    MessageBox.Show("Для долговых квитанций необходимо указать УК");
                    return;
                }
            }

            this.Cursor = Cursors.WaitCursor;

            SqlConnection sqlconn = StaticConfig.MainConnection;
            sqlconn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlconn;
            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;

            var companies = "";
            if (chBxTwoDebtComp.Checked)
            {
                companies = "'" + customEdit1.EditValue.ToString() + "'";
            }
            else
            {
                companies = comboBox3.SelectedValue.ToString();
            }

            cmd.CommandText = string.Format(" declare @codes varchar(max)" +

                        " SELECT " +
                        " @codes =" +
                        " Filter_Codes FROM Reports.Bill_Kind WHERE ID_Bill_Kind = {2}" +

                        " exec [town].[MassBillNew_Show_LS_by_UK] @period={0}, @ID_company={1},@id_bill_filter = @codes", comboBox2.SelectedValue,
                        //comboBox3.SelectedValue
                        companies
                        , comboBox1.SelectedValue);

            SqlDataReader reader = cmd.ExecuteReader();

            debtCompanyAccountList.Clear();
            while (reader.Read())
            {
                if (chBxTwoDebtComp.Checked)
                {
                    var debtCompanyAccount = new DebtCompanyAccount()
                    {
                        idAccount = int.Parse(reader["id_account"].ToString()),
                        idDebtCompany = int.Parse(reader["id_derive_company"].ToString())
                    };
                    debtCompanyAccountList.Add(debtCompanyAccount);
                }
                else
                {
                    listBox1.Items.Add(reader["caption"]); //добавляем в listbox
                    TagList.Add("id_account = " + reader["id_account"]); // ИД  в taglist
                }
            }

            sqlconn.Close();

            this.Cursor = Cursors.Arrow;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            PrepareDebtAccounts();
        }



        private void listBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            

            ArrayList list = new ArrayList();
            ArrayList listTag = new ArrayList();

            if (e.KeyCode == Keys.Delete)
            {
                //if (chBxTwoDebtComp.Checked)
                //{
                //    MessageBox.Show("При выборе нескольких долговых УК данное поле только для чтения. Редактирование запрещено");
                //}
                //else
                //{
                    foreach (object item in listBox1.SelectedItems)
                    {
                        list.Add(item);
                        listTag.Add(TagList[(listBox1.Items.IndexOf(item))]);

                    }

                    foreach (object item in list)
                    {
                        listBox1.Items.Remove(item);

                    }

                    foreach (object item in listTag)
                    {
                        TagList.Remove(item);

                    }
                //} //if (chBxTwoDebtComp.Checked)
            } //if (e.KeyCode == Keys.Delete)

        }


        string PrepareIdPrintList(ArrayList objectList, int count, bool saveHistory, bool backSide, bool printBillsWithSubscription, string savepath, bool printDoubleKvit, string sqlCommandText)
        {
            int n = 0;
            string IDPrintList = "";
            string filename = "";

            if (chBxTwoDebtComp.Checked)
            {
                PrepareDebtAccounts();
            }

            foreach (var name in objectList)
            {
                SqlConnection connection = StaticConfig.MainConnection;

                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandTimeout = 0;
                cmd2.CommandType = CommandType.Text;
                cmd2.Connection = connection;

                report1 = new Report();
                report2 = new Report();

                n += 1;

                var idName = "";

                if (chBxTwoDebtComp.Checked) //если выбрано форм-е несколких долг. УК, то ID УК заменяем на назв., если идет формир-е по улицам то оставлять название улицы
                {
                    cmd2.CommandText = string.Format(" select name from dbo.company where id_company = {0} ", name.ToString());
                    connection.Open();
                    idName = cmd2.ExecuteScalar().ToString();
                    connection.Close();
                    idName = idName.Replace("\"", "");
                    idName = idName.Replace("\\", "");
                    idName = idName.Replace("/", "");
                    idName = idName.Replace("*", "");
                    idName = idName.Replace(":", "");
                    idName = idName.Replace("?", "");
                    idName = idName.Replace(">", "");
                    idName = idName.Replace("<", "");
                    idName = idName.Replace("|", "");
                }
                else
                {
                    idName = name.ToString();
                }

                if (n == count || objectList.IndexOf(name) == objectList.Count - 1)
                    filename += idName;
                else
                    filename += idName + ",";

                if (chBxTwoDebtComp.Checked)
                {
                    //cmd2.CommandText = sqlCommandText + string.Format(" select distinct id_account from #temp ");
                    foreach (DebtCompanyAccount item in debtCompanyAccountList)
                    {
                        if (int.Parse(name.ToString()) == item.idDebtCompany)
                        {
                            AccountList.Add(item.idAccount);
                        }
                    }

                }
                else
                {
                    cmd2.CommandText = sqlCommandText + string.Format(" select distinct id_account from #temp where street= '{0}' ", name.ToString());

                    connection.Open();

                    SqlDataReader reader;
                    reader = cmd2.ExecuteReader();

                    while (reader.Read())
                    {
                        AccountList.Add(reader[0]);
                    }

                    connection.Close();
                }

                //report1.Preview = previewControl1;
                if (n == count || objectList.IndexOf(name) == objectList.Count - 1)
                {
                    connection.Open();

                    SqlCommand cmd3 = new SqlCommand();
                    cmd3.CommandTimeout = 0;
                    cmd3.CommandType = CommandType.Text;
                    cmd3.Connection = connection;

                    cmd3.CommandText = "exec Reports.Bill_Binary_Convert  " + comboBox2.SelectedValue.ToString();

                    byte[] module = (byte[])cmd3.ExecuteScalar();

                    connection.Close();

                    report1.LoadFromString(Encoding.UTF8.GetString(module));
                    report2.LoadFromString(Encoding.UTF8.GetString(module));

                    long idPrint = -1;
                    if (chBxTwoDebtComp.Checked)
                    {
                        idPrint = PrepareRepotData(saveHistory, backSide, printBillsWithSubscription, int.Parse(name.ToString()));
                    }
                    else
                    {
                        idPrint = PrepareRepotData(saveHistory, backSide, printBillsWithSubscription);
                    }

                    IDPrintList += idPrint + ",";

                    textBox3.Text = idPrint.ToString();

                    int prepare = 1;
                    cmd3.CommandText = string.Format(" if exists (select * from Reports.Print_Bill where ID_Print = {0}) select 1 else select 0 ", idPrint);
                    connection.Open();
                    prepare = int.Parse(cmd3.ExecuteScalar().ToString());
                    connection.Close();

                    if (prepare == 1)
                    {
                        report1.SetParameterValue("PrintFio", true);
                        report1.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);
                        report1.SetParameterValue("IDPrint", idPrint);
                        report1.SetParameterValue("PrintBillsWithSubscription", printBillsWithSubscription);

                        report1.SetParameterValue("IS_DEBT", radioButton2.Checked);

                        report1.Prepare();

                        report2.SetParameterValue("PrintFio", true);
                        report2.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);
                        report2.SetParameterValue("IDPrint", idPrint);
                        report2.SetParameterValue("PrintBillsWithSubscription", printBillsWithSubscription);

                        report2.SetParameterValue("IS_DEBT", radioButton2.Checked);

                        //report1.Clear();

                        var filenamefull = savepath + "\\" + filename + ".fpx";

                        if (printDoubleKvit)
                        {
                            //if (fileName.Contains(".fpx"))
                            //{
                            //    fileName = Path.GetFileNameWithoutExtension(fileName);
                            //}


                            report1.SavePrepared(filenamefull
                            //+ ".fpx"
                            );



                            //report1.Clear();
                            report2.SetParameterValue("PrintBillsWithSubscription", true);
                            report2.Prepare();

                            report2.SavePrepared(filenamefull.Replace(".fpx", "(копия для МЭС).fpx")
                            //.fpx"
                            );

                        }

                        report1.SavePrepared(filenamefull
                        //+ ".fpx"
                        );


                        //tabControl1.SelectTab(1);

                        AccountList.Clear();

                        n = 0;
                        filename = "";

                        report1.Dispose();
                        report2.Dispose();
                    }
                }
        }//foreach (var street in StreetList)

            IDPrintList = IDPrintList.TrimEnd(',');

            return IDPrintList;
        }

        void PrepareReportsAndSaveToFiles()
        {
            bool backSide = false;
            bool saveHistory = false;
            bool printBillsWithSubscription = false;
            bool printDoubleKvit = false;

            PrintHistoryDialog printHistoryDialog = new PrintHistoryDialog();

            if (printHistoryDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                backSide = printHistoryDialog.BackSide;
                saveHistory = printHistoryDialog.SaveHistory;
                printBillsWithSubscription = printHistoryDialog.PrintBillsWithSubscription;
                printDoubleKvit = printHistoryDialog.PrintDoubleKvit;
            }
            else return;

            string savepath = "";

            int count = 3;

            //CountDialog cd = new CountDialog();

            //var fileName = "";
            //if (ShowDoubleKvitDialog(printDoubleKvit, fileName) == false) return;

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                savepath = folderBrowserDialog1.SelectedPath;

                //if (cd.ShowDialog() == DialogResult.OK)
                //{
                //    count = cd.count;
                //}
                //else
                //{
                //    return; 
                //}
            }
            else
            {
                return;
            }

            if (radioButton1.Checked)
            {
                if (TagList.Count == 0)
                {
                    MessageBox.Show("Ничего не выбрано");
                    return;
                }

                if (((int)comboBox3.SelectedValue == -1 && customEdit1.EditValue.ToString() == "") & radioButton2.Checked)
                {
                    if (chbDebtFilterCodePeriod.Checked == false)
                    {
                        MessageBox.Show("Для долговых квитанций необходимо указать УК");
                        return;
                    }
                }

                label1.Text = "Выполняется формирование отчета...";
                label1.Refresh();

                this.Cursor = Cursors.WaitCursor;
                SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();


                cmd.CommandText =

                   " IF OBJECT_ID('tempdb..#temp')is not null drop table #temp select distinct aa.id_account,aa.street into #temp" +
                   " FROM dbo.Bill_Filter F" +
                   " INNER JOIN dbo.List_To_Table((SELECT Filter_Codes FROM Reports.Bill_Kind WHERE ID_Bill_Kind = " + comboBox1.SelectedValue + "), DEFAULT) List" +
                   " ON List.ID = F.Code" +
                   " inner join reports.client_detail cd on cd.id_bill_filter = f.id_bill_filter and " + comboBox2.SelectedValue + " between begin_period and isnull(end_period," + comboBox2.SelectedValue + ")" +
                   " inner join reports.client_detail_house cdh on cd.id_client_detail = cdh.id_client_detail " +
                   " inner join house_derive_company hdc on hdc.id_house = cdh.id_house and hdc.period = " + comboBox2.SelectedValue + " and hdc.id_company = cd.id_derive_company" +
                   " inner join vaccount_address_all aa on aa.id_house = cdh.id_house" +
                   " inner join company c on c.id_company = cd.id_derive_company" +
                   " where (";

                for (int i = 0; i < TagList.Count; i++)
                {

                    if (i == (TagList.Count - 1))
                    {
                        cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") ) ";
                    }
                    else
                    {
                        cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") or ";
                    }

                }

                cmd.CommandText = cmd.CommandText.Replace("aa.id_derive_company", "id_derive_company");

                cmd.CommandText = cmd.CommandText.Replace("aa.id_client", "id_client");

                foreach (DataGridViewRow item in parameterFilter2.dataGridView2.Rows)
                {
                    string owner = "";
                    switch (item.Cells["id_owner"].Value.ToString())
                    {
                        case "1":
                            owner = "account";
                            break;
                        case "2":
                            owner = "flat";
                            break;
                        case "4":
                            owner = "house";
                            break;
                    }

                    if (item.Index == 0)
                        item.Cells["condition"].Value = "and ("; //добавить открывающую скобку и заменить условие на "и", так как при "или" запрос будет выводить все лицевые с первым парметром

                    if (item.Cells["value"].Value == "")
                    {
                        item.Cells["value"].Value = " null ";
                        if (item.Cells["operator_"].Value == "=")
                        {
                            item.Cells["operator_"].Value = "is";
                        }
                        if (item.Cells["operator_"].Value == "<>")
                        {
                            item.Cells["operator_"].Value = "is not";
                        }
                    }


                    //добавляем выбор ЛС по параметрам, расположенным в parameterFilter2.dataGridView2

                    cmd.CommandText = cmd.CommandText + String.Format(" {0} aa.id_{5} in (select id_{5} from v{5}_parameter where period = {1} and parameter = '{2}' and {6}_value {3} {4} )", item.Cells["condition"].Value, item.Cells["period"].Value, item.Cells["parameter"].Value, item.Cells["operator_"].Value, item.Cells["value"].Value, owner, item.Cells["type"].Value);
                }

                if (parameterFilter2.dataGridView2.Rows.Count != 0) //если есть параметры - закрываем скобку 
                {
                    cmd.CommandText += " ) ";
                }

                ArrayList StreetList = new ArrayList();
                ArrayList DebtUkList = new ArrayList();

                SqlCommand cmd1 = new SqlCommand();
                cmd1.CommandTimeout = 0;
                cmd1.CommandType = CommandType.Text;
                cmd1.Connection = sqlConnection1;

                cmd1.CommandText = cmd.CommandText + " select distinct street from #temp  order by street";

                sqlConnection1.Open();

                SqlDataReader reader1;
                reader1 = cmd1.ExecuteReader();
                while (reader1.Read())
                {
                    StreetList.Add(reader1[0]);
                }
                reader1.Close();

                if (chBxTwoDebtComp.Checked)
                {
                    cmd1.CommandText = string.Format(" select id_company from dbo.company where id_compny in ({0}) ", customEdit1.EditValue.ToString());

                    reader1 = cmd1.ExecuteReader();
                    while (reader1.Read())
                    {
                        DebtUkList.Add(reader1[0]);
                    }
                }

                sqlConnection1.Close();

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                var objectList = StreetList;

                if (chBxTwoDebtComp.Checked)
                {
                    objectList = DebtUkList;
                    count = 1;
                }

                var IDPrintList = PrepareIdPrintList(objectList, count,saveHistory,backSide,printBillsWithSubscription,savepath,printDoubleKvit,cmd.CommandText);

                report1 = new Report();

                report1.Load("count_ls.frx");

                report1.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);
                report1.SetParameterValue("IDPrint", IDPrintList);
                report1.SetParameterValue("PrintBillsWithSubscription", printBillsWithSubscription);

                report1.Prepare();
                report1.SavePrepared(savepath + "\\ИТОГО.fpx");

                SqlCommand cmd4 = new SqlCommand();
                cmd4.CommandTimeout = 0;
                cmd4.CommandType = CommandType.Text;
                cmd4.Connection = sqlConnection1;

                string[] idPrints = string.Concat(IDPrintList).Split(new Char[] { ',' });

                sqlConnection1.Open();

                foreach (string idPrint in idPrints)
                {
                    cmd4.CommandText = " exec [Reports].[Clear_Print_Data] @ID_Print=" + idPrint;
                    cmd4.ExecuteNonQuery();
                }

                report1.Dispose();

                sqlConnection1.Close();

                StreetList.Clear();
            }
            else
            {

                if (((int)comboBox3.SelectedValue == -1 && customEdit1.EditValue.ToString() == "") & radioButton2.Checked)
                {
                    if (chbDebtFilterCodePeriod.Checked == false)
                    {
                        MessageBox.Show("Для долговых квитанций необходимо указать УК");
                        return;
                    }
                }


                label1.Text = "Выполняется формирование отчета...";
                label1.Refresh();

                this.Cursor = Cursors.WaitCursor;

                ArrayList StreetList = new ArrayList();
                ArrayList DebtUkList = new ArrayList();
                
                SqlDataReader reader1;

                SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                SqlCommand cmd1 = new SqlCommand();
                cmd1.CommandTimeout = 0;
                cmd1.CommandType = CommandType.Text;
                cmd1.Connection = sqlConnection1;

                if (!chBxTwoDebtComp.Checked)
                {
                    if (TagList.Count == 0)
                    {
                        MessageBox.Show("Ничего не выбрано");
                        return;
                    }

                    cmd.CommandText =
                    " IF OBJECT_ID('tempdb..#temp')is not null drop table #temp select distinct aa.id_account,aa.street into #temp from  house_derive_company hdc " +
                    "inner join vaccount_address_all aa on aa.id_house = hdc.id_house " +
                     " where hdc.period = dbo.work_period() and (";

                    for (int i = 0; i < TagList.Count; i++)
                    {

                        if (i == (TagList.Count - 1))
                        {
                            cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") ) ";
                        }
                        else
                        {
                            cmd.CommandText = cmd.CommandText + " (aa." + TagList[i] + ") or ";
                        }

                    }

                    cmd.CommandText = cmd.CommandText.Replace("aa.id_company", "id_company");


                    cmd.CommandText += "  select * into #temp1 from #temp where ";

                    foreach (DataGridViewRow item in parameterFilter2.dataGridView2.Rows)
                    {
                        string owner = "";
                        switch (item.Cells["id_owner"].Value.ToString())
                        {
                            case "1":
                                owner = "account";
                                break;
                            case "2":
                                owner = "flat";
                                break;
                            case "4":
                                owner = "house";
                                break;
                        }

                        if (item.Index == 0)
                            item.Cells["condition"].Value = " ("; //добавить открывающую скобку и заменить условие на "и", так как при "или" запрос будет выводить все лицевые с первым парметром

                        if (item.Cells["value"].Value == "")
                        {
                            item.Cells["value"].Value = " null ";
                            if (item.Cells["operator_"].Value == "=")
                            {
                                item.Cells["operator_"].Value = "is";
                            }
                            if (item.Cells["operator_"].Value == "<>")
                            {
                                item.Cells["operator_"].Value = "is not";
                            }
                        }


                        //добавляем выбор ЛС по параметрам, расположенным в parameterFilter2.dataGridView2

                        cmd.CommandText = cmd.CommandText + String.Format(" {0} id_{5} in (select id_{5} from v{5}_parameter where period = {1} and parameter = '{2}' and {6}_value {3} {4} )", item.Cells["condition"].Value, item.Cells["period"].Value, item.Cells["parameter"].Value, item.Cells["operator_"].Value, item.Cells["value"].Value, owner, item.Cells["type"].Value);
                    }

                    if (parameterFilter2.dataGridView2.Rows.Count != 0) //если есть параметры - закрываем скобку 
                    {
                        cmd.CommandText += " ) ";
                    }
                    else //если нет  - добавляем 1=1 
                    {
                        cmd.CommandText += " 1=1 ";
                    }

                    cmd1.CommandText = cmd.CommandText + " select distinct street from #temp1  order by street";

                    sqlConnection1.Open();

                    reader1 = cmd1.ExecuteReader();

                    while (reader1.Read())
                    {
                        StreetList.Add(reader1[0]);
                    }
                    sqlConnection1.Close();

                    reader1.Close();
                }

                cmd1.CommandText = string.Format(" select id_company from dbo.company where id_company in ({0}) ", customEdit1.EditValue.ToString());

                

                sqlConnection1.Open();

                reader1 = cmd1.ExecuteReader();
                while (reader1.Read())
                {
                    DebtUkList.Add(reader1[0]);
                }

                sqlConnection1.Close();

  

                var objectList = StreetList;

                if (chBxTwoDebtComp.Checked)
                {
                    objectList = DebtUkList;
                    count = 1;
                }

                var IDPrintList = PrepareIdPrintList(objectList, count, saveHistory, backSide, printBillsWithSubscription, savepath, printDoubleKvit, cmd.CommandText);

                report1.Load("count_ls.frx");

                report1.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);
                report1.SetParameterValue("IDPrint", IDPrintList);
                report1.SetParameterValue("PrintBillsWithSubscription", printBillsWithSubscription);

                report1.Prepare();
                report1.SavePrepared(savepath + "\\ИТОГО.fpx");

                SqlCommand cmd4 = new SqlCommand();
                cmd4.CommandTimeout = 0;
                cmd4.CommandType = CommandType.Text;
                cmd4.Connection = sqlConnection1;

                string[] idPrints = string.Concat(IDPrintList).Split(new Char[] { ',' });

                sqlConnection1.Open();

                foreach (string idPrint in idPrints)
                {
                    cmd4.CommandText = " exec [Reports].[Clear_Print_Data] @ID_Print=" + idPrint;
                    cmd4.ExecuteNonQuery();
                }

                sqlConnection1.Close();

                StreetList.Clear();
                //}
                //catch (Exception exp)
                //{

                //    MessageBox.Show(exp.ToString());
                //    this.Cursor = Cursors.Arrow;
                //    label1.Text = "";

                //    AccountList.Clear();
                //}
            }

            this.Cursor = Cursors.Arrow;
            label1.Text = "Готово";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PrepareReportsAndSaveToFiles();
        }

        private void comboBox2_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
                FillTreeView();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            string s = "";

            foreach (var tag in TagList)
            {
                s += tag+"\n";
            }

            MessageBox.Show(s);
        }



        private void tabPage5_Enter(object sender, EventArgs e)
        {


            gridPrintHistory.ReloadPrintHistoryData();


        }

        private void btnDebtFilterCodePeriod_Click(object sender, EventArgs e)
        {
            SqlConnection sqlconn = StaticConfig.MainConnection;
            sqlconn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlconn;
            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = string.Format(" declare @codes varchar(max)" +

                        " SELECT " +
                        " @codes =" +
                        " Filter_Codes FROM Reports.Bill_Kind WHERE ID_Bill_Kind = {1}" +

                        " exec [town].[MassBillNew_Show_LS_by_Code] @period={0},@bill_filter_code = @codes", comboBox2.SelectedValue, comboBox1.SelectedValue);

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                listBox1.Items.Add(reader["caption"]); //добавляем в listbox
                TagList.Add("id_account = " + reader["id_account"]); // ИД  в taglist
            }

            sqlconn.Close();

            this.Cursor = Cursors.Arrow;
        }

        private void chbDebtFilterCodePeriod_CheckedChanged(object sender, EventArgs e)
        {
            btnDebtFilterCodePeriod.Enabled = chbDebtFilterCodePeriod.Checked;
        }

        private void chBxTwoDebtComp_CheckedChanged(object sender, EventArgs e)
        {
            if (chBxTwoDebtComp.Checked)
            {
                comboBox3.Enabled = false;
                customEdit1.Enabled = true;
                button4.Enabled = false;
                button8.Enabled = false;
            }
            else
            {
                comboBox3.Enabled = true;
                customEdit1.Enabled = false;
                button4.Enabled = true;
                button8.Enabled = true;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (chBxTwoDebtComp.Checked)
            {
                PrepareReportsAndSaveToFiles();
                return;
            }

            if (TagList.Count == 0)
            {
                MessageBox.Show("Ничего не выбрано");
                return;
            }

            if (((int)comboBox3.SelectedValue == -1
                //&& customEdit1.EditValue.ToString() == ""
                ) & radioButton2.Checked)
            {
                if (chbDebtFilterCodePeriod.Checked == false)
                {
                    MessageBox.Show("Для долговых квитанций необходимо указать УК");
                    return;
                }

            }

            bool backSide = false;
            bool saveHistory = false;
            bool printBillsWithSubscription = false;
            bool printDoubleKvit = false;

            if (comboBox1.SelectedValue.ToString() == "462")
            {
                printDoubleKvit = true;
            }

            if (comboBox1.SelectedValue.ToString() == "463")
            {
                printDoubleKvit = true;
            }

            if (comboBox1.SelectedValue.ToString() == "484")
            {
                printDoubleKvit = true;
            }

            PrintHistoryDialog printHistoryDialog = new PrintHistoryDialog();

            printHistoryDialog.PrintDoubleKvit = printDoubleKvit;

            if (printHistoryDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                backSide = printHistoryDialog.BackSide;
                saveHistory = printHistoryDialog.SaveHistory;
                printBillsWithSubscription = printHistoryDialog.PrintBillsWithSubscription;
                printDoubleKvit = printHistoryDialog.PrintDoubleKvit;
            }
            else return;

            PrintKvit(backSide, saveHistory, printBillsWithSubscription, printDoubleKvit);

        }
    }
}
