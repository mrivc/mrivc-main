﻿#region Using directives 
using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Data; 
using System.Data.OleDb; 
using System.Data.SqlClient; 
using System.Configuration;
#endregion Using directives 
 

public class ExcelHelper2
{
    public static void ExportToExcel(DataSet dsSQL, string excelFilePath)
    {
        foreach (DataTable table in dsSQL.Tables)
        {
            ExportToExcel(table, excelFilePath);
        }
    }

    // Export data to an Excel spreadsheet via ADO.NET 
    public static void ExportToExcel(DataTable dtSQL, string excelFilePath) 
    {
        if (dtSQL.Rows.Count == 0)
            return;

        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strDownloadFileName + ";Extended Properties='Excel 8.0;HDR=Yes'";
        var strConn = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={excelFilePath};Extended Properties='Excel 12.0 Xml;HDR=Yes'";

        using (OleDbConnection conn = new OleDbConnection(strConn)) 
        {
            ////DataTable dt = new DataTable();
            ////using (OleDbDataAdapter da1 = new OleDbDataAdapter())
            ////{
                
            ////    conn.Open();
            ////    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

            ////    da1.SelectCommand = new OleDbCommand("select * from [Характеристики МКД$]", conn);
            ////    da1.Fill(dt);
            ////}

            var q = $@"create table [{dtSQL.TableName}] (";
            var insertQuery = $"INSERT INTO [{dtSQL.TableName}] (";
            var values = " values (";

            var cmd = new OleDbCommand("",conn);

            // Create a new sheet in the Excel spreadsheet. 
            foreach (DataColumn column in dtSQL.Columns)
            {
                q += $"[{column.ColumnName}] varchar(200),";
                insertQuery += $"[{column.ColumnName}],";
                values += "?,";
                cmd.Parameters.Add($"{column.ColumnName}", OleDbType.VarChar, 200, $"{column.ColumnName}");
            }

            q = q.TrimEnd(',');
            insertQuery = insertQuery.TrimEnd(',');
            values = values.TrimEnd(',');

            q += ")";
            insertQuery += ")";
            values += ")";

            cmd.CommandText = q;

            //// Open the connection. 
            conn.Open();

            //// Execute the OleDbCommand. 
            cmd.ExecuteNonQuery();

            cmd.CommandText = insertQuery + values;

            // Initialize an OleDBDataAdapter object. 
            OleDbDataAdapter da = new OleDbDataAdapter($"select * from [{dtSQL.TableName}]", conn);

            // Set the InsertCommand of OleDbDataAdapter,  
            // which is used to insert data. 
            da.InsertCommand = cmd;

            foreach (DataRow dr in dtSQL.Rows)
            {
                dr.SetAdded();
            }

            // Insert the data into the Excel spreadsheet. 
            da.Update(dtSQL); 
        } 
    }

    public static DataTable ImportExcelData(string sheetName, string excelFilePath)
    {
        DataTable dt = new DataTable();
        string strDownloadFileName = "";
        string strConn = "";

        strDownloadFileName = excelFilePath;
        strConn = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={strDownloadFileName};Extended Properties='Excel 12.0 Xml;HDR=Yes'";

        using (OleDbConnection conn = new OleDbConnection(strConn))
        {
            using (OleDbDataAdapter da = new OleDbDataAdapter())
            {
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                if (sheetName.Length == 0) sheetName = schemaTable.Rows[0]["TABLE_NAME"].ToString();

                da.SelectCommand = new OleDbCommand("select * from [" + sheetName + "]", conn);
                da.Fill(dt);
            }
        }
        return dt;
    }
} 
