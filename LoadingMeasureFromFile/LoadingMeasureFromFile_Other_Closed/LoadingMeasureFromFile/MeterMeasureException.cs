﻿using System;

namespace LoadingMeasureFromFile_Other
{
    /// <summary>
    /// Класс для обработки исключений связанных с таблицей meter_measure
    /// </summary>
    public class MeterMeasureException : ApplicationException
    { public MeterMeasureException(string message) : base(message) { } }
}
