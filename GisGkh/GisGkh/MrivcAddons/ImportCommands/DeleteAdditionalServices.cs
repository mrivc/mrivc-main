﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class DeleteAdditionalServices : MrivcImportCommand
    {
        public string AddServiceGuid { get; set; }

        public DeleteAdditionalServices(string senderID) : base("importAdditionalServices", "NsiAsync", senderID)
        {
            _importValueName = "DeleteAdditionalServiceType";
            Description = "Удаление дополнительных услуг";
        }


        protected override void CreateXmlBody()
        {
            var deleteAddServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.DeleteAdditionalServices @SenderID, @addServiceGuid", new[,] { { "@SenderID", SenderID }, { "@addServiceGuid", AddServiceGuid } });

            foreach (DataRow row in deleteAddServices.Rows)
            {
                var values = new[]
                {
                    new Tuple<string[],string>(new [] { "ElementGuid" }, row["ElementGuid"].ToString())
                };

                AddChildElement(MrivcStatic.GisConfig.Services["NsiAsync"].Methods["importAdditionalServices_delete"].Template, values, "importAdditionalServicesRequest");
            }
        }
    }
}
