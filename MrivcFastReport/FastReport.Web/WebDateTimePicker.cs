﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using FastReport.Dialog;
using System.Globalization;

namespace FastReport.Web.Handlers
{
    public partial class WebExport : IHttpHandler
    {
        string date_format = "mm/dd/yy";

        private void DateTimePickerChange(DateTimePickerControl dp, string value)
        {
            dp.Value = DateTime.ParseExact(value, "d", CultureInfo.InvariantCulture); 
        }

        private string GetDateTimePickerHtml(DateTimePickerControl control, WebReport webReport)
        {
            control.FillData();
            ControlFilterRefresh(control);
            string id = webReport.Prop.ControlID + control.Name;
            StringBuilder html = new StringBuilder();
            string s = control.Value.Month.ToString() + "/" + control.Value.Day.ToString() + "/" + control.Value.Year.ToString();
            string ev = GetEvent("onchange", control, webReport, string.Format("document.getElementById('{0}').value", id));
            html.Append(String.Format("<input type=\"text\" value=\"{4}\" class=\"{0}\" style=\"{1}\" onchange=\"{2}\" id=\"{3}\"/>",
                "",
                GetDateTimePickerStyle(control),
                ev,
                id,
                s
                ));
            html.Append("<script>$(function() {$( \"#").Append(id).Append("\" ).datepicker();");                
            html.Append("$( \"#").Append(id).Append("\" ).datepicker( \"option\", \"dateFormat\", \"").
                Append(date_format).Append("\" );");
            html.Append("});</script>");            
            
            //control.FilterData();
            return html.ToString();
        }

        private string GetDateTimePickerStyle(DateTimePickerControl control)
        {
            return string.Format("{0}{1}", GetControlPosition(control), GetControlFont(control));
        }        

    }
}
