﻿namespace EllisBillFilter
{
    partial class FrmClaim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmClaim));
            this.txtSum = new DevExpress.XtraEditors.TextEdit();
            this.txtSum_Fine = new DevExpress.XtraEditors.TextEdit();
            this.cbSummTariff = new DevExpress.XtraEditors.CheckEdit();
            this.txtSum_Full = new DevExpress.XtraEditors.TextEdit();
            this.cbFine = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.deDateRepayment = new DevExpress.XtraEditors.DateEdit();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.paperStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.deriveCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.providerGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ddProviderGroup = new EllisBillFilter.BaseDropDown();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProvider_Group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.deClaimStartDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.deClaimEndDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.seDaysCount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ddDeriveCompany = new EllisBillFilter.BaseDropDown();
            this.baseDropDown1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.ddPaperStatus = new EllisBillFilter.BaseDropDown();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPaper_Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.deIskDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.deSudDateBegin = new DevExpress.XtraEditors.DateEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.deSudDateEnd = new DevExpress.XtraEditors.DateEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.dsDolgDateEnd = new DevExpress.XtraEditors.DateEdit();
            this.dsDolgDateBegin = new DevExpress.XtraEditors.DateEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.ddSud = new EllisBillFilter.BaseDropDown();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ddJudge = new EllisBillFilter.BaseDropDown();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum_Fine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSummTariff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum_Full.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFine.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deDateRepayment.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateRepayment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerGroupBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddProviderGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deClaimStartDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClaimStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClaimEndDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClaimEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seDaysCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddDeriveCompany.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDropDown1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPaperStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deIskDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deIskDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSudDateBegin.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSudDateBegin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSudDateEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSudDateEnd.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsDolgDateEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsDolgDateEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsDolgDateBegin.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsDolgDateBegin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddSud.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddJudge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // txtSum
            // 
            this.txtSum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSum.Location = new System.Drawing.Point(71, 58);
            this.txtSum.Name = "txtSum";
            this.txtSum.Properties.DisplayFormat.FormatString = "c2";
            this.txtSum.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSum.Properties.EditFormat.FormatString = "c2";
            this.txtSum.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSum.Properties.Mask.EditMask = "c";
            this.txtSum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSum.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSum.Size = new System.Drawing.Size(173, 20);
            this.txtSum.TabIndex = 6;
            this.txtSum.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtSum_EditValueChanging);
            // 
            // txtSum_Fine
            // 
            this.txtSum_Fine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSum_Fine.Location = new System.Drawing.Point(71, 84);
            this.txtSum_Fine.Name = "txtSum_Fine";
            this.txtSum_Fine.Properties.DisplayFormat.FormatString = "c2";
            this.txtSum_Fine.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSum_Fine.Properties.EditFormat.FormatString = "c2";
            this.txtSum_Fine.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSum_Fine.Properties.Mask.EditMask = "c";
            this.txtSum_Fine.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSum_Fine.Size = new System.Drawing.Size(173, 20);
            this.txtSum_Fine.TabIndex = 7;
            this.txtSum_Fine.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtSum_Fine_EditValueChanging);
            // 
            // cbSummTariff
            // 
            this.cbSummTariff.Enabled = false;
            this.cbSummTariff.Location = new System.Drawing.Point(15, 24);
            this.cbSummTariff.Name = "cbSummTariff";
            this.cbSummTariff.Properties.Caption = "Начисления текущего месяца";
            this.cbSummTariff.Size = new System.Drawing.Size(229, 19);
            this.cbSummTariff.TabIndex = 8;
            // 
            // txtSum_Full
            // 
            this.txtSum_Full.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSum_Full.Location = new System.Drawing.Point(71, 123);
            this.txtSum_Full.Name = "txtSum_Full";
            this.txtSum_Full.Properties.DisplayFormat.FormatString = "c2";
            this.txtSum_Full.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSum_Full.Properties.EditFormat.FormatString = "c2";
            this.txtSum_Full.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSum_Full.Properties.Mask.EditMask = "c";
            this.txtSum_Full.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSum_Full.Size = new System.Drawing.Size(173, 20);
            this.txtSum_Full.TabIndex = 9;
            // 
            // cbFine
            // 
            this.cbFine.EditValue = true;
            this.cbFine.Location = new System.Drawing.Point(15, 85);
            this.cbFine.Name = "cbFine";
            this.cbFine.Properties.Caption = "Пеня:";
            this.cbFine.Size = new System.Drawing.Size(50, 19);
            this.cbFine.TabIndex = 10;
            this.cbFine.CheckedChanged += new System.EventHandler(this.cbFine_CheckedChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(16, 300);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(61, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "Поставщик:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.cbSummTariff);
            this.groupBox1.Controls.Add(this.txtSum);
            this.groupBox1.Controls.Add(this.txtSum_Fine);
            this.groupBox1.Controls.Add(this.txtSum_Full);
            this.groupBox1.Controls.Add(this.cbFine);
            this.groupBox1.Location = new System.Drawing.Point(389, 236);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(263, 158);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Сумма претензии/иска:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(17, 126);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 13);
            this.labelControl5.TabIndex = 16;
            this.labelControl5.Text = "Всего:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(17, 61);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(35, 13);
            this.labelControl4.TabIndex = 15;
            this.labelControl4.Text = "Сумма:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(14, 173);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(54, 13);
            this.labelControl9.TabIndex = 19;
            this.labelControl9.Text = "Погашена:";
            // 
            // deDateRepayment
            // 
            this.deDateRepayment.EditValue = null;
            this.deDateRepayment.Location = new System.Drawing.Point(70, 170);
            this.deDateRepayment.Name = "deDateRepayment";
            this.deDateRepayment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateRepayment.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deDateRepayment.Size = new System.Drawing.Size(173, 20);
            this.deDateRepayment.TabIndex = 18;
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(15, 351);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(368, 43);
            this.txtNote.TabIndex = 20;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(16, 332);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(65, 13);
            this.labelControl10.TabIndex = 21;
            this.labelControl10.Text = "Примечание:";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(493, 401);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 22;
            this.btnOK.Text = "ОК";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(577, 401);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 23;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // paperStatusBindingSource
            // 
            this.paperStatusBindingSource.DataMember = "PaperStatus";
            this.paperStatusBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // deriveCompanyBindingSource
            // 
            this.deriveCompanyBindingSource.DataMember = "DeriveCompany";
            this.deriveCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // providerGroupBindingSource
            // 
            this.providerGroupBindingSource.DataMember = "ProviderGroup";
            this.providerGroupBindingSource.DataSource = this.mDataSet;
            // 
            // ddProviderGroup
            // 
            this.ddProviderGroup.EditValue = "";
            this.ddProviderGroup.Enabled = false;
            this.ddProviderGroup.Location = new System.Drawing.Point(147, 297);
            this.ddProviderGroup.Name = "ddProviderGroup";
            this.ddProviderGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddProviderGroup.Properties.DataSource = this.providerGroupBindingSource;
            this.ddProviderGroup.Properties.DisplayMember = "Provider_Group";
            this.ddProviderGroup.Properties.NullText = "";
            this.ddProviderGroup.Properties.PopupFormSize = new System.Drawing.Size(0, 70);
            this.ddProviderGroup.Properties.PopupSizeable = false;
            this.ddProviderGroup.Properties.ShowFooter = false;
            this.ddProviderGroup.Properties.ValueMember = "ID_Provider_Group";
            this.ddProviderGroup.Properties.View = this.gridView1;
            this.ddProviderGroup.Size = new System.Drawing.Size(236, 20);
            this.ddProviderGroup.TabIndex = 1;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProvider_Group});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowColumnHeaders = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // colProvider_Group
            // 
            this.colProvider_Group.FieldName = "Provider_Group";
            this.colProvider_Group.Name = "colProvider_Group";
            this.colProvider_Group.Visible = true;
            this.colProvider_Group.VisibleIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(650, 231);
            this.tabControl1.TabIndex = 24;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.labelControl9);
            this.tabPage1.Controls.Add(this.deDateRepayment);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(642, 205);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Претензия";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.ddJudge);
            this.tabPage2.Controls.Add(this.ddSud);
            this.tabPage2.Controls.Add(this.labelControl17);
            this.tabPage2.Controls.Add(this.labelControl16);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.deIskDate);
            this.tabPage2.Controls.Add(this.labelControl11);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(642, 205);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Иск";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.deClaimStartDate);
            this.groupBox2.Controls.Add(this.labelControl8);
            this.groupBox2.Controls.Add(this.deClaimEndDate);
            this.groupBox2.Controls.Add(this.labelControl7);
            this.groupBox2.Controls.Add(this.seDaysCount);
            this.groupBox2.Controls.Add(this.labelControl6);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(285, 158);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Срок претензии:";
            // 
            // deClaimStartDate
            // 
            this.deClaimStartDate.EditValue = null;
            this.deClaimStartDate.Location = new System.Drawing.Point(12, 49);
            this.deClaimStartDate.Name = "deClaimStartDate";
            this.deClaimStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deClaimStartDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deClaimStartDate.Size = new System.Drawing.Size(137, 20);
            this.deClaimStartDate.TabIndex = 3;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(12, 91);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(162, 13);
            this.labelControl8.TabIndex = 17;
            this.labelControl8.Text = "Дата планируемого погашения:";
            // 
            // deClaimEndDate
            // 
            this.deClaimEndDate.EditValue = null;
            this.deClaimEndDate.Location = new System.Drawing.Point(12, 110);
            this.deClaimEndDate.Name = "deClaimEndDate";
            this.deClaimEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deClaimEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deClaimEndDate.Size = new System.Drawing.Size(260, 20);
            this.deClaimEndDate.TabIndex = 4;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(172, 30);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(65, 13);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Срок (дней):";
            // 
            // seDaysCount
            // 
            this.seDaysCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seDaysCount.Location = new System.Drawing.Point(172, 49);
            this.seDaysCount.Name = "seDaysCount";
            this.seDaysCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seDaysCount.Size = new System.Drawing.Size(100, 20);
            this.seDaysCount.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(12, 30);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(64, 13);
            this.labelControl6.TabIndex = 15;
            this.labelControl6.Text = "Дата ввода:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(16, 274);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(128, 13);
            this.labelControl1.TabIndex = 26;
            this.labelControl1.Text = "Управляющая компания:";
            // 
            // ddDeriveCompany
            // 
            this.ddDeriveCompany.EditValue = "";
            this.ddDeriveCompany.Location = new System.Drawing.Point(147, 271);
            this.ddDeriveCompany.Name = "ddDeriveCompany";
            this.ddDeriveCompany.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddDeriveCompany.Properties.DataSource = this.deriveCompanyBindingSource;
            this.ddDeriveCompany.Properties.DisplayMember = "Name";
            this.ddDeriveCompany.Properties.NullText = "";
            this.ddDeriveCompany.Properties.PopupFormSize = new System.Drawing.Size(0, 70);
            this.ddDeriveCompany.Properties.PopupSizeable = false;
            this.ddDeriveCompany.Properties.ValueMember = "ID_Company";
            this.ddDeriveCompany.Properties.View = this.baseDropDown1View;
            this.ddDeriveCompany.Size = new System.Drawing.Size(236, 20);
            this.ddDeriveCompany.TabIndex = 25;
            // 
            // baseDropDown1View
            // 
            this.baseDropDown1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName});
            this.baseDropDown1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.baseDropDown1View.Name = "baseDropDown1View";
            this.baseDropDown1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.baseDropDown1View.OptionsView.ShowColumnHeaders = false;
            this.baseDropDown1View.OptionsView.ShowGroupPanel = false;
            this.baseDropDown1View.OptionsView.ShowIndicator = false;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(16, 248);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(40, 13);
            this.labelControl3.TabIndex = 28;
            this.labelControl3.Text = "Статус:";
            // 
            // ddPaperStatus
            // 
            this.ddPaperStatus.EditValue = "";
            this.ddPaperStatus.Location = new System.Drawing.Point(147, 245);
            this.ddPaperStatus.Name = "ddPaperStatus";
            this.ddPaperStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddPaperStatus.Properties.DataSource = this.paperStatusBindingSource;
            this.ddPaperStatus.Properties.DisplayMember = "Paper_Status";
            this.ddPaperStatus.Properties.NullText = "";
            this.ddPaperStatus.Properties.PopupFormSize = new System.Drawing.Size(0, 50);
            this.ddPaperStatus.Properties.PopupSizeable = false;
            this.ddPaperStatus.Properties.ValueMember = "ID_Paper_Status";
            this.ddPaperStatus.Properties.View = this.gridView2;
            this.ddPaperStatus.Size = new System.Drawing.Size(236, 20);
            this.ddPaperStatus.TabIndex = 27;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPaper_Status});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowColumnHeaders = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // colPaper_Status
            // 
            this.colPaper_Status.FieldName = "Paper_Status";
            this.colPaper_Status.Name = "colPaper_Status";
            this.colPaper_Status.Visible = true;
            this.colPaper_Status.VisibleIndex = 0;
            // 
            // deIskDate
            // 
            this.deIskDate.EditValue = null;
            this.deIskDate.Location = new System.Drawing.Point(10, 30);
            this.deIskDate.Name = "deIskDate";
            this.deIskDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deIskDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deIskDate.Size = new System.Drawing.Size(137, 20);
            this.deIskDate.TabIndex = 16;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(10, 11);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(64, 13);
            this.labelControl11.TabIndex = 17;
            this.labelControl11.Text = "Дата ввода:";
            // 
            // deSudDateBegin
            // 
            this.deSudDateBegin.EditValue = null;
            this.deSudDateBegin.Location = new System.Drawing.Point(7, 38);
            this.deSudDateBegin.Name = "deSudDateBegin";
            this.deSudDateBegin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deSudDateBegin.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deSudDateBegin.Size = new System.Drawing.Size(137, 20);
            this.deSudDateBegin.TabIndex = 18;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(7, 19);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(69, 13);
            this.labelControl12.TabIndex = 19;
            this.labelControl12.Text = "Дата начала:";
            // 
            // deSudDateEnd
            // 
            this.deSudDateEnd.EditValue = null;
            this.deSudDateEnd.Location = new System.Drawing.Point(151, 38);
            this.deSudDateEnd.Name = "deSudDateEnd";
            this.deSudDateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deSudDateEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deSudDateEnd.Size = new System.Drawing.Size(137, 20);
            this.deSudDateEnd.TabIndex = 20;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(151, 19);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(87, 13);
            this.labelControl13.TabIndex = 21;
            this.labelControl13.Text = "Дата окончания:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelControl12);
            this.groupBox3.Controls.Add(this.deSudDateEnd);
            this.groupBox3.Controls.Add(this.deSudDateBegin);
            this.groupBox3.Controls.Add(this.labelControl13);
            this.groupBox3.Location = new System.Drawing.Point(9, 56);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(290, 67);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Судебное разбирательство";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelControl14);
            this.groupBox4.Controls.Add(this.dsDolgDateEnd);
            this.groupBox4.Controls.Add(this.dsDolgDateBegin);
            this.groupBox4.Controls.Add(this.labelControl15);
            this.groupBox4.Location = new System.Drawing.Point(315, 56);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(290, 67);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Образование задолженности";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(7, 19);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(69, 13);
            this.labelControl14.TabIndex = 19;
            this.labelControl14.Text = "Дата начала:";
            // 
            // dsDolgDateEnd
            // 
            this.dsDolgDateEnd.EditValue = null;
            this.dsDolgDateEnd.Location = new System.Drawing.Point(151, 38);
            this.dsDolgDateEnd.Name = "dsDolgDateEnd";
            this.dsDolgDateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dsDolgDateEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dsDolgDateEnd.Size = new System.Drawing.Size(137, 20);
            this.dsDolgDateEnd.TabIndex = 20;
            // 
            // dsDolgDateBegin
            // 
            this.dsDolgDateBegin.EditValue = null;
            this.dsDolgDateBegin.Location = new System.Drawing.Point(7, 38);
            this.dsDolgDateBegin.Name = "dsDolgDateBegin";
            this.dsDolgDateBegin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dsDolgDateBegin.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dsDolgDateBegin.Size = new System.Drawing.Size(137, 20);
            this.dsDolgDateBegin.TabIndex = 18;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(151, 19);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(87, 13);
            this.labelControl15.TabIndex = 21;
            this.labelControl15.Text = "Дата окончания:";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(16, 132);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(24, 13);
            this.labelControl16.TabIndex = 25;
            this.labelControl16.Text = "Суд:";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(16, 161);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(36, 13);
            this.labelControl17.TabIndex = 27;
            this.labelControl17.Text = "Судья:";
            // 
            // ddSud
            // 
            this.ddSud.EditValue = "";
            this.ddSud.Location = new System.Drawing.Point(61, 129);
            this.ddSud.Name = "ddSud";
            this.ddSud.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddSud.Properties.DataSource = this.paperStatusBindingSource;
            this.ddSud.Properties.DisplayMember = "Paper_Status";
            this.ddSud.Properties.NullText = "";
            this.ddSud.Properties.PopupFormSize = new System.Drawing.Size(0, 50);
            this.ddSud.Properties.PopupSizeable = false;
            this.ddSud.Properties.ValueMember = "ID_Paper_Status";
            this.ddSud.Properties.View = this.gridView3;
            this.ddSud.Size = new System.Drawing.Size(407, 20);
            this.ddSud.TabIndex = 28;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowColumnHeaders = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "Paper_Status";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // ddJudge
            // 
            this.ddJudge.EditValue = "";
            this.ddJudge.Location = new System.Drawing.Point(61, 158);
            this.ddJudge.Name = "ddJudge";
            this.ddJudge.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddJudge.Properties.DataSource = this.paperStatusBindingSource;
            this.ddJudge.Properties.DisplayMember = "Paper_Status";
            this.ddJudge.Properties.NullText = "";
            this.ddJudge.Properties.PopupFormSize = new System.Drawing.Size(0, 50);
            this.ddJudge.Properties.PopupSizeable = false;
            this.ddJudge.Properties.ValueMember = "ID_Paper_Status";
            this.ddJudge.Properties.View = this.gridView4;
            this.ddJudge.Size = new System.Drawing.Size(407, 20);
            this.ddJudge.TabIndex = 29;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowColumnHeaders = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "Paper_Status";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // FrmClaim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 433);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.ddPaperStatus);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.ddDeriveCompany);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.ddProviderGroup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(681, 472);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(681, 472);
            this.Name = "FrmClaim";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Новая претензия";
            this.Load += new System.EventHandler(this.FrmClaim_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtSum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum_Fine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSummTariff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum_Full.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFine.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deDateRepayment.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateRepayment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerGroupBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddProviderGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deClaimStartDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClaimStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClaimEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClaimEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seDaysCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddDeriveCompany.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDropDown1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPaperStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deIskDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deIskDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSudDateBegin.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSudDateBegin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSudDateEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSudDateEnd.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsDolgDateEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsDolgDateEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsDolgDateBegin.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsDolgDateBegin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddSud.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddJudge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private BaseDropDown ddProviderGroup;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.TextEdit txtSum;
        private DevExpress.XtraEditors.TextEdit txtSum_Fine;
        private DevExpress.XtraEditors.CheckEdit cbSummTariff;
        private DevExpress.XtraEditors.TextEdit txtSum_Full;
        private DevExpress.XtraEditors.CheckEdit cbFine;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.DateEdit deDateRepayment;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private System.Windows.Forms.BindingSource deriveCompanyBindingSource;
        private DS.MDataSet mDataSet;
        private System.Windows.Forms.BindingSource providerGroupBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colProvider_Group;
        private System.Windows.Forms.BindingSource paperStatusBindingSource;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.DateEdit deClaimStartDate;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.DateEdit deClaimEndDate;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SpinEdit seDaysCount;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private System.Windows.Forms.TabPage tabPage2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private BaseDropDown ddDeriveCompany;
        private DevExpress.XtraGrid.Views.Grid.GridView baseDropDown1View;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private BaseDropDown ddPaperStatus;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colPaper_Status;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private System.Windows.Forms.GroupBox groupBox4;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.DateEdit dsDolgDateEnd;
        private DevExpress.XtraEditors.DateEdit dsDolgDateBegin;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.DateEdit deSudDateEnd;
        private DevExpress.XtraEditors.DateEdit deSudDateBegin;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.DateEdit deIskDate;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private BaseDropDown ddJudge;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private BaseDropDown ddSud;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}