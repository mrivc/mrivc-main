﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading;

namespace Helper
{
    public class EmailSender
    {
        private SmtpClient smtpClient;

        public string SmtpServerHost { get; set; }
        public int SmtpServerPort { get; set; }
        public string SmtpServerMailFrom { get; set; }
        public string SmtpServerMailLogin { get; set; }
        public string SmtpServerPswd { get; set; }
        public bool SmtpServerSSL { get; set; }
        public string SendCopyTo { get; set; }

        public int CountSendedMessages
        {
            get; private set;
        }

        public int CountSendedMessagesWithError
        {
            get; private set;
        }

        public void Send(string email, string[] attachmentPaths, string mailSubject, string mailBody)
        {
            string error = SendMail(SmtpServerHost,
                      SmtpServerMailFrom,
                       SmtpServerPswd,
                       email,
                       SendCopyTo,
                       mailSubject,
                       mailBody,
                       attachmentPaths);

            Thread.Sleep(1000);

            this.CountSendedMessages++;
        }

        public EmailSender()
        {

        }

        public string SendMail(string smtpServer, string from, string password, string mailto, string mailcopyto, string caption, string message, string[] attachFiles = null)
        {
            smtpClient = new SmtpClient(

                SmtpServerHost

                , SmtpServerPort);
            smtpClient.Credentials = new NetworkCredential(SmtpServerMailLogin, SmtpServerPswd);
            smtpClient.EnableSsl = SmtpServerSSL;
            CountSendedMessages = 0;
            CountSendedMessagesWithError = 0;

            MailMessage mail = new MailMessage();

            string error = string.Empty;
            try
            {

                mail.From = new MailAddress(from);

                string[] mailsTo = mailto.Split(';');

                string[] mailsCopyTo = null;

                if (mailcopyto != null)
                    mailsCopyTo = mailcopyto.Split(';');

                foreach (var s in mailsTo) if (s.Length > 0) mail.To.Add(new MailAddress(s.Trim()));

                if (mailcopyto != null)
                    foreach (var s in mailsCopyTo) if (s.Length > 0) mail.CC.Add(new MailAddress(s.Trim()));

                mail.Subject = caption;
                mail.Body = message;

                if (attachFiles != null)
                    foreach (string attachFile in attachFiles)
                    {
                        string fullPath = Path.GetFullPath(attachFile);
                        if (!string.IsNullOrEmpty(attachFile)) mail.Attachments.Add(new Attachment(fullPath));
                    }


                smtpClient.Send(mail);


                error = string.Empty;
            }
            catch (Exception ex)
            {
                //FileHelper.WriteLog("Mail.Send: " + ex.Message + " mailto:" + mailto);
                //error = "Mail.Send: " + ex.Message + " mailto:" + mailto;
                //CountSendedMessagesWithError++;
                throw ex;
            }

            mail.Dispose();

            return error;
        }
    }
}
