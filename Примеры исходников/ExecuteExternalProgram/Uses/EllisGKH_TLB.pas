unit EllisGKH_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 8291 $
// File generated on 01.06.2010 10:51:01 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Projects\ELLISGKH6\EllisGKH.tlb (1)
// LIBID: {303B965C-A99B-4BA1-B497-99DD340FD237}
// LCID: 0
// Helpfile: 
// HelpString: EllisGKH Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  EllisGKHMajorVersion = 1;
  EllisGKHMinorVersion = 0;

  LIBID_EllisGKH: TGUID = '{303B965C-A99B-4BA1-B497-99DD340FD237}';

  IID_IEllisCOMServer: TGUID = '{5EAFE089-5889-4532-8E91-7E80B8FF2884}';
  IID_IEllisCOMSearch: TGUID = '{7E4AC701-5956-4A6C-BD88-833B88B24E95}';
  IID_IEllisAccountSubSystem: TGUID = '{E6D0C486-6717-42BC-9E53-D77D8E31032F}';
  IID_IEllisCOMFilter: TGUID = '{D2F9AB09-51A9-4781-B88C-F164B066DE34}';
  IID_IEllisFisReg: TGUID = '{D812A3E4-106C-4B2A-A61E-B995F26ED6B6}';
  CLASS_EllisCOMServer: TGUID = '{6D15B140-8211-4FF9-99F9-439CF3CB9C0F}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IEllisCOMServer = interface;
  IEllisCOMSearch = interface;
  IEllisAccountSubSystem = interface;
  IEllisCOMFilter = interface;
  IEllisFisReg = interface;
  IEllisFisRegDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  EllisCOMServer = IEllisCOMServer;


// *********************************************************************//
// Interface: IEllisCOMServer
// Flags:     (0)
// GUID:      {5EAFE089-5889-4532-8E91-7E80B8FF2884}
// *********************************************************************//
  IEllisCOMServer = interface(IUnknown)
    ['{5EAFE089-5889-4532-8E91-7E80B8FF2884}']
    function _dlgInfo(const AMessage: WideString; Timer: Integer): HResult; stdcall;
    function _dlgError(const AMessage: WideString; Timer: Integer): HResult; stdcall;
    function _dlgQuery(const AMessage: WideString; Timer: Integer): HResult; stdcall;
    function _dlgQueryYN(const AMessage: WideString; Timer: Integer): HResult; stdcall;
    function _goAccount(Account: OleVariant): HResult; stdcall;
    function _frmCreated(ASelf: Integer): HResult; stdcall;
    function _frmDestoyed(AIndex: Integer): HResult; stdcall;
    function _frmActivated(AIndex: Integer): HResult; stdcall;
    function _frmSetCaption(AIndex: Integer): HResult; stdcall;
    function Get_ImageList: Integer; stdcall;
    function Get_Connection: Integer; stdcall;
    function Get_StyleRepository: Integer; stdcall;
    function Get_Application: Integer; stdcall;
    function Get_Screen: Integer; stdcall;
    function _DoFilter(AFilterObject: Integer): WideString; stdcall;
    function Get_HintWindowClass: Integer; stdcall;
    function Parameter_Changed(AOwner: Integer; Header: OleVariant; Old_Value: OleVariant; 
                               Parameter_Type: Integer; Dictionary: OleVariant; 
                               Dictionary_Key: OleVariant; Dictionary_Display: OleVariant; 
                               out New_Value: OleVariant; Default: OleVariant; ACon: Integer): Integer; stdcall;
    function RefreshAccountCard(ID_Account: Integer): HResult; stdcall;
    function SetNavigatorHints(AView: Integer; ARefresh: Integer): HResult; stdcall;
    function StartOperation: HResult; stdcall;
    function FinishOperation(const ADescr: WideString): HResult; stdcall;
    function Get_Options(const OptionName: WideString; out Value: OleVariant): HResult; stdcall;
    function Get_ApplicationHandle(out Value: Integer): HResult; stdcall;
    function Get_MainFormHandle(out Value: Integer): HResult; stdcall;
    function CreateTempTable(const APrefix: WideString; const ASuffix: WideString; 
                             const ATempPrefix: WideString; var ATempTableName: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IEllisCOMSearch
// Flags:     (0)
// GUID:      {7E4AC701-5956-4A6C-BD88-833B88B24E95}
// *********************************************************************//
  IEllisCOMSearch = interface(IUnknown)
    ['{7E4AC701-5956-4A6C-BD88-833B88B24E95}']
    function _SearchByRegistration(APeriod: Integer): Integer; stdcall;
    function _SearchByResidenceAccount(AID_Account: OleVariant): Integer; stdcall;
    function _SearchByRegistrationAccount(AID_Account: OleVariant): Integer; stdcall;
    function _Person_LocateByName(out ID_Person: Int64): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IEllisAccountSubSystem
// Flags:     (0)
// GUID:      {E6D0C486-6717-42BC-9E53-D77D8E31032F}
// *********************************************************************//
  IEllisAccountSubSystem = interface(IUnknown)
    ['{E6D0C486-6717-42BC-9E53-D77D8E31032F}']
    function GetParameterTable(Dataset: Integer): WideString; stdcall;
    function Prepare_Recalculation(Params: Integer): WideString; stdcall;
    function UnPrepare_Recalculation(const Parameter_Table_Name: WideString): HResult; stdcall;
    function MassAccount_Recalculation(const ARecalcProc: WideString; AOwner: Integer; 
                                       const Account_Source: WideString; Rule_Period: Integer; 
                                       Percent: Integer; Begin_Date: Integer; End_Date: Integer; 
                                       Hours: Integer; ApplyToServ: OleVariant; 
                                       const Service_List: WideString; Params: Integer; 
                                       const Recalculation_Parameter_Table_Name: WideString; 
                                       const Info: WideString; StartOnShow: Integer; Coef: Integer; 
                                       AMemData: Integer; ABenefitData: Integer): Integer; stdcall;
    function Recalculation_Apply(Recalc_Type: Integer; const Info: WideString; Apply_Data: Integer; 
                                 Parameters: Integer; BenefitData: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IEllisCOMFilter
// Flags:     (0)
// GUID:      {D2F9AB09-51A9-4781-B88C-F164B066DE34}
// *********************************************************************//
  IEllisCOMFilter = interface(IUnknown)
    ['{D2F9AB09-51A9-4781-B88C-F164B066DE34}']
    function _CreateFrame(AParent: Integer; AConnection: Integer; ALevel: Integer): Integer; stdcall;
    function _Apply(ALevel: Integer; AAddress: Integer; AParameters: Integer; AProvider: Integer; 
                    var ATempTableName: WideString): HResult; stdcall;
    function _ReleaseFrame(AFrame: Integer): HResult; stdcall;
    function _CreateParametersFrame(AParent: Integer; AConnection: Integer; ALevel: Integer): Integer; stdcall;
    function _CreateProviderFrame(AParent: Integer; AConnection: Integer): Integer; stdcall;
    function _CreateAddressTreeFrame(AParent: Integer; AConnection: Integer; ALevel: Integer): Integer; stdcall;
    function SaveFilterSettings(AAddress: Integer; AParameter: Integer; AProvider: Integer): HResult; stdcall;
    function LoadFilterSettings(AAddress: Integer; AParameter: Integer; AProvider: Integer): HResult; stdcall;
    function _CreateMeterFrame(AParent: Integer; AConnection: Integer; ALevel: Integer): Integer; stdcall;
    function _CreateBalanceFrame(AParent: Integer; AConnection: Integer; ALevel: Integer): Integer; stdcall;
    function _ApplyEx(var ATempTableName: WideString; ALevel: Integer; AFrames: OleVariant): HResult; stdcall;
    function LoadFilterSettings2(AFrames: OleVariant): HResult; stdcall;
    function SaveFilterSettings2(AFrames: OleVariant): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IEllisFisReg
// Flags:     (320) Dual OleAutomation
// GUID:      {D812A3E4-106C-4B2A-A61E-B995F26ED6B6}
// *********************************************************************//
  IEllisFisReg = interface(IUnknown)
    ['{D812A3E4-106C-4B2A-A61E-B995F26ED6B6}']
    procedure Process(ID_Bill: Int64; Advance: Double; out CanPay: SYSINT); safecall;
  end;

// *********************************************************************//
// DispIntf:  IEllisFisRegDisp
// Flags:     (320) Dual OleAutomation
// GUID:      {D812A3E4-106C-4B2A-A61E-B995F26ED6B6}
// *********************************************************************//
  IEllisFisRegDisp = dispinterface
    ['{D812A3E4-106C-4B2A-A61E-B995F26ED6B6}']
    procedure Process(ID_Bill: {??Int64}OleVariant; Advance: Double; out CanPay: SYSINT); dispid 101;
  end;

// *********************************************************************//
// The Class CoEllisCOMServer provides a Create and CreateRemote method to          
// create instances of the default interface IEllisCOMServer exposed by              
// the CoClass EllisCOMServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoEllisCOMServer = class
    class function Create: IEllisCOMServer;
    class function CreateRemote(const MachineName: string): IEllisCOMServer;
  end;

implementation

uses ComObj;

class function CoEllisCOMServer.Create: IEllisCOMServer;
begin
  Result := CreateComObject(CLASS_EllisCOMServer) as IEllisCOMServer;
end;

class function CoEllisCOMServer.CreateRemote(const MachineName: string): IEllisCOMServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_EllisCOMServer) as IEllisCOMServer;
end;

end.
