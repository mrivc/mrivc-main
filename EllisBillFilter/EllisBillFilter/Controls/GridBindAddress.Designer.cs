﻿namespace EllisBillFilter.Controls
{
    partial class GridBindAddress
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.baseGrid1 = new EllisBillFilter.BaseGrid();
            this.gridBindAddressView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupBoxBindSettings = new System.Windows.Forms.GroupBox();
            this.panelBindByAccountSettings = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.tableColumnsDestinationPicker1 = new MrivcControls.DataPickers.TableColumnsPicker();
            this.tableColumnsSourcePicker1 = new MrivcControls.DataPickers.TableColumnsPicker();
            this.label10 = new System.Windows.Forms.Label();
            this.panelBindByAddressSettings = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtStreetSeparator = new System.Windows.Forms.TextBox();
            this.txtRoomSeparator = new System.Windows.Forms.TextBox();
            this.txtHouseSeparator = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFlatSeparator = new System.Windows.Forms.TextBox();
            this.txtBuildingSeparator = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBlockSeparator = new System.Windows.Forms.TextBox();
            this.cbBindAccountType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAccount = new System.Windows.Forms.TextBox();
            this.btnCheckAddress = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dabaBaseTablePicker1 = new MrivcControls.DataPickers.DabaBaseTablePicker();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBindAddressView)).BeginInit();
            this.groupBoxBindSettings.SuspendLayout();
            this.panelBindByAccountSettings.SuspendLayout();
            this.panelBindByAddressSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // baseGrid1
            // 
            this.baseGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.baseGrid1.Location = new System.Drawing.Point(0, 152);
            this.baseGrid1.MainView = this.gridBindAddressView;
            this.baseGrid1.Name = "baseGrid1";
            this.baseGrid1.Size = new System.Drawing.Size(1044, 386);
            this.baseGrid1.TabIndex = 0;
            this.baseGrid1.TrimCellValueBeforeValidate = true;
            this.baseGrid1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridBindAddressView});
            // 
            // gridBindAddressView
            // 
            this.gridBindAddressView.GridControl = this.baseGrid1;
            this.gridBindAddressView.Name = "gridBindAddressView";
            this.gridBindAddressView.OptionsView.ColumnAutoWidth = false;
            this.gridBindAddressView.OptionsView.ShowGroupPanel = false;
            // 
            // groupBoxBindSettings
            // 
            this.groupBoxBindSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxBindSettings.Controls.Add(this.panelBindByAccountSettings);
            this.groupBoxBindSettings.Controls.Add(this.panelBindByAddressSettings);
            this.groupBoxBindSettings.Location = new System.Drawing.Point(0, 33);
            this.groupBoxBindSettings.Name = "groupBoxBindSettings";
            this.groupBoxBindSettings.Size = new System.Drawing.Size(1044, 81);
            this.groupBoxBindSettings.TabIndex = 1;
            this.groupBoxBindSettings.TabStop = false;
            this.groupBoxBindSettings.Text = "Настройки соединение адресов";
            // 
            // panelBindByAccountSettings
            // 
            this.panelBindByAccountSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBindByAccountSettings.Controls.Add(this.label11);
            this.panelBindByAccountSettings.Controls.Add(this.tableColumnsDestinationPicker1);
            this.panelBindByAccountSettings.Controls.Add(this.tableColumnsSourcePicker1);
            this.panelBindByAccountSettings.Controls.Add(this.label10);
            this.panelBindByAccountSettings.Location = new System.Drawing.Point(707, 11);
            this.panelBindByAccountSettings.Name = "panelBindByAccountSettings";
            this.panelBindByAccountSettings.Size = new System.Drawing.Size(331, 65);
            this.panelBindByAccountSettings.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(144, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "со столбцом (dbo.Account):";
            // 
            // tableColumnsDestinationPicker1
            // 
            this.tableColumnsDestinationPicker1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableColumnsDestinationPicker1.Location = new System.Drawing.Point(153, 40);
            this.tableColumnsDestinationPicker1.Name = "tableColumnsDestinationPicker1";
            this.tableColumnsDestinationPicker1.SelectedColumn = "";
            this.tableColumnsDestinationPicker1.Size = new System.Drawing.Size(172, 21);
            this.tableColumnsDestinationPicker1.TabIndex = 2;
            // 
            // tableColumnsSourcePicker1
            // 
            this.tableColumnsSourcePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableColumnsSourcePicker1.Location = new System.Drawing.Point(153, 14);
            this.tableColumnsSourcePicker1.Name = "tableColumnsSourcePicker1";
            this.tableColumnsSourcePicker1.SelectedColumn = "";
            this.tableColumnsSourcePicker1.Size = new System.Drawing.Size(172, 21);
            this.tableColumnsSourcePicker1.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(3, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Соединить столбец ЛС:";
            // 
            // panelBindByAddressSettings
            // 
            this.panelBindByAddressSettings.Controls.Add(this.label2);
            this.panelBindByAddressSettings.Controls.Add(this.label6);
            this.panelBindByAddressSettings.Controls.Add(this.txtStreetSeparator);
            this.panelBindByAddressSettings.Controls.Add(this.txtRoomSeparator);
            this.panelBindByAddressSettings.Controls.Add(this.txtHouseSeparator);
            this.panelBindByAddressSettings.Controls.Add(this.label7);
            this.panelBindByAddressSettings.Controls.Add(this.label3);
            this.panelBindByAddressSettings.Controls.Add(this.txtFlatSeparator);
            this.panelBindByAddressSettings.Controls.Add(this.txtBuildingSeparator);
            this.panelBindByAddressSettings.Controls.Add(this.label4);
            this.panelBindByAddressSettings.Controls.Add(this.label5);
            this.panelBindByAddressSettings.Controls.Add(this.txtBlockSeparator);
            this.panelBindByAddressSettings.Location = new System.Drawing.Point(1, 11);
            this.panelBindByAddressSettings.Name = "panelBindByAddressSettings";
            this.panelBindByAddressSettings.Size = new System.Drawing.Size(700, 65);
            this.panelBindByAddressSettings.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Разделитель улицы:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(463, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Разделитель комнаты:";
            // 
            // txtStreetSeparator
            // 
            this.txtStreetSeparator.Location = new System.Drawing.Point(123, 14);
            this.txtStreetSeparator.Name = "txtStreetSeparator";
            this.txtStreetSeparator.Size = new System.Drawing.Size(100, 20);
            this.txtStreetSeparator.TabIndex = 0;
            this.txtStreetSeparator.Text = ",";
            // 
            // txtRoomSeparator
            // 
            this.txtRoomSeparator.Location = new System.Drawing.Point(592, 40);
            this.txtRoomSeparator.Name = "txtRoomSeparator";
            this.txtRoomSeparator.Size = new System.Drawing.Size(101, 20);
            this.txtRoomSeparator.TabIndex = 12;
            // 
            // txtHouseSeparator
            // 
            this.txtHouseSeparator.Location = new System.Drawing.Point(123, 40);
            this.txtHouseSeparator.Name = "txtHouseSeparator";
            this.txtHouseSeparator.Size = new System.Drawing.Size(100, 20);
            this.txtHouseSeparator.TabIndex = 4;
            this.txtHouseSeparator.Text = ",";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(463, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Разделитель квартиры:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Разделитель дома:";
            // 
            // txtFlatSeparator
            // 
            this.txtFlatSeparator.Location = new System.Drawing.Point(593, 14);
            this.txtFlatSeparator.Name = "txtFlatSeparator";
            this.txtFlatSeparator.Size = new System.Drawing.Size(100, 20);
            this.txtFlatSeparator.TabIndex = 10;
            this.txtFlatSeparator.Text = ",";
            // 
            // txtBuildingSeparator
            // 
            this.txtBuildingSeparator.Location = new System.Drawing.Point(385, 14);
            this.txtBuildingSeparator.Name = "txtBuildingSeparator";
            this.txtBuildingSeparator.Size = new System.Drawing.Size(73, 20);
            this.txtBuildingSeparator.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(229, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Разделитель блока (\"к.\"):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(229, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Разделитель строения (\"/\"):";
            // 
            // txtBlockSeparator
            // 
            this.txtBlockSeparator.Location = new System.Drawing.Point(385, 40);
            this.txtBlockSeparator.Name = "txtBlockSeparator";
            this.txtBlockSeparator.Size = new System.Drawing.Size(72, 20);
            this.txtBlockSeparator.TabIndex = 8;
            this.txtBlockSeparator.Text = "к.";
            // 
            // cbBindAccountType
            // 
            this.cbBindAccountType.FormattingEnabled = true;
            this.cbBindAccountType.Items.AddRange(new object[] {
            "по адресу",
            "по адресу (учитывать собственника)",
            "по лицевому счету"});
            this.cbBindAccountType.Location = new System.Drawing.Point(467, 7);
            this.cbBindAccountType.Name = "cbBindAccountType";
            this.cbBindAccountType.Size = new System.Drawing.Size(227, 21);
            this.cbBindAccountType.TabIndex = 15;
            this.cbBindAccountType.SelectedIndexChanged += new System.EventHandler(this.cbBindAccountType_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(207, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Проверить составленный адрес по ЛС:";
            // 
            // txtAccount
            // 
            this.txtAccount.Location = new System.Drawing.Point(215, 126);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(129, 20);
            this.txtAccount.TabIndex = 3;
            this.txtAccount.Text = "000401002857";
            // 
            // btnCheckAddress
            // 
            this.btnCheckAddress.Location = new System.Drawing.Point(353, 124);
            this.btnCheckAddress.Name = "btnCheckAddress";
            this.btnCheckAddress.Size = new System.Drawing.Size(136, 23);
            this.btnCheckAddress.TabIndex = 4;
            this.btnCheckAddress.Text = "проверить адрес...";
            this.btnCheckAddress.UseVisualStyleBackColor = true;
            this.btnCheckAddress.Click += new System.EventHandler(this.btnCheckAddress_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(308, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Тип соединения адреса:";
            // 
            // dabaBaseTablePicker1
            // 
            this.dabaBaseTablePicker1.Location = new System.Drawing.Point(102, 7);
            this.dabaBaseTablePicker1.Name = "dabaBaseTablePicker1";
            this.dabaBaseTablePicker1.SelectedTable = "";
            this.dabaBaseTablePicker1.Size = new System.Drawing.Size(193, 21);
            this.dabaBaseTablePicker1.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Таблица данных:";
            // 
            // GridBindAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dabaBaseTablePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCheckAddress);
            this.Controls.Add(this.cbBindAccountType);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBoxBindSettings);
            this.Controls.Add(this.baseGrid1);
            this.Name = "GridBindAddress";
            this.Size = new System.Drawing.Size(1044, 538);
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBindAddressView)).EndInit();
            this.groupBoxBindSettings.ResumeLayout(false);
            this.panelBindByAccountSettings.ResumeLayout(false);
            this.panelBindByAccountSettings.PerformLayout();
            this.panelBindByAddressSettings.ResumeLayout(false);
            this.panelBindByAddressSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseGrid baseGrid1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridBindAddressView;
        private System.Windows.Forms.GroupBox groupBoxBindSettings;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRoomSeparator;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFlatSeparator;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBlockSeparator;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBuildingSeparator;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtHouseSeparator;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtStreetSeparator;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAccount;
        private System.Windows.Forms.Button btnCheckAddress;
        private System.Windows.Forms.ComboBox cbBindAccountType;
        private System.Windows.Forms.Label label1;
        private MrivcControls.DataPickers.DabaBaseTablePicker dabaBaseTablePicker1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelBindByAddressSettings;
        private System.Windows.Forms.Panel panelBindByAccountSettings;
        private System.Windows.Forms.Label label10;
        private MrivcControls.DataPickers.TableColumnsPicker tableColumnsSourcePicker1;
        private System.Windows.Forms.Label label11;
        private MrivcControls.DataPickers.TableColumnsPicker tableColumnsDestinationPicker1;
    }
}
