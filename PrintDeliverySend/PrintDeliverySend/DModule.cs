﻿using System.ComponentModel;
using System.Text;

namespace PrintDeliverySend
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;

    public partial class DModule : Component
    {
        private static DModule instance;

        public DModule()
        {
            InitializeComponent();
            sqlMainConnection.ConnectionString = Properties.Settings.Default.MainConnection;
        }

        public SqlConnection MainConnection
        {
            get
            {
                return sqlMainConnection;
            }
        }

        public static DModule Instance
        {
            get
            {
                if (instance == null)
                    instance = new DModule();
                return instance;
            }
        }

        public int WorkPeriod()
        {
            return (int)ExecuteScalarCommand("select dbo.Work_Period()");
        }

        public string GetPeriodMY(int period)
        {
            return
                ExecuteScalarCommand(
                    "select right('0000000000'+convert(varchar,[month]),2)+substring(convert(varchar,[year]),3,2) as Period from period where period="
                    + period).ToString();
        }

        public string GetPeriodName(int period)
        {
            return
                ExecuteScalarCommand(
                    "select (Month_Name + ' ' + convert(varchar,[Year])) Period from period where period="
                    + period).ToString();
        }

        public int GetPeriodYear(int period)
        {
            string val = ExecuteScalarCommand(
                    "select [Year] from period where period="
                    + period).ToString();
            return Convert.ToInt32(val);
        }

        public int GetPeriodMonth(int period)
        {
            string val = ExecuteScalarCommand(
                    "select [Month] from period where period="
                    + period).ToString();
            return Convert.ToInt32(val);
        }

        public string GetBillStdReport()
        {
            byte[] module = (byte[])this.LoadBillStdTemplate();
            return Encoding.UTF8.GetString(module);
        }

        private object LoadBillStdTemplate()
        {
            return ExecuteScalarQueryCommand("select convert(varbinary(max),[File]) from dbo.Module where name like '%_BillStd_Full%'");
        }

        public void ExecuteNonQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.ExecuteNonQuery();
        }

        public DataTable GetPrintDeliveryData()
        {
            DataTable dt = new DataTable();
            daPrintDeliveryData.Fill(dt);
            return dt;
        }

        public void InsertPrintDeliveryInfo(long idPrint, int period, int idAccount, int idClientDetail, string email, string fileName, int success, string error)
        {
            string sql = string.Format(
                "exec [Reports].[Print_Delivery_Bill_Insert] @ID_Print={0}, @Period={1}, @ID_Account={2}, @ID_Client_Detail={3}, @Email='{4}', @File_Name='{5}', @Send_Success={6}, @Error='{7}'",
                idPrint, period, idAccount, idClientDetail, email, fileName, success, error);
            this.ExecuteNonQuery(sql);
        }

        public object ExecuteNonQuery(string sql)
        {
            cmdAnyCommand.Connection.Close();
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.Connection.Open();
            return cmdAnyCommand.ExecuteNonQuery();
        }

        public object ExecuteScalarCommand(string sql)
        {
            cmdAnyCommand.Connection.Close();
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.Connection.Open();
            return cmdAnyCommand.ExecuteScalar();
        }

        public object ExecuteScalarQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.Connection.Close();
            cmdAnyCommand.Connection.Open();
            object res = cmdAnyCommand.ExecuteScalar();
            return res;
        }
    }
}

