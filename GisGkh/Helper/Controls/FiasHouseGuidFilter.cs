﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Helper.Data;

namespace Helper.Controls
{
    public partial class FiasHouseGuidFilter : UserControl
    {
        public string SenderID { get; set; }

        public bool All
        {
            get { return cbAllHouse.Checked; }
        }

        public string FiasHouseGuid
        {
            get { return cbHouse.SelectedValue.ToString(); }
        }

        public FiasHouseGuidFilter()
        {
            InitializeComponent();
        }

        public void Fill()
        {
            cbHouse.DataSource = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.SelectHouseBySenderID", new object[,] { { "senderID", SenderID } });
            cbHouse.ValueMember = "FiasHouseGuid";
            cbHouse.DisplayMember = "House_Address";
        }

        private void cbAllHouse_CheckedChanged(object sender, EventArgs e)
        {
            cbHouse.Enabled = !cbAllHouse.Checked;
        }

        private void gbHouse_Enter(object sender, EventArgs e)
        {

        }

        private void cbHouse_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
