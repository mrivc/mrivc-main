﻿namespace Payments
{
    partial class FrmPayCapitalRepair
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelPayment1 = new Payments.Controls.PanelPayment();
            this.label1 = new System.Windows.Forms.Label();
            this.gridPaymentParameter1 = new Payments.Controls.GridPaymentParameter();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBarcode
            // 
            this.txtBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBarcode.Location = new System.Drawing.Point(72, 13);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(561, 20);
            this.txtBarcode.TabIndex = 0;
            this.txtBarcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panelPayment1);
            this.groupBox1.Location = new System.Drawing.Point(5, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(632, 211);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // panelPayment1
            // 
            this.panelPayment1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPayment1.CashType = 1;
            this.panelPayment1.Location = new System.Drawing.Point(5, 12);
            this.panelPayment1.Name = "panelPayment1";
            this.panelPayment1.PaymentCash = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.panelPayment1.PaymentComment = "";
            this.panelPayment1.PaymentDate = new System.DateTime(2017, 3, 16, 9, 31, 21, 0);
            this.panelPayment1.PaymentSumm = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.panelPayment1.Size = new System.Drawing.Size(621, 194);
            this.panelPayment1.TabIndex = 0;
            this.panelPayment1.PayEvent += new Payments.Controls.PanelPayment.PayEventHandler(this.panelPayment1_PayEvent);
            this.panelPayment1.PaymentSummChangedEvent += new Payments.Controls.PanelPayment.PaymentSummEventHandler(this.panelPayment1_PaymentSummChangedEvent);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Штрих-код:";
            // 
            // gridPaymentParameter1
            // 
            this.gridPaymentParameter1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPaymentParameter1.Location = new System.Drawing.Point(5, 255);
            this.gridPaymentParameter1.Name = "gridPaymentParameter1";
            this.gridPaymentParameter1.Size = new System.Drawing.Size(632, 265);
            this.gridPaymentParameter1.TabIndex = 1;
            // 
            // FrmPayCapitalRepair
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 522);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.gridPaymentParameter1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(658, 560);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(658, 560);
            this.Name = "FrmPayCapitalRepair";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оплата квитанции";
            this.Shown += new System.EventHandler(this.FrmPayCapitalRepair_Shown);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.PanelPayment panelPayment1;
        private Controls.GridPaymentParameter gridPaymentParameter1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
    }
}