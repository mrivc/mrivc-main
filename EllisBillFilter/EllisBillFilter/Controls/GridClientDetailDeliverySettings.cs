﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;

    using DevExpress.XtraEditors;

    using EllisBillFilter.DS;
    using EllisBillFilter.Helpers;
    using EllisBillFilter.Logic;
    using EllisBillFilter.Properties;

    using FastReport.Map;

    public partial class GridClientDetailDeliverySettings : UserControl
    {
        public GridClientDetailDeliverySettings()
        {
            InitializeComponent();
            txtPath.Text = Properties.Settings.Default.SberSaldoPath;
        }

        public void LoadData()
        {
            mDataSet.Clear();
            mDataSet.Client.Merge(DAccess.Instance.PayReceiverCompany);
            gridClientDetailDeliverySettings1.InitEx();

            MDataSet.BillFilterRow r1 = DAccess.Instance.BillFilter.NewBillFilterRow();
            r1.ID_Bill_Filter = -1;
            r1.Bill_Filter = "Все";
            r1.Tag = -1;
            r1.Info = "Все";
            r1.Code = -1;

            if(!DAccess.Instance.BillFilter.Any(r => r.ID_Bill_Filter == -1))
                DAccess.Instance.BillFilter.Rows.InsertAt(r1, 0);
            mDataSet.BillFilter.Merge(DAccess.Instance.BillFilter);

            mDataSet.Period.Merge(DAccess.Instance.Periods);
            cbPeriod.SelectedValue = DAccess.Instance.WorkPeriod;

            DAccess.DataModule.ClientDetailDeliverySettingsSelect(mDataSet.ClientDetailDeliverySettings, DAccess.Instance.WorkPeriod, false);
        }

        private void miAddRecord_Click(object sender, EventArgs e)
        {
            gridClientDetailDeliverySettings1.AddNewRow();
        }

        private void miDeleteRecord_Click(object sender, EventArgs e)
        {
            gridClientDetailDeliverySettings1.DeleteActiveRow(true);
        }

        private void gridClientDetailDeliverySettings1_SaveData()
        {
            try
            {
                DAccess.DataModule.ClientDetailDeliverySettingsUpdate(mDataSet.ClientDetailDeliverySettings);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void riBtnSelectEmails_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRow row = gridViewClientDetailDeliverySettings1.GetDataRow(gridViewClientDetailDeliverySettings1.FocusedRowHandle);

            ButtonEdit be = sender as ButtonEdit;
            var f = new FrmClientDetailDeliveryEmail(row["Email_IDs"] == DBNull.Value ? string.Empty : row["Email_IDs"].ToString());
            if (f.ShowDialog(this) == DialogResult.OK && (gridViewClientDetailDeliverySettings1.FocusedRowHandle >= 0 || gridViewClientDetailDeliverySettings1.IsNewItemRow(gridViewClientDetailDeliverySettings1.FocusedRowHandle)))
            {
                row["EmailsAddresses"] = f.Emails;
                row["EmailsText"] = f.EmailNames;
                row["Email_IDs"] = f.EmailIDs;
                be.Text = f.EmailNames;
                gridClientDetailDeliverySettings1.AcceptChanges();
            }
        }

        private void gridViewClientDetailDeliverySettings1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DataRow row = gridViewClientDetailDeliverySettings1.GetDataRow(e.RowHandle);
            row["Checked"] = false;
            row["SendToday"] = false;
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void miSendToday_Click(object sender, EventArgs e)
        {
            DataRow row = gridViewClientDetailDeliverySettings1.GetDataRow(gridViewClientDetailDeliverySettings1.FocusedRowHandle);
            row["SendToday"] = !bool.Parse(row["SendToday"].ToString());
            gridClientDetailDeliverySettings1.AcceptChanges();
        }

        private void gridViewClientDetailDeliverySettings1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DataRow row = gridViewClientDetailDeliverySettings1.GetDataRow(e.RowHandle);
           
            if(row == null)
                return;

            if (bool.Parse(row["SendToday"].ToString()) == true)
            {
                e.Appearance.BackColor = Color.LightGreen;
            }
            else
            {
                e.Appearance.BackColor = Color.White;
            }
        }

        private void btnGenerateAndSend_Click(object sender, EventArgs e)
        {

            int period = (int)cbPeriod.SelectedValue;

            if (mDataSet.ClientDetailDeliverySettings.Where(r => r.Checked == true).Count() == 0)
            {
                MessageBox.Show(@"Не выбрано ни одного клиента.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.ShowMailSettingsForm(period) == DialogResult.OK)
            {
                progressBar1.Value = 10;
                btnGenerateAndSend.Enabled = false;
                this.Cursor = Cursors.WaitCursor;
                backgroundWorker1.RunWorkerAsync(period);
            }
        }

        private DialogResult ShowMailSettingsForm(int period)
        {
            DataRow periodInfo = DAccess.Instance.GetPeriodInfo(period);
            int periodYear = (int)periodInfo["Year"];
            int periodMonth = (int)periodInfo["Month"];
            string periodName = periodInfo["Name"].ToString();

            string emails =
                mDataSet.ClientDetailDeliverySettings.Where(r => r.Checked == true)
                    .Select(c => c.EmailsText)
                    .Aggregate((i, j) => i + ";" + j);

            string mailSubject = Settings.Default.MailSubject.Replace("@Period@", periodName)
                .Replace("@CurrentDate@", string.Format("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString()));

            FrmMailSimple f = new FrmMailSimple(emails, Settings.Default.SendCopyTo, mailSubject, Settings.Default.MailBody.Replace("@Period@", periodName));
            return f.ShowDialog();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            EnumerableRowCollection<MDataSet.ClientDetailDeliverySettingsRow> clients = mDataSet.ClientDetailDeliverySettings.Where(r => r.Checked == true);
            int progress = 10;
            int progressStep = (90 / clients.Count());

            foreach (var row in clients)
            {
                progress += progressStep;

                string path = Properties.Settings.Default.SberSaldoPath;

                ReestrGenerator.GenerateAndSend((int)e.Argument, path, row.ID_Client, row.ID_Bill_Filter, row.EmailsAddresses.Replace(" ", ""), Settings.Default.IsSendReestrViaEmail, row.Is_Debt);
                // Thread.Sleep(1000);

                backgroundWorker1.ReportProgress(progress);

                DAccess.DataModule.SberReestrLog(row.ID_Delivery, DateTime.Now, DAccess.Instance.UserId);
                row.Last_Send = DateTime.Now;
                row.Last_Send_By = DAccess.Instance.UserId;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnGenerateAndSend.Enabled = true;
            progressBar1.Value = 100;
            this.Cursor = Cursors.Default;
            MessageBox.Show("Реестры сформированы и отосланы успешно.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            Process.Start("notepad.exe", Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "NLog.txt"));
        }
    }
}
