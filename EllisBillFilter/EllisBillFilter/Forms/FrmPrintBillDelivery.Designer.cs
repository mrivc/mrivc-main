﻿namespace EllisBillFilter.Forms
{
    partial class FrmPrintBillDelivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrintBillDelivery));
            this.gridPrintDeliveryBuild = new EllisBillFilter.BaseGrid();
            this.printDeliveryBillBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gvPrintDeliveryBill = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Print = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Account = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Client_Detail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSend_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFile_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSend_Success = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colError = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBill_Filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riPayCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.payReceiverCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Derive_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riDeriveCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.deriveCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridPrintDeliveryBuild)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printDeliveryBillBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrintDeliveryBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPayCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeriveCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridPrintDeliveryBuild
            // 
            this.gridPrintDeliveryBuild.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPrintDeliveryBuild.DataSource = this.printDeliveryBillBindingSource;
            this.gridPrintDeliveryBuild.Location = new System.Drawing.Point(0, 35);
            this.gridPrintDeliveryBuild.MainView = this.gvPrintDeliveryBill;
            this.gridPrintDeliveryBuild.Name = "gridPrintDeliveryBuild";
            this.gridPrintDeliveryBuild.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riPeriod,
            this.riPayCompany,
            this.riDeriveCompany});
            this.gridPrintDeliveryBuild.Size = new System.Drawing.Size(1068, 524);
            this.gridPrintDeliveryBuild.TabIndex = 0;
            this.gridPrintDeliveryBuild.TrimCellValueBeforeValidate = true;
            this.gridPrintDeliveryBuild.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPrintDeliveryBill});
            // 
            // printDeliveryBillBindingSource
            // 
            this.printDeliveryBillBindingSource.DataMember = "PrintDeliveryBill";
            this.printDeliveryBillBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gvPrintDeliveryBill
            // 
            this.gvPrintDeliveryBill.ColumnPanelRowHeight = 40;
            this.gvPrintDeliveryBill.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Print,
            this.colPeriod,
            this.colID_Account,
            this.colID_Client_Detail,
            this.colEmail,
            this.colSend_Date,
            this.colFile_Name,
            this.colSend_Success,
            this.colError,
            this.colBill_Filter,
            this.colID_Client,
            this.colID_Derive_Company});
            this.gvPrintDeliveryBill.GridControl = this.gridPrintDeliveryBuild;
            this.gvPrintDeliveryBill.Name = "gvPrintDeliveryBill";
            this.gvPrintDeliveryBill.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSend_Date, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gvPrintDeliveryBill.DoubleClick += new System.EventHandler(this.gvPrintDeliveryBill_DoubleClick);
            // 
            // colID_Print
            // 
            this.colID_Print.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Print.AppearanceCell.Options.UseBackColor = true;
            this.colID_Print.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Print.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID_Print.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Print.FieldName = "ID_Print";
            this.colID_Print.Name = "colID_Print";
            this.colID_Print.OptionsColumn.AllowEdit = false;
            this.colID_Print.OptionsColumn.ReadOnly = true;
            this.colID_Print.Width = 63;
            // 
            // colPeriod
            // 
            this.colPeriod.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colPeriod.AppearanceCell.Options.UseBackColor = true;
            this.colPeriod.AppearanceHeader.Options.UseTextOptions = true;
            this.colPeriod.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPeriod.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPeriod.Caption = "Период";
            this.colPeriod.ColumnEdit = this.riPeriod;
            this.colPeriod.FieldName = "Period";
            this.colPeriod.Name = "colPeriod";
            this.colPeriod.OptionsColumn.AllowEdit = false;
            this.colPeriod.OptionsColumn.ReadOnly = true;
            this.colPeriod.Visible = true;
            this.colPeriod.VisibleIndex = 0;
            this.colPeriod.Width = 102;
            // 
            // riPeriod
            // 
            this.riPeriod.AutoHeight = false;
            this.riPeriod.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riPeriod.DataSource = this.periodBindingSource;
            this.riPeriod.DisplayMember = "Name";
            this.riPeriod.Name = "riPeriod";
            this.riPeriod.ValueMember = "Period";
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Account
            // 
            this.colID_Account.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Account.AppearanceCell.Options.UseBackColor = true;
            this.colID_Account.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Account.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID_Account.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Account.FieldName = "ID_Account";
            this.colID_Account.Name = "colID_Account";
            this.colID_Account.OptionsColumn.AllowEdit = false;
            this.colID_Account.OptionsColumn.ReadOnly = true;
            this.colID_Account.Width = 58;
            // 
            // colID_Client_Detail
            // 
            this.colID_Client_Detail.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Client_Detail.AppearanceCell.Options.UseBackColor = true;
            this.colID_Client_Detail.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Client_Detail.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID_Client_Detail.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Client_Detail.FieldName = "ID_Client_Detail";
            this.colID_Client_Detail.Name = "colID_Client_Detail";
            this.colID_Client_Detail.OptionsColumn.AllowEdit = false;
            this.colID_Client_Detail.OptionsColumn.ReadOnly = true;
            this.colID_Client_Detail.Width = 58;
            // 
            // colEmail
            // 
            this.colEmail.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colEmail.AppearanceCell.Options.UseBackColor = true;
            this.colEmail.AppearanceHeader.Options.UseTextOptions = true;
            this.colEmail.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEmail.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEmail.Caption = "E-mail";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowEdit = false;
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 3;
            this.colEmail.Width = 58;
            // 
            // colSend_Date
            // 
            this.colSend_Date.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colSend_Date.AppearanceCell.Options.UseBackColor = true;
            this.colSend_Date.AppearanceHeader.Options.UseTextOptions = true;
            this.colSend_Date.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSend_Date.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSend_Date.Caption = "Дата отправки";
            this.colSend_Date.FieldName = "Send_Date";
            this.colSend_Date.Name = "colSend_Date";
            this.colSend_Date.OptionsColumn.AllowEdit = false;
            this.colSend_Date.OptionsColumn.ReadOnly = true;
            this.colSend_Date.Visible = true;
            this.colSend_Date.VisibleIndex = 5;
            this.colSend_Date.Width = 58;
            // 
            // colFile_Name
            // 
            this.colFile_Name.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colFile_Name.AppearanceCell.Options.UseBackColor = true;
            this.colFile_Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colFile_Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFile_Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFile_Name.Caption = "Файл";
            this.colFile_Name.FieldName = "File_Name";
            this.colFile_Name.Name = "colFile_Name";
            this.colFile_Name.OptionsColumn.AllowEdit = false;
            this.colFile_Name.OptionsColumn.ReadOnly = true;
            this.colFile_Name.Visible = true;
            this.colFile_Name.VisibleIndex = 4;
            this.colFile_Name.Width = 58;
            // 
            // colSend_Success
            // 
            this.colSend_Success.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colSend_Success.AppearanceCell.Options.UseBackColor = true;
            this.colSend_Success.AppearanceHeader.Options.UseTextOptions = true;
            this.colSend_Success.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSend_Success.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSend_Success.Caption = "Доставлено";
            this.colSend_Success.FieldName = "Send_Success";
            this.colSend_Success.Name = "colSend_Success";
            this.colSend_Success.OptionsColumn.AllowEdit = false;
            this.colSend_Success.OptionsColumn.ReadOnly = true;
            this.colSend_Success.Visible = true;
            this.colSend_Success.VisibleIndex = 6;
            this.colSend_Success.Width = 58;
            // 
            // colError
            // 
            this.colError.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colError.AppearanceCell.Options.UseBackColor = true;
            this.colError.AppearanceHeader.Options.UseTextOptions = true;
            this.colError.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colError.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colError.Caption = "Ошибка отправки";
            this.colError.FieldName = "Error";
            this.colError.Name = "colError";
            this.colError.OptionsColumn.AllowEdit = false;
            this.colError.OptionsColumn.ReadOnly = true;
            this.colError.Width = 58;
            // 
            // colBill_Filter
            // 
            this.colBill_Filter.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colBill_Filter.AppearanceCell.Options.UseBackColor = true;
            this.colBill_Filter.AppearanceHeader.Options.UseTextOptions = true;
            this.colBill_Filter.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBill_Filter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBill_Filter.Caption = "Квитанция";
            this.colBill_Filter.FieldName = "Bill_Filter";
            this.colBill_Filter.Name = "colBill_Filter";
            this.colBill_Filter.OptionsColumn.AllowEdit = false;
            this.colBill_Filter.OptionsColumn.ReadOnly = true;
            this.colBill_Filter.Visible = true;
            this.colBill_Filter.VisibleIndex = 2;
            this.colBill_Filter.Width = 59;
            // 
            // colID_Client
            // 
            this.colID_Client.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colID_Client.AppearanceCell.Options.UseBackColor = true;
            this.colID_Client.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Client.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID_Client.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Client.Caption = "Расчетный счет";
            this.colID_Client.ColumnEdit = this.riPayCompany;
            this.colID_Client.FieldName = "ID_Client";
            this.colID_Client.Name = "colID_Client";
            this.colID_Client.OptionsColumn.AllowEdit = false;
            this.colID_Client.OptionsColumn.ReadOnly = true;
            // 
            // riPayCompany
            // 
            this.riPayCompany.AutoHeight = false;
            this.riPayCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riPayCompany.DataSource = this.payReceiverCompanyBindingSource;
            this.riPayCompany.DisplayMember = "Name";
            this.riPayCompany.Name = "riPayCompany";
            this.riPayCompany.ValueMember = "ID_Company";
            // 
            // payReceiverCompanyBindingSource
            // 
            this.payReceiverCompanyBindingSource.DataMember = "PayReceiverCompany";
            this.payReceiverCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Derive_Company
            // 
            this.colID_Derive_Company.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colID_Derive_Company.AppearanceCell.Options.UseBackColor = true;
            this.colID_Derive_Company.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Derive_Company.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID_Derive_Company.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Derive_Company.Caption = "Управляющая компания";
            this.colID_Derive_Company.ColumnEdit = this.riDeriveCompany;
            this.colID_Derive_Company.FieldName = "ID_Derive_Company";
            this.colID_Derive_Company.Name = "colID_Derive_Company";
            this.colID_Derive_Company.OptionsColumn.AllowEdit = false;
            this.colID_Derive_Company.OptionsColumn.ReadOnly = true;
            this.colID_Derive_Company.Visible = true;
            this.colID_Derive_Company.VisibleIndex = 1;
            // 
            // riDeriveCompany
            // 
            this.riDeriveCompany.AutoHeight = false;
            this.riDeriveCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riDeriveCompany.DataSource = this.deriveCompanyBindingSource;
            this.riDeriveCompany.DisplayMember = "Name";
            this.riDeriveCompany.Name = "riDeriveCompany";
            this.riDeriveCompany.ValueMember = "ID_Company";
            // 
            // deriveCompanyBindingSource
            // 
            this.deriveCompanyBindingSource.DataMember = "DeriveCompany";
            this.deriveCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(609, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Для того  чтобы открыть нужную квитанцию два раза нажмите по соответствующей запи" +
    "си в таблице.";
            // 
            // FrmPrintBillDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gridPrintDeliveryBuild);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrintBillDelivery";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Квитанции отправленные на E-mail";
            this.Load += new System.EventHandler(this.FrmPrintBillDelivery_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridPrintDeliveryBuild)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printDeliveryBillBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrintDeliveryBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPayCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeriveCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseGrid gridPrintDeliveryBuild;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPrintDeliveryBill;
        private System.Windows.Forms.BindingSource printDeliveryBillBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Print;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Account;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client_Detail;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colSend_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colFile_Name;
        private DevExpress.XtraGrid.Columns.GridColumn colSend_Success;
        private DevExpress.XtraGrid.Columns.GridColumn colError;
        private DevExpress.XtraGrid.Columns.GridColumn colBill_Filter;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riPayCompany;
        private System.Windows.Forms.BindingSource payReceiverCompanyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Derive_Company;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riDeriveCompany;
        private System.Windows.Forms.BindingSource deriveCompanyBindingSource;
        private System.Windows.Forms.Label label1;
    }
}