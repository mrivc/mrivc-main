﻿using System;
using System.Windows.Forms;
using MrivcControls.Data;
using DevExpress.XtraTreeList;
using DevExpress.XtraEditors;
using System.Drawing;
using DevExpress.XtraTreeList.Nodes;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MrivcControls
{
    public partial class AddressFilter : UserControl
    {
        EllisFilterSettings ellisFilterSettings;
        TreeListHitInfo dragStartHitInfo;

        string connectionString;
        public string ConnectionString
        {
            get { return connectionString; }
            set
            {
                connectionString = value;
                //DAccess.DataModule.SetConnection(connectionString);
            }
        }

        public void AddAddress(int id, int type, string caption)
        {
            ellisFilterSettings.AddAddress(id, type, caption);
            SetAddressListDataSource();
        }

        public string CreateHouseQuery()
        {
            if (ellisFilterSettings.AddressTable() != null)
                return AddressList(0).Rows[0][0].ToString();
            else
                return null;
        }

        public string CreateFlatQuery()
        {
            if (ellisFilterSettings.AddressTable() != null)
                return AddressList(1).Rows[0][0].ToString();
            else
                return null;
        }

        public string CreateAccountQuery()
        {
            if (ellisFilterSettings.AddressTable() != null)
                return AddressList(2).Rows[0][0].ToString();
            else
                return null;
        }

        DataTable AddressList(int type)
        {
            var dt = ellisFilterSettings.AddressTable();
            DAccess.DataModule.CopyDataTableToTempDB(dt);
            return DAccess.DataModule.GetDataTableByStoredProcedure1("town.CreateAddressLists", new object[,] { {"type",type } });
        }

        public bool IsEmpty
        {
            get
            {
                return ellisFilterSettings.AddressList.Count == 0;
            }
        }

        public bool ParameterTab
        {
            get { return parameterTabPage.PageVisible; }
            set { parameterTabPage.PageVisible = value; }
        }

        public AddressFilter()
        {
            InitializeComponent();
        }

        private void AddressFilter_Load(object sender, EventArgs e)
        {
            ellisFilterSettings = new EllisFilterSettings();
            //FillAddressTree();
        }

        void SetAddressListDataSource()
        {
            selectedAdrListBox.DataSource = null;
            selectedAdrListBox.DataSource = ellisFilterSettings.AddressList;
        }

        private void selectAllBtn_Click(object sender, EventArgs e)
        {
            ellisFilterSettings.AddressList =
                (from TreeListNode adr in addressTreeList.Nodes
                where adr.Level == 0
                select new EllisAddress()
                {
                    ID = int.Parse(adr[0].ToString()),
                    TypeID = int.Parse(adr[2].ToString()),
                    DeriveCompany = int.Parse(adr[4].ToString()),
                    Caption = adr[5].ToString()
                }).ToList();

            SetAddressListDataSource();
        }

        private void openFilterBtn_Click(object sender, EventArgs e)
        {
            ellisFilterSettings.LoadFromFile();
            SetAddressListDataSource();
        }

        private void saveFilterBtn_Click(object sender, EventArgs e)
        {
            ellisFilterSettings.SaveToFile();
        }

        #region selectedAdrListBox Methods

        private void selectedAdrListBox_DragEnter(object sender, DragEventArgs e)
        {
            if (GetDragNodes(e.Data) != null)
                e.Effect = DragDropEffects.Copy;
        }

        private void selectedAdrListBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (ellisFilterSettings.AddressList.Count != 0)
                {
                    selectedAdrListBox.SelectedIndex = selectedAdrListBox.IndexFromPoint(e.Location);
                    if (selectedAdrListBox.SelectedItems.Count == 0)
                        popupMenu2.ShowPopup(MousePosition);
                    else
                        popupMenu1.ShowPopup(MousePosition);
                }
            }
        }

        private void selectedAdrListBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.Clicks == 2)
            {
                if (ellisFilterSettings.AddressList.Count != 0)
                {
                    selectedAdrListBox.SelectedIndex = selectedAdrListBox.IndexFromPoint(e.Location);
                    if (selectedAdrListBox.SelectedIndex != -1)
                    {
                        ellisFilterSettings.AddressList.Remove((EllisAddress)selectedAdrListBox.SelectedItem);
                        SetAddressListDataSource();
                    }
                }   
            }
        }

        private void selectedAdrListBox_DragDrop(object sender, DragEventArgs e)
        {
            ImageListBoxControl lb = sender as ImageListBoxControl;
            TreeListMultiSelection nodes = GetDragNodes(e.Data);
            if (nodes != null)
            {
                int ind = lb.IndexFromPoint(lb.PointToClient(new Point(e.X, e.Y)));
                for (int i = 0; i < nodes.Count; i++)
                {
                    //if (ind == -1)
                    //{
                        object[] obj = GetRowByNode(nodes[i]);
                        ellisFilterSettings.AddressList.Add(
                            new EllisAddress()
                            {
                                ID = int.Parse(obj[0].ToString()),
                                TypeID = int.Parse(obj[2].ToString()),
                                DeriveCompany = int.Parse(obj[4].ToString()),
                                Caption = obj[5].ToString()
                            });
                    //}
                }
            }
            SetAddressListDataSource();
        }

        private TreeListMultiSelection GetDragNodes(IDataObject data)
        {
            return data.GetData(typeof(TreeListMultiSelection)) as TreeListMultiSelection;
        }

        private object[] GetRowByNode(TreeListNode node)
        {
            object[] ret = new object[addressTreeList.Columns.Count];
            for (int i = 0; i < addressTreeList.Columns.Count; i++)
                ret[i] = node.GetValue(i);
            return ret;
        }

        #endregion

        #region addressTreeList Methods

        public void FillAddressTree()
        {
            DAccess.DataModule.AddressFilterTreeSelect(cDataSet.AddressFilterTree, 5);
            AddAddressTreeNode(null, cDataSet.AddressFilterTree);
        }

        void AddAddressTreeNode(TreeListNode parentNode, DataTable table)
        {
            foreach (DataRow row in table.Rows)
            {
                addressTreeList.BeginUnboundLoad();

                var objs = new object[] { row["ID"], row["ID_Parent"], row["Type"], row["Name"], row["DeriveCompany"], row["Caption"] };
                TreeListNode rootNode = addressTreeList.AppendNode(objs, parentNode);
                rootNode.StateImageIndex = int.Parse(row["Type"].ToString());

                TreeListNode Node;
                if (parentNode == null || parentNode.Level != 4)
                    Node = addressTreeList.AppendNode(new object[] { "", "", "", "", "", "" }, rootNode);

                addressTreeList.EndUnboundLoad();
            }
        }

        private void addressTreeList_BeforeExpand(object sender, BeforeExpandEventArgs e)
        {
            e.Node.Nodes.Clear();
            int type = int.Parse(e.Node.GetValue(2).ToString()) - 1;
            int id = int.Parse(e.Node[0].ToString());
            int deriveCompany = int.Parse(e.Node[4].ToString());

            cDataSet.AddressFilterTree.Clear();
            DAccess.DataModule.AddressFilterTreeSelect(cDataSet.AddressFilterTree, type, id, deriveCompany);
            AddAddressTreeNode(e.Node, cDataSet.AddressFilterTree);
        }

        private void addressTreeList_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.None)
            {
                TreeList tl = sender as TreeList;
                dragStartHitInfo = tl.CalcHitInfo(e.Location);
            }
        }

        private void addressTreeList_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && dragStartHitInfo != null && dragStartHitInfo.Node != null)
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(dragStartHitInfo.MousePoint.X - dragSize.Width / 2,
                    dragStartHitInfo.MousePoint.Y - dragSize.Height / 2), dragSize);
                if (!dragRect.Contains(e.Location))
                    ((TreeList)sender).DoDragDrop(addressTreeList.Selection, DragDropEffects.Copy);
            }
        }

        #endregion

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ellisFilterSettings.AddressList.Remove((EllisAddress)selectedAdrListBox.SelectedItem);
            SetAddressListDataSource();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ellisFilterSettings.AddressList.Clear();
            SetAddressListDataSource();
        }
    }
}
