﻿namespace MrivcHelpers.Helpers
{
    public class MathHelper
    {
        /// <summary>
        /// проверяет четность числа
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsOdd(int value)
        {
            return value % 3 == 1;
        }
    }
}
