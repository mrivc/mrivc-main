﻿using System;
using System.Collections.Generic;
using System.Text;
using BillsLoader.Logic;
using System.Data;

namespace BillsLoader.DM
{
    class DAccess
    {
        private static DAccess instance;
        private static DModule dModule;

        private DAccess() { }

        public static DAccess Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DAccess();
                }
                return instance;
            }
        }

        public static DModule DataModule
        {
            get
            {
                if (dModule == null)
                {
                    dModule = new DModule();
                }
                return dModule;
            }
        }

        private int period = 0;
        public int Period
        {
            get {
                if (period == 0)
                    period = DataModule.WorkPeriod();
                return period;
            }
        }

        private string periodName = string.Empty;
        public string PeriodName
        {
            get
            {
                if (periodName == string.Empty)
                    periodName = DataModule.WorkPeriodName();
                return periodName;
            }
        }

        private ZXPeriod prevPeriod;
        public ZXPeriod PrevPeriod
        {
            get
            {
                if (prevPeriod == null)
                    prevPeriod = new ZXPeriod(DataModule.PrevPeriod());
                return prevPeriod;
            }
        }


        private MDataSet.Pack_TypeDataTable packType;
        public MDataSet.Pack_TypeDataTable PackType
        {
            get {
                if (packType == null)
                {
                    packType = new MDataSet.Pack_TypeDataTable();
                    DataModule.PackTypeSelect(packType);
                }
                return packType;
            }
        }

        private MDataSet.Sber_Saldo_Reestr_TypeDataTable sberReestrType;
        public MDataSet.Sber_Saldo_Reestr_TypeDataTable SberReestrType
        {
            get
            {
                if (sberReestrType == null)
                {
                    sberReestrType = new MDataSet.Sber_Saldo_Reestr_TypeDataTable();
                    DataModule.SberSaldoReestrTypeSelect(sberReestrType);
                }
                return sberReestrType;
            }
        }


        private MDataSet.Sber_saldoDataTable sberSaldo;
        public MDataSet.Sber_saldoDataTable SberSaldo
        {
            get
            {
                if (sberSaldo == null)
                {
                    sberSaldo = new MDataSet.Sber_saldoDataTable();
                    DataModule.SberSaldoSelect(sberSaldo);
                }
                return sberSaldo;
            }
        }

        public DataRow[] GetBillFilter(string account, int idClient)
        {
            return DAccess.Instance.SberSaldo.Select("Account='" + account + "' and ID_Client=" + idClient.ToString());
        }

    }
}
