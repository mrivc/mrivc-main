﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using FastReport.Design.StandardDesigner;
using FastReport;
using System.Data;

namespace MassBillNew
{
    using MrivcControls.Data;
    using MrivcHelpers.Helpers;

    public static class StaticConfig
    {
        private static SqlConnection connection;

        public static void SetConnection(string value)
        {
            SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();

            if (isWinLogin(value))
                connBuilder.IntegratedSecurity = true;
            else
            {
                connBuilder.UserID = value.GetSqlConnectionPart("UserID");
                connBuilder.Password = value.GetSqlConnectionPart("Password");
            }
            
            connBuilder.InitialCatalog = value.GetSqlConnectionPart("InitialCatalog");
            connBuilder.DataSource = value.GetSqlConnectionPart("DataSource");
            connBuilder.PersistSecurityInfo = true;
            connection = new SqlConnection();
            connection.ConnectionString = connBuilder.ConnectionString;
        }

        static bool isWinLogin(string connectionString)
        {
            string value = string.Empty;

            try
            {
                value = connectionString.GetSqlConnectionPart("IntegratedSecurity");
            } 
            catch {}

            if (value.ToLower() == "true" || value.ToLower() == "sspi")
                return true;
            else
                return false;
        }

        public static void SetConnectionReadyString(string connectionString)
        {
            connection = new SqlConnection();
            connection.ConnectionString = connectionString;


        }

        public static SqlConnection MainConnection
        {
            get {
                if (connection == null)
                {
                   
                    connection = new SqlConnection(Properties.Settings.Default.MurmanskConnectionString);

                }
                return connection;
            }
        }

        public static object ExecuteScalar(string SqlCommand)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandTimeout = 30;
            cmd.CommandText = SqlCommand;

            return cmd.ExecuteScalar();
        }

        public static string Title(string text)
        {
            return text = text + (" (" + StaticConfig.MainConnection.DataSource + " - " + StaticConfig.MainConnection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
        }

        public static DesignerForm OpenInDesigner(bool reportLoadedFromFile, string fileName)
        {
            DesignerForm designerForm = new DesignerForm(DAccess.DataModule.ConnectionString);
            designerForm.Load += designerForm_Load;

            Report report = LoadReport(reportLoadedFromFile, fileName);

            report.FileName = fileName;

            designerForm.Designer.Report = report;

            return designerForm;
        }

        static void designerForm_Load(object sender, EventArgs e)
        {
            DesignerForm form = sender as DesignerForm;

            form.Designer.MdiMode = true;

            //form.UpdateContent();
        }

        private static Report LoadReport(bool reportLoadedFromFile,string fileName)
        {
            Report report = new Report();

            //если отчет загружен из файла
            if (reportLoadedFromFile)
            {
                report.Load(fileName);
                report.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);

            }
            else //либо отчет хранится в БД
            {

                SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                cmd.CommandText = string.Format("select convert(varbinary(max),[file]) from dbo.module where name ='{0}' ", fileName);

                sqlConnection1.Open();

                byte[] module = (byte[])cmd.ExecuteScalar();

                sqlConnection1.Close();

                report.LoadFromString(Encoding.UTF8.GetString(module));

                report.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);


            }

            return report;
        }
    }
}
