﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.Columns;

namespace EllisBillFilter
{
    public delegate void GoToActiveClientDetailEventHandler(int idClientDetail);

    public partial class HouseClientDetailFilter : UserControl
    {
        public event GoToActiveClientDetailEventHandler GoToActiveClientDetailActiveted;

        private bool initialized;
        int period;
        long selectedHouseId;

        public HouseClientDetailFilter()
        {
            InitializeComponent();

            initialized = false;
            gridView1.Name = "gridHouseBillFilter";
            treeHouseHierarhy1.Tree.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(Tree_FocusedNodeChanged);
        }

        public void FindAndSelectHouse(int idHouse)
        {
            TreeListNode node = treeHouseHierarhy1.FindNodeByKeyId(idHouse);
            //treeHouseHierarhy1.Tree.FocusedNode = node;
            treeHouseHierarhy1.Tree.CollapseAll();
            treeHouseHierarhy1.Tree.SetFocusedNode(node);
        }

        public bool Initialized
        {
            get { return initialized; }
        }

        void Tree_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
            {
                lblAddress.Text = "";
                mDataSet.ClientDetail.Clear();
                if (e.Node.Level == 2)
                {
                    selectedHouseId = treeHouseHierarhy1.SelectedId;
                    ReLoadData(period);
                    lblAddress.Text = DAccess.DataModule.GetHouseAddress((int)selectedHouseId);
                }
            }
        }

        public void LoadData(int period)
        {
            treeHouseHierarhy1.LoadData();
            this.period = period;

            DAccess.DataModule.HouseWithoutBillFilterSelect(mDataSet.HouseWithoutBillFilter);
            mDataSet.Period.Merge(DAccess.Instance.Periods);
            mDataSet.BillFilter.Merge(DAccess.Instance.BillFilter);
            mDataSet.DeriveCompany.Merge(DAccess.Instance.DeriveCompany);
            mDataSet.PayReceiverCompany.Merge(DAccess.Instance.PayReceiverCompany);
            mDataSet.FilterType.Merge(DAccess.Instance.FilterType);

            lblHousesWithoutBF.Text = "Перешедшие дома без кода фильтра (" + mDataSet.HouseWithoutBillFilter.Rows.Count.ToString() + "):";

            baseGrid1.InitEx();
            initialized = true;
        }

        public void ReLoadData(int period)
        {
            if (!initialized)
                return;

            mDataSet.ClientDetail.Clear();
            this.period = period;
            DAccess.DataModule.FilterHouseSelect(mDataSet.ClientDetail, period, selectedHouseId);
        }

        private void miGoToActiveFilters_Click(object sender, EventArgs e)
        {
            if (GoToActiveClientDetailActiveted != null)
            {
                if (gridView1.FocusedRowHandle >= 0)
                    GoToActiveClientDetailActiveted((int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Client_Detail"]);
            }
        }

        private void miCheckUncheckFilterForHouse_Click(object sender, EventArgs e)
        {
            if (this.period != DAccess.Instance.WorkPeriod)
            {
                MessageBox.Show("Запрещено редактирование  записей в прошлых периодах.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (gridView1.FocusedRowHandle < 0)
            {
                MessageBox.Show("Необходимо выбрать код фильтра из текущего списка.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (treeHouseHierarhy1.SelectedId < 0)
            {
                MessageBox.Show("Необходимо выбрать дом и код фильтра", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (gridView1.FocusedRowHandle >= 0)
            {
                if ((bool)gridView1.GetDataRow(gridView1.FocusedRowHandle)["IsChecked"] == false)
                    CheckBillFilter(1);
                else
                    CheckBillFilter(0);
            }
        }

        private void CheckBillFilter(int check)
        {
            switch (idFilterType)
            { 
                case 1: //текущий код фильтра
                    DAccess.DataModule.ClientDetailHouseUpdate((int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Client_Detail"],
                            (int)treeHouseHierarhy1.SelectedId, check, 0);
                    break;
                case 2: //долговой код фильтра
                    DAccess.DataModule.ClientDetailHouseUpdate((int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Client_Detail"],
                        (int)treeHouseHierarhy1.SelectedId, check, 1);
                    break;
                case 3: //смешанный код фильтра
                    DAccess.DataModule.ClientDetailHouseUpdate((int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Client_Detail"],
                        (int)treeHouseHierarhy1.SelectedId, check, 0);
                    DAccess.DataModule.ClientDetailHouseUpdate((int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Client_Detail"],
                        (int)treeHouseHierarhy1.SelectedId, check, 1);
                    break;
            }
            gridView1.GetDataRow(gridView1.FocusedRowHandle)["IsChecked"] = (check == 1? true : false);
        }

        private int idFilterType
        {
            get {
                return (int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Filter_Type"];
            }        
        }

        private void cbHouseWithoutBF_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            FindAndSelectHouse((int)e.NewValue);
        }
    }
}
