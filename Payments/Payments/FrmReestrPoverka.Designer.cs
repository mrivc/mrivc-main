﻿namespace Payments
{
    partial class FrmReestrPoverka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridPaymentPoverka1 = new Payments.Controls.GridPaymentPoverka();
            this.paymentsDataSet = new Payments.PaymentsDataSet();
            this.paymentClassBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentClassBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridPaymentPoverka1
            // 
            this.gridPaymentPoverka1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPaymentPoverka1.Location = new System.Drawing.Point(0, 0);
            this.gridPaymentPoverka1.Name = "gridPaymentPoverka1";
            this.gridPaymentPoverka1.Size = new System.Drawing.Size(808, 494);
            this.gridPaymentPoverka1.TabIndex = 0;
            this.gridPaymentPoverka1.Load += new System.EventHandler(this.gridPaymentPoverka1_Load);
            // 
            // paymentsDataSet
            // 
            this.paymentsDataSet.DataSetName = "PaymentsDataSet";
            this.paymentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // paymentClassBindingSource
            // 
            this.paymentClassBindingSource.DataMember = "Payment_Class";
            this.paymentClassBindingSource.DataSource = this.paymentsDataSet;
            // 
            // FrmReestrPoverka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 494);
            this.Controls.Add(this.gridPaymentPoverka1);
            this.Name = "FrmReestrPoverka";
            this.Text = "Реестр платежей по услугам";
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentClassBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.GridPaymentPoverka gridPaymentPoverka1;
        private PaymentsDataSet paymentsDataSet;
        private System.Windows.Forms.BindingSource paymentClassBindingSource;
    }
}