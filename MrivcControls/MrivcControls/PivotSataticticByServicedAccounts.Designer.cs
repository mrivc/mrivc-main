﻿namespace MrivcControls
{
    partial class PivotSataticticByServicedAccounts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cDataSet = new MrivcControls.Data.CDataSet();
            this.btnUpdateDate = new System.Windows.Forms.Button();
            this.pivotGridControl2 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miExpandAllGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.miCollapseAllGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.statisticByServicedAccountsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nDataSet = new MrivcControls.Data.NDataSet();
            this.fieldPeriod = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIDDeriveCompany = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIDProvider = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIDProviderGroup = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDeriveCompany = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClient = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldBillFilter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldServiceName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldProvider = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHouseAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldBalance = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSumTariff = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCreationDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldProviderGroup = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIDClient = new DevExpress.XtraPivotGrid.PivotGridField();
            this.cbClients = new MassBillNew.CustomEdit();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.miCopyAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.cbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl2)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statisticByServicedAccountsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbClients.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Период:";
            // 
            // cbPeriod
            // 
            this.cbPeriod.Location = new System.Drawing.Point(69, 20);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPeriod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Period", "Period", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Month", "Month", 40, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Year", "Year", 32, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Work_Period", "Is_Work_Period", 86, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.cbPeriod.Properties.DataSource = this.periodBindingSource;
            this.cbPeriod.Properties.DisplayMember = "Name";
            this.cbPeriod.Properties.DropDownRows = 12;
            this.cbPeriod.Properties.NullText = "";
            this.cbPeriod.Properties.ShowFooter = false;
            this.cbPeriod.Properties.ShowHeader = false;
            this.cbPeriod.Properties.ShowLines = false;
            this.cbPeriod.Properties.ValueMember = "Period";
            this.cbPeriod.Size = new System.Drawing.Size(187, 20);
            this.cbPeriod.TabIndex = 8;
            this.cbPeriod.EditValueChanged += new System.EventHandler(this.cbPeriod_EditValueChanged);
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.cDataSet;
            // 
            // cDataSet
            // 
            this.cDataSet.DataSetName = "CDataSet";
            this.cDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnUpdateDate
            // 
            this.btnUpdateDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateDate.Location = new System.Drawing.Point(1143, 18);
            this.btnUpdateDate.Name = "btnUpdateDate";
            this.btnUpdateDate.Size = new System.Drawing.Size(198, 23);
            this.btnUpdateDate.TabIndex = 10;
            this.btnUpdateDate.Text = "Обновить данные";
            this.btnUpdateDate.UseVisualStyleBackColor = true;
            this.btnUpdateDate.Click += new System.EventHandler(this.btnUpdateDate_Click);
            // 
            // pivotGridControl2
            // 
            this.pivotGridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pivotGridControl2.ContextMenuStrip = this.contextMenuStrip1;
            this.pivotGridControl2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl2.DataSource = this.statisticByServicedAccountsBindingSource;
            this.pivotGridControl2.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldPeriod,
            this.fieldIDDeriveCompany,
            this.fieldIDProvider,
            this.fieldIDProviderGroup,
            this.fieldDeriveCompany,
            this.fieldClient,
            this.fieldBillFilter,
            this.fieldServiceName,
            this.fieldProvider,
            this.fieldHouseAddress,
            this.fieldAccount,
            this.fieldBalance,
            this.fieldSumTariff,
            this.fieldCreationDate,
            this.fieldProviderGroup,
            this.fieldIDClient});
            this.pivotGridControl2.Location = new System.Drawing.Point(3, 61);
            this.pivotGridControl2.Name = "pivotGridControl2";
            this.pivotGridControl2.OptionsBehavior.BestFitMode = DevExpress.XtraPivotGrid.PivotGridBestFitMode.FieldValue;
            this.pivotGridControl2.OptionsCustomization.AllowFilter = false;
            this.pivotGridControl2.OptionsData.DrillDownMaxRowCount = 100;
            this.pivotGridControl2.Size = new System.Drawing.Size(1349, 553);
            this.pivotGridControl2.TabIndex = 11;
            this.pivotGridControl2.EndRefresh += new System.EventHandler(this.pivotGridControl2_EndRefresh);
            this.pivotGridControl2.FieldValueExpanding += new DevExpress.XtraPivotGrid.PivotFieldValueCancelEventHandler(this.pivotGridControl2_FieldValueExpanding);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExpandAllGroups,
            this.miCollapseAllGroups,
            this.toolStripSeparator1,
            this.miCopyAccount,
            this.toolStripSeparator2,
            this.miExportExcel});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(201, 126);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // miExpandAllGroups
            // 
            this.miExpandAllGroups.Image = global::MrivcControls.Properties.Resources.expand;
            this.miExpandAllGroups.Name = "miExpandAllGroups";
            this.miExpandAllGroups.Size = new System.Drawing.Size(200, 22);
            this.miExpandAllGroups.Tag = "All";
            this.miExpandAllGroups.Text = "Развернуть все группы";
            this.miExpandAllGroups.Click += new System.EventHandler(this.miExpandAllGroups_Click);
            // 
            // miCollapseAllGroups
            // 
            this.miCollapseAllGroups.Image = global::MrivcControls.Properties.Resources.collapse_all;
            this.miCollapseAllGroups.Name = "miCollapseAllGroups";
            this.miCollapseAllGroups.Size = new System.Drawing.Size(200, 22);
            this.miCollapseAllGroups.Tag = "All";
            this.miCollapseAllGroups.Text = "Свернуть все группы";
            this.miCollapseAllGroups.Click += new System.EventHandler(this.miCollapseAllGroups_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(197, 6);
            // 
            // miExportExcel
            // 
            this.miExportExcel.Name = "miExportExcel";
            this.miExportExcel.Size = new System.Drawing.Size(200, 22);
            this.miExportExcel.Text = "Экспорт в Excel";
            this.miExportExcel.Click += new System.EventHandler(this.miExportExcel_Click);
            // 
            // statisticByServicedAccountsBindingSource
            // 
            this.statisticByServicedAccountsBindingSource.DataMember = "Statistic_By_Serviced_Accounts";
            this.statisticByServicedAccountsBindingSource.DataSource = this.nDataSet;
            // 
            // nDataSet
            // 
            this.nDataSet.DataSetName = "NDataSet";
            this.nDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fieldPeriod
            // 
            this.fieldPeriod.AreaIndex = 0;
            this.fieldPeriod.Caption = "Period";
            this.fieldPeriod.FieldName = "Period";
            this.fieldPeriod.Name = "fieldPeriod";
            this.fieldPeriod.Visible = false;
            // 
            // fieldIDDeriveCompany
            // 
            this.fieldIDDeriveCompany.AreaIndex = 0;
            this.fieldIDDeriveCompany.Caption = "ID_Derive_Company";
            this.fieldIDDeriveCompany.FieldName = "ID_Derive_Company";
            this.fieldIDDeriveCompany.Name = "fieldIDDeriveCompany";
            this.fieldIDDeriveCompany.Visible = false;
            // 
            // fieldIDProvider
            // 
            this.fieldIDProvider.AreaIndex = 0;
            this.fieldIDProvider.Caption = "ID_Provider";
            this.fieldIDProvider.FieldName = "ID_Provider";
            this.fieldIDProvider.Name = "fieldIDProvider";
            this.fieldIDProvider.Visible = false;
            // 
            // fieldIDProviderGroup
            // 
            this.fieldIDProviderGroup.AreaIndex = 0;
            this.fieldIDProviderGroup.Caption = "ID_Provider_Group";
            this.fieldIDProviderGroup.FieldName = "ID_Provider_Group";
            this.fieldIDProviderGroup.Name = "fieldIDProviderGroup";
            this.fieldIDProviderGroup.Visible = false;
            // 
            // fieldDeriveCompany
            // 
            this.fieldDeriveCompany.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldDeriveCompany.AreaIndex = 2;
            this.fieldDeriveCompany.Caption = "УК";
            this.fieldDeriveCompany.FieldName = "Derive_Company";
            this.fieldDeriveCompany.Name = "fieldDeriveCompany";
            this.fieldDeriveCompany.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldClient
            // 
            this.fieldClient.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldClient.AreaIndex = 0;
            this.fieldClient.Caption = "Клиент";
            this.fieldClient.FieldName = "Client";
            this.fieldClient.Name = "fieldClient";
            this.fieldClient.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldBillFilter
            // 
            this.fieldBillFilter.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldBillFilter.AreaIndex = 1;
            this.fieldBillFilter.Caption = "Код фильтра";
            this.fieldBillFilter.FieldName = "Bill_Filter";
            this.fieldBillFilter.Name = "fieldBillFilter";
            this.fieldBillFilter.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldServiceName
            // 
            this.fieldServiceName.AreaIndex = 0;
            this.fieldServiceName.Caption = "Услуга";
            this.fieldServiceName.FieldName = "ServiceName";
            this.fieldServiceName.Name = "fieldServiceName";
            this.fieldServiceName.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldProvider
            // 
            this.fieldProvider.AreaIndex = 1;
            this.fieldProvider.Caption = "Поставщик";
            this.fieldProvider.FieldName = "Provider";
            this.fieldProvider.Name = "fieldProvider";
            this.fieldProvider.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldHouseAddress
            // 
            this.fieldHouseAddress.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldHouseAddress.AreaIndex = 3;
            this.fieldHouseAddress.Caption = "Дом";
            this.fieldHouseAddress.FieldName = "House_Address";
            this.fieldHouseAddress.Name = "fieldHouseAddress";
            this.fieldHouseAddress.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldAccount
            // 
            this.fieldAccount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldAccount.AreaIndex = 0;
            this.fieldAccount.Caption = "Лицевой счет";
            this.fieldAccount.FieldName = "Account";
            this.fieldAccount.Name = "fieldAccount";
            this.fieldAccount.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldAccount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count;
            // 
            // fieldBalance
            // 
            this.fieldBalance.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldBalance.AreaIndex = 1;
            this.fieldBalance.Caption = "Баланс";
            this.fieldBalance.FieldName = "Balance";
            this.fieldBalance.Name = "fieldBalance";
            this.fieldBalance.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldSumTariff
            // 
            this.fieldSumTariff.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldSumTariff.AreaIndex = 2;
            this.fieldSumTariff.Caption = "Начислено";
            this.fieldSumTariff.FieldName = "Sum_Tariff";
            this.fieldSumTariff.Name = "fieldSumTariff";
            this.fieldSumTariff.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldCreationDate
            // 
            this.fieldCreationDate.AreaIndex = 3;
            this.fieldCreationDate.Caption = "Creation Date";
            this.fieldCreationDate.FieldName = "CreationDate";
            this.fieldCreationDate.Name = "fieldCreationDate";
            this.fieldCreationDate.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.fieldCreationDate.Visible = false;
            // 
            // fieldProviderGroup
            // 
            this.fieldProviderGroup.AreaIndex = 2;
            this.fieldProviderGroup.Caption = "Группа поставщиков";
            this.fieldProviderGroup.FieldName = "Provider_Group";
            this.fieldProviderGroup.Name = "fieldProviderGroup";
            this.fieldProviderGroup.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            // 
            // fieldIDClient
            // 
            this.fieldIDClient.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldIDClient.AreaIndex = 1;
            this.fieldIDClient.FieldName = "ID_Client";
            this.fieldIDClient.Name = "fieldIDClient";
            this.fieldIDClient.Visible = false;
            // 
            // cbClients
            // 
            this.cbClients.Location = new System.Drawing.Point(333, 20);
            this.cbClients.Name = "cbClients";
            this.cbClients.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbClients.Properties.DataSource = this.companyBindingSource;
            this.cbClients.Properties.DisplayMember = "Name";
            this.cbClients.Properties.IncrementalSearch = false;
            this.cbClients.Properties.ValueMember = "ID_Company";
            this.cbClients.Properties.EditValueChanged += new System.EventHandler(this.customEdit1_Properties_EditValueChanged);
            this.cbClients.Size = new System.Drawing.Size(457, 20);
            this.cbClients.TabIndex = 13;
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataMember = "Company";
            this.companyBindingSource.DataSource = this.cDataSet;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Клиенты:";
            // 
            // miCopyAccount
            // 
            this.miCopyAccount.Name = "miCopyAccount";
            this.miCopyAccount.Size = new System.Drawing.Size(200, 22);
            this.miCopyAccount.Text = "Скопировать ЛС";
            this.miCopyAccount.Click += new System.EventHandler(this.miCopyAccount_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(197, 6);
            // 
            // PivotSataticticByServicedAccounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbClients);
            this.Controls.Add(this.pivotGridControl2);
            this.Controls.Add(this.btnUpdateDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPeriod);
            this.Name = "PivotSataticticByServicedAccounts";
            this.Size = new System.Drawing.Size(1355, 614);
            ((System.ComponentModel.ISupportInitialize)(this.cbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl2)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statisticByServicedAccountsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbClients.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit cbPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private Data.CDataSet cDataSet;
        private System.Windows.Forms.Button btnUpdateDate;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl2;
        private System.Windows.Forms.BindingSource statisticByServicedAccountsBindingSource;
        private Data.NDataSet nDataSet;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPeriod;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIDDeriveCompany;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIDProvider;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIDProviderGroup;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDeriveCompany;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClient;
        private DevExpress.XtraPivotGrid.PivotGridField fieldBillFilter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldServiceName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldProvider;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHouseAddress;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldBalance;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSumTariff;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCreationDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldProviderGroup;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miExportExcel;
        private System.Windows.Forms.ToolStripMenuItem miExpandAllGroups;
        private System.Windows.Forms.ToolStripMenuItem miCollapseAllGroups;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private MassBillNew.CustomEdit cbClients;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIDClient;
        private System.Windows.Forms.ToolStripMenuItem miCopyAccount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}
