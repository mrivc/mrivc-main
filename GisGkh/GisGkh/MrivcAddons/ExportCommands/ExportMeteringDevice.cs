﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportMeteringDevice : MrivcExportCommand , ISingleHouseCommand
    {
        public ExportMeteringDevice(string senderID) : base("exportMeteringDeviceData", "HomeManagementAsync", senderID, "exportMeteringDeviceDataResult")
        {}

        public string FiasHouseGuid { get; set; }

        public new void Execute()
        {
      
            //if (CommandType == AsyncCommandType.Send || CommandType == AsyncCommandType.All)
            //{
            //    var values = new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) };

            //    base.Execute(values);
            //}

            //if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
            //{
                this.ExecuteToTempDB();
            //}

        
                   
        }

        public new void ExecuteToTempDB()
        {
            var values = new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) };

            MrivcData.CreateTempExportMeterTables();
            base.ExecuteToTempDB(values, _exportNodeName);

            if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
            {
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportMeteringDeviceData");
                DAccess.DataModule.ExecuteNonQueryCommand1("exec [GIS].[SaveExportedMeterData]");
            }
        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            var ds = new DataSet("MeterData");
            var dt = DAccess.DataModule.GetDataTableByQueryCommand1("select * from #exportMeterData");
            dt.TableName = "Meter";
            ds.Tables.Add(dt);

            return ds;
        }
    }
}
