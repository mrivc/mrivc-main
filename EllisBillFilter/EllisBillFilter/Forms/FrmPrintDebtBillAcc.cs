﻿using EllisBillFilter.DS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using System.Collections;
    using System.Globalization;

    using DevExpress.Data;
    using DevExpress.XtraGrid;

    using EllisBillFilter.Properties;

    using FastReport;

    public partial class FrmPrintDebtBillAcc : Form
    {
        private int idAccount;
        private int period;

        public FrmPrintDebtBillAcc(int idAccount, int period)
        {
            InitializeComponent();

            this.idAccount = idAccount;
            this.period = period;

            Clipboard.SetText(idAccount.ToString());
            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
            this.lblAddress.Text = "Адрес: " + DAccess.DataModule.GetAccountAddress(idAccount);

            this.gridDebtView.Columns["balance"].SummaryItem.SummaryType = SummaryItemType.Sum;
            this.gridDebtView.Columns["balance"].SummaryItem.DisplayFormat = "Сумма = {0:#.##}";
        }

        public void LoadData(int idAccount, int period)
        {
            this.idAccount = idAccount;
            this.period = period;

            DAccess.DataModule.DebtAccountBalanceSelect(mDataSet.DebtAccountBalance, idAccount, period, -1, -1);
            FillDeriveCompanyDataTable();

            this.gridDebt.InitEx();
        }

        private void FillDeriveCompanyDataTable()
        {
            var companies = mDataSet.DebtAccountBalance.GroupBy(
                p => new { p.id_derive_company, p.derive_company },
                p => p,
                (key, g) => new { id_derive_company = key.id_derive_company, derive_company = key.derive_company });

            var row = mDataSet.DeriveCompany.NewDeriveCompanyRow();
            row.ID_Company = -1;
            row.Name = "Все";
            mDataSet.DeriveCompany.Rows.Add(row);

            foreach (var company in companies)
            {
                row = mDataSet.DeriveCompany.NewDeriveCompanyRow();
                row.ID_Company = company.id_derive_company;
                row.Name = company.derive_company;
                mDataSet.DeriveCompany.Rows.Add(row);
            }

            ddComboBox.EditValue = -1;
        }

        private void FrmPrintDebtBillAcc_Load(object sender, EventArgs e)
        {
            LoadData(this.idAccount, this.period);
        }

        private void ddComboBox_EditValueChanged(object sender, EventArgs e)
        {
            mDataSet.DebtAccountBalance.Clear();
            mDataSet.DebtClientDetail.Clear();

            ddClientDetail.ResetText();
            ddClientDetail.EditValue = DBNull.Value;
            int idDeriveCompany = (int)ddComboBox.EditValue;

            DAccess.DataModule.DebtAccountBalanceSelect(mDataSet.DebtAccountBalance, this.idAccount, this.period, idDeriveCompany, -1);
            DAccess.DataModule.DebtBillClientDetailSelect(mDataSet.DebtClientDetail, idDeriveCompany);
            this.gridDebtView.ExpandAllGroups();
        }

        private void ddClientDetail_EditValueChanged(object sender, EventArgs e)
        {
            ReloadBillFilterList();
        }

        private void ReloadBillFilterList()
        {
            if (ddClientDetail.EditValue is int && ddComboBox.EditValue is int)
            {
                int idDeriveCompany = (int)ddComboBox.EditValue;
                int idBillFilter = (int)ddClientDetail.EditValue;

                mDataSet.DebtAccountBalance.Clear();
                DAccess.DataModule.DebtAccountBalanceSelect(mDataSet.DebtAccountBalance, this.idAccount, this.period, idDeriveCompany, idBillFilter);
                this.gridDebtView.ExpandAllGroups();
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (ddClientDetail.EditValue is int && ddComboBox.EditValue is int)
            {
                int idDeriveCompany = (int)ddComboBox.EditValue;
                int idBillFilter = (int)ddClientDetail.EditValue;

                var clientDetailInfo = mDataSet.DebtClientDetail.Where(cd => cd.id_bill_filter == idBillFilter).ToList<MDataSet.DebtClientDetailRow>()[0];
                string error = DAccess.DataModule.DebtBillPrintCheck(
                    this.idAccount,
                    clientDetailInfo.id_client,
                    idDeriveCompany,
                    idBillFilter);

                if (error.Length > 0)
                {
                    MessageBox.Show(error, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                this.CreateClientDetailIfNotExist();

                btnPrint.Enabled = false;

                SetStatusText("загрузка отчета...");
                report1 = new Report();
                report1.LoadFromString(DAccess.Instance.GetBillStdReport());
                report1.DoublePass = false;
                report1.UseFileCache = false;
                report1.SetParameterValue("PrintBillsWithSubscription", true);
                report1.SetParameterValue("MainConnection", DAccess.DataModule.Connection.ConnectionString);

                SetStatusText("подготовка списка лицевый счетов...");
                DAccess.DataModule.PrepareBillReportAccounts(idAccount.ToString());

                long idPrint = DAccess.Instance.GenerateIdPrint();

                SetStatusText(string.Format("подготовка данных для квитанций ({0})... ", idPrint));
                DAccess.DataModule.PrepareBillReportData(
                    idPrint,
                    idBillFilter,
                    this.period,
                    true,
                    true,
                    idDeriveCompany, 
                    1);

                report1.SetParameterValue("IDPrint", (long)idPrint);
                report1.SetParameterValue("IS_DEBT", true);
                report1.SetParameterValue("PrintSummaryPage", false);
                report1.Show();

                SetStatusText(string.Format("Готово ({0}).", idPrint));
                Clipboard.Clear();
                Clipboard.SetText(idPrint.ToString());

                DAccess.DataModule.ClearPrintDataTables(idPrint);
                btnPrint.Enabled = true;
                
                if (Settings.Default.CloseWindowAfterPrintBill)
                    this.Close();
            }
            else
            {
                MessageBox.Show("Для печати квитанции необходимо выбрать \"Управляющую компанию\" и \"Код фильтра\".", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CreateClientDetailIfNotExist()
        {
            if (ddClientDetail.EditValue is int && ddComboBox.EditValue is int)
            {
                int idDeriveCompany = (int)ddComboBox.EditValue;
                int idBillFilter = (int)ddClientDetail.EditValue;

                var clientDetailInfo = mDataSet.DebtClientDetail.Where(cd => cd.id_bill_filter == idBillFilter).ToList<MDataSet.DebtClientDetailRow>()[0];
                if (clientDetailInfo.end_period < this.period)
                {
                    DAccess.DataModule.CreateClientDetail(
                        -1,
                        clientDetailInfo.id_client,
                        idDeriveCompany,
                        idBillFilter,
                        this.period,
                        this.period,
                        0,
                        -1,
                        "PK",
                        string.Empty,
                        2,
                        "Создан автоматически при печати долговых квитанций",
                        string.Empty);
                }
            }
        }

        private void SetStatusText(string text)
        {
            txtStatus.Text = text;
            txtStatus.Refresh();
        }
    }
}
