﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MrivcControls.Data
{
    using System.Data.SqlClient;

    using MrivcHelpers.Helpers;
    using System.Data;
    public partial class DModule : Component
    {
        public DModule()
        {
            InitializeComponent();
        }

        public DModule(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        public string ConnectionString
        {
            set
            {
                SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();

                //Буторин 27-04-16 Добавлена возможность входа с логином Windows
                if (isWinLogin(value))
                    connBuilder.IntegratedSecurity = true; 
                else
                {
                    connBuilder.UserID = value.GetSqlConnectionPart("UserID");
                    connBuilder.Password = value.GetSqlConnectionPart("Password");
                }

                connBuilder.InitialCatalog = value.GetSqlConnectionPart("InitialCatalog");
                connBuilder.DataSource = value.GetSqlConnectionPart("DataSource");
                connBuilder.PersistSecurityInfo = true;
                connBuilder.ConnectTimeout = 200;
                sqlMainConnection.Close();
                sqlMainConnection.ConnectionString = connBuilder.ConnectionString;
                sqlMainConnection.Open();
            }
            get
            {
                return sqlMainConnection.ConnectionString;
            }
        }

        bool isWinLogin(string connectionString)
        {
            string value = string.Empty;

            try
            {
                value = connectionString.GetSqlConnectionPart("IntegratedSecurity");
            }
            catch { }

            if (value.ToLower() == "true" || value.ToLower() == "sspi")
                return true;
            else
                return false;
        }

        SqlConnection connection1;
        public SqlConnection Connection1
        {
            get
            {
               

                return connection1;
            }
            set
            {
                
                connection1 = value;
                SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();
                connBuilder.ConnectionString = Connection1.ConnectionString;
                connBuilder.ConnectTimeout = 200;
                connection1.ConnectionString = connBuilder.ConnectionString;
                connection1.Open();
            }
        }

        public SqlConnection Connection
        {
            get
            {
                return this.sqlMainConnection;
            }
        }

        public void SetConnection(string connectionString)
        {
            sqlMainConnection.Close();
            sqlMainConnection.ConnectionString = connectionString;
            sqlMainConnection.Open();
        }

        public int GetCurrentUserId()
        {
            return (int)ExecuteScalarQueryCommand("select dbo.get_user_id()");
        }

        public bool CurrentUserIsAdmin()
        {
            return this.CurrentUserInRole(0, GetCurrentUserId());
        }

        public bool CurrentUserInRole(int idRole, int idEmployee)
        {
            object val = ExecuteScalarQueryCommand(string.Format("select * from employee_role_member where id_employee_role = {0} and id_employee = {1}", idRole, idEmployee));
            return val == null ? false : true;          
        }

        public int GetWorkPeriod()
        {
            return (int)ExecuteScalarQueryCommand("select dbo.Work_Period()");
        }

        void CheckConnection(SqlConnection connection)
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();
        }

        public void ExecuteNonQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.ExecuteNonQuery();
        }

        public void ExecuteNonQueryCommand1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;

            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();
        }

        public void ExecuteNonQueryCommand1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;


            for (int i = 0; i < prms.GetLength(0); i++)
            {
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }

            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();
        }

        public void ExecuteNonQueryStoredProcedure(string sql, object[,] prms)
        {
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.CommandType = CommandType.StoredProcedure;
            cmdAnyCommand.Parameters.Clear();

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                cmdAnyCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }

            cmdAnyCommand.ExecuteNonQuery();
            cmdAnyCommand.CommandType = CommandType.Text;
        }

        public void ExecuteNonQueryStoredProcedure1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql,Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }
            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();
        }

        public void ExecuteNonQueryStoredProcedure1(string sql, object[,] prms, int timeout)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = timeout;

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }
            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();
        }

        public void ExecuteNonQueryStoredProcedure1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;
            CheckConnection(sqlCommand.Connection);

            sqlCommand.ExecuteNonQuery();
        }

        public object ExecuteScalarStoredProcedure1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }
            CheckConnection(sqlCommand.Connection);

            return sqlCommand.ExecuteScalar();
        }

        public object ExecuteScalarStoredProcedure1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            CheckConnection(sqlCommand.Connection);

            return sqlCommand.ExecuteScalar();
        }

        public object ExecuteScalarQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            object res = cmdAnyCommand.ExecuteScalar();
            return res;
        }

        public object ExecuteScalarQueryCommand1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;
            CheckConnection(sqlCommand.Connection);

            object res = sqlCommand.ExecuteScalar();
            return res;
        }

        public object ExecuteScalarQueryCommand1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            CheckConnection(sqlCommand.Connection);

            object res = sqlCommand.ExecuteScalar();
            return res;
        }

        public object ExecuteScalarQueryCommand(string sql, object[,] prms)
        {
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.Parameters.Clear();

            for (int i = 0; i < prms.GetLength(0); i++)
                cmdAnyCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            object res = cmdAnyCommand.ExecuteScalar();
            return res;
        }

        public DataTable GetDataTableByQueryCommand(string sql, object[,] prms)
        {
            cmdAnyCommand.CommandText = sql;

            for (int i = 0; i < prms.GetLength(0); i++)
                cmdAnyCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var res = new DataTable();
            res.Load(cmdAnyCommand.ExecuteReader());
            cmdAnyCommand.Parameters.Clear();
            return res;
        }

        public DataTable GetDataTableByQueryCommand1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);


            res.Load(sqlCommand.ExecuteReader());
            return res;
        }

        public DataTable GetDataTableByQueryCommand1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);

          
            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);


            res.Load(sqlCommand.ExecuteReader());
            return res;
        }


        public DataTable GetDataTableByStoredProcedure1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);

            var reader = sqlCommand.ExecuteReader();

            res.Load(reader);
            reader.Close();
            return res;
        }

        public DataTable GetDataTableByStoredProcedure1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;
            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);

            var reader = sqlCommand.ExecuteReader();

            res.Load(reader);
            reader.Close();
            return res;
        }

        public void FillDataTable(string sql, DataTable table)
        {
            using (SqlDataAdapter adapter = new SqlDataAdapter(sql, Connection))
            {
                adapter.Fill(table);
            }
        }

        public DataSet GetDataSetByStoredProcedure1(string sql, object[,] prms, params string[] tableNames)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var tables = new DataTable[tableNames.Count()];
            var res = new DataSet();

            for (int i = 0; i < tableNames.Count(); i++)
            {
                tables[i] = new DataTable(tableNames[i]);
            }
            CheckConnection(sqlCommand.Connection);

            res.Tables.AddRange(tables);
            res.Load(sqlCommand.ExecuteReader(), LoadOption.OverwriteChanges, tables);
            return res;
        }

        public DataSet GetDataSetByQueryCommand1(string sql, object[,] prms, params string[] tableNames)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var tables = new DataTable[tableNames.Count()];
            var res = new DataSet();

            for (int i = 0; i < tableNames.Count(); i++)
            {
                tables[i] = new DataTable(tableNames[i]);
            }
            CheckConnection(sqlCommand.Connection);

            res.Tables.AddRange(tables);
            res.Load(sqlCommand.ExecuteReader(), LoadOption.OverwriteChanges,tables);
            return res;
        }

        public DataSet GetDataSetByQueryCommand1(string sql, params string[] tableNames)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);

            var tables = new DataTable[tableNames.Count()];
            var res = new DataSet();

            for (int i = 0; i < tableNames.Count(); i++)
            {
                tables[i] = new DataTable(tableNames[i]);
            }
            CheckConnection(sqlCommand.Connection);

            res.Tables.AddRange(tables);
            res.Load(sqlCommand.ExecuteReader(), LoadOption.OverwriteChanges, tables);
            return res;
        }

        //public DataTable GetDataTableByQueryCommand1(string sql)
        //{
        //    var sqlCommand = new SqlCommand(sql, Connection1);
        //    var res = new DataTable();
        //    CheckConnection(sqlCommand.Connection);

        //    res.Load(sqlCommand.ExecuteReader());
        //    return res;
        //}

        public DataTable GetDataTableByQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            var res = new DataTable();
            res.Load(cmdAnyCommand.ExecuteReader());
            return res;
        }

        public void PeriodSelect(CDataSet.PeriodDataTable table)
        {
            daPeriod.Fill(table);
        }

        public void PrintHistorySelect(CDataSet.Print_HistoryDataTable table, int period, int userId)
        {
            daPrintHistory.SelectCommand.Parameters["@Period"].Value = period;
            daPrintHistory.SelectCommand.Parameters["@PrintBy"].Value = userId;
            daPrintHistory.Fill(table);
        }

        public void PrintHistoryLogSelect(CDataSet.Print_History_LogDataTable table, long idPrint)
        {
            daPrintHistoryLog.SelectCommand.Parameters["@IdPrint"].Value = idPrint;
            daPrintHistoryLog.Fill(table);
        }

        public void PrintHistoryUpdate(CDataSet.Print_HistoryDataTable table)
        {
            daPrintHistory.Update(table);
        }

        public void PrintHistoryDetailSelect(CDataSet.Print_History_DetailDataTable table, long idPrint)
        {
            daPrintHistoryDetail.SelectCommand.Connection.Close();
            daPrintHistoryDetail.SelectCommand.Parameters["@ID_Print"].Value = idPrint;
            daPrintHistoryDetail.SelectCommand.Connection.Open();
            daPrintHistoryDetail.Fill(table);
        }

        public void PrintHistoryPivotDataSelect(CDataSet.PrintPivotDataDataTable table, int period, bool showAllBill)
        {
            daPrintHistoryPivotData.SelectCommand.Connection.Close();
            daPrintHistoryPivotData.SelectCommand.Parameters["@Period"].Value = period;
            daPrintHistoryPivotData.SelectCommand.Parameters["@ShowAllBill"].Value = showAllBill;
            daPrintHistoryPivotData.SelectCommand.Connection.Open();
            daPrintHistoryPivotData.Fill(table);
        }

        public void PrintHistoryDetailUpdate(CDataSet.Print_History_DetailDataTable table)
        {
            daPrintHistoryDetail.Update(table);
        }

        public void EmployeeSelect(CDataSet.EmployeeDataTable table)
        {
            daEmployee.Fill(table);
        }

        public void CompanySelect(CDataSet.CompanyDataTable table)
        {
            daCompany.Fill(table);
        }

        public void BillKindSelect(CDataSet.Bill_KindDataTable table)
        {
            daBillKind.Fill(table);
        }

        public void DeliveryTypeSelect(CDataSet.Print_Delivery_TypeDataTable table)
        {
            daDeliveryType.Fill(table);
        }

        public void ClientsSelect(CDataSet.CompanyDataTable table)
        {
            FillDataTable("select * from dbo.Company where ID_Company in (select ID_Client from  Reports.Client_Detail) order by name", table);
        }

        public void StatisticByServicedAccountsSelect(NDataSet.Statistic_By_Serviced_AccountsDataTable table, int period, string clients)
        {
            SqlCommand command = new SqlCommand($"exec [Reports].[Statistic_By_Serviced_Accounts_Select] {period.ToString()}, '{clients}'", Connection);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    object[] row = new object[reader.FieldCount];
                    reader.GetValues(row);
                    table.LoadDataRow(row, LoadOption.PreserveChanges);
                }
            }
            reader.Close();
        }

        public void StatisticByServicedAccountsUpdate(int period)
        {
            ExecuteNonQueryCommand($"exec [Reports].[Statistic_By_Serviced_Accounts_Update] {period.ToString()}");
        }

        public void AddressFilterTreeSelect(CDataSet.AddressFilterTreeDataTable table, int type)
        {
            daAddressFilterTree.SelectCommand.Connection.Close();
            daAddressFilterTree.SelectCommand.Parameters["type"].Value = type;
            daAddressFilterTree.SelectCommand.Connection.Open();
            daAddressFilterTree.Fill(table);
        }

        public void AddressFilterTreeSelect(CDataSet.AddressFilterTreeDataTable table, int type, int id_parent, int id_company)
        {
            daAddressFilterTree.SelectCommand.Connection.Close();
            daAddressFilterTree.SelectCommand.Parameters["type"].Value = type;
            daAddressFilterTree.SelectCommand.Parameters["id_parent"].Value = id_parent;
            daAddressFilterTree.SelectCommand.Parameters["id_company"].Value = id_company;
            daAddressFilterTree.SelectCommand.Connection.Open();
            daAddressFilterTree.Fill(table);
        }

        public void DefferedCalculationSelect(CDataSet.DefferedCalculationDataTable table)
        {
            daDefferedCalculation.Fill(table);
        }

        //Закрыть счетчик по idMeter датой removeDate
        public void RemoveMeter(int idMeter, DateTime removeDate)
        {
            string sql = "town.RemoveMeter";
            object[,] prms = new object[,] { { "id_meter", idMeter }, { "date", removeDate } };

            ExecuteNonQueryStoredProcedure(sql,prms);
        }

        public long PrepareRepotData(int period, int id_bill_kind, int[] id_accounts, bool saveHistory, bool backSide, bool printBillsForSubscribers)
        {
            string idPrint = period + StringHelper.ZeroCode(DateTime.Now.Month) + StringHelper.ZeroCode(DateTime.Now.Day) +
                StringHelper.ZeroCode(DateTime.Now.Hour) + StringHelper.ZeroCode(DateTime.Now.Minute) + StringHelper.ZeroCode(DateTime.Now.Millisecond);

            string sql = $@"execute [Reports].[Prepare_Bill_Report_Data]  
                           @ID_Print = {idPrint}  
                         , @ID_Bill_Kind =   {id_bill_kind}
                         , @Period =  {period} 
                         , @Print_Zero_Param = 0
                         , @Print_Empty_Flat = 0
                         , @Id_company = null
                         , @saveprinthistory = {saveHistory} 
                         , @PrintedBackSide =  {backSide} 
                         , @PrintBillsForSubscribers =  {printBillsForSubscribers}";

            string sqlTableAccountCreate = "create table #__Account(ID int)\n";

            foreach (var id_account in id_accounts)
                sqlTableAccountCreate += $"insert into #__Account(ID) values ({id_account})\n";

            string finalSql = sqlTableAccountCreate + Environment.NewLine + sql;

            DAccess.DataModule.ExecuteNonQueryCommand(finalSql);

            return long.Parse(idPrint);
        }

        string NameByID (int id, string table, string idColumn, string nameColumn) =>
            ExecuteScalarQueryCommand($"select {nameColumn} from {table} where {idColumn} = @{idColumn}", new object[,] { { idColumn, id } }).ToString();

        public string CompanyByID(int idCompany) => NameByID(idCompany, "dbo.company", "id_company", "name");

        public string BillKindByID(int idBillKind) => NameByID(idBillKind, "reports.bill_kind", "id_bill_kind", "bill_kind");

        public string AccountByID(int idAccount) => NameByID(idAccount, "dbo.account", "id_account", "account");

        public string PeriodByID(int period) => NameByID(period, "dbo.vperiod", "period", "period_name");

        public DataTable GetBillKindsByClient(int period, int idClient)
        {
            DataTable dt = GetDataTableByQueryCommand("exec town.Get_Bill_Kinds_By_Client @period, @id_client", new object[,] { { "period", period }, { "id_client", idClient } });
            return dt;
        }

        public void BulkCopy(string destinationTableName, DataTable sourceTable, string[,] columnMappings)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(Connection1))
            {
                bulkCopy.DestinationTableName = destinationTableName;

                for (int i = 0; i < columnMappings.GetLength(0); i++)
                    bulkCopy.ColumnMappings.Add(columnMappings[i, 0].ToString(), columnMappings[i, 1].ToString());

                bulkCopy.WriteToServer(sourceTable);
            }
        }

        public void BulkCopy(string destinationTableName, DataTable sourceTable)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(Connection1))
            {
                bulkCopy.DestinationTableName = destinationTableName;
                bulkCopy.WriteToServer(sourceTable);
            }
        }

        public void TruncateTable(params string[] tableNames)
        {
            foreach (var tableName in tableNames)
            {
                ExecuteNonQueryCommand1($"Truncate table {tableName}");
            }
        }

        public void CopyDataTableToTempDB(DataTable table)
        {
            var sql = "";
            var tempTable = $"#{table.TableName}";
            sql += $"if object_id('tempdb..{tempTable}') is not null drop table {tempTable}";
            sql += Environment.NewLine;
            sql += $"create table {tempTable} (";
            sql += Environment.NewLine;
            foreach (DataColumn column in table.Columns)
            {
                sql += $"[{column.ColumnName}] varchar(1000),";
                sql += Environment.NewLine;
            }
            sql.TrimEnd(',');
            sql += Environment.NewLine;
            sql += ")";
            sql += Environment.NewLine;

            DAccess.DataModule.ExecuteNonQueryCommand1(sql);
            DAccess.DataModule.BulkCopy($"#{table.TableName}", table);
        }

        public void CopyDataSetToTempDB(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                CopyDataTableToTempDB(dt);
            }
        }


        public string GisCommandReport(string senderID, DateTime date, bool succesful, DateTime endDate)
        {
            var dt = GetDataTableByStoredProcedure1("GIS.PrepareCommandReport", new object[,] { { "senderID", senderID }, { "date", date }, { "succesful", succesful }, { "order", 1 }, { "endDate", endDate } });
            string text = "";

            if (dt.Rows.Count != 0)
            {
                text += "Внимание! Описание ошибок находится в отдельном файле \"Описание ошибок\"";
                text += Environment.NewLine + Environment.NewLine;
            }

            foreach (DataRow row in dt.Rows)
            {
                

                text += row["house_address"] + " | " + row["command"] + Environment.NewLine;

                if (row["errormessage"] == DBNull.Value)
                {
                    text += "Успешно";
                }
                else
                {
                    text += "Код ошибки: " + row["errorcode"] + " Ошибка: " + row["errormessage"];
                }

                text += Environment.NewLine + Environment.NewLine;
            }
            return text;
        }

        public string GisErrorDescriptionReport(string senderID, DateTime date)
        {
            var dt = GetDataTableByStoredProcedure1("GIS.PrepareCommandReport", new object[,] { { "senderID", senderID }, { "date", date }, { "succesful", false }, { "order", 2 } });
            string text = "";
            foreach (DataRow row in dt.Rows)
            {
                text += "Код ошибки: " + row["errorcode"] + " Ошибка: " + row["errormessage"] + Environment.NewLine + "Описание ошибки: " + row["description"];
                text += Environment.NewLine + Environment.NewLine;
            }
            return text;
        }

        public bool CreateGisCommandDescriptionExcelReport(string senderID, DateTime date, string path)
        {
            var dt = GetDataTableByStoredProcedure1("GIS.PrepareCommandReport", new object[,] { { "senderID", senderID }, { "date", date }, { "succesful", false }, { "order", 2 } });

            path = Environment.CurrentDirectory + @"\" + path;

            dt.TableName = "Report";
            if (dt.Columns.Count != 0)
            {
                dt.Columns["errorcode"].ColumnName = "Код ошибки";
                dt.Columns["errormessage"].ColumnName = "Ошибка";
                dt.Columns["description"].ColumnName = "Описание";
            }

            if (dt.Rows.Count != 0)
            {
                ExcelHelper2.ExportToExcel(dt, path);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool CreateGisCommandExcelReport(string senderID, DateTime date, bool succesful, string path)
        {
            var dt = GetDataTableByStoredProcedure1("GIS.PrepareCommandReport", new object[,] { { "senderID", senderID }, { "date", date }, { "succesful", succesful } });

            path = Environment.CurrentDirectory + @"\" + path;

            dt.TableName = "Report";
            if (dt.Columns.Count != 0)
            {
                dt.Columns["senderName"].ColumnName = "Организация";
                dt.Columns["commandCreationDate"].ColumnName = "Дата";
                dt.Columns["command"].ColumnName = "Команда";
                dt.Columns["house_address"].ColumnName = "Адрес";
                dt.Columns["errorcode"].ColumnName = "Код ошибки";
                dt.Columns["errormessage"].ColumnName = "Ошибка";
                dt.Columns["description"].ColumnName = "Описание";
            }

            if (dt.Rows.Count != 0)
            {
                ExcelHelper2.ExportToExcel(dt, path);
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataTable GetDatabaseTables(string databaseName)
        {
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder(ConnectionString);
            conn.InitialCatalog = string.IsNullOrEmpty(databaseName) ? Connection.Database : databaseName;

            using (SqlConnection conenction = new SqlConnection(conn.ConnectionString))
            {
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter($"SELECT ('[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']') as Name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG='{databaseName}' order by '[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']'", conenction);
                da.Fill(dt);
                return dt;
            }
        }

        public DataTable GetTableColumns(string databaseName, string tableName)
        {
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder(ConnectionString);
            conn.InitialCatalog = string.IsNullOrEmpty(databaseName) ? Connection.Database : databaseName;

            using (SqlConnection conenction = new SqlConnection(conn.ConnectionString))
            {
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter($"SELECT COLUMN_NAME as Name FROM {databaseName}.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{tableName}'", conenction);
                da.Fill(dt);
                return dt;
            }

        }
    }
}
