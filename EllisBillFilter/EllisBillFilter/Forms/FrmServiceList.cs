﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using System.Collections;

namespace EllisBillFilter
{
    public partial class FrmServiceList : Form
    {
        string checkedList;
        public FrmServiceList()
        {
            InitializeComponent();
            LoadData();
        }

        public FrmServiceList(string checkedList)
        {
            InitializeComponent();
            LoadData();
            this.checkedList = checkedList;
        }

        private void InitCheck()
        {
            foreach(string s in checkedList.Split(','))
            {
                int id_service;

                if (int.TryParse(s.Trim(), out id_service))
                {
                    int index = FindIndexByIdService(id_service);
                    if (index > -1)
                        checkedListBoxControl1.SetItemChecked(index, true);
                }
            }
        }

        void LoadData()
        {
            DAccess.DataModule.ServiceSelect(mDataSet.Service);
        }

        public string SelectedServices
        {
            get {
                string s = string.Empty;
                foreach (DataRowView item in checkedListBoxControl1.CheckedItems)
                    s += "," + item["ID_Service"].ToString();

                if (s.Length > 1)
                    return s.Substring(1);
                else
                    return string.Empty;
            }
        }

        private int FindIndexByIdService(int id)
        {
            for (int i = 0; i < (checkedListBoxControl1.DataSource as IList).Count; i++)
            {
                DataRowView rowView = checkedListBoxControl1.GetItem(i) as DataRowView;
                if (rowView != null && (int)rowView["ID_Service"] == id)
                    return i;
            }
            return -1;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmServiceList_Load(object sender, EventArgs e)
        {
            InitCheck();
        }
    }
}
