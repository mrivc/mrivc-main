﻿namespace XadesDemo
{
    partial class ImportBillForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportBillForm));
            this.importBtn = new System.Windows.Forms.Button();
            this.prepareBtn = new System.Windows.Forms.Button();
            this.payInfoGridView = new System.Windows.Forms.DataGridView();
            this.billDataGrid = new System.Windows.Forms.DataGridView();
            this.munServDataGrid = new System.Windows.Forms.DataGridView();
            this.capitalRepairDataGrid = new System.Windows.Forms.DataGridView();
            this.capitalRepairBillgridView = new System.Windows.Forms.DataGridView();
            this.billTabControl = new System.Windows.Forms.TabControl();
            this.billTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.capitalRepairBillTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.sumCheckDataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.payInfoGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.munServDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capitalRepairDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capitalRepairBillgridView)).BeginInit();
            this.billTabControl.SuspendLayout();
            this.billTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.capitalRepairBillTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sumCheckDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // importBtn
            // 
            this.importBtn.Enabled = false;
            this.importBtn.Location = new System.Drawing.Point(528, 41);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(134, 23);
            this.importBtn.TabIndex = 1;
            this.importBtn.Text = "Импорт";
            this.importBtn.UseVisualStyleBackColor = true;
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // prepareBtn
            // 
            this.prepareBtn.Location = new System.Drawing.Point(528, 12);
            this.prepareBtn.Name = "prepareBtn";
            this.prepareBtn.Size = new System.Drawing.Size(134, 23);
            this.prepareBtn.TabIndex = 2;
            this.prepareBtn.Text = "Просмотр";
            this.prepareBtn.UseVisualStyleBackColor = true;
            this.prepareBtn.Click += new System.EventHandler(this.prepareBtn_Click);
            // 
            // payInfoGridView
            // 
            this.payInfoGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.payInfoGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.payInfoGridView.Location = new System.Drawing.Point(3, 3);
            this.payInfoGridView.Name = "payInfoGridView";
            this.payInfoGridView.Size = new System.Drawing.Size(954, 436);
            this.payInfoGridView.TabIndex = 3;
            // 
            // billDataGrid
            // 
            this.billDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.billDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billDataGrid.Location = new System.Drawing.Point(0, 0);
            this.billDataGrid.Name = "billDataGrid";
            this.billDataGrid.Size = new System.Drawing.Size(954, 219);
            this.billDataGrid.TabIndex = 8;
            this.billDataGrid.SelectionChanged += new System.EventHandler(this.billDataGrid_SelectionChanged);
            // 
            // munServDataGrid
            // 
            this.munServDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.munServDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.munServDataGrid.Location = new System.Drawing.Point(3, 3);
            this.munServDataGrid.Name = "munServDataGrid";
            this.munServDataGrid.Size = new System.Drawing.Size(940, 181);
            this.munServDataGrid.TabIndex = 9;
            // 
            // capitalRepairDataGrid
            // 
            this.capitalRepairDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.capitalRepairDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.capitalRepairDataGrid.Location = new System.Drawing.Point(3, 3);
            this.capitalRepairDataGrid.Name = "capitalRepairDataGrid";
            this.capitalRepairDataGrid.Size = new System.Drawing.Size(940, 204);
            this.capitalRepairDataGrid.TabIndex = 12;
            // 
            // capitalRepairBillgridView
            // 
            this.capitalRepairBillgridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.capitalRepairBillgridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.capitalRepairBillgridView.Location = new System.Drawing.Point(0, 0);
            this.capitalRepairBillgridView.Name = "capitalRepairBillgridView";
            this.capitalRepairBillgridView.Size = new System.Drawing.Size(954, 196);
            this.capitalRepairBillgridView.TabIndex = 13;
            // 
            // billTabControl
            // 
            this.billTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.billTabControl.Controls.Add(this.billTabPage);
            this.billTabControl.Controls.Add(this.capitalRepairBillTabPage);
            this.billTabControl.Controls.Add(this.tabPage2);
            this.billTabControl.Controls.Add(this.tabPage3);
            this.billTabControl.Location = new System.Drawing.Point(12, 73);
            this.billTabControl.Name = "billTabControl";
            this.billTabControl.SelectedIndex = 0;
            this.billTabControl.Size = new System.Drawing.Size(968, 468);
            this.billTabControl.TabIndex = 14;
            // 
            // billTabPage
            // 
            this.billTabPage.Controls.Add(this.splitContainer2);
            this.billTabPage.Location = new System.Drawing.Point(4, 22);
            this.billTabPage.Name = "billTabPage";
            this.billTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.billTabPage.Size = new System.Drawing.Size(960, 442);
            this.billTabPage.TabIndex = 0;
            this.billTabPage.Text = "Основные услуги";
            this.billTabPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.billDataGrid);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer2.Size = new System.Drawing.Size(954, 436);
            this.splitContainer2.SplitterDistance = 219;
            this.splitContainer2.TabIndex = 13;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(954, 213);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.munServDataGrid);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(946, 187);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Услуги";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // capitalRepairBillTabPage
            // 
            this.capitalRepairBillTabPage.Controls.Add(this.splitContainer1);
            this.capitalRepairBillTabPage.Location = new System.Drawing.Point(4, 22);
            this.capitalRepairBillTabPage.Name = "capitalRepairBillTabPage";
            this.capitalRepairBillTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.capitalRepairBillTabPage.Size = new System.Drawing.Size(960, 442);
            this.capitalRepairBillTabPage.TabIndex = 1;
            this.capitalRepairBillTabPage.Text = "Капитальный ремонт";
            this.capitalRepairBillTabPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.capitalRepairBillgridView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl2);
            this.splitContainer1.Size = new System.Drawing.Size(954, 436);
            this.splitContainer1.SplitterDistance = 196;
            this.splitContainer1.TabIndex = 14;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(954, 236);
            this.tabControl2.TabIndex = 13;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.capitalRepairDataGrid);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(946, 210);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Услуги";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.payInfoGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(960, 442);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Платежные реквизиты";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.sumCheckDataGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(960, 442);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Проверка сумм";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // sumCheckDataGridView
            // 
            this.sumCheckDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sumCheckDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sumCheckDataGridView.Location = new System.Drawing.Point(3, 3);
            this.sumCheckDataGridView.Name = "sumCheckDataGridView";
            this.sumCheckDataGridView.Size = new System.Drawing.Size(954, 436);
            this.sumCheckDataGridView.TabIndex = 16;
            // 
            // ImportBillForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 547);
            this.Controls.Add(this.billTabControl);
            this.Controls.Add(this.prepareBtn);
            this.Controls.Add(this.importBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportBillForm";
            this.Text = "Синхронизация платежных документов";
            this.Load += new System.EventHandler(this.ImportBillForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.payInfoGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.munServDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capitalRepairDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capitalRepairBillgridView)).EndInit();
            this.billTabControl.ResumeLayout(false);
            this.billTabPage.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.capitalRepairBillTabPage.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sumCheckDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button importBtn;
        private System.Windows.Forms.Button prepareBtn;
        private System.Windows.Forms.DataGridView payInfoGridView;
        private System.Windows.Forms.DataGridView billDataGrid;
        private System.Windows.Forms.DataGridView munServDataGrid;
        private System.Windows.Forms.DataGridView capitalRepairDataGrid;
        private System.Windows.Forms.DataGridView capitalRepairBillgridView;
        private System.Windows.Forms.TabControl billTabControl;
        private System.Windows.Forms.TabPage billTabPage;
        private System.Windows.Forms.TabPage capitalRepairBillTabPage;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView sumCheckDataGridView;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
    }
}