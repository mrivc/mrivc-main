﻿using MrivcControls.Data;
using MrivcHelpers.Helpers;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace MassBillNew
{
    public partial class SingleBillByClient : Form
    {
        int period;

        public SingleBillByClient()
        {
            InitializeComponent();
        }

        void PrepareAndSave(int idClient, string path)
        {
            var dt = DAccess.DataModule.GetBillKindsByClient(period, idClient);

            foreach (DataRow row in dt.Rows)
            {
                int id_bill_kind = int.Parse(row["id_bill_kind"].ToString());
                int[] id_account = new int[] { int.Parse(row["id_account"].ToString()) };

                var companyName = DAccess.DataModule.CompanyByID(idClient);
                var billKindName = DAccess.DataModule.BillKindByID(id_bill_kind);
                var account = DAccess.DataModule.AccountByID(id_account[0]);
                var periodName = DAccess.DataModule.PeriodByID(period);

                var fileName =  $"{companyName} {periodName} {account} {billKindName}";
                fileName = FileHelper.CreateCorrectFileName(fileName);
                fileName = path + fileName;

                FastReport.Report report = ReportHelper.PrepareBillReport(period, id_bill_kind, id_account, false, false, false, false);

                ReportHelper.ExportFastReportToPdf(report, fileName);
            }
        }

        private void execBtn_Click(object sender, EventArgs e)
        {
            period = periodPicker1.Period;

            var idClients = StringHelper.CreateListFromMultiLineString(clientsTextBox.Text);
            idClients = idClients.Distinct().ToList();

            clientsTextBox.Clear();

            foreach (var item in idClients)
                clientsTextBox.Text += item + Environment.NewLine;

            FolderBrowserDialog folderDlg = new FolderBrowserDialog();

            string savePath = "";
            if (folderDlg.ShowDialog() == DialogResult.OK)
                savePath = $@"{folderDlg.SelectedPath}\";
            else
                return;

            foreach (var idClient in idClients)
            {
                int _idClient = int.Parse(idClient);
                PrepareAndSave(_idClient, savePath);
            }

            MessageBox.Show("Квитанции успешно сформированы", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void SingleBillByClient_Load(object sender, EventArgs e)
        {

            periodPicker1.Fill(DAccess.DataModule.ConnectionString);
            //periodPicker1.ConnectionString = DAccess.DataModule.ConnectionString;
        }
    }
}
