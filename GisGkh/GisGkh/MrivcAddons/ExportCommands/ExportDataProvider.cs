﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportDataProvider : MrivcExportCommand
    {
        ExportDataProviderItem exportDataProviderItem;

        public ExportDataProvider(string senderID) : base("exportDataProviderNsiItem", "NsiAsync", senderID, "NsiItem")
        {
            exportDataProviderItem = new ExportDataProviderItem(SenderID);
        }

        public new void ExecuteToDataBase(string[] nsiRegistryNumbers)
        {
            exportDataProviderItem.AddToReport = this.AddToReport;
            exportDataProviderItem.Description = this.Description;
            exportDataProviderItem.CommandType = this.CommandType;

            foreach (var regNumber in nsiRegistryNumbers)
            {
                try
                {
                    exportDataProviderItem.ExecuteToTempDB(regNumber);

                    if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
                    {
                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportDataProviderNsiItems", new object[,] { { "@senderID", SenderID }, { "@registryNumber", regNumber } });
                    }                    
                }
                catch (InvalidOperationException)
                {}
            }

            this.HasErrors = exportDataProviderItem.HasErrors;
        }
    }
}
