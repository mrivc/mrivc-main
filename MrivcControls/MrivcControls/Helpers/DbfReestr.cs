﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data.Common;

namespace MrivcControls
{
    using MrivcHelpers.Helpers;

    public class DbfReestr
    {
        public static DateTime LastUnloadedPaymentsDate = DateTime.Today;
        private static string lastConnectionString = string.Empty;


        public static void UnloadToDBF(string unloadedPath, string fileName, DataTable dataTable)
        {
            UnloadToDBF(unloadedPath, fileName, dataTable, string.Empty, false);
        }

        public static void UnloadToDBF(string unloadedPath, string fileName, DataTable dataTable, string clientName, bool addPokFields)
        {
            string templatePath = Application.StartupPath + "\\" + "TEMPLATE.DBF";

            string unloadedFileName;
            if (fileName.Length > 8)
                unloadedFileName = fileName.Substring(0, 8); //уменьшаем имя файла до 8ми знаков
            else
                unloadedFileName = fileName;

            if (!FileHelper.ExistFile(templatePath))
                throw new Exception("Не найден файл шаблона " + templatePath + ". Восстановите файл шаблона и повторите выгрузку.");

            if (!FileHelper.DirectoryExists(unloadedPath))
                FileHelper.CreateDirectory(unloadedPath);

            if (!unloadedPath.EndsWith("\\"))
                unloadedPath = unloadedPath + "\\";
                
            FileHelper.CopyFile(templatePath, unloadedPath + unloadedFileName + ".DBF");

            DbConnection con = GetConnection(unloadedPath);
            DbCommand cmd = GetCommand();

            cmd.Connection = con;
            con.Open();
            cmd.CommandText = "delete from " + unloadedFileName;
            cmd.ExecuteNonQuery();

            if (addPokFields)
            {
                cmd.CommandText = "ALTER TABLE " + unloadedFileName + " ADD COLUMN POK1NEW NUMERIC(20,5)";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "ALTER TABLE " + unloadedFileName + " ADD COLUMN POK2NEW NUMERIC(20,5)";
                cmd.ExecuteNonQuery();
            }

            foreach (DataRow row in dataTable.Rows)
            {
                string insertSql = "insert into " + unloadedFileName + " values("
                    + "'" + row["LS"].ToString() + "',"
                    + "'" + (StringHelper.ReplaceEscape(row["ADRES"].ToString())) + "',"
                    + "'" + (StringHelper.ReplaceEscape(row["UK"].ToString())) + "',"
                    + "'" + (row["FAM"].ToString()) + "',"
                    + "'" + (row["IM"].ToString()) + "',"
                    + "'" + (row["OT"].ToString()) + "',"
                    + "'" + row["AUTHCOD_LONG"].ToString() + "',"
                    + "'" + row["KVITID"].ToString() + "',"
                    //+ "'" + row["DATPLAT"].ToString() + "',"
                    //+ "" + row["SUMPLAT"].ToString().Replace(",",".") + ","
                    + "'" + row["PU1"].ToString() + "',"
                    + "" + row["POK1"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU2"].ToString() + "',"
                    + "" + row["POK2"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU3"].ToString() + "',"
                    + "" + row["POK3"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU4"].ToString() + "',"
                    + "" + row["POK4"].ToString().Replace(",", ".") + ","
                    + "null,null"
                    + (addPokFields ? ",null,null" : "")
                    + ")";

                cmd.CommandText = insertSql;
                cmd.ExecuteNonQuery();
            }

            //возвращаем файлу исходное наименование
            FileHelper.RewriteFileWithRename(unloadedPath + unloadedFileName + ".DBF", unloadedPath + fileName + ".DBF");
            lastConnectionString = con.ConnectionString;
            con.Close();
        }

        //public static string CompressLastUploadedFiles(bool deleteOriginalFiles)
        //{
        //    List<string> files = new List<string>();
        //    string info = "";

        //    using (ZipFile zip = new ZipFile())
        //    {
        //        foreach (KeyValuePair<string, string> kvp in unloadedFiles)
        //        {
        //            files.Add(kvp.Key);
        //            info += kvp.Value + "\n";
        //        }

        //        FileHelper.WriteTextFile(UnloadedPath + "Information.txt", info, Encoding.UTF8);

        //        string saveZippedFile = UnloadedPath + LastUnloadedPaymentsDate.Year.ToString() +
        //                                    StringHelper.ZeroCode(LastUnloadedPaymentsDate.Month) +
        //                                    StringHelper.ZeroCode(LastUnloadedPaymentsDate.Day) + "_" +
        //                                    StringHelper.ZeroCode(DateTime.Now.Hour) +
        //                                    StringHelper.ZeroCode(DateTime.Now.Minute) + "_" +
        //                                    DAccess.Instance.CurrentUser.Name + ".zip";
        //        zip.AddFiles(files, false, "");
        //        zip.AddFile(UnloadedPath + "Information.txt", "");
        //        zip.Save(saveZippedFile);

        //        if (deleteOriginalFiles)
        //            FileHelper.DeleteFiles(files);

        //        FileHelper.DeleteFileIfExist(UnloadedPath + "Information.txt");
        //        return saveZippedFile;
        //    }
        //}

        //public static void ClearUnloadedFiles()
        //{
        //    unloadedFiles.Clear();
        //}

        public static string LastConnectionString
        {
            get {
                return lastConnectionString;
            }
        }

        private static DbConnection GetConnection(string path)
        {
          //  if (Settings.Default.DbfReestrUseOleDbConnection)
            return new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=FileNameHere;Extended Properties=dBASE IV;".Replace("FileNameHere", path));
          //  else
          //  return new OdbcConnection("Driver={Microsoft dBASE Driver (*.dbf)};DriverID=277;Dbq=FileNameHere;".Replace("FileNameHere", path));
        }

        private static DbCommand GetCommand()
        {
           // if (Settings.Default.DbfReestrUseOleDbConnection)
                return new OleDbCommand();
           // else
           //     return new OdbcCommand();
        }
    }
}
