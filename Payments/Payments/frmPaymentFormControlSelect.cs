﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Payments.DS;

namespace Payments
{
    public partial class frmPaymentFormControlSelect : Form
    {
        int personIdentificationSelectedID;

        public frmPaymentFormControlSelect()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            DAccess.DataModule.PersonIdentificationSelect(paymentsDataSet.PersonIdentification, -1);
            baseGrid1.InitEx();
            baseGrid1.DisableGrid();
        }

        private void frmPaymentFormControlSelect_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        public int PersonIdentificationSelectedID
        {
            get { return personIdentificationSelectedID; }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            personIdentificationSelectedID = (int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID"];
            Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            paymentsDataSet.PersonIdentification.Clear();
            DAccess.DataModule.PersonIdentificationSearch(paymentsDataSet.PersonIdentification, txtLastNameSearch.Text);
        }

        private void txtLastNameSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                paymentsDataSet.PersonIdentification.Clear();
                DAccess.DataModule.PersonIdentificationSearch(paymentsDataSet.PersonIdentification, txtLastNameSearch.Text);
            }
        }
    }
}
