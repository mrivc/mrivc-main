﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class CloseAccountData : MrivcImportCommand, ISingleHouseCommand
    {
        bool _isRSO;
        bool _preview = true;

        public string FiasHouseGuid { get; set; } = null;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = (bool)DAccess.DataModule.ExecuteScalarQueryCommand1("select isRso from gis.Provider where senderID = @senderID", new[,] { { "senderID", value } });
                base.SenderID = value;
            }
        }

        public bool CapitalRepair { get; set; }

        public CloseAccountData(string senderID) : base("importAccountData", "HomeManagementAsync", senderID)
        {
            _maxImportValue = 100;
            _importValueName = "Account";
            Description = "Закрытие лицевых счетов";
        }

        public void Execute()
        {
            //bool check = (bool)DAccess.DataModule.ExecuteScalarStoredProcedure1("gis.CheckAccountDataFull", new[,] { { "@senderID", SenderID }, { "@fiasHouseGuid", FiasHouseGuid } });
            bool check = true;
            base.Execute();

            if (!this.IsEmpty && !HasErrors)
            {
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.DeleteClosedAccount", new object[,] { { "@SenderID", SenderID }, { "@FiasHouseGuid", FiasHouseGuid }, { "@CapitalRepair", CapitalRepair } });

                var exportAccountData = new ExportAccountData(SenderID);
                exportAccountData.FiasHouseGuid = FiasHouseGuid;
                exportAccountData.Description = Description;
                exportAccountData.AddToReport = AddToReport;
                exportAccountData.Execute();
            }
        }

        public DataSet PrepareToDataSet()
        {
            var exportAccountData = new ExportAccountData(SenderID);
            exportAccountData.FiasHouseGuid = FiasHouseGuid;
            exportAccountData.Description = Description;
            exportAccountData.AddToReport = AddToReport;
            exportAccountData.Execute();

            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.PrepareCloseAccount", new object[,] { { "@SenderID", SenderID }, { "@empty", exportAccountData.HasErrors }, { "@preview", _preview }, { "@FiasHouseGuid", FiasHouseGuid }, { "@CapitalRepair", CapitalRepair } }, "Account");
        }

        protected override void CreateXmlBody()
        {
            _preview = false;
            var _preparedDataSet = PrepareToDataSet();
            _preview = true;

            var updateValues = new List<Tuple<string[], string>>
            {
                 new Tuple<string[], string>(new [] { "AccountGUID" }, "AccountGUID" ),
                 new Tuple<string[], string>(new [] { "Closed","CloseDate" }, "CloseDate" ),
                 new Tuple<string[], string>(new [] { "Closed", "CloseReason", "Code" }, "Code" ),
                 new Tuple<string[], string>(new [] { "Closed", "CloseReason", "GUID" }, "GUID" ),
                 new Tuple<string[], string>(new [] { "Closed", "CloseReason", "Name" }, "Name" )
            };

            if (!_isRSO)
            {
                if (!CapitalRepair)
                {
                        var commonValues = new List<Tuple<string[], string>>
                    {
                            new Tuple<string[], string>(new [] { "isUOAccount" }, "isUOAccount" ),
                            new Tuple<string[], string>(new [] { "TotalSquare" }, "TotalSquare" ),
                            new Tuple<string[], string>(new [] { "Accommodation", "PremisesGUID" }, "PremisesGUID" ),
                            new Tuple<string[], string>(new [] { "PayerInfo","Ind","Surname" }, "Surname" ),
                            new Tuple<string[], string>(new [] { "PayerInfo", "Ind", "FirstName" }, "FirstName" ),
                            new Tuple<string[], string>(new [] { "AccountNumber" }, "AccountNumber" ),
                            new Tuple<string[], string>(new [] { "CreationDate" }, "CreationDate" )
                    };

                        foreach (DataRow row in _preparedDataSet.Tables["Account"].Select("isUOAccount = 'true'"))
                        {
                            AddChildElement(row, "HomeManagementAsync", "importAccount_close", "importAccount_close", commonValues, updateValues, updateValues, "importAccountRequest");
                            CounterCheck();
                        }
                }

                if (CapitalRepair)
                {
                    var commonValues = new List<Tuple<string[], string>>
                {
                        new Tuple<string[], string>(new [] { "isCRAccount" }, "isCRAccount" ),
                        new Tuple<string[], string>(new [] { "TotalSquare" }, "TotalSquare" ),
                        new Tuple<string[], string>(new [] { "Accommodation", "PremisesGUID" }, "PremisesGUID" ),
                        new Tuple<string[], string>(new [] { "PayerInfo","Ind","Surname" }, "Surname" ),
                        new Tuple<string[], string>(new [] { "PayerInfo", "Ind", "FirstName" }, "FirstName" ),
                        new Tuple<string[], string>(new [] { "AccountNumber" }, "AccountNumber" ),
                        new Tuple<string[], string>(new [] { "CreationDate" }, "CreationDate" )
                };

                    foreach (DataRow row in _preparedDataSet.Tables["Account"].Select("isCRAccount = 'true'"))
                    {
                        AddChildElement(row, "HomeManagementAsync", "importAccount_cr_close", "importAccount_cr_close", commonValues, null, updateValues, "importAccountRequest");
                        CounterCheck();
                    }
                }
               
            }
            else
            {
                //var commonValues = new List<Tuple<string[], string>>
                //{
                //        new Tuple<string[], string>(new [] { "isRSOAccount" }, "isRSOAccount" ),
                //        new Tuple<string[], string>(new [] { "TotalSquare" }, "TotalSquare" ),
                //        new Tuple<string[], string>(new [] { "Accommodation", "PremisesGUID" }, "PremisesGUID" ),
                //        new Tuple<string[], string>(new [] { "PayerInfo","Ind","Surname" }, "Surname" ),
                //        new Tuple<string[], string>(new [] { "PayerInfo", "Ind", "FirstName" }, "FirstName" ),
                //        new Tuple<string[], string>(new [] { "AccountNumber" }, "AccountNumber" ),
                //        new Tuple<string[], string>(new [] { "CreationDate" }, "CreationDate" )
                //};

                //foreach (DataRow row in _preparedDataSet.Tables["Account"].Select())
                //{
                //    AddChildElement(row, "HomeManagementAsync", "importAccount_rso_close", "importAccount_rso_close", commonValues, null, updateValues, "importAccountRequest");
                //    CounterCheck();
                //}
            }
        }
    }
}
