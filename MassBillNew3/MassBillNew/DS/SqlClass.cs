﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data;

namespace MassBillNew
{
    static class SqlClass
    {
        static SqlCommand cmd;
        static SqlConnection conn;




        static void ConnectionInit()
        {
          conn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);
        }

        static public void CopyData(DataTable fromTable,string toTable)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
            {
                bulkCopy.DestinationTableName = toTable;
                bulkCopy.WriteToServer(fromTable);
            }
        }

        static void SqlInit(string sql,CommandType type)
        {
          cmd = new SqlCommand();
          cmd.Connection = conn;
          cmd.CommandTimeout = 0;
          cmd.CommandType = type;
          cmd.CommandText = sql;
        }

        static public void Open()
        {
            ConnectionInit();
            conn.Open();
        }

        static public void Close()
        {
            conn.Close();
        }

        static public object ExecuteScalar(string sql)
        {
            SqlInit(sql,CommandType.Text);
            object result = cmd.ExecuteScalar();
            return result;
        }

        static public object ExecuteScalarSP(string sql, object[,] prms)
        {
            SqlInit(sql, CommandType.StoredProcedure);

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                cmd.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }

            object result = cmd.ExecuteScalar();
            return result;
        }

        static public object[] ExecuteScalarSP(string sql, object[,] prms, object outParameter, string outParameterName, SqlDbType outParameterSqlDbType, int outParameterSize)
        {
            SqlInit(sql, CommandType.StoredProcedure);

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                cmd.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }

            cmd.Parameters.Add(outParameterName, outParameterSqlDbType, outParameterSize);

            cmd.Parameters[outParameterName].Direction = ParameterDirection.Output;

            object[] result = new object[2];

            result[0] = cmd.ExecuteScalar();
            result[1] = cmd.Parameters[outParameterName].Value;

            return result;

       
        }

        static public object ExecuteScalar(string sql, object[,] prms)
        {
            SqlInit(sql, CommandType.Text);

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                cmd.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
            }

            object result = cmd.ExecuteScalar();
            return result;
        }



       static public void ExecuteNonQuerySP(string sql, object[,] prms)
        {
            SqlInit(sql, CommandType.StoredProcedure);

            for (int i = 0; i < prms.GetLength(0); i++)
            {
                cmd.Parameters.AddWithValue(prms[i,0].ToString(), prms[i,1]);
            }

            cmd.ExecuteNonQuery();

        }

       static public void ExecuteNonQuery(string sql, object[,] prms)
       {
           SqlInit(sql, CommandType.Text);

           for (int i = 0; i < prms.GetLength(0); i++)
           {
               cmd.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);
           }

           cmd.ExecuteNonQuery();

       }

       static public void ExecuteNonQuery(string sql)
       {
           SqlInit(sql, CommandType.Text);
           cmd.ExecuteNonQuery();
       }

        static public DateTime CurrentBeginDate()
        {
            return (DateTime)ExecuteScalar("select begin_date from dbo.period where period = dbo.work_period()");
        }

        static public DateTime CurrentEndDate()
        {
            return (DateTime)ExecuteScalar("select end_date from dbo.period where period = dbo.work_period()");
        }

        static public DataTable ExecuteToDataTable(string sql)
        {
            SqlInit(sql, CommandType.Text);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();
            adapter.Fill(table);
            return table;
        }

        static public Boolean EllisAdminRole()
        {
            Open();

            string query =
            " if object_id('tempdb..#roles') is not null drop table #roles" +
            " create table #roles (id_employee_role int,employee_role varchar(200))" +
            " insert into #roles " +
            " exec [dbo].[Employee_Get_Roles]" +
            " select 1 from #roles where id_employee_role = 0";

            object res = ExecuteScalar(query);

            Close();

            if (res == null)
                return false;
            else
                return true;

            

        }
    }
}
