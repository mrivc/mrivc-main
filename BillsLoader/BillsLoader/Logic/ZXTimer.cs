﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillsLoader.Logic
{
    using MrivcHelpers.Helpers;

    public class ZXTimer
    {
        int hour;
        int min;
        int second;

        public ZXTimer() {
            this.hour = 0;
            this.min = 0;
            this.second = 0;
        }

        public void Tick()
        {
            if (second < 59)
                second++;
            else
            {
                min++;
                second = 0;
            }

            if (min >= 59)
            {
                hour++;
                second = 0;
                min = 0;
            }

            if (hour >= 24)
            {
                second = 0;
                min = 0;
                hour = 0;
            }
        }

        public void Reset()
        {
            this.hour = 0;
            this.min = 0;
            this.second = 0;
        }

        public string StringValue
        {
            get {
                return StringHelper.ZeroCode(hour) + ":" + StringHelper.ZeroCode(min) + ":" + StringHelper.ZeroCode(second);
            }        
        }
    }
}
