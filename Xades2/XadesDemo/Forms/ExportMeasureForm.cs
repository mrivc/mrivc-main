﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo
{
    public partial class ExportMeasureForm : Form
    {
        public ExportMeasureForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var exportMeasure = new ExportMeasureData("");
            exportMeasure.ExecuteToTempDB();

            exportMeasureDataGridView.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("select * from #exportMeteringDeviceHistory");
        }

        private void ExportMeasureForm_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var exportMeasure = new ExportMeasureData("");
            exportMeasure.Execute();
        }
    }
}
