﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MassBillNew
{
    public partial class ParameterFilter : UserControl
    {
        

        static string Cs = Properties.Settings.Default.MurmanskConnectionString;
        BindingSource period_binding;
        BindingSource owner_binding;
        BindingSource param_binding;
        BindingSource value_binding;

        SqlDataAdapter period_adapter;
        SqlDataAdapter owner_adapter;
        SqlDataAdapter param_adapter;
        SqlDataAdapter value_adapter;

        DataSet ds = new DataSet();


        public ParameterFilter()
        {
            InitializeComponent();

            CaseCb.SelectedIndex = 0;
            OperatorCb.SelectedIndex = 0;
        }

        public void Init()
        {
            

            period_binding = new BindingSource();
            owner_binding = new BindingSource();
            param_binding = new BindingSource();
            value_binding = new BindingSource();

            period_adapter = new SqlDataAdapter("select * from vperiod order by period desc", Cs);
            owner_adapter = new SqlDataAdapter("select id_parameter_owner,parameter_owner from parameter_owner where id_parameter_owner in  (4,2,1) order by id_parameter_owner", Cs);
            param_adapter = new SqlDataAdapter();

            value_adapter = new SqlDataAdapter();

            if (this.DesignMode)
                return;

            if (Cs == "")
                Cs = Properties.Settings.Default.MurmanskConnectionString;

            SqlConnection sqlConn = new SqlConnection();


            sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Cs;
            sqlConn.Open();

            period_adapter.Fill(ds, "period");
            owner_adapter.Fill(ds, "owner");



            period_binding.DataSource = ds.Tables["period"];
            owner_binding.DataSource = ds.Tables["owner"];


            PeriodCb.DataSource = period_binding;
            PeriodCb.ValueMember = "period";
            PeriodCb.DisplayMember = "period_name";

            OwnerCb.DataSource = owner_binding;
            OwnerCb.ValueMember = "id_parameter_owner";
            OwnerCb.DisplayMember = "parameter_owner";



            param_adapter.SelectCommand = new SqlCommand(string.Format("select code,display_name ,po.id_parameter_owner from parameter p inner join parameter_owner po on p.id_parameter_owner = po.id_parameter_owner where po.id_parameter_owner = {0}", OwnerCb.SelectedValue), sqlConn);


            param_adapter.Fill(ds, "param");




            param_binding.DataSource = ds.Tables["param"];


            ParamCb.DataSource = param_binding;
            ParamCb.ValueMember = "code";
            ParamCb.DisplayMember = "display_name";

            ParamCb.SelectedValue = "ACCOUNT_STATUS";

            value_adapter.SelectCommand = new SqlCommand(string.Format("select number,value from dbo.Parameter_dictionary where parameter = '{0}'", ParamCb.SelectedValue), sqlConn);


            value_adapter.Fill(ds, "value");

            value_binding.DataSource = ds.Tables["value"];

            ValueCb.DataSource = value_binding;
            ValueCb.ValueMember = "number";
            ValueCb.DisplayMember = "value";

            sqlConn.Close();
        }

        private void buttonAddCase_Click(object sender, EventArgs e)
        {
            if (!(CaseCb.Text != "" & PeriodCb.SelectedValue != null & ParamCb.SelectedValue != null & OperatorCb.Text != "" & OwnerCb.SelectedValue != null))
            {
                MessageBox.Show("Не все поля заполнены");
                return;
            }

            string condition = "";

            switch (CaseCb.Text)
            {
                case "и":
                    condition = "and";
                    break;
                case "или":
                    condition = "or";
                    break;
            }

            SqlConnection sqlConnection1 = StaticConfig.MainConnection;

            SqlCommand cmd = new SqlCommand();


            cmd.CommandText =
            "select id_parameter_type from parameter where code ='" + ParamCb.SelectedValue.ToString() + "'";

            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            sqlConnection1.Close();

            sqlConnection1.Open();

            string type = "";

            int id_type = (int)cmd.ExecuteScalar();


            string operat = "";
            switch (OperatorCb.Text)
            {
                case "равно":
                    operat = "=";
                    break;
                case "не равно":
                    operat = "<>";
                    break;
                case "меньше":
                    operat = "<";
                    break;
                case "меньше или равно":
                    operat = "<=";
                    break;
                case "больше":
                    operat = ">";
                    break;
                case "больше или равно":
                    operat = ">=";
                    break;
            }


            string display = "";
            string value = "";

            switch (id_type)
            {
                case 1:
                    display = ValueTb.Text;
                    value = ValueTb.Text;
                    break;
                case 2:
                    display = ValueTb.Text;
                    value = ValueTb.Text;
                    break;
                case 3:
                    display = ValueTb.Text;
                    if (ValueTb.Text != "")
                        value = "'" + ValueTb.Text + "'";
                    else
                        value = "";
                    break;
                case 6:
                    display = ValueTb.Text;
                    value = ValueTb.Text;
                    break;
                case 4:
                    display = ValueDtp.Value.Date.ToShortDateString();
                    cmd.CommandText = String.Format("declare @d datetime set @d = '{0}' select cast(@d as int)", ValueDtp.Value.Date);
                    value = cmd.ExecuteScalar().ToString();
                    break;
                case 5:
                    if (ValueCb.SelectedValue == null)
                    {
                        display = "";
                        value = "";
                    }
                    else
                    {
                        display = ValueCb.Text;
                        value = ValueCb.SelectedValue.ToString();
                    }

                    break;
            }

            sqlConnection1.Close();



            if (id_type == 3)
            {
                type = "string";
            }
            else
            {
                type = "float";
            }

            string period = PeriodCb.SelectedValue.ToString();
            string parameter = ParamCb.SelectedValue.ToString();
            string owner = OwnerCb.SelectedValue.ToString();



            object[] o = new object[]{CaseCb.Text,PeriodCb.Text,
                ParamCb.Text,OperatorCb.Text,display,condition,period,parameter,owner,operat,value.Replace(',','.'),type};

            dataGridView2.Rows.Add(o);
        }

        private void OwnerCb_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ds.Tables["param"].Clear();

            param_adapter.SelectCommand.CommandText = string.Format("select code,display_name ,po.id_parameter_owner from parameter p inner join parameter_owner po on p.id_parameter_owner = po.id_parameter_owner where po.id_parameter_owner = {0}", OwnerCb.SelectedValue);
            param_adapter.Fill(ds, "param");

            ParamCb.SelectedValue = -1;


            ValueCb.SelectedValue = -1;
            ValueTb.Text = "";
            ValueDtp.Value = DateTime.Today;
        }

        private void ParamCb_SelectionChangeCommitted(object sender, EventArgs e)
        {




            SqlConnection sqlConnection1 = new SqlConnection(Cs);

            SqlCommand cmd = new SqlCommand();

            if (ParamCb.SelectedValue != null)
            {
                cmd.CommandText =
                "select id_parameter_type from parameter where code ='" + ParamCb.SelectedValue.ToString() + "'";

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                sqlConnection1.Open();

                string type = cmd.ExecuteScalar().ToString();

                if (type == "5")
                {
                    ds.Tables["value"].Clear();

                    value_adapter.SelectCommand = new SqlCommand(string.Format("select number,value from dbo.Parameter_dictionary where parameter = '{0}'", ParamCb.SelectedValue), sqlConnection1);

                    value_adapter.Fill(ds, "value");

                    ValueCb.DataSource = ds.Tables["value"];

                    ValueCb.Visible = true;
                    ValueCb.Enabled = true;
                    ValueTb.Visible = false;
                    ValueDtp.Visible = false;
                }
                else
                {
                    if (type == "4")
                    {
                        ValueDtp.Location = new Point(6, 203);

                        ValueTb.Visible = false;
                        ValueCb.Visible = false;
                        ValueDtp.Visible = true;
                        ValueDtp.Enabled = true;
                    }
                    else
                    {
                        ValueTb.Location = new Point(6, 203);

                        ValueDtp.Visible = false;
                        ValueCb.Visible = false;
                        ValueTb.Visible = true;
                        ValueTb.Enabled = true;
                    }
                }

                this.ValueTb.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.ValueTb_KeyPress1);
                this.ValueTb.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.ValueTb_KeyPress2);

                switch (type)
                {
                    case "1":

                        this.ValueTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValueTb_KeyPress1);
                        break;
                    case "2":
                        this.ValueTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValueTb_KeyPress2);
                        break;
                }

                sqlConnection1.Close();
            }

            ValueCb.SelectedValue = -1;
            ValueTb.Text = "";
            ValueDtp.Value = DateTime.Today;
        }

        private void ValueTb_KeyPress1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 & !Char.IsDigit(e.KeyChar))
                e.Handled = true;

        }

        private void ValueTb_KeyPress2(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == '.')
                e.KeyChar = ',';
            if (e.KeyChar != 22)
                e.Handled = !Char.IsDigit(e.KeyChar) && (e.KeyChar != ',' || (((TextBox)sender).Text.Contains(",") && !((TextBox)sender).SelectedText.Contains(","))) && e.KeyChar != (char)Keys.Back && (e.KeyChar != '-' || ((TextBox)sender).SelectionStart != 0 || (((TextBox)sender).Text.Contains("-") && !((TextBox)sender).SelectedText.Contains("-")));
            else
            {
                double d;
                e.Handled = !double.TryParse(Clipboard.GetText(), out d) || (d < 0 && (((TextBox)sender).SelectionStart != 0 || ((TextBox)sender).Text.Contains("-") && !((TextBox)sender).SelectedText.Contains("-"))) || ((d - (int)d) != 0 && ((TextBox)sender).Text.Contains(",") && !((TextBox)sender).SelectedText.Contains(","));

            }


        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView2.Rows.RemoveAt(e.RowIndex);
        }

    }
}
