﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace PaymentStatementParser
{
    public class Utils
    {
        public struct ResultTableColumn
        {
            public const string ACCOUNT = "account";
            public const string SUMPLAT = "sumplat";
            public const string DATPLAT = "datplat";
        }

        public static readonly List<Regex> REGEXS = new List<Regex>()
        {
            new Regex(@"\w*[0-9]{24,}"),
            new Regex(@"\w*[0-9]{18,}"),
            new Regex(@"\w*(?i)Номер\sлицевого\sсчета(?-i)\D*[0-9]{12,}"),
            new Regex(@"\w*(?i)ЛИЦЕВОЙ\sСЧЕТ(?-i)\D*[0-9]{12,}"),
            new Regex(@"\w*(?i)лс(?-i)\D*[0-9]{12,}"),
            new Regex(@"\w*(?i)л/с(?-i)\D*[0-9]{12,}"),
            new Regex(@"\w*(?i)лс(?-i)\s[0-9]{2}\s[0-9]{10,}"),
            new Regex(@"\w*(?i)л/с(?-i)\s[0-9]{2}\s[0-9]{10,}"),
            new Regex(@"\w*[0-9]{4}\s[0-9]{2}\s[0-9]{2}\s[0-9]{4}\s[0-9]*"),
        };

        public static DateTime DateDayIncrement(string stringDate, int increment)
        {
            DateTime date = DateTime.Parse(stringDate);
            return date.AddDays(increment);
        }
    }
}
