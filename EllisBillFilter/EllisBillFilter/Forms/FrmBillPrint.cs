﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FastReport;
using EllisBillFilter.DS;
using FastReport.Data;
using System.Data.SqlClient;
using System.Collections;

namespace EllisBillFilter
{
    using MrivcHelpers.Helpers;

    public partial class FrmBillPrint : Form
    {
        private string connectionString;
        private bool formShown;

        public FrmBillPrint()
        {
            InitializeComponent();
        }

        public FrmBillPrint(string connectionString)
        {
            formShown = false;
            InitializeComponent();
            this.connectionString = connectionString;
            DAccess.DataModule.ConnectionString = connectionString;

            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Report report = new Report();
            
            report.LoadFromString(DAccess.Instance.GetBillStdReport());

            report.SetParameterValue("MainConnection", DAccess.DataModule.Connection.ConnectionString);
            report.SetParameterValue("IDPrint", 2417609011232547);
            report.Show();
        }

        private void Print()
        {
            if (lbAccountList.Items.Count == 0)
                return;

            // create report instance
            Report report = new Report();

            // load the existing report
            report.Load(@"Reports\\_BillStd.frx");

            this.Cursor = Cursors.WaitCursor;
            long idPrint = PrepareRepotData();
            this.Cursor = Cursors.Default;

            report.SetParameterValue("MainConnection", DAccess.DataModule.Connection.ConnectionString);
            report.SetParameterValue("IDPrint", idPrint);

            report.Show();

            // free resources used by report
            report.Dispose();
        }

        public long PrepareRepotData()
        {
            string idPrint = "24176" + StringHelper.ZeroCode(DateTime.Now.Month) + StringHelper.ZeroCode(DateTime.Now.Day) +
                StringHelper.ZeroCode(DateTime.Now.Hour) + StringHelper.ZeroCode(DateTime.Now.Minute) + StringHelper.ZeroCode(DateTime.Now.Millisecond);
            string sql = "execute [Reports].[Prepare_Bill_Report_Data] " +
                         " @ID_Print = " + idPrint +
                         ", @ID_Bill_Kind = " + cbBillKind.SelectedValue +
                         ", @Period = 24176";

            string finalSql = CreateSqlFillAccounttable() + Environment.NewLine + sql;
            DAccess.DataModule.ExecuteNonQueryCommand(finalSql);

            return long.Parse(idPrint);
        }

        public string CreateSqlFillAccounttable()
        {
            string sqlTableAccountCreate = "create table #__Account(ID int)";
            string sqlAddAccTemplate = "insert into #__Account(ID) values (@account)";
            string sqlAddAcc = "";

            foreach (var item in lbAccountList.Items)
                sqlAddAcc += sqlAddAccTemplate.Replace("@account", item.ToString().Split("-".ToCharArray())[0].Trim()) + ";" + Environment.NewLine;
            sqlTableAccountCreate += Environment.NewLine + sqlAddAcc;

            return sqlTableAccountCreate;
        }



        private void btnAddAccount_Click(object sender, EventArgs e)
        {
            object acc = DAccess.DataModule.ExecuteScalarQueryCommand("select (convert(varchar,ID_Account)+' - '+Account+' - '+Address) as Acc from vaccount_address_all where account='" + txtAccount.Text + "'");
            if (acc == null)
            {
                MessageBox.Show("Лицевой счет не найден");
                return;
            }

            lbAccountList.Items.Add(acc);
        }

        private void FrmBillPrint_Shown(object sender, EventArgs e)
        {
            DAccess.DataModule.BillKindSelect(mDataSet.Bill_Kind);
        }
    }
}
