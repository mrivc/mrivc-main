﻿namespace PaymentStatementParser
{
    //public static class Provider
    //{
    //    public static string providerName;
    //    public static string defaultCode;

    //    public static int paymentDate;
    //    public static int loanAmount;
    //    public static int paymentPurpose;

    //    public static void setProviderProperties(string name, int[] rowIndexes, string code = "")
    //    {
    //        providerName = name;

    //        defaultCode = code;

    //        paymentDate = rowIndexes[0];
    //        loanAmount = rowIndexes[1];
    //        paymentPurpose = rowIndexes[2];
    //    }
    //}

    public class Provider
    {
        public string DefaultCode { get; private set; }

        public int PaymentDate { get; private set; }
        public int LoanAmount { get; private set; }
        public int PaymentPurpose { get; private set; }

        public Provider(int[] rowIndexes, string code = "")
        {
            DefaultCode = code;

            PaymentDate = rowIndexes[0];
            LoanAmount = rowIndexes[1];
            PaymentPurpose = rowIndexes[2];
        }
    }
}
