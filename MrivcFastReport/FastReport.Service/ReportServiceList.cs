﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Configuration;
using System.Collections;
using FastReport;
using FastReport.Utils;
using System.ServiceModel.Web;

namespace FastReport.Service
{
    public partial class ReportService : IFastReportService
    {        
        public List<ReportItem> GetReportsList()
        {
            return GetReportsListByPath("");
        }

        public List<ReportItem> GetReportsListByPath(string path)
        {
            string reportsRoot = GetReportsPath(); 
            List<ReportItem> list = new List<ReportItem>();
            if (Directory.Exists(Path.Combine(reportsRoot, path)))
            {
                if (string.IsNullOrEmpty(path))
                    path = "";

                List<string> reports = GetReportFiles(Path.Combine(reportsRoot, path));

                Report report = new Report();
                try
                {
                    foreach (string s in reports)
                    {
                        ReportItem item = new ReportItem();
                        item.Path = s.Replace(reportsRoot + Path.DirectorySeparatorChar, "");
                        report.Load(s);
                        item.Name = !string.IsNullOrEmpty(report.ReportInfo.Name) ? report.ReportInfo.Name : Path.GetFileNameWithoutExtension(s);
                        item.Description = report.ReportInfo.Description;
                        list.Add(item);
                    }
                }
                catch (Exception e)
                {
                    throw new WebFaultException<ErrorHandler>(new ErrorHandler { Cause = e.Message, ErrorCode = 105 },
                        System.Net.HttpStatusCode.InternalServerError);
                }
            }
            else
                throw new WebFaultException<ErrorHandler>(new ErrorHandler { Cause = "Reports directory not found", ErrorCode = 104 },
                    System.Net.HttpStatusCode.NotFound);

            return list;
        }

        private List<string> GetReportFiles(string path)
        {
            List<string> list = new List<string>();
            if (Directory.Exists(path))
            {
                string[] reports = Directory.GetFiles(path, "*.frx");
                list.AddRange(reports);
                list.Sort();

                string[] dirs = Directory.GetDirectories(path);
                foreach (string dir in dirs)
                {
                    List<string> folders = GetReportFiles(Path.Combine(path, dir));
                    folders.Sort();
                    list.AddRange(folders);
                }
            }
            return list;
        }
    }
}
