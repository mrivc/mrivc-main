﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1.DS
{
    using System.Configuration;

    public partial class DModule : Component
    {
        private static DModule instance;

        public DModule()
        {
            InitializeComponent();
            this.mainConnection.ConnectionString = Properties.Settings.Default.MainConnection;
        }

        public DModule(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            this.mainConnection.ConnectionString = Properties.Settings.Default.MainConnection;
        }

        public static DModule Instance
        {
            get
            {
                if (instance == null)
                    instance = new DModule();

                return instance;
            }
        }

        public int WorkPeriod()
        {
            return (int)ExecuteScalarCommand("select dbo.Work_Period()");
        }

        public DataTable GetClientDetailBank()
        {
            DataTable dt = new DataTable();
            daClientDetailBank.SelectCommand.Parameters["@Period"].Value = WorkPeriod();
            daClientDetailBank.Fill(dt);
            return dt;
        }

        public void ExecuteNonQueryCommand(string sql)
        {
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.ExecuteNonQuery();
        }

        public object ExecuteScalarCommand(string sql)
        {
            cmdAnyCommand.Connection.Close();
            cmdAnyCommand.CommandText = sql;
            cmdAnyCommand.Connection.Open();
            return cmdAnyCommand.ExecuteScalar();
        }
    }
}
