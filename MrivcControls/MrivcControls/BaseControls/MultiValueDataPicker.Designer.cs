﻿namespace MrivcControls
{
    partial class MultiValueDataPicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parameterGroupBox = new System.Windows.Forms.GroupBox();
            this.selectAllCheckBox = new System.Windows.Forms.CheckBox();
            this.customEdit = new MassBillNew.CustomEdit();
            this.parameterGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // parameterGroupBox
            // 
            this.parameterGroupBox.Controls.Add(this.selectAllCheckBox);
            this.parameterGroupBox.Controls.Add(this.customEdit);
            this.parameterGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parameterGroupBox.Location = new System.Drawing.Point(0, 0);
            this.parameterGroupBox.Name = "parameterGroupBox";
            this.parameterGroupBox.Size = new System.Drawing.Size(270, 55);
            this.parameterGroupBox.TabIndex = 0;
            this.parameterGroupBox.TabStop = false;
            this.parameterGroupBox.Text = "parameterGroupBox";
            // 
            // selectAllCheckBox
            // 
            this.selectAllCheckBox.AutoSize = true;
            this.selectAllCheckBox.Checked = true;
            this.selectAllCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.selectAllCheckBox.Location = new System.Drawing.Point(8, 23);
            this.selectAllCheckBox.Name = "selectAllCheckBox";
            this.selectAllCheckBox.Size = new System.Drawing.Size(45, 17);
            this.selectAllCheckBox.TabIndex = 1;
            this.selectAllCheckBox.Text = "Все";
            this.selectAllCheckBox.UseVisualStyleBackColor = true;
            this.selectAllCheckBox.CheckedChanged += new System.EventHandler(this.selectAllCheckBox_CheckedChanged);
            this.selectAllCheckBox.Click += new System.EventHandler(this.selectAllCheckBox_Click);
            // 
            // customEdit
            // 
            this.customEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customEdit.EditValue = "";
            this.customEdit.Location = new System.Drawing.Point(59, 21);
            this.customEdit.Name = "customEdit";
            this.customEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.customEdit.Properties.IncrementalSearch = false;
            this.customEdit.Size = new System.Drawing.Size(196, 20);
            this.customEdit.TabIndex = 0;
            this.customEdit.EditValueChanged += new System.EventHandler(this.customEdit_EditValueChanged);
            // 
            // MultiValueDataPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.parameterGroupBox);
            this.Name = "MultiValueDataPicker";
            this.Size = new System.Drawing.Size(270, 55);
            this.parameterGroupBox.ResumeLayout(false);
            this.parameterGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox parameterGroupBox;
        private System.Windows.Forms.CheckBox selectAllCheckBox;
        private MassBillNew.CustomEdit customEdit;
    }
}
