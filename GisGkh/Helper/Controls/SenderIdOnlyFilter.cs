﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Helper.Data;

namespace Helper.Controls
{
    public partial class SenderIdOnlyFilter : UserControl
    {
        public string SenderID
        {
            get { return cbSender.SelectedValue.ToString(); }
        }

        public SenderIdOnlyFilter()
        {
            InitializeComponent();
        }

        public void Fill()
        {
            cbSender.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("select senderid,sendername from gis.provider order by 2");
            cbSender.ValueMember = "senderid";
            cbSender.DisplayMember = "sendername";
        }
    }
}
