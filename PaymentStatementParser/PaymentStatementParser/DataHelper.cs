﻿using System.Collections.Generic;

namespace PaymentStatementParser
{
    public static class DataHelper
    {
        public static bool IsFullRow(object[] row)
        {
            foreach (object obj in row)
            {
                if (obj.GetType().Name == "DBNull")
                {
                    return false;
                }
            }

            return true;
        }

        public static List<object> GetClearedRow(object[] row)
        {
            List<object> newRow = new List<object>();

            foreach (object obj in row)
            {
                if (!(obj.GetType().Name == "DBNull"))
                {
                    newRow.Add(obj);
                }
            }

            return newRow;
        }
    }
}
