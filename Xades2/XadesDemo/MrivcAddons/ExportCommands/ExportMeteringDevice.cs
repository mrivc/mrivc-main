﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ExportMeteringDevice : MrivcExportCommand , ISingleHouseCommand
    {
        public ExportMeteringDevice(string senderID) : base("exportMeteringDeviceData", "HomeManagementAsync", senderID, "exportMeteringDeviceDataResult")
        {
            MrivcData.CreateTempExportMeterTables();
        }

        public string FiasHouseGuid { get; set; }

        public new void Execute()
        {
            this.ExecuteToTempDB();
            DAccess.DataModule.ExecuteNonQueryCommand1("exec [GIS].[SaveExportedMeterData]");
        }

        public new void ExecuteToTempDB()
        {
            base.ExecuteToTempDB(new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) }, _exportNodeName);
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportMeteringDeviceData");
        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            var ds = new DataSet("MeterData");
            var dt = DAccess.DataModule.GetDataTableByQueryCommand1("select * from #exportMeterData");
            dt.TableName = "Meter";
            ds.Tables.Add(dt);

            return ds;
        }
    }
}
