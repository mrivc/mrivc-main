﻿namespace Payments
{
    partial class frmPaymentFormControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.txtMiddleName = new System.Windows.Forms.TextBox();
            this.lblMiddleName = new System.Windows.Forms.Label();
            this.txtINN = new System.Windows.Forms.TextBox();
            this.lblINN = new System.Windows.Forms.Label();
            this.lblBirthDate = new System.Windows.Forms.Label();
            this.dpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.txtBirthPlace = new System.Windows.Forms.TextBox();
            this.lblBirthPlace = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbRelation = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDocumentCountry = new System.Windows.Forms.TextBox();
            this.lblDocumentCountry = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbCountry = new System.Windows.Forms.ComboBox();
            this.dpDocumentDate = new System.Windows.Forms.DateTimePicker();
            this.txtDocumentSeria = new System.Windows.Forms.TextBox();
            this.cbDocumentType = new System.Windows.Forms.ComboBox();
            this.txtDocumentOrg = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDocumentDate = new System.Windows.Forms.Label();
            this.txtDocumentNumber = new System.Windows.Forms.TextBox();
            this.lblDocumentNumber = new System.Windows.Forms.Label();
            this.lblDocumentType = new System.Windows.Forms.Label();
            this.txtCitizenship = new System.Windows.Forms.TextBox();
            this.lblCitizenship = new System.Windows.Forms.Label();
            this.txtRegion = new System.Windows.Forms.TextBox();
            this.lblRegion = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.lblStreet = new System.Windows.Forms.Label();
            this.lblHouse = new System.Windows.Forms.Label();
            this.lblHouseBlock = new System.Windows.Forms.Label();
            this.txtHouseBlock = new System.Windows.Forms.TextBox();
            this.txtFlat = new System.Windows.Forms.TextBox();
            this.lblFlat = new System.Windows.Forms.Label();
            this.cbPlaceCountry = new System.Windows.Forms.ComboBox();
            this.lblPlaceCountry = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtHouseP = new System.Windows.Forms.MaskedTextBox();
            this.cbCodeRFP = new System.Windows.Forms.ComboBox();
            this.lblCodeRFP = new System.Windows.Forms.Label();
            this.cbPlaceCountryP = new System.Windows.Forms.ComboBox();
            this.lblPlaceCountryP = new System.Windows.Forms.Label();
            this.lblRegionP = new System.Windows.Forms.Label();
            this.txtRegionP = new System.Windows.Forms.TextBox();
            this.txtFlatP = new System.Windows.Forms.TextBox();
            this.lblCityP = new System.Windows.Forms.Label();
            this.lblFlatP = new System.Windows.Forms.Label();
            this.txtCityP = new System.Windows.Forms.TextBox();
            this.txtHouseBlockP = new System.Windows.Forms.TextBox();
            this.lblStreetP = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtStreetP = new System.Windows.Forms.TextBox();
            this.lblHouseP = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtHouse = new System.Windows.Forms.MaskedTextBox();
            this.cbCodeRF = new System.Windows.Forms.ComboBox();
            this.lblCodeRF = new System.Windows.Forms.Label();
            this.lblHouseBlockP = new System.Windows.Forms.TabControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblOrgFio = new System.Windows.Forms.Label();
            this.lblOrgAddress = new System.Windows.Forms.Label();
            this.lblOrgInn = new System.Windows.Forms.Label();
            this.lblOrgName = new System.Windows.Forms.Label();
            this.lblRegName = new System.Windows.Forms.Label();
            this.lblRegNumber = new System.Windows.Forms.Label();
            this.dpRegDate = new System.Windows.Forms.DateTimePicker();
            this.txtOrgAddress = new System.Windows.Forms.TextBox();
            this.lblRegDate = new System.Windows.Forms.Label();
            this.txtOrgFio = new System.Windows.Forms.TextBox();
            this.txtRegName = new System.Windows.Forms.TextBox();
            this.txtOrgName = new System.Windows.Forms.TextBox();
            this.txtOrgInn = new System.Windows.Forms.TextBox();
            this.txtRegNum = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDelegateFIO = new System.Windows.Forms.TextBox();
            this.txtPresentFIO = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtResiverFIO = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFIO = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.paymentsDataSet1 = new Payments.PaymentsDataSet();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOsn = new System.Windows.Forms.TextBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.lblHouseBlockP.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(7, 52);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(63, 13);
            this.lblLastName.TabIndex = 0;
            this.lblLastName.Text = "Фамилия*:";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(80, 49);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(202, 20);
            this.txtLastName.TabIndex = 0;
            this.txtLastName.Leave += new System.EventHandler(this.txtLastName_Leave);
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(80, 75);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(202, 20);
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.Leave += new System.EventHandler(this.txtFirstName_Leave);
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(7, 78);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(36, 13);
            this.lblFirstName.TabIndex = 2;
            this.lblFirstName.Text = "Имя*:";
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.Location = new System.Drawing.Point(80, 101);
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(202, 20);
            this.txtMiddleName.TabIndex = 2;
            this.txtMiddleName.Leave += new System.EventHandler(this.txtMiddleName_Leave);
            // 
            // lblMiddleName
            // 
            this.lblMiddleName.AutoSize = true;
            this.lblMiddleName.Location = new System.Drawing.Point(7, 104);
            this.lblMiddleName.Name = "lblMiddleName";
            this.lblMiddleName.Size = new System.Drawing.Size(61, 13);
            this.lblMiddleName.TabIndex = 4;
            this.lblMiddleName.Text = "Отчество*:";
            // 
            // txtINN
            // 
            this.txtINN.Location = new System.Drawing.Point(101, 234);
            this.txtINN.Name = "txtINN";
            this.txtINN.Size = new System.Drawing.Size(181, 20);
            this.txtINN.TabIndex = 7;
            // 
            // lblINN
            // 
            this.lblINN.AutoSize = true;
            this.lblINN.Location = new System.Drawing.Point(8, 237);
            this.lblINN.Name = "lblINN";
            this.lblINN.Size = new System.Drawing.Size(34, 13);
            this.lblINN.TabIndex = 6;
            this.lblINN.Text = "ИНН:";
            // 
            // lblBirthDate
            // 
            this.lblBirthDate.AutoSize = true;
            this.lblBirthDate.Location = new System.Drawing.Point(7, 133);
            this.lblBirthDate.Name = "lblBirthDate";
            this.lblBirthDate.Size = new System.Drawing.Size(93, 13);
            this.lblBirthDate.TabIndex = 8;
            this.lblBirthDate.Text = "Дата рождения*:";
            // 
            // dpBirthDate
            // 
            this.dpBirthDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpBirthDate.Location = new System.Drawing.Point(101, 127);
            this.dpBirthDate.Name = "dpBirthDate";
            this.dpBirthDate.Size = new System.Drawing.Size(181, 20);
            this.dpBirthDate.TabIndex = 3;
            // 
            // txtBirthPlace
            // 
            this.txtBirthPlace.Location = new System.Drawing.Point(101, 182);
            this.txtBirthPlace.Name = "txtBirthPlace";
            this.txtBirthPlace.Size = new System.Drawing.Size(181, 20);
            this.txtBirthPlace.TabIndex = 5;
            // 
            // lblBirthPlace
            // 
            this.lblBirthPlace.AutoSize = true;
            this.lblBirthPlace.Location = new System.Drawing.Point(7, 158);
            this.lblBirthPlace.Name = "lblBirthPlace";
            this.lblBirthPlace.Size = new System.Drawing.Size(90, 13);
            this.lblBirthPlace.TabIndex = 10;
            this.lblBirthPlace.Text = "Место рожд-ия*:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbRelation);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDocumentCountry);
            this.groupBox1.Controls.Add(this.lblDocumentCountry);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbCountry);
            this.groupBox1.Controls.Add(this.dpDocumentDate);
            this.groupBox1.Controls.Add(this.txtLastName);
            this.groupBox1.Controls.Add(this.txtDocumentSeria);
            this.groupBox1.Controls.Add(this.txtINN);
            this.groupBox1.Controls.Add(this.cbDocumentType);
            this.groupBox1.Controls.Add(this.txtDocumentOrg);
            this.groupBox1.Controls.Add(this.lblINN);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtBirthPlace);
            this.groupBox1.Controls.Add(this.lblDocumentDate);
            this.groupBox1.Controls.Add(this.lblLastName);
            this.groupBox1.Controls.Add(this.txtDocumentNumber);
            this.groupBox1.Controls.Add(this.lblBirthPlace);
            this.groupBox1.Controls.Add(this.lblDocumentNumber);
            this.groupBox1.Controls.Add(this.lblFirstName);
            this.groupBox1.Controls.Add(this.lblDocumentType);
            this.groupBox1.Controls.Add(this.dpBirthDate);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.lblBirthDate);
            this.groupBox1.Controls.Add(this.txtCitizenship);
            this.groupBox1.Controls.Add(this.lblCitizenship);
            this.groupBox1.Controls.Add(this.lblMiddleName);
            this.groupBox1.Controls.Add(this.txtMiddleName);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(291, 424);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Сведения о физическом лице - участнике операции:";
            // 
            // cbRelation
            // 
            this.cbRelation.FormattingEnabled = true;
            this.cbRelation.Items.AddRange(new object[] {
            "Собственник",
            "По доверенности (представитель собственника)"});
            this.cbRelation.Location = new System.Drawing.Point(140, 21);
            this.cbRelation.Name = "cbRelation";
            this.cbRelation.Size = new System.Drawing.Size(142, 21);
            this.cbRelation.TabIndex = 62;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Отношение к операции:";
            // 
            // txtDocumentCountry
            // 
            this.txtDocumentCountry.Location = new System.Drawing.Point(99, 288);
            this.txtDocumentCountry.Name = "txtDocumentCountry";
            this.txtDocumentCountry.Size = new System.Drawing.Size(183, 20);
            this.txtDocumentCountry.TabIndex = 30;
            this.txtDocumentCountry.Text = "Россия";
            // 
            // lblDocumentCountry
            // 
            this.lblDocumentCountry.AutoSize = true;
            this.lblDocumentCountry.Location = new System.Drawing.Point(8, 291);
            this.lblDocumentCountry.Name = "lblDocumentCountry";
            this.lblDocumentCountry.Size = new System.Drawing.Size(50, 13);
            this.lblDocumentCountry.TabIndex = 29;
            this.lblDocumentCountry.Text = "Страна*:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(33, 267);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(233, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Документ, удостоверяющий личность";
            // 
            // cbCountry
            // 
            this.cbCountry.FormattingEnabled = true;
            this.cbCountry.Items.AddRange(new object[] {
            "РОССИЯ",
            "АБХАЗИЯ",
            "АВСТРАЛИЯ",
            "АВСТРИЯ",
            "АЗЕРБАЙДЖАН",
            "АЛБАНИЯ",
            "АЛЖИР",
            "АМЕРИКАНСКОЕ САМОА",
            "АНГИЛЬЯ",
            "АНГОЛА",
            "АНДОРРА",
            "АНТАРКТИДА",
            "АНТИГУА И БАРБУДА",
            "АРГЕНТИНА",
            "АРМЕНИЯ",
            "АРУБА",
            "АФГАНИСТАН",
            "БАГАМЫ",
            "БАНГЛАДЕШ",
            "БАРБАДОС",
            "БАХРЕЙН",
            "БЕЛАРУСЬ",
            "БЕЛИЗ",
            "БЕЛЬГИЯ",
            "БЕНИН",
            "БЕРМУДЫ",
            "БОЛГАРИЯ",
            "БОЛИВИЯ, МНОГОНАЦИОНАЛЬНОЕ ГОСУДАРСТВО",
            "БОНЭЙР, СИНТ-ЭСТАТИУС И САБА",
            "БОСНИЯ И ГЕРЦЕГОВИНА",
            "БОТСВАНА",
            "БРАЗИЛИЯ",
            "БРИТАНСКАЯ ТЕРРИТОРИЯ В ИНДИЙСКОМ ОКЕАНЕ",
            "БРУНЕЙ-ДАРУССАЛАМ",
            "БУРКИНА-ФАСО",
            "БУРУНДИ",
            "БУТАН",
            "ВАНУАТУ",
            "ВЕНГРИЯ",
            "ВЕНЕСУЭЛА БОЛИВАРИАНСКАЯ РЕСПУБЛИКА",
            "ВИРГИНСКИЕ ОСТРОВА, БРИТАНСКИЕ",
            "ВИРГИНСКИЕ ОСТРОВА, США",
            "ТИМОР-ЛЕСТЕ",
            "ВЬЕТНАМ",
            "ГАБОН",
            "ГАИТИ",
            "ГАЙАНА",
            "ГАМБИЯ",
            "ГАНА",
            "ГВАДЕЛУПА",
            "ГВАТЕМАЛА",
            "ГВИНЕЯ",
            "ГВИНЕЯ-БИСАУ",
            "ГЕРМАНИЯ",
            "ГЕРНСИ",
            "ГИБРАЛТАР",
            "ГОНДУРАС",
            "ГОНКОНГ",
            "ГРЕНАДА",
            "ГРЕНЛАНДИЯ",
            "ГРЕЦИЯ",
            "ГРУЗИЯ",
            "ГУАМ",
            "ДАНИЯ",
            "ДЖЕРСИ",
            "ДЖИБУТИ",
            "ДОМИНИКА",
            "ДОМИНИКАНСКАЯ РЕСПУБЛИКА",
            "ЕГИПЕТ",
            "ЗАМБИЯ",
            "ЗАПАДНАЯ САХАРА",
            "ЗИМБАБВЕ",
            "ИЗРАИЛЬ",
            "ИНДИЯ",
            "ИНДОНЕЗИЯ",
            "ИОРДАНИЯ",
            "ИРАК",
            "ИРАН, ИСЛАМСКАЯ РЕСПУБЛИКА",
            "ИРЛАНДИЯ",
            "ИСЛАНДИЯ",
            "ИСПАНИЯ",
            "ИТАЛИЯ",
            "ЙЕМЕН",
            "КАБО-ВЕРДЕ",
            "КАЗАХСТАН",
            "КАМБОДЖА",
            "КАМЕРУН",
            "КАНАДА",
            "КАТАР",
            "КЕНИЯ",
            "КИПР",
            "КИРГИЗИЯ",
            "КИРИБАТИ",
            "КИТАЙ",
            "КОКОСОВЫЕ (КИЛИНГ) ОСТРОВА",
            "КОЛУМБИЯ",
            "КОМОРЫ",
            "КОНГО",
            "КОНГО, ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "КОРЕЯ, НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "КОРЕЯ, РЕСПУБЛИКА",
            "КОСТА-РИКА",
            "КОТ Д’ИВУАР",
            "КУБА",
            "КУВЕЙТ",
            "КЮРАСАО",
            "ЛАОССКАЯ НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "ЛАТВИЯ",
            "ЛЕСОТО",
            "ЛИБЕРИЯ",
            "ЛИВАН",
            "ЛИВИЯ",
            "ЛИТВА",
            "ЛИХТЕНШТЕЙН",
            "ЛЮКСЕМБУРГ",
            "МАВРИКИЙ",
            "МАВРИТАНИЯ",
            "МАДАГАСКАР",
            "МАЙОТТА",
            "МАКАО",
            "РЕСПУБЛИКА МАКЕДОНИЯ[2]",
            "МАЛАВИ",
            "МАЛАЙЗИЯ",
            "МАЛИ",
            "МАЛЫЕ ТИХООКЕАНСКИЕ ОТДАЛЕННЫЕ ОСТРОВА СОЕДИНЕННЫХ ШТАТОВ",
            "МАЛЬДИВЫ",
            "МАЛЬТА",
            "МАРОККО",
            "МАРТИНИКА",
            "МАРШАЛЛОВЫ ОСТРОВА",
            "МЕКСИКА",
            "МИКРОНЕЗИЯ, ФЕДЕРАТИВНЫЕ ШТАТЫ",
            "МОЗАМБИК",
            "МОЛДОВА, РЕСПУБЛИКА",
            "МОНАКО",
            "МОНГОЛИЯ",
            "МОНТСЕРРАТ",
            "МЬЯНМА",
            "НАМИБИЯ",
            "НАУРУ",
            "НЕПАЛ",
            "НИГЕР",
            "НИГЕРИЯ",
            "НИДЕРЛАНДЫ",
            "НИКАРАГУА",
            "НИУЭ",
            "НОВАЯ ЗЕЛАНДИЯ",
            "НОВАЯ КАЛЕДОНИЯ",
            "НОРВЕГИЯ",
            "ОБЪЕДИНЕННЫЕ АРАБСКИЕ ЭМИРАТЫ",
            "ОМАН",
            "ОСТРОВА КАЙМАН",
            "ОСТРОВА КУКА",
            "ОСТРОВА ТЕРКС И КАЙКОС",
            "ОСТРОВ БУВЕ",
            "ОСТРОВ МЭН",
            "ОСТРОВ НОРФОЛК",
            "ОСТРОВ РОЖДЕСТВА",
            "ОСТРОВ ХЕРД И ОСТРОВА МАКДОНАЛЬД",
            "ПАКИСТАН",
            "ПАЛАУ",
            "ПАЛЕСТИНСКАЯ ТЕРРИТОРИЯ, ОККУПИРОВАННАЯ",
            "ПАНАМА",
            "ПАПСКИЙ ПРЕСТОЛ (ГОСУДАРСТВО — ГОРОД ВАТИКАН)",
            "ПАПУА-НОВАЯ ГВИНЕЯ",
            "ПАРАГВАЙ",
            "ПЕРУ",
            "ПИТКЕРН",
            "ПОЛЬША",
            "ПОРТУГАЛИЯ",
            "ПУЭРТО-РИКО",
            "РЕЮНЬОН",
            "РОССИЯ",
            "РУАНДА",
            "РУМЫНИЯ",
            "САМОА",
            "САН-МАРИНО",
            "САН-ТОМЕ И ПРИНСИПИ",
            "САУДОВСКАЯ АРАВИЯ",
            "СВАЗИЛЕНД",
            "СВЯТАЯ ЕЛЕНА, ОСТРОВ ВОЗНЕСЕНИЯ, ТРИСТАН-ДА-КУНЬЯ",
            "СЕВЕРНЫЕ МАРИАНСКИЕ ОСТРОВА",
            "СЕЙШЕЛЫ",
            "СЕН-БАРТЕЛЕМИ",
            "СЕН-МАРТЕН",
            "СЕН-МАРТЕН (нидерландская часть)",
            "СЕНЕГАЛ",
            "СЕНТ-ВИНСЕНТ И ГРЕНАДИНЫ",
            "СЕНТ-КИТС И НЕВИС",
            "СЕНТ-ЛЮСИЯ",
            "СЕН-ПЬЕР И МИКЕЛОН",
            "СЕРБИЯ",
            "СИНГАПУР",
            "СИРИЙСКАЯ АРАБСКАЯ РЕСПУБЛИКА",
            "СЛОВАКИЯ",
            "СЛОВЕНИЯ",
            "СОЕДИНЕННОЕ КОРОЛЕВСТВО",
            "СОЕДИНЕННЫЕ ШТАТЫ",
            "СОЛОМОНОВЫ ОСТРОВА",
            "СОМАЛИ",
            "СУДАН",
            "СУРИНАМ",
            "СЬЕРРА-ЛЕОНЕ",
            "ТАДЖИКИСТАН",
            "ТАИЛАНД",
            "ТАЙВАНЬ (КИТАЙ)",
            "ТАНЗАНИЯ, ОБЪЕДИНЕННАЯ РЕСПУБЛИКА",
            "ТОГО",
            "ТОКЕЛАУ",
            "ТОНГА",
            "ТРИНИДАД И ТОБАГО",
            "ТУВАЛУ",
            "ТУНИС",
            "ТУРКМЕНИЯ",
            "ТУРЦИЯ",
            "УГАНДА",
            "УЗБЕКИСТАН",
            "УКРАИНА",
            "УОЛЛИС И ФУТУНА",
            "УРУГВАЙ",
            "ФАРЕРСКИЕ ОСТРОВА",
            "ФИДЖИ",
            "ФИЛИППИНЫ",
            "ФИНЛЯНДИЯ",
            "ФОЛКЛЕНДСКИЕ ОСТРОВА (МАЛЬВИНСКИЕ)",
            "ФРАНЦИЯ",
            "ФРАНЦУЗСКАЯ ГВИАНА",
            "ФРАНЦУЗСКАЯ ПОЛИНЕЗИЯ",
            "ФРАНЦУЗСКИЕ ЮЖНЫЕ ТЕРРИТОРИИ",
            "ХОРВАТИЯ",
            "ЦЕНТРАЛЬНО-АФРИКАНСКАЯ РЕСПУБЛИКА",
            "ЧАД",
            "ЧЕРНОГОРИЯ",
            "ЧЕШСКАЯ РЕСПУБЛИКА",
            "ЧИЛИ",
            "ШВЕЙЦАРИЯ",
            "ШВЕЦИЯ",
            "ШПИЦБЕРГЕН И ЯН МАЙЕН",
            "ШРИ-ЛАНКА",
            "ЭКВАДОР",
            "ЭКВАТОРИАЛЬНАЯ ГВИНЕЯ",
            "ЭЛАНДСКИЕ ОСТРОВА",
            "ЭЛЬ-САЛЬВАДОР",
            "ЭРИТРЕЯ",
            "ЭСТОНИЯ",
            "ЭФИОПИЯ",
            "ЮЖНАЯ АФРИКА",
            "ЮЖНАЯ ДЖОРДЖИЯ И ЮЖНЫЕ САНДВИЧЕВЫ ОСТРОВА",
            "ЮЖНАЯ ОСЕТИЯ",
            "ЮЖНЫЙ СУДАН",
            "ЯМАЙКА",
            "ЯПОНИЯ"});
            this.cbCountry.Location = new System.Drawing.Point(101, 155);
            this.cbCountry.Name = "cbCountry";
            this.cbCountry.Size = new System.Drawing.Size(181, 21);
            this.cbCountry.TabIndex = 4;
            // 
            // dpDocumentDate
            // 
            this.dpDocumentDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpDocumentDate.Location = new System.Drawing.Point(99, 368);
            this.dpDocumentDate.Name = "dpDocumentDate";
            this.dpDocumentDate.Size = new System.Drawing.Size(183, 20);
            this.dpDocumentDate.TabIndex = 11;
            // 
            // txtDocumentSeria
            // 
            this.txtDocumentSeria.Location = new System.Drawing.Point(101, 342);
            this.txtDocumentSeria.Name = "txtDocumentSeria";
            this.txtDocumentSeria.Size = new System.Drawing.Size(52, 20);
            this.txtDocumentSeria.TabIndex = 9;
            // 
            // cbDocumentType
            // 
            this.cbDocumentType.FormattingEnabled = true;
            this.cbDocumentType.Items.AddRange(new object[] {
            "Паспорт гражданина РФ",
            "Заграничный паспорт",
            "Водительское удостоверение"});
            this.cbDocumentType.Location = new System.Drawing.Point(99, 315);
            this.cbDocumentType.Name = "cbDocumentType";
            this.cbDocumentType.Size = new System.Drawing.Size(183, 21);
            this.cbDocumentType.TabIndex = 8;
            // 
            // txtDocumentOrg
            // 
            this.txtDocumentOrg.Location = new System.Drawing.Point(99, 394);
            this.txtDocumentOrg.Name = "txtDocumentOrg";
            this.txtDocumentOrg.Size = new System.Drawing.Size(183, 20);
            this.txtDocumentOrg.TabIndex = 12;
            this.txtDocumentOrg.Leave += new System.EventHandler(this.txtDocumentOrg_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 397);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Кем выдан*:";
            // 
            // lblDocumentDate
            // 
            this.lblDocumentDate.AutoSize = true;
            this.lblDocumentDate.Location = new System.Drawing.Point(7, 371);
            this.lblDocumentDate.Name = "lblDocumentDate";
            this.lblDocumentDate.Size = new System.Drawing.Size(80, 13);
            this.lblDocumentDate.TabIndex = 19;
            this.lblDocumentDate.Text = "Дата выдачи*:";
            // 
            // txtDocumentNumber
            // 
            this.txtDocumentNumber.Location = new System.Drawing.Point(159, 342);
            this.txtDocumentNumber.Name = "txtDocumentNumber";
            this.txtDocumentNumber.Size = new System.Drawing.Size(123, 20);
            this.txtDocumentNumber.TabIndex = 10;
            // 
            // lblDocumentNumber
            // 
            this.lblDocumentNumber.AutoSize = true;
            this.lblDocumentNumber.Location = new System.Drawing.Point(7, 345);
            this.lblDocumentNumber.Name = "lblDocumentNumber";
            this.lblDocumentNumber.Size = new System.Drawing.Size(82, 13);
            this.lblDocumentNumber.TabIndex = 17;
            this.lblDocumentNumber.Text = "Серия/номер*:";
            // 
            // lblDocumentType
            // 
            this.lblDocumentType.AutoSize = true;
            this.lblDocumentType.Location = new System.Drawing.Point(6, 318);
            this.lblDocumentType.Name = "lblDocumentType";
            this.lblDocumentType.Size = new System.Drawing.Size(90, 13);
            this.lblDocumentType.TabIndex = 15;
            this.lblDocumentType.Text = "Тип документа*:";
            // 
            // txtCitizenship
            // 
            this.txtCitizenship.Location = new System.Drawing.Point(101, 208);
            this.txtCitizenship.Name = "txtCitizenship";
            this.txtCitizenship.Size = new System.Drawing.Size(181, 20);
            this.txtCitizenship.TabIndex = 6;
            // 
            // lblCitizenship
            // 
            this.lblCitizenship.AutoSize = true;
            this.lblCitizenship.Location = new System.Drawing.Point(7, 211);
            this.lblCitizenship.Name = "lblCitizenship";
            this.lblCitizenship.Size = new System.Drawing.Size(81, 13);
            this.lblCitizenship.TabIndex = 13;
            this.lblCitizenship.Text = "Гражданство*:";
            // 
            // txtRegion
            // 
            this.txtRegion.Location = new System.Drawing.Point(96, 61);
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Size = new System.Drawing.Size(200, 20);
            this.txtRegion.TabIndex = 15;
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Location = new System.Drawing.Point(4, 64);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(41, 13);
            this.lblRegion.TabIndex = 30;
            this.lblRegion.Text = "Район:";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(96, 87);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(200, 20);
            this.txtCity.TabIndex = 16;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(4, 90);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(68, 13);
            this.lblCity.TabIndex = 32;
            this.lblCity.Text = "Нас. пункт*:";
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(96, 113);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(200, 20);
            this.txtStreet.TabIndex = 17;
            this.txtStreet.Text = "ул. ";
            // 
            // lblStreet
            // 
            this.lblStreet.AutoSize = true;
            this.lblStreet.Location = new System.Drawing.Point(4, 116);
            this.lblStreet.Name = "lblStreet";
            this.lblStreet.Size = new System.Drawing.Size(46, 13);
            this.lblStreet.TabIndex = 34;
            this.lblStreet.Text = "Улица*:";
            // 
            // lblHouse
            // 
            this.lblHouse.AutoSize = true;
            this.lblHouse.Location = new System.Drawing.Point(4, 142);
            this.lblHouse.Name = "lblHouse";
            this.lblHouse.Size = new System.Drawing.Size(51, 13);
            this.lblHouse.TabIndex = 36;
            this.lblHouse.Text = "Дом №*:";
            // 
            // lblHouseBlock
            // 
            this.lblHouseBlock.AutoSize = true;
            this.lblHouseBlock.Location = new System.Drawing.Point(145, 142);
            this.lblHouseBlock.Name = "lblHouseBlock";
            this.lblHouseBlock.Size = new System.Drawing.Size(37, 13);
            this.lblHouseBlock.TabIndex = 38;
            this.lblHouseBlock.Text = "корп.:";
            // 
            // txtHouseBlock
            // 
            this.txtHouseBlock.Location = new System.Drawing.Point(182, 139);
            this.txtHouseBlock.Name = "txtHouseBlock";
            this.txtHouseBlock.Size = new System.Drawing.Size(46, 20);
            this.txtHouseBlock.TabIndex = 19;
            // 
            // txtFlat
            // 
            this.txtFlat.Location = new System.Drawing.Point(265, 139);
            this.txtFlat.Name = "txtFlat";
            this.txtFlat.Size = new System.Drawing.Size(31, 20);
            this.txtFlat.TabIndex = 20;
            // 
            // lblFlat
            // 
            this.lblFlat.AutoSize = true;
            this.lblFlat.Location = new System.Drawing.Point(234, 142);
            this.lblFlat.Name = "lblFlat";
            this.lblFlat.Size = new System.Drawing.Size(25, 13);
            this.lblFlat.TabIndex = 40;
            this.lblFlat.Text = "кв.:";
            // 
            // cbPlaceCountry
            // 
            this.cbPlaceCountry.FormattingEnabled = true;
            this.cbPlaceCountry.Items.AddRange(new object[] {
            "РОССИЯ",
            "АБХАЗИЯ",
            "АВСТРАЛИЯ",
            "АВСТРИЯ",
            "АЗЕРБАЙДЖАН",
            "АЛБАНИЯ",
            "АЛЖИР",
            "АМЕРИКАНСКОЕ САМОА",
            "АНГИЛЬЯ",
            "АНГОЛА",
            "АНДОРРА",
            "АНТАРКТИДА",
            "АНТИГУА И БАРБУДА",
            "АРГЕНТИНА",
            "АРМЕНИЯ",
            "АРУБА",
            "АФГАНИСТАН",
            "БАГАМЫ",
            "БАНГЛАДЕШ",
            "БАРБАДОС",
            "БАХРЕЙН",
            "БЕЛАРУСЬ",
            "БЕЛИЗ",
            "БЕЛЬГИЯ",
            "БЕНИН",
            "БЕРМУДЫ",
            "БОЛГАРИЯ",
            "БОЛИВИЯ, МНОГОНАЦИОНАЛЬНОЕ ГОСУДАРСТВО",
            "БОНЭЙР, СИНТ-ЭСТАТИУС И САБА",
            "БОСНИЯ И ГЕРЦЕГОВИНА",
            "БОТСВАНА",
            "БРАЗИЛИЯ",
            "БРИТАНСКАЯ ТЕРРИТОРИЯ В ИНДИЙСКОМ ОКЕАНЕ",
            "БРУНЕЙ-ДАРУССАЛАМ",
            "БУРКИНА-ФАСО",
            "БУРУНДИ",
            "БУТАН",
            "ВАНУАТУ",
            "ВЕНГРИЯ",
            "ВЕНЕСУЭЛА БОЛИВАРИАНСКАЯ РЕСПУБЛИКА",
            "ВИРГИНСКИЕ ОСТРОВА, БРИТАНСКИЕ",
            "ВИРГИНСКИЕ ОСТРОВА, США",
            "ТИМОР-ЛЕСТЕ",
            "ВЬЕТНАМ",
            "ГАБОН",
            "ГАИТИ",
            "ГАЙАНА",
            "ГАМБИЯ",
            "ГАНА",
            "ГВАДЕЛУПА",
            "ГВАТЕМАЛА",
            "ГВИНЕЯ",
            "ГВИНЕЯ-БИСАУ",
            "ГЕРМАНИЯ",
            "ГЕРНСИ",
            "ГИБРАЛТАР",
            "ГОНДУРАС",
            "ГОНКОНГ",
            "ГРЕНАДА",
            "ГРЕНЛАНДИЯ",
            "ГРЕЦИЯ",
            "ГРУЗИЯ",
            "ГУАМ",
            "ДАНИЯ",
            "ДЖЕРСИ",
            "ДЖИБУТИ",
            "ДОМИНИКА",
            "ДОМИНИКАНСКАЯ РЕСПУБЛИКА",
            "ЕГИПЕТ",
            "ЗАМБИЯ",
            "ЗАПАДНАЯ САХАРА",
            "ЗИМБАБВЕ",
            "ИЗРАИЛЬ",
            "ИНДИЯ",
            "ИНДОНЕЗИЯ",
            "ИОРДАНИЯ",
            "ИРАК",
            "ИРАН, ИСЛАМСКАЯ РЕСПУБЛИКА",
            "ИРЛАНДИЯ",
            "ИСЛАНДИЯ",
            "ИСПАНИЯ",
            "ИТАЛИЯ",
            "ЙЕМЕН",
            "КАБО-ВЕРДЕ",
            "КАЗАХСТАН",
            "КАМБОДЖА",
            "КАМЕРУН",
            "КАНАДА",
            "КАТАР",
            "КЕНИЯ",
            "КИПР",
            "КИРГИЗИЯ",
            "КИРИБАТИ",
            "КИТАЙ",
            "КОКОСОВЫЕ (КИЛИНГ) ОСТРОВА",
            "КОЛУМБИЯ",
            "КОМОРЫ",
            "КОНГО",
            "КОНГО, ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "КОРЕЯ, НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "КОРЕЯ, РЕСПУБЛИКА",
            "КОСТА-РИКА",
            "КОТ Д’ИВУАР",
            "КУБА",
            "КУВЕЙТ",
            "КЮРАСАО",
            "ЛАОССКАЯ НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "ЛАТВИЯ",
            "ЛЕСОТО",
            "ЛИБЕРИЯ",
            "ЛИВАН",
            "ЛИВИЯ",
            "ЛИТВА",
            "ЛИХТЕНШТЕЙН",
            "ЛЮКСЕМБУРГ",
            "МАВРИКИЙ",
            "МАВРИТАНИЯ",
            "МАДАГАСКАР",
            "МАЙОТТА",
            "МАКАО",
            "РЕСПУБЛИКА МАКЕДОНИЯ[2]",
            "МАЛАВИ",
            "МАЛАЙЗИЯ",
            "МАЛИ",
            "МАЛЫЕ ТИХООКЕАНСКИЕ ОТДАЛЕННЫЕ ОСТРОВА СОЕДИНЕННЫХ ШТАТОВ",
            "МАЛЬДИВЫ",
            "МАЛЬТА",
            "МАРОККО",
            "МАРТИНИКА",
            "МАРШАЛЛОВЫ ОСТРОВА",
            "МЕКСИКА",
            "МИКРОНЕЗИЯ, ФЕДЕРАТИВНЫЕ ШТАТЫ",
            "МОЗАМБИК",
            "МОЛДОВА, РЕСПУБЛИКА",
            "МОНАКО",
            "МОНГОЛИЯ",
            "МОНТСЕРРАТ",
            "МЬЯНМА",
            "НАМИБИЯ",
            "НАУРУ",
            "НЕПАЛ",
            "НИГЕР",
            "НИГЕРИЯ",
            "НИДЕРЛАНДЫ",
            "НИКАРАГУА",
            "НИУЭ",
            "НОВАЯ ЗЕЛАНДИЯ",
            "НОВАЯ КАЛЕДОНИЯ",
            "НОРВЕГИЯ",
            "ОБЪЕДИНЕННЫЕ АРАБСКИЕ ЭМИРАТЫ",
            "ОМАН",
            "ОСТРОВА КАЙМАН",
            "ОСТРОВА КУКА",
            "ОСТРОВА ТЕРКС И КАЙКОС",
            "ОСТРОВ БУВЕ",
            "ОСТРОВ МЭН",
            "ОСТРОВ НОРФОЛК",
            "ОСТРОВ РОЖДЕСТВА",
            "ОСТРОВ ХЕРД И ОСТРОВА МАКДОНАЛЬД",
            "ПАКИСТАН",
            "ПАЛАУ",
            "ПАЛЕСТИНСКАЯ ТЕРРИТОРИЯ, ОККУПИРОВАННАЯ",
            "ПАНАМА",
            "ПАПСКИЙ ПРЕСТОЛ (ГОСУДАРСТВО — ГОРОД ВАТИКАН)",
            "ПАПУА-НОВАЯ ГВИНЕЯ",
            "ПАРАГВАЙ",
            "ПЕРУ",
            "ПИТКЕРН",
            "ПОЛЬША",
            "ПОРТУГАЛИЯ",
            "ПУЭРТО-РИКО",
            "РЕЮНЬОН",
            "РОССИЯ",
            "РУАНДА",
            "РУМЫНИЯ",
            "САМОА",
            "САН-МАРИНО",
            "САН-ТОМЕ И ПРИНСИПИ",
            "САУДОВСКАЯ АРАВИЯ",
            "СВАЗИЛЕНД",
            "СВЯТАЯ ЕЛЕНА, ОСТРОВ ВОЗНЕСЕНИЯ, ТРИСТАН-ДА-КУНЬЯ",
            "СЕВЕРНЫЕ МАРИАНСКИЕ ОСТРОВА",
            "СЕЙШЕЛЫ",
            "СЕН-БАРТЕЛЕМИ",
            "СЕН-МАРТЕН",
            "СЕН-МАРТЕН (нидерландская часть)",
            "СЕНЕГАЛ",
            "СЕНТ-ВИНСЕНТ И ГРЕНАДИНЫ",
            "СЕНТ-КИТС И НЕВИС",
            "СЕНТ-ЛЮСИЯ",
            "СЕН-ПЬЕР И МИКЕЛОН",
            "СЕРБИЯ",
            "СИНГАПУР",
            "СИРИЙСКАЯ АРАБСКАЯ РЕСПУБЛИКА",
            "СЛОВАКИЯ",
            "СЛОВЕНИЯ",
            "СОЕДИНЕННОЕ КОРОЛЕВСТВО",
            "СОЕДИНЕННЫЕ ШТАТЫ",
            "СОЛОМОНОВЫ ОСТРОВА",
            "СОМАЛИ",
            "СУДАН",
            "СУРИНАМ",
            "СЬЕРРА-ЛЕОНЕ",
            "ТАДЖИКИСТАН",
            "ТАИЛАНД",
            "ТАЙВАНЬ (КИТАЙ)",
            "ТАНЗАНИЯ, ОБЪЕДИНЕННАЯ РЕСПУБЛИКА",
            "ТОГО",
            "ТОКЕЛАУ",
            "ТОНГА",
            "ТРИНИДАД И ТОБАГО",
            "ТУВАЛУ",
            "ТУНИС",
            "ТУРКМЕНИЯ",
            "ТУРЦИЯ",
            "УГАНДА",
            "УЗБЕКИСТАН",
            "УКРАИНА",
            "УОЛЛИС И ФУТУНА",
            "УРУГВАЙ",
            "ФАРЕРСКИЕ ОСТРОВА",
            "ФИДЖИ",
            "ФИЛИППИНЫ",
            "ФИНЛЯНДИЯ",
            "ФОЛКЛЕНДСКИЕ ОСТРОВА (МАЛЬВИНСКИЕ)",
            "ФРАНЦИЯ",
            "ФРАНЦУЗСКАЯ ГВИАНА",
            "ФРАНЦУЗСКАЯ ПОЛИНЕЗИЯ",
            "ФРАНЦУЗСКИЕ ЮЖНЫЕ ТЕРРИТОРИИ",
            "ХОРВАТИЯ",
            "ЦЕНТРАЛЬНО-АФРИКАНСКАЯ РЕСПУБЛИКА",
            "ЧАД",
            "ЧЕРНОГОРИЯ",
            "ЧЕШСКАЯ РЕСПУБЛИКА",
            "ЧИЛИ",
            "ШВЕЙЦАРИЯ",
            "ШВЕЦИЯ",
            "ШПИЦБЕРГЕН И ЯН МАЙЕН",
            "ШРИ-ЛАНКА",
            "ЭКВАДОР",
            "ЭКВАТОРИАЛЬНАЯ ГВИНЕЯ",
            "ЭЛАНДСКИЕ ОСТРОВА",
            "ЭЛЬ-САЛЬВАДОР",
            "ЭРИТРЕЯ",
            "ЭСТОНИЯ",
            "ЭФИОПИЯ",
            "ЮЖНАЯ АФРИКА",
            "ЮЖНАЯ ДЖОРДЖИЯ И ЮЖНЫЕ САНДВИЧЕВЫ ОСТРОВА",
            "ЮЖНАЯ ОСЕТИЯ",
            "ЮЖНЫЙ СУДАН",
            "ЯМАЙКА",
            "ЯПОНИЯ"});
            this.cbPlaceCountry.Location = new System.Drawing.Point(96, 7);
            this.cbPlaceCountry.Name = "cbPlaceCountry";
            this.cbPlaceCountry.Size = new System.Drawing.Size(200, 21);
            this.cbPlaceCountry.TabIndex = 13;
            // 
            // lblPlaceCountry
            // 
            this.lblPlaceCountry.AutoSize = true;
            this.lblPlaceCountry.Location = new System.Drawing.Point(4, 10);
            this.lblPlaceCountry.Name = "lblPlaceCountry";
            this.lblPlaceCountry.Size = new System.Drawing.Size(50, 13);
            this.lblPlaceCountry.TabIndex = 43;
            this.lblPlaceCountry.Text = "Страна*:";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtHouseP);
            this.tabPage4.Controls.Add(this.cbCodeRFP);
            this.tabPage4.Controls.Add(this.lblCodeRFP);
            this.tabPage4.Controls.Add(this.cbPlaceCountryP);
            this.tabPage4.Controls.Add(this.lblPlaceCountryP);
            this.tabPage4.Controls.Add(this.lblRegionP);
            this.tabPage4.Controls.Add(this.txtRegionP);
            this.tabPage4.Controls.Add(this.txtFlatP);
            this.tabPage4.Controls.Add(this.lblCityP);
            this.tabPage4.Controls.Add(this.lblFlatP);
            this.tabPage4.Controls.Add(this.txtCityP);
            this.tabPage4.Controls.Add(this.txtHouseBlockP);
            this.tabPage4.Controls.Add(this.lblStreetP);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.txtStreetP);
            this.tabPage4.Controls.Add(this.lblHouseP);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(306, 165);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Адрес пребывания";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtHouseP
            // 
            this.txtHouseP.Location = new System.Drawing.Point(96, 139);
            this.txtHouseP.Mask = "0000";
            this.txtHouseP.Name = "txtHouseP";
            this.txtHouseP.Size = new System.Drawing.Size(43, 20);
            this.txtHouseP.TabIndex = 55;
            this.txtHouseP.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // cbCodeRFP
            // 
            this.cbCodeRFP.FormattingEnabled = true;
            this.cbCodeRFP.Items.AddRange(new object[] {
            "-",
            "22 - Алтайский край",
            "28 - Амурская область",
            "29 - Архангельская область",
            "30 - Астраханская область",
            "31 - Белгородская область",
            "32 - Брянская область",
            "33 - Владимирская область",
            "34 - Волгоградская область",
            "35 - Вологодская область",
            "36 - Воронежская область",
            "79 - Еврейская автономная область",
            "75 - Забайкальский край",
            "37 - Ивановская область",
            "38 - Иркутская область",
            "07 - Кабардино-Балкарская республика",
            "39 - Калининградская область",
            "40 - Калужская область",
            "41 - Камчатский край",
            "09 - Карачаево-Черкесская республика",
            "42 - Кемеровская область",
            "43 - Кировская область",
            "44 - Костромская область",
            "23 - Краснодарский край",
            "24 - Красноярский край",
            "45 - Курганская область",
            "46 - Курская область",
            "47 - Ленинградская область",
            "48 - Липецкая область",
            "49 - Магаданская область",
            "77 - Москва",
            "50 - Московская область",
            "51 - Мурманская область",
            "83 - Ненецкий автономный округ",
            "52 - Нижегородская область",
            "53 - Новгородская область",
            "54 - Новосибирская область",
            "55 - Омская область",
            "56 - Оренбургская область",
            "57 - Орловская область",
            "58 - Пензенская область",
            "59 - Пермский край",
            "25 - Приморский край",
            "60 - Псковская область",
            "01 - Республика Адыгея",
            "04 - Республика Алтай",
            "02 - Республика Башкортостан",
            "03 - Республика Бурятия",
            "05 - Республика Дагестан",
            "06 - Республика Ингушетия",
            "08 - Республика Калмыкия",
            "10 - Республика Карелия",
            "11 - Республика Коми",
            "12 - Республика Марий Эл",
            "13 - Республика Мордовия",
            "14 - Республика Саха (Якутия)",
            "15 - Республика Северная Осетия — Алания",
            "16 - Республика Татарстан",
            "17 - Республика Тыва",
            "19 - Республика Хакасия",
            "61 - Ростовская область",
            "62 - Рязанская область",
            "63 - Самарская область",
            "78 - Санкт-Петербург",
            "64 - Саратовская область",
            "65 - Сахалинская область",
            "66 - Свердловская область",
            "67 - Смоленская область",
            "26 - Ставропольский край",
            "68 - Тамбовская область",
            "69 - Тверская область",
            "70 - Томская область",
            "71 - Тульская область",
            "72 - Тюменская область",
            "18 - Удмуртская республика",
            "73 - Ульяновская область",
            "27 - Хабаровский край",
            "86 - Ханты-Мансийский автономный округ - Югра",
            "74 - Челябинская область",
            "20 - Чеченская республика",
            "21 - Чувашская республика",
            "87 - Чукотский автономный округ",
            "89 - Ямало-Ненецкий автономный округ",
            "76 - Ярославская область"});
            this.cbCodeRFP.Location = new System.Drawing.Point(96, 34);
            this.cbCodeRFP.Name = "cbCodeRFP";
            this.cbCodeRFP.Size = new System.Drawing.Size(200, 21);
            this.cbCodeRFP.TabIndex = 51;
            // 
            // lblCodeRFP
            // 
            this.lblCodeRFP.AutoSize = true;
            this.lblCodeRFP.Location = new System.Drawing.Point(4, 37);
            this.lblCodeRFP.Name = "lblCodeRFP";
            this.lblCodeRFP.Size = new System.Drawing.Size(50, 13);
            this.lblCodeRFP.TabIndex = 61;
            this.lblCodeRFP.Text = "Код РФ:";
            // 
            // cbPlaceCountryP
            // 
            this.cbPlaceCountryP.FormattingEnabled = true;
            this.cbPlaceCountryP.Items.AddRange(new object[] {
            "РОССИЯ",
            "АБХАЗИЯ",
            "АВСТРАЛИЯ",
            "АВСТРИЯ",
            "АЗЕРБАЙДЖАН",
            "АЛБАНИЯ",
            "АЛЖИР",
            "АМЕРИКАНСКОЕ САМОА",
            "АНГИЛЬЯ",
            "АНГОЛА",
            "АНДОРРА",
            "АНТАРКТИДА",
            "АНТИГУА И БАРБУДА",
            "АРГЕНТИНА",
            "АРМЕНИЯ",
            "АРУБА",
            "АФГАНИСТАН",
            "БАГАМЫ",
            "БАНГЛАДЕШ",
            "БАРБАДОС",
            "БАХРЕЙН",
            "БЕЛАРУСЬ",
            "БЕЛИЗ",
            "БЕЛЬГИЯ",
            "БЕНИН",
            "БЕРМУДЫ",
            "БОЛГАРИЯ",
            "БОЛИВИЯ, МНОГОНАЦИОНАЛЬНОЕ ГОСУДАРСТВО",
            "БОНЭЙР, СИНТ-ЭСТАТИУС И САБА",
            "БОСНИЯ И ГЕРЦЕГОВИНА",
            "БОТСВАНА",
            "БРАЗИЛИЯ",
            "БРИТАНСКАЯ ТЕРРИТОРИЯ В ИНДИЙСКОМ ОКЕАНЕ",
            "БРУНЕЙ-ДАРУССАЛАМ",
            "БУРКИНА-ФАСО",
            "БУРУНДИ",
            "БУТАН",
            "ВАНУАТУ",
            "ВЕНГРИЯ",
            "ВЕНЕСУЭЛА БОЛИВАРИАНСКАЯ РЕСПУБЛИКА",
            "ВИРГИНСКИЕ ОСТРОВА, БРИТАНСКИЕ",
            "ВИРГИНСКИЕ ОСТРОВА, США",
            "ТИМОР-ЛЕСТЕ",
            "ВЬЕТНАМ",
            "ГАБОН",
            "ГАИТИ",
            "ГАЙАНА",
            "ГАМБИЯ",
            "ГАНА",
            "ГВАДЕЛУПА",
            "ГВАТЕМАЛА",
            "ГВИНЕЯ",
            "ГВИНЕЯ-БИСАУ",
            "ГЕРМАНИЯ",
            "ГЕРНСИ",
            "ГИБРАЛТАР",
            "ГОНДУРАС",
            "ГОНКОНГ",
            "ГРЕНАДА",
            "ГРЕНЛАНДИЯ",
            "ГРЕЦИЯ",
            "ГРУЗИЯ",
            "ГУАМ",
            "ДАНИЯ",
            "ДЖЕРСИ",
            "ДЖИБУТИ",
            "ДОМИНИКА",
            "ДОМИНИКАНСКАЯ РЕСПУБЛИКА",
            "ЕГИПЕТ",
            "ЗАМБИЯ",
            "ЗАПАДНАЯ САХАРА",
            "ЗИМБАБВЕ",
            "ИЗРАИЛЬ",
            "ИНДИЯ",
            "ИНДОНЕЗИЯ",
            "ИОРДАНИЯ",
            "ИРАК",
            "ИРАН, ИСЛАМСКАЯ РЕСПУБЛИКА",
            "ИРЛАНДИЯ",
            "ИСЛАНДИЯ",
            "ИСПАНИЯ",
            "ИТАЛИЯ",
            "ЙЕМЕН",
            "КАБО-ВЕРДЕ",
            "КАЗАХСТАН",
            "КАМБОДЖА",
            "КАМЕРУН",
            "КАНАДА",
            "КАТАР",
            "КЕНИЯ",
            "КИПР",
            "КИРГИЗИЯ",
            "КИРИБАТИ",
            "КИТАЙ",
            "КОКОСОВЫЕ (КИЛИНГ) ОСТРОВА",
            "КОЛУМБИЯ",
            "КОМОРЫ",
            "КОНГО",
            "КОНГО, ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "КОРЕЯ, НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "КОРЕЯ, РЕСПУБЛИКА",
            "КОСТА-РИКА",
            "КОТ Д’ИВУАР",
            "КУБА",
            "КУВЕЙТ",
            "КЮРАСАО",
            "ЛАОССКАЯ НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА",
            "ЛАТВИЯ",
            "ЛЕСОТО",
            "ЛИБЕРИЯ",
            "ЛИВАН",
            "ЛИВИЯ",
            "ЛИТВА",
            "ЛИХТЕНШТЕЙН",
            "ЛЮКСЕМБУРГ",
            "МАВРИКИЙ",
            "МАВРИТАНИЯ",
            "МАДАГАСКАР",
            "МАЙОТТА",
            "МАКАО",
            "РЕСПУБЛИКА МАКЕДОНИЯ[2]",
            "МАЛАВИ",
            "МАЛАЙЗИЯ",
            "МАЛИ",
            "МАЛЫЕ ТИХООКЕАНСКИЕ ОТДАЛЕННЫЕ ОСТРОВА СОЕДИНЕННЫХ ШТАТОВ",
            "МАЛЬДИВЫ",
            "МАЛЬТА",
            "МАРОККО",
            "МАРТИНИКА",
            "МАРШАЛЛОВЫ ОСТРОВА",
            "МЕКСИКА",
            "МИКРОНЕЗИЯ, ФЕДЕРАТИВНЫЕ ШТАТЫ",
            "МОЗАМБИК",
            "МОЛДОВА, РЕСПУБЛИКА",
            "МОНАКО",
            "МОНГОЛИЯ",
            "МОНТСЕРРАТ",
            "МЬЯНМА",
            "НАМИБИЯ",
            "НАУРУ",
            "НЕПАЛ",
            "НИГЕР",
            "НИГЕРИЯ",
            "НИДЕРЛАНДЫ",
            "НИКАРАГУА",
            "НИУЭ",
            "НОВАЯ ЗЕЛАНДИЯ",
            "НОВАЯ КАЛЕДОНИЯ",
            "НОРВЕГИЯ",
            "ОБЪЕДИНЕННЫЕ АРАБСКИЕ ЭМИРАТЫ",
            "ОМАН",
            "ОСТРОВА КАЙМАН",
            "ОСТРОВА КУКА",
            "ОСТРОВА ТЕРКС И КАЙКОС",
            "ОСТРОВ БУВЕ",
            "ОСТРОВ МЭН",
            "ОСТРОВ НОРФОЛК",
            "ОСТРОВ РОЖДЕСТВА",
            "ОСТРОВ ХЕРД И ОСТРОВА МАКДОНАЛЬД",
            "ПАКИСТАН",
            "ПАЛАУ",
            "ПАЛЕСТИНСКАЯ ТЕРРИТОРИЯ, ОККУПИРОВАННАЯ",
            "ПАНАМА",
            "ПАПСКИЙ ПРЕСТОЛ (ГОСУДАРСТВО — ГОРОД ВАТИКАН)",
            "ПАПУА-НОВАЯ ГВИНЕЯ",
            "ПАРАГВАЙ",
            "ПЕРУ",
            "ПИТКЕРН",
            "ПОЛЬША",
            "ПОРТУГАЛИЯ",
            "ПУЭРТО-РИКО",
            "РЕЮНЬОН",
            "РОССИЯ",
            "РУАНДА",
            "РУМЫНИЯ",
            "САМОА",
            "САН-МАРИНО",
            "САН-ТОМЕ И ПРИНСИПИ",
            "САУДОВСКАЯ АРАВИЯ",
            "СВАЗИЛЕНД",
            "СВЯТАЯ ЕЛЕНА, ОСТРОВ ВОЗНЕСЕНИЯ, ТРИСТАН-ДА-КУНЬЯ",
            "СЕВЕРНЫЕ МАРИАНСКИЕ ОСТРОВА",
            "СЕЙШЕЛЫ",
            "СЕН-БАРТЕЛЕМИ",
            "СЕН-МАРТЕН",
            "СЕН-МАРТЕН (нидерландская часть)",
            "СЕНЕГАЛ",
            "СЕНТ-ВИНСЕНТ И ГРЕНАДИНЫ",
            "СЕНТ-КИТС И НЕВИС",
            "СЕНТ-ЛЮСИЯ",
            "СЕН-ПЬЕР И МИКЕЛОН",
            "СЕРБИЯ",
            "СИНГАПУР",
            "СИРИЙСКАЯ АРАБСКАЯ РЕСПУБЛИКА",
            "СЛОВАКИЯ",
            "СЛОВЕНИЯ",
            "СОЕДИНЕННОЕ КОРОЛЕВСТВО",
            "СОЕДИНЕННЫЕ ШТАТЫ",
            "СОЛОМОНОВЫ ОСТРОВА",
            "СОМАЛИ",
            "СУДАН",
            "СУРИНАМ",
            "СЬЕРРА-ЛЕОНЕ",
            "ТАДЖИКИСТАН",
            "ТАИЛАНД",
            "ТАЙВАНЬ (КИТАЙ)",
            "ТАНЗАНИЯ, ОБЪЕДИНЕННАЯ РЕСПУБЛИКА",
            "ТОГО",
            "ТОКЕЛАУ",
            "ТОНГА",
            "ТРИНИДАД И ТОБАГО",
            "ТУВАЛУ",
            "ТУНИС",
            "ТУРКМЕНИЯ",
            "ТУРЦИЯ",
            "УГАНДА",
            "УЗБЕКИСТАН",
            "УКРАИНА",
            "УОЛЛИС И ФУТУНА",
            "УРУГВАЙ",
            "ФАРЕРСКИЕ ОСТРОВА",
            "ФИДЖИ",
            "ФИЛИППИНЫ",
            "ФИНЛЯНДИЯ",
            "ФОЛКЛЕНДСКИЕ ОСТРОВА (МАЛЬВИНСКИЕ)",
            "ФРАНЦИЯ",
            "ФРАНЦУЗСКАЯ ГВИАНА",
            "ФРАНЦУЗСКАЯ ПОЛИНЕЗИЯ",
            "ФРАНЦУЗСКИЕ ЮЖНЫЕ ТЕРРИТОРИИ",
            "ХОРВАТИЯ",
            "ЦЕНТРАЛЬНО-АФРИКАНСКАЯ РЕСПУБЛИКА",
            "ЧАД",
            "ЧЕРНОГОРИЯ",
            "ЧЕШСКАЯ РЕСПУБЛИКА",
            "ЧИЛИ",
            "ШВЕЙЦАРИЯ",
            "ШВЕЦИЯ",
            "ШПИЦБЕРГЕН И ЯН МАЙЕН",
            "ШРИ-ЛАНКА",
            "ЭКВАДОР",
            "ЭКВАТОРИАЛЬНАЯ ГВИНЕЯ",
            "ЭЛАНДСКИЕ ОСТРОВА",
            "ЭЛЬ-САЛЬВАДОР",
            "ЭРИТРЕЯ",
            "ЭСТОНИЯ",
            "ЭФИОПИЯ",
            "ЮЖНАЯ АФРИКА",
            "ЮЖНАЯ ДЖОРДЖИЯ И ЮЖНЫЕ САНДВИЧЕВЫ ОСТРОВА",
            "ЮЖНАЯ ОСЕТИЯ",
            "ЮЖНЫЙ СУДАН",
            "ЯМАЙКА",
            "ЯПОНИЯ"});
            this.cbPlaceCountryP.Location = new System.Drawing.Point(96, 7);
            this.cbPlaceCountryP.Name = "cbPlaceCountryP";
            this.cbPlaceCountryP.Size = new System.Drawing.Size(200, 21);
            this.cbPlaceCountryP.TabIndex = 50;
            // 
            // lblPlaceCountryP
            // 
            this.lblPlaceCountryP.AutoSize = true;
            this.lblPlaceCountryP.Location = new System.Drawing.Point(4, 10);
            this.lblPlaceCountryP.Name = "lblPlaceCountryP";
            this.lblPlaceCountryP.Size = new System.Drawing.Size(46, 13);
            this.lblPlaceCountryP.TabIndex = 59;
            this.lblPlaceCountryP.Text = "Страна:";
            // 
            // lblRegionP
            // 
            this.lblRegionP.AutoSize = true;
            this.lblRegionP.Location = new System.Drawing.Point(4, 64);
            this.lblRegionP.Name = "lblRegionP";
            this.lblRegionP.Size = new System.Drawing.Size(41, 13);
            this.lblRegionP.TabIndex = 46;
            this.lblRegionP.Text = "Район:";
            // 
            // txtRegionP
            // 
            this.txtRegionP.Location = new System.Drawing.Point(96, 61);
            this.txtRegionP.Name = "txtRegionP";
            this.txtRegionP.Size = new System.Drawing.Size(200, 20);
            this.txtRegionP.TabIndex = 52;
            // 
            // txtFlatP
            // 
            this.txtFlatP.Location = new System.Drawing.Point(265, 139);
            this.txtFlatP.Name = "txtFlatP";
            this.txtFlatP.Size = new System.Drawing.Size(31, 20);
            this.txtFlatP.TabIndex = 57;
            // 
            // lblCityP
            // 
            this.lblCityP.AutoSize = true;
            this.lblCityP.Location = new System.Drawing.Point(4, 90);
            this.lblCityP.Name = "lblCityP";
            this.lblCityP.Size = new System.Drawing.Size(64, 13);
            this.lblCityP.TabIndex = 48;
            this.lblCityP.Text = "Нас. пункт:";
            // 
            // lblFlatP
            // 
            this.lblFlatP.AutoSize = true;
            this.lblFlatP.Location = new System.Drawing.Point(234, 142);
            this.lblFlatP.Name = "lblFlatP";
            this.lblFlatP.Size = new System.Drawing.Size(25, 13);
            this.lblFlatP.TabIndex = 56;
            this.lblFlatP.Text = "кв.:";
            // 
            // txtCityP
            // 
            this.txtCityP.Location = new System.Drawing.Point(96, 87);
            this.txtCityP.Name = "txtCityP";
            this.txtCityP.Size = new System.Drawing.Size(200, 20);
            this.txtCityP.TabIndex = 53;
            // 
            // txtHouseBlockP
            // 
            this.txtHouseBlockP.Location = new System.Drawing.Point(182, 139);
            this.txtHouseBlockP.Name = "txtHouseBlockP";
            this.txtHouseBlockP.Size = new System.Drawing.Size(46, 20);
            this.txtHouseBlockP.TabIndex = 56;
            // 
            // lblStreetP
            // 
            this.lblStreetP.AutoSize = true;
            this.lblStreetP.Location = new System.Drawing.Point(4, 116);
            this.lblStreetP.Name = "lblStreetP";
            this.lblStreetP.Size = new System.Drawing.Size(42, 13);
            this.lblStreetP.TabIndex = 50;
            this.lblStreetP.Text = "Улица:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(145, 142);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 54;
            this.label8.Text = "корп.:";
            // 
            // txtStreetP
            // 
            this.txtStreetP.Location = new System.Drawing.Point(96, 113);
            this.txtStreetP.Name = "txtStreetP";
            this.txtStreetP.Size = new System.Drawing.Size(200, 20);
            this.txtStreetP.TabIndex = 54;
            // 
            // lblHouseP
            // 
            this.lblHouseP.AutoSize = true;
            this.lblHouseP.Location = new System.Drawing.Point(4, 142);
            this.lblHouseP.Name = "lblHouseP";
            this.lblHouseP.Size = new System.Drawing.Size(47, 13);
            this.lblHouseP.TabIndex = 52;
            this.lblHouseP.Text = "Дом №:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtHouse);
            this.tabPage3.Controls.Add(this.cbCodeRF);
            this.tabPage3.Controls.Add(this.lblCodeRF);
            this.tabPage3.Controls.Add(this.cbPlaceCountry);
            this.tabPage3.Controls.Add(this.lblPlaceCountry);
            this.tabPage3.Controls.Add(this.lblRegion);
            this.tabPage3.Controls.Add(this.txtRegion);
            this.tabPage3.Controls.Add(this.txtFlat);
            this.tabPage3.Controls.Add(this.lblCity);
            this.tabPage3.Controls.Add(this.lblFlat);
            this.tabPage3.Controls.Add(this.txtCity);
            this.tabPage3.Controls.Add(this.txtHouseBlock);
            this.tabPage3.Controls.Add(this.lblStreet);
            this.tabPage3.Controls.Add(this.lblHouseBlock);
            this.tabPage3.Controls.Add(this.txtStreet);
            this.tabPage3.Controls.Add(this.lblHouse);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(306, 165);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Адрес регистрации";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtHouse
            // 
            this.txtHouse.Location = new System.Drawing.Point(96, 139);
            this.txtHouse.Mask = "0000";
            this.txtHouse.Name = "txtHouse";
            this.txtHouse.Size = new System.Drawing.Size(43, 20);
            this.txtHouse.TabIndex = 18;
            this.txtHouse.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // cbCodeRF
            // 
            this.cbCodeRF.FormattingEnabled = true;
            this.cbCodeRF.Items.AddRange(new object[] {
            "-",
            "22 - Алтайский край",
            "28 - Амурская область",
            "29 - Архангельская область",
            "30 - Астраханская область",
            "31 - Белгородская область",
            "32 - Брянская область",
            "33 - Владимирская область",
            "34 - Волгоградская область",
            "35 - Вологодская область",
            "36 - Воронежская область",
            "79 - Еврейская автономная область",
            "75 - Забайкальский край",
            "37 - Ивановская область",
            "38 - Иркутская область",
            "07 - Кабардино-Балкарская республика",
            "39 - Калининградская область",
            "40 - Калужская область",
            "41 - Камчатский край",
            "09 - Карачаево-Черкесская республика",
            "42 - Кемеровская область",
            "43 - Кировская область",
            "44 - Костромская область",
            "23 - Краснодарский край",
            "24 - Красноярский край",
            "45 - Курганская область",
            "46 - Курская область",
            "47 - Ленинградская область",
            "48 - Липецкая область",
            "49 - Магаданская область",
            "77 - Москва",
            "50 - Московская область",
            "51 - Мурманская область",
            "83 - Ненецкий автономный округ",
            "52 - Нижегородская область",
            "53 - Новгородская область",
            "54 - Новосибирская область",
            "55 - Омская область",
            "56 - Оренбургская область",
            "57 - Орловская область",
            "58 - Пензенская область",
            "59 - Пермский край",
            "25 - Приморский край",
            "60 - Псковская область",
            "01 - Республика Адыгея",
            "04 - Республика Алтай",
            "02 - Республика Башкортостан",
            "03 - Республика Бурятия",
            "05 - Республика Дагестан",
            "06 - Республика Ингушетия",
            "08 - Республика Калмыкия",
            "10 - Республика Карелия",
            "11 - Республика Коми",
            "12 - Республика Марий Эл",
            "13 - Республика Мордовия",
            "14 - Республика Саха (Якутия)",
            "15 - Республика Северная Осетия — Алания",
            "16 - Республика Татарстан",
            "17 - Республика Тыва",
            "19 - Республика Хакасия",
            "61 - Ростовская область",
            "62 - Рязанская область",
            "63 - Самарская область",
            "78 - Санкт-Петербург",
            "64 - Саратовская область",
            "65 - Сахалинская область",
            "66 - Свердловская область",
            "67 - Смоленская область",
            "26 - Ставропольский край",
            "68 - Тамбовская область",
            "69 - Тверская область",
            "70 - Томская область",
            "71 - Тульская область",
            "72 - Тюменская область",
            "18 - Удмуртская республика",
            "73 - Ульяновская область",
            "27 - Хабаровский край",
            "86 - Ханты-Мансийский автономный округ - Югра",
            "74 - Челябинская область",
            "20 - Чеченская республика",
            "21 - Чувашская республика",
            "87 - Чукотский автономный округ",
            "89 - Ямало-Ненецкий автономный округ",
            "76 - Ярославская область"});
            this.cbCodeRF.Location = new System.Drawing.Point(96, 34);
            this.cbCodeRF.Name = "cbCodeRF";
            this.cbCodeRF.Size = new System.Drawing.Size(200, 21);
            this.cbCodeRF.TabIndex = 14;
            // 
            // lblCodeRF
            // 
            this.lblCodeRF.AutoSize = true;
            this.lblCodeRF.Location = new System.Drawing.Point(4, 37);
            this.lblCodeRF.Name = "lblCodeRF";
            this.lblCodeRF.Size = new System.Drawing.Size(50, 13);
            this.lblCodeRF.TabIndex = 45;
            this.lblCodeRF.Text = "Код РФ:";
            // 
            // lblHouseBlockP
            // 
            this.lblHouseBlockP.Controls.Add(this.tabPage3);
            this.lblHouseBlockP.Controls.Add(this.tabPage4);
            this.lblHouseBlockP.Location = new System.Drawing.Point(303, 12);
            this.lblHouseBlockP.Name = "lblHouseBlockP";
            this.lblHouseBlockP.SelectedIndex = 0;
            this.lblHouseBlockP.Size = new System.Drawing.Size(314, 191);
            this.lblHouseBlockP.TabIndex = 44;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblOrgFio);
            this.groupBox2.Controls.Add(this.lblOrgAddress);
            this.groupBox2.Controls.Add(this.lblOrgInn);
            this.groupBox2.Controls.Add(this.lblOrgName);
            this.groupBox2.Controls.Add(this.lblRegName);
            this.groupBox2.Controls.Add(this.lblRegNumber);
            this.groupBox2.Controls.Add(this.dpRegDate);
            this.groupBox2.Controls.Add(this.txtOrgAddress);
            this.groupBox2.Controls.Add(this.lblRegDate);
            this.groupBox2.Controls.Add(this.txtOrgFio);
            this.groupBox2.Controls.Add(this.txtRegName);
            this.groupBox2.Controls.Add(this.txtOrgName);
            this.groupBox2.Controls.Add(this.txtOrgInn);
            this.groupBox2.Controls.Add(this.txtRegNum);
            this.groupBox2.Location = new System.Drawing.Point(305, 210);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(312, 220);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Сведения о юр. лице - участнике операции";
            // 
            // lblOrgFio
            // 
            this.lblOrgFio.AutoSize = true;
            this.lblOrgFio.Location = new System.Drawing.Point(6, 184);
            this.lblOrgFio.Name = "lblOrgFio";
            this.lblOrgFio.Size = new System.Drawing.Size(110, 13);
            this.lblOrgFio.TabIndex = 51;
            this.lblOrgFio.Text = "ФИО руководителя:";
            // 
            // lblOrgAddress
            // 
            this.lblOrgAddress.AutoSize = true;
            this.lblOrgAddress.Location = new System.Drawing.Point(6, 158);
            this.lblOrgAddress.Name = "lblOrgAddress";
            this.lblOrgAddress.Size = new System.Drawing.Size(109, 13);
            this.lblOrgAddress.TabIndex = 50;
            this.lblOrgAddress.Text = "Место регистрации:";
            // 
            // lblOrgInn
            // 
            this.lblOrgInn.AutoSize = true;
            this.lblOrgInn.Location = new System.Drawing.Point(6, 132);
            this.lblOrgInn.Name = "lblOrgInn";
            this.lblOrgInn.Size = new System.Drawing.Size(34, 13);
            this.lblOrgInn.TabIndex = 49;
            this.lblOrgInn.Text = "ИНН:";
            // 
            // lblOrgName
            // 
            this.lblOrgName.AutoSize = true;
            this.lblOrgName.Location = new System.Drawing.Point(6, 106);
            this.lblOrgName.Name = "lblOrgName";
            this.lblOrgName.Size = new System.Drawing.Size(109, 13);
            this.lblOrgName.TabIndex = 48;
            this.lblOrgName.Text = "Наим. организации:";
            // 
            // lblRegName
            // 
            this.lblRegName.AutoSize = true;
            this.lblRegName.Location = new System.Drawing.Point(6, 54);
            this.lblRegName.Name = "lblRegName";
            this.lblRegName.Size = new System.Drawing.Size(129, 13);
            this.lblRegName.TabIndex = 47;
            this.lblRegName.Text = "Регистрирующий орган:";
            // 
            // lblRegNumber
            // 
            this.lblRegNumber.AutoSize = true;
            this.lblRegNumber.Location = new System.Drawing.Point(6, 28);
            this.lblRegNumber.Name = "lblRegNumber";
            this.lblRegNumber.Size = new System.Drawing.Size(66, 13);
            this.lblRegNumber.TabIndex = 46;
            this.lblRegNumber.Text = "Рег. номер:";
            // 
            // dpRegDate
            // 
            this.dpRegDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpRegDate.Location = new System.Drawing.Point(141, 77);
            this.dpRegDate.Name = "dpRegDate";
            this.dpRegDate.Size = new System.Drawing.Size(154, 20);
            this.dpRegDate.TabIndex = 23;
            // 
            // txtOrgAddress
            // 
            this.txtOrgAddress.Location = new System.Drawing.Point(141, 155);
            this.txtOrgAddress.Name = "txtOrgAddress";
            this.txtOrgAddress.Size = new System.Drawing.Size(154, 20);
            this.txtOrgAddress.TabIndex = 26;
            // 
            // lblRegDate
            // 
            this.lblRegDate.AutoSize = true;
            this.lblRegDate.Location = new System.Drawing.Point(6, 82);
            this.lblRegDate.Name = "lblRegDate";
            this.lblRegDate.Size = new System.Drawing.Size(103, 13);
            this.lblRegDate.TabIndex = 29;
            this.lblRegDate.Text = "Дата регистрация:";
            // 
            // txtOrgFio
            // 
            this.txtOrgFio.Location = new System.Drawing.Point(141, 181);
            this.txtOrgFio.Name = "txtOrgFio";
            this.txtOrgFio.Size = new System.Drawing.Size(154, 20);
            this.txtOrgFio.TabIndex = 27;
            // 
            // txtRegName
            // 
            this.txtRegName.Location = new System.Drawing.Point(141, 51);
            this.txtRegName.Name = "txtRegName";
            this.txtRegName.Size = new System.Drawing.Size(154, 20);
            this.txtRegName.TabIndex = 22;
            // 
            // txtOrgName
            // 
            this.txtOrgName.Location = new System.Drawing.Point(141, 103);
            this.txtOrgName.Name = "txtOrgName";
            this.txtOrgName.Size = new System.Drawing.Size(154, 20);
            this.txtOrgName.TabIndex = 24;
            // 
            // txtOrgInn
            // 
            this.txtOrgInn.Location = new System.Drawing.Point(95, 129);
            this.txtOrgInn.Name = "txtOrgInn";
            this.txtOrgInn.Size = new System.Drawing.Size(200, 20);
            this.txtOrgInn.TabIndex = 25;
            // 
            // txtRegNum
            // 
            this.txtRegNum.Location = new System.Drawing.Point(95, 25);
            this.txtRegNum.Name = "txtRegNum";
            this.txtRegNum.Size = new System.Drawing.Size(200, 20);
            this.txtRegNum.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 477);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(339, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Лицо, от имени и по поручению которого совершается операция:";
            // 
            // txtDelegateFIO
            // 
            this.txtDelegateFIO.Location = new System.Drawing.Point(358, 474);
            this.txtDelegateFIO.Name = "txtDelegateFIO";
            this.txtDelegateFIO.Size = new System.Drawing.Size(245, 20);
            this.txtDelegateFIO.TabIndex = 30;
            // 
            // txtPresentFIO
            // 
            this.txtPresentFIO.Location = new System.Drawing.Point(358, 500);
            this.txtPresentFIO.Name = "txtPresentFIO";
            this.txtPresentFIO.Size = new System.Drawing.Size(245, 20);
            this.txtPresentFIO.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 503);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(252, 13);
            this.label11.TabIndex = 54;
            this.label11.Text = "Представитель лица, совершающего операцию:";
            // 
            // txtResiverFIO
            // 
            this.txtResiverFIO.Location = new System.Drawing.Point(358, 526);
            this.txtResiverFIO.Name = "txtResiverFIO";
            this.txtResiverFIO.Size = new System.Drawing.Size(245, 20);
            this.txtResiverFIO.TabIndex = 33;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(13, 522);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(332, 26);
            this.label12.TabIndex = 56;
            this.label12.Text = "Получатель по операции с денежными средствами или иным имуществом:";
            // 
            // txtFIO
            // 
            this.txtFIO.Location = new System.Drawing.Point(358, 552);
            this.txtFIO.Name = "txtFIO";
            this.txtFIO.Size = new System.Drawing.Size(245, 20);
            this.txtFIO.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 555);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(199, 13);
            this.label13.TabIndex = 58;
            this.label13.Text = "Представитель получателя операции:";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(427, 581);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(176, 23);
            this.btnCreate.TabIndex = 35;
            this.btnCreate.Text = "Печать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // paymentsDataSet1
            // 
            this.paymentsDataSet1.DataSetName = "PaymentsDataSet";
            this.paymentsDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(14, 445);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 59;
            this.label1.Text = "Сумма операции:";
            // 
            // txtSum
            // 
            this.txtSum.DecimalPlaces = 2;
            this.txtSum.Location = new System.Drawing.Point(130, 443);
            this.txtSum.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtSum.Name = "txtSum";
            this.txtSum.Size = new System.Drawing.Size(82, 20);
            this.txtSum.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(220, 445);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Основание операции:";
            // 
            // txtOsn
            // 
            this.txtOsn.Location = new System.Drawing.Point(358, 445);
            this.txtOsn.Name = "txtOsn";
            this.txtOsn.Size = new System.Drawing.Size(245, 20);
            this.txtOsn.TabIndex = 29;
            this.txtOsn.Text = "Оплата задолженности по квартплате и коммунальным услугам ";
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(130, 581);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(176, 23);
            this.btnSelect.TabIndex = 62;
            this.btnSelect.Text = "Выбрать (найти)";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(15, 581);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(109, 23);
            this.btnClear.TabIndex = 63;
            this.btnClear.Text = "Очистить форму ";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // frmPaymentFormControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(624, 615);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.txtOsn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.txtFIO);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtResiverFIO);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtPresentFIO);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtDelegateFIO);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblHouseBlockP);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(640, 617);
            this.Name = "frmPaymentFormControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Формы контроля платежа";
            this.Load += new System.EventHandler(this.frmPaymentFormControl_Load);
            this.Shown += new System.EventHandler(this.frmPaymentFormControl_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.lblHouseBlockP.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtINN;
        private System.Windows.Forms.Label lblINN;
        private System.Windows.Forms.TextBox txtBirthPlace;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblBirthPlace;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.DateTimePicker dpBirthDate;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label lblBirthDate;
        private System.Windows.Forms.Label lblMiddleName;
        private System.Windows.Forms.TextBox txtMiddleName;
        private System.Windows.Forms.TextBox txtDocumentOrg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblDocumentDate;
        private System.Windows.Forms.TextBox txtDocumentNumber;
        private System.Windows.Forms.Label lblDocumentNumber;
        private System.Windows.Forms.Label lblDocumentType;
        private System.Windows.Forms.TextBox txtCitizenship;
        private System.Windows.Forms.Label lblCitizenship;
        private System.Windows.Forms.ComboBox cbDocumentType;
        private System.Windows.Forms.DateTimePicker dpDocumentDate;
        private System.Windows.Forms.TextBox txtDocumentSeria;
        private System.Windows.Forms.Label lblPlaceCountry;
        private System.Windows.Forms.ComboBox cbPlaceCountry;
        private System.Windows.Forms.TextBox txtFlat;
        private System.Windows.Forms.Label lblFlat;
        private System.Windows.Forms.TextBox txtHouseBlock;
        private System.Windows.Forms.Label lblHouseBlock;
        private System.Windows.Forms.Label lblHouse;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.Label lblStreet;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.TextBox txtRegion;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.ComboBox cbCountry;
        private System.Windows.Forms.TabControl lblHouseBlockP;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbCodeRF;
        private System.Windows.Forms.Label lblCodeRF;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblOrgFio;
        private System.Windows.Forms.Label lblOrgAddress;
        private System.Windows.Forms.Label lblOrgInn;
        private System.Windows.Forms.Label lblOrgName;
        private System.Windows.Forms.Label lblRegName;
        private System.Windows.Forms.Label lblRegNumber;
        private System.Windows.Forms.DateTimePicker dpRegDate;
        private System.Windows.Forms.TextBox txtOrgAddress;
        private System.Windows.Forms.Label lblRegDate;
        private System.Windows.Forms.TextBox txtOrgFio;
        private System.Windows.Forms.TextBox txtRegName;
        private System.Windows.Forms.TextBox txtOrgName;
        private System.Windows.Forms.TextBox txtOrgInn;
        private System.Windows.Forms.TextBox txtRegNum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDelegateFIO;
        private System.Windows.Forms.TextBox txtPresentFIO;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtResiverFIO;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtFIO;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.ComboBox cbCodeRFP;
        private System.Windows.Forms.Label lblCodeRFP;
        private System.Windows.Forms.ComboBox cbPlaceCountryP;
        private System.Windows.Forms.Label lblPlaceCountryP;
        private System.Windows.Forms.Label lblRegionP;
        private System.Windows.Forms.TextBox txtRegionP;
        private System.Windows.Forms.TextBox txtFlatP;
        private System.Windows.Forms.Label lblCityP;
        private System.Windows.Forms.Label lblFlatP;
        private System.Windows.Forms.TextBox txtCityP;
        private System.Windows.Forms.TextBox txtHouseBlockP;
        private System.Windows.Forms.Label lblStreetP;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtStreetP;
        private System.Windows.Forms.Label lblHouseP;
        private System.Windows.Forms.TextBox txtDocumentCountry;
        private System.Windows.Forms.Label lblDocumentCountry;
        private PaymentsDataSet paymentsDataSet1;
        private System.Windows.Forms.MaskedTextBox txtHouseP;
        private System.Windows.Forms.MaskedTextBox txtHouse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtSum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOsn;
        private System.Windows.Forms.ComboBox cbRelation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnClear;
    }
}