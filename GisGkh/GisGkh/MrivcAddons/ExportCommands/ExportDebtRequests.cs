﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportDebtRequests : MrivcExportCommand
    {
        public ExportDebtRequests(string senderID) : base("exportDebtRequests", "DebtRequestsServiceAsync", senderID, "exportDebtRequestsResult")
        {
            Description = "Выгрузка из ГИС ЖКХ запросов о наличии задолженности за ЖКУ";
        }
    }
}
