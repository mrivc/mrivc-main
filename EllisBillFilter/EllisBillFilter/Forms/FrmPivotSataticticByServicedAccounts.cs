﻿using EllisBillFilter.DS;
using MrivcControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace EllisBillFilter.Forms
{
    public partial class FrmPivotSataticticByServicedAccounts : Form
    {
        public FrmPivotSataticticByServicedAccounts()
        {
            InitializeComponent();
            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
        }

        private void FrmPivotSataticticByServicedAccounts_Load(object sender, EventArgs e)
        {
            //Thread t = new Thread(new ThreadStart(() => { new FrmSplashLoadingDialog().ShowDialog(); }));
            //t.Start();

            pivotSataticticByServicedAccounts1.LoadData();
            pivotSataticticByServicedAccounts1.LoadLayout();

            //t.Abort();
            //this.Activate();
        }

        private void FrmPivotSataticticByServicedAccounts_FormClosing(object sender, FormClosingEventArgs e)
        {
            pivotSataticticByServicedAccounts1.SaveLayout();
        }
    }
}
