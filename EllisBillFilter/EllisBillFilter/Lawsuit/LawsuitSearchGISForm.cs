﻿using System;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using ClosedXML.Excel;
using ExcelDataReader;

namespace EllisBillFilter.Lawsuit
{
    using LoadMeasure;

    public partial class LawsuitSearchGISForm : Form
    {
        #region Constants

        private const string INVALID_FILE_TYPE = "Недопустимый тип файла";

        private const string REQUEST_PROCEDURE = "[User_Reports].[lawsuit_gis_find]";

        private const string FILE_NAME = "Excel file";
        private const string FILTER = "Excel file (*.xlsx;*.xls)|*.xlsx;*.xls";
        private const string MATCH_PATTERN = @"\w*.(xlsx|xls)$";

        private enum AsyncComman
        {
            LOAD_REQUEST,
            SEND_REQUEST
        }

        #endregion

        private bool _isLoad;
        private SqlDatabase _sqlDatabase;

        private string _requestFilePath;
        private string _responceFilePath;

        private DataTable _requestDataTable;
        private DataTable _responceDataTable;

        public LawsuitSearchGISForm(string connectionString)
        {
            InitializeComponent();

            Init(connectionString);
        }

        private void Init(string connectionString)
        {
            _isLoad = false;

            _requestFilePath = string.Empty;
            _responceFilePath = string.Empty;

            _requestDataTable = null;
            _responceDataTable = null;

            try { _sqlDatabase = SqlDatabase.Create(connectionString); }
            catch (Exception ex) { ExceptionHandling(ex); }

            openFileDialog.FileName = FILE_NAME;
            openFileDialog.Filter = FILTER;

            saveFileDialog.FileName = FILE_NAME;
            saveFileDialog.Filter = FILTER;
            saveFileDialog.RestoreDirectory = true;
        }

        private void ExceptionHandling(Exception ex)
        {
            MessageBox.Show(ex.Message);
        }

        private void ToggleBootMode()
        {
            _isLoad = !_isLoad;

            Cursor = _isLoad ? Cursors.WaitCursor : Cursors.Default;

            toolStripStatusLabel.Visible = _isLoad;

            Enabled = !_isLoad;
        }

        private void AsyncLoad(AsyncComman command)
        {
            if (backgroundWorker.IsBusy == true) return;

            if (!_isLoad) ToggleBootMode();

            backgroundWorker.RunWorkerAsync(command);
        }

        private bool IsValidString(string text, string pattern)
        {
            return new Regex(pattern).IsMatch(text);
        }

        private DataTable ReadExcel(string file, ExcelDataSetConfiguration option = null)
        {
            using (var stream = File.Open(file, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet(option);

                    return result.Tables[0];
                }
            }
        }

        private void WriteExcel(string file, DataTable table)
        {
            if (table == null) return;

            var workBook = new XLWorkbook();

            workBook.Worksheets.Add(table);

            workBook.SaveAs(file);
        }

        private DataTable LoadRequestDataTable(string file)
        {
            if (file == string.Empty) return null;

            DataTable request = ReadExcel(file, new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = true,
                }
            });

            return request;
        }

        private SqlParameter[] GetRequestParameters(DataRow row)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@number", row.ItemArray[0]),
                new SqlParameter("@begin_date", row.ItemArray[5]),
                new SqlParameter("@end_date", row.ItemArray[6]),
                new SqlParameter("@fias", row.ItemArray[7]),
                new SqlParameter("@address", row.ItemArray[8]),
                new SqlParameter("@flat", row.ItemArray[9])
            };
        }

        private DataTable SendRequestDataTable(DataTable request)
        {
            if (request == null) return null;

            _sqlDatabase.Open();

            SqlCommand getRequestToDB = _sqlDatabase
                .CreateCommand(REQUEST_PROCEDURE, CommandType.StoredProcedure);

            DataTable responce = new DataTable("Responce");

            foreach (DataRow row in request.Rows)
            {
                getRequestToDB.Parameters.Clear();
                getRequestToDB.Parameters.AddRange(GetRequestParameters(row));

                SqlDataReader reader = getRequestToDB.ExecuteReader();

                responce.Load(reader);
            }

            return responce;
        }

        #region Events

        private void openFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel) return;

            filePathTextBox.Text = openFileDialog.FileName;

            AsyncLoad(AsyncComman.LOAD_REQUEST);
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidString(filePathTextBox.Text, MATCH_PATTERN)) throw new Exception(INVALID_FILE_TYPE);

                if (saveFileDialog.ShowDialog() == DialogResult.Cancel) return;

                _responceFilePath = saveFileDialog.FileName;

                AsyncLoad(AsyncComman.SEND_REQUEST);
            }
            catch (Exception ex)
            {
                ExceptionHandling(ex);
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                AsyncComman command = (AsyncComman)e.Argument;
                BackgroundWorker worker = sender as BackgroundWorker;

                switch (command)
                {
                    case AsyncComman.LOAD_REQUEST:
                        _requestDataTable = LoadRequestDataTable(_requestFilePath);
                        resReqStatusLabel.Text = "Запрос загружен";
                        break;
                    case AsyncComman.SEND_REQUEST:
                        try { _responceDataTable = SendRequestDataTable(_requestDataTable); }
                        catch { throw new Exception("Неверная структура запроса!"); }
                        WriteExcel(_responceFilePath, _responceDataTable);
                        resReqStatusLabel.Text = "Ответ получен";
                        break;
                    default:
                        throw new Exception("Ошибка в аргументах метода AsyncLoad.");
                }
            }
            catch (Exception ex) { ExceptionHandling(ex); }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _sqlDatabase.Close();
            if (_isLoad) ToggleBootMode();
        }

        private void LawsuitSearchGISForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isLoad) e.Cancel = true;
        }

        private void filePathTextBox_TextChanged(object sender, EventArgs e)
        {
            _requestFilePath = (sender as TextBox).Text;
        }

        #endregion
    }
}
