﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace EllisBillFilter
{
    public class BaseDropDown : GridLookUpEdit
    {
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit fProperties;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;

        public BaseDropDown()
        {
            InitializeComponent();
            this.Properties.View.OptionsView.ShowIndicator = false;
            this.Properties.View.OptionsView.ShowColumnHeaders = false;
            //    this.Properties.AllowFocused = false;
            this.Properties.PopupSizeable = false;
            //this.Properties.PopupFormSize = new System.Drawing.Size(200, 300); //ширина по умолчанию
            this.Text = "";
        }

        public void InitEx()
        {
            this.KeyDown += new KeyEventHandler(ucDropDown_KeyDown);
            gridView2.KeyDown += new KeyEventHandler(gridView2_KeyDown);
            this.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(Properties_ButtonClick);
        }

        void Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete && this.Properties.AllowNullInput != DevExpress.Utils.DefaultBoolean.False)
            {
                this.EditValue = "";
            }
        }

        void ucDropDown_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.PageDown && gridView2.IsLastVisibleRow == false)
            //    gridView2.FocusedRowHandle++;
            //if (e.KeyCode == Keys.PageUp && gridView2.IsFirstRow == false)
            //    gridView2.FocusedRowHandle--;
        }

        void gridView2_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Down && gridView2.IsLastVisibleRow == false)
            //    gridView2.FocusedRowHandle++;
            //if (e.KeyCode == Keys.Up && gridView2.IsFirstRow == false)
            //    gridView2.FocusedRowHandle--;
        }


        private void InitializeComponent()
        {
            this.fProperties = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.fProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // fProperties
            // 
            this.fProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fProperties.Name = "fProperties";
            this.fProperties.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            ((System.ComponentModel.ISupportInitialize)(this.fProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);
        }

        public object GetActiveCellValue(string fieldName)
        {
            if (this.Properties.View.FocusedRowHandle >= 0)
                return this.Properties.View.GetRowCellValue(this.Properties.View.FocusedRowHandle, this.Properties.View.Columns[fieldName]);
            else
                return null;

        }
    }

}
