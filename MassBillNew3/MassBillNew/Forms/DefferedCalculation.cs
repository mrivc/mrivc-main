﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using MrivcControls;
using MrivcControls.Data;
using MrivcHelpers.Helpers;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

namespace MassBillNew
{
    public partial class DefferedCalculation : Form
    {
        public DefferedCalculation()
        {
            InitializeComponent();

            this.Text = StaticConfig.Title(this.Text);

            SqlClass.Open();

            dateEdit1.DateTime = SqlClass.CurrentBeginDate();
            dateEdit2.DateTime = SqlClass.CurrentEndDate();

            checkedListBoxControlExt1.PopulateListBySqlQuery("select id_service,name from dbo.service order by position");
            checkedListBoxControlExt2.PopulateListBySqlQuery("select id_company,name from dbo.company where is_derive_company = 1 order by name");

            SqlClass.Close();
        }

        public void DefferedCalculation_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "";

            DAccess.DataModule.DefferedCalculationSelect(cDataSet.DefferedCalculation);
            addressFilter.ParameterTab = false;

            addressFilter.FillAddressTree();
        }

        bool CheckBeforeCalc()
        {
            string text = "";

            if (addressFilter.IsEmpty)
                text = "Необходимо выбрать адрес";

            if (text != "" )
            {
                Messages.Warning(text, "");
                return false;
            }
            else
            {
                return true;
            }
        }

        private void calcBtn_Click(object sender, EventArgs e)
        {

            Calculate();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            groupControl1.Enabled = radioButton2.Checked;
            checkBox1.Enabled = radioButton1.Checked;
            checkBox2.Enabled = radioButton1.Checked;
            checkBox3.Enabled = radioButton1.Checked;
        }

        void Calculate()
        {
            if (!CheckBeforeCalc())
                return;

            toolStripStatusLabel1.Text = "Идет расчет...";
            this.Refresh();

            var houseList = addressFilter.CreateHouseQuery();
            var flatList = addressFilter.CreateFlatQuery();
            var accountList = addressFilter.CreateAccountQuery();

            //string servMes = "2,4,43,46,246";

            //DataTable serviceList = DAccess.DataModule.GetDataTableByQueryCommand1("select ID_Service from dbo.Service where Id_service not in (" + servMes + ")");

            //var serviceString = "";

            //foreach (DataRow row in serviceList.Rows)
            //{
            //    serviceString = serviceString + row[0].ToString()+",";
            //}

            //serviceString = serviceString.TrimEnd(',');

            try
            {

                if (radioButton1.Checked)
                {
                    if (checkBox2.Checked)
                    {
                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("town.Calculate_Account_Service", new[,] { { "@idAccountList", accountList }  }, 0);
                    }
                    if (checkBox1.Checked)
                    {

                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("town.Calculate_Parameters", new[,] { { "@idAccountList", accountList }, { "@idFlatList", flatList }, { "@idHouseList", houseList } }, 0); 
                    }
                    if (checkBox2.Checked)
                    {
                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("town.Calculate_Account_Service", new[,] { { "@idAccountList", accountList } }, 0);
                    }
                        
                    if (checkBox1.Checked || checkBox2.Checked)
                    {
                        MessageBox.Show("Расчет параметров/начислений успешно завершен", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        Messages.Warning("Необходимо выбрать \"Считать параметры\" и\\или \"Считать начисления\"","Ошибка");
                    }
                }
                else
                {
                    var serviceListFine = checkedListBoxControlExt1.GetValue();
                    var deriveCompanyList = checkedListBoxControlExt2.GetValue();

                    if (radioButton2.Checked)
                    {
                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("town.Calculate_Fine",
                            new object[,] {
                                { "@beginDate", dateEdit1.DateTime },
                                { "@endDate", dateEdit2.DateTime },
                                { "@idAccountList", accountList },
                                { "@idServiceList", serviceListFine },
                                { "@idDeriveCompanyList", deriveCompanyList } }
                            , 0);
                    }
                   
                    MessageBox.Show("Расчет пени успешно завершен", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

       
                toolStripStatusLabel1.Text = "Готово";
            }
            catch (Exception ex)
            {
                        toolStripStatusLabel1.Text = "Ошибка";
                        throw ex;
            }
        }

        private void btnDolgUK_Click(object sender, EventArgs e)
        {
            var serviceList = "";
            var deriveCompanyList = "";

            serviceList = checkedListBoxControlExt1.GetValue();
            deriveCompanyList = checkedListBoxControlExt2.GetValue();

            var dt = DAccess.DataModule.GetDataTableByStoredProcedure1("town.DebtFine", new[,] { { "id_company", deriveCompanyList }, { "id_service", serviceList } });

            foreach (DataRow row in dt.Rows)
            {
                addressFilter.AddAddress(int.Parse(row["id_account"].ToString()), 0, row["caption"].ToString());
            }
        }

        private void btnUslMes_Click(object sender, EventArgs e)
        {
            string servMes = "2,4,43,46,246";

            DataTable serviceList = DAccess.DataModule.GetDataTableByQueryCommand1("select ID_Service from dbo.Service where Id_service in (" + servMes + ")");

            var serviceString = "";

            foreach (DataRow row in serviceList.Rows)
            {
                serviceString = serviceString + row[0].ToString() + ",";
            }

            serviceString = serviceString.TrimEnd(',');

            var accountList = addressFilter.CreateAccountQuery();
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("town.Calculate_Account_Service", new[,] { { "@idAccountList", accountList }, { "@service_List", serviceString } }, 0);

            MessageBox.Show("Расчет Закончен");
        }
    }
}
