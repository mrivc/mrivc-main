﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportSupplyContractsObjectAddress : MrivcExportCommand, ISingleHouseCommand
    {
        bool _isRSO;

        public string FiasHouseGuid { get; set; } = null;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                base.SenderID = value;
            }
        }

        public ExportSupplyContractsObjectAddress(string senderID) : base("exportSupplyResourceContractObjectAddress", "HomeManagementAsync", senderID, "exportSupplyResourceContractObjectAddress")
        {
            Description = "Экспорт объектов договоров ресурсоснабжения";
        }

        new void ExecuteToTempDB()
        {
            //base.ExecuteToTempDB(new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) },_exportNodeName);
            base.ExecuteToTempDB(_exportNodeName);
        }

        public new void Execute()
        {
            var dt = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.SelectContractGUID", new object[,] { { "senderid", SenderID } });

            foreach (DataRow row in dt.Rows)
            {
                var contractRootGuid = row[0].ToString();

                Values = new List<Tuple<string[], string>>()
                {
                    new Tuple<string[], string> (new string[] { "ContractRootGUID" }, contractRootGuid),
                    //new Tuple<string[], string> (new string[] { "ContractGUID" }, "063dfd95-ac9b-495e-a143-abbdfa627571"),
                };

                this.ExecuteToTempDB();

                if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
                {
                    DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportSupplyResourceContractObjectAddressData", new object[,] { { "senderID", SenderID }, { "contractRootGuid", contractRootGuid } });
                }
            }

        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            //return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.ExportHouseData", new object[,] { { "houseGUID", FiasHouseGuid } }, "Houses", "Entrance", "Premises");
            return new DataSet();
        }
    }
}
