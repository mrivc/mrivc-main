﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace EllisBillFilter.Lawsuit
{
    static public class Utils
    {
        static public string[] DeParseString(string namesString, char separator)
        {
            string[] names = namesString.Split(separator);

            for (int i = 0; i < names.Length; i++)
            {
                names[i] = names[i].Trim(' ');
                string[] temp = names[i].Split(' ');
                names[i] = string.Join(" ", temp);
            }

            return names;
        }

        static public int GetPeriodByDate(DateTime date)
        {
            int year = date.Year;
            int month = date.Month;

            return year * 12 + month;
        }

        static public int GetPeriodByDate(string dateString)
        {
            DateTime date = DateTime.Parse(dateString);

            int year = date.Year;
            int month = date.Month;

            return year * 12 + month;
        }

        static public void EnableControlToggle(object obj, bool toggle)
        {
            (obj as Control).Enabled = toggle;
        }

        static public void EnableControlToggle(List<object> objectsList, bool toggle)
        {
            foreach (Control obj in objectsList)
            {
                obj.Enabled = toggle;
            }
        }

        static public string JoinCheckedListBox(string separator, CheckedListBox cListBox, bool isOnlyChecked)
        {
            List<string> items = new List<string>();

            foreach (DataRowView item in cListBox.CheckedItems)
            {
                items.Add(item[0].ToString());
            }

            return string.Join(", ", items.ToArray()); ;
        }

        static public int GetDayFromDate(string date, char separator)
        {
            int day = int.Parse(date.Split(separator)[0]);

            return day;
        }

        static public List<string> RowsToStringsList(DataRowCollection rows, string column)
        {
            List<string> result = new List<string>();

            foreach (DataRow row in rows)
            {
                result.Add(row[column].ToString());
            }

            return result;
        }
    }
}
