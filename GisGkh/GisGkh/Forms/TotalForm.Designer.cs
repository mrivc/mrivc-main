﻿namespace GisGkh.Forms
{
    partial class TotalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExecute = new System.Windows.Forms.Button();
            this.gbSelection = new System.Windows.Forms.GroupBox();
            this.grbNsi = new System.Windows.Forms.GroupBox();
            this.chbTimer = new System.Windows.Forms.CheckBox();
            this.tbTimer = new System.Windows.Forms.TextBox();
            this.chbPredPodDan = new System.Windows.Forms.CheckBox();
            this.chbDorRes = new System.Windows.Forms.CheckBox();
            this.btnUnselectAll = new System.Windows.Forms.Button();
            this.gbIntObjects = new System.Windows.Forms.GroupBox();
            this.chbService = new System.Windows.Forms.CheckBox();
            this.chbMeasure = new System.Windows.Forms.CheckBox();
            this.chbHouse = new System.Windows.Forms.CheckBox();
            this.chbMeter = new System.Windows.Forms.CheckBox();
            this.chbAccount = new System.Windows.Forms.CheckBox();
            this.chbPD = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.periodFilter1 = new Helper.Controls.PeriodFilter();
            this.cbPeriod = new Helper.Controls.PeriodFilter();
            this.chbSenderAll = new System.Windows.Forms.CheckBox();
            this.gbSender = new System.Windows.Forms.GroupBox();
            this.customEdit1 = new MassBillNew.CustomEdit();
            this.cbAllHouse = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.customEdit2 = new MassBillNew.CustomEdit();
            this.gbSelection.SuspendLayout();
            this.grbNsi.SuspendLayout();
            this.gbIntObjects.SuspendLayout();
            this.gbSender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit1.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(420, 444);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(176, 23);
            this.btnExecute.TabIndex = 0;
            this.btnExecute.Text = "Выполнить";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // gbSelection
            // 
            this.gbSelection.Controls.Add(this.grbNsi);
            this.gbSelection.Controls.Add(this.btnUnselectAll);
            this.gbSelection.Controls.Add(this.gbIntObjects);
            this.gbSelection.Location = new System.Drawing.Point(18, 201);
            this.gbSelection.Name = "gbSelection";
            this.gbSelection.Size = new System.Drawing.Size(505, 216);
            this.gbSelection.TabIndex = 4;
            this.gbSelection.TabStop = false;
            this.gbSelection.Text = "Интеграция по объектам";
            // 
            // grbNsi
            // 
            this.grbNsi.Controls.Add(this.chbTimer);
            this.grbNsi.Controls.Add(this.tbTimer);
            this.grbNsi.Controls.Add(this.chbPredPodDan);
            this.grbNsi.Controls.Add(this.chbDorRes);
            this.grbNsi.Location = new System.Drawing.Point(6, 45);
            this.grbNsi.Name = "grbNsi";
            this.grbNsi.Size = new System.Drawing.Size(288, 165);
            this.grbNsi.TabIndex = 10;
            this.grbNsi.TabStop = false;
            // 
            // chbTimer
            // 
            this.chbTimer.AutoSize = true;
            this.chbTimer.Location = new System.Drawing.Point(6, 18);
            this.chbTimer.Name = "chbTimer";
            this.chbTimer.Size = new System.Drawing.Size(194, 17);
            this.chbTimer.TabIndex = 4;
            this.chbTimer.Text = "Ожидать ответа на запрос (мин.)";
            this.chbTimer.UseVisualStyleBackColor = true;
            this.chbTimer.CheckedChanged += new System.EventHandler(this.chbTimer_CheckedChanged);
            // 
            // tbTimer
            // 
            this.tbTimer.Enabled = false;
            this.tbTimer.Location = new System.Drawing.Point(206, 16);
            this.tbTimer.Name = "tbTimer";
            this.tbTimer.Size = new System.Drawing.Size(63, 20);
            this.tbTimer.TabIndex = 2;
            this.tbTimer.Text = "5";
            // 
            // chbPredPodDan
            // 
            this.chbPredPodDan.AutoSize = true;
            this.chbPredPodDan.Location = new System.Drawing.Point(6, 65);
            this.chbPredPodDan.Name = "chbPredPodDan";
            this.chbPredPodDan.Size = new System.Drawing.Size(218, 17);
            this.chbPredPodDan.TabIndex = 1;
            this.chbPredPodDan.Text = "Предварительная подготовка данных";
            this.chbPredPodDan.UseVisualStyleBackColor = true;
            // 
            // chbDorRes
            // 
            this.chbDorRes.AutoSize = true;
            this.chbDorRes.Location = new System.Drawing.Point(6, 42);
            this.chbDorRes.Name = "chbDorRes";
            this.chbDorRes.Size = new System.Drawing.Size(149, 17);
            this.chbDorRes.TabIndex = 0;
            this.chbDorRes.Text = "Экспорт договоров РСО";
            this.chbDorRes.UseVisualStyleBackColor = true;
            // 
            // btnUnselectAll
            // 
            this.btnUnselectAll.Location = new System.Drawing.Point(305, 19);
            this.btnUnselectAll.Name = "btnUnselectAll";
            this.btnUnselectAll.Size = new System.Drawing.Size(75, 23);
            this.btnUnselectAll.TabIndex = 5;
            this.btnUnselectAll.Text = "Снять все";
            this.btnUnselectAll.UseVisualStyleBackColor = true;
            this.btnUnselectAll.Click += new System.EventHandler(this.btnUnselectAll_Click);
            // 
            // gbIntObjects
            // 
            this.gbIntObjects.Controls.Add(this.chbService);
            this.gbIntObjects.Controls.Add(this.chbMeasure);
            this.gbIntObjects.Controls.Add(this.chbHouse);
            this.gbIntObjects.Controls.Add(this.chbMeter);
            this.gbIntObjects.Controls.Add(this.chbAccount);
            this.gbIntObjects.Controls.Add(this.chbPD);
            this.gbIntObjects.Location = new System.Drawing.Point(305, 45);
            this.gbIntObjects.Name = "gbIntObjects";
            this.gbIntObjects.Size = new System.Drawing.Size(194, 165);
            this.gbIntObjects.TabIndex = 9;
            this.gbIntObjects.TabStop = false;
            // 
            // chbService
            // 
            this.chbService.AutoSize = true;
            this.chbService.Checked = true;
            this.chbService.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbService.Location = new System.Drawing.Point(6, 19);
            this.chbService.Name = "chbService";
            this.chbService.Size = new System.Drawing.Size(62, 17);
            this.chbService.TabIndex = 1;
            this.chbService.Text = "Услуги";
            this.chbService.UseVisualStyleBackColor = true;
            // 
            // chbMeasure
            // 
            this.chbMeasure.AutoSize = true;
            this.chbMeasure.Checked = true;
            this.chbMeasure.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbMeasure.Location = new System.Drawing.Point(6, 134);
            this.chbMeasure.Name = "chbMeasure";
            this.chbMeasure.Size = new System.Drawing.Size(101, 17);
            this.chbMeasure.TabIndex = 8;
            this.chbMeasure.Text = "Показания ПУ";
            this.chbMeasure.UseVisualStyleBackColor = true;
            // 
            // chbHouse
            // 
            this.chbHouse.AutoSize = true;
            this.chbHouse.Checked = true;
            this.chbHouse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbHouse.Location = new System.Drawing.Point(6, 42);
            this.chbHouse.Name = "chbHouse";
            this.chbHouse.Size = new System.Drawing.Size(177, 17);
            this.chbHouse.TabIndex = 5;
            this.chbHouse.Text = "Дома, подъезды, помещения";
            this.chbHouse.UseVisualStyleBackColor = true;
            // 
            // chbMeter
            // 
            this.chbMeter.AutoSize = true;
            this.chbMeter.Checked = true;
            this.chbMeter.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbMeter.Location = new System.Drawing.Point(6, 111);
            this.chbMeter.Name = "chbMeter";
            this.chbMeter.Size = new System.Drawing.Size(102, 17);
            this.chbMeter.TabIndex = 7;
            this.chbMeter.Text = "Приборы учета";
            this.chbMeter.UseVisualStyleBackColor = true;
            // 
            // chbAccount
            // 
            this.chbAccount.AutoSize = true;
            this.chbAccount.Checked = true;
            this.chbAccount.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAccount.Location = new System.Drawing.Point(6, 65);
            this.chbAccount.Name = "chbAccount";
            this.chbAccount.Size = new System.Drawing.Size(103, 17);
            this.chbAccount.TabIndex = 5;
            this.chbAccount.Text = "Лицевые счета";
            this.chbAccount.UseVisualStyleBackColor = true;
            // 
            // chbPD
            // 
            this.chbPD.AutoSize = true;
            this.chbPD.Checked = true;
            this.chbPD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbPD.Location = new System.Drawing.Point(6, 88);
            this.chbPD.Name = "chbPD";
            this.chbPD.Size = new System.Drawing.Size(144, 17);
            this.chbPD.TabIndex = 6;
            this.chbPD.Text = "Платежные документы";
            this.chbPD.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(412, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "(начальный)";
            // 
            // periodFilter1
            // 
            this.periodFilter1.Location = new System.Drawing.Point(358, 12);
            this.periodFilter1.Name = "periodFilter1";
            this.periodFilter1.Size = new System.Drawing.Size(279, 61);
            this.periodFilter1.TabIndex = 7;
            // 
            // cbPeriod
            // 
            this.cbPeriod.Location = new System.Drawing.Point(358, 79);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Size = new System.Drawing.Size(279, 61);
            this.cbPeriod.TabIndex = 5;
            // 
            // chbSenderAll
            // 
            this.chbSenderAll.AutoSize = true;
            this.chbSenderAll.Location = new System.Drawing.Point(12, 21);
            this.chbSenderAll.Name = "chbSenderAll";
            this.chbSenderAll.Size = new System.Drawing.Size(45, 17);
            this.chbSenderAll.TabIndex = 9;
            this.chbSenderAll.Text = "Все";
            this.chbSenderAll.UseVisualStyleBackColor = true;
            this.chbSenderAll.CheckedChanged += new System.EventHandler(this.chbSenderAll_CheckedChanged);
            // 
            // gbSender
            // 
            this.gbSender.Controls.Add(this.chbSenderAll);
            this.gbSender.Controls.Add(this.customEdit1);
            this.gbSender.Location = new System.Drawing.Point(18, 12);
            this.gbSender.Name = "gbSender";
            this.gbSender.Size = new System.Drawing.Size(322, 81);
            this.gbSender.TabIndex = 10;
            this.gbSender.TabStop = false;
            this.gbSender.Text = "Поставщик информации";
            // 
            // customEdit1
            // 
            this.customEdit1.EditValue = "";
            this.customEdit1.Location = new System.Drawing.Point(12, 44);
            this.customEdit1.Name = "customEdit1";
            this.customEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.customEdit1.Properties.IncrementalSearch = true;
            this.customEdit1.Properties.PopupFormMinSize = new System.Drawing.Size(400, 300);
            this.customEdit1.Properties.PopupFormSize = new System.Drawing.Size(400, 300);
            this.customEdit1.Properties.ShowButtons = false;
            this.customEdit1.Properties.ShowPopupCloseButton = false;
            this.customEdit1.Size = new System.Drawing.Size(304, 20);
            this.customEdit1.TabIndex = 13;
            this.customEdit1.EditValueChanged += new System.EventHandler(this.customEdit1_EditValueChanged);
            // 
            // cbAllHouse
            // 
            this.cbAllHouse.AutoSize = true;
            this.cbAllHouse.Checked = true;
            this.cbAllHouse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAllHouse.Location = new System.Drawing.Point(12, 17);
            this.cbAllHouse.Name = "cbAllHouse";
            this.cbAllHouse.Size = new System.Drawing.Size(45, 17);
            this.cbAllHouse.TabIndex = 0;
            this.cbAllHouse.Text = "Все";
            this.cbAllHouse.UseVisualStyleBackColor = true;
            this.cbAllHouse.CheckedChanged += new System.EventHandler(this.cbAllHouse_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.customEdit2);
            this.groupBox1.Controls.Add(this.cbAllHouse);
            this.groupBox1.Location = new System.Drawing.Point(18, 99);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(322, 83);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Адрес дома";
            // 
            // customEdit2
            // 
            this.customEdit2.EditValue = "";
            this.customEdit2.Enabled = false;
            this.customEdit2.Location = new System.Drawing.Point(12, 39);
            this.customEdit2.Name = "customEdit2";
            this.customEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.customEdit2.Properties.IncrementalSearch = true;
            this.customEdit2.Properties.PopupFormMinSize = new System.Drawing.Size(400, 300);
            this.customEdit2.Properties.PopupFormSize = new System.Drawing.Size(400, 300);
            this.customEdit2.Properties.ShowButtons = false;
            this.customEdit2.Properties.ShowPopupCloseButton = false;
            this.customEdit2.Size = new System.Drawing.Size(304, 20);
            this.customEdit2.TabIndex = 14;
            // 
            // TotalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 488);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbSender);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.periodFilter1);
            this.Controls.Add(this.cbPeriod);
            this.Controls.Add(this.gbSelection);
            this.Controls.Add(this.btnExecute);
            this.Name = "TotalForm";
            this.Text = "Общая синхронизация";
            this.Load += new System.EventHandler(this.TotalForm_Load);
            this.gbSelection.ResumeLayout(false);
            this.grbNsi.ResumeLayout(false);
            this.grbNsi.PerformLayout();
            this.gbIntObjects.ResumeLayout(false);
            this.gbIntObjects.PerformLayout();
            this.gbSender.ResumeLayout(false);
            this.gbSender.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit1.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit2.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.GroupBox gbSelection;
        private System.Windows.Forms.GroupBox gbIntObjects;
        private System.Windows.Forms.CheckBox chbService;
        private System.Windows.Forms.CheckBox chbMeasure;
        private System.Windows.Forms.CheckBox chbHouse;
        private System.Windows.Forms.CheckBox chbMeter;
        private System.Windows.Forms.CheckBox chbAccount;
        private System.Windows.Forms.CheckBox chbPD;
        private System.Windows.Forms.Button btnUnselectAll;
        private Helper.Controls.PeriodFilter cbPeriod;
        private Helper.Controls.PeriodFilter periodFilter1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grbNsi;
        private System.Windows.Forms.CheckBox chbPredPodDan;
        private System.Windows.Forms.CheckBox chbDorRes;
        private System.Windows.Forms.CheckBox chbTimer;
        private System.Windows.Forms.TextBox tbTimer;
        private System.Windows.Forms.CheckBox chbSenderAll;
        private System.Windows.Forms.GroupBox gbSender;
        private System.Windows.Forms.CheckBox cbAllHouse;
        private MassBillNew.CustomEdit customEdit1;
        private System.Windows.Forms.GroupBox groupBox1;
        private MassBillNew.CustomEdit customEdit2;
    }
}