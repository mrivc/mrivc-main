﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EllisBillFilter.Helpers
{
    public class ReestrNumerator
    {
        private Dictionary<string, int> reestrList;

        public ReestrNumerator()
        {
            reestrList = new Dictionary<string, int>();
        }

        public int GetReestrNumber(string clientIdentity)
        {
            if (reestrList.ContainsKey(clientIdentity))
            {
                int max = reestrList.Where(r => r.Key == clientIdentity).Max(r => r.Value);
                reestrList.Add(clientIdentity, ++max);
                return max;
            }

            reestrList.Add(clientIdentity, 1);
            return 1;
        }
    }
}
