using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using System.Data.SqlClient;
using FastReport;
using System.IO;

namespace MassBillNew
{
    public partial class XtraFrxMenu : DevExpress.XtraEditors.XtraForm
    {
        public XtraFrxMenu()
        {
            InitializeComponent();


            this.Text = StaticConfig.Title(this.Text);


            //DrawMenu();
        }

        void DrawMenu()
        {
            SqlConnection conn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);

            SqlCommand cmd = new SqlCommand("select * from town.vfrx_menu where parent_item is null", conn);

            conn.Open();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                BarSubItem bs = new BarSubItem();


                barManager1.Items.Add(bs);

                bs.Name = "ReportsMenu";//reader["item"].ToString();
                bs.Caption = "������ FRX � ������";//reader["Display_Name"].ToString();

                MainMenu.LinksPersistInfo.Add(new DevExpress.XtraBars.LinkPersistInfo(bs));

                DrawSubMenu(bs);
            }

            conn.Close();
        }

        void DrawSubMenu(BarSubItem item)
         {
            
            SqlConnection conn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);

            SqlCommand cmd = new SqlCommand("select name,Display_Name from dbo.module where is_report = 1 and ext = 'FRX'", conn);

            conn.Open();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {

                    BarButtonItem bt = new BarButtonItem();

                    barManager1.Items.Add(bt);

                    bt.Name = reader["name"].ToString();
                    bt.Caption = reader["Display_Name"].ToString();
                    bt.ImageIndexDisabled = 1;

                    //bt.ItemClick += new ItemClickEventHandler(bt_ItemClick);

                    item.LinksPersistInfo.Add(new DevExpress.XtraBars.LinkPersistInfo(bt));


            }
            
            conn.Close();
        }

        private void ShowPreviewWithDialog(string text, string fileName, bool reportLoadedFromFile)
        {
            XtraFrxPreview frxPreview = new XtraFrxPreview(fileName, reportLoadedFromFile);
            frxPreview.Text = text;
            frxPreview.MdiParent = this;
            //frxPreview.FileName = fileName;
            //frxPreview.ReportLoadedFromFile = reportLoadedFromFile;
            frxPreview.Show();

            //FrxDialogShow(frxPreview);
        }

        void bt_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowPreviewWithDialog(e.Item.Caption, e.Item.Name, false);
        }

        public static void FrxDialogShow(XtraFrxPreview ParentPreviewForm)
        {
            //�������� � ������ ��� ����� �� ������������ ����� ���������
            //�������� �� ������������ ����� ��������� ������� ����, ��� �� ����� �������� �� �����
            XtraFrxDialog frxDialog = new XtraFrxDialog(ParentPreviewForm.FileName,ParentPreviewForm.ReportLoadedFromFile); 
            

            if (frxDialog.ShowDialog() == DialogResult.OK)
            {
                if (!frxDialog.CreateManyReports)//���� �� ������� ��������� �������, �� ���������� ����� �� ����� ���������
                { 
                    ParentPreviewForm.ShowReport(frxDialog.ReturnedReport);
                    
                }
            }
        }



        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            
            
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //���������� ����� ��������� ������ � �������� ������ ���������, 
                //����� ��������� ������ �������� ��������� � ���� ����� �����, ���� � ������� ����, ��� ����� ��� �������� �� �����
                ShowPreviewWithDialog(openFileDialog1.SafeFileName, openFileDialog1.FileName, true);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //XtraHouseFilterDialog houseFilterDialog = new XtraHouseFilterDialog();
            //houseFilterDialog.ShowDialog();
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {

        }



    }
}