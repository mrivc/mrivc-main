﻿namespace GisGkh
{
    partial class CloseAccountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCloseAccount = new System.Windows.Forms.Button();
            this.senderIdFilter1 = new Helper.Controls.SenderIdFilter();
            this.btnCloseCr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCloseAccount
            // 
            this.btnCloseAccount.Location = new System.Drawing.Point(440, 160);
            this.btnCloseAccount.Name = "btnCloseAccount";
            this.btnCloseAccount.Size = new System.Drawing.Size(177, 23);
            this.btnCloseAccount.TabIndex = 1;
            this.btnCloseAccount.Text = "Закрыть ЛС";
            this.btnCloseAccount.UseVisualStyleBackColor = true;
            this.btnCloseAccount.Click += new System.EventHandler(this.btnCloseAccount_Click);
            // 
            // senderIdFilter1
            // 
            this.senderIdFilter1.Location = new System.Drawing.Point(12, 12);
            this.senderIdFilter1.Name = "senderIdFilter1";
            this.senderIdFilter1.Size = new System.Drawing.Size(397, 121);
            this.senderIdFilter1.TabIndex = 0;
            // 
            // btnCloseCr
            // 
            this.btnCloseCr.Location = new System.Drawing.Point(283, 160);
            this.btnCloseCr.Name = "btnCloseCr";
            this.btnCloseCr.Size = new System.Drawing.Size(151, 23);
            this.btnCloseCr.TabIndex = 2;
            this.btnCloseCr.Text = "Закрыть ЛС Кап. рем.";
            this.btnCloseCr.UseVisualStyleBackColor = true;
            this.btnCloseCr.Click += new System.EventHandler(this.btnCloseCr_Click);
            // 
            // CloseAccountForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 192);
            this.Controls.Add(this.btnCloseCr);
            this.Controls.Add(this.btnCloseAccount);
            this.Controls.Add(this.senderIdFilter1);
            this.Name = "CloseAccountForm";
            this.Text = "CloseAccountForm";
            this.Load += new System.EventHandler(this.CloseAccountForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Helper.Controls.SenderIdFilter senderIdFilter1;
        private System.Windows.Forms.Button btnCloseAccount;
        private System.Windows.Forms.Button btnCloseCr;
    }
}