﻿using FastReport.Design.StandardDesigner;
using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace MassBillNew
{
    static class Program
    {
        //text123
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //var s = "";
            //foreach (var item in args)
            //{
            //    s += item;
            //}
            //Clipboard.SetText(s);
            //MessageBox.Show(s);

            //args = new string[] { @"Provider=SQLOLEDB.1;Password=sr4kx54;PersistSecurityInfo=True;UserID=BUTORIN;InitialCatalog=Murmansk;DataSource=Srv-mrivc6;UseProcedureforPrepare=1;AutoTranslate=True;PacketSize=4096;WorkstationID=BUTORIN-PC;UseEncryptionforData=False;Tagwithcolumncollationwhenpossible=False|Module=Common\MassBillNew.exe|Report=_fk0101_NET" };

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DevExpress.LookAndFeel.DefaultLookAndFeel defaultLook = new DevExpress.LookAndFeel.DefaultLookAndFeel();
            defaultLook.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;

            string[] prms = string.Concat(args).Split(new Char[] { '|' });
            if (args.Length > 0)
            {
                DAccess.DataModule.ConnectionString = prms[0];
                StaticConfig.SetConnection(prms[0]);
                Properties.Settings.Default["MurmanskConnectionString"] = StaticConfig.MainConnection.ConnectionString;
                DAccess.DataModule.Connection1 = new SqlConnection(StaticConfig.MainConnection.ConnectionString);


                switch (GetExecuteModule(prms))
                {
                    case "form1":
                        Application.Run(new Form1());
                        break;
                    case "xtrafrxmenu":
                        Application.Run(new XtraFrxMenu());
                        break;
                    case "dropmeter":
                        Application.Run(new DropMeter());
                        break;
                    case "penetration":
                        Application.Run(new Penetration());
                        break;
                    case "defferedcalculation":
                        Application.Run(new DefferedCalculation());
                        break;
                    case "_billstd_full_designer":
                        BillStd_Full_OpenInDesigner();
                        break;
                }

              

                if (GetReport(prms) != string.Empty)
                    Application.Run(new XtraFrxPreview(GetReport(prms), false));
            }
            else
            {
                DAccess.DataModule.SetConnection(Properties.Settings.Default["MurmanskConnectionStringTest"].ToString());
                DAccess.DataModule.Connection1 = new SqlConnection(Properties.Settings.Default.MurmanskConnectionStringTest);
                //StaticConfig.SetConnectionReadyString(Properties.Settings.Default["MurmanskConnectionString"].ToString());
                //Properties.Settings.Default["MurmanskConnectionString"] = StaticConfig.MainConnection.ConnectionString;

                Application.Run(new Form1());

                //Application.Run(new ReportDesigner());
                //BillStd_Full_OpenInDesigner();

                //DesignerForm designerForm = new DesignerForm(DAccess.DataModule.ConnectionString);
                //designerForm.Load += designerForm_Load;

                //Application.Run(designerForm);


            }
        }

        static void designerForm_Load(object sender, EventArgs e)
        {
            DesignerForm form = sender as DesignerForm;

            form.Designer.MdiMode = true;

            //form.UpdateContent();
        }

        static void BillStd_Full_OpenInDesigner()
        {
            if (SqlClass.EllisAdminRole() == true)
                Application.Run(StaticConfig.OpenInDesigner(false, "_BillStd_Full"));
            else
                MessageBox.Show("Для выполнения необходима роль пользователя \"Администратор\"");
        }

        static string GetExecuteModule(string[] prms)
        {
            if (prms[2].ToLower().Contains("execute") && prms[2].IndexOf("=") > -1)
            {
                string execute = prms[2].Substring(prms[2].IndexOf("=") + 1);
                return execute.ToLower();
            }
            return "";
        }

        static string GetReport(string[] prms)
        {
            if (prms[2].ToLower().Contains("report") && prms[2].IndexOf("=") > -1)
            {
                string execute = prms[2].Substring(prms[2].IndexOf("=") + 1);
                return execute.ToLower();
            }
            return "";
        }
    }
}
