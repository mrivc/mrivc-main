﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EllisBillFilter.Lawsuit
{
    public class SearchRatio
    {
        public string Column { get; }
        public string Pattern { get; }

        public SearchRatio(string column, string pattern)
        {
            Column = column;
            Pattern = pattern;
        }
    }
}
