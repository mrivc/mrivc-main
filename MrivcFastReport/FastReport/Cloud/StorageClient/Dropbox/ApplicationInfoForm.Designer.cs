namespace FastReport.Cloud.StorageClient.Dropbox
{
    partial class ApplicationInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAppKey = new System.Windows.Forms.Label();
            this.labelAppSecret = new System.Windows.Forms.Label();
            this.tbAppKey = new System.Windows.Forms.TextBox();
            this.tbAppSecret = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(166, 65);
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(247, 65);
            // 
            // labelAppId
            // 
            this.labelAppKey.AutoSize = true;
            this.labelAppKey.Location = new System.Drawing.Point(12, 15);
            this.labelAppKey.Name = "labelAppId";
            this.labelAppKey.Size = new System.Drawing.Size(51, 13);
            this.labelAppKey.TabIndex = 1;
            this.labelAppKey.Text = "App Key:";
            // 
            // labelAppSecret
            // 
            this.labelAppSecret.AutoSize = true;
            this.labelAppSecret.Location = new System.Drawing.Point(12, 41);
            this.labelAppSecret.Name = "labelAppSecret";
            this.labelAppSecret.Size = new System.Drawing.Size(64, 13);
            this.labelAppSecret.TabIndex = 2;
            this.labelAppSecret.Text = "App Secret:";
            // 
            // tbAppId
            // 
            this.tbAppKey.Location = new System.Drawing.Point(142, 12);
            this.tbAppKey.Name = "tbAppId";
            this.tbAppKey.Size = new System.Drawing.Size(180, 20);
            this.tbAppKey.TabIndex = 3;
            // 
            // tbAppSecret
            // 
            this.tbAppSecret.Location = new System.Drawing.Point(142, 38);
            this.tbAppSecret.Name = "tbAppSecret";
            this.tbAppSecret.Size = new System.Drawing.Size(180, 20);
            this.tbAppSecret.TabIndex = 4;
            // 
            // ApplicationInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 100);
            this.Controls.Add(this.tbAppKey);
            this.Controls.Add(this.tbAppSecret);
            this.Controls.Add(this.labelAppSecret);
            this.Controls.Add(this.labelAppKey);
            this.Name = "ApplicationInfoForm";
            this.Text = "Application Info";
            this.Controls.SetChildIndex(this.labelAppKey, 0);
            this.Controls.SetChildIndex(this.labelAppSecret, 0);
            this.Controls.SetChildIndex(this.btnOk, 0);
            this.Controls.SetChildIndex(this.tbAppSecret, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.tbAppKey, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAppKey;
        private System.Windows.Forms.Label labelAppSecret;
        private System.Windows.Forms.TextBox tbAppKey;
        private System.Windows.Forms.TextBox tbAppSecret;
    }
}