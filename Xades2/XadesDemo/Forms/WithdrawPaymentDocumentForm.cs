﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo
{
    public partial class WithdrawPaymentDocumentForm : Form
    {
        public string SenderID
        {
            get { return ""; }
        }

        public int Period
        {
            get { return -1; }
        }

        public WithdrawPaymentDocumentForm()
        {
            InitializeComponent();
        }

        private void WithdrawPaymentDocumentForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var hg = MrivcData.HouseFiasGuidList(SenderID);
            foreach (var item in hg)
            {
                    var withdrawBill = new WithdrawPaymentDocument(SenderID, Period);
                    withdrawBill.FiasHouseGuid = item;
                    withdrawBill.Execute();
            }
        }
    }
}
