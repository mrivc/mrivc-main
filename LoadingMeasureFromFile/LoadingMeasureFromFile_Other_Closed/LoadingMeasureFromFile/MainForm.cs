﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Threading.Tasks;

namespace LoadingMeasureFromFile_Other
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Метод для создания соединения с БД
        /// </summary>
        private SqlConnection CreateConnection(string dataBaseName)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString =
                $@"Data Source=SRV-MRIVC6;Initial Catalog={dataBaseName};Persist Security Info=True;User ID=komzolchev;Password=kOpyn2";
            try
            {
                connection.Open();
            }
            catch
            {
                MessageBox.Show($"Ошибка соединения с базой данных {dataBaseName}");
                return null;
            }

            return connection;
        }

        private DateTime ChangeDateDay(DateTime date, int day)
        {
            int diff = day - date.Day;
            DateTime newDate = date.AddDays(diff);

            return newDate;
        }

        /// <summary>
        /// Список для хранения экземпляров класса счетчика с данными
        /// и загрузка в этот список всех данных из файла как объектов класса Meter
        /// </summary>
        private void LoadMetersFromFile()
        {
            using (StreamReader sr = new StreamReader(filePath.Text))
            {
                string line;
                string[] parameters;

                int idMeter;
                DateTime date;
                double measure;

                Measure meter;

                while ((line = sr.ReadLine()) != null)
                {
                    parameters = line.Split(';');

                    try
                    {
                        idMeter = int.Parse(parameters[0]);
                        date = ChangeDateDay(DateTime.Parse(parameters[2]), 26);
                        //date = date.AddMonths(-4);
                        measure = Math.Round(double.Parse(parameters[1].Replace('.', ',')), 3);

                        meter = new Measure(idMeter, date, measure);

                        MetersIntoMeter("meter_account", meter);
                        MetersIntoMeterMeasure(meter);
                    } catch {}
                }
            }
        }

        /// <summary>
        /// Метод загрузки показаний из файла в таблицу meter_measure_a
        /// </summary>
        private int ErrorCount { get; set; } = 0;
        private void MetersIntoMeter(string table, Measure meter)
        {
            using (SqlConnection connection = CreateConnection("Meter"))
            {
                if (connection == null)
                    return;

                SqlCommand command = connection.CreateCommand();
                command.CommandTimeout = 0;

                SqlTransaction transaction;

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                command.CommandText =
                        $"INSERT INTO {table} (id_meter, measure_date, measure_value) " +
                        $"VALUES ({meter.IdMeter}, '{meter.SqlFormattedDate}', {meter.MeasureValue.ToString().Replace(',', '.')})";
                try
                {
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    ErrorCount++;
                    IntoLog(meter, ex.TargetSite.ToString(), ex.Message);
                    transaction.Rollback();
                }
            }
        }

        /// <summary>
        /// Метод загрузки показаний из файла в процедуру [dbo].[Meter_Measure_Add]
        /// </summary>
        private void MetersIntoMeterMeasure(Measure meter)
        {
            using (SqlConnection connection = CreateConnection("Murmansk"))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandTimeout = 0;

                SqlCommand tempCommand = connection.CreateCommand();
                SqlCommand getMeterType = connection.CreateCommand();

                SqlTransaction transaction;

                int check = 0;

                command.CommandText = "[dbo].[Meter_Measure_Add]";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add("@ID_Meter", System.Data.SqlDbType.Int);
                command.Parameters.Add("@Measure_Date", System.Data.SqlDbType.DateTime);
                command.Parameters.Add("@Measure_Value", System.Data.SqlDbType.Float);
                command.Parameters.Add("@ID_Meter_Measure_Type", System.Data.SqlDbType.Int);
                command.Parameters.Add("@Change_IF_Exists", System.Data.SqlDbType.Bit);
                command.Parameters.Add("@ID_Meter_Status", System.Data.SqlDbType.Int);

                getMeterType.CommandText = $@"
                        select distinct m.ID_Meter_Type
                        from dbo.Meter_Measure mm
                        inner join dbo.Meter m on m.ID_Meter = mm.ID_Meter
                        where mm.ID_Meter = {meter.IdMeter}";

                try
                {
                    int checkMeasureVolumeResult
                        = CheckMeasureVolume(connection, meter, int.Parse(getMeterType.ExecuteScalar().ToString()));
                    if (checkMeasureVolumeResult == 1)
                        throw new MeterMeasureException(
                            "Недопустимый объем показаний!");
                    else if (checkMeasureVolumeResult == -1)
                        throw new MeterMeasureException(
                            "Возникала непредвиденная ошибка!!!");

                    tempCommand.CommandText =
                        "SELECT COUNT(*) FROM Meter_Measure WHERE " +
                        $"ID_Meter = {meter.IdMeter} " +
                        $"AND Measure_Date = '{meter.SqlFormattedDate}' " +
                        $"AND Measure_Value > {meter.MeasureValue}";

                    check = int.Parse(tempCommand.ExecuteScalar().ToString());

                    if (check != 0)
                        throw new MeterMeasureException(
                            "Новые показания по счестчику имеют туже дату что и в базе, но меньшее значение!");
                }
                catch (Exception ex)
                {
                    ErrorCount++;
                    IntoLog(meter, ex.TargetSite.ToString(), ex.Message);
                    return;
                }

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                command.Parameters[0].Value = meter.IdMeter;
                command.Parameters[1].Value = meter.MeasureDate;
                command.Parameters[2].Value = meter.MeasureValue;
                command.Parameters[3].Value = 3;
                command.Parameters[4].Value = 0;
                command.Parameters[5].Value = 1;

                for (int i = 0; i < 6; ++i)
                    command.Parameters[i].Direction = System.Data.ParameterDirection.Input;

                try
                {
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    ErrorCount++;
                    IntoLog(meter, ex.TargetSite.ToString(), ex.Message);
                    transaction.Rollback();
                }
            }
        }

        /// <summary>
        /// Метод промерки объема показаний
        /// </summary>
        private int CheckMeasureVolume(SqlConnection connection, Measure meter, int idMeterType)
        {
            SqlCommand command = connection.CreateCommand();

            command.CommandText = $@"
                select Measure_Value 
                from Meter_Measure mm
                inner join (
	                select mm.ID_Meter, max(mm.measure_date) as Last_Measure_date 
	                from dbo.meter_measure mm
	                inner join meter m on m.id_meter = mm.id_meter 
		                and mm.ID_Meter = {meter.IdMeter}
	                group by mm.ID_Meter
                ) as Temp on Temp.ID_Meter = mm.ID_Meter and Temp.Last_Measure_date = mm.Measure_Date";

            int prevMeasureValue = 0;
            try
            {
                prevMeasureValue = int.Parse(command.ExecuteScalar().ToString());
            }
            catch
            {
                return -1;
            }

            switch (idMeterType)
            {
                case 64:
                    if ((meter.MeasureValue - prevMeasureValue) > 10)
                        return 1;
                    break;
                case 1:
                case 2:
                case 4:
                case 5:
                    if ((meter.MeasureValue - prevMeasureValue) > 150)
                        return 1;
                    break;
                case 7:
                case 8:
                case 10:
                    if ((meter.MeasureValue - prevMeasureValue) > 4000)
                        return 1;
                    break;
                case 3:
                case 6:
                    if ((meter.MeasureValue - prevMeasureValue) > 50)
                        return 1;
                    break;
            }

            return 0;
        }


        /// <summary>
        /// Логирование ошибок
        /// </summary>
        private string pathLog = "log.xml";
        private List<string[]> logs = new List<string[]>();

        private void IntoLog(Measure meter, string from, string message)
        {
            string[] temp = 
                {
                    DateTime.Now.ToString("HH:mm:ss"),
                    from,
                    message,
                    meter.IdMeter.ToString(),
                    meter.SqlFormattedDate,
                    meter.MeasureValue.ToString()
                };
            logs.Add(temp);
        }

        private void LogIntoFile()
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(pathLog);

            XmlElement xRoot = xDoc.DocumentElement;

            XmlElement logElem = xDoc.CreateElement("log");
            XmlAttribute dateAttr = xDoc.CreateAttribute("date");
            XmlText dateText = xDoc.CreateTextNode(DateTime.Now.ToString("dd MMMM yyyy"));

            dateAttr.AppendChild(dateText);
            logElem.Attributes.Append(dateAttr);

            foreach (string[] parameters in logs)
            {
                XmlElement errorElem = xDoc.CreateElement("error");
                XmlAttribute timeAttr = xDoc.CreateAttribute("time");

                XmlElement fromElem = xDoc.CreateElement("from");
                XmlElement messageElem = xDoc.CreateElement("message");

                XmlElement meterElem = xDoc.CreateElement("measure");
                XmlAttribute idMeterAttr = xDoc.CreateAttribute("idMeter");
                XmlAttribute measureDateAttr = xDoc.CreateAttribute("measureDate");
                XmlAttribute measureValueAttr = xDoc.CreateAttribute("measureValue");

                XmlText timeText = xDoc.CreateTextNode(parameters[0]);
                XmlText fromText = xDoc.CreateTextNode(parameters[1]);
                XmlText messageText = xDoc.CreateTextNode(parameters[2]);
                XmlText idMeterText = xDoc.CreateTextNode(parameters[3]);
                XmlText measureDateText = xDoc.CreateTextNode(parameters[4]);
                XmlText measureValueText = xDoc.CreateTextNode(parameters[5]);

                timeAttr.AppendChild(timeText);
                fromElem.AppendChild(fromText);
                messageElem.AppendChild(messageText);

                idMeterAttr.AppendChild(idMeterText);
                measureDateAttr.AppendChild(measureDateText);
                measureValueAttr.AppendChild(measureValueText);

                meterElem.Attributes.Append(idMeterAttr);
                meterElem.Attributes.Append(measureDateAttr);
                meterElem.Attributes.Append(measureValueAttr);

                errorElem.Attributes.Append(timeAttr);
                errorElem.AppendChild(fromElem);
                errorElem.AppendChild(messageElem);
                errorElem.AppendChild(meterElem);

                logElem.AppendChild(errorElem);

                xRoot.AppendChild(logElem);

                xDoc.Save(pathLog);
            }
        }

        private void Clear()
        {
            logs.Clear();
            ErrorCount = 0;
        }

        #region Events
        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!File.Exists(pathLog))
            {
                using (FileStream fs = new FileStream(pathLog, FileMode.Create))
                {
                    using (XmlTextWriter xmlOut = new XmlTextWriter(fs, Encoding.Unicode))
                    {
                        xmlOut.Formatting = Formatting.Indented;

                        xmlOut.WriteStartDocument();
                        xmlOut.WriteComment("Log list from LoadingMeasureFromFile");

                        xmlOut.WriteStartElement("logs");

                        xmlOut.WriteEndElement();
                        xmlOut.WriteEndDocument();
                    }
                }
            }
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
                filePath.Text = openFileDialog.FileName;
        }

        private async void btnLoadFromFile_Click(object sender, EventArgs e)
        {
            try
            {
                Enabled = false;
                Cursor = Cursors.WaitCursor;

                await Task.Run(() => {
                    LoadMetersFromFile();
                });

                if (ErrorCount == 0)
                    MessageBox.Show("Загрузка показаний завершена!");
                else
                    MessageBox.Show($"Загрузка показаний завершена!\nОшибок при загрузке - {ErrorCount}");

                LogIntoFile();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Enabled = true;
                Cursor = Cursors.Default;
            }
        }

        private void filePath_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Link;
            else
                e.Effect = DragDropEffects.None;
        }

        private void filePath_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (files != null)
                filePath.Text = files[0];
        }

        private void openLogBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists(pathLog))
                System.Diagnostics.Process.Start(pathLog);
        }
        #endregion
    }
}