﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helper.Data
{
    public class DAccess
    {
        private static DModule dModule;

        public static DModule DataModule
        {
            get
            {
                if (dModule == null)
                {
                    dModule = new DModule();
                }
                return dModule;
            }
        }
    }
}
