unit uProperties;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                                                                            //
//              ���������� �������� "����� ���"                               //
//              ���������� � ������ uPropertyCollection                       //
//              ��������� ���� �������                                        //
//              �����: ������� �.�.                                           //
//              Ellis-Krd@Mail.ru                                             //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

Interface

{NOFORMAT}
Const
  pApplication    : String = 'Application';    //Forms.TApplication
  pScreen         : String = 'Screen';         //Forms.TScreen
  pHintWindowClass: String = 'HintWindowClass';//Controls.THintWindowClass
  pResultForm     : String = 'ResultForm';     //Forms.TForm
  pConnection     : String = 'Connection';     //ADODB.TADOConnection
  pImageList      : String = 'ImageList';      //ImgList.TCustomImageList
  pEllisCOMServer : String = 'EllisCOMServer'; //TObject
  pDLLHandle      : String = 'DLLHandle';      //Windows.THandle
  pNativeStyle    : String = 'NativeStyle';    //Boolean
  pStyleKind      : String = 'StyleKind';      //cxLookAndFeels.TcxLookAndFeelKind
  pPCStyle        : String = 'PCStyle';        //cxPC.TcxPCStyleID

  pID_Account: String = 'ID_Account';
  pPeriod    : String = 'Period';

  pModules_Folder: String = 'Modules_Folder';//���� � ����� � ��������
  pLog_Folder    : String = 'Log_Folder';    //���� � ����� ��� �����
  pTemp_Folder   : String = 'Temp_Folder';   //���� � ����� ��� ��������� ������

  pWork_Period     : String = 'Work_Period';     //��� �������� �������
  pWork_Period_Name: String = 'Work_Period_Name';//�������� �������� �������
  pWork_Begin_Date : String = 'Work_Begin_Date'; //������ �������� �������
  pWork_End_Date   : String = 'Work_End_Date';   //����� �������� �������

  pDefault_End_Date: String = '_Def_End_Date';// ���� ��������� �� ���������

{FORMAT}
Implementation

End.
