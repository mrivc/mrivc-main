﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XadesDemo.Forms;

namespace XadesDemo
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        void ShowChildForm(Form childForm)
        {
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void soapCommandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new CommandForm());
        }

        private void gisSenderProviderToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exportHouseDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ExportHouseDataForm());
        }

        private void импортИнформацииОДомахToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ImportHouseDataForm());
        }

        private void MainForm_Load(object sender, EventArgs e)
        {}

        private void exportNsiMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ExportNsiListForm());
        }

        private void importBillsMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ImportBillForm());
        }

        private void importServicesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ImportServicesForm());
        }

        private void exportBillMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ExportPaymentDocumentForm());
        }

        private void exportMeterToolStrip_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ExportMeteringDeviceForm());
        }

        private void importMeterToolStrip_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ImportMeteringDeviceForm());
        }

        private void отозватьПлатежныеДокументыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new WithdrawPaymentDocumentForm());
        }

        private void импортПоказанийПУToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ImportMeasureForm());
        }

        private void exportMeasureToolStrip_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ExportMeasureForm());
        }

        private void отчетОВыполненныхКомандахToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new CommandReportForm());
        }

        private void списокУслугДляПривязкиКДоговоруToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void эспортДоговоровУправленияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new ExportContractsForm());
        }

        private void LogSaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChildForm(new LogSaveForm());
        }
    }
}
