﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Payments.DS;
using System.Linq;

namespace Payments.Controls
{
    public partial class GridPaymentEllisReestr : UserControl
    {
        private bool initialized = false;

        public GridPaymentEllisReestr()
        {
            InitializeComponent();
            deBeginDate.DateTime = DateTime.Now;
            deEndDate.DateTime = DateTime.Now;
            initialized = true;
        }

        public void LoadData()
        {
            if (!initialized)
                return;

            DAccess.DataModule.PaymentEllisReestrSelect(paymentsDataSet.VPaymentEllisReestFull, deBeginDate.DateTime, deEndDate.DateTime);

            gridPaymentEllis.InitEx();
            gridViewPaymentEllisReestr.ExpandAllGroups();
        }

        public void ReLoadData()
        {
            if (!initialized)
                return;
            paymentsDataSet.VPaymentEllisReestFull.Clear();
            DAccess.DataModule.PaymentEllisReestrSelect(paymentsDataSet.VPaymentEllisReestFull, deBeginDate.DateTime, deEndDate.DateTime);
            gridViewPaymentEllisReestr.ExpandAllGroups();
        }

        private void deBeginDate_EditValueChanged(object sender, EventArgs e)
        {
            ReLoadData();
        }

        private void deEndDate_EditValueChanged(object sender, EventArgs e)
        {
            ReLoadData();
        }

        private void gridViewPaymentEllisReestr_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0 && (int)gridViewPaymentEllisReestr.GetDataRow(e.RowHandle)["ID_Loaded_Status"] == 1)
                e.Appearance.BackColor = Color.LightGreen;
        }

        private void btnCreateDbfReestr_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            List<string> files = new List<string>();
            DataRow[] rows = paymentsDataSet.VPaymentEllisReestFull.Select("", "[CLIENT_CODE] DESC");
            var groups = paymentsDataSet.VPaymentEllisReestFull.AsEnumerable().GroupBy(row => row["CLIENT_CODE"]);
            DbfReestr.ClearUnloadedFiles();

            foreach (var group in groups)
            {
                DataTable table = paymentsDataSet.VPaymentEllisReestFull.Clone();
                foreach (DataRow row in group)
                {
                    row["ID_Loaded_Status"] = 1;
                    table.ImportRow(row);
                }

                var r = group.AsEnumerable<PaymentsDataSet.VPaymentEllisReestFullRow>().First();
                DateTime d = DateTime.Parse(r.DATPLAT);
                string fileName =  r["CLIENT_CODE"].ToString() + StringHelper.DayCode(d.Day) + StringHelper.ZeroCode(d.Month);
                DbfReestr.LastUnloadedPaymentsDate = d;           
                DbfReestr.UnloadToDBF(fileName, table, r["CLIENT_NAME"].ToString());
            }

            DAccess.DataModule.PaymentLoadedStatusUpdate(paymentsDataSet.VPaymentEllisReestFull);
            string zipedFile = DbfReestr.CompressLastUploadedFiles(true);
            MessageBox.Show("Реестры платежей были созданы успешно.\n\nФайл реестров находится тут:\n" + zipedFile, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

            ParentForm.Text = "Выбрузка реестра платежей " + DbfReestr.LastConnectionString;
            Cursor = Cursors.Default;
        }
    }
}
