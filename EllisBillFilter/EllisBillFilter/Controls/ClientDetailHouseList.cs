﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using System.Collections;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace EllisBillFilter
{
    public delegate void ShowAllBillFiltersByHouseIDEventHandler(int idHouse);
    public delegate void RequestReloadHouseListEventHandler();

    public partial class ClientDetailHouseList : UserControl
    {
        [System.ComponentModel.Browsable(true)]
        public event ShowAllBillFiltersByHouseIDEventHandler ShowAllBillFiltersByHouseIDEvent;
        [System.ComponentModel.Browsable(true)]
        public event RequestReloadHouseListEventHandler RequestReloadHouseListEvent;


        private int isDebtHouseList; 
        private int idClientDetail;
        private int period;

        private bool ignoreCheck;

        public ClientDetailHouseList()
        {
            InitializeComponent();
            ignoreCheck = true;
        }

        /// <summary> Перезагрузка дерева адресов для УК по idClientDetail
        /// </summary>
        /// <param name="idClientDetail"></param>
        /// <param name="period">Период в котором дома принадлежали УК</param>
        /// <param name="show_all_houses">False - показывать только текущие дома по УК, по заданному периоду. True - показывать все дома которые когда либо были в УК.</param>
        public void ReLoadHouseListForDeriveCompany(int idClientDetail, int period, bool show_all_houses)
        {
            this.FindForm().Cursor = Cursors.WaitCursor;
            mDataSet.ClientDetailHouse.Clear();

            this.isDebtHouseList = (show_all_houses == true ? 1:0);
            this.idClientDetail = idClientDetail;
            this.period = period;

            DAccess.DataModule.ClientDetailHouseSelect(mDataSet.ClientDetailHouse, idClientDetail, period, show_all_houses);
            InitHouseCheckedList();

            this.FindForm().Cursor = Cursors.Default;
        }

        public void ClearHouseList() {
            mDataSet.ClientDetailHouse.Clear();
        }

        private void InitHouseCheckedList()
        {
            houseList.ItemChecking -= houseList_ItemChecking;
            for (int i = 0; i < (houseList.DataSource as IList).Count; i++)
            {
                DataRowView rowView = houseList.GetItem(i) as DataRowView;
                if (rowView!=null && rowView["ID_Client_Detail"] != DBNull.Value)
                    houseList.SetItemChecked(i, true);
            }
            houseList.ItemChecking += new DevExpress.XtraEditors.Controls.ItemCheckingEventHandler(houseList_ItemChecking);
        }

        private bool AllowEdit()
        {
            if (this.period == DAccess.Instance.WorkPeriod)
                return true;

            MessageBox.Show("Запрещено редактирование  записей в прошлых периодах.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return false;
        }

        private void houseList_ItemChecking(object sender, DevExpress.XtraEditors.Controls.ItemCheckingEventArgs e)
        {
            if (!AllowEdit())
            {
                e.Cancel = true;
                return;
            }

            if (ignoreCheck)
            {
                e.Cancel = true;
                return;
            }

            int idHouse = (int)houseList.GetItemValue(e.Index);

            if (e.NewValue == CheckState.Unchecked)
            {
                DAccess.DataModule.ClientDetailHouseUpdate(idClientDetail,
                    idHouse, 0, isDebtHouseList);
            }
            else if (e.NewValue == CheckState.Checked)
            {
                DAccess.DataModule.ClientDetailHouseUpdate(idClientDetail,
                    idHouse, 1, isDebtHouseList);
            }
        }



        private void miHouseCheckAll_Click(object sender, EventArgs e)
        {
            if (!AllowEdit())
                return;

            ignoreCheck = false;
            houseList.CheckAll();
            ignoreCheck = true;
        }

        private void miHouseUnCheckAll_Click(object sender, EventArgs e)
        {
            if (!AllowEdit())
                return;

            ignoreCheck = false;
            houseList.UnCheckAll();
            ignoreCheck = true;
        }

        private void miHouseCheck_SelectedHouses_Click(object sender, EventArgs e)
        {
            if (!AllowEdit())
                return;

            ignoreCheck = false;
            houseList.CheckSelectedItems();
            ignoreCheck = true;
        }

        private void miHouseUnCheck_SelectedHouses_Click(object sender, EventArgs e)
        {
            if (!AllowEdit())
                return;

            ignoreCheck = false;
            houseList.UnCheckSelectedItems();
            ignoreCheck = true;
        }

        private void miHouse_ShowAllClientDetailByHouse_Click(object sender, EventArgs e)
        {
            object house = houseList.GetItemValue(houseList.SelectedIndex);
            if (house != null)
            {
                if (ShowAllBillFiltersByHouseIDEvent != null)
                    ShowAllBillFiltersByHouseIDEvent((int)house);
            }
        }

        private void miHouse_Refresh_Click(object sender, EventArgs e)
        {
            if (idClientDetail <= 0)
                return;

            if (RequestReloadHouseListEvent != null)
                RequestReloadHouseListEvent();
            //ReLoadHouseListForDeriveCompany(idClientDetail, period, (isDebtHouseList==1?true:false));
        }

        private void miHouse_CopyIdHouseToClipboard_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(houseList.GetItemValue(houseList.SelectedIndex).ToString());
        }

        private void txtHouseSearch_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            for (int i = 0; i < (houseList.DataSource as IList).Count; i++)
            {
                DataRowView rowView = houseList.GetItem(i) as DataRowView;
                if (rowView["House_address"].ToString().ToLower().Contains(e.NewValue.ToString().ToLower()))
                {
                    houseList.SelectedItem = houseList.GetItem(i);
                    break;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.Multiselect = false;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string[] lines = System.IO.File.ReadAllLines(openFileDialog1.FileName);
                    SelectHouses(lines);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void SelectHouses(string[] lines)
        {
            List<int> houses = new List<int>();
            string housesNotFound = "";
            foreach (string h in lines)
            {
                try
                {
                    int index = houseList.FindItem(0, true, delegate(ListBoxFindItemArgs e)
                    {
                        e.IsFound = object.Equals(int.Parse(h), e.ItemValue);
                    });
                    if (index > -1)
                        houses.Add(index);
                    else
                        housesNotFound = h + Environment.NewLine;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (houses.Count == lines.Length)
                MessageBox.Show("Найдено совпадений " + houses.Count.ToString() + " из " + lines.Length.ToString(), "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Найдено совпадений " + houses.Count.ToString() + " из " + lines.Length.ToString() + "." + Environment.NewLine + Environment.NewLine +
                "Следующие дома не были найдены: " + housesNotFound + Environment.NewLine + "Список не найденных ИД домов скопирован в буфер обмена.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (housesNotFound.Length > 0)
                Clipboard.SetText(housesNotFound);

            foreach (int h in houses)
            {
                houseList.SetSelected(h, true);
            }
        }

        private void miHouseCheckUncheckSelected_Click(object sender, EventArgs e)
        {
            if (houseList.SelectedIndices.Count == 0)
                return;

            ignoreCheck = false;
            if (houseList.GetItemCheckState(houseList.SelectedIndices[0]) == CheckState.Checked)
                houseList.UnCheckSelectedItems();
            else
                houseList.CheckSelectedItems();
            ignoreCheck = true;
        }

        private void houseList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //ignoreCheck = false;
            //if (houseList.GetItemCheckState(houseList.SelectedIndices[0]) == CheckState.Checked)
            //    houseList.UnCheckSelectedItems();
            //else
            //    houseList.CheckSelectedItems();
            //ignoreCheck = true;
        }
    }
}
