﻿using System;
using System.Collections.Generic;
using System.Text;
using DrvFRLib;
using Payments.DS;

namespace Payments
{
    public class OperationRegister
    {
        DrvFRClass fis;

        public string LastOperationRegisterName;
        public object LastOperationRegisterValue;

        public OperationRegister(DrvFRClass fis)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            this.fis = fis;
        }

        public int LastSaleNumber {
            get 
            {
                if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                    return -1;
                return (int)this.GetRegisterValue(148);
            }
        }

        public object GetRegisterValue(int registerNumber)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return 0;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.RegisterNumber = registerNumber;
            fis.GetOperationReg();
            LastOperationRegisterName = fis.NameOperationReg;
            LastOperationRegisterValue = fis.ContentsOfOperationRegister;
            return LastOperationRegisterValue;
        }
    }
}
