using System;
using System.Collections.Generic;

namespace GisGkh.Commands
{
    public interface ICommand
    {
        string Execute(string xmlBody, string senderID);
    }
}