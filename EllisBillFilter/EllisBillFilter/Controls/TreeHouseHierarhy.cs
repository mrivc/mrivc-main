﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace EllisBillFilter
{
    public partial class TreeHouseHierarhy : UserControl
    {
        public TreeHouseHierarhy()
        {
            InitializeComponent();
        }

        public TreeList Tree
        {
            get { return treeList1; }
        }

        public long SelectedId
        {
            get {
                DataRowView data = (DataRowView)treeList1.GetDataRecordByNode(treeList1.Selection[0]);
                return (long) data.Row["id"];
            }
        }

        public void LoadData()
        {
            mDataSet.HouseHierarhy.Clear();
            DAccess.DataModule.HouseHierarhySelect(mDataSet.HouseHierarhy);
            treeList1.ForceInitialize();
        }

        public TreeListNode FindNodeByKeyId(int value)
        {
            TreeListOperationFindNodeByValue op = new TreeListOperationFindNodeByValue(colId, value.ToString());
            treeList1.NodesIterator.DoOperation(op);

            return op.Node;
        }

        private void miShowHouseID_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(SelectedId.ToString());
            MessageBox.Show("ID дома " + SelectedId.ToString() + " скопирован в буфер обмена.");
        }

        private void treeList1_MouseDown(object sender, MouseEventArgs e)
        {
            TreeListHitInfo ht = treeList1.CalcHitInfo(new Point(e.X, e.Y));
            if (ht != null && ht.Node != null)
                treeList1.FocusedNode = ht.Node;
        }

        private void cmsHouseTree_Opening(object sender, CancelEventArgs e)
        {
            if (treeList1.Selection[0].Level != 2)
                miShowHouseID.Enabled = false;
            else
                miShowHouseID.Enabled = true;
        }

        private void treeList1_KeyDown(object sender, KeyEventArgs e)
        {
            TreeListNode node = treeList1.FocusedNode;
         //   if (!node.HasChildren) return;
            if (e.KeyData == Keys.Left)
            {
                if (!node.Expanded && node.ParentNode != null)
                    node.ParentNode.Expanded = false;
                else
                    node.Expanded = false;

                e.Handled = true;
            }
            else if (e.KeyData == Keys.Right && !node.Expanded)
            {
                node.Expanded = true;
                e.Handled = true;
            }
        }

        internal class TreeListOperationFindNodeByValue : TreeListOperation
        {

            private TreeListNode foundNode;
            private TreeListColumn column;
            private string value;

            public TreeListOperationFindNodeByValue(TreeListColumn column, string value)
            {
                this.foundNode = null;
                this.column = column;
                this.value = value;
            }

            public override void Execute(TreeListNode node)
            {
                string s = node[column].ToString();
                if (s == value)
                    this.foundNode = node;
            }

            public override bool NeedsVisitChildren(TreeListNode node) { return foundNode == null; }

            public TreeListNode Node { get { return foundNode; } }
        }
    }
}
