﻿namespace MrivcControls
{
    public class EllisAddress
    {
        public int ID { get; set; }
        public int TypeID { get; set; }
        public int DeriveCompany { get; set; } = -3;
        public int District { get; set; } = -3;
        public int Settlement { get; set; } = -3;
        public int MaintainCompany { get; set; } = -3;
        public string Caption { get; set; }

        public override string ToString()
        {
            return Caption;
        }
    }
}
