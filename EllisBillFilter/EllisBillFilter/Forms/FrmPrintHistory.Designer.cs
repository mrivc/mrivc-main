﻿namespace EllisBillFilter
{
    partial class FrmPrintHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrintHistory));
            this.gridPrintHistory1 = new MrivcControls.GridPrintHistory();
            this.SuspendLayout();
            // 
            // gridPrintHistory1
            // 
            this.gridPrintHistory1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPrintHistory1.Location = new System.Drawing.Point(0, 0);
            this.gridPrintHistory1.Name = "gridPrintHistory1";
            this.gridPrintHistory1.Size = new System.Drawing.Size(1126, 758);
            this.gridPrintHistory1.TabIndex = 0;
            // 
            // FrmPrintHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 758);
            this.Controls.Add(this.gridPrintHistory1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrintHistory";
            this.Text = "История печати квитанций";
            this.Load += new System.EventHandler(this.FrmPrintHistory_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MrivcControls.GridPrintHistory gridPrintHistory1;


    }
}