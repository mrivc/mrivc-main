﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using BillsLoader.DM;

namespace BillsLoader.Logic
{
    using MrivcHelpers.Helpers;

    public class ReestrInfo
    {
        System.IO.FileInfo file;
        string rootPath;
        string error = string.Empty;

        int idPackType;
        int packNumber;
        DateTime packDate;

        /// <summary>
        /// Номер реестра
        /// </summary>
        public string ReestrNumber { get; set; }

        /// <summary>
        /// Загружать с файла Excel со столбцами Account,Sumplat,Datplat
        /// </summary>
        public bool LoadFromExcel { get; set; }
        public string BillFilter { get; set; }
        public string TempTableName { get; set; }

        /// <summary>
        /// Количество обработанных платежей
        /// </summary>
        public int PayCountLoaded;

        /// <summary>
        /// Количество загруженный платежей в БД
        /// </summary>
        private int payCountLoadedInEllis;
        /// <summary>
        /// Сумма загруженный платежей в БД
        /// </summary>
        private double paySummLoadedInEllis;

        /// <summary>
        /// Время начала загрузки реестра
        /// </summary>
        public DateTime StartLoadTime;

        /// <summary>
        /// Время окончания загрузки реестра
        /// </summary>
        public DateTime EndLoadTime;

        /// <summary>
        /// Если лицевой счет == 12 символам и нету кода авторизации, то
        /// программа пытается сама определить текущую УК и код фильтра
        /// </summary>
        public bool AutoCompliteAccount;
        int idClient;

        /// <summary>
        /// Типы счетчиков по которым будем загружать показания
        /// </summary>
        public MeterLoadType MeterForLoad { get; set; }

        /// <summary>
        /// Лицевые счета по которым не удалось определить код фильтра, попадают сюда с разбивкой по УК;
        /// после окончания загрузки ЛС из этого списка выгружаются по файлам (каждый файл это УК)
        /// </summary>
        private SortedList<string, string> wrongSeparatedData = new SortedList<string, string>();

        public ReestrInfo(System.IO.FileInfo file, MeterLoadType meterForLoad)
        {
            this.MeterForLoad = (meterForLoad ?? new MeterLoadType(true, true, true, true));
            this.file = file;
            this.PayCountLoaded = 0;
            this.paySummLoadedInEllis = -1;
            this.payCountLoadedInEllis = -1;
            rootPath = Properties.Settings.Default.RootReestrPath;
        }

        public ReestrInfo(int idPackType, int packNumber, DateTime packDate, string filePath, MeterLoadType meterForLoad, string reestrNumber)
        {
            this.MeterForLoad = (meterForLoad ?? new MeterLoadType(true, true, true, true));
            this.file = new FileInfo(filePath);
            this.idPackType = idPackType;
            this.packNumber = packNumber;
            this.packDate = packDate;

            this.PayCountLoaded = 0;
            this.paySummLoadedInEllis = -1;
            this.payCountLoadedInEllis = -1;

            ReestrNumber = reestrNumber;
        }

        public ReestrInfo(int idPackType, int packNumber, DateTime payDate, string filePath, int idClient, bool autoCompliteAccount, MeterLoadType meterForLoad, string reestrNumber)
        {
            this.MeterForLoad = (meterForLoad ?? new MeterLoadType(true, true, true, true));
            this.file = new FileInfo(filePath);
            this.idPackType = idPackType;
            this.packNumber = packNumber;
            this.packDate = payDate;
            this.idClient = idClient;

            this.PayCountLoaded = 0;
            this.paySummLoadedInEllis = -1;
            this.payCountLoadedInEllis = -1;

            this.AutoCompliteAccount = autoCompliteAccount;

            ReestrNumber = reestrNumber;
        }

        public FileInfo SourceFile
        {
            get { return file; }
        }

        public int IdPackType
        {
            get { return idPackType; }
        }

        public int PackNumber
        {
            get { return packNumber; }
        }

        public DateTime PackDate
        {
            get { return packDate; }
        }

        public string Error
        {
            get { return error; }
        }

        public int IdClient
        {
            get { return idClient; }
        }

        public bool Check()
        {
            try
            {
                string[] pathSegment = file.DirectoryName.Split(new Char[] { '\\' });
                
                int packTypePathSegmentIndex = Properties.Settings.Default.PackTypePathSegmentIndex;
                int packDatePathSegmentIndex = Properties.Settings.Default.PackDatePathSegmentIndex;

                string packType = pathSegment[packTypePathSegmentIndex].Substring(0, pathSegment[packTypePathSegmentIndex].IndexOf("_"));
                string packDate = pathSegment[packDatePathSegmentIndex];
                int year, month, day;


                if (file.Name.IndexOf("_loaded") > -1)
                    throw new Exception("Файл уже был загружен ранее: " + file.FullName);

                if (!int.TryParse(packType, out idPackType))
                    throw new Exception("Не удалось распознать тип пачки: " + file.FullName);

                if (!int.TryParse(packDate.Substring(0, 4), out year) ||
                    !int.TryParse(packDate.Substring(4, 2), out month) ||
                    !int.TryParse(packDate.Substring(6, 2), out day))
                    throw new Exception("Не удалось распознать дату пачки пачки: " + file.FullName);

                if (file.Name.IndexOf("_") == -1)
                    throw new Exception("Не удалось распознать номер пачки по имени файла: " + file.FullName);

                if (!int.TryParse(file.Name.Substring(0, file.Name.IndexOf("_")), out packNumber))
                    throw new Exception("Не удалось распознать номер пачки по имени файла: " + file.FullName);

                this.packDate = new DateTime(year, month, day);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                FileHelper.WriteLog("ReestrInfo.Check() - " + ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Количество загруженный платежей в БД
        /// </summary>
        public int PayCountLoadedInEllis 
        {
            get
            {
                if (this.payCountLoadedInEllis == -1)
                    DAccess.DataModule.SumPay(this);
                return this.payCountLoadedInEllis;
            }
            set
            {
                payCountLoadedInEllis = value;
            }
        }

        /// <summary>
        /// Сумма загруженный платежей в БД
        /// </summary>
        public double PaySummLoadedInEllis
        {
            get
            {
                if (this.paySummLoadedInEllis == -1)
                    DAccess.DataModule.SumPay(this);
                return this.paySummLoadedInEllis;
            }
            set
            {
                paySummLoadedInEllis = value; 
            }
        }

        public bool Peni { get; set; }

        /// <summary>
        /// Сохраняем в отдельный файл строку с ошибкой, по которой не смогли определить код фильтра
        /// </summary>
        /// <param name="line"></param>
        public void LogWrongData(string line)
        {
            string fname = FileHelper.GetFileName(file.FullName);
            string fpath = FileHelper.GetFilePath(file.FullName);

            string[] s = line.Split(new Char[] { ';', ':' });
            string ls = s[2];

            string company = "";

            //Буторин Д.А. 3.07.14, Добавил обработку данных: если ЛС неопределен, то
            //компанию оставлять пустой
            try
            {
                company = DAccess.DataModule.CurrentDeriveCompanyFileName(ls);
            }
            catch (Exception)
            {
                company = "";
                //throw;
            }
            //End

            string key = fpath + "ош_" + fname + "_" + company;

            if (wrongSeparatedData.ContainsKey(key))
                wrongSeparatedData[key] = wrongSeparatedData[key] + Environment.NewLine + line;
            else
                wrongSeparatedData.Add(key, line);
        }

        public void SaveWrongDataInSeparetedFiles()
        {
            foreach (KeyValuePair<string, string> kvp in wrongSeparatedData)
                FileHelper.WriteTextFile(kvp.Key + ".txt", kvp.Value);
        }

        /// <summary>
        /// Сохранить статистику загруги реестра в таблицу БД town.Log_BillLoader
        /// </summary>
        public void LogLoadInfo()
        {
            LogLoadInfo(string.Empty);
        }

        /// <summary>
        /// Сохранить статистику загруги реестра в таблицу БД town.Log_BillLoader
        /// </summary>
        public void LogLoadInfo(string comment)
        {
            DAccess.DataModule.LogBillLoader(this, comment);
        }
    }

    public class MeterLoadType
    {
        public bool Vh { get; set; }
        public bool Vg { get; set; }
        public bool Gaz { get; set; }
        public bool El { get; set; }

        public MeterLoadType(bool vh, bool vg, bool gaz, bool el)
        {
            this.Vh = vh;
            this.Vg = vg;
            this.Gaz = gaz;
            this.El = el;
        }
    }
}
