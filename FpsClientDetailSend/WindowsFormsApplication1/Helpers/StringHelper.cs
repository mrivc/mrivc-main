﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsFormsApplication1
{
    public static class StringHelper
    {
        public static string GetSubstring(string str, string startChar, string endChar)
        {
            int _ind1 = str.IndexOf(startChar);
            int _ind2 = str.IndexOf(endChar, _ind1);

            if (_ind1 > 0 && _ind2 > _ind1)
                return str.Substring(_ind1 + startChar.Length, _ind2 - _ind1 - startChar.Length);
            //return str.Substring(str.IndexOf(startChar)+1, str.LastIndexOf(endChar) - str.IndexOf(startChar)-1);
            else
                return "";
        }

        public static string GetShortTimeDateString()
        {
            return ZeroCode(DateTime.Now.Month) + ZeroCode(DateTime.Now.Day) + ZeroCode(DateTime.Now.Hour) + ZeroCode(DateTime.Now.Minute) + ZeroCode(DateTime.Now.Millisecond);
        }

        public static string GetSqlConnectionPart(this string sqlConnection, string part)
        {
            return GetSubstring(sqlConnection, part + "=", ";");
        }

        public static string ReplaceEscape(string str)
        {
            str = str.Replace("'", "''");
            return str;
        }

        public static string ZeroCode4(int value)
        {
            if (value < 10)
                return "000" + value.ToString();
            if (value < 100)
                return "00" + value.ToString();
            if (value < 1000)
                return "0" + value.ToString();
            return value.ToString();
        }

        public static string ZeroCode(int value)
        {
            if (value < 10)
                return "0" + value.ToString();
            return value.ToString();
        }

        public static string DayCode(int day)
        {
            if (day < 10)
                return "0" + day.ToString();
            return day.ToString();
        }
    }
}
