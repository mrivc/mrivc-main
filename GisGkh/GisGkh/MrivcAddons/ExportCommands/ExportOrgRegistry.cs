﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportOrgRegistry : MrivcExportCommand
    {
        public ExportOrgRegistry() : base("exportOrgRegistry", "RegOrgServiceAsync", "exportOrgRegistryResult")
        {
            Description = "Экспорт сведений об организациях";
        }

        public new void Execute()
        {
            this.ExecuteToTempDB();
        }

        new void ExecuteToTempDB()
        {
            var inns = DAccess.DataModule.GetDataTableByQueryCommand1($@"select distinct ogrn from GIS.[VContacts] where ogrn is not null");

            if (inns.Rows.Count != 0)
            {
                int f = 0;

                foreach (DataRow row in inns.Rows)
                {
                    var values1 = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string> (new [] { "OGRN" },row["OGRN"].ToString())
                    };
                    if (f==0)
                    {
                        AddChildElement(MrivcStatic.GisConfig.Services[Service].Methods["exportOrgRegistry_inn_add"].Template, values1, "exportOrgRegistryRequest");
                        f = 1;
                    }
                    else
                    {
                        Values = values1;
                    }

                    base.ExecuteToTempDB(_exportNodeName);

                    if (!HasErrors)
                    {
                        if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
                        {
                            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.SaveExportedSenderID");
                        }
                    }
                }

                
            }

        }
    }
}
