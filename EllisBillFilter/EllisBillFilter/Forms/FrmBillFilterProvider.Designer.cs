﻿namespace EllisBillFilter
{
    partial class FrmBillFilterProvider
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBillFilterProvider));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.cmdBillFilterDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miBillFilterDetail_DeleteProviderFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miBillFilterDetail_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.billFilterDetailProviderFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gridViewBillFilterProvider = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Bill_Filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Service = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Provider_Group_Filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riProviderGroupFilter = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.providerGroupFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colServiceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtCondition = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.rbConditionIn = new System.Windows.Forms.RadioButton();
            this.rbConditionNotIn = new System.Windows.Forms.RadioButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.cmsProviderGroup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miProviderGroupAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miProviderGroupRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miProviderGroupRemoveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.providerGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewProvider = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Provider_Group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProvider_Group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode_Group = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            this.cmdBillFilterDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterDetailProviderFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillFilterProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riProviderGroupFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerGroupFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            this.cmsProviderGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.providerGroupBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.ContextMenuStrip = this.cmdBillFilterDetail;
            this.gridControl1.DataSource = this.billFilterDetailProviderFilterBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridViewBillFilterProvider;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riProviderGroupFilter});
            this.gridControl1.Size = new System.Drawing.Size(600, 564);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBillFilterProvider});
            // 
            // cmdBillFilterDetail
            // 
            this.cmdBillFilterDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miBillFilterDetail_DeleteProviderFilter,
            this.toolStripSeparator1,
            this.miBillFilterDetail_Refresh});
            this.cmdBillFilterDetail.Name = "cmdBillFilterDetail";
            this.cmdBillFilterDetail.Size = new System.Drawing.Size(370, 54);
            // 
            // miBillFilterDetail_DeleteProviderFilter
            // 
            this.miBillFilterDetail_DeleteProviderFilter.Image = global::EllisBillFilter.Properties.Resources.clear;
            this.miBillFilterDetail_DeleteProviderFilter.Name = "miBillFilterDetail_DeleteProviderFilter";
            this.miBillFilterDetail_DeleteProviderFilter.Size = new System.Drawing.Size(369, 22);
            this.miBillFilterDetail_DeleteProviderFilter.Text = "Снять фильтр по поставщикам по выбранной услуги";
            this.miBillFilterDetail_DeleteProviderFilter.Click += new System.EventHandler(this.miBillFilterDetail_DeleteProviderFilter_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(366, 6);
            // 
            // miBillFilterDetail_Refresh
            // 
            this.miBillFilterDetail_Refresh.Image = global::EllisBillFilter.Properties.Resources.refresh;
            this.miBillFilterDetail_Refresh.Name = "miBillFilterDetail_Refresh";
            this.miBillFilterDetail_Refresh.Size = new System.Drawing.Size(369, 22);
            this.miBillFilterDetail_Refresh.Text = "Обновить";
            this.miBillFilterDetail_Refresh.Click += new System.EventHandler(this.miBillFilterDetail_Refresh_Click);
            // 
            // billFilterDetailProviderFilterBindingSource
            // 
            this.billFilterDetailProviderFilterBindingSource.DataMember = "BillFilterDetailProviderFilter";
            this.billFilterDetailProviderFilterBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewBillFilterProvider
            // 
            this.gridViewBillFilterProvider.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Bill_Filter,
            this.colID_Service,
            this.colID_Provider_Group_Filter,
            this.colServiceName});
            this.gridViewBillFilterProvider.CustomizationFormBounds = new System.Drawing.Rectangle(1055, 690, 216, 199);
            this.gridViewBillFilterProvider.GridControl = this.gridControl1;
            this.gridViewBillFilterProvider.Name = "gridViewBillFilterProvider";
            this.gridViewBillFilterProvider.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewBillFilterProvider.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewBillFilterProvider.OptionsView.ShowGroupPanel = false;
            this.gridViewBillFilterProvider.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // colID_Bill_Filter
            // 
            this.colID_Bill_Filter.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colID_Bill_Filter.AppearanceCell.Options.UseBackColor = true;
            this.colID_Bill_Filter.FieldName = "ID_Bill_Filter";
            this.colID_Bill_Filter.Name = "colID_Bill_Filter";
            this.colID_Bill_Filter.OptionsColumn.AllowEdit = false;
            this.colID_Bill_Filter.OptionsColumn.ReadOnly = true;
            // 
            // colID_Service
            // 
            this.colID_Service.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colID_Service.AppearanceCell.Options.UseBackColor = true;
            this.colID_Service.FieldName = "ID_Service";
            this.colID_Service.Name = "colID_Service";
            this.colID_Service.OptionsColumn.AllowEdit = false;
            this.colID_Service.OptionsColumn.ReadOnly = true;
            // 
            // colID_Provider_Group_Filter
            // 
            this.colID_Provider_Group_Filter.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colID_Provider_Group_Filter.AppearanceCell.Options.UseBackColor = true;
            this.colID_Provider_Group_Filter.Caption = "Фильтр по поставщикам";
            this.colID_Provider_Group_Filter.ColumnEdit = this.riProviderGroupFilter;
            this.colID_Provider_Group_Filter.FieldName = "ID_Provider_Group_Filter";
            this.colID_Provider_Group_Filter.Name = "colID_Provider_Group_Filter";
            this.colID_Provider_Group_Filter.Visible = true;
            this.colID_Provider_Group_Filter.VisibleIndex = 1;
            this.colID_Provider_Group_Filter.Width = 182;
            // 
            // riProviderGroupFilter
            // 
            this.riProviderGroupFilter.AutoHeight = false;
            this.riProviderGroupFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riProviderGroupFilter.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Provider_Group_Filter", "ID_Provider_Group_Filter", 145, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Provider_Group_Filter", "Provider_Group_Filter", 115, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Condition", "Condition", 55, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "Info", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.riProviderGroupFilter.DataSource = this.providerGroupFilterBindingSource;
            this.riProviderGroupFilter.DisplayMember = "Provider_Group_Filter";
            this.riProviderGroupFilter.DropDownRows = 15;
            this.riProviderGroupFilter.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.riProviderGroupFilter.Name = "riProviderGroupFilter";
            this.riProviderGroupFilter.NullText = "Нет фильтра";
            this.riProviderGroupFilter.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.riProviderGroupFilter.ShowFooter = false;
            this.riProviderGroupFilter.ShowHeader = false;
            this.riProviderGroupFilter.ValueMember = "ID_Provider_Group_Filter";
            this.riProviderGroupFilter.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.riProviderGroupFilter_EditValueChanging);
            // 
            // providerGroupFilterBindingSource
            // 
            this.providerGroupFilterBindingSource.DataMember = "ProviderGroupFilter";
            this.providerGroupFilterBindingSource.DataSource = this.mDataSet;
            // 
            // colServiceName
            // 
            this.colServiceName.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colServiceName.AppearanceCell.Options.UseBackColor = true;
            this.colServiceName.Caption = "Услуга";
            this.colServiceName.FieldName = "ServiceName";
            this.colServiceName.Name = "colServiceName";
            this.colServiceName.OptionsColumn.AllowEdit = false;
            this.colServiceName.OptionsColumn.ReadOnly = true;
            this.colServiceName.Visible = true;
            this.colServiceName.VisibleIndex = 0;
            this.colServiceName.Width = 178;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1022, 650);
            this.splitContainerControl1.SplitterPosition = 600;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.txtCondition);
            this.groupControl1.Location = new System.Drawing.Point(2, 567);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(598, 81);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Текст условия";
            // 
            // txtCondition
            // 
            this.txtCondition.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCondition.Location = new System.Drawing.Point(2, 22);
            this.txtCondition.Multiline = true;
            this.txtCondition.Name = "txtCondition";
            this.txtCondition.ReadOnly = true;
            this.txtCondition.Size = new System.Drawing.Size(594, 57);
            this.txtCondition.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.rbConditionIn);
            this.groupBox1.Controls.Add(this.rbConditionNotIn);
            this.groupBox1.Location = new System.Drawing.Point(4, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 54);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Условие включения поставщиков:";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(318, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 32);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rbConditionIn
            // 
            this.rbConditionIn.AutoSize = true;
            this.rbConditionIn.Location = new System.Drawing.Point(139, 22);
            this.rbConditionIn.Name = "rbConditionIn";
            this.rbConditionIn.Size = new System.Drawing.Size(87, 17);
            this.rbConditionIn.TabIndex = 1;
            this.rbConditionIn.TabStop = true;
            this.rbConditionIn.Text = "ТОЛЬКО (in)";
            this.rbConditionIn.UseVisualStyleBackColor = true;
            this.rbConditionIn.CheckedChanged += new System.EventHandler(this.rbConditionIn_CheckedChanged);
            // 
            // rbConditionNotIn
            // 
            this.rbConditionNotIn.AutoSize = true;
            this.rbConditionNotIn.Location = new System.Drawing.Point(11, 22);
            this.rbConditionNotIn.Name = "rbConditionNotIn";
            this.rbConditionNotIn.Size = new System.Drawing.Size(122, 17);
            this.rbConditionNotIn.TabIndex = 0;
            this.rbConditionNotIn.TabStop = true;
            this.rbConditionNotIn.Text = "ВСЕ КРОМЕ (not in)";
            this.rbConditionNotIn.UseVisualStyleBackColor = true;
            this.rbConditionNotIn.CheckedChanged += new System.EventHandler(this.rbConditionNotIn_CheckedChanged);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.ContextMenuStrip = this.cmsProviderGroup;
            this.gridControl2.DataSource = this.providerGroupBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(0, 62);
            this.gridControl2.MainView = this.gridViewProvider;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(416, 588);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProvider});
            // 
            // cmsProviderGroup
            // 
            this.cmsProviderGroup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miProviderGroupAdd,
            this.miProviderGroupRemove,
            this.toolStripSeparator2,
            this.miProviderGroupRemoveAll});
            this.cmsProviderGroup.Name = "cmsProviderGroup";
            this.cmsProviderGroup.Size = new System.Drawing.Size(222, 76);
            // 
            // miProviderGroupAdd
            // 
            this.miProviderGroupAdd.Image = global::EllisBillFilter.Properties.Resources.record_add;
            this.miProviderGroupAdd.Name = "miProviderGroupAdd";
            this.miProviderGroupAdd.Size = new System.Drawing.Size(221, 22);
            this.miProviderGroupAdd.Text = "Добавить поставщика";
            this.miProviderGroupAdd.Click += new System.EventHandler(this.miProviderGroupAdd_Click);
            // 
            // miProviderGroupRemove
            // 
            this.miProviderGroupRemove.Image = global::EllisBillFilter.Properties.Resources.record_delete;
            this.miProviderGroupRemove.Name = "miProviderGroupRemove";
            this.miProviderGroupRemove.Size = new System.Drawing.Size(221, 22);
            this.miProviderGroupRemove.Text = "Удалить поставщика";
            this.miProviderGroupRemove.Click += new System.EventHandler(this.miProviderGroupRemove_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(218, 6);
            // 
            // miProviderGroupRemoveAll
            // 
            this.miProviderGroupRemoveAll.Name = "miProviderGroupRemoveAll";
            this.miProviderGroupRemoveAll.Size = new System.Drawing.Size(221, 22);
            this.miProviderGroupRemoveAll.Text = "Удалить всех поставщиков";
            this.miProviderGroupRemoveAll.Click += new System.EventHandler(this.miProviderGroupRemoveAll_Click);
            // 
            // providerGroupBindingSource
            // 
            this.providerGroupBindingSource.DataMember = "ProviderGroup";
            this.providerGroupBindingSource.DataSource = this.mDataSet;
            // 
            // gridViewProvider
            // 
            this.gridViewProvider.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewProvider.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewProvider.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Provider_Group,
            this.colProvider_Group,
            this.colID_Company,
            this.colCode_Group});
            this.gridViewProvider.GridControl = this.gridControl2;
            this.gridViewProvider.Name = "gridViewProvider";
            this.gridViewProvider.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewProvider.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewProvider.OptionsView.ShowGroupPanel = false;
            this.gridViewProvider.OptionsView.ShowIndicator = false;
            this.gridViewProvider.OptionsView.ShowViewCaption = true;
            this.gridViewProvider.ViewCaption = "ВСЕ КРОМЕ";
            // 
            // colID_Provider_Group
            // 
            this.colID_Provider_Group.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Provider_Group.AppearanceCell.Options.UseBackColor = true;
            this.colID_Provider_Group.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Provider_Group.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID_Provider_Group.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID_Provider_Group.FieldName = "ID_Provider_Group";
            this.colID_Provider_Group.Name = "colID_Provider_Group";
            this.colID_Provider_Group.OptionsColumn.AllowEdit = false;
            this.colID_Provider_Group.OptionsColumn.ReadOnly = true;
            this.colID_Provider_Group.Visible = true;
            this.colID_Provider_Group.VisibleIndex = 0;
            this.colID_Provider_Group.Width = 131;
            // 
            // colProvider_Group
            // 
            this.colProvider_Group.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colProvider_Group.AppearanceCell.Options.UseBackColor = true;
            this.colProvider_Group.AppearanceHeader.Options.UseTextOptions = true;
            this.colProvider_Group.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProvider_Group.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProvider_Group.Caption = "Группа поставщиков";
            this.colProvider_Group.FieldName = "Provider_Group";
            this.colProvider_Group.Name = "colProvider_Group";
            this.colProvider_Group.OptionsColumn.AllowEdit = false;
            this.colProvider_Group.OptionsColumn.ReadOnly = true;
            this.colProvider_Group.Visible = true;
            this.colProvider_Group.VisibleIndex = 1;
            this.colProvider_Group.Width = 336;
            // 
            // colID_Company
            // 
            this.colID_Company.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Company.AppearanceCell.Options.UseBackColor = true;
            this.colID_Company.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Company.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID_Company.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID_Company.FieldName = "ID_Company";
            this.colID_Company.Name = "colID_Company";
            this.colID_Company.OptionsColumn.AllowEdit = false;
            this.colID_Company.OptionsColumn.ReadOnly = true;
            // 
            // colCode_Group
            // 
            this.colCode_Group.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colCode_Group.AppearanceCell.Options.UseBackColor = true;
            this.colCode_Group.AppearanceHeader.Options.UseTextOptions = true;
            this.colCode_Group.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode_Group.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCode_Group.FieldName = "Code_Group";
            this.colCode_Group.Name = "colCode_Group";
            this.colCode_Group.OptionsColumn.AllowEdit = false;
            this.colCode_Group.OptionsColumn.ReadOnly = true;
            // 
            // FrmBillFilterProvider
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 650);
            this.Controls.Add(this.splitContainerControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmBillFilterProvider";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройка поставщиков по коду фильтра";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            this.cmdBillFilterDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.billFilterDetailProviderFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillFilterProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riProviderGroupFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerGroupFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            this.cmsProviderGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.providerGroupBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBillFilterProvider;
        private System.Windows.Forms.BindingSource billFilterDetailProviderFilterBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Bill_Filter;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Service;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Provider_Group_Filter;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceName;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProvider;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbConditionIn;
        private System.Windows.Forms.RadioButton rbConditionNotIn;
        private System.Windows.Forms.BindingSource providerGroupBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Provider_Group;
        private DevExpress.XtraGrid.Columns.GridColumn colProvider_Group;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colCode_Group;
        private System.Windows.Forms.ContextMenuStrip cmsProviderGroup;
        private System.Windows.Forms.ToolStripMenuItem miProviderGroupAdd;
        private System.Windows.Forms.ToolStripMenuItem miProviderGroupRemove;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TextBox txtCondition;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolStripMenuItem miProviderGroupRemoveAll;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riProviderGroupFilter;
        private System.Windows.Forms.BindingSource providerGroupFilterBindingSource;
        private System.Windows.Forms.ContextMenuStrip cmdBillFilterDetail;
        private System.Windows.Forms.ToolStripMenuItem miBillFilterDetail_Refresh;
        private System.Windows.Forms.ToolStripMenuItem miBillFilterDetail_DeleteProviderFilter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}