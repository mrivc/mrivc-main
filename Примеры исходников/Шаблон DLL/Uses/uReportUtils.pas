Unit uReportUtils;

Interface

Uses ADODB, Classes, Controls, ImgList, Variants;

Function GetTmpFileName(APath: String; AFileName: String; AExtension: String): String;
Function OpenFile(AHandle: Integer; AFileName: String): Boolean;
Function OpenExcelFile(AFileName: String): Boolean;
Function CaptionByDescription(AHInstance: Cardinal): String;
Procedure AssignDataSetConnections(AParent: TComponent; AConnection: TADOConnection; OpenIfTag1: Boolean = True);
Procedure AddWithDelim(Var Str: String; SubStr: String; Delim: String = ',');
Function Add(Str1, Str2, Delim: String): String;
Function BuildSelectQuery(SELECT: String; FROM: String; WHERE: String = ''; GROUP: String = '';
  ORDER: String = ''): String;

Function RoundTo(Const aValue: Variant; Const ADigit: Integer): Variant;

Function IsSummableUnit(aID_Unit: Variant): Boolean;

Type
  TErrorTester = Class(TObject)
  Private
    FMessage: String;
    FIsError: Boolean;
    FDelimiter: String;
  Public
    Constructor Create; Overload;
    Constructor Create(ACondition: Boolean; AMessage: String); Overload;
    Procedure Add(ACondition: Boolean; AMessage: String);
    Function Test: Boolean;
    Function GetMessage: String;
    Function ShowMessage: Boolean;
  End;

  TStringObj = Class(TObject)
  Public
    Text: String;
    Constructor Create(S: String);
  End;

  TCounter = Class(TObject)
  Private
    FCount: Cardinal;
    Function GetCount: Cardinal;
  Public
    Property Count: Cardinal Read GetCount;
    Constructor Create(BeginRandom: Boolean = False);
    Procedure SetZero;
  End;

  TNameType = (ntFull, ntShort, ntInCase);

Function MonthName(AMonthNumber: Byte; ANameType: TNameType = ntFull): String;

Function DateString(ADate: TDate; ANameType: TNameType = ntFull): String;

Const
  NewLine = #13#10;
  SELECT_Provider_Group = 'SELECT DISTINCT ID_Provider_Group, Provider_Group FROM dbo.VProvider ORDER BY Provider_Group';
  SELECT_Service = 'SELECT ID_Service, [Name] FROM dbo.[Service] ORDER BY Position';
  SELECT_Reports_Service = 'SELECT ID_Service, [Name] FROM Reports.VService ORDER BY Position';
  SELECT_Company = 'SELECT DISTINCT ID_Company, Company FROM VProvider ORDER BY Company';
  SELECT_Company_Test = 'SELECT DISTINCT ID_Company, ''('' + CAST(ID_Company AS VARCHAR)+ '') '' + Company AS Name FROM VProvider ORDER BY 1';
  SELECT_Derive_Company = 'SELECT DISTINCT ID_Company, Name FROM dbo.Company WHERE Is_Derive_Company = 1 ORDER BY 2';
  SELECT_Pack_Type = 'SELECT ID_Pack_Type, Pack_Type FROM dbo.Pack_Type';
  SELECT_Numeric_Tariff =
    'SELECT Main_Formula, [Name] + '' ('' +Main_Formula + '')'' FROM dbo.VTariff'#13#10 +
    'WHERE ISNUMERIC(Main_Formula) = 1'#13#10 +
    '  AND ID_Service = %s'#13#10 +
    '  AND Period = %s'#13#10 +
    'ORDER BY CAST(Main_Formula AS Float) ASC';

  SELECT_Pay_Receiver = 'SELECT ID_Company, RTRIM([Name]) AS [Name] FROM dbo.Company WHERE IS_Pay_Receiver = 1';

Resourcestring
  cFormingReport = '������������ ������';
  cExtractData = '���������� ������';
  cExtractInfoFields = '���������� �������������� �����';
  cExtractAccountParam = '���������� ����� ���������� ��...';
  cExtractFlatParam = '���������� ����� ���������� �������...';
  cExtractHouseParam = '���������� ����� ���������� �����...';
  cExtractServiceFields = '���������� ����� �����...';
  cExtractTotalFields = '���������� �������� �����...';
  cApplyFilter = '���������� �������';
  cUnknownReport = '����������� ��� ������';
  cReportException = '������ ��� ������������ ������';
  cNoServicesSelected = '���������� ������� ������';
  cInccorectDates = '������������ �������� ���';
Implementation

Uses DB, StrUtils, ShellAPI, Registry, Windows, SysUtils, Dialogs, Math, Printers;

Var
  InternalXLSOpenCmdRegString: String;

Const
  MonthNameArray: Packed Array[TNameType, 1..12] Of String =
  (('������', '�������', '����', '������', '���', '����', '����', '������',
    '��������', '�������', '������', '�������'),
    ('���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���',
    '���'),
    ('������', '�������', '�����', '������', '���', '����', '����', '�������',
    '��������', '�������', '������', '�������'));

Function GetDllFileName(AHInstance: Cardinal): String;
Begin
  SetLength(Result, MAX_PATH);
  GetModuleFileName(HInstance, PChar(Result), MAX_PATH);
End;

Function GetFileStringInfo(FileName, InfoStr: String): String;
Var
  P: Pointer;
  Value: Pointer;
  Len: UINT;
  GetTranslationString: String;
  Size: DWORD;
  Handle: DWORD;
  Buffer: PChar;
Begin
  Buffer := Nil;
  Size := 0;
  Try
    Size := GetFileVersionInfoSize(PChar(FileName), Handle);
    If Size > 0 Then
      Try
        GetMem(Buffer, Size);
        If GetFileVersionInfo(PChar(FileName), Handle, Size, Buffer) Then
          Begin
            If VerQueryValue(Buffer, '\VarFileInfo\Translation', P, Len) Then
              Begin
                GetTranslationString := IntToHex(MakeLong(HiWord(Longint(P^)), LoWord(Longint(P^))),
                  8);
                If VerQueryValue(
                  Buffer, PChar('\StringFileInfo\' + GetTranslationString + '\' + InfoStr), Value,
                  Len) Then
                  Result := StrPas(PChar(Value));
              End;
          End;
      Except
        Result := '';
        Raise;
      End;
  Finally
    Try
      If Buffer <> Nil Then
        FreeMem(Buffer, Size);
    Except
    End;
  End;
End;

Function GetTmpFileName(APath: String; AFileName: String; AExtension: String): String;
Var
  I: Integer;
Begin
  Result := '';
  APath := IncludeTrailingPathDelimiter(APath);
  Result := APath + 'TMP';
  If Not DirectoryExists(Result) Then
    CreateDir(Result);
  Result := Result + '\' + AFileName;
  I := 1;
  While FileExists(Result + IfThen(I = 1, '', IntToStr(I)) + '.' + AExtension) And
    (Not DeleteFile(Result + IfThen(I = 1, '', IntToStr(I)) + '.' + AExtension)) Do
    Inc(I);
  Result := Result + IfThen(I = 1, '', IntToStr(I)) + '.' + AExtension;
End;

Function OpenFile(AHandle: Integer; AFileName: String): Boolean;
Begin
  Result := (ShellExecute(AHandle, 'open', PChar(AFileName), Nil, Nil, SW_SHOWNORMAL) > 32);
End;

Function OpenExcelFile(AFileName: String): Boolean;
Var
  Reg: TRegistry;
  XLSFileType: String;
  OpenFileCmd: String;
Begin
  If InternalXLSOpenCmdRegString = '' Then
    Begin
      Reg := TRegistry.Create(KEY_READ);
      Reg.RootKey := HKEY_CLASSES_ROOT;
      Reg.OpenKey('\.xls', False);
      XLSFileType := Reg.ReadString('');
      Reg.OpenKey('\' + XLSFileType + '\shell\open\command', False);
      InternalXLSOpenCmdRegString := Reg.ReadString('');
      Reg.Free;
      InternalXLSOpenCmdRegString := StringReplace(InternalXLSOpenCmdRegString, ' /e', ' "%1"', []);
    End;
  OpenFileCmd := StringReplace(InternalXLSOpenCmdRegString, '%1', AFileName, []);
  Result := (WinExec(PChar(OpenFileCmd), SW_SHOWNORMAL) > 31)
End;

Function CaptionByDescription(AHInstance: Cardinal): String;
Var
  FileName: String;
Begin
  FileName := GetDllFileName(AHInstance);
  Result := GetFileStringInfo(FileName, 'FileDescription') + ' (' + GetFileStringInfo(FileName, 'FileVersion') + ')';
End;

Procedure AssignDataSetConnections(AParent: TComponent; AConnection: TADOConnection; OpenIfTag1:
  Boolean = True);
Var
  C: TComponent;
Begin
  For C In AParent Do
    Begin
      If C Is TCustomADODataSet Then
        Begin
          TCustomADODataSet(C).Connection := AConnection;
          If OpenIfTag1 And (TCustomADODataSet(C).Tag = 1) Then
            Begin
              TCustomADODataSet(C).Active := True;
            End;
        End;
      If C Is TADOCommand Then
        Begin
          TADOCommand(C).Connection := AConnection;
        End;
    End;
End;

Procedure AddWithDelim(Var Str: String; SubStr: String; Delim: String = ',');
Begin
  Str := IfThen(Str = '', SubStr, Str + Delim + SubStr);
End;

{ TErrorTester }

Procedure TErrorTester.Add(ACondition: Boolean; AMessage: String);
Begin
  FIsError := FIsError Or ACondition;
  If ACondition Then
    AddWithDelim(FMessage, AMessage, FDelimiter);
End;

Constructor TErrorTester.Create;
Begin
  FMessage := '';
  FIsError := False;
  FDelimiter := ',' + NewLine;
End;

Constructor TErrorTester.Create(ACondition: Boolean; AMessage: String);
Begin
  Create;
  Add(ACondition, AMessage);
End;

Function TErrorTester.GetMessage: String;
Begin
  Result := FMessage;
End;

Function TErrorTester.Test: Boolean;
Begin
  Result := FIsError;
End;

Function TErrorTester.ShowMessage: Boolean;
Begin
  Result := Test;
  If Result Then
    Dialogs.ShowMessage(FMessage);
End;

{ TStringObj }

Constructor TStringObj.Create(S: String);
Begin
  Inherited Create;
  Text := S;
End;

{ TCounter }

Constructor TCounter.Create(BeginRandom: Boolean);
Begin
  If BeginRandom Then
    FCount := GetTickCount
  Else
    FCount := 0;
End;

Function TCounter.GetCount: Cardinal;
Begin
  Result := FCount;
  If FCount < High(FCount) Then
    Inc(FCount)
  Else
    FCount := 0;
End;

Procedure TCounter.SetZero;
Begin
  FCount := 0;
End;

Function Add(Str1, Str2, Delim: String): String;
Begin
  If Str1 = '' Then
    Result := Str2
  Else If Str2 = '' Then
    Result := Str1
  Else
    Result := Str1 + Delim + Str2;
End;

Function BuildSelectQuery;
Begin
  Result := 'SELECT' + NewLine + SELECT + NewLine + 'FROM' + NewLine + FROM;
  Result := Add(Result, WHERE, NewLine + 'WHERE' + NewLine);
  Result := Add(Result, GROUP, NewLine + 'GROUP BY' + NewLine);
  Result := Add(Result, ORDER, NewLine + 'ORDER BY' + NewLine);
End;

Function RoundTo;
Begin
  If VarIsNumeric(aValue) And Not VarIsNull(aValue) Then
    Begin
      Result := Math.RoundTo(aValue, ADigit)
    End
  Else
    Begin
      Result := aValue;
    End;
End;

Function IsSummableUnit(aID_Unit: Variant): Boolean;
Begin
  If VarIsNull(aID_Unit) Then
    Result := False
  Else If Integer(aID_Unit) In [5, 8] Then
    Result := False
  Else
    Result := True;
End;

Function MonthName(AMonthNumber: Byte; ANameType: TNameType = ntFull): String;
Begin
  Result := IfThen((AMonthNumber >= 1) And (AMonthNumber <= 12), MonthNameArray[ANameType, AMonthNumber]);
End;

Function DateString(ADate: TDate; ANameType: TNameType = ntFull): String;
Var
  D, M, Y: Word;
Begin
  DecodeDate(ADate, Y, M, D);
  Result := IntToStr(D) + IfThen(ANameType = ntInCase, '-� ', ' ') + MonthName(M, ANameType) + ' ' + IntToStr(Y);
End;

Begin
  InternalXLSOpenCmdRegString := '';
End.

