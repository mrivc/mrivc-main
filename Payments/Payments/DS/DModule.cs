﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using Payments.LogicClasses;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace Payments.DS
{
    public partial class DModule : Component
    {
        bool connectedPayments = false;
        bool connectedEllis = false;


        public DModule()
        {
            InitializeComponent();
        }

        public DModule(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        SqlConnection connection1;
        public SqlConnection Connection1
        {
            get
            {
                return connection1;
            }
            set
            {
                connection1 = value;
                connection1.Open();
            }
        }

        void CheckConnection(SqlConnection connection)
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();
        }

        public DataTable GetDataTableByStoredProcedure1(string sql, object[,] prms)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;
            sqlCommand.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < prms.GetLength(0); i++)
                sqlCommand.Parameters.AddWithValue(prms[i, 0].ToString(), prms[i, 1]);

            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);


            res.Load(sqlCommand.ExecuteReader());

            sqlCommand.Dispose();

            return res;
        }

        public DataTable GetDataTableByStoredProcedure1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;
            sqlCommand.CommandType = CommandType.StoredProcedure;


            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);

            res.Load(sqlCommand.ExecuteReader());

            sqlCommand.Dispose();

            return res;
        }

        public DataTable GetDataTableByQuery1(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;

            var res = new DataTable();
            CheckConnection(sqlCommand.Connection);

            res.Load(sqlCommand.ExecuteReader());

            sqlCommand.Dispose();

            return res;
        }

        public object ExecuteScalar(string sql)
        {
            var sqlCommand = new SqlCommand(sql, Connection1);
            sqlCommand.CommandTimeout = 0;

            CheckConnection(sqlCommand.Connection);

            var res = sqlCommand.ExecuteScalar();

            sqlCommand.Dispose();

            return res;
        }

        #region Database connection settings
        public string ConnectionStringToPayment;
        public bool SetPaymentConnection()
        {
            try
            {
                if (ConnectionStringToPayment.Trim().Length == 0)
                    throw new Exception("Пустая строка соединения к БД Payments. Variable DAccess.DataModule.ConnectionStringToPayment is empty/");

                sqlPaymentConnection.ConnectionString = ConnectionStringToPayment;
                sqlPaymentConnection.Open();
                connectedPayments = true;
            }
            catch (Exception ex)
            {
                connectedPayments = false;
                throw ex;
            }
            finally
            { sqlPaymentConnection.Close(); }
            return true;
        }

        public string ConnectionStringToEllis;
        public bool SetEllisConnection()
        {
            try
            {
                if (ConnectionStringToEllis.Trim().Length == 0)
                    throw new Exception("Пустая строка соединения к БД ЭЛЛИС. Variable DAccess.DataModule.ConnectionStringToEllis is empty.");

                sqlEllisConnection.ConnectionString = ConnectionStringToEllis;
                sqlEllisConnection.Open();
                connectedEllis = true;
            }
            catch (Exception ex)
            {
                connectedEllis = false;
                throw ex;
            }
            finally
            { sqlEllisConnection.Close(); }
            return true;
        }

        public bool ConnectedPayments
        {
            get { return connectedPayments; }
        }

        public bool ConnectedEllis
        {
            get { return connectedEllis; }
        }

        public string ConnectionPaymentsDBName
        {
            get { return sqlPaymentConnection.Database; }
        }

        public string ConnectionPaymentsServerName
        {
            get { return sqlPaymentConnection.DataSource; }
        }

        public string ConnectionEllisDBName
        {
            get { return sqlEllisConnection.Database; }
        }

        public string ConnectionEllisServerName
        {
            get { return sqlEllisConnection.DataSource; }
        }
        #endregion

        public void PaymentSelect(PaymentsDataSet.PaymentDataTable table, int idPaymentClass, DateTime paymentBeginDate, DateTime paymentEndDate) {
            //daPayment.SelectCommand.Connection.Close();
            //daPayment.SelectCommand.Connection.Open();
            daPayment.SelectCommand.Parameters["@Payment_Begin_Date"].Value = paymentBeginDate;
            daPayment.SelectCommand.Parameters["@Payment_End_Date"].Value = paymentEndDate;
            daPayment.SelectCommand.Parameters["@ID_Payment_Class"].Value = idPaymentClass;
            daPayment.SelectCommand.Parameters["@ID_User"].Value = DAccess.Instance.CurrentUser.InvariantUserId;
            daPayment.Fill(table);
        }

        public int PaymentUpdate(PaymentsDataSet.PaymentDataTable table)
        {
            daPayment.Update(table);
            if (daPayment.InsertCommand.Parameters["@ID_Payment"].Value != null)
                return (int)daPayment.InsertCommand.Parameters["@ID_Payment"].Value;
            return -1;
        }

        public void PaymentClassSelect(PaymentsDataSet.Payment_ClassDataTable table)
        {
            daPaymentClass.Fill(table);
        }

        public void PaymentEllisReestrSelect(PaymentsDataSet.VPaymentEllisReestFullDataTable table, DateTime paymentBeginDate, DateTime paymentEndDate)
        {
            daPaymentEllisReestr.SelectCommand.Connection.Close();
            daPaymentEllisReestr.SelectCommand.Parameters["@Payment_Begin_Date"].Value = paymentBeginDate;
            daPaymentEllisReestr.SelectCommand.Parameters["@Payment_End_Date"].Value = paymentEndDate;
            daPaymentEllisReestr.SelectCommand.Parameters["@ID_User"].Value = DAccess.Instance.CurrentUser.InvariantUserId;
            daPaymentEllisReestr.Fill(table);
        }

        public void PaymentLoadedStatusUpdate(PaymentsDataSet.TxtReestrDataTable table)
        {
            daPaymentEllisReestr.SelectCommand.Connection.Close();
            daPaymentEllisReestr.Update(table);
        }

        public void PaymentLoadedStatusUpdate(PaymentsDataSet.VPaymentEllisReestFullDataTable table)
        {
            daPaymentEllisReestr.SelectCommand.Connection.Close();
            daPaymentEllisReestr.Update(table);
        }

        public void PaymentTxtReestrSelect(PaymentsDataSet.TxtReestrDataTable table, DateTime paymentBeginDate, DateTime paymentEndDate)
        {
            daPaymentTxtReestr.SelectCommand.Connection.Close();
            daPaymentTxtReestr.SelectCommand.Parameters["@Payment_Begin_Date"].Value = paymentBeginDate;
            daPaymentTxtReestr.SelectCommand.Parameters["@Payment_End_Date"].Value = paymentEndDate;
            daPaymentTxtReestr.SelectCommand.Parameters["@ID_User"].Value = DAccess.Instance.CurrentUser.InvariantUserId;
            daPaymentTxtReestr.Fill(table);
        }

        public void PaymentParameterDetailSelect(PaymentsDataSet.PaymentParameterDetailDataTable table, PaymentClass paymentClass)
        {
            daPaymentParameterDetail.SelectCommand.Connection.Close();
            daPaymentParameterDetail.SelectCommand.Parameters["@CreatedBy"].Value = DAccess.Instance.CurrentUser.Id_User;
            daPaymentParameterDetail.SelectCommand.Parameters["@ID_Payment"].Value = paymentClass.IdPayment;
            daPaymentParameterDetail.SelectCommand.Parameters["@ID_Payment_Class"].Value = paymentClass.IdPaymentClass;
            daPaymentParameterDetail.Fill(table);
        }

        public void PaymentParameterDetailUpdate(PaymentsDataSet.PaymentParameterDetailDataTable table)
        {
            daPaymentParameterDetail.Update(table);
        }

        public void PersonIdentificationSelect(PaymentsDataSet.PersonIdentificationDataTable table, int id)
        {
            daPersonIdentification.SelectCommand.Connection.Close();
            daPersonIdentification.SelectCommand.Parameters["@ID"].Value = id;
            daPersonIdentification.Fill(table);
        }

        public void PersonIdentificationSearch(PaymentsDataSet.PersonIdentificationDataTable table, string lastName)
        {
            daPersonIdentificationSearch.SelectCommand.Connection.Close();
            daPersonIdentificationSearch.SelectCommand.Parameters["@LastName"].Value = lastName;
            daPersonIdentificationSearch.Fill(table);
        }

        public void PersonIdentificationUpdate(PaymentsDataSet.PersonIdentificationDataTable table)
        {
            daPersonIdentification.Update(table);
        }

        public void UserSelect(PaymentsDataSet.UserDataTable table)
        {
            daUser.Fill(table);
        }

        public void UserUpdate(PaymentsDataSet.UserDataTable table)
        {
            daUser.Update(table);
        }

        public string GetProgramSettingsByCode(string code)
        {
            sqlPaymentConnection.Close();
            sqlPaymentConnection.Open();
            cmdProgramSettingsSelectByCode.Parameters["@Code"].Value = code;
            return cmdProgramSettingsSelectByCode.ExecuteScalar().ToString();
        }

        public void ClearDb()
        {
            sqlPaymentConnection.Close();
            sqlPaymentConnection.Open();
            cmdClearDb.ExecuteNonQuery();
        }

        public void RegistryVerificationSetLoadedStatus(int idRegistryVerification, bool loaded)
        {
            cmdRegistryVerificationSetLoadedStatus.Connection.Close();
            cmdRegistryVerificationSetLoadedStatus.Parameters["@ID_Registry_Verification"].Value = idRegistryVerification;
            cmdRegistryVerificationSetLoadedStatus.Parameters["@Registry_Loaded"].Value = loaded;
            cmdRegistryVerificationSetLoadedStatus.Connection.Open();
            cmdRegistryVerificationSetLoadedStatus.ExecuteNonQuery();
        }

        public int RegistryVerificationCreate(int idClient, string fileName, string emailAddress, string emailSubject, string emailDate)
        {
            cmdRegistryVerification.Connection.Close();
            cmdRegistryVerification.Parameters["@ID_Client"].Value = idClient;
            cmdRegistryVerification.Parameters["@FileName"].Value = fileName;
            cmdRegistryVerification.Parameters["@EmailAddress"].Value = emailAddress;
            cmdRegistryVerification.Parameters["@EmailSubject"].Value = emailSubject;
            cmdRegistryVerification.Parameters["@EmailDate"].Value = emailDate;
            cmdRegistryVerification.Connection.Open();
            cmdRegistryVerification.ExecuteNonQuery();
            return (int) cmdRegistryVerification.Parameters["@ID_Registry_Verification"].Value;
        }

        public void RegistryVerificationLineInsert(int idRegistryVerification, int idClient, string address, string account, decimal summPay, 
            decimal smmFine, string barcode, string comment)
        {
            cmdRegistryVerificationLineInsert.Connection.Close();
            cmdRegistryVerificationLineInsert.Parameters["@ID_Registry_Verification"].Value = idRegistryVerification;
            cmdRegistryVerificationLineInsert.Parameters["@ID_Client"].Value = idClient;
            cmdRegistryVerificationLineInsert.Parameters["@Address"].Value = address;
            cmdRegistryVerificationLineInsert.Parameters["@Account"].Value = account;
            cmdRegistryVerificationLineInsert.Parameters["@SummPay"].Value = summPay;
            cmdRegistryVerificationLineInsert.Parameters["@SummFine"].Value = smmFine;
            cmdRegistryVerificationLineInsert.Parameters["@Barcode"].Value = barcode;
            cmdRegistryVerificationLineInsert.Parameters["@Comment"].Value = comment;
            cmdRegistryVerificationLineInsert.Connection.Open();
            cmdRegistryVerificationLineInsert.ExecuteNonQuery();
        }

        public void CashTypeSelect(PaymentsDataSet.Cash_TypeDataTable table)
        {
            daCashType.Fill(table);
        }

        public DataTable GetPaymentEllisInfo(string authcod)
        {
            DataTable table = new DataTable();
            daPaymentInfo.SelectCommand.Parameters["@Authcod"].Value = authcod;
            daPaymentInfo.Fill(table);
            return table;
        }

        public DataTable GetPaymentVerificationInfo(string barcode)
        {
            DataTable table = new DataTable();
            daPaymentVerification.SelectCommand.Parameters["@Barcode"].Value = barcode;
            daPaymentVerification.Fill(table);
            return table;
        }

        public void UnloadToDbf()
        {
            string constr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\db1.mdb";
            OleDbConnection conn = new OleDbConnection(constr);

        }

        public DataTable FilterClientSelect()
        {
            DataTable table = new DataTable();
            daFilter_Client.Fill(table);
            return table;
        }
    }
}
