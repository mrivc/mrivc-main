﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MtecExcelConverter
{
    public enum SummType
    {
        None = -1,
        Balance = 0,
        Tariff = 1,
        Bill = 2
    }

    public class CellDataItem
    {
        public string FileName { get; set; }
        public object Account { get; set; }
        public object Address { get; set; }
        public object SumType { get; set; }
        public object Period { get; set; }
        public object SumHeating { get; set; }
        public object SumHotWaterMop { get; set; }
        public object SumHotWater { get; set; }
        public object SumSummary { get; set; }

        public static DateTime LastValidPeriod;

        private string accountValue = string.Empty;
        public string AccountValue
        {
            get
            {
                if (!string.IsNullOrEmpty(accountValue)) return accountValue;
                string token = "Лицевой счет абонента: ";

                if (Account == null) return string.Empty;

                if (Account.ToString().IndexOf(token) > -1)
                {
                    this.accountValue = Account.ToString().Substring(token.Length, 12);
                }

                return accountValue;
            }
        }

        private string addressValue = string.Empty;
        public string AddressValue
        {
            get
            {
                if (!string.IsNullOrEmpty(addressValue)) return addressValue;

                if (Address == null) return string.Empty;

                string[] val = this.Address.ToString().Split('\n');
                if (val.Length > 0)
                {
                    this.addressValue = val[0].Replace("Адрес: ", string.Empty);
                }

                return this.addressValue;
            }
        }

        private string ownerValue = string.Empty;
        public string OwnerValue
        {
            get
            {
                if (!string.IsNullOrEmpty(ownerValue)) return ownerValue;

                if (Account == null) return string.Empty;

                string[] val = this.Account.ToString().Split('\n');
                if (val.Length > 0)
                {
                    this.ownerValue = val[1].Replace("Владелец лицевого счета: ", string.Empty);
                }

                return this.ownerValue;
            }
        }

        private DateTime periodValue = DateTime.MaxValue;
        public DateTime PeriodValue
        {
            get
            {
                if (periodValue != DateTime.MaxValue) return periodValue;

                if (this.SumType == null) return DateTime.MaxValue;

                string token = "Сальдо на ";
                if (this.SumType.ToString().Contains(token))
                {
                    this.periodValue = Convert.ToDateTime(this.SumType.ToString().Replace(token, string.Empty));
                    CellDataItem.LastValidPeriod = this.periodValue;
                }
                else if (this.Period != null)
                {
                    this.periodValue = this.GetPeriodDate(this.Period.ToString());
                    CellDataItem.LastValidPeriod = this.periodValue;
                }
                else
                {
                    this.periodValue = CellDataItem.LastValidPeriod;
                }

                return this.periodValue;
            }
        }

        private SummType sumTypeValue = SummType.None;
        public SummType SumTypeValue
        {
            get
            {
                if (sumTypeValue != SummType.None) return sumTypeValue;

                if (this.SumType == null) return SummType.None;

                string sumType = this.SumType.ToString();

                if (sumType.Contains("Сальдо на")) this.sumTypeValue = SummType.Balance;
                if (sumType.Contains("начислено")) this.sumTypeValue = SummType.Tariff;
                if (sumType.Contains("оплачено")) this.sumTypeValue = SummType.Bill;
                if (sumType.Contains("Итого")) this.sumTypeValue = SummType.None;

                return this.sumTypeValue;
            }
        }

        /// <summary>
        /// Отопление РСО
        /// </summary>
        public decimal SumHeatingValue
        {
            get
            {
                if (this.SumHeating == null) return 0;

                return Convert.ToDecimal(this.SumHeating.ToString().Replace(".", ","));
            }
        }

        /// <summary>
        /// Подогрев воды МОП РСО
        /// </summary>
        public decimal SumHotWaterMopValue
        {
            get
            {
                if (this.SumHotWaterMop == null) return 0;

                return Convert.ToDecimal(this.SumHotWaterMop.ToString().Replace(".", ","));
            }
        }

        /// <summary>
        /// Подогрев воды РСО
        /// </summary>
        public decimal SumHotWaterValue
        {
            get
            {
                if (this.SumHotWater == null) return 0;

                return Convert.ToDecimal(this.SumHotWater.ToString().Replace(".", ","));
            }
        }

        public decimal SumSummaryValue
        {
            get
            {
                return 0;
            }
        }

        private DateTime GetPeriodDate(string value)
        {
            string[] str = value.Split(' ');
            int month = 0;

            if (str[0].Equals("Январь", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 1;
            }
            else if (str[0].Equals("Февраль", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 2;
            }
            else if (str[0].Equals("Март", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 3;
            }
            else if (str[0].Equals("Апрель", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 4;
            }
            else if (str[0].Equals("Май", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 5;
            }
            else if (str[0].Equals("Июнь", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 6;
            }
            else if (str[0].Equals("Июль", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 7;
            }
            else if (str[0].Equals("Август", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 8;
            }
            else if (str[0].Equals("Сентябрь", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 9;
            }
            else if (str[0].Equals("Октябрь", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 10;
            }
            else if (str[0].Equals("Ноябрь", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 11;
            }
            else if (str[0].Equals("Декабрь", StringComparison.InvariantCultureIgnoreCase))
            {
                month = 12;
            }

            int year = Convert.ToInt32(str[1].Trim());

            return new DateTime(year, month, 1);
        }
    }
}
