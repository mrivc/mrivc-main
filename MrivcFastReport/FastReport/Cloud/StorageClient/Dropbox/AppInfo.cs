using System;
using System.Collections.Generic;
using System.Text;

namespace FastReport.Cloud.StorageClient.Dropbox
{
    /// <summary>
    /// Represents the information about Dropbox application.
    /// </summary>
    public class AppInfo
    {
        #region Fields

        private string name;
        private string key;
        private string secret;
        private string accessType;
        private string folder;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Gets or sets the app name.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Gets or sets the app key.
        /// </summary>
        public string Key
        {
            get { return key; }
            set { key = value; }
        }

        /// <summary>
        /// Gets or sets the app secret.
        /// </summary>
        public string Secret
        {
            get { return secret; }
            set { secret = value; }
        }

        /// <summary>
        /// Gets or sets the access type.
        /// </summary>
        public string AccessType
        {
            get { return accessType; }
            set { accessType = value; }
        }

        /// <summary>
        /// Gets or sets the name of app folder.
        /// </summary>
        public string Folder
        {
            get { return folder; }
            set { folder = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cret="AppInfo"/> class with a specified properties.
        /// </summary>
        /// <param name="name">The app name.</param>
        /// <param name="key">The app key.</param>
        /// <param name="secret">The app secret.</param>
        /// <param name="folder">The name of app folder.</param>
        public AppInfo(string name, string key, string secret, string folder)
        {
            this.name = name;
            this.key = key;
            this.secret = secret;
            this.folder = folder;
            this.accessType = Dropbox.AccessType.Sandbox;
        }

        #endregion // Constructors
    }

    /// <summary>
    /// Represents the access type of application.
    /// </summary>
    public static class AccessType
    {
        #region Constants

        /// <summary>
        /// Full access to all the files and folders in a user's Dropbox.
        /// </summary>
        public const string Dropbox = "dropbox";

        /// <summary>
        /// Access to app folder only (sandbox).
        /// </summary>
        public const string Sandbox = "sandbox";

        #endregion // Constants
    }
}
