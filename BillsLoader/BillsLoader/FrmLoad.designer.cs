﻿namespace BillsLoader
{
    partial class FrmLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLoad));
            this.mDataSet = new BillsLoader.DM.MDataSet();
            this.packTypeBindingSource = new System.Windows.Forms.BindingSource();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnFilePath = new System.Windows.Forms.Button();
            this.dtpReestrDate = new System.Windows.Forms.DateTimePicker();
            this.cbPackType = new System.Windows.Forms.ComboBox();
            this.packType1BindingSource = new System.Windows.Forms.BindingSource();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtPackNumber = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer();
            this.lblStatus = new System.Windows.Forms.Label();
            this.cbSberReestrType = new System.Windows.Forms.ComboBox();
            this.sberSaldoReestrTypeBindingSource = new System.Windows.Forms.BindingSource();
            this.label1 = new System.Windows.Forms.Label();
            this.cbHv = new System.Windows.Forms.CheckBox();
            this.cbGv = new System.Windows.Forms.CheckBox();
            this.cbGaz = new System.Windows.Forms.CheckBox();
            this.cbEl = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnOpenErrorFile = new System.Windows.Forms.Button();
            this.rbReestrLoad = new System.Windows.Forms.RadioButton();
            this.rbExcelLoad = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.cbBillFilter = new System.Windows.Forms.ComboBox();
            this.billFilterBindingSource = new System.Windows.Forms.BindingSource();
            this.gbBillFilter = new System.Windows.Forms.GroupBox();
            this.gbReestrNumber = new System.Windows.Forms.GroupBox();
            this.tbReestrNumber = new System.Windows.Forms.TextBox();
            this.lbFilePath = new System.Windows.Forms.Label();
            this.mDataSetBindingSource = new System.Windows.Forms.BindingSource();
            this.pack_Type1TableAdapter = new BillsLoader.DM.MDataSetTableAdapters.Pack_Type1TableAdapter();
            this.chbPeni = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packType1BindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPackNumber)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sberSaldoReestrTypeBindingSource)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).BeginInit();
            this.gbBillFilter.SuspendLayout();
            this.gbReestrNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // packTypeBindingSource
            // 
            this.packTypeBindingSource.DataMember = "Pack_Type";
            this.packTypeBindingSource.DataSource = this.mDataSet;
            // 
            // txtFilePath
            // 
            this.txtFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilePath.Location = new System.Drawing.Point(58, 58);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(578, 20);
            this.txtFilePath.TabIndex = 1;
            // 
            // btnFilePath
            // 
            this.btnFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFilePath.Location = new System.Drawing.Point(642, 57);
            this.btnFilePath.Name = "btnFilePath";
            this.btnFilePath.Size = new System.Drawing.Size(35, 20);
            this.btnFilePath.TabIndex = 2;
            this.btnFilePath.Text = "...";
            this.btnFilePath.UseVisualStyleBackColor = true;
            this.btnFilePath.Click += new System.EventHandler(this.btnFilePath_Click);
            // 
            // dtpReestrDate
            // 
            this.dtpReestrDate.CustomFormat = "dd.mm.yyyy";
            this.dtpReestrDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpReestrDate.Location = new System.Drawing.Point(6, 19);
            this.dtpReestrDate.Name = "dtpReestrDate";
            this.dtpReestrDate.Size = new System.Drawing.Size(111, 20);
            this.dtpReestrDate.TabIndex = 3;
            // 
            // cbPackType
            // 
            this.cbPackType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPackType.DataSource = this.packType1BindingSource;
            this.cbPackType.DisplayMember = "Pack_Type";
            this.cbPackType.FormattingEnabled = true;
            this.cbPackType.Location = new System.Drawing.Point(7, 18);
            this.cbPackType.Name = "cbPackType";
            this.cbPackType.Size = new System.Drawing.Size(278, 21);
            this.cbPackType.TabIndex = 0;
            this.cbPackType.ValueMember = "ID_Pack_Type";
            // 
            // packType1BindingSource
            // 
            this.packType1BindingSource.DataMember = "Pack_Type1";
            this.packType1BindingSource.DataSource = this.mDataSet;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtpReestrDate);
            this.groupBox1.Location = new System.Drawing.Point(12, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(124, 49);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Дата пачки:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtPackNumber);
            this.groupBox2.Location = new System.Drawing.Point(142, 84);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(108, 49);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Номер пачки:";
            // 
            // txtPackNumber
            // 
            this.txtPackNumber.Location = new System.Drawing.Point(6, 19);
            this.txtPackNumber.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.txtPackNumber.Name = "txtPackNumber";
            this.txtPackNumber.Size = new System.Drawing.Size(91, 20);
            this.txtPackNumber.TabIndex = 8;
            this.txtPackNumber.Enter += new System.EventHandler(this.txtPackNumber_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.cbPackType);
            this.groupBox3.Location = new System.Drawing.Point(256, 84);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(294, 49);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Тип пачки:";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(12, 249);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(124, 23);
            this.btnLoad.TabIndex = 8;
            this.btnLoad.Text = "Загрузить реестр";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStatus.Location = new System.Drawing.Point(143, 254);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(35, 13);
            this.lblStatus.TabIndex = 8;
            this.lblStatus.Text = "label1";
            // 
            // cbSberReestrType
            // 
            this.cbSberReestrType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSberReestrType.DataSource = this.sberSaldoReestrTypeBindingSource;
            this.cbSberReestrType.DisplayMember = "Reestr_Type";
            this.cbSberReestrType.FormattingEnabled = true;
            this.cbSberReestrType.Location = new System.Drawing.Point(234, 251);
            this.cbSberReestrType.Name = "cbSberReestrType";
            this.cbSberReestrType.Size = new System.Drawing.Size(306, 21);
            this.cbSberReestrType.TabIndex = 7;
            this.cbSberReestrType.ValueMember = "ID_Client";
            this.cbSberReestrType.Visible = false;
            // 
            // sberSaldoReestrTypeBindingSource
            // 
            this.sberSaldoReestrTypeBindingSource.DataMember = "Sber_Saldo_Reestr_Type";
            this.sberSaldoReestrTypeBindingSource.DataSource = this.mDataSet;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(184, 254);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Клиент:";
            this.label1.Visible = false;
            // 
            // cbHv
            // 
            this.cbHv.AutoSize = true;
            this.cbHv.Checked = true;
            this.cbHv.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbHv.Location = new System.Drawing.Point(9, 21);
            this.cbHv.Name = "cbHv";
            this.cbHv.Size = new System.Drawing.Size(40, 17);
            this.cbHv.TabIndex = 10;
            this.cbHv.Text = "ХВ";
            this.cbHv.UseVisualStyleBackColor = true;
            // 
            // cbGv
            // 
            this.cbGv.AutoSize = true;
            this.cbGv.Checked = true;
            this.cbGv.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGv.Location = new System.Drawing.Point(55, 21);
            this.cbGv.Name = "cbGv";
            this.cbGv.Size = new System.Drawing.Size(39, 17);
            this.cbGv.TabIndex = 11;
            this.cbGv.Text = "ГВ";
            this.cbGv.UseVisualStyleBackColor = true;
            // 
            // cbGaz
            // 
            this.cbGaz.AutoSize = true;
            this.cbGaz.Checked = true;
            this.cbGaz.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGaz.Location = new System.Drawing.Point(100, 21);
            this.cbGaz.Name = "cbGaz";
            this.cbGaz.Size = new System.Drawing.Size(44, 17);
            this.cbGaz.TabIndex = 12;
            this.cbGaz.Text = "Газ";
            this.cbGaz.UseVisualStyleBackColor = true;
            // 
            // cbEl
            // 
            this.cbEl.AutoSize = true;
            this.cbEl.Checked = true;
            this.cbEl.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEl.Location = new System.Drawing.Point(150, 21);
            this.cbEl.Name = "cbEl";
            this.cbEl.Size = new System.Drawing.Size(56, 17);
            this.cbEl.TabIndex = 13;
            this.cbEl.Text = "Эл/эн";
            this.cbEl.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbHv);
            this.groupBox4.Controls.Add(this.cbEl);
            this.groupBox4.Controls.Add(this.cbGv);
            this.groupBox4.Controls.Add(this.cbGaz);
            this.groupBox4.Location = new System.Drawing.Point(12, 189);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(238, 47);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Загружать показания по (для DBF): ";
            // 
            // btnOpenErrorFile
            // 
            this.btnOpenErrorFile.Location = new System.Drawing.Point(547, 249);
            this.btnOpenErrorFile.Name = "btnOpenErrorFile";
            this.btnOpenErrorFile.Size = new System.Drawing.Size(130, 23);
            this.btnOpenErrorFile.TabIndex = 15;
            this.btnOpenErrorFile.Text = "Посмотреть ошибки";
            this.btnOpenErrorFile.UseVisualStyleBackColor = true;
            this.btnOpenErrorFile.Click += new System.EventHandler(this.btnOpenErrorFile_Click);
            // 
            // rbReestrLoad
            // 
            this.rbReestrLoad.AutoSize = true;
            this.rbReestrLoad.Checked = true;
            this.rbReestrLoad.Location = new System.Drawing.Point(16, 12);
            this.rbReestrLoad.Name = "rbReestrLoad";
            this.rbReestrLoad.Size = new System.Drawing.Size(174, 17);
            this.rbReestrLoad.TabIndex = 16;
            this.rbReestrLoad.TabStop = true;
            this.rbReestrLoad.Text = "Загрузка реестров платежей";
            this.rbReestrLoad.UseVisualStyleBackColor = true;
            this.rbReestrLoad.CheckedChanged += new System.EventHandler(this.rbReestrLoad_CheckedChanged);
            // 
            // rbExcelLoad
            // 
            this.rbExcelLoad.AutoSize = true;
            this.rbExcelLoad.Location = new System.Drawing.Point(245, 12);
            this.rbExcelLoad.Name = "rbExcelLoad";
            this.rbExcelLoad.Size = new System.Drawing.Size(295, 17);
            this.rbExcelLoad.TabIndex = 17;
            this.rbExcelLoad.Text = "Загрузка платежей по лицевому счету, дате и сумме";
            this.rbExcelLoad.UseVisualStyleBackColor = true;
            this.rbExcelLoad.CheckedChanged += new System.EventHandler(this.rbReestrLoad_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(263, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(285, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "из файла Excel со столбцами Account, SumPlat, DatPlat";
            // 
            // cbBillFilter
            // 
            this.cbBillFilter.DataSource = this.billFilterBindingSource;
            this.cbBillFilter.DisplayMember = "bill_filter";
            this.cbBillFilter.Enabled = false;
            this.cbBillFilter.FormattingEnabled = true;
            this.cbBillFilter.Location = new System.Drawing.Point(6, 17);
            this.cbBillFilter.Name = "cbBillFilter";
            this.cbBillFilter.Size = new System.Drawing.Size(278, 21);
            this.cbBillFilter.TabIndex = 19;
            this.cbBillFilter.ValueMember = "code";
            // 
            // billFilterBindingSource
            // 
            this.billFilterBindingSource.DataMember = "Bill_Filter";
            this.billFilterBindingSource.DataSource = this.mDataSet;
            // 
            // gbBillFilter
            // 
            this.gbBillFilter.Controls.Add(this.cbBillFilter);
            this.gbBillFilter.Location = new System.Drawing.Point(256, 189);
            this.gbBillFilter.Name = "gbBillFilter";
            this.gbBillFilter.Size = new System.Drawing.Size(294, 47);
            this.gbBillFilter.TabIndex = 20;
            this.gbBillFilter.TabStop = false;
            this.gbBillFilter.Text = "Код фильтра";
            // 
            // gbReestrNumber
            // 
            this.gbReestrNumber.Controls.Add(this.tbReestrNumber);
            this.gbReestrNumber.Location = new System.Drawing.Point(12, 135);
            this.gbReestrNumber.Name = "gbReestrNumber";
            this.gbReestrNumber.Size = new System.Drawing.Size(538, 48);
            this.gbReestrNumber.TabIndex = 21;
            this.gbReestrNumber.TabStop = false;
            this.gbReestrNumber.Text = "Номер реестра";
            // 
            // tbReestrNumber
            // 
            this.tbReestrNumber.Location = new System.Drawing.Point(9, 17);
            this.tbReestrNumber.Name = "tbReestrNumber";
            this.tbReestrNumber.Size = new System.Drawing.Size(519, 20);
            this.tbReestrNumber.TabIndex = 22;
            // 
            // lbFilePath
            // 
            this.lbFilePath.AutoSize = true;
            this.lbFilePath.Location = new System.Drawing.Point(13, 61);
            this.lbFilePath.Name = "lbFilePath";
            this.lbFilePath.Size = new System.Drawing.Size(39, 13);
            this.lbFilePath.TabIndex = 22;
            this.lbFilePath.Text = "Файл:";
            // 
            // mDataSetBindingSource
            // 
            this.mDataSetBindingSource.DataSource = this.mDataSet;
            this.mDataSetBindingSource.Position = 0;
            // 
            // pack_Type1TableAdapter
            // 
            this.pack_Type1TableAdapter.ClearBeforeFill = true;
            // 
            // chbPeni
            // 
            this.chbPeni.AutoSize = true;
            this.chbPeni.Location = new System.Drawing.Point(16, 32);
            this.chbPeni.Name = "chbPeni";
            this.chbPeni.Size = new System.Drawing.Size(182, 17);
            this.chbPeni.TabIndex = 23;
            this.chbPeni.Text = "Загружать только пени (ТЕСТ)";
            this.chbPeni.UseVisualStyleBackColor = true;
            // 
            // FrmLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 284);
            this.Controls.Add(this.chbPeni);
            this.Controls.Add(this.lbFilePath);
            this.Controls.Add(this.gbReestrNumber);
            this.Controls.Add(this.gbBillFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbExcelLoad);
            this.Controls.Add(this.rbReestrLoad);
            this.Controls.Add(this.btnOpenErrorFile);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbSberReestrType);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnFilePath);
            this.Controls.Add(this.txtFilePath);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmLoad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Загрузка реестров платежей";
            this.Load += new System.EventHandler(this.FrmLoad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packType1BindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPackNumber)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sberSaldoReestrTypeBindingSource)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).EndInit();
            this.gbBillFilter.ResumeLayout(false);
            this.gbReestrNumber.ResumeLayout(false);
            this.gbReestrNumber.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource packTypeBindingSource;
        private DM.MDataSet mDataSet;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btnFilePath;
        private System.Windows.Forms.DateTimePicker dtpReestrDate;
        private System.Windows.Forms.ComboBox cbPackType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.NumericUpDown txtPackNumber;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ComboBox cbSberReestrType;
        private System.Windows.Forms.BindingSource sberSaldoReestrTypeBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbHv;
        private System.Windows.Forms.CheckBox cbGv;
        private System.Windows.Forms.CheckBox cbGaz;
        private System.Windows.Forms.CheckBox cbEl;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnOpenErrorFile;
        private System.Windows.Forms.RadioButton rbReestrLoad;
        private System.Windows.Forms.RadioButton rbExcelLoad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbBillFilter;
        private System.Windows.Forms.GroupBox gbBillFilter;
        private System.Windows.Forms.BindingSource billFilterBindingSource;
        private System.Windows.Forms.GroupBox gbReestrNumber;
        private System.Windows.Forms.TextBox tbReestrNumber;
        private System.Windows.Forms.Label lbFilePath;
        private System.Windows.Forms.BindingSource mDataSetBindingSource;
        private System.Windows.Forms.BindingSource packType1BindingSource;
        private DM.MDataSetTableAdapters.Pack_Type1TableAdapter pack_Type1TableAdapter;
        private System.Windows.Forms.CheckBox chbPeni;
    }
}