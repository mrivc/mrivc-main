﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportDataProviderItem : MrivcExportCommand
    {
        public ExportDataProviderItem(string senderID) : base("exportDataProviderNsiItem", "NsiAsync", senderID, "NsiItem")
        {
        }

        public new void ExecuteToTempDB(string nsiRegistryNumber)
        {
            var values = (new Tuple<string[], string>[]
            {
                new Tuple<string[], string>(new[] { "RegistryNumber" }, nsiRegistryNumber),
                new Tuple<string[], string>(new[] { "ModifiedAfter" }, "2000-01-01T00:00:00.812+03:00")
            }).AsEnumerable();

            base.ExecuteToTempDB(values, "NsiItem");
        }
    }
}
