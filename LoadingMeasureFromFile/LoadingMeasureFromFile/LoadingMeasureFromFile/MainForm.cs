﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Collections.Generic;
using System.Text;

namespace LoadingMeasureFromFile
{
    public partial class MainForm : Form
    {
        private User _user;
        private SqlConnection _murmanskConnection;
        private SqlConnection _meterConnection;

        public MainForm(User user)
        {
            InitializeComponent();

            _user = user;
            
            try
            {
                InitConnections();
                Text = $"(Login: {user.Login}) {Text}";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void InitConnections()
        {
            _murmanskConnection = CreateConnection("Murmansk", _user);
            _meterConnection = CreateConnection("Meter", _user);
        }

        private bool CloseConnection(SqlConnection connection)
        {
            try
            {
                connection.Close();
            }
            catch { return false; }

            return true;
        }

        /// <summary>
        /// Метод для создания соединения с БД
        /// </summary>
        private SqlConnection CreateConnection(string dataBaseName, User user)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString =
                $@"Data Source=194.41.41.2;Initial Catalog={dataBaseName};Persist Security Info=True;User ID={user.Login};Password={user.Password}";
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return connection;
        }

        private DateTime ChangeDate(DateTime date, int day)
        {
            int diff = day - date.Day;
            DateTime newDate = date.AddDays(diff);

            return newDate;
        }

        private DateTime ChangeDate(DateTime date, int day, int mounth)
        {
            int diffDay = day - date.Day;
            int diffMounth = mounth - date.Month;
            DateTime newDate = date.AddDays(diffDay).AddMonths(diffMounth);

            return newDate;
        }

        /// <summary>
        /// Список для хранения экземпляров класса счетчика с данными
        /// и загрузка в этот список всех данных из файла как объектов класса Meter
        /// </summary>
        private void LoadMetersFromFile()
        {
            using (StreamReader sr = new StreamReader(filePath.Text))
            {
                string line;
                string[] parameters;

                int idMeter;
                DateTime date;
                double measure;

                Meter meter;

                while ((line = sr.ReadLine()) != null)
                {
                    parameters = line.Split(';');
                    
                    try
                    {
                        idMeter = int.Parse(parameters[0]);
                        date = ChangeDate(DateTime.Parse(parameters[2]), 26, DateTime.Now.Month);
                        measure = Math.Round(double.Parse(parameters[1].Replace('.', ',')), 3);

                        meter = new Meter(idMeter, date, measure);

                        MetersIntoMeter("meter_account", meter);
                        MetersIntoMeter("meter_measure_a", meter);
                    } catch {}
                }
            }
        }

        /// <summary>
        /// Метод загрузки показаний из файла в таблицу meter_measure_a
        /// </summary>
        private int ErrorCount { get; set; } = 0;
        private void MetersIntoMeter(string table, Meter meter)
        {
            if (_meterConnection.State == System.Data.ConnectionState.Open)
            {
                SqlCommand command = _meterConnection.CreateCommand();
                command.CommandTimeout = 0;

                SqlTransaction transaction;

                transaction = _meterConnection.BeginTransaction();
                command.Transaction = transaction;

                command.CommandText =
                        $"INSERT INTO {table} (id_meter, measure_date, measure_value) " +
                        $"VALUES ({meter.IdMeter}, '{meter.SqlFormattedDate}', {meter.MeasureValue.ToString().Replace(',', '.')})";
                try
                {
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    ErrorCount++;
                    IntoLog(meter, ex.TargetSite.ToString(), ex.Message);
                    transaction.Rollback();
                }
            }
        }

        /// <summary>
        /// Метод запуска процедуры [town].[Meter_Site_Add]
        /// </summary>
        private void CallMeterSiteAddProcedure()
        {
            if (_murmanskConnection.State == System.Data.ConnectionState.Open)
            {
                SqlCommand command = _murmanskConnection.CreateCommand();
                command.CommandTimeout = 0;

                command.CommandText = "[town].[Meter_Site_Add]";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Очистка таблицы meter_account
        /// </summary>
        private void MeterAccountClear()
        {
            if (_meterConnection.State == System.Data.ConnectionState.Open)
            {
                SqlCommand command = _meterConnection.CreateCommand();
                command.CommandTimeout = 0;

                command.CommandText = "TRUNCATE TABLE meter_account";
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Логирование ошибок
        /// </summary>
        private string pathLog = "log.xml";
        private List<string[]> logs = new List<string[]>();

        private void IntoLog(Meter meter, string from, string message)
        {
            string[] temp =
                {
                    DateTime.Now.ToString("HH:mm:ss"),
                    from,
                    message,
                    meter.IdMeter.ToString(),
                    meter.SqlFormattedDate,
                    meter.MeasureValue.ToString()
                };
            logs.Add(temp);
        }

        private void LogIntoFile()
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(pathLog);

            XmlElement xRoot = xDoc.DocumentElement;

            XmlElement logElem = xDoc.CreateElement("log");
            XmlAttribute dateAttr = xDoc.CreateAttribute("date");
            XmlText dateText = xDoc.CreateTextNode(DateTime.Now.ToString("dd MMMM yyyy"));

            dateAttr.AppendChild(dateText);
            logElem.Attributes.Append(dateAttr);

            foreach (string[] parameters in logs)
            {
                XmlElement errorElem = xDoc.CreateElement("error");
                XmlAttribute timeAttr = xDoc.CreateAttribute("time");

                XmlElement fromElem = xDoc.CreateElement("from");
                XmlElement messageElem = xDoc.CreateElement("message");

                XmlElement meterElem = xDoc.CreateElement("measure");
                XmlAttribute idMeterAttr = xDoc.CreateAttribute("idMeter");
                XmlAttribute measureDateAttr = xDoc.CreateAttribute("measureDate");
                XmlAttribute measureValueAttr = xDoc.CreateAttribute("measureValue");

                XmlText timeText = xDoc.CreateTextNode(parameters[0]);
                XmlText fromText = xDoc.CreateTextNode(parameters[1]);
                XmlText messageText = xDoc.CreateTextNode(parameters[2]);
                XmlText idMeterText = xDoc.CreateTextNode(parameters[3]);
                XmlText measureDateText = xDoc.CreateTextNode(parameters[4]);
                XmlText measureValueText = xDoc.CreateTextNode(parameters[5]);

                timeAttr.AppendChild(timeText);
                fromElem.AppendChild(fromText);
                messageElem.AppendChild(messageText);

                idMeterAttr.AppendChild(idMeterText);
                measureDateAttr.AppendChild(measureDateText);
                measureValueAttr.AppendChild(measureValueText);

                meterElem.Attributes.Append(idMeterAttr);
                meterElem.Attributes.Append(measureDateAttr);
                meterElem.Attributes.Append(measureValueAttr);

                errorElem.Attributes.Append(timeAttr);
                errorElem.AppendChild(fromElem);
                errorElem.AppendChild(messageElem);
                errorElem.AppendChild(meterElem);

                logElem.AppendChild(errorElem);

                xRoot.AppendChild(logElem);

                xDoc.Save(pathLog);
            }
        }

        private void LogClear()
        {
            logs.Clear();
            ErrorCount = 0;
        }

        #region Events
        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!File.Exists(pathLog))
            {
                using (FileStream fs = new FileStream(pathLog, FileMode.Create))
                {
                    using (XmlTextWriter xmlOut = new XmlTextWriter(fs, Encoding.Unicode))
                    {
                        xmlOut.Formatting = Formatting.Indented;

                        xmlOut.WriteStartDocument();
                        xmlOut.WriteComment("Log list from LoadingMeasureFromFile");

                        xmlOut.WriteStartElement("logs");

                        xmlOut.WriteEndElement();
                        xmlOut.WriteEndDocument();
                    }
                }
            }
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
                filePath.Text = openFileDialog.FileName;
        }

        private async void btnLoadFromFile_Click(object sender, EventArgs e)
        {
            try
            {
                Enabled = false;
                Cursor = Cursors.WaitCursor;

                await Task.Run(() => {
                    LoadMetersFromFile();
                    CallMeterSiteAddProcedure();
                    MeterAccountClear();
                });

                if (ErrorCount == 0)
                    MessageBox.Show("Загрузка показаний завершена!");
                else
                    MessageBox.Show($"Загрузка показаний завершена!\nОшибок при загрузке - {ErrorCount}");
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
            finally
            {
                LogIntoFile();
                LogClear();

                Enabled = true;
                Cursor = Cursors.Default;
            }
        }

        private void btnOpenLog_Click(object sender, EventArgs e)
        {
            if (File.Exists(pathLog))
                System.Diagnostics.Process.Start(pathLog);
        }

        private void filePath_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Link;
            else
                e.Effect = DragDropEffects.None;
        }

        private void filePath_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (files != null)
                filePath.Text = files[0];
        }
        #endregion

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseConnection(_murmanskConnection);
            CloseConnection(_meterConnection);
        }
    }
}