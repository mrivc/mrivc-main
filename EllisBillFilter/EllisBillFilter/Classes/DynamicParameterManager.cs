﻿using DevExpress.XtraEditors;
using MassBillNew;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    public class DynamicParameterManager
    {
        private string parametersString;

        public DynamicParameterManager(string id, string parametersString)
        {
            this.parametersString = parametersString;

            Parameters = new List<DynamicParameter>();
            ParameterControls = new List<DynamicParameterBaseControl>();

            ParametersPanelControlsHolder = new GroupBox();
            ParametersPanelControlsHolder.Text = "Параметры запроса:";
            ParametersPanelControlsHolder.Name = ID = id;

            CreateParameterCollection();
            GenerateControls();
            AddParameterControlsToPanelHolder();
        }

        public string ID { get; private set; }

        /// <summary>
        /// Коллекция параметров
        /// </summary>
        public List<DynamicParameter> Parameters { get; private set; }

        /// <summary>
        /// Панель которая размещает контролы всех параметров
        /// </summary>
        public GroupBox ParametersPanelControlsHolder { get; private set; }

        /// <summary>
        /// Коллекция контролов для всех параметров
        /// </summary>
        public List<DynamicParameterBaseControl> ParameterControls { get; private set; }

        public string FillScriptParameters(string script)
        {
            foreach (var parameterControl in ParameterControls)
            {
                switch (parameterControl.Parameter.Type)
                {
                    case "SingleValueList":
                        script = script.Replace(parameterControl.Parameter.LinkSqlParameter, parameterControl.EditValue.ToString());
                        break;
                    case "MultiValueList":
                        script = script.Replace(parameterControl.Parameter.LinkSqlParameter, $"'{parameterControl.EditValue.ToString()}'");
                        break;
                    case "Text":
                        script = script.Replace(parameterControl.Parameter.LinkSqlParameter, $"'{parameterControl.EditValue.ToString()}'");
                        break;
                    case "DateTime":
                        script = script.Replace(parameterControl.Parameter.LinkSqlParameter, $"'{parameterControl.EditValue.ToString()}'");
                        break;
                }
            }

            return script;
        }

        private void CreateParameterCollection()
        {
            var parameterDescriptorSctrings = parametersString.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var parameterInfo in parameterDescriptorSctrings)
            {
                Parameters.Add(new DynamicParameter(parameterInfo));
            }
        }

        private void GenerateControls()
        {
            foreach (var parameter in Parameters)
            {
                DynamicParameterBaseControl control = null;
                switch (parameter.Type)
                {
                    case "SingleValueList":
                        control = new DynamicParameterSingleValueListControl(parameter);
                        break;
                    case "MultiValueList":
                        control = new DynamicParameterMultiValueListControl(parameter);
                        break;
                    case "Text":
                        control = new DynamicParameterTextControl(parameter);
                        break;
                    case "DateTime":
                        control = new DynamicParameterDateTimeControl(parameter);
                        break;
                }
                this.ParameterControls.Add(control);
            }
        }

        /// <summary>
        /// Добавить все контролы параметров на одну панель ParametersPanelHolder
        /// </summary>
        private void AddParameterControlsToPanelHolder()
        {
            if (ParameterControls.Count == 0)
                return;

            int commonHeight = ParameterControls.Where(c => c != null && c.PanelControlHolder != null).Sum(c => c.PanelControlHolder.Height);
            ParametersPanelControlsHolder.Size = new Size(ParameterControls[0].PanelControlHolder.Width + 7, commonHeight + 30);

            foreach (var control in ParameterControls)
            {
                control.PanelControlHolder.Location = new Point(3, CalcControlYLocation());
                ParametersPanelControlsHolder.Controls.Add(control.PanelControlHolder);
            }
        }

        private int CalcControlYLocation()
        {
            int space = 20;
            if (ParametersPanelControlsHolder.Controls.Count == 0)
                return space;

            return ParametersPanelControlsHolder.Controls.Count * DynamicParameterBaseControl.ControlPanelDefaultSize.Height + space;
        }
    }
}
