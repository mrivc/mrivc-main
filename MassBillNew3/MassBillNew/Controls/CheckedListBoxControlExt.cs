﻿using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils.Design;
using System.Data;

namespace MassBillNew {
    class CheckedListBoxControlExt : CheckedListBoxControl
    {
        SelectAllCheckedListBoxItem checkAllItem;

        public CheckedListBoxControlExt()
        {
            this.checkAllItem = new SelectAllCheckedListBoxItem();
            
        }

        void AddCheckAllItem()
        {
            this.Items.Clear();
            this.Items.Add(checkAllItem, false);
            this.ItemCheck += List_ItemCheck;
        }


        public void PopulateListBySqlQuery(string sqlQuery){

            AddCheckAllItem();

            if (sqlQuery != string.Empty)
            {
                DataTable result = SqlClass.ExecuteToDataTable(sqlQuery);

                for (int i = 0; i < result.Rows.Count; i++)
                {
                    var item = new CheckedListBoxItem(result.Rows[i].ItemArray[0], result.Rows[i].ItemArray[1].ToString());
                    this.Items.Add(item);
                }
            }


        }

        public string GetValue()
        {
   
            string result = string.Empty;
            foreach (CheckedListBoxItem item in this.CheckedItems)
            {
                if(this.Items[0] != item)
                result += item.Value.ToString() + ",";
            }
            result = result.TrimEnd(',');
            return result;

        }
        
        void List_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e) {
            CheckedListBoxItem item = this.Items[e.Index];
            if(!IsSelectAllItemChecked(item))
                return;
            RefreshListItemsCheckState(e.State == CheckState.Checked);
        }
        bool IsSelectAllItemChecked(CheckedListBoxItem item) {
            return object.ReferenceEquals(this.checkAllItem, item.Value);
        }
        void RefreshListItemsCheckState(bool checkAll) {
            if(checkAll)
                this.CheckAll();
            else this.UnCheckAll();
        }
        
        protected override void Dispose(bool disposing) {
            this.ItemCheck -= List_ItemCheck;
            base.Dispose(disposing);
        }
    }

    class SelectAllCheckedListBoxItem : CheckedListBoxItem {
        public SelectAllCheckedListBoxItem() { }
        public override string ToString() {
            return "(Выбрать все)";
        }
    }

}
