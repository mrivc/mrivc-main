using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FastReport.Forms;
using FastReport.Export;
using FastReport.Utils;
using FastReport.Cloud.OAuth;

namespace FastReport.Cloud.StorageClient.Dropbox
{
    /// <summary>
    /// Represents form of Dropbox storage client.
    /// </summary>
    public partial class DropboxStorageClientForm : BaseDialogForm
    {
        #region Fields

        private DropboxStorageClient dropboxClient;
        private List<ExportBase> exports;
        private Report report;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DropboxStorageClientForm"/> class.
        /// </summary>
        /// <param name="appInfo">The Dropbox application info.</param>
        /// <param name="report">The report template.</param>
        public DropboxStorageClientForm(AppInfo appInfo, Report report)
        {
            DropboxStorageConfig storageConfig = new DropboxStorageConfig("");
            dropboxClient = new DropboxStorageClient(appInfo, storageConfig);
            this.report = report;
            InitializeComponent();
            Localize();
            Init();
        }

        #endregion // Constructors

        #region Private Methods

        private void Init()
        {
            exports = new List<ExportBase>();
            List<ObjectInfo> list = new List<ObjectInfo>();
            RegisteredObjects.Objects.EnumItems(list);
            cbFileType.Items.Add(Res.Get("Preview,SaveNative"));
            exports.Add(null);
            foreach (ObjectInfo info in list)
            {
                if (info.Object != null && info.Object.IsSubclassOf(typeof(ExportBase)))
                {
                    cbFileType.Items.Add(Res.TryGet(info.Text));
                    exports.Add(Activator.CreateInstance(info.Object) as ExportBase);
                }
            }
            cbFileType.SelectedIndex = 0;

            XmlItem xi = Config.Root.FindItem("DropboxCloud").FindItem("StorageSettings");
            string key = xi.GetProp("ApplicationKey");
            string secret = xi.GetProp("ApplicationSecret");
            if (!String.IsNullOrEmpty(key) && !String.IsNullOrEmpty(secret))
            {
                dropboxClient.AppInfo.Key = key;
                dropboxClient.AppInfo.Secret = secret;
            }
            string str = xi.GetProp("IsUserAuthorized");
            if (String.IsNullOrEmpty(str))
            {
                dropboxClient.IsUserAuthorized = false;
            }
            else
            {
                dropboxClient.IsUserAuthorized = Convert.ToBoolean(str);
            }
            string tokenKey = xi.GetProp("AccessTokenKey");
            string tokenSecret = xi.GetProp("AccessTokenSecret");
            if (!String.IsNullOrEmpty(tokenKey) && !String.IsNullOrEmpty(tokenSecret))
            {
                dropboxClient.AccessToken = new Token(tokenKey, tokenSecret);
            }

            tbServer.Text = xi.GetProp("Server");
            tbPort.Text = xi.GetProp("Port");
            tbUsername.Text = xi.GetProp("Username");
            tbPassword.Text = xi.GetProp("Password");
        }

        private bool IsNumeric(string str)
        {
            if (!String.IsNullOrEmpty(str))
            {
                try
                {
                    Convert.ToInt32(str);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private bool Done()
        {
            if (!String.IsNullOrEmpty(tbPort.Text))
            {
                if (!IsNumeric(tbPort.Text))
                {
                    FRMessageBox.Error(Res.Get("Cloud,Dropbox,PortError"));
                    tbPort.Focus();
                    return false;
                }
            }

            XmlItem xi = Config.Root.FindItem("DropboxCloud").FindItem("StorageSettings");

            xi.SetProp("ApplicationKey", dropboxClient.AppInfo.Key);
            xi.SetProp("ApplicationSecret", dropboxClient.AppInfo.Secret);
            xi.SetProp("IsUserAuthorized", dropboxClient.IsUserAuthorized.ToString());
            xi.SetProp("AccessTokenKey", dropboxClient.AccessToken.TokenKey);
            xi.SetProp("AccessTokenSecret", dropboxClient.AccessToken.TokenSecret);

            xi.SetProp("Server", tbServer.Text);
            xi.SetProp("Port", tbPort.Text);
            xi.SetProp("Username", tbUsername.Text);
            xi.SetProp("Password", tbPassword.Text);

            return true;
        }

        #endregion // Private Methods

        #region Public Methods

        /// <inheritdoc/>
        public override void Localize()
        {
            base.Localize();

            MyRes res = new MyRes("Cloud,Dropbox");
            this.Text = res.Get("");
            pgFile.Text = res.Get("File");
            pgProxy.Text = res.Get("Proxy");
            labelFileType.Text = res.Get("FileType");
            buttonSettings.Text = res.Get("Settings");
            labelServer.Text = res.Get("Server");
            labelUsername.Text = res.Get("Username");
            labelPassword.Text = res.Get("Password");
        }

        #endregion // Public Methods

        #region Events Handlers

        private void cbFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonSettings.Enabled = cbFileType.SelectedIndex != 0;
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            ExportBase export = exports[cbFileType.SelectedIndex];
            export.SetReport(report);
            export.ShowDialog();
        }

        private void DropboxStorageClientForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                if (!Done())
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (!dropboxClient.IsUserAuthorized)
            {
                if (!String.IsNullOrEmpty(tbServer.Text))
                {
                    int port = 0;
                    if (!IsNumeric(tbPort.Text))
                    {
                        FRMessageBox.Error(Res.Get("Cloud,Dropbox,PortError"));
                        tbPort.Focus();
                    }
                    else
                    {
                        port = Convert.ToInt32(tbPort.Text);
                    }
                    dropboxClient.ProxySettings = new CloudProxySettings(ProxyType.Http, tbServer.Text, port, tbUsername.Text, tbPassword.Text);
                }
                string authorizationUrl = dropboxClient.GetAuthorizationUrl();
                WebBrowserForm browser = new WebBrowserForm(authorizationUrl);
                browser.ShowDialog();
                dropboxClient.IsUserAuthorized = true;
            }
            if (dropboxClient.AccessToken == null || String.IsNullOrEmpty(dropboxClient.AccessToken.TokenKey) || String.IsNullOrEmpty(dropboxClient.AccessToken.TokenSecret))
            {
                dropboxClient.GetAccessToken();
            }
            dropboxClient.SaveReport(report, exports[cbFileType.SelectedIndex]);
            DialogResult = DialogResult.OK;
            Close();
        }

        #endregion // Events Handlers
    }
}