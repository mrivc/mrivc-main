﻿namespace EllisBillFilter
{
    partial class FrmPrintDebtBillAcc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrintDebtBillAcc));
            this.gridDebt = new EllisBillFilter.BaseGrid();
            this.debtAccountBalanceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gridDebtView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colchecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colperiod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_account = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_service = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_provider = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbalance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsum_tariff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsum_benefit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_provider_group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colprovider_group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_derive_company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colderive_company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblAddress = new System.Windows.Forms.Label();
            this.ddComboBox = new EllisBillFilter.BaseDropDown();
            this.deriveCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.baseDropDown1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIs_Provider = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIs_Derive_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIs_Pay_Receiver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMeter_Future_Period_Day = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoad_Measure_In_Future = new DevExpress.XtraGrid.Columns.GridColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.ddClientDetail = new EllisBillFilter.BaseDropDown();
            this.debtClientDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_bill_filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbill_filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colclient_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbegin_period_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colend_period_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbegin_period = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colend_period = new DevExpress.XtraGrid.Columns.GridColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.report1 = new FastReport.Report();
            this.txtStatus = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridDebt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.debtAccountBalanceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDebtView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDropDown1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddClientDetail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.debtClientDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridDebt
            // 
            this.gridDebt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDebt.DataSource = this.debtAccountBalanceBindingSource;
            this.gridDebt.Location = new System.Drawing.Point(0, 57);
            this.gridDebt.MainView = this.gridDebtView;
            this.gridDebt.Name = "gridDebt";
            this.gridDebt.Size = new System.Drawing.Size(704, 345);
            this.gridDebt.TabIndex = 0;
            this.gridDebt.TrimCellValueBeforeValidate = true;
            this.gridDebt.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridDebtView});
            // 
            // debtAccountBalanceBindingSource
            // 
            this.debtAccountBalanceBindingSource.DataMember = "DebtAccountBalance";
            this.debtAccountBalanceBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridDebtView
            // 
            this.gridDebtView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colchecked,
            this.colperiod,
            this.colid_account,
            this.colid_service,
            this.colid_provider,
            this.colbalance,
            this.colsum_tariff,
            this.colsum_benefit,
            this.colServiceName,
            this.colid_provider_group,
            this.colprovider_group,
            this.colid_derive_company,
            this.colderive_company});
            this.gridDebtView.GridControl = this.gridDebt;
            this.gridDebtView.GroupCount = 1;
            this.gridDebtView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "balance", null, "")});
            this.gridDebtView.Name = "gridDebtView";
            this.gridDebtView.OptionsView.ShowFooter = true;
            this.gridDebtView.OptionsView.ShowGroupPanel = false;
            this.gridDebtView.OptionsView.ShowIndicator = false;
            this.gridDebtView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colderive_company, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colchecked
            // 
            this.colchecked.Caption = "Д";
            this.colchecked.FieldName = "checked";
            this.colchecked.Name = "colchecked";
            this.colchecked.OptionsColumn.FixedWidth = true;
            this.colchecked.Width = 30;
            // 
            // colperiod
            // 
            this.colperiod.FieldName = "period";
            this.colperiod.Name = "colperiod";
            // 
            // colid_account
            // 
            this.colid_account.FieldName = "id_account";
            this.colid_account.Name = "colid_account";
            // 
            // colid_service
            // 
            this.colid_service.FieldName = "id_service";
            this.colid_service.Name = "colid_service";
            // 
            // colid_provider
            // 
            this.colid_provider.FieldName = "id_provider";
            this.colid_provider.Name = "colid_provider";
            // 
            // colbalance
            // 
            this.colbalance.Caption = "Вх. сальдо";
            this.colbalance.FieldName = "balance";
            this.colbalance.Name = "colbalance";
            this.colbalance.OptionsColumn.AllowEdit = false;
            this.colbalance.OptionsColumn.ReadOnly = true;
            this.colbalance.Visible = true;
            this.colbalance.VisibleIndex = 1;
            this.colbalance.Width = 201;
            // 
            // colsum_tariff
            // 
            this.colsum_tariff.FieldName = "sum_tariff";
            this.colsum_tariff.Name = "colsum_tariff";
            // 
            // colsum_benefit
            // 
            this.colsum_benefit.FieldName = "sum_benefit";
            this.colsum_benefit.Name = "colsum_benefit";
            // 
            // colServiceName
            // 
            this.colServiceName.Caption = "Услуга";
            this.colServiceName.FieldName = "ServiceName";
            this.colServiceName.Name = "colServiceName";
            this.colServiceName.OptionsColumn.AllowEdit = false;
            this.colServiceName.OptionsColumn.ReadOnly = true;
            this.colServiceName.Visible = true;
            this.colServiceName.VisibleIndex = 0;
            this.colServiceName.Width = 178;
            // 
            // colid_provider_group
            // 
            this.colid_provider_group.FieldName = "id_provider_group";
            this.colid_provider_group.Name = "colid_provider_group";
            // 
            // colprovider_group
            // 
            this.colprovider_group.Caption = "Поставщик";
            this.colprovider_group.FieldName = "provider_group";
            this.colprovider_group.Name = "colprovider_group";
            this.colprovider_group.Visible = true;
            this.colprovider_group.VisibleIndex = 2;
            this.colprovider_group.Width = 265;
            // 
            // colid_derive_company
            // 
            this.colid_derive_company.FieldName = "id_derive_company";
            this.colid_derive_company.Name = "colid_derive_company";
            // 
            // colderive_company
            // 
            this.colderive_company.Caption = "УК";
            this.colderive_company.FieldName = "derive_company";
            this.colderive_company.Name = "colderive_company";
            this.colderive_company.OptionsColumn.AllowEdit = false;
            this.colderive_company.OptionsColumn.ReadOnly = true;
            this.colderive_company.Width = 450;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAddress.Location = new System.Drawing.Point(12, 9);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 13);
            this.lblAddress.TabIndex = 4;
            this.lblAddress.Text = "Адрес: ";
            // 
            // ddComboBox
            // 
            this.ddComboBox.EditValue = "";
            this.ddComboBox.Location = new System.Drawing.Point(154, 31);
            this.ddComboBox.Name = "ddComboBox";
            this.ddComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddComboBox.Properties.DataSource = this.deriveCompanyBindingSource;
            this.ddComboBox.Properties.DisplayMember = "Name";
            this.ddComboBox.Properties.NullText = "";
            this.ddComboBox.Properties.PopupFormSize = new System.Drawing.Size(0, 30);
            this.ddComboBox.Properties.PopupSizeable = false;
            this.ddComboBox.Properties.ShowFooter = false;
            this.ddComboBox.Properties.ValueMember = "ID_Company";
            this.ddComboBox.Properties.View = this.baseDropDown1View;
            this.ddComboBox.Size = new System.Drawing.Size(195, 20);
            this.ddComboBox.TabIndex = 5;
            this.ddComboBox.EditValueChanged += new System.EventHandler(this.ddComboBox_EditValueChanged);
            // 
            // deriveCompanyBindingSource
            // 
            this.deriveCompanyBindingSource.DataMember = "DeriveCompany";
            this.deriveCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // baseDropDown1View
            // 
            this.baseDropDown1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Company,
            this.colName,
            this.colIs_Provider,
            this.colIs_Derive_Company,
            this.colIs_Pay_Receiver,
            this.colCode,
            this.colMeter_Future_Period_Day,
            this.colLoad_Measure_In_Future});
            this.baseDropDown1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.baseDropDown1View.Name = "baseDropDown1View";
            this.baseDropDown1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.baseDropDown1View.OptionsView.ShowColumnHeaders = false;
            this.baseDropDown1View.OptionsView.ShowGroupPanel = false;
            this.baseDropDown1View.OptionsView.ShowIndicator = false;
            // 
            // colID_Company
            // 
            this.colID_Company.FieldName = "ID_Company";
            this.colID_Company.Name = "colID_Company";
            this.colID_Company.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colIs_Provider
            // 
            this.colIs_Provider.FieldName = "Is_Provider";
            this.colIs_Provider.Name = "colIs_Provider";
            // 
            // colIs_Derive_Company
            // 
            this.colIs_Derive_Company.FieldName = "Is_Derive_Company";
            this.colIs_Derive_Company.Name = "colIs_Derive_Company";
            // 
            // colIs_Pay_Receiver
            // 
            this.colIs_Pay_Receiver.FieldName = "Is_Pay_Receiver";
            this.colIs_Pay_Receiver.Name = "colIs_Pay_Receiver";
            // 
            // colCode
            // 
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            // 
            // colMeter_Future_Period_Day
            // 
            this.colMeter_Future_Period_Day.FieldName = "Meter_Future_Period_Day";
            this.colMeter_Future_Period_Day.Name = "colMeter_Future_Period_Day";
            // 
            // colLoad_Measure_In_Future
            // 
            this.colLoad_Measure_In_Future.FieldName = "Load_Measure_In_Future";
            this.colLoad_Measure_In_Future.Name = "colLoad_Measure_In_Future";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Управляющая компания:";
            // 
            // ddClientDetail
            // 
            this.ddClientDetail.EditValue = "";
            this.ddClientDetail.Location = new System.Drawing.Point(448, 31);
            this.ddClientDetail.Name = "ddClientDetail";
            this.ddClientDetail.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddClientDetail.Properties.DataSource = this.debtClientDetailBindingSource;
            this.ddClientDetail.Properties.DisplayMember = "bill_filter";
            this.ddClientDetail.Properties.NullText = "";
            this.ddClientDetail.Properties.PopupFormSize = new System.Drawing.Size(650, 0);
            this.ddClientDetail.Properties.ValueMember = "id_bill_filter";
            this.ddClientDetail.Properties.View = this.gridView1;
            this.ddClientDetail.Size = new System.Drawing.Size(244, 20);
            this.ddClientDetail.TabIndex = 7;
            this.ddClientDetail.EditValueChanged += new System.EventHandler(this.ddClientDetail_EditValueChanged);
            // 
            // debtClientDetailBindingSource
            // 
            this.debtClientDetailBindingSource.DataMember = "DebtClientDetail";
            this.debtClientDetailBindingSource.DataSource = this.mDataSet;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_bill_filter,
            this.colbill_filter,
            this.colclient_name,
            this.colbegin_period_name,
            this.colend_period_name,
            this.colbegin_period,
            this.colend_period});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // colid_bill_filter
            // 
            this.colid_bill_filter.FieldName = "id_bill_filter";
            this.colid_bill_filter.Name = "colid_bill_filter";
            // 
            // colbill_filter
            // 
            this.colbill_filter.Caption = "Код фильтра";
            this.colbill_filter.FieldName = "bill_filter";
            this.colbill_filter.Name = "colbill_filter";
            this.colbill_filter.Visible = true;
            this.colbill_filter.VisibleIndex = 0;
            this.colbill_filter.Width = 526;
            // 
            // colclient_name
            // 
            this.colclient_name.Caption = "Получатель платежа";
            this.colclient_name.FieldName = "client_name";
            this.colclient_name.Name = "colclient_name";
            this.colclient_name.Visible = true;
            this.colclient_name.VisibleIndex = 1;
            this.colclient_name.Width = 351;
            // 
            // colbegin_period_name
            // 
            this.colbegin_period_name.Caption = "Дествует С";
            this.colbegin_period_name.FieldName = "begin_period_name";
            this.colbegin_period_name.Name = "colbegin_period_name";
            this.colbegin_period_name.Width = 239;
            // 
            // colend_period_name
            // 
            this.colend_period_name.Caption = "Дествует ПО";
            this.colend_period_name.FieldName = "end_period_name";
            this.colend_period_name.Name = "colend_period_name";
            this.colend_period_name.Visible = true;
            this.colend_period_name.VisibleIndex = 2;
            this.colend_period_name.Width = 291;
            // 
            // colbegin_period
            // 
            this.colbegin_period.FieldName = "begin_period";
            this.colbegin_period.Name = "colbegin_period";
            // 
            // colend_period
            // 
            this.colend_period.FieldName = "end_period";
            this.colend_period.Name = "colend_period";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(365, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Код фильтра:";
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(11, 409);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(184, 23);
            this.btnPrint.TabIndex = 9;
            this.btnPrint.Text = "Сформировать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            // 
            // txtStatus
            // 
            this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatus.Location = new System.Drawing.Point(216, 406);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(476, 30);
            this.txtStatus.TabIndex = 11;
            // 
            // FrmPrintDebtBillAcc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 440);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ddClientDetail);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddComboBox);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.gridDebt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrintDebtBillAcc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Печать долговых квитанций";
            this.Load += new System.EventHandler(this.FrmPrintDebtBillAcc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridDebt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.debtAccountBalanceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDebtView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDropDown1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddClientDetail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.debtClientDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseGrid gridDebt;
        private DevExpress.XtraGrid.Views.Grid.GridView gridDebtView;
        private System.Windows.Forms.BindingSource debtAccountBalanceBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colchecked;
        private DevExpress.XtraGrid.Columns.GridColumn colperiod;
        private DevExpress.XtraGrid.Columns.GridColumn colid_account;
        private DevExpress.XtraGrid.Columns.GridColumn colid_service;
        private DevExpress.XtraGrid.Columns.GridColumn colid_provider;
        private DevExpress.XtraGrid.Columns.GridColumn colbalance;
        private DevExpress.XtraGrid.Columns.GridColumn colsum_tariff;
        private DevExpress.XtraGrid.Columns.GridColumn colsum_benefit;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceName;
        private DevExpress.XtraGrid.Columns.GridColumn colid_provider_group;
        private DevExpress.XtraGrid.Columns.GridColumn colprovider_group;
        private DevExpress.XtraGrid.Columns.GridColumn colid_derive_company;
        private DevExpress.XtraGrid.Columns.GridColumn colderive_company;
        private System.Windows.Forms.Label lblAddress;
        private BaseDropDown ddComboBox;
        private System.Windows.Forms.BindingSource deriveCompanyBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView baseDropDown1View;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colIs_Provider;
        private DevExpress.XtraGrid.Columns.GridColumn colIs_Derive_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colIs_Pay_Receiver;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMeter_Future_Period_Day;
        private DevExpress.XtraGrid.Columns.GridColumn colLoad_Measure_In_Future;
        private System.Windows.Forms.Label label1;
        private BaseDropDown ddClientDetail;
        private System.Windows.Forms.BindingSource debtClientDetailBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colid_bill_filter;
        private DevExpress.XtraGrid.Columns.GridColumn colbill_filter;
        private DevExpress.XtraGrid.Columns.GridColumn colclient_name;
        private DevExpress.XtraGrid.Columns.GridColumn colbegin_period_name;
        private DevExpress.XtraGrid.Columns.GridColumn colend_period_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPrint;
        private FastReport.Report report1;
        private DevExpress.XtraGrid.Columns.GridColumn colbegin_period;
        private DevExpress.XtraGrid.Columns.GridColumn colend_period;
        private System.Windows.Forms.TextBox txtStatus;
    }
}