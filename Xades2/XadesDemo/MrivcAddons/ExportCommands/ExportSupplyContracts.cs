﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ExportSupplyContracts : MrivcExportCommand, ISingleHouseCommand
    {
        bool _isRSO;

        public string FiasHouseGuid { get; set; } = null;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                base.SenderID = value;
            }
        }

        public ExportSupplyContracts(string senderID) : base("exportSupplyResourceContract", "HomeManagementAsync", senderID, "exportSupplyResourceContractResult")
        {
            Description = "Экспорт договоров ресурсоснабжения";
        }

        public bool ContractExists(string senderID, string fiasHouseGuid)
        {
            var obj = DAccess.DataModule.ExecuteScalarQueryCommand1("select GIS.SupplyContractExists(@senderID,@fiasHouseGUID)", new[,] { { "@senderID", senderID }, { "@fiasHouseGUID", fiasHouseGuid } });
            return bool.Parse(obj.ToString());
        }

        new void ExecuteToTempDB()
        {
            base.ExecuteToTempDB(_exportNodeName);
        }

        public new void Execute()
        {
            this.ExecuteToTempDB();
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportSupplyResourceContractData", new object[,] { { "senderID", SenderID } });
        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            //return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.ExportHouseData", new object[,] { { "houseGUID", FiasHouseGuid } }, "Houses", "Entrance", "Premises");
            return new DataSet();
        }
    }
}
