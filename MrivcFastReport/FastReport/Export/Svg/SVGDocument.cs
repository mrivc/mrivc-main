﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;

namespace FastReport.Export.Svg
{
    /// <summary>
    /// Contains Dashes enum
    /// </summary>
    public enum Dashes
    {
        /// <summary>
        /// Specifies the Dash.
        /// </summary>
        Dash,

        /// <summary>
        /// Specifies the Dot.
        /// </summary>
        Dot,

        /// <summary>
        /// Specifies the DashDot.
        /// </summary>
        DashDot,

        /// <summary>
        /// Specifies the DashDotDot.
        /// </summary>
        DashDotDot,

        /// <summary>
        /// Specifies the Double line.
        /// </summary>
        Double
    }
    class SVGDocument
    {
        private XmlAttribute nsAttribute;
        private XmlElement root;
        private XmlDocument doc = new XmlDocument();

       // float Width, Height;
        /// <summary>
        /// Create Window.
        /// </summary>
        public void CreateWindow(string name, float Width, float Height)
        {
           // Width = lWidth; Height = lHeight;
            root = doc.CreateElement("svg");

            nsAttribute = doc.CreateAttribute("xmlns");
            nsAttribute.Value = "http://www.w3.org/2000/svg";
            root.Attributes.Append(nsAttribute);

            nsAttribute = doc.CreateAttribute("xmlns", "xlink", "http://www.w3.org/2000/xmlns/");
            nsAttribute.Value = "http://www.w3.org/1999/xlink";
            root.Attributes.Append(nsAttribute);        

            if (Width == 0)
            {
                Width = 640;
            }
            if (Height == 0)
            {
                Height = 480;
            }
            nsAttribute = doc.CreateAttribute("Width");
            nsAttribute.Value = FloatToString(Width);
            root.Attributes.Append(nsAttribute);

            nsAttribute = doc.CreateAttribute("Height");
            nsAttribute.Value = FloatToString(Height);
            root.Attributes.Append(nsAttribute);

            doc.AppendChild(root);

            XmlElement title = doc.CreateElement("title");
            title.InnerText = name;
            root.AppendChild(title);
        }

        private float TextAlignments(float x, ref float y, string HorizontalAlignment, string VerticalAlignment,XmlElement text, float Width, float Height, Font font, string textstr,
            float PaddingLeft, float PaddingRight, float PaddingTop, float PaddingBottom)
        {
            float Xold = x;
            float Yold = y;
            int N = 1;
            if (textstr != null)
            {
                string[] words = textstr.Split(' ');
                string line = "";
                for (int n = 0; n < words.Length; n++)
                {
                    string testLine = line + words[n] + " ";
                    if (TextRenderer.MeasureText(testLine, font).Width > Width)
                    {
                        N++;
                        line = words[n] + " ";
                    }
                    else
                    {
                        line = testLine;
                    }
                }
            }
            if (HorizontalAlignment == "Center")
            {
                nsAttribute = doc.CreateAttribute("text-anchor");
                nsAttribute.Value = "middle";
                text.Attributes.Append(nsAttribute);

                x = Width / 2 + x;
            }
            if (HorizontalAlignment == "Right")
            {
                nsAttribute = doc.CreateAttribute("text-anchor");
                nsAttribute.Value = "end";
                text.Attributes.Append(nsAttribute);
                x = Width + x;
            }

            if (VerticalAlignment == "Center")
            {
                y = Height / 2 + y - TextRenderer.MeasureText(textstr, font).Height / 2 * N;
            }
            if (VerticalAlignment == "Bottom")
            {
                y = Height + y - TextRenderer.MeasureText(textstr, font).Height * N;
            }
            //Paddings
            if (Xold + PaddingLeft > x)
            {
                x += PaddingLeft;
            }
            if (Xold + Width - PaddingRight < x)
            {
                x -= PaddingRight;
            }
            if (Yold + PaddingTop > y)
            {
                y += PaddingTop;
            }
            if (Yold + Height - PaddingTop < y)
            {
                y -= PaddingBottom;
            }
            return x;
        }

        private bool gBorderLines(string BorderLines, out bool None, out bool Left, out bool Right,
            out bool Top, out bool Bottom)
        {
            string[] masLines = BorderLines.Split(',', ' ');//names lines borders 
            //string AllBorderThickness = null;//set lines
            bool All = false;
             None = false;
             Left = false;
             Right = false;
             Top = false;
             Bottom = false;
            for (int i = 0; i < masLines.Length; i++)
            {
                if (masLines[i] == "All")
                {
                    All = true;
                }
                if (masLines[i] == "None")
                {
                    None = true;
                }
                if (masLines[i] == "Left")
                {
                    Left = true;
                }
                if (masLines[i] == "Right")
                {
                    Right = true;
                }
                if (masLines[i] == "Top")
                {
                    Top = true;
                }
                if (masLines[i] == "Bottom")
                {
                    Bottom = true;
                }
            }
            return All;
        }

        private XmlElement createSVGtext(string caption, float x, float y, XmlElement svgText, float Width, float Height, Font font)
        {     
         string[] words = caption.Split(' ');
         string line = "";
         XmlElement svgTSpan;
         XmlNode tSpanTextNode;
         for (int n = 0; n < words.Length; n++) {
             string testLine = line + words[n] + " ";
             if (TextRenderer.MeasureText(testLine, font).Width > Width)
             {
                 //  Add a new <tspan> element
                 svgTSpan = doc.CreateElement("tspan");
                 svgTSpan.SetAttribute("x", FloatToString(x));
                 svgTSpan.SetAttribute("dy", "1em");

                 tSpanTextNode = doc.CreateTextNode(line);
                 svgTSpan.AppendChild(tSpanTextNode);
                 svgText.AppendChild(svgTSpan);

                 line = words[n] + " ";
             }
             else {
                 line = testLine;
             }
         }

         svgTSpan = doc.CreateElement("tspan");
         svgTSpan.SetAttribute("x", FloatToString(x));
         svgTSpan.SetAttribute("dy", "1em");

         tSpanTextNode = doc.CreateTextNode(line);
         svgTSpan.AppendChild(tSpanTextNode);

         svgText.AppendChild(svgTSpan);

         return svgText;
     }
        ///<summary>
        ///Method for add TextObject.
        /// </summary>
        public void AddTextObject(float x, float y, float Width, float Height,
       string HorizontalAlignment, string VerticalAlignment, string BorderBrush, float BorderThickness,
       float LeftLine, float TopLine, float RightLine, float BottomLine, string LeftLineDashStile,
       string TopLineDashStile, string RightLineDashStile, string BottomLineDashStile, string colorLeftLine,
       string colorTopLine, string colorRightLine, string colorBottomLine, bool Shadow, string ShadowColor, float ShadowWidth, string Background, string BorderLines,
       string Text,string Foreground, float PaddingLeft, float PaddingTop, float PaddingRight, float PaddingBottom, bool WordWrap, float Angle, bool Glass, string colorTop, Font font)
        {
            x = x + BorderThickness;
            y = y + BorderThickness;
            Width = Width + BorderThickness;
            Height = Height + BorderThickness;
            //----Glass--------------
            int i = 0; ++i;
                if (Glass)
                {
                    
                    XmlElement defs = doc.CreateElement("defs");
                    root.AppendChild(defs);

                    XmlElement linearGradient = doc.CreateElement("linearGradient");
                    nsAttribute = doc.CreateAttribute("id");
                    nsAttribute.Value = "grad" + Convert.ToString(i);
                    linearGradient.Attributes.Append(nsAttribute);

                    nsAttribute = doc.CreateAttribute("x1");
                    nsAttribute.Value = "0%";
                    linearGradient.Attributes.Append(nsAttribute);

                    nsAttribute = doc.CreateAttribute("x2");
                    nsAttribute.Value = "0%";
                    linearGradient.Attributes.Append(nsAttribute);

                    nsAttribute = doc.CreateAttribute("y1");
                    nsAttribute.Value = "100%";
                    linearGradient.Attributes.Append(nsAttribute);

                    nsAttribute = doc.CreateAttribute("y2");
                    nsAttribute.Value = "0%";
                    linearGradient.Attributes.Append(nsAttribute);

                    defs.AppendChild(linearGradient);

                    XmlElement stop = doc.CreateElement("stop");

                    nsAttribute = doc.CreateAttribute("offset");
                    nsAttribute.Value = "50%";
                    stop.Attributes.Append(nsAttribute);
                    nsAttribute = doc.CreateAttribute("stop-color");
                    nsAttribute.Value = Background;
                    stop.Attributes.Append(nsAttribute);
                    nsAttribute = doc.CreateAttribute("stop-opacity");
                    nsAttribute.Value = "1";
                    stop.Attributes.Append(nsAttribute);
                    linearGradient.AppendChild(stop);

                    stop = doc.CreateElement("stop");

                    nsAttribute = doc.CreateAttribute("offset");
                    nsAttribute.Value = "50%";
                    stop.Attributes.Append(nsAttribute);
                    nsAttribute = doc.CreateAttribute("stop-color");
                    nsAttribute.Value = colorTop;
                    stop.Attributes.Append(nsAttribute);
                    nsAttribute = doc.CreateAttribute("stop-opacity");
                    nsAttribute.Value = "1";
                    stop.Attributes.Append(nsAttribute);
                    linearGradient.AppendChild(stop);

                    Background = "url(#grad" + Convert.ToString(i) + ")";
                }            
            //--------------------------------------------------------------
            bool All = false;
            bool None = false;
            bool Left = false;
            bool Right = false;
            bool Top = false;
            bool Bottom = false;
            All = gBorderLines(BorderLines, out None, out Left, out Right, out Top, out Bottom);

            if (All && (LeftLine == TopLine && TopLine == RightLine && RightLine == BottomLine) &&
               (LeftLineDashStile == TopLineDashStile && TopLineDashStile == RightLineDashStile &&
               RightLineDashStile == BottomLineDashStile && BottomLineDashStile == "Solid") &&
               (colorLeftLine == colorTopLine && colorTopLine == colorRightLine && colorRightLine == colorBottomLine && colorBottomLine == Background))
            {
                AddRectangle(x, y, Width, Height, BorderBrush, BorderThickness, Background, false);
            }
            else
            {
                if (Background != "none")
                {
                    AddRectangle(x, y, Width, Height, BorderBrush, 0, Background, false);
                }
                if (Left || All)
                {
                    if (LeftLineDashStile == "Solid")
                    {
                        AddLine(x, y, x, y + Height, colorLeftLine, LeftLine);
                    }
                    if (LeftLineDashStile == "Dash")
                    {
                        AddLine(x, y, x, y + Height, colorLeftLine, LeftLine, Dashes.Dash);
                    }
                    if (LeftLineDashStile == "Dot")
                    {
                        AddLine(x, y, x, y + Height, colorLeftLine, LeftLine, Dashes.Dot);
                    }
                    if (LeftLineDashStile == "DashDot")
                    {
                        AddLine(x, y, x, y + Height, colorLeftLine, LeftLine, Dashes.DashDot);
                    }
                    if (LeftLineDashStile == "DashDotDot")
                    {
                        AddLine(x, y, x, y + Height, colorLeftLine, LeftLine, Dashes.DashDotDot);
                    }
                    if (LeftLineDashStile == "Double")
                    {
                        AddLine(x, y, x, y + Height, colorLeftLine, LeftLine, Dashes.Double);
                    }
                }
                if (Right || All)
                {
                    if (RightLineDashStile == "Solid")
                    {
                        AddLine(x + Width, y, x + Width, y + Height, colorRightLine, RightLine);
                    }
                    if (RightLineDashStile == "Dash")
                    {
                        AddLine(x + Width, y, x + Width, y + Height, colorRightLine, RightLine, Dashes.Dash);
                    }
                    if (RightLineDashStile == "Dot")
                    {
                        AddLine(x + Width, y, x + Width, y + Height, colorRightLine, RightLine, Dashes.Dot);
                    }
                    if (RightLineDashStile == "DashDot")
                    {
                        AddLine(x + Width, y, x + Width, y + Height, colorRightLine, RightLine, Dashes.DashDot);
                    }
                    if (RightLineDashStile == "DashDotDot")
                    {
                        AddLine(x + Width, y, x + Width, y + Height, colorRightLine, RightLine, Dashes.DashDotDot);
                    }
                    if (RightLineDashStile == "Double")
                    {
                        AddLine(x + Width, y, x + Width, y + Height, colorRightLine, RightLine, Dashes.DashDotDot);
                    }
                }
                if (Top || All)
                {
                    if (TopLineDashStile == "Solid")
                    {
                        AddLine(x, y, x + Width, y, colorTopLine, TopLine);
                    }
                    if (TopLineDashStile == "Dash")
                    {
                        AddLine(x, y, x + Width, y, colorTopLine, TopLine, Dashes.Dash);
                    }
                    if (TopLineDashStile == "Dot")
                    {
                        AddLine(x, y, x + Width, y, colorTopLine, TopLine, Dashes.Dot);
                    }
                    if (TopLineDashStile == "DashDot")
                    {
                        AddLine(x, y, x + Width, y, colorTopLine, TopLine, Dashes.DashDot);
                    }
                    if (TopLineDashStile == "DashDotDot")
                    {
                        AddLine(x, y, x + Width, y, colorTopLine, TopLine, Dashes.DashDotDot);
                    }
                    if (TopLineDashStile == "Double")
                    {
                        AddLine(x, y, x + Width, y, colorTopLine, TopLine, Dashes.DashDotDot);
                    }
                }
                    if (Bottom || All)
                    {
                        if (BottomLineDashStile == "Solid")
                        {
                            AddLine(x, y + Height, x + Width, y + Height, colorBottomLine, BottomLine);
                        }
                        if (BottomLineDashStile == "Dash")
                        {
                            AddLine(x, y + Height, x + Width, y + Height, colorBottomLine, BottomLine, Dashes.Dash);
                        }
                        if (BottomLineDashStile == "Dot")
                        {
                            AddLine(x, y + Height, x + Width, y + Height, colorBottomLine, BottomLine, Dashes.Dot);
                        }
                        if (BottomLineDashStile == "DashDot")
                        {
                            AddLine(x, y + Height, x + Width, y + Height, colorBottomLine, BottomLine, Dashes.DashDot);
                        }
                        if (BottomLineDashStile == "DashDotDot")
                        {
                            AddLine(x, y + Height, x + Width, y + Height, colorBottomLine, BottomLine, Dashes.DashDotDot);
                        }
                        if (BottomLineDashStile == "Double")
                        {
                            AddLine(x, y + Height, x + Width, y + Height, colorBottomLine, BottomLine, Dashes.DashDotDot);
                        }
                    }
                }
            //Shadow-------------------
            if (Shadow)
            {
                AddLine(x + ShadowWidth, y + Height + ShadowWidth / 2, x + Width + ShadowWidth, y + Height + ShadowWidth / 2, ShadowColor, ShadowWidth);

                AddLine(x + Width + ShadowWidth / 2, y + ShadowWidth, x + Width + ShadowWidth / 2, y + Height + ShadowWidth, ShadowColor, ShadowWidth);
            }
                XmlElement text = doc.CreateElement("text");

                x = TextAlignments(x, ref y, HorizontalAlignment, VerticalAlignment, text, Width, Height, font, Text, PaddingLeft, PaddingRight, PaddingTop, PaddingBottom);

                if (Text != null)
                {
                    text = createSVGtext(Text, x, y, text, Width, Height, font);
                }
                else
                    text = doc.CreateElement("text");

                nsAttribute = doc.CreateAttribute("x");
                nsAttribute.Value = FloatToString(x);
                text.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("y");
                nsAttribute.Value = FloatToString(y);
                text.Attributes.Append(nsAttribute);

            
                nsAttribute = doc.CreateAttribute("font-size");
                nsAttribute.Value = FloatToString(font.Size) + "pt";
                text.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("font-family");
                nsAttribute.Value = Convert.ToString(font.Name);
                text.Attributes.Append(nsAttribute);
                root.AppendChild(text);

                if (font.Bold)
                {
                    nsAttribute = doc.CreateAttribute("font-weight");
                    nsAttribute.Value = "bold";
                    text.Attributes.Append(nsAttribute);
                    root.AppendChild(text);
                }
                if (font.Italic)
                {
                    nsAttribute = doc.CreateAttribute("font-style");
                    nsAttribute.Value = "italic";
                    text.Attributes.Append(nsAttribute);
                    root.AppendChild(text);
                }
                if (font.Underline)
                {
                    nsAttribute = doc.CreateAttribute("text-decoration");
                    nsAttribute.Value = "underline";
                    text.Attributes.Append(nsAttribute);
                    root.AppendChild(text);
                }
                nsAttribute = doc.CreateAttribute("fill");
                nsAttribute.Value = Foreground;
                text.Attributes.Append(nsAttribute);
                root.AppendChild(text);

                if (Angle != 0)
                {
                    string rot = "0";

                    if (Angle >= 90)
                    {
                        rot = "rotate(" + FloatToString(Angle) + "," + FloatToString(x + Width / 2) + "," + FloatToString(y + Height / 2) + ")";
                    }
                    else
                        rot = "rotate(" + FloatToString(Angle) + "," + FloatToString(x) + "," + FloatToString(y) + ")";
                    nsAttribute = doc.CreateAttribute("transform");
                    nsAttribute.Value = rot;
                    text.Attributes.Append(nsAttribute);
                    root.AppendChild(text);
                }             
            
        }
        /// <summary>
        /// Method to add rectangle.
        /// </summary>
        public void AddRectangle(float x, float y, float Width, float Height,
                                  string Stroke, float StrokeThickness, string Fill, bool Rounded)
        {
            XmlElement rect = doc.CreateElement("rect");

            nsAttribute = doc.CreateAttribute("x");
            nsAttribute.Value = FloatToString(x);
            rect.Attributes.Append(nsAttribute);

            nsAttribute = doc.CreateAttribute("y");
            nsAttribute.Value = FloatToString(y);
            rect.Attributes.Append(nsAttribute);
            //attribute 2----------------------------------
            if (Width != 0)
            {
                nsAttribute = doc.CreateAttribute("width");
                nsAttribute.Value = FloatToString(Width);
                rect.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Height != 0)
            {
                nsAttribute = doc.CreateAttribute("height");
                nsAttribute.Value = FloatToString(Height);
                rect.Attributes.Append(nsAttribute);
            }
            //attribute 4--------------------------------------------
           // if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("stroke");
                nsAttribute.Value = Stroke;
                rect.Attributes.Append(nsAttribute);
            }
            //attribute 5----------------------------------------------
            if (StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("stroke-width");
                nsAttribute.Value = FloatToString(StrokeThickness);
                rect.Attributes.Append(nsAttribute);
            }
            //attribute 6-----------------------------------------------
            if (Fill != null)
            {
                nsAttribute = doc.CreateAttribute("fill");
                nsAttribute.Value = Fill;
                rect.Attributes.Append(nsAttribute);
            }
            //attribute 7 Rounded
            if (Rounded)
            {
                nsAttribute = doc.CreateAttribute("rx");
                nsAttribute.Value = "10";
                rect.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("ry");
                nsAttribute.Value = "10";
                rect.Attributes.Append(nsAttribute);
            }
            root.AppendChild(rect);
        }

        /// <summary>
        /// Method for add ellips.
        /// </summary>
        public void AddEllipse(float x, float y, float Width, float Height,
             string Stroke, float StrokeThickness, string Fill)
        {
            XmlElement ellipse = doc.CreateElement("ellipse");

            //attribute 1------------------------------
            if (x != 0 || y != 0)
            {
                nsAttribute = doc.CreateAttribute("cx");
                nsAttribute.Value = FloatToString(x + Width/2);
                ellipse.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("cy");
                nsAttribute.Value = FloatToString(y + Height/2);
                ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 2----------------------------------
            if (Width != 0)
            {
                nsAttribute = doc.CreateAttribute("rx");
                nsAttribute.Value = FloatToString(Width/2);
                ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Height != 0)
            {
                nsAttribute = doc.CreateAttribute("ry");
                nsAttribute.Value = FloatToString(Height/2);
                ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 4--------------------------------------------
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("stroke");
                nsAttribute.Value = Stroke;
                ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 5----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("stroke-width");
                nsAttribute.Value = FloatToString(StrokeThickness);
                ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 6-----------------------------------------------
            if (Fill != null)
            {
                nsAttribute = doc.CreateAttribute("fill");
                nsAttribute.Value = Fill;
                ellipse.Attributes.Append(nsAttribute);
            }
            root.AppendChild(ellipse);
        }

        /// <summary>
        /// Method for add triangle.
        /// </summary>
        public void AddTriangle(float x, float y, float Width, float Height,
             string Stroke, float StrokeThickness, string Fill)
        {
            XmlElement polygon = doc.CreateElement("polygon");

            {
                string Points;
                float x1 = Width / 2 + x;
                float y1 = y;
                float x2 = Width + x;
                float y2 = Height + y;
                float x3 = x;
                float y3 = Height + y;
                Points = FloatToString(x1) + "," + FloatToString(y1) + "," + FloatToString(x2) + "," + FloatToString(y2) + "," + FloatToString(x3) + "," + FloatToString(y3);
                nsAttribute = doc.CreateAttribute("points");
                nsAttribute.Value = Points;
                polygon.Attributes.Append(nsAttribute);
            }
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("stroke");
                nsAttribute.Value = Stroke;
                polygon.Attributes.Append(nsAttribute);
            }
            //attribute -----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("stroke-width");
                nsAttribute.Value = FloatToString(StrokeThickness);
                polygon.Attributes.Append(nsAttribute);
            }
            if (Fill != null)
            {
                nsAttribute = doc.CreateAttribute("fill");
                nsAttribute.Value = Fill;
                polygon.Attributes.Append(nsAttribute);
            }
            root.AppendChild(polygon);
        }

        /// <summary>
        /// Method for add Diamond.
        /// </summary>
        public void AddDiamond(float x, float y, float Width, float Height,
             string Stroke, float StrokeThickness, string Fill)
        {
            XmlElement polygon = doc.CreateElement("polygon");

            {
                string Points;
                float x1 = Width / 2 + x; float y1 = y;
                float x2 = Width + x; float y2 = Height / 2 + y;
                float x3 = Width / 2 + x; float y3 = Height + y;
                float x4 = x; float y4 = Height / 2 + y;
                Points = FloatToString(x1) + "," + FloatToString(y1) + "," + FloatToString(x2) + "," + FloatToString(y2) + "," + FloatToString(x3) + "," + FloatToString(y3) + "," + FloatToString(x4) + "," + FloatToString(y4);
                nsAttribute = doc.CreateAttribute("points");
                nsAttribute.Value = Points;
                polygon.Attributes.Append(nsAttribute);
            }
            //attribute 5----------------------------------------------
            if (Stroke != null)
            {
                nsAttribute = doc.CreateAttribute("stroke");
                nsAttribute.Value = Stroke;
                polygon.Attributes.Append(nsAttribute);
            }
            //attribute 6-----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("stroke-width");
                nsAttribute.Value = FloatToString(StrokeThickness);
                polygon.Attributes.Append(nsAttribute);
            }

            if (Fill != null)
            {
                nsAttribute = doc.CreateAttribute("fill");
                nsAttribute.Value = Fill;
                polygon.Attributes.Append(nsAttribute);
            }
            root.AppendChild(polygon);
        }

        ///<summary>
        ///Method for add line.
        /// </summary>
        public void AddLine(float x, float y, float X2, float Y2, string Stroke, float StrokeThickness/*,
            string Fill*/)
        {
            XmlElement line = doc.CreateElement("line");

            if (x != 0 || y != 0)
            {
                nsAttribute = doc.CreateAttribute("x1");
                nsAttribute.Value = FloatToString(x);
                line.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("y1");
                nsAttribute.Value = FloatToString(y);
                line.Attributes.Append(nsAttribute);
            }

            //attribute 2----------------------------------
            if (X2 != 0)
            {
                nsAttribute = doc.CreateAttribute("x2");
                nsAttribute.Value = FloatToString(X2);
                line.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Y2 != 0)
            {
                nsAttribute = doc.CreateAttribute("y2");
                nsAttribute.Value = FloatToString(Y2);
                line.Attributes.Append(nsAttribute);
            }

            //attribute 6--------------------------------------------
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("stroke");
                nsAttribute.Value = Stroke;
                line.Attributes.Append(nsAttribute);
            }
            //attribute 7----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("stroke-width");
                nsAttribute.Value = FloatToString(StrokeThickness);
                line.Attributes.Append(nsAttribute);
            }
            root.AppendChild(line);
        }

        ///<summary>
        ///Method for add line with dash.
        /// </summary>
        public void AddLine(float x, float y, float X2, float Y2, string Stroke, float StrokeThickness, Dashes dash)
        {
            XmlElement line = doc.CreateElement("line");
            if (x != 0 || y != 0)
            {
                nsAttribute = doc.CreateAttribute("x1");
                nsAttribute.Value = FloatToString(x);
                line.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("y1");
                nsAttribute.Value = FloatToString(y);
                line.Attributes.Append(nsAttribute);
            }

            //attribute 2----------------------------------
            if (X2 != 0)
            {
                nsAttribute = doc.CreateAttribute("x2");
                nsAttribute.Value = FloatToString(X2);
                line.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Y2 != 0)
            {
                nsAttribute = doc.CreateAttribute("y2");
                nsAttribute.Value = FloatToString(Y2);
                line.Attributes.Append(nsAttribute);
            }

            //attribute 6--------------------------------------------
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("stroke");
                nsAttribute.Value = Stroke;
                line.Attributes.Append(nsAttribute);
            }
            //attribute 7----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("stroke-width");
                nsAttribute.Value = FloatToString(StrokeThickness);
                line.Attributes.Append(nsAttribute);
            }
            
            //attribute 8-----------------------------------------------
            string StrokeDashArray = null;
            if (dash == Dashes.Dash)
            {
                StrokeDashArray = "3";
            }
            if (dash == Dashes.Dot)
            {
                StrokeDashArray = "1";
            }
            if (dash == Dashes.DashDot)
            {
                StrokeDashArray = "1 3 1";
            }
            if (dash == Dashes.DashDotDot)
            {
                StrokeDashArray = "1 1 3 1 1";
            }
            if (dash == Dashes.Double)
            {
                StrokeDashArray = null;
                AddLine(x+10, y+10, X2+10, Y2+10, Stroke, StrokeThickness);
            }
            if (StrokeDashArray != null)
            {
                nsAttribute = doc.CreateAttribute("stroke-dasharray");
                nsAttribute.Value = StrokeDashArray;
                line.Attributes.Append(nsAttribute);
            }

            root.AppendChild(line);
        }

        /// <summary>
        /// Add image
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void AddImage(string filename, float left, float top, float width, float height)
        {
            if (!String.IsNullOrEmpty(filename))
            {
                XmlElement image = doc.CreateElement("image");
                nsAttribute = doc.CreateAttribute("xlink", "href", "http://www.w3.org/1999/xlink");
                nsAttribute.Value = filename;
                image.Attributes.Append(nsAttribute);
                if (left != 0 || top != 0)
                {
                    nsAttribute = doc.CreateAttribute("x");
                    nsAttribute.Value = FloatToString(left);
                    image.Attributes.Append(nsAttribute);

                    nsAttribute = doc.CreateAttribute("y");
                    nsAttribute.Value = FloatToString(top);
                    image.Attributes.Append(nsAttribute);
                }
                if (width != 0)
                {
                    nsAttribute = doc.CreateAttribute("width");
                    nsAttribute.Value = FloatToString(width);
                    image.Attributes.Append(nsAttribute);
                }
                if (height != 0)
                {
                    nsAttribute = doc.CreateAttribute("height");
                    nsAttribute.Value = FloatToString(height);
                    image.Attributes.Append(nsAttribute);
                }
                root.AppendChild(image);
            }
        }

        /// <summary>
        /// Save svg file.
        /// </summary>
        public void Save(string filename)
        {
            doc.Save(filename);
        }

        /// <summary>
        /// Save svg stream.
        /// </summary>
        public void Save(Stream stream)
        {
            doc.Save(stream);
        }

        /// <param name="name"></param>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        public SVGDocument(string name, float Width, float Height)
        {
            CreateWindow(name, Width, Height);        
        }

        private string FloatToString(double value)
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberGroupSeparator = String.Empty;
            provider.NumberDecimalSeparator = ".";
            return Convert.ToString(Math.Round(value, 2), provider);
        }
    }
}
