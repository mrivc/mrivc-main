﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Xades.Helpers;

namespace GisGkh
{
    public class MrivcXmlFormatter: ICloneable
    {
        XmlDocument xmlBody = new XmlDocument();
        XmlDocument root = new XmlDocument();
        XmlNamespaceManager manager;

        IEnumerable<Tuple<string[], string>> _values;

        public IEnumerable<Tuple<string[], string>> Values
        {
            set
            {
                _values = value;
                foreach (Tuple<string[], string> valuePair in _values)
                {
                    try
                    {
                        var _node = xmlBody.SelectSingleNode(MrivcStatic.NodePathWithoutPrefix(valuePair.Item1), manager);
                        _node.InnerText = valuePair.Item2;
                    }
                    catch (Exception)
                    {
                        throw new Exception($"Элемент {valuePair.Item1} не найден");
                    }
                }
            }
        }

        public string XmlBody
        {
            get
            {
                return xmlBody.InnerXml;
            }
        }

        public void LoadRoot(string xml)
        {
            xmlBody.LoadXml(xml);
            root.LoadXml(xml);
            manager = xmlBody.CreateNamespaceManager();
        }

        public bool Contains(string[] element)
        {
            var node = xmlBody.SelectSingleNode(MrivcStatic.NodePathWithoutPrefix(element), manager);
            return node != null;
        }

        public bool Contains(string[] element, string[] element2)
        {
            var node = xmlBody.SelectSingleNode(MrivcStatic.NodePathWithoutPrefix(element), manager);
            if (node == null)
            {
                node = xmlBody.SelectSingleNode(MrivcStatic.NodePathWithoutPrefix(element2), manager);
            }
            return node != null;
        }

        public void RemoveNodesByName(string[] element)
        {
            var nodes = xmlBody.SelectNodes(MrivcStatic.NodePathWithoutPrefix(element), manager);
            foreach (XmlNode node in nodes)
            {
                node.ParentNode.RemoveChild(node);
            }
        }

        public void RemoveNodesByName(string[] element, string[] element2)
        {
            var nodes = xmlBody.SelectNodes(MrivcStatic.NodePathWithoutPrefix(element), manager);
            foreach (XmlNode node in nodes)
            {
                node.ParentNode.RemoveChild(node);
            }
            
            nodes = xmlBody.SelectNodes(MrivcStatic.NodePathWithoutPrefix(element2), manager);
            foreach (XmlNode node in nodes)
            {
                node.ParentNode.RemoveChild(node);
            }
        }

        public void RemoveNodesByName(string[] element, XmlNode xmlNode)
        {
            var node1 = xmlNode.Clone();
            var nodes = node1.SelectNodes(MrivcStatic.NodePathWithoutPrefix(element), manager);
            foreach (XmlNode node in nodes)
            {
                node.ParentNode.RemoveChild(node);
            }
            xmlNode.InnerXml = node1.InnerXml;
        }

        public XmlNode AddChildElement(string childNodeXml, string[] elementsNameToAdd, IEnumerable<Tuple<string[], string>> values)
        {
            var elementToAdd = MrivcStatic.NodePathWithoutPrefix(elementsNameToAdd);
            var rootCopy = root.Clone();
            var node = rootCopy.SelectSingleNode(elementToAdd, manager);

            node.InnerXml = childNodeXml;

            foreach (Tuple<string[],string> valuePair in values)
            {
                try
                {
                    var _node = node.SelectSingleNode(MrivcStatic.NodePathWithoutPrefix(valuePair.Item1), manager);
                    _node.InnerText = valuePair.Item2;
                }
                catch (Exception)
                {
                    throw new Exception($"Элемент {valuePair.Item1} не найден");
                }
            }

            var node1 = xmlBody.SelectSingleNode(MrivcStatic.NodePathWithoutPrefix(elementsNameToAdd), manager);
            node1.InnerXml += node.InnerXml;

            return node1.LastChild;
        }

        public XmlNode AddChildElement(string childNodeXml, XmlNode xmlNodeToAdd, IEnumerable<Tuple<string[], string>> values)
        {
            //var elementToAdd = MrivcStatic.NodePathWithoutPrefix(elementsNameToAdd);
            var rootCopy = root.Clone();
            var node = rootCopy.FirstChild;

            node.InnerXml = childNodeXml;

            foreach (Tuple<string[], string> valuePair in values)
            {
                try
                {
                    var _node = node.SelectSingleNode(MrivcStatic.NodePathWithoutPrefix(valuePair.Item1), manager);
                    _node.InnerText = valuePair.Item2;
                }
                catch (Exception)
                {
                    throw new Exception($"Элемент {valuePair.Item1} не найден");
                }
            }

            var node1 = xmlNodeToAdd;
            node1.InnerXml += node.InnerXml;

            return node1.LastChild;
        }

        public XmlNode AddChildElement(string childNodeXml, string[] elementsNameToAdd)
        {
            var elementToAdd = MrivcStatic.NodePathWithoutPrefix(elementsNameToAdd);
            var rootCopy = root.Clone();
            var node = rootCopy.SelectSingleNode(elementToAdd, manager);
            node.InnerXml = childNodeXml;


            var node1 = xmlBody.SelectSingleNode(MrivcStatic.NodePathWithoutPrefix(elementsNameToAdd), manager);
            node1.InnerXml += node.InnerXml;

            return node1.LastChild;
        }

        public XmlNode AddChildElement(string childNodeXml, XmlNode xmlNodeToAdd)
        {
            //var elementToAdd = MrivcStatic.NodePathWithoutPrefix(elementsNameToAdd);
            var rootCopy = root.Clone();
            var node = rootCopy.FirstChild;
            node.InnerXml = childNodeXml;


            var node1 = xmlNodeToAdd;
            node1.InnerXml += node.InnerXml;

            return node1.LastChild;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
