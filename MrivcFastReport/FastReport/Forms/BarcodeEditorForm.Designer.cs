﻿namespace FastReport.Forms
{
    partial class BarcodeEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BarcodeEditorForm));
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.qrTabs = new System.Windows.Forms.TabControl();
            this.qrTypeText = new System.Windows.Forms.TabPage();
            this.qrText = new System.Windows.Forms.TextBox();
            this.qrTypeVcard = new System.Windows.Forms.TabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.qrVcPhoneWork = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.qrVcPhone = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.qrVcEmailWork = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.qrVcEmailHome = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.qrVcCountry = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.qrVcCity = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.qrVcZip = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.qrVcStreet = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.qrVcWebsite = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.qrVcCompany = new System.Windows.Forms.TextBox();
            this.qrVcPhoneHome = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.qrVcTitle = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.qrVcLN = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.qrVcFN = new System.Windows.Forms.TextBox();
            this.qrTypeURI = new System.Windows.Forms.TabPage();
            this.label30 = new System.Windows.Forms.Label();
            this.qrURI = new System.Windows.Forms.TextBox();
            this.qrTypeEmailAddress = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.qrEmail = new System.Windows.Forms.TextBox();
            this.qrTypeEmailMessage = new System.Windows.Forms.TabPage();
            this.qrEmailText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.qrEmailSub = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.qrEmailTo = new System.Windows.Forms.TextBox();
            this.qrTypeGeo = new System.Windows.Forms.TabPage();
            this.qrGeoMeters = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.qrGeoLongitude = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.qrGeoLatitude = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.qrTypeSMS = new System.Windows.Forms.TabPage();
            this.qrSMSText = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.qrSMSTo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.qrTypeCall = new System.Windows.Forms.TabPage();
            this.qrCall = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.qrTypeEvent = new System.Windows.Forms.TabPage();
            this.qrEventTo = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.qrEventFrom = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.qrEventDesc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.qrTypeWifi = new System.Windows.Forms.TabPage();
            this.qrWifiHidden = new System.Windows.Forms.CheckBox();
            this.qrWifiPass = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.qrWifiName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.qrWifiEncryption = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.qrSelectType = new System.Windows.Forms.ComboBox();
            this.tabItem1 = new FastReport.DevComponents.DotNetBar.TabItem(this.components);
            this.tabItem2 = new FastReport.DevComponents.DotNetBar.TabItem(this.components);
            this.tabItem3 = new FastReport.DevComponents.DotNetBar.TabItem(this.components);
            this.tabItem4 = new FastReport.DevComponents.DotNetBar.TabItem(this.components);
            this.tabItem5 = new FastReport.DevComponents.DotNetBar.TabItem(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.tvData = new FastReport.Controls.DataTreeView();
            this.expandableSplitter1 = new System.Windows.Forms.Splitter();
            this.lblDescription = new FastReport.Controls.DescriptionControl();
            this.qrTabs.SuspendLayout();
            this.qrTypeText.SuspendLayout();
            this.qrTypeVcard.SuspendLayout();
            this.qrTypeURI.SuspendLayout();
            this.qrTypeEmailAddress.SuspendLayout();
            this.qrTypeEmailMessage.SuspendLayout();
            this.qrTypeGeo.SuspendLayout();
            this.qrTypeSMS.SuspendLayout();
            this.qrTypeCall.SuspendLayout();
            this.qrTypeEvent.SuspendLayout();
            this.qrTypeWifi.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(428, 319);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(84, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "Create";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(519, 319);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(84, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // qrTabs
            // 
            this.qrTabs.Controls.Add(this.qrTypeText);
            this.qrTabs.Controls.Add(this.qrTypeVcard);
            this.qrTabs.Controls.Add(this.qrTypeURI);
            this.qrTabs.Controls.Add(this.qrTypeEmailAddress);
            this.qrTabs.Controls.Add(this.qrTypeEmailMessage);
            this.qrTabs.Controls.Add(this.qrTypeGeo);
            this.qrTabs.Controls.Add(this.qrTypeSMS);
            this.qrTabs.Controls.Add(this.qrTypeCall);
            this.qrTabs.Controls.Add(this.qrTypeEvent);
            this.qrTabs.Controls.Add(this.qrTypeWifi);
            this.qrTabs.Location = new System.Drawing.Point(5, 39);
            this.qrTabs.Name = "qrTabs";
            this.qrTabs.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.qrTabs.SelectedIndex = 0;
            this.qrTabs.Size = new System.Drawing.Size(421, 298);
            this.qrTabs.TabIndex = 2;
            // 
            // qrTypeText
            // 
            this.qrTypeText.Controls.Add(this.qrText);
            this.qrTypeText.Location = new System.Drawing.Point(4, 22);
            this.qrTypeText.Name = "qrTypeText";
            this.qrTypeText.Padding = new System.Windows.Forms.Padding(3);
            this.qrTypeText.Size = new System.Drawing.Size(413, 272);
            this.qrTypeText.TabIndex = 0;
            this.qrTypeText.Text = "Text";
            this.qrTypeText.UseVisualStyleBackColor = true;
            // 
            // qrText
            // 
            this.qrText.AllowDrop = true;
            this.qrText.Location = new System.Drawing.Point(3, 3);
            this.qrText.Multiline = true;
            this.qrText.Name = "qrText";
            this.qrText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.qrText.Size = new System.Drawing.Size(407, 266);
            this.qrText.TabIndex = 0;
            // 
            // qrTypeVcard
            // 
            this.qrTypeVcard.Controls.Add(this.label28);
            this.qrTypeVcard.Controls.Add(this.qrVcPhoneWork);
            this.qrTypeVcard.Controls.Add(this.label19);
            this.qrTypeVcard.Controls.Add(this.qrVcPhone);
            this.qrTypeVcard.Controls.Add(this.label27);
            this.qrTypeVcard.Controls.Add(this.label26);
            this.qrTypeVcard.Controls.Add(this.qrVcEmailWork);
            this.qrTypeVcard.Controls.Add(this.label25);
            this.qrTypeVcard.Controls.Add(this.qrVcEmailHome);
            this.qrTypeVcard.Controls.Add(this.label24);
            this.qrTypeVcard.Controls.Add(this.qrVcCountry);
            this.qrTypeVcard.Controls.Add(this.label23);
            this.qrTypeVcard.Controls.Add(this.qrVcCity);
            this.qrTypeVcard.Controls.Add(this.label22);
            this.qrTypeVcard.Controls.Add(this.qrVcZip);
            this.qrTypeVcard.Controls.Add(this.label31);
            this.qrTypeVcard.Controls.Add(this.qrVcStreet);
            this.qrTypeVcard.Controls.Add(this.label21);
            this.qrTypeVcard.Controls.Add(this.qrVcWebsite);
            this.qrTypeVcard.Controls.Add(this.label20);
            this.qrTypeVcard.Controls.Add(this.qrVcCompany);
            this.qrTypeVcard.Controls.Add(this.qrVcPhoneHome);
            this.qrTypeVcard.Controls.Add(this.label18);
            this.qrTypeVcard.Controls.Add(this.qrVcTitle);
            this.qrTypeVcard.Controls.Add(this.label17);
            this.qrTypeVcard.Controls.Add(this.qrVcLN);
            this.qrTypeVcard.Controls.Add(this.label16);
            this.qrTypeVcard.Controls.Add(this.qrVcFN);
            this.qrTypeVcard.Location = new System.Drawing.Point(4, 22);
            this.qrTypeVcard.Name = "qrTypeVcard";
            this.qrTypeVcard.Padding = new System.Windows.Forms.Padding(3);
            this.qrTypeVcard.Size = new System.Drawing.Size(413, 272);
            this.qrTypeVcard.TabIndex = 1;
            this.qrTypeVcard.Text = "vCard";
            this.qrTypeVcard.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(209, 78);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(88, 13);
            this.label28.TabIndex = 35;
            this.label28.Text = "Phone (business)";
            // 
            // qrVcPhoneWork
            // 
            this.qrVcPhoneWork.AllowDrop = true;
            this.qrVcPhoneWork.Location = new System.Drawing.Point(211, 94);
            this.qrVcPhoneWork.Name = "qrVcPhoneWork";
            this.qrVcPhoneWork.Size = new System.Drawing.Size(200, 20);
            this.qrVcPhoneWork.TabIndex = 34;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(209, 3);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Phone (mobile)";
            // 
            // qrVcPhone
            // 
            this.qrVcPhone.AllowDrop = true;
            this.qrVcPhone.Location = new System.Drawing.Point(211, 16);
            this.qrVcPhone.Name = "qrVcPhone";
            this.qrVcPhone.Size = new System.Drawing.Size(200, 20);
            this.qrVcPhone.TabIndex = 32;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(209, 39);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(87, 13);
            this.label27.TabIndex = 31;
            this.label27.Text = "Phone (personal)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(0, 234);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(82, 13);
            this.label26.TabIndex = 29;
            this.label26.Text = "Email (business)";
            // 
            // qrVcEmailWork
            // 
            this.qrVcEmailWork.AllowDrop = true;
            this.qrVcEmailWork.Location = new System.Drawing.Point(2, 250);
            this.qrVcEmailWork.Name = "qrVcEmailWork";
            this.qrVcEmailWork.Size = new System.Drawing.Size(200, 20);
            this.qrVcEmailWork.TabIndex = 28;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(0, 195);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(81, 13);
            this.label25.TabIndex = 27;
            this.label25.Text = "Email (personal)";
            // 
            // qrVcEmailHome
            // 
            this.qrVcEmailHome.AllowDrop = true;
            this.qrVcEmailHome.Location = new System.Drawing.Point(2, 211);
            this.qrVcEmailHome.Name = "qrVcEmailHome";
            this.qrVcEmailHome.Size = new System.Drawing.Size(200, 20);
            this.qrVcEmailHome.TabIndex = 26;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(209, 234);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 13);
            this.label24.TabIndex = 25;
            this.label24.Text = "Country";
            // 
            // qrVcCountry
            // 
            this.qrVcCountry.AllowDrop = true;
            this.qrVcCountry.Location = new System.Drawing.Point(211, 250);
            this.qrVcCountry.Name = "qrVcCountry";
            this.qrVcCountry.Size = new System.Drawing.Size(200, 20);
            this.qrVcCountry.TabIndex = 24;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(209, 195);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(24, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "City";
            // 
            // qrVcCity
            // 
            this.qrVcCity.AllowDrop = true;
            this.qrVcCity.Location = new System.Drawing.Point(211, 211);
            this.qrVcCity.Name = "qrVcCity";
            this.qrVcCity.Size = new System.Drawing.Size(200, 20);
            this.qrVcCity.TabIndex = 22;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(209, 156);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "Zip code";
            // 
            // qrVcZip
            // 
            this.qrVcZip.AllowDrop = true;
            this.qrVcZip.Location = new System.Drawing.Point(211, 172);
            this.qrVcZip.Name = "qrVcZip";
            this.qrVcZip.Size = new System.Drawing.Size(200, 20);
            this.qrVcZip.TabIndex = 20;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(209, 117);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 13);
            this.label31.TabIndex = 19;
            this.label31.Text = "Street";
            // 
            // qrVcStreet
            // 
            this.qrVcStreet.AllowDrop = true;
            this.qrVcStreet.Location = new System.Drawing.Point(211, 133);
            this.qrVcStreet.Name = "qrVcStreet";
            this.qrVcStreet.Size = new System.Drawing.Size(200, 20);
            this.qrVcStreet.TabIndex = 18;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(0, 156);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 13);
            this.label21.TabIndex = 17;
            this.label21.Text = "Website";
            // 
            // qrVcWebsite
            // 
            this.qrVcWebsite.AllowDrop = true;
            this.qrVcWebsite.Location = new System.Drawing.Point(2, 172);
            this.qrVcWebsite.Name = "qrVcWebsite";
            this.qrVcWebsite.Size = new System.Drawing.Size(200, 20);
            this.qrVcWebsite.TabIndex = 16;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(0, 117);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(51, 13);
            this.label20.TabIndex = 15;
            this.label20.Text = "Company";
            // 
            // qrVcCompany
            // 
            this.qrVcCompany.AllowDrop = true;
            this.qrVcCompany.Location = new System.Drawing.Point(2, 133);
            this.qrVcCompany.Name = "qrVcCompany";
            this.qrVcCompany.Size = new System.Drawing.Size(200, 20);
            this.qrVcCompany.TabIndex = 14;
            // 
            // qrVcPhoneHome
            // 
            this.qrVcPhoneHome.AllowDrop = true;
            this.qrVcPhoneHome.Location = new System.Drawing.Point(211, 55);
            this.qrVcPhoneHome.Name = "qrVcPhoneHome";
            this.qrVcPhoneHome.Size = new System.Drawing.Size(200, 20);
            this.qrVcPhoneHome.TabIndex = 12;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(0, 78);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(27, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Title";
            // 
            // qrVcTitle
            // 
            this.qrVcTitle.AllowDrop = true;
            this.qrVcTitle.Location = new System.Drawing.Point(2, 94);
            this.qrVcTitle.Name = "qrVcTitle";
            this.qrVcTitle.Size = new System.Drawing.Size(200, 20);
            this.qrVcTitle.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(0, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 7;
            this.label17.Text = "Last name";
            // 
            // qrVcLN
            // 
            this.qrVcLN.AllowDrop = true;
            this.qrVcLN.Location = new System.Drawing.Point(2, 55);
            this.qrVcLN.Name = "qrVcLN";
            this.qrVcLN.Size = new System.Drawing.Size(200, 20);
            this.qrVcLN.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "First name";
            // 
            // qrVcFN
            // 
            this.qrVcFN.AllowDrop = true;
            this.qrVcFN.Location = new System.Drawing.Point(3, 16);
            this.qrVcFN.Name = "qrVcFN";
            this.qrVcFN.Size = new System.Drawing.Size(200, 20);
            this.qrVcFN.TabIndex = 4;
            // 
            // qrTypeURI
            // 
            this.qrTypeURI.Controls.Add(this.label30);
            this.qrTypeURI.Controls.Add(this.qrURI);
            this.qrTypeURI.Location = new System.Drawing.Point(4, 22);
            this.qrTypeURI.Name = "qrTypeURI";
            this.qrTypeURI.Size = new System.Drawing.Size(413, 272);
            this.qrTypeURI.TabIndex = 2;
            this.qrTypeURI.Text = "URI";
            this.qrTypeURI.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(26, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "URI";
            // 
            // qrURI
            // 
            this.qrURI.AllowDrop = true;
            this.qrURI.Location = new System.Drawing.Point(3, 16);
            this.qrURI.Name = "qrURI";
            this.qrURI.Size = new System.Drawing.Size(407, 20);
            this.qrURI.TabIndex = 0;
            // 
            // qrTypeEmailAddress
            // 
            this.qrTypeEmailAddress.Controls.Add(this.label29);
            this.qrTypeEmailAddress.Controls.Add(this.qrEmail);
            this.qrTypeEmailAddress.Location = new System.Drawing.Point(4, 22);
            this.qrTypeEmailAddress.Name = "qrTypeEmailAddress";
            this.qrTypeEmailAddress.Size = new System.Drawing.Size(413, 272);
            this.qrTypeEmailAddress.TabIndex = 3;
            this.qrTypeEmailAddress.Text = "E-mail Address";
            this.qrTypeEmailAddress.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(0, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(35, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "E-mail";
            // 
            // qrEmail
            // 
            this.qrEmail.AllowDrop = true;
            this.qrEmail.Location = new System.Drawing.Point(3, 16);
            this.qrEmail.Name = "qrEmail";
            this.qrEmail.Size = new System.Drawing.Size(407, 20);
            this.qrEmail.TabIndex = 1;
            // 
            // qrTypeEmailMessage
            // 
            this.qrTypeEmailMessage.Controls.Add(this.qrEmailText);
            this.qrTypeEmailMessage.Controls.Add(this.label3);
            this.qrTypeEmailMessage.Controls.Add(this.qrEmailSub);
            this.qrTypeEmailMessage.Controls.Add(this.label2);
            this.qrTypeEmailMessage.Controls.Add(this.label1);
            this.qrTypeEmailMessage.Controls.Add(this.qrEmailTo);
            this.qrTypeEmailMessage.Location = new System.Drawing.Point(4, 22);
            this.qrTypeEmailMessage.Name = "qrTypeEmailMessage";
            this.qrTypeEmailMessage.Size = new System.Drawing.Size(413, 272);
            this.qrTypeEmailMessage.TabIndex = 4;
            this.qrTypeEmailMessage.Text = "E-mail Message";
            this.qrTypeEmailMessage.UseVisualStyleBackColor = true;
            // 
            // qrEmailText
            // 
            this.qrEmailText.AllowDrop = true;
            this.qrEmailText.Location = new System.Drawing.Point(3, 117);
            this.qrEmailText.Multiline = true;
            this.qrEmailText.Name = "qrEmailText";
            this.qrEmailText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.qrEmailText.Size = new System.Drawing.Size(407, 152);
            this.qrEmailText.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Text";
            // 
            // qrEmailSub
            // 
            this.qrEmailSub.AllowDrop = true;
            this.qrEmailSub.Location = new System.Drawing.Point(3, 66);
            this.qrEmailSub.Name = "qrEmailSub";
            this.qrEmailSub.Size = new System.Drawing.Size(407, 20);
            this.qrEmailSub.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Subject";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "E-mail";
            // 
            // qrEmailTo
            // 
            this.qrEmailTo.AllowDrop = true;
            this.qrEmailTo.Location = new System.Drawing.Point(3, 16);
            this.qrEmailTo.Name = "qrEmailTo";
            this.qrEmailTo.Size = new System.Drawing.Size(407, 20);
            this.qrEmailTo.TabIndex = 2;
            // 
            // qrTypeGeo
            // 
            this.qrTypeGeo.Controls.Add(this.qrGeoMeters);
            this.qrTypeGeo.Controls.Add(this.label6);
            this.qrTypeGeo.Controls.Add(this.qrGeoLongitude);
            this.qrTypeGeo.Controls.Add(this.label4);
            this.qrTypeGeo.Controls.Add(this.qrGeoLatitude);
            this.qrTypeGeo.Controls.Add(this.label5);
            this.qrTypeGeo.Location = new System.Drawing.Point(4, 22);
            this.qrTypeGeo.Name = "qrTypeGeo";
            this.qrTypeGeo.Size = new System.Drawing.Size(413, 272);
            this.qrTypeGeo.TabIndex = 5;
            this.qrTypeGeo.Text = "Geolocation";
            this.qrTypeGeo.UseVisualStyleBackColor = true;
            // 
            // qrGeoMeters
            // 
            this.qrGeoMeters.AllowDrop = true;
            this.qrGeoMeters.Location = new System.Drawing.Point(3, 118);
            this.qrGeoMeters.Name = "qrGeoMeters";
            this.qrGeoMeters.Size = new System.Drawing.Size(407, 20);
            this.qrGeoMeters.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(0, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Height";
            // 
            // qrGeoLongitude
            // 
            this.qrGeoLongitude.AllowDrop = true;
            this.qrGeoLongitude.Location = new System.Drawing.Point(3, 66);
            this.qrGeoLongitude.Name = "qrGeoLongitude";
            this.qrGeoLongitude.Size = new System.Drawing.Size(407, 20);
            this.qrGeoLongitude.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Longitude";
            // 
            // qrGeoLatitude
            // 
            this.qrGeoLatitude.AllowDrop = true;
            this.qrGeoLatitude.Location = new System.Drawing.Point(3, 16);
            this.qrGeoLatitude.Name = "qrGeoLatitude";
            this.qrGeoLatitude.Size = new System.Drawing.Size(407, 20);
            this.qrGeoLatitude.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Latitude";
            // 
            // qrTypeSMS
            // 
            this.qrTypeSMS.Controls.Add(this.qrSMSText);
            this.qrTypeSMS.Controls.Add(this.label8);
            this.qrTypeSMS.Controls.Add(this.qrSMSTo);
            this.qrTypeSMS.Controls.Add(this.label7);
            this.qrTypeSMS.Location = new System.Drawing.Point(4, 22);
            this.qrTypeSMS.Name = "qrTypeSMS";
            this.qrTypeSMS.Size = new System.Drawing.Size(413, 272);
            this.qrTypeSMS.TabIndex = 6;
            this.qrTypeSMS.Text = "SMS";
            this.qrTypeSMS.UseVisualStyleBackColor = true;
            // 
            // qrSMSText
            // 
            this.qrSMSText.AllowDrop = true;
            this.qrSMSText.Location = new System.Drawing.Point(3, 66);
            this.qrSMSText.Name = "qrSMSText";
            this.qrSMSText.Size = new System.Drawing.Size(407, 20);
            this.qrSMSText.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(0, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Text";
            // 
            // qrSMSTo
            // 
            this.qrSMSTo.AllowDrop = true;
            this.qrSMSTo.Location = new System.Drawing.Point(3, 16);
            this.qrSMSTo.Name = "qrSMSTo";
            this.qrSMSTo.Size = new System.Drawing.Size(407, 20);
            this.qrSMSTo.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Phone number";
            // 
            // qrTypeCall
            // 
            this.qrTypeCall.Controls.Add(this.qrCall);
            this.qrTypeCall.Controls.Add(this.label9);
            this.qrTypeCall.Location = new System.Drawing.Point(4, 22);
            this.qrTypeCall.Name = "qrTypeCall";
            this.qrTypeCall.Size = new System.Drawing.Size(413, 272);
            this.qrTypeCall.TabIndex = 7;
            this.qrTypeCall.Text = "Call";
            this.qrTypeCall.UseVisualStyleBackColor = true;
            // 
            // qrCall
            // 
            this.qrCall.AllowDrop = true;
            this.qrCall.Location = new System.Drawing.Point(3, 16);
            this.qrCall.Name = "qrCall";
            this.qrCall.Size = new System.Drawing.Size(407, 20);
            this.qrCall.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Phone number";
            // 
            // qrTypeEvent
            // 
            this.qrTypeEvent.Controls.Add(this.qrEventTo);
            this.qrTypeEvent.Controls.Add(this.label12);
            this.qrTypeEvent.Controls.Add(this.qrEventFrom);
            this.qrTypeEvent.Controls.Add(this.label11);
            this.qrTypeEvent.Controls.Add(this.qrEventDesc);
            this.qrTypeEvent.Controls.Add(this.label10);
            this.qrTypeEvent.Location = new System.Drawing.Point(4, 22);
            this.qrTypeEvent.Name = "qrTypeEvent";
            this.qrTypeEvent.Size = new System.Drawing.Size(413, 272);
            this.qrTypeEvent.TabIndex = 8;
            this.qrTypeEvent.Text = "Event";
            this.qrTypeEvent.UseVisualStyleBackColor = true;
            // 
            // qrEventTo
            // 
            this.qrEventTo.CustomFormat = "dd MMMM yyyy    HH:mm:ss";
            this.qrEventTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.qrEventTo.Location = new System.Drawing.Point(209, 66);
            this.qrEventTo.Name = "qrEventTo";
            this.qrEventTo.Size = new System.Drawing.Size(200, 20);
            this.qrEventTo.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(206, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "To";
            // 
            // qrEventFrom
            // 
            this.qrEventFrom.CustomFormat = "dd MMMM yyyy    HH:mm:ss";
            this.qrEventFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.qrEventFrom.Location = new System.Drawing.Point(3, 66);
            this.qrEventFrom.Name = "qrEventFrom";
            this.qrEventFrom.Size = new System.Drawing.Size(200, 20);
            this.qrEventFrom.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(0, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "From";
            // 
            // qrEventDesc
            // 
            this.qrEventDesc.AllowDrop = true;
            this.qrEventDesc.Location = new System.Drawing.Point(3, 16);
            this.qrEventDesc.Name = "qrEventDesc";
            this.qrEventDesc.Size = new System.Drawing.Size(407, 20);
            this.qrEventDesc.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Description";
            // 
            // qrTypeWifi
            // 
            this.qrTypeWifi.Controls.Add(this.qrWifiHidden);
            this.qrTypeWifi.Controls.Add(this.qrWifiPass);
            this.qrTypeWifi.Controls.Add(this.label15);
            this.qrTypeWifi.Controls.Add(this.qrWifiName);
            this.qrTypeWifi.Controls.Add(this.label14);
            this.qrTypeWifi.Controls.Add(this.qrWifiEncryption);
            this.qrTypeWifi.Controls.Add(this.label13);
            this.qrTypeWifi.Location = new System.Drawing.Point(4, 22);
            this.qrTypeWifi.Name = "qrTypeWifi";
            this.qrTypeWifi.Size = new System.Drawing.Size(413, 272);
            this.qrTypeWifi.TabIndex = 9;
            this.qrTypeWifi.Text = "Wi-Fi";
            this.qrTypeWifi.UseVisualStyleBackColor = true;
            // 
            // qrWifiHidden
            // 
            this.qrWifiHidden.AutoSize = true;
            this.qrWifiHidden.Location = new System.Drawing.Point(3, 100);
            this.qrWifiHidden.Name = "qrWifiHidden";
            this.qrWifiHidden.Size = new System.Drawing.Size(130, 17);
            this.qrWifiHidden.TabIndex = 20;
            this.qrWifiHidden.Text = "Wifi network is hidden";
            this.qrWifiHidden.UseVisualStyleBackColor = true;
            // 
            // qrWifiPass
            // 
            this.qrWifiPass.AllowDrop = true;
            this.qrWifiPass.Location = new System.Drawing.Point(209, 66);
            this.qrWifiPass.Name = "qrWifiPass";
            this.qrWifiPass.Size = new System.Drawing.Size(200, 20);
            this.qrWifiPass.TabIndex = 19;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(206, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Password";
            // 
            // qrWifiName
            // 
            this.qrWifiName.AllowDrop = true;
            this.qrWifiName.Location = new System.Drawing.Point(3, 66);
            this.qrWifiName.Name = "qrWifiName";
            this.qrWifiName.Size = new System.Drawing.Size(200, 20);
            this.qrWifiName.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(0, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Network name";
            // 
            // qrWifiEncryption
            // 
            this.qrWifiEncryption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.qrWifiEncryption.FormattingEnabled = true;
            this.qrWifiEncryption.Items.AddRange(new object[] {
            "WPA",
            "WEP",
            "unencrypted"});
            this.qrWifiEncryption.Location = new System.Drawing.Point(3, 16);
            this.qrWifiEncryption.Name = "qrWifiEncryption";
            this.qrWifiEncryption.Size = new System.Drawing.Size(121, 21);
            this.qrWifiEncryption.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Encryption";
            // 
            // qrSelectType
            // 
            this.qrSelectType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.qrSelectType.FormattingEnabled = true;
            this.qrSelectType.Items.AddRange(new object[] {
            "Text",
            "vCard",
            "URI",
            "E-mail Address",
            "E-mail Message",
            "Geolocation",
            "SMS",
            "Call",
            "Event",
            "Wi-Fi"});
            this.qrSelectType.Location = new System.Drawing.Point(12, 12);
            this.qrSelectType.Name = "qrSelectType";
            this.qrSelectType.Size = new System.Drawing.Size(407, 21);
            this.qrSelectType.TabIndex = 16;
            this.qrSelectType.SelectedIndexChanged += new System.EventHandler(this.qrSelectType_SelectedIndexChanged);
            // 
            // tabItem1
            // 
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.Text = "1";
            this.tabItem1.Visible = false;
            // 
            // tabItem2
            // 
            this.tabItem2.Name = "tabItem2";
            this.tabItem2.Text = "2";
            // 
            // tabItem3
            // 
            this.tabItem3.Name = "tabItem3";
            this.tabItem3.Text = "3";
            // 
            // tabItem4
            // 
            this.tabItem4.Name = "tabItem4";
            this.tabItem4.Text = "4";
            // 
            // tabItem5
            // 
            this.tabItem5.Name = "tabItem5";
            this.tabItem5.Text = "5";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tvData);
            this.panel1.Controls.Add(this.expandableSplitter1);
            this.panel1.Controls.Add(this.lblDescription);
            this.panel1.Location = new System.Drawing.Point(428, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 301);
            this.panel1.TabIndex = 17;
            // 
            // tvData
            // 
            this.tvData.AllowDrop = true;
            this.tvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvData.ExpandedNodes = ((System.Collections.Generic.List<string>)(resources.GetObject("tvData.ExpandedNodes")));
            this.tvData.ImageIndex = 0;
            this.tvData.Location = new System.Drawing.Point(0, 0);
            this.tvData.Name = "tvData";
            this.tvData.SelectedImageIndex = 0;
            this.tvData.ShowColumns = true;
            this.tvData.ShowDataSources = true;
            this.tvData.ShowDialogs = true;
            this.tvData.ShowEnabledOnly = true;
            this.tvData.ShowFunctions = true;
            this.tvData.ShowNone = false;
            this.tvData.ShowParameters = true;
            this.tvData.ShowRelations = true;
            this.tvData.ShowTotals = true;
            this.tvData.ShowVariables = true;
            this.tvData.Size = new System.Drawing.Size(175, 222);
            this.tvData.TabIndex = 3;
            this.tvData.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvData_AfterSelect);
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.expandableSplitter1.Location = new System.Drawing.Point(0, 222);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(175, 3);
            this.expandableSplitter1.TabIndex = 4;
            this.expandableSplitter1.TabStop = false;
            this.expandableSplitter1.Visible = false;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoScroll = true;
            this.lblDescription.BackColor = System.Drawing.SystemColors.Window;
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblDescription.Location = new System.Drawing.Point(0, 225);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(175, 76);
            this.lblDescription.TabIndex = 5;
            this.lblDescription.Visible = false;
            // 
            // BarcodeEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(614, 350);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.qrSelectType);
            this.Controls.Add(this.qrTabs);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BarcodeEditorForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QR_Edit";
            this.qrTabs.ResumeLayout(false);
            this.qrTypeText.ResumeLayout(false);
            this.qrTypeText.PerformLayout();
            this.qrTypeVcard.ResumeLayout(false);
            this.qrTypeVcard.PerformLayout();
            this.qrTypeURI.ResumeLayout(false);
            this.qrTypeURI.PerformLayout();
            this.qrTypeEmailAddress.ResumeLayout(false);
            this.qrTypeEmailAddress.PerformLayout();
            this.qrTypeEmailMessage.ResumeLayout(false);
            this.qrTypeEmailMessage.PerformLayout();
            this.qrTypeGeo.ResumeLayout(false);
            this.qrTypeGeo.PerformLayout();
            this.qrTypeSMS.ResumeLayout(false);
            this.qrTypeSMS.PerformLayout();
            this.qrTypeCall.ResumeLayout(false);
            this.qrTypeCall.PerformLayout();
            this.qrTypeEvent.ResumeLayout(false);
            this.qrTypeEvent.PerformLayout();
            this.qrTypeWifi.ResumeLayout(false);
            this.qrTypeWifi.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TabControl qrTabs;
        private System.Windows.Forms.TabPage qrTypeText;
        private System.Windows.Forms.TabPage qrTypeVcard;
        private System.Windows.Forms.TabPage qrTypeURI;
        private System.Windows.Forms.TextBox qrURI;
        private System.Windows.Forms.TabPage qrTypeEmailAddress;
        private System.Windows.Forms.TextBox qrEmail;
        private System.Windows.Forms.TabPage qrTypeEmailMessage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox qrEmailSub;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox qrEmailTo;
        private System.Windows.Forms.TabPage qrTypeGeo;
        private System.Windows.Forms.TextBox qrGeoMeters;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox qrGeoLongitude;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox qrGeoLatitude;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage qrTypeSMS;
        private System.Windows.Forms.TextBox qrSMSText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox qrSMSTo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage qrTypeCall;
        private System.Windows.Forms.TextBox qrCall;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage qrTypeEvent;
        private System.Windows.Forms.TabPage qrTypeWifi;
        private System.Windows.Forms.DateTimePicker qrEventTo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker qrEventFrom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox qrEventDesc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox qrWifiEncryption;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox qrVcWebsite;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox qrVcCompany;
        private System.Windows.Forms.TextBox qrVcPhoneHome;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox qrVcTitle;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox qrVcLN;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox qrVcFN;
        private System.Windows.Forms.CheckBox qrWifiHidden;
        private System.Windows.Forms.TextBox qrWifiPass;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox qrWifiName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox qrVcCountry;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox qrVcCity;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox qrVcZip;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox qrVcStreet;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox qrVcPhone;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox qrVcEmailWork;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox qrVcEmailHome;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox qrVcPhoneWork;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox qrSelectType;
        private FastReport.DevComponents.DotNetBar.TabItem tabItem1;
        private FastReport.DevComponents.DotNetBar.TabItem tabItem2;
        private FastReport.DevComponents.DotNetBar.TabItem tabItem3;
        private FastReport.DevComponents.DotNetBar.TabItem tabItem4;
        private FastReport.DevComponents.DotNetBar.TabItem tabItem5;
        private System.Windows.Forms.TextBox qrText;
        private System.Windows.Forms.TextBox qrEmailText;
        private System.Windows.Forms.Panel panel1;
        private Controls.DataTreeView tvData;
        private System.Windows.Forms.Splitter expandableSplitter1;
        private Controls.DescriptionControl lblDescription;
    }
}