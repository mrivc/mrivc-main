﻿using System.Data;

namespace GisGkh
{
    public interface ISingleHouseCommand
    {
        string FiasHouseGuid { get; set; }
        void Execute();
        DataSet PrepareToDataSet();
        string SenderID { get; set; }
        //еуыы
    }
}
