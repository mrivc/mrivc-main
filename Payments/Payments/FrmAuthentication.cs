﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using Payments.LogicClasses;
using Payments.DS;
using System.Diagnostics;

namespace Payments
{
    public partial class FrmAuthentication : Form
    {


        public FrmAuthentication()
        {
            InitializeComponent();
        }

        private void frmAuthentication_Load(object sender, EventArgs e)
        {
            //получаем доступных пользователей программы
            //try
            //{
                this.userTableAdapter.Connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["Payments"].ConnectionString);
                this.userTableAdapter.Fill(this.paymentsDataSet.User);

                if (Properties.Settings.Default.LastUserID > -1)
                    ddUser.EditValue = Properties.Settings.Default.LastUserID;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Не удалось установить подключение с базой данных платежей: Payments.\nПроверьте строку подключения в файле настроек Payments.exe.config.\n\nОшибка: " + ex.Message);
            //    this.Close();
            //}
        }

        private bool CheckUser()
        {
            if (ddUser.EditValue == null || txtPassword.Text.Length == 0)
                return false;

            Properties.Settings.Default.LastUserID = (int) ddUser.EditValue;
            Properties.Settings.Default.Save();

            DataRowView row = (DataRowView)ddUser.Properties.GetDataSourceRowByKeyValue(ddUser.EditValue);
            ApplicationUser user = new ApplicationUser(
                    (int)row["ID_User"],
                    row["Login"].ToString(),
                    row["Name"].ToString(),
                    row["Full_Name"].ToString(),
                    row["Position"].ToString(),
                    row["Manager_Fio"].ToString(),
                    (row["Fiscal_User_ID"] == DBNull.Value ? 0 : (int)row["Fiscal_User_ID"]),
                    (row["Fiscal_User_Password"] == DBNull.Value ? 0 : (int)row["Fiscal_User_Password"]),
                    (bool)row["ReadOnlyPrivilegy"],
                    (bool)row["FilterPaymentsByUser"],
                    (bool)row["SimulateTerminalMode"],
                    (DateTime)row["CreatedDate"]
                );
            DAccess.Instance.SetCurrentUser(user);

            //устанавливаем соединение с БД
            return SetConnections();
        }

        private bool SetConnections(){
            //проверка соединения с базой данных платежей Payments
            this.Cursor = Cursors.WaitCursor;
            if (!DAccess.DataModule.ConnectedPayments)
                try
                {
                    DAccess.DataModule.ConnectionStringToPayment = "Data Source=" + this.userTableAdapter.Connection.DataSource + ";Initial Catalog=" + this.userTableAdapter.Connection.Database + ";Persist Security Info=True;User ID=" + DAccess.Instance.CurrentUser.Login + ";Password=" + txtPassword.Text;
                    DAccess.DataModule.SetPaymentConnection();
                    lblConnPayments.Text = lblConnPayments.Text.Substring(0, lblConnPayments.Text.IndexOf("<")) + "<соединение установлено>";
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    lblConnPayments.Text = lblConnPayments.Text.Substring(0, lblConnPayments.Text.IndexOf("<")) + "<ошибка>";
                    MessageBox.Show("Не удалось соединиться с базой данных платежей (Сервер " + 
                        this.userTableAdapter.Connection.DataSource + ", база данных " + this.userTableAdapter.Connection.Database + ")." +
                        "\nПроверьте строку подключения в файле настроек Payments.exe.config." +
                        "\nВ строку соединения подставляется пользователь, выбранный из списка.\n\nОшибка: " + ex.Message);
                    return false;
                }

            string ellisDbServer = DAccess.Instance.ProgramSettings.ELLIS_DB_SERVER;
            string ellisDbName = DAccess.Instance.ProgramSettings.ELLIS_DB_NAME;

            //проверка соединения в основной базой данных ЭЛЛИС - Ellis
            if (!DAccess.DataModule.ConnectedEllis)
                try
                {
                    DAccess.DataModule.ConnectionStringToEllis = "Data Source=" + ellisDbServer + ";Initial Catalog=" + ellisDbName + ";Persist Security Info=True;User ID=" + DAccess.Instance.CurrentUser.Login + ";Password=" + txtPassword.Text;
                    DAccess.DataModule.SetEllisConnection();
                    lblConnEllis.Text = lblConnEllis.Text.Substring(0, lblConnEllis.Text.IndexOf("<")) + "<соединение установлено>";
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    lblConnEllis.Text = lblConnEllis.Text.Substring(0, lblConnEllis.Text.IndexOf("<")) + "<ошибка>";

                    if (MessageBox.Show("Не удалось соединиться с базой данных ЭЛЛИС ЖКХ (Сервер " + ellisDbServer + ", база данных " + ellisDbName + ").\n" +
                            "\nПрием коммунальных платежей будет не доступен, продолжить работу с программой?" +
                            "\n\nНаименование сервера и база данных указаны в настроечной таблице Program.Settings.\nОшибка: " + ex.Message, "Информация", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.OK)
                        return true;
                    else
                        return false;
                }
            this.Cursor = Cursors.Default;
            return true;
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (CheckUser() && RunComkey())
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Загрузка драйвера сканера штрих кода 
        /// </summary>
        /// <returns></returns>
        private bool RunComkey()
        {
            //если сканер штрих кодов не используется тогда не загружаем его
            if (!Properties.Settings.Default.UseBarcodeScaner)
                return true;

            string comkeyDriver = Properties.Settings.Default.BarcodeScannerDriverExe;
            string comkeyPrecess = Properties.Settings.Default.BarcodeScannerDriverExe.ToLower().Replace(".exe","");

            Process[] processes = Process.GetProcesses();
            foreach (Process p in processes)
                if (p.ProcessName.ToLower() == comkeyPrecess)
                    return true;

            if (!FileHelper.ExistFile("Comkey\\"+comkeyDriver))
            {
                if (MessageBox.Show("Не удалось найти драйвер сканера шрих-кодов в папке приложения (\"<Корневая папка программы>\\Comkey\\" + comkeyDriver + "\"), сканер работать не будет. Запустить программу?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.OK)
                    return true;
                else
                    return false;
            }

            System.Diagnostics.Process.Start("Comkey\\" + comkeyDriver);


            return true;
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                CheckUser();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
