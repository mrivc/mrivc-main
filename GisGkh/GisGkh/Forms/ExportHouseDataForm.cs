﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class ExportHouseDataForm : Form
    {
        public string SenderID
        {
            get { return ""; }
        }

        public ExportHouseDataForm()
        {
            InitializeComponent();
        }

        private void ExportHouseDataForm_Load(object sender, EventArgs e)
        {

        }

        private void exportBtn_Click(object sender, EventArgs e)
        {
            var exportHouseData = new ExportHouseData(SenderID);
            var ds = MrivcStatic.HouseIteration(exportHouseData, true);

            exportDataGridView.DataSource = ds.Tables[0];
            entranceDataGridView.DataSource = ds.Tables[1];
            premisesDataGridView.DataSource = ds.Tables[2];


            var exportAccountData = new ExportAccountData(SenderID);
            ds = MrivcStatic.HouseIteration(exportAccountData, true);

            accountDataGridView.DataSource = ds.Tables[0];
        }

        private void exportDataGridView_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void entranceDataGridView_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void exportToDbBtn_Click(object sender, EventArgs e)
        {
            var exportHouseData = new ExportHouseData(SenderID);
            MrivcStatic.HouseIteration(exportHouseData, false);
        }

        private void exportAccountBtn_Click(object sender, EventArgs e)
        {
            var exportAccountData = new ExportAccountData(SenderID);
            var ds = MrivcStatic.HouseIteration(exportAccountData, true);

            accountDataGridView.DataSource = ds.Tables[0];
        }

        private void exportAccountToDbBtn_Click(object sender, EventArgs e)
        {
            var exportAccountData = new ExportAccountData(SenderID);
            MrivcStatic.HouseIteration(exportAccountData, false);
        }

        private void exportDataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //label1.Text = exportDataGridView.Rows.Count.ToString();
        }

        private void accountDataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

        }

        private void exportHouseBtn_Click(object sender, EventArgs e)
        {

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }
    }
}
