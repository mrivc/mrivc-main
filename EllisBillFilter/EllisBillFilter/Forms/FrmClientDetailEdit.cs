﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;

namespace EllisBillFilter
{
    public partial class FrmClientDetailEdit : Form
    {
        private DataRow copyRowSource;

        public FrmClientDetailEdit(MDataSet.ClientDetailRow activeData, bool isNewRow, DataRow copyRowSource = null, bool deepCopy = false)
        {
            InitializeComponent();
            this.copyRowSource = copyRowSource;

            if (copyRowSource != null && deepCopy)
                rowID_Derive_Company.Enabled = false;
            else
                rowID_Derive_Company.Enabled = true;

            LoadData(activeData, isNewRow);
        }

        private void LoadData(MDataSet.ClientDetailRow activeData, bool isNewRow)
        {
            mDataSet.Period.Merge(DAccess.Instance.Periods);
            mDataSet.BillFilter.Merge(DAccess.Instance.BillFilter);
            mDataSet.DeriveCompany.Merge(DAccess.Instance.DeriveCompany);
            mDataSet.PayReceiverCompany.Merge(DAccess.Instance.PayReceiverCompany);
            mDataSet.FilterType.Merge(DAccess.Instance.FilterType);

            if (isNewRow && copyRowSource != null)
            {
                MDataSet.ClientDetailRow newRow = (MDataSet.ClientDetailRow)mDataSet.ClientDetail.NewRow();
                newRow.ID_Client_Detail = -1;
                newRow.ID_Client = (int)copyRowSource["ID_Client"];
                newRow.ID_Derive_Company = (int)copyRowSource["ID_Derive_Company"];
                newRow.ID_Bill_Filter = (int)copyRowSource["ID_Bill_Filter"];
                newRow.Begin_Period = DAccess.Instance.WorkPeriod;
                newRow.Advance = (double)copyRowSource["Advance"];
                newRow.RoundTo = (int)copyRowSource["RoundTo"];
                newRow.Prefix = copyRowSource["Prefix"].ToString();
                newRow.Show_Meter_For_Services = copyRowSource["Show_Meter_For_Services"].ToString();
                newRow.ID_Filter_Type = (int)copyRowSource["ID_Filter_Type"];
                newRow.Bill_Name = copyRowSource["Bill_Name"].ToString();
                newRow.Comment = "Фильтр скопирован по шаблону с ID_Client_Detail=" + copyRowSource["ID_Client_Detail"].ToString();
                mDataSet.ClientDetail.AddClientDetailRow(newRow);
            }
            else if (isNewRow && copyRowSource == null)
            {
                MDataSet.ClientDetailRow newRow = (MDataSet.ClientDetailRow)mDataSet.ClientDetail.NewRow();
                newRow.Begin_Period = DAccess.Instance.WorkPeriod;
                newRow.RoundTo = -1;
                //newRow.Prefix = "EK";
                newRow.Advance = 0;
                newRow.ID_Filter_Type = 1;
                mDataSet.ClientDetail.AddClientDetailRow(newRow);
            }
            else
                mDataSet.ClientDetail.ImportRow(activeData);
        }

        public DataRow ActiveData
        {
            get {
                return mDataSet.ClientDetail[0];
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (CheckData())
            {
                Close();
            }
            else
                DialogResult = System.Windows.Forms.DialogResult.None;
        }

        private bool CheckData()
        {
            MDataSet.ClientDetailRow activeData = (MDataSet.ClientDetailRow) ActiveData;
            if (ActiveData["Begin_Period"] == DBNull.Value || ActiveData["ID_Client"] == DBNull.Value
                || ActiveData["ID_Derive_Company"] == DBNull.Value || ActiveData["ID_Bill_Filter"] == DBNull.Value)
            {
                MessageBox.Show("Необходимо заполнить все обязательные поля (*).", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (ActiveData["End_Period"] != DBNull.Value && activeData.Begin_Period > activeData.End_Period)
            {
                MessageBox.Show("Период «Действует ПО» должен быть больше или равен периоду «Действует С».", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void riServiceList_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FrmServiceList f = new FrmServiceList(ActiveData["Show_Meter_For_Services"].ToString());
            if (f.ShowDialog() == DialogResult.OK)
            {
                riServiceList.BeginUpdate();
                ActiveData["Show_Meter_For_Services"] = f.SelectedServices;
                riServiceList.EndUpdate();
            }
        }

        private void riClientPayReceiver_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            mDataSet.CompanyServiceNumber.Clear();
            DAccess.DataModule.CompanyServiceNumberSelect(mDataSet.CompanyServiceNumber, (int)e.NewValue);

            if (mDataSet.CompanyServiceNumber.Rows.Count > 0)
            {
                this.ActiveData["Prefix"] = mDataSet.CompanyServiceNumber.Rows[0]["String_Value"];
            }
            else
            {
                this.ActiveData["Prefix"] = "EK";
            }
        }

        private void riPrefixButton_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FrmCompany f = new FrmCompany(false);
            if (f.ShowDialog() == DialogResult.OK)
            {
                vGridControl1.ActiveEditor.Text = f.SberServiceNumber;
                this.ActiveData["Prefix"] = f.SberServiceNumber;
            }
        }
    }
}
