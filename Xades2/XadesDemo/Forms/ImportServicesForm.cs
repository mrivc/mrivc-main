﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo
{
    public partial class ImportServicesForm : Form
    {
        public string SenderID
        {
            get { return ""; }
        }

        public ImportServicesForm()
        {
            InitializeComponent();
        }

        private void prepareBtn_Click(object sender, EventArgs e)
        {
            var munServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.Prepare_Import_Municipal_Services @senderID", new[,] { { "@senderID", SenderID } });
            var addServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.Prepare_Import_Additional_Services @senderID", new[,] { { "@senderID", SenderID } });

            municipalServicesDataGrid.DataSource = munServices;
            additionalServicesDataGrid.DataSource = addServices;

            importBtn.Enabled = true;
        }

        private void ImportServicesForm_Load(object sender, EventArgs e)
        {

        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            var importMunicipalServices = new ImportMunicipalServices(SenderID);
            importMunicipalServices.Execute();

            var importAdditionalServices = new ImportAdditionalServices(SenderID);
            importAdditionalServices.Execute();
        }

        private void recoverBtn_Click(object sender, EventArgs e)
        {
            var recoverMun = new RecoverMunicipalServices(SenderID);
            recoverMun.Execute();
        }
    }
}
