﻿
using Helper.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class WithdrawPaymentDocumentForm : Form
    {
        public string SenderID
        {
            get { return senderIdOnlyFilter1.EditValue.ToString(); }
        }

        public string FiasHouseGuid
        {
            //get { return cbHouse.SelectedValue.ToString(); }
            get { return customEdit2.EditValue.ToString(); }
        }

        List<string> ListByParameter(string list)
        {
            string[] s1 = list.Split(',');
            List<string> _list = s1.ToList<string>();
            return _list;
        }

        public int Period
        {
            get { return cbPeriod.Period; }
        }

        public WithdrawPaymentDocumentForm()
        {
            InitializeComponent();
        }

        void FillHouseDataSource()
        {

            var SIDpar =
               //"'" + 
               SenderID.Replace(" ", "")
               //+ "'"
               ;

            customEdit2.Properties.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.SelectHouseBySenderID @senderID", new object[,] { { "@senderID", SIDpar } });
            customEdit2.Properties.ValueMember = "FiasHouseGuid";
            customEdit2.Properties.DisplayMember = "House_Address";
        }

        private void WithdrawPaymentDocumentForm_Load(object sender, EventArgs e)
        {
            senderIdOnlyFilter1.Properties.DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("select SenderID,sendername from gis.provider order by 2");
            senderIdOnlyFilter1.Properties.ValueMember = "SenderID";
            senderIdOnlyFilter1.Properties.DisplayMember = "sendername";

            FillHouseDataSource();

            cbPeriod.Fill();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var hg = MrivcData.HouseFiasGuidList(SenderID);
            var houseGuidList = ListByParameter(FiasHouseGuid);
            foreach (var item in houseGuidList)
            {
                    var withdrawBill = new WithdrawPaymentDocument(SenderID, Period);
                    withdrawBill.FiasHouseGuid = item;
                    withdrawBill.Execute();
            }
        }

        private void customEdit2_EditValueChanged(object sender, EventArgs e)
        {
           
        }

        private void senderIdOnlyFilter1_EditValueChanged(object sender, EventArgs e)
        {
            FillHouseDataSource();
        }
    }
}
