﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MrivcControls
{
    using System.IO;

    using DevExpress.Utils;
    using DevExpress.XtraPivotGrid;
    using DevExpress.XtraPivotGrid.Localization;

    using MrivcControls.Data;

    using MrivcHelpers.Forms;
    using MrivcHelpers.Helpers;

    public partial class PivotPrintHistory : UserControl
    {
        private bool initialized = false;

        private const string layoutFolder = "System\\Common\\BillPivotLayouts";

        public PivotPrintHistory()
        {
            InitializeComponent();
            InitBaseLayoutSettings();
        }

        public void LoadData()
        {
            cDataSet.Period.Merge(DAccess.Instance.Periods);
            DAccess.DataModule.PrintHistoryPivotDataSelect(cDataSet.PrintPivotData, DAccess.Instance.WorkPeriod, cbShowAllBill.Checked);

            cbPeriod.EditValue = DAccess.Instance.WorkPeriod;

            FillLayoutList();
            cbLayout.SelectedIndex = 0;
            pivotGridControl1.CollapseAll();
            pivotGridControl1.Fields["bill_filter"].ExpandAll();
            pivotGridControl1.Fields["city"].ExpandAll();

            initialized = true;
        }

        public void ReLoadData()
        {
            if (!initialized) 
                return;

            cDataSet.PrintPivotData.Clear();
            DAccess.DataModule.PrintHistoryPivotDataSelect(cDataSet.PrintPivotData, (int)cbPeriod.EditValue, cbShowAllBill.Checked);

            pivotGridControl1.CollapseAll();
            pivotGridControl1.Fields["bill_filter"].ExpandAll();
            pivotGridControl1.Fields["city"].ExpandAll();
        }

        #region Layout methods
        private void LoadLayout(string layout)
        {
            MemoryStream reader = new MemoryStream(Encoding.UTF8.GetBytes(layout));
            pivotGridControl1.RestoreLayoutFromStream(reader);
            InitBaseLayoutSettings();
        }

        private void InitBaseLayoutSettings()
        {
            foreach (PivotGridFieldBase field in pivotGridControl1.Fields)
            {
                field.Options.AllowFilter = DefaultBoolean.True;
            }

            pivotGridControl1.Groups.Clear();
            AddLayoutFieldsToContextMenu();
        }

        private void AddLayoutFieldsToContextMenu()
        {
            miExpandAllGroups.DropDownItems.Clear();
            miCollapseAllGroups.DropDownItems.Clear();

            ToolStripMenuItem miFieldExpand = new ToolStripMenuItem("Все группы", null, miExpandAllGroups_Click);
            miFieldExpand.Tag = "All";
            miExpandAllGroups.DropDownItems.Add(miFieldExpand);

            ToolStripMenuItem miFieldCollapse = new ToolStripMenuItem("Свернуть группы", null, this.miCollapseAllGroups_Click);
            miFieldCollapse.Tag = "All";
            miCollapseAllGroups.DropDownItems.Add(miFieldCollapse);

            foreach (var field in pivotGridControl1.GetFieldsByArea(PivotArea.RowArea).Where(f => f.Visible))
            {
                miFieldExpand = new ToolStripMenuItem(field.Caption, null, miExpandAllGroups_Click);
                miFieldExpand.Tag = field;
                miExpandAllGroups.DropDownItems.Add(miFieldExpand);

                miFieldCollapse = new ToolStripMenuItem(field.Caption, null, miCollapseAllGroups_Click);
                miFieldCollapse.Tag = field;
                miCollapseAllGroups.DropDownItems.Add(miFieldCollapse);
            }
        }

        private void SaveLayout(string fileName)
        {
            pivotGridControl1.SaveLayoutToXml(fileName);
        }

        private void FillLayoutList()
        {
            cbLayout.Items.Clear();
            List<FileInfo> files = FileHelper.GetDirectoryTree(layoutFolder, "*.xml");
            files.ForEach(f => cbLayout.Items.Add(f.Name.Replace(".xml", string.Empty)));
        }
        #endregion

        private void miExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.RestoreDirectory = true;
            sfd.Filter = "xls files (*.xls)|*.xls|xlsx files (*.xlsx)|*.xlsx";
            sfd.AddExtension = true;
            sfd.DefaultExt = "xls";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if (FileHelper.GetFileExt(sfd.FileName).ToLower() == "xls")
                    pivotGridControl1.ExportToXls(sfd.FileName);
                if (FileHelper.GetFileExt(sfd.FileName).ToLower() == "xlsx")
                    pivotGridControl1.ExportToXlsx(sfd.FileName);
            }
        }

        private void cbPeriod_EditValueChanged(object sender, EventArgs e)
        {
            this.ReLoadData();
        }

        private void cbShowAllBill_CheckStateChanged(object sender, EventArgs e)
        {
            this.ReLoadData();
        }

        private void pivotGridControl1_FieldFilterChanged(object sender, DevExpress.XtraPivotGrid.PivotFieldEventArgs e)
        {
            pivotGridControl1.CollapseAll();
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            this.ReLoadData();
        }

        private void miExpandAllGroups_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (menuItem.Tag.ToString().Equals("All", StringComparison.InvariantCultureIgnoreCase))
            {
                pivotGridControl1.ExpandAll();
                return;
            }

            (menuItem.Tag as PivotGridField).ExpandAll();
        }

        private void miCollapseAllGroups_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (menuItem.Tag.ToString().Equals("All", StringComparison.InvariantCultureIgnoreCase))
            {
                pivotGridControl1.CollapseAll();
                return;
            }

            (menuItem.Tag as PivotGridField).CollapseAll();
        }

        private void miExpandColumnGroups_Click(object sender, EventArgs e)
        {
            pivotGridControl1.Fields["bill_filter"].ExpandAll();
        }

        private void miCollapseColumnGroups_Click(object sender, EventArgs e)
        {
            pivotGridControl1.Fields["bill_filter"].CollapseAll();
        }

        private void cbLayout_SelectedIndexChanged(object sender, EventArgs e)
        {
            string layout = FileHelper.ReadTextFile(string.Format("{0}//{1}.xml", layoutFolder, cbLayout.SelectedItem.ToString()));
            LoadLayout(layout);
            this.InitBaseLayoutSettings();

            if (cbLayout.SelectedItem.ToString().Contains("001"))
            {
                pivotGridControl1.CollapseAll();
                pivotGridControl1.Fields["bill_filter"].ExpandAll();
                pivotGridControl1.Fields["city"].ExpandAll();
            }

            if (cbLayout.SelectedItem.ToString().Contains("002"))
            {
                cbShowAllBill.Checked = false;
                LoadLayout(Properties.Resources.PivotIssueDate);
                pivotGridControl1.CollapseAll();
                pivotGridControl1.Fields["client_name"].ExpandAll();
                pivotGridControl1.Fields["bill_filter"].ExpandAll();
            }

            if (cbLayout.SelectedItem.ToString().Contains("003"))
            {
                cbShowAllBill.Checked = false;
                LoadLayout(Properties.Resources.PivotIssueDateWithoutBillFilter);
                pivotGridControl1.CollapseAll();
                pivotGridControl1.Fields["client_name"].ExpandAll();
                pivotGridControl1.Fields["bill_filter"].ExpandAll();
            }
        }

        private void miSaveLayout_Click(object sender, EventArgs e)
        {
            //this.SaveLayout(string.Format("{0}//{1}.xml", layoutFolder, cbLayout.SelectedItem.ToString()));
            this.SaveLayout("PivotPrintHistoryCurrentLayout.xml");
        }

        private void miSaveNewLayout_Click(object sender, EventArgs e)
        {
            var inputForm = new FrmStringInput("Введите имя отчета");

            if (inputForm.ShowDialog() == DialogResult.OK)
            {
                this.SaveLayout(string.Format("{0}//{1}.xml", layoutFolder, inputForm.InputValue));
                FillLayoutList();
            }
        }

        private void printPivotDataBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void periodBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
    }

    public class CustomDXPivotGridLocalizer : PivotGridLocalizer
    {
        public override string GetLocalizedString(PivotGridStringId id)
        {
            switch (id)
            {
                case PivotGridStringId.GrandTotal:
                    return "ИТОГО";
                case PivotGridStringId.TotalFormat:
                    return "Всего по группе {0}";
                case PivotGridStringId.FilterHeadersCustomization:
                    return "Перетащите сюда колонки для фильтра";
                default:
                    return base.GetLocalizedString(id);
            }
        }
    }
}
