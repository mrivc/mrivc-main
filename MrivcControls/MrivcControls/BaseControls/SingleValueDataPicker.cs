﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MrivcControls.Data;

namespace MrivcControls
{
    public partial class SingleValueDataPicker : UserControl, IDataPicker
    {
        public string ParameterName
        {
            get { return parameterGroupBox.Text; }
            set { parameterGroupBox.Text = value; }
        }

        public object DataSource
        {
            get { return parameterComboBox.DataSource; }
            set { parameterComboBox.DataSource = value; }
        }

        public string ValueMember
        {
            get { return parameterComboBox.ValueMember; }
            set { parameterComboBox.ValueMember = value; }
        }

        public string DisplayMember
        {
            get { return parameterComboBox.DisplayMember; }
            set { parameterComboBox.DisplayMember = value; }
        }

        public object SelectedValue
        {
            get { return parameterComboBox.SelectedValue; }
        }

        public string SelectedName
        {
            get
            {
                return parameterComboBox.SelectedText;
            }
        }

        public SingleValueDataPicker()
        {
            InitializeComponent();
        }
    }
}
