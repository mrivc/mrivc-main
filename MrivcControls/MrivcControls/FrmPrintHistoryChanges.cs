﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MrivcControls
{
    public partial class FrmPrintHistoryChanges : Form
    {
        private long idPrint;

        public FrmPrintHistoryChanges()
        {
            InitializeComponent();
            
        }

        public FrmPrintHistoryChanges(long idPrint)
        {
            this.idPrint = idPrint;
            InitializeComponent();
            //ЫЫЫ РАБОТАЕТ
        }

        private void baseGrid1_Load(object sender, EventArgs e)
        {
            DAccess.DataModule.PrintHistoryLogSelect(cDataSet.Print_History_Log, idPrint);
            cDataSet.Employee.Merge(DAccess.Instance.Employee);
        }

        private void FrmPrintHistoryChanges_Load(object sender, EventArgs e)
        {
            this.Text = "История изменений";
        }
    }
}
