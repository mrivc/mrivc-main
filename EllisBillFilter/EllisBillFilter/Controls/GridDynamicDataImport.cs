﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using DevExpress.XtraGrid.Columns;
using MrivcControls.Helpers;
using MrivcHelpers.Helpers;
using MrivcControls;

namespace EllisBillFilter.Controls
{
    public partial class GridDynamicDataImport : UserControl
    {
        string databaseName = "Hlam";

        private DynamicParameterManager dynamicParameterManager;

        public GridDynamicDataImport()
        {
            InitializeComponent();
            dateCreationPicker.Value = DateTime.Now;
        }

        public DataTable DynamicDataImportTable
        {
            get
            {
                return ddDynamicDataImport.Properties.DataSource as DataTable;
            }
        }

        public DataRow SelectedDynamicQuery
        {
            get
            {
                if (ddDynamicDataImportView.FocusedRowHandle < 0)
                    return null;

                return ddDynamicDataImportView.GetDataRow(ddDynamicDataImportView.FocusedRowHandle);
            }
        }

        public void LoadData()
        {
            gridDataPreview1.LoadData();
            FillDropDownDynamicDataImport();
        }

        private void FillDropDownDynamicDataImport()
        {
            ddDynamicDataImport.EditValue = string.Empty;
            ddDynamicDataImport.Properties.DataSource = null;
            ddDynamicDataImport.Properties.DisplayMember = "Name";
            ddDynamicDataImport.Properties.ValueMember = "Name";
            ddDynamicDataImport.Properties.DataSource = DAccess.DataModule.SelectDynamicDataImport();

            foreach (GridColumn column in ddDynamicDataImportView.Columns)
            {
                if (column.FieldName != "Name")
                    column.Visible = false;
            }
        }

        public string GetSqlDynamicScript()
        {
            if (SelectedDynamicQuery == null)
            {
                return string.Empty;
            }

            string tableName = $"[{databaseName}].{gridDataPreview1.TableName}";
            string creationDate = dateCreationPicker.Value.ShortDateStringForSQL();
            string runInTestMode = cbRunInTestMode.Checked ? "1" : "0";

            string script = SelectedDynamicQuery["SQL"].ToString();
            script = script.Replace("?TableName", tableName);
            script = script.Replace("?CreationDate", $"'{creationDate}'");
            script = script.Replace("?RunInTestMode", $"{runInTestMode}");

            if (dynamicParameterManager != null)
                script = dynamicParameterManager.FillScriptParameters(script);

            return script;
        }

        public void ExcuteDynamicImportScript()
        {
            var checkResult = CheckPreviewData();
            if (!checkResult.IsError)
            {
                var result = DAccess.DataModule.ExecuteScalarQueryCommand(GetSqlDynamicScript());

                if(result == null || result.ToString().Equals("DONE", StringComparison.InvariantCultureIgnoreCase))
                    MessageBox.Show("Операция выполнена успешно.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show($"Ошибка выполнения запроса:{result}", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                checkResult.ShowErrorMessage();
        }

        public ErrorResult CheckPreviewData()
        {
            if (SelectedDynamicQuery == null)
                return new ErrorResult(true, "Необходимо указать запрос для обработки данных.");

            if (string.IsNullOrEmpty(gridDataPreview1.TableName) || gridDataPreview1.TableData == null)
                return new ErrorResult(true, "Необходимо выбрать таблицу данных.");

            if (SelectedDynamicQuery["Required_Fields"] != DBNull.Value)
            {
                string []columns = SelectedDynamicQuery["Required_Fields"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                List<DataColumn> tableColumns = gridDataPreview1.TableData.Columns.OfType<DataColumn>().ToList();
                string missedColumns = Environment.NewLine;
                foreach (var col in columns)
                {
                    if (tableColumns.Find(c => c.ColumnName.Equals(col.Trim(), StringComparison.InvariantCultureIgnoreCase)) == null)
                        missedColumns += "*" + col + Environment.NewLine;
                }

                if (missedColumns.Trim().Length > 0)
                    return new ErrorResult(true, "В таблице данных пропущены обязательные колонки:" + missedColumns);
            }

            return new ErrorResult(false);
        }

        private void ddDynamicDataImport_EditValueChanged(object sender, EventArgs e)
        {
            DataRow row = SelectedDynamicQuery;

            if (dynamicParameterManager != null)
                splitContainer2.Panel1.Controls.RemoveByKey(dynamicParameterManager.ID);
            
            if (row["Parameters"] != DBNull.Value)
            {
                dynamicParameterManager = new DynamicParameterManager($"ParameterPanel{row["ID"].ToString()}", row["Parameters"].ToString());
                dynamicParameterManager.ParametersPanelControlsHolder.Location = new Point(9, 102);
                splitContainer2.Panel1.Controls.Add(dynamicParameterManager.ParametersPanelControlsHolder);
            }

            txtNotes.Text = "Обязательные поля: ?Required_Fields" + Environment.NewLine + Environment.NewLine;
            txtNotes.Text += "Описание запроса: ?Notes";

            if (row != null && row["Required_Fields"] != DBNull.Value)
                txtNotes.Text = txtNotes.Text.Replace("?Required_Fields", row["Required_Fields"].ToString());

            if (row != null && row["Notes"] != DBNull.Value)
                txtNotes.Text = txtNotes.Text.Replace("?Notes", row["Notes"].ToString());
        }

        private void btnDataCheck_Click(object sender, EventArgs e)
        {
            var result = CheckPreviewData();
            result.ShowErrorMessage(true);
        }

        private void btnShowScript_Click(object sender, EventArgs e)
        {
            string script = GetSqlDynamicScript();
            FrmShowBigString.Show("Скрипт", script);
        }
    }
}
