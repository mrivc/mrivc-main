﻿using System;
using System.Windows.Forms;
using EllisBillFilter.DS;

namespace EllisBillFilter
{
    public partial class FrmPrintHistory : Form
    {
        public FrmPrintHistory()
        {
            InitializeComponent();
            this.Text = this.Text + (" (" + MrivcControls.Data.DAccess.DataModule.Connection.DataSource + " - " + MrivcControls.Data.DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
        }

        private void FrmPrintHistory_Load(object sender, EventArgs e)
        {
            MrivcControls.Data.DAccess.DataModule.SetConnection(DAccess.DataModule.ConnectionString);
            gridPrintHistory1.LoadData();
        }

        private void lblButtonFind_Click(object sender, EventArgs e)
        {
        }
    }
}
