﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Payments.DS;

namespace Payments.Helpers
{
    public static class RegistryVerificationHelper
    {
        /// <summary>
        /// Загрузка реестра сальдо из файла
        /// </summary>
        /// <param name="idClient">Организация по которой прислан реестр</param>
        /// <param name="fileName">Полный путь к фалу</param>
        /// <returns>Возвращает ИД созданного реестра</returns>
        public static int LoadToDbFromFile(int idClient, string fileName)
        {
            string sql = string.Empty;

            if (!FileHelper.ExistFile(fileName))
                return -1;

            int id = DAccess.DataModule.RegistryVerificationCreate(idClient, fileName, "", "", null);

            string[] lines = File.ReadAllLines(fileName, Encoding.Default);
            foreach (string line in lines)
            {
                string[] s = line.Split(new Char[] {';', ':'});
                if (s[0].StartsWith("#") || s[0].StartsWith("SIGN") || s[0].StartsWith("<")) continue;

                decimal sumPay = 0;
                decimal.TryParse(s[3].Replace(",", "."), out sumPay);

                DAccess.DataModule.RegistryVerificationLineInsert(id, idClient, s[1], s[2], sumPay, 0, s[8], line);
            }
            return id;
        }

        public static void SetLoadedStatus(int idRegistryVerification, bool loaded)
        {
            if (idRegistryVerification == -1)
                return;

            DAccess.DataModule.RegistryVerificationSetLoadedStatus(idRegistryVerification, loaded);
        }
    }
}