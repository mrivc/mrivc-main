﻿namespace Helper.Controls
{
    partial class FiasHouseGuidFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbHouse = new System.Windows.Forms.ComboBox();
            this.cbAllHouse = new System.Windows.Forms.CheckBox();
            this.gbHouse = new System.Windows.Forms.GroupBox();
            this.gbHouse.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbHouse
            // 
            this.cbHouse.Enabled = false;
            this.cbHouse.FormattingEnabled = true;
            this.cbHouse.Location = new System.Drawing.Point(57, 17);
            this.cbHouse.Name = "cbHouse";
            this.cbHouse.Size = new System.Drawing.Size(399, 21);
            this.cbHouse.TabIndex = 3;
            this.cbHouse.SelectedIndexChanged += new System.EventHandler(this.cbHouse_SelectedIndexChanged);
            // 
            // cbAllHouse
            // 
            this.cbAllHouse.AutoSize = true;
            this.cbAllHouse.Checked = true;
            this.cbAllHouse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAllHouse.Location = new System.Drawing.Point(6, 19);
            this.cbAllHouse.Name = "cbAllHouse";
            this.cbAllHouse.Size = new System.Drawing.Size(45, 17);
            this.cbAllHouse.TabIndex = 0;
            this.cbAllHouse.Text = "Все";
            this.cbAllHouse.UseVisualStyleBackColor = true;
            this.cbAllHouse.CheckedChanged += new System.EventHandler(this.cbAllHouse_CheckedChanged);
            // 
            // gbHouse
            // 
            this.gbHouse.Controls.Add(this.cbHouse);
            this.gbHouse.Controls.Add(this.cbAllHouse);
            this.gbHouse.Location = new System.Drawing.Point(3, 3);
            this.gbHouse.Name = "gbHouse";
            this.gbHouse.Size = new System.Drawing.Size(462, 51);
            this.gbHouse.TabIndex = 3;
            this.gbHouse.TabStop = false;
            this.gbHouse.Text = "Адрес дома";
            this.gbHouse.Enter += new System.EventHandler(this.gbHouse_Enter);
            // 
            // FiasHouseGuidFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbHouse);
            this.Name = "FiasHouseGuidFilter";
            this.Size = new System.Drawing.Size(468, 59);
            this.gbHouse.ResumeLayout(false);
            this.gbHouse.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbHouse;
        private System.Windows.Forms.CheckBox cbAllHouse;
        private System.Windows.Forms.GroupBox gbHouse;
    }
}
