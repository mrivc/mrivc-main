﻿namespace EllisBillFilter
{
    partial class GridClientDetailDeliverySettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.riClient = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.clientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.riBtnSelectEmails = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.riDays = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.gridClientDetailDeliverySettings1 = new EllisBillFilter.BaseGrid();
            this.contextMenuMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAddRecord = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeleteRecord = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miSendToday = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.clientDetailDeliverySettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewClientDetailDeliverySettings1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riClient1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colID_Bill_Filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riBillFilter = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.billFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colIs_Debt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail_IDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailsText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riBtnSelectEmails2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLast_Send = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLast_Send_By = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailsAddresses = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSendToday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Delivery = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnGenerateAndSend = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPeriod = new System.Windows.Forms.ComboBox();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnLog = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.riClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBtnSelectEmails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridClientDetailDeliverySettings1)).BeginInit();
            this.contextMenuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailDeliverySettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientDetailDeliverySettings1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riClient1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBtnSelectEmails2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // riClient
            // 
            this.riClient.AutoHeight = false;
            this.riClient.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riClient.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Company", "ID_Company", 85, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riClient.DataSource = this.clientBindingSource;
            this.riClient.DisplayMember = "Name";
            this.riClient.DropDownRows = 20;
            this.riClient.Name = "riClient";
            this.riClient.NullText = "";
            this.riClient.ValueMember = "ID_Company";
            // 
            // clientBindingSource
            // 
            this.clientBindingSource.DataMember = "Client";
            this.clientBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // riBtnSelectEmails
            // 
            this.riBtnSelectEmails.AutoHeight = false;
            this.riBtnSelectEmails.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.riBtnSelectEmails.Name = "riBtnSelectEmails";
            this.riBtnSelectEmails.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // riDays
            // 
            this.riDays.AutoHeight = false;
            this.riDays.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riDays.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("28", "28"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("29", "29"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("30", "30"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("31", "31"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("1", "1"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("2", "2"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("3", "3"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("4", "4"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("5", "5"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("6", "6"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("7", "7"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("8", "8"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("9", "9"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("10", "10"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("11", "11"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("12", "12")});
            this.riDays.Name = "riDays";
            // 
            // gridClientDetailDeliverySettings1
            // 
            this.gridClientDetailDeliverySettings1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridClientDetailDeliverySettings1.ContextMenuStrip = this.contextMenuMain;
            this.gridClientDetailDeliverySettings1.DataSource = this.clientDetailDeliverySettingsBindingSource;
            this.gridClientDetailDeliverySettings1.Location = new System.Drawing.Point(0, 34);
            this.gridClientDetailDeliverySettings1.MainView = this.gridViewClientDetailDeliverySettings1;
            this.gridClientDetailDeliverySettings1.Name = "gridClientDetailDeliverySettings1";
            this.gridClientDetailDeliverySettings1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riClient1,
            this.riBillFilter,
            this.riBtnSelectEmails2});
            this.gridClientDetailDeliverySettings1.Size = new System.Drawing.Size(1099, 320);
            this.gridClientDetailDeliverySettings1.TabIndex = 0;
            this.gridClientDetailDeliverySettings1.TrimCellValueBeforeValidate = true;
            this.gridClientDetailDeliverySettings1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewClientDetailDeliverySettings1});
            this.gridClientDetailDeliverySettings1.SaveData += new EllisBillFilter.BaseGrid.SaveDataEventHandler(this.gridClientDetailDeliverySettings1_SaveData);
            // 
            // contextMenuMain
            // 
            this.contextMenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAddRecord,
            this.miDeleteRecord,
            this.toolStripSeparator2,
            this.miSendToday,
            this.toolStripSeparator1,
            this.miRefresh});
            this.contextMenuMain.Name = "contextMenuMain";
            this.contextMenuMain.Size = new System.Drawing.Size(172, 104);
            // 
            // miAddRecord
            // 
            this.miAddRecord.Image = global::EllisBillFilter.Properties.Resources.record_add;
            this.miAddRecord.Name = "miAddRecord";
            this.miAddRecord.Size = new System.Drawing.Size(171, 22);
            this.miAddRecord.Text = "Добавить запись";
            this.miAddRecord.Click += new System.EventHandler(this.miAddRecord_Click);
            // 
            // miDeleteRecord
            // 
            this.miDeleteRecord.Image = global::EllisBillFilter.Properties.Resources.record_delete;
            this.miDeleteRecord.Name = "miDeleteRecord";
            this.miDeleteRecord.Size = new System.Drawing.Size(171, 22);
            this.miDeleteRecord.Text = "Удалить запись";
            this.miDeleteRecord.Click += new System.EventHandler(this.miDeleteRecord_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(168, 6);
            // 
            // miSendToday
            // 
            this.miSendToday.Name = "miSendToday";
            this.miSendToday.Size = new System.Drawing.Size(171, 22);
            this.miSendToday.Text = "Отослать сегодня";
            this.miSendToday.Click += new System.EventHandler(this.miSendToday_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(168, 6);
            // 
            // miRefresh
            // 
            this.miRefresh.Image = global::EllisBillFilter.Properties.Resources.refresh;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(171, 22);
            this.miRefresh.Text = "Обновить";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // clientDetailDeliverySettingsBindingSource
            // 
            this.clientDetailDeliverySettingsBindingSource.DataMember = "ClientDetailDeliverySettings";
            this.clientDetailDeliverySettingsBindingSource.DataSource = this.mDataSet;
            // 
            // gridViewClientDetailDeliverySettings1
            // 
            this.gridViewClientDetailDeliverySettings1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewClientDetailDeliverySettings1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewClientDetailDeliverySettings1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewClientDetailDeliverySettings1.ColumnPanelRowHeight = 40;
            this.gridViewClientDetailDeliverySettings1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colChecked,
            this.colID_Client,
            this.colID_Bill_Filter,
            this.colIs_Debt,
            this.colEmail_IDs,
            this.colEmailsText,
            this.colDays,
            this.colLast_Send,
            this.colLast_Send_By,
            this.colCreatedBy,
            this.colCreationDate,
            this.colEmailsAddresses,
            this.colSendToday,
            this.colID_Delivery});
            this.gridViewClientDetailDeliverySettings1.GridControl = this.gridClientDetailDeliverySettings1;
            this.gridViewClientDetailDeliverySettings1.Name = "gridViewClientDetailDeliverySettings1";
            this.gridViewClientDetailDeliverySettings1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewClientDetailDeliverySettings1.OptionsView.ColumnAutoWidth = false;
            this.gridViewClientDetailDeliverySettings1.OptionsView.ShowGroupPanel = false;
            this.gridViewClientDetailDeliverySettings1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewClientDetailDeliverySettings1_RowStyle);
            this.gridViewClientDetailDeliverySettings1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewClientDetailDeliverySettings1_InitNewRow);
            // 
            // colChecked
            // 
            this.colChecked.Caption = "Д";
            this.colChecked.FieldName = "Checked";
            this.colChecked.Name = "colChecked";
            this.colChecked.OptionsColumn.FixedWidth = true;
            this.colChecked.Visible = true;
            this.colChecked.VisibleIndex = 0;
            this.colChecked.Width = 32;
            // 
            // colID_Client
            // 
            this.colID_Client.Caption = "Клиент";
            this.colID_Client.ColumnEdit = this.riClient1;
            this.colID_Client.FieldName = "ID_Client";
            this.colID_Client.Name = "colID_Client";
            this.colID_Client.Visible = true;
            this.colID_Client.VisibleIndex = 1;
            this.colID_Client.Width = 280;
            // 
            // riClient1
            // 
            this.riClient1.AutoHeight = false;
            this.riClient1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riClient1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Company", "ID_Company", 85, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riClient1.DataSource = this.clientBindingSource;
            this.riClient1.DisplayMember = "Name";
            this.riClient1.DropDownRows = 14;
            this.riClient1.Name = "riClient1";
            this.riClient1.NullText = "";
            this.riClient1.ShowFooter = false;
            this.riClient1.ShowHeader = false;
            this.riClient1.ValueMember = "ID_Company";
            // 
            // colID_Bill_Filter
            // 
            this.colID_Bill_Filter.Caption = "Код фильтра";
            this.colID_Bill_Filter.ColumnEdit = this.riBillFilter;
            this.colID_Bill_Filter.FieldName = "ID_Bill_Filter";
            this.colID_Bill_Filter.Name = "colID_Bill_Filter";
            this.colID_Bill_Filter.Visible = true;
            this.colID_Bill_Filter.VisibleIndex = 2;
            // 
            // riBillFilter
            // 
            this.riBillFilter.AutoHeight = false;
            this.riBillFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riBillFilter.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Bill_Filter", "ID_Bill_Filter", 82, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Bill_Filter", "Bill_Filter", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tag", "Tag", 28, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "Info", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("IsActivate", "Is Activate", 62, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Account_Type", "Account_Type", 79, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Position", "Position", 47, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far)});
            this.riBillFilter.DataSource = this.billFilterBindingSource;
            this.riBillFilter.DisplayMember = "Bill_Filter";
            this.riBillFilter.Name = "riBillFilter";
            this.riBillFilter.ShowHeader = false;
            this.riBillFilter.ValueMember = "ID_Bill_Filter";
            // 
            // billFilterBindingSource
            // 
            this.billFilterBindingSource.DataMember = "BillFilter";
            this.billFilterBindingSource.DataSource = this.mDataSet;
            // 
            // colIs_Debt
            // 
            this.colIs_Debt.Caption = "Долговой";
            this.colIs_Debt.FieldName = "Is_Debt";
            this.colIs_Debt.Name = "colIs_Debt";
            this.colIs_Debt.Visible = true;
            this.colIs_Debt.VisibleIndex = 3;
            // 
            // colEmail_IDs
            // 
            this.colEmail_IDs.Caption = "Адресаты (ИД)";
            this.colEmail_IDs.FieldName = "Email_IDs";
            this.colEmail_IDs.Name = "colEmail_IDs";
            this.colEmail_IDs.OptionsColumn.AllowEdit = false;
            this.colEmail_IDs.Width = 275;
            // 
            // colEmailsText
            // 
            this.colEmailsText.Caption = "Адресаты (Наименования)";
            this.colEmailsText.ColumnEdit = this.riBtnSelectEmails2;
            this.colEmailsText.FieldName = "EmailsText";
            this.colEmailsText.Name = "colEmailsText";
            this.colEmailsText.Visible = true;
            this.colEmailsText.VisibleIndex = 4;
            this.colEmailsText.Width = 277;
            // 
            // riBtnSelectEmails2
            // 
            this.riBtnSelectEmails2.AutoHeight = false;
            this.riBtnSelectEmails2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.riBtnSelectEmails2.Name = "riBtnSelectEmails2";
            this.riBtnSelectEmails2.ReadOnly = true;
            this.riBtnSelectEmails2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.riBtnSelectEmails2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.riBtnSelectEmails_ButtonClick);
            // 
            // colDays
            // 
            this.colDays.Caption = "Дни авторассылки";
            this.colDays.ColumnEdit = this.riDays;
            this.colDays.FieldName = "Days";
            this.colDays.Name = "colDays";
            this.colDays.OptionsColumn.FixedWidth = true;
            this.colDays.Visible = true;
            this.colDays.VisibleIndex = 5;
            this.colDays.Width = 99;
            // 
            // colLast_Send
            // 
            this.colLast_Send.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colLast_Send.AppearanceCell.Options.UseBackColor = true;
            this.colLast_Send.Caption = "Дата последней отправки";
            this.colLast_Send.DisplayFormat.FormatString = "d/MM/yyyy hh:mm tt";
            this.colLast_Send.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colLast_Send.FieldName = "Last_Send";
            this.colLast_Send.Name = "colLast_Send";
            this.colLast_Send.OptionsColumn.AllowEdit = false;
            this.colLast_Send.OptionsColumn.ReadOnly = true;
            this.colLast_Send.Visible = true;
            this.colLast_Send.VisibleIndex = 6;
            this.colLast_Send.Width = 146;
            // 
            // colLast_Send_By
            // 
            this.colLast_Send_By.FieldName = "Last_Send_By";
            this.colLast_Send_By.Name = "colLast_Send_By";
            this.colLast_Send_By.Width = 195;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.Width = 195;
            // 
            // colCreationDate
            // 
            this.colCreationDate.FieldName = "CreationDate";
            this.colCreationDate.Name = "colCreationDate";
            this.colCreationDate.Width = 208;
            // 
            // colEmailsAddresses
            // 
            this.colEmailsAddresses.Caption = "Адресаты (E-mails)";
            this.colEmailsAddresses.FieldName = "EmailsAddresses";
            this.colEmailsAddresses.Name = "colEmailsAddresses";
            // 
            // colSendToday
            // 
            this.colSendToday.Caption = "Отослать сегодня";
            this.colSendToday.FieldName = "SendToday";
            this.colSendToday.Name = "colSendToday";
            // 
            // colID_Delivery
            // 
            this.colID_Delivery.FieldName = "ID_Delivery";
            this.colID_Delivery.Name = "colID_Delivery";
            // 
            // btnGenerateAndSend
            // 
            this.btnGenerateAndSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGenerateAndSend.Location = new System.Drawing.Point(5, 361);
            this.btnGenerateAndSend.Name = "btnGenerateAndSend";
            this.btnGenerateAndSend.Size = new System.Drawing.Size(180, 36);
            this.btnGenerateAndSend.TabIndex = 1;
            this.btnGenerateAndSend.Text = "Сформировать и отослать реестры сальдо";
            this.btnGenerateAndSend.UseVisualStyleBackColor = true;
            this.btnGenerateAndSend.Click += new System.EventHandler(this.btnGenerateAndSend_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(841, 366);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(197, 23);
            this.progressBar1.TabIndex = 2;
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(110, 8);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(256, 20);
            this.txtPath.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Путь к выгрузке:";
            // 
            // cbPeriod
            // 
            this.cbPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPeriod.BackColor = System.Drawing.Color.White;
            this.cbPeriod.DataSource = this.periodBindingSource;
            this.cbPeriod.DisplayMember = "Name";
            this.cbPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbPeriod.FormattingEnabled = true;
            this.cbPeriod.Location = new System.Drawing.Point(943, 8);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Size = new System.Drawing.Size(144, 21);
            this.cbPeriod.TabIndex = 5;
            this.cbPeriod.ValueMember = "Period";
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // btnLog
            // 
            this.btnLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLog.Location = new System.Drawing.Point(1044, 366);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(47, 23);
            this.btnLog.TabIndex = 6;
            this.btnLog.Text = "Log";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // GridClientDetailDeliverySettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.cbPeriod);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnGenerateAndSend);
            this.Controls.Add(this.gridClientDetailDeliverySettings1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "GridClientDetailDeliverySettings";
            this.Size = new System.Drawing.Size(1099, 404);
            ((System.ComponentModel.ISupportInitialize)(this.riClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBtnSelectEmails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridClientDetailDeliverySettings1)).EndInit();
            this.contextMenuMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailDeliverySettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientDetailDeliverySettings1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riClient1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBtnSelectEmails2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseGrid gridClientDetailDeliverySettings1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewClientDetailDeliverySettings1;
        private System.Windows.Forms.BindingSource clientDetailDeliverySettingsBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail_IDs;
        private DevExpress.XtraGrid.Columns.GridColumn colDays;
        private DevExpress.XtraGrid.Columns.GridColumn colLast_Send;
        private DevExpress.XtraGrid.Columns.GridColumn colLast_Send_By;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colCreationDate;
        private System.Windows.Forms.BindingSource clientBindingSource;
        private System.Windows.Forms.ContextMenuStrip contextMenuMain;
        private System.Windows.Forms.ToolStripMenuItem miAddRecord;
        private System.Windows.Forms.ToolStripMenuItem miDeleteRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailsText;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailsAddresses;
        private DevExpress.XtraGrid.Columns.GridColumn colChecked;
        private System.Windows.Forms.ToolStripMenuItem miRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miSendToday;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private DevExpress.XtraGrid.Columns.GridColumn colSendToday;
        private System.Windows.Forms.Button btnGenerateAndSend;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riClient;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit riBtnSelectEmails;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit riDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riClient1;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Delivery;
        private System.Windows.Forms.Button btnLog;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Bill_Filter;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riBillFilter;
        private System.Windows.Forms.BindingSource billFilterBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit riBtnSelectEmails2;
        private DevExpress.XtraGrid.Columns.GridColumn colIs_Debt;
    }
}
