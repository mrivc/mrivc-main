﻿namespace GisGkh
{
    partial class WithdrawPaymentDocumentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WithdrawPaymentDocumentForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbAllHouse = new System.Windows.Forms.CheckBox();
            this.customEdit2 = new MassBillNew.CustomEdit();
            this.gbSender = new System.Windows.Forms.GroupBox();
            this.senderIdOnlyFilter1 = new MassBillNew.CustomEdit();
            this.cbPeriod = new Helper.Controls.PeriodFilter();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit2.Properties)).BeginInit();
            this.gbSender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.senderIdOnlyFilter1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(1, 195);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(922, 307);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(914, 281);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Основные услуги";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(908, 275);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(914, 281);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Капитальный ремонт";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(691, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Отозвать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.customEdit2);
            this.groupBox1.Controls.Add(this.cbAllHouse);
            this.groupBox1.Location = new System.Drawing.Point(12, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(322, 83);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Адрес дома";
            // 
            // cbAllHouse
            // 
            this.cbAllHouse.AutoSize = true;
            this.cbAllHouse.Checked = true;
            this.cbAllHouse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAllHouse.Location = new System.Drawing.Point(12, 17);
            this.cbAllHouse.Name = "cbAllHouse";
            this.cbAllHouse.Size = new System.Drawing.Size(45, 17);
            this.cbAllHouse.TabIndex = 0;
            this.cbAllHouse.Text = "Все";
            this.cbAllHouse.UseVisualStyleBackColor = true;
            // 
            // customEdit2
            // 
            this.customEdit2.EditValue = "";
            this.customEdit2.Location = new System.Drawing.Point(12, 39);
            this.customEdit2.Name = "customEdit2";
            this.customEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.customEdit2.Properties.IncrementalSearch = true;
            this.customEdit2.Properties.PopupFormMinSize = new System.Drawing.Size(400, 300);
            this.customEdit2.Properties.PopupFormSize = new System.Drawing.Size(400, 300);
            this.customEdit2.Properties.ShowButtons = false;
            this.customEdit2.Properties.ShowPopupCloseButton = false;
            this.customEdit2.Size = new System.Drawing.Size(304, 20);
            this.customEdit2.TabIndex = 14;
            this.customEdit2.EditValueChanged += new System.EventHandler(this.customEdit2_EditValueChanged);
            // 
            // gbSender
            // 
            this.gbSender.Controls.Add(this.senderIdOnlyFilter1);
            this.gbSender.Location = new System.Drawing.Point(12, 6);
            this.gbSender.Name = "gbSender";
            this.gbSender.Size = new System.Drawing.Size(322, 61);
            this.gbSender.TabIndex = 13;
            this.gbSender.TabStop = false;
            this.gbSender.Text = "Поставщик информации";
            // 
            // senderIdOnlyFilter1
            // 
            this.senderIdOnlyFilter1.EditValue = "";
            this.senderIdOnlyFilter1.Location = new System.Drawing.Point(12, 28);
            this.senderIdOnlyFilter1.Name = "senderIdOnlyFilter1";
            this.senderIdOnlyFilter1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.senderIdOnlyFilter1.Properties.IncrementalSearch = true;
            this.senderIdOnlyFilter1.Properties.PopupFormMinSize = new System.Drawing.Size(400, 300);
            this.senderIdOnlyFilter1.Properties.PopupFormSize = new System.Drawing.Size(400, 300);
            this.senderIdOnlyFilter1.Properties.ShowButtons = false;
            this.senderIdOnlyFilter1.Properties.ShowPopupCloseButton = false;
            this.senderIdOnlyFilter1.Size = new System.Drawing.Size(304, 20);
            this.senderIdOnlyFilter1.TabIndex = 13;
            this.senderIdOnlyFilter1.EditValueChanged += new System.EventHandler(this.senderIdOnlyFilter1_EditValueChanged);
            // 
            // cbPeriod
            // 
            this.cbPeriod.Location = new System.Drawing.Point(406, 6);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Size = new System.Drawing.Size(279, 61);
            this.cbPeriod.TabIndex = 4;
            // 
            // WithdrawPaymentDocumentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 501);
            this.Controls.Add(this.gbSender);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbPeriod);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WithdrawPaymentDocumentForm";
            this.Text = "Отозвать платежные документы";
            this.Load += new System.EventHandler(this.WithdrawPaymentDocumentForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit2.Properties)).EndInit();
            this.gbSender.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.senderIdOnlyFilter1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private Helper.Controls.PeriodFilter cbPeriod;
        private System.Windows.Forms.GroupBox groupBox1;
        private MassBillNew.CustomEdit customEdit2;
        private System.Windows.Forms.CheckBox cbAllHouse;
        private System.Windows.Forms.GroupBox gbSender;
        private MassBillNew.CustomEdit senderIdOnlyFilter1;
    }
}