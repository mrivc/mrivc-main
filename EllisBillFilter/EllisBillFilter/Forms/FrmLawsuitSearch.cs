﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter.Forms
{
    using DS;
    using Lawsuit;
    using System.Text.RegularExpressions;

    public partial class FrmLawsuitSearch : Form
    {
        private DataTable searchResult;

        public FrmLawsuitSearch()
        {
            InitializeComponent();

            try
            {
                init();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void refresh()
        {
            mDataSet.LawsuitSearch.Clear();

            searchResult = mDataSet.LawsuitSearch.Copy();

            DAccess.DataModule.FillLawsuitSearchTable(mDataSet.LawsuitSearch);

            search(new SearchRatio[] {
                new SearchRatio("case_number", $@"\w*{caseTextBox.Text}\w*"),
                new SearchRatio("FIO", $@"\w*{fullnameTextBox.Text}\w*")
            });

            dataGridView.DataSource = searchResult;
        }

        private void dataGridInit()
        {
            dataGridView.Columns["ID_Account"].Visible = false;
            dataGridView.Columns["ID_Lawsuit"].Visible = false;

            dataGridView.Columns["case_number"].HeaderText = "Номер дела";
            dataGridView.Columns["list_number"].HeaderText = "Исполнительный лист";
            dataGridView.Columns["stady"].HeaderText = "Стадия рассмотрения";
            dataGridView.Columns["FIO"].HeaderText = "ФИО";
            dataGridView.Columns["ratio"].HeaderText = "Доля";
            dataGridView.Columns["company"].HeaderText = "Управляющая компания";
            dataGridView.Columns["Date_Debt_Begin"].HeaderText = "Начало периода взыскания";
            dataGridView.Columns["Date_Debt_End"].HeaderText = "Окончание периода взыскания";
            dataGridView.Columns["Sum"].HeaderText = "Сумма иска";
            dataGridView.Columns["Sum_Fine"].HeaderText = "Пени";
            dataGridView.Columns["Legal_Costs"].HeaderText = "Гос. пошлина";
            dataGridView.Columns["Date_Proceedings_Begin"].HeaderText = "Дата принятия СП";
            dataGridView.Columns["Date_Proceedings_End"].HeaderText = "Дата отмены СП";
            dataGridView.Columns["court"].HeaderText = "Судебный орган";

            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void init()
        {
            ContextMenuStrip cMenuStrip = new ContextMenuStrip();

            ToolStripMenuItem updateRow = new ToolStripMenuItem("Редактировать");

            cMenuStrip.Items.AddRange( new[] { updateRow });

            dataGridView.ContextMenuStrip = cMenuStrip;

            updateRow.Click += UpdateMenu_Click;

            refresh();

            dataGridInit();
        }

        private void search(SearchRatio[] searchRatios)
        {
            DataTable temp = searchResult.Clone();
            dataGridView.DataSource = temp;

            searchResult.Clear();

            foreach (DataRow row in mDataSet.LawsuitSearch.Rows)
            {
                bool isMatch = true;

                foreach (SearchRatio searchRatio in searchRatios)
                {
                    string rowText = row[searchRatio.Column].ToString();

                    if (!Regex.IsMatch(rowText, searchRatio.Pattern, RegexOptions.IgnoreCase))
                    {
                        isMatch = false;
                    }
                }

                if (isMatch) searchResult.Rows.Add(row.ItemArray);
            }

            dataGridView.DataSource = searchResult;
        }

        private void UpdateMenu_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedIndex = dataGridView.SelectedRows[0].Index;

                DataRow selectedRow = searchResult.Rows[selectedIndex];

                int idAccount = int.Parse(selectedRow["ID_Account"].ToString());
                int idLawsuit = int.Parse(selectedRow["ID_Lawsuit"].ToString());

                Form editForm = new FrmLawsuitEdit(idAccount, DAccess.Instance.WorkPeriod, idLawsuit);
                editForm.ShowDialog();

                refresh();

                dataGridView.Rows[selectedIndex].Selected = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                search(new SearchRatio[] {
                    new SearchRatio("case_number", $@"\w*{caseTextBox.Text}\w*"),
                    new SearchRatio("FIO", $@"\w*{fullnameTextBox.Text}\w*")
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            caseTextBox.Text = "";
            fullnameTextBox.Text = "";
        }
    }
}
