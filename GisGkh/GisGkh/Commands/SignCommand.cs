﻿using System;
using System.Collections.Generic;
using System.IO;
using Xades.Abstractions;
using Xades.Helpers;
using GisGkh.Configurations.Options;
using GisGkh.Configurations.Sections;

namespace GisGkh.Commands
{
    public class SignCommand : XadesCommandBase<SignOptions>
    {
        public SignCommand(SignOptions option, IXadesService xadesService, SigningConfiguration signingConfig) : base(option, xadesService, signingConfig)
        {
        }

        protected override string OnExecute(SignOptions option, string xmlBody, string senderID)
        {
            Info(string.Format("Выпоняется чтение файла {0}...", option.InputFileName));
            var xmlDocument = XmlDocumentHelper.Load(option.InputFileName);
            var elementId = Option.Element;

            Info("Выполняется подпись файла...");
            var resultXmlText = Sign(xmlDocument, elementId);
            File.WriteAllText(option.OutputFileName, resultXmlText);
            Success("Файл успешно подписан");
            return "";
        }
    }
}