﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helper
{
    public class FileHelper
    {
        private static string FileLogPath
        {
            get
            {
                return Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Log\\NLog.txt");
            }
        }

        public static string CreateCorrectFileName(string fileName)
        {
            fileName = fileName.Trim();

            string[] symbols = new string[] { "/", "\\", ":", "*", "?", "\"", "<", ">", "|", "«", "»" };

            foreach (var symbol in symbols)
                fileName = fileName.Replace(symbol, "");

            return fileName;
        }

        public static void WriteLog(string message)
        {
            WriteTextFile(FileLogPath, ">>>" + DateTime.Now.ToString() + " - " + message + Environment.NewLine, Encoding.Default, true);
        }

        public static void WriteTextFile(string fileName, string fileContent)
        {
            CreateDirectory(GetFilePath(fileName));
            StreamWriter _sr = new StreamWriter(fileName, false, Encoding.Default);
            _sr.Write(fileContent);
            _sr.Close();
        }

        public static void WriteTextFile(string fileName, string fileContent, Encoding encoding, bool append)
        {
            CreateDirectory(GetFilePath(fileName));
            StreamWriter _sr = new StreamWriter(fileName, append, encoding);
            _sr.Write(fileContent);
            _sr.Close();
        }

        public static string CreateDirectory(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);
            return directoryPath;
        }

        public static string GetFilePath(string filePath)
        {
            return Path.GetDirectoryName(filePath) + "\\";
        }
    }
}
