﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadingMeasureFromFile
{
    public class EmptyFieldException : Exception
    {
        private string _message;

        public EmptyFieldException(string field)
        {
            _message = $"Пустое поле '{field}'";
        }

        public override string Message
        {
            get
            {
                return _message;
            }
        }
    }
}
