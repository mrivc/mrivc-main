﻿namespace EllisBillFilter
{
    partial class FrmCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridCompany = new EllisBillFilter.BaseGrid();
            this.payReceiverCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gridViewCompany = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIs_Provider = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIs_Derive_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIs_Pay_Receiver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMeter_Future_Period_Day = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoad_Measure_In_Future = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtFilterName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.colSberServiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCompany)).BeginInit();
            this.SuspendLayout();
            // 
            // gridCompany
            // 
            this.gridCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridCompany.DataSource = this.payReceiverCompanyBindingSource;
            this.gridCompany.Location = new System.Drawing.Point(0, 31);
            this.gridCompany.MainView = this.gridViewCompany;
            this.gridCompany.Name = "gridCompany";
            this.gridCompany.Size = new System.Drawing.Size(549, 560);
            this.gridCompany.TabIndex = 1;
            this.gridCompany.TrimCellValueBeforeValidate = true;
            this.gridCompany.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCompany});
            this.gridCompany.DoubleClick += new System.EventHandler(this.gridCompany_DoubleClick);
            // 
            // payReceiverCompanyBindingSource
            // 
            this.payReceiverCompanyBindingSource.DataMember = "PayReceiverCompany";
            this.payReceiverCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewCompany
            // 
            this.gridViewCompany.ColumnPanelRowHeight = 40;
            this.gridViewCompany.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Company,
            this.colName,
            this.colIs_Provider,
            this.colIs_Derive_Company,
            this.colIs_Pay_Receiver,
            this.colCode,
            this.colMeter_Future_Period_Day,
            this.colLoad_Measure_In_Future,
            this.colSberServiceNumber});
            this.gridViewCompany.GridControl = this.gridCompany;
            this.gridViewCompany.Name = "gridViewCompany";
            this.gridViewCompany.OptionsBehavior.Editable = false;
            this.gridViewCompany.OptionsView.ShowGroupPanel = false;
            // 
            // colID_Company
            // 
            this.colID_Company.AppearanceHeader.Options.UseTextOptions = true;
            this.colID_Company.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID_Company.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID_Company.Caption = "ИД";
            this.colID_Company.FieldName = "ID_Company";
            this.colID_Company.Name = "colID_Company";
            this.colID_Company.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colName.Caption = "Наименование";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 241;
            // 
            // colIs_Provider
            // 
            this.colIs_Provider.AppearanceHeader.Options.UseTextOptions = true;
            this.colIs_Provider.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIs_Provider.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIs_Provider.Caption = "Поставщик";
            this.colIs_Provider.FieldName = "Is_Provider";
            this.colIs_Provider.Name = "colIs_Provider";
            this.colIs_Provider.Visible = true;
            this.colIs_Provider.VisibleIndex = 2;
            this.colIs_Provider.Width = 78;
            // 
            // colIs_Derive_Company
            // 
            this.colIs_Derive_Company.AppearanceHeader.Options.UseTextOptions = true;
            this.colIs_Derive_Company.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIs_Derive_Company.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIs_Derive_Company.Caption = "УК";
            this.colIs_Derive_Company.FieldName = "Is_Derive_Company";
            this.colIs_Derive_Company.Name = "colIs_Derive_Company";
            this.colIs_Derive_Company.Visible = true;
            this.colIs_Derive_Company.VisibleIndex = 1;
            this.colIs_Derive_Company.Width = 76;
            // 
            // colIs_Pay_Receiver
            // 
            this.colIs_Pay_Receiver.AppearanceHeader.Options.UseTextOptions = true;
            this.colIs_Pay_Receiver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIs_Pay_Receiver.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIs_Pay_Receiver.Caption = "Получ. платежа";
            this.colIs_Pay_Receiver.FieldName = "Is_Pay_Receiver";
            this.colIs_Pay_Receiver.Name = "colIs_Pay_Receiver";
            this.colIs_Pay_Receiver.Visible = true;
            this.colIs_Pay_Receiver.VisibleIndex = 3;
            this.colIs_Pay_Receiver.Width = 69;
            // 
            // colCode
            // 
            this.colCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCode.Caption = "Код";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            // 
            // colMeter_Future_Period_Day
            // 
            this.colMeter_Future_Period_Day.AppearanceHeader.Options.UseTextOptions = true;
            this.colMeter_Future_Period_Day.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMeter_Future_Period_Day.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMeter_Future_Period_Day.FieldName = "Meter_Future_Period_Day";
            this.colMeter_Future_Period_Day.Name = "colMeter_Future_Period_Day";
            // 
            // colLoad_Measure_In_Future
            // 
            this.colLoad_Measure_In_Future.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoad_Measure_In_Future.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoad_Measure_In_Future.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLoad_Measure_In_Future.FieldName = "Load_Measure_In_Future";
            this.colLoad_Measure_In_Future.Name = "colLoad_Measure_In_Future";
            // 
            // txtFilterName
            // 
            this.txtFilterName.Location = new System.Drawing.Point(154, 5);
            this.txtFilterName.Name = "txtFilterName";
            this.txtFilterName.Size = new System.Drawing.Size(218, 20);
            this.txtFilterName.TabIndex = 0;
            this.txtFilterName.TextChanged += new System.EventHandler(this.txtFilterName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Поиск по наименованию:";
            // 
            // colSberServiceNumber
            // 
            this.colSberServiceNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colSberServiceNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSberServiceNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSberServiceNumber.Caption = "Номер услуги СБ РФ";
            this.colSberServiceNumber.FieldName = "SberServiceNumber";
            this.colSberServiceNumber.Name = "colSberServiceNumber";
            this.colSberServiceNumber.Visible = true;
            this.colSberServiceNumber.VisibleIndex = 4;
            // 
            // FrmCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 591);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFilterName);
            this.Controls.Add(this.gridCompany);
            this.Name = "FrmCompany";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmCompany";
            this.Load += new System.EventHandler(this.FrmCompany_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCompany)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseGrid gridCompany;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCompany;
        private System.Windows.Forms.BindingSource payReceiverCompanyBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colIs_Provider;
        private DevExpress.XtraGrid.Columns.GridColumn colIs_Derive_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colIs_Pay_Receiver;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMeter_Future_Period_Day;
        private DevExpress.XtraGrid.Columns.GridColumn colLoad_Measure_In_Future;
        private System.Windows.Forms.TextBox txtFilterName;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn colSberServiceNumber;
    }
}