﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using EllisBillFilter.DS;

    public partial class FrmSberSaldoSettings : Form
    {
        public FrmSberSaldoSettings()
        {
            InitializeComponent();
            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
        }

        private void gridClientDetailDeliverySettings1_Load(object sender, EventArgs e)
        {
            gridClientDetailDeliverySettings1.LoadData();
        }
    }
}
