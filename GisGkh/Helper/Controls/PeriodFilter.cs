﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Helper.Data;

namespace Helper.Controls
{
    public partial class PeriodFilter : UserControl
    {
        public int Period
        {
            get { return Convert.ToInt32(cbPeriod.SelectedValue); }
        }

        public PeriodFilter()
        {
            InitializeComponent();
        }

        public void Fill()
        {
            cbPeriod.DataSource = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.SelectPeriod");
            cbPeriod.ValueMember = "period";
            cbPeriod.DisplayMember = "period_name";
        }
    }
}
