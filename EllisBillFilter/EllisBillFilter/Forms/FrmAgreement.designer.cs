﻿namespace EllisBillFilter
{
    partial class FrmAgreement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAgreement));
            this.deBeginDate = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.deEndDate = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFilterAgent = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFilterPrincipal = new System.Windows.Forms.TextBox();
            this.txtFilterNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.gridAgreement1 = new EllisBillFilter.GridAgreement();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // deBeginDate
            // 
            this.deBeginDate.EditValue = null;
            this.deBeginDate.Location = new System.Drawing.Point(79, 10);
            this.deBeginDate.Name = "deBeginDate";
            this.deBeginDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deBeginDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deBeginDate.Size = new System.Drawing.Size(124, 20);
            this.deBeginDate.TabIndex = 1;
            this.deBeginDate.EditValueChanged += new System.EventHandler(this.deBeginDate_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Договора с";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(209, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "по";
            // 
            // deEndDate
            // 
            this.deEndDate.EditValue = null;
            this.deEndDate.Location = new System.Drawing.Point(234, 10);
            this.deEndDate.Name = "deEndDate";
            this.deEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEndDate.Size = new System.Drawing.Size(124, 20);
            this.deEndDate.TabIndex = 4;
            this.deEndDate.EditValueChanged += new System.EventHandler(this.deEndDate_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(391, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Фильтр по №:";
            // 
            // txtFilterAgent
            // 
            this.txtFilterAgent.Location = new System.Drawing.Point(625, 10);
            this.txtFilterAgent.Name = "txtFilterAgent";
            this.txtFilterAgent.Size = new System.Drawing.Size(127, 20);
            this.txtFilterAgent.TabIndex = 6;
            this.txtFilterAgent.TextChanged += new System.EventHandler(this.txtFilterAgent_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(762, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Принципалу:";
            // 
            // txtFilterPrincipal
            // 
            this.txtFilterPrincipal.Location = new System.Drawing.Point(836, 10);
            this.txtFilterPrincipal.Name = "txtFilterPrincipal";
            this.txtFilterPrincipal.Size = new System.Drawing.Size(127, 20);
            this.txtFilterPrincipal.TabIndex = 8;
            this.txtFilterPrincipal.TextChanged += new System.EventHandler(this.txtFilterPrincipal_TextChanged);
            // 
            // txtFilterNumber
            // 
            this.txtFilterNumber.Location = new System.Drawing.Point(488, 10);
            this.txtFilterNumber.Name = "txtFilterNumber";
            this.txtFilterNumber.Size = new System.Drawing.Size(82, 20);
            this.txtFilterNumber.TabIndex = 9;
            this.txtFilterNumber.TextChanged += new System.EventHandler(this.txtFilterNumber_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(579, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Агенту:";
            // 
            // gridAgreement1
            // 
            this.gridAgreement1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridAgreement1.Location = new System.Drawing.Point(0, 40);
            this.gridAgreement1.Name = "gridAgreement1";
            this.gridAgreement1.Size = new System.Drawing.Size(985, 575);
            this.gridAgreement1.TabIndex = 0;
            // 
            // FrmAgreement
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(985, 617);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtFilterNumber);
            this.Controls.Add(this.txtFilterPrincipal);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtFilterAgent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.deEndDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.deBeginDate);
            this.Controls.Add(this.gridAgreement1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmAgreement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Справочник договоров";
            this.Load += new System.EventHandler(this.FrmAgreement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GridAgreement gridAgreement1;
        private DevExpress.XtraEditors.DateEdit deBeginDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.DateEdit deEndDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFilterAgent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFilterPrincipal;
        private System.Windows.Forms.TextBox txtFilterNumber;
        private System.Windows.Forms.Label label5;
    }
}