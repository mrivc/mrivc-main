﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using FastReport;
using System.Threading;

namespace EllisBillFilter
{
    using EllisBillFilter.Properties;
    using FastReport.Export.Pdf;
    using MrivcHelpers.Helpers;
    public partial class FrmPrintBillAcc : Form
    {
        int idAccount;
        string account;
        string address;
        MDataSet.ClientDetailDataTable dtClientDetail;
        private bool initialized = false;
        long idPrint;
        private FastReport.Report report1;

        public FrmPrintBillAcc()
        {
            InitializeComponent();
        }

        public FrmPrintBillAcc(int idAccount)
        {
            InitializeComponent();

            Clipboard.SetText(idAccount.ToString());
            this.idAccount = idAccount;
            this.account = DAccess.DataModule.GetAccount(idAccount);
            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
        }

        private void FrmPrintBillAcc_Shown(object sender, EventArgs e)
        {
            this.report1 = new FastReport.Report();
            //this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
        }

        private void UpdateLableCompanyName()
        {
            if (dtClientDetail.Rows.Count > 0)
                lblUK.Text = "Управляющая компания: " + DAccess.DataModule.GetCompanyName((int)dtClientDetail.Rows[0]["ID_Derive_Company"]);
            else
                lblUK.Text = "Управляющая компания: информация не доступна.";
        }

        private void CheckAllFilters(bool check)
        {
            for (int i = 0; i <= (clBillFilter.Items.Count - 1); i++)
                if (check)
                    clBillFilter.SetItemCheckState(i, CheckState.Checked);
                else
                    clBillFilter.SetItemCheckState(i, CheckState.Unchecked);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cbEmailSend.Checked && (txtEmail.Text.Trim().Length == 0 || !txtEmail.Text.IsValidEmail()))
            {
                MessageBox.Show("Для отправки квитанций, пожалуйста, укажите правильный E-mail адрес.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            btnPrint.Enabled = false;
            InitReport();

            SetStatusText("подготовка списка лицевый счетов...");
            DAccess.DataModule.PrepareBillReportAccounts(idAccount.ToString());

            int period = (int)cbPeriod.SelectedValue;

            string periodName = DAccess.DataModule.GetPeriodName(period);
            int periodYear = DAccess.DataModule.GetPeriodYear(period);
            int periodMonth = DAccess.DataModule.GetPeriodMonth(period);

            foreach (DataRowView row in clBillFilter.CheckedItems)
            {
                int idBillFilter = (int)row["ID_Bill_Filter"];
                idPrint = DAccess.Instance.GenerateIdPrint();
                SetStatusText("подготовка данных для квитанций... " + row["Bill_Filter"].ToString());
                DAccess.DataModule.PrepareBillReportData(idPrint, idBillFilter, period, false, false, -1, 0);
                PrepareBillReport(idPrint);

                DAccess.DataModule.ClearPrintDataTables(idPrint);
                SetStatusText(string.Format("Готово {0}", idPrint));

				Clipboard.Clear();
				Clipboard.SetText(idPrint.ToString());
            }

            if (cbEmailSend.Checked)
            {
                SendBillByEmail(periodName, periodYear, periodMonth);
            }

            report1.ShowPrepared();
            DialogResult = System.Windows.Forms.DialogResult.OK;
			btnPrint.Enabled = true;

			if (Settings.Default.CloseWindowAfterPrintBill)
				this.Close();
        }

        private void SendBillByEmail(string periodName, int periodYear, int periodMonth)
        {
            PDFExport pdfExport;
            pdfExport = new FastReport.Export.Pdf.PDFExport();
            pdfExport.ShowProgress = false;
            pdfExport.Compressed = true;
            pdfExport.AllowPrint = true;
            pdfExport.EmbeddingFonts = true;

           // string billKind = row["Bill_Filter"].ToString();
            int period = (int)cbPeriod.SelectedValue;
            string email = txtEmail.Text;

            pdfExport.Subject = "АО \"МРИВЦ\"";
            pdfExport.Title = Guid.NewGuid().ToString();

            string pathToSave = string.Format("{0}\\{1}\\{2}\\", Settings.Default.PathToBill, periodYear, StringHelper.ZeroCode(periodMonth));
            FileHelper.CreateDirectory(pathToSave);

            string fileName = GetFileName(account, period) + ".pdf";
            report1.Export(pdfExport, pathToSave + fileName);
            List<string> attachFiles = new List<string>();
            attachFiles.Add(pathToSave + fileName);

            string error = MailManager.SendMail(Settings.Default.SmtpServerHost,
                    Settings.Default.SmtpServerPort,
                    Settings.Default.SmtpServerSSL,
                   Settings.Default.SmtpServerMailFrom,
                   Settings.Default.SmtpServerPswd,
                   email,
                   Settings.Default.SendCopyTo,
                   Settings.Default.MailBillSubject.Replace("@CurrentDate@", DateTime.Now.ToShortDateString()).Replace("@Period@", periodName).Replace("@BillKind@", string.Empty).Replace("@Account@", account).Replace("@Address@", address),
                   Settings.Default.MailBillBody.Replace("@CurrentDate@", DateTime.Now.ToShortDateString()).Replace("@Period@", periodName).Replace("@BillKind@", string.Empty).Replace("@Account@", account).Replace("@Address@", address),
                   attachFiles);

            Thread.Sleep(500);

            if (error.Length == 0)
            {
                DAccess.DataModule.InsertPrintDeliveryInfo(idPrint, period, idAccount, -1, email, fileName, 1, "");
            }
            else
            {
                DAccess.DataModule.InsertPrintDeliveryInfo(idPrint, period, idAccount, -1, email, fileName, 0, "");
            }
        }

        private string GetFileName(string account, int period)
        {
            return string.Format("{0}_{1}_{2}", account, DAccess.DataModule.GetPeriodMY(period), StringHelper.GetShortTimeDateString());
        }

        private void InitReport()
        {
            report1 = new Report();

            SetStatusText("загрузка отчета...");
            report1.LoadFromString(DAccess.Instance.GetBillStdReport());
            report1.DoublePass = false;
            report1.UseFileCache = false;
            report1.SetParameterValue("MainConnection", DAccess.DataModule.Connection.ConnectionString);
            report1.SetParameterValue("PrintFio", cbPrintFio.Checked);
        }

        private void PrepareBillReport(object idPrint)
        {
            report1.SetParameterValue("IDPrint", (long)idPrint);
            report1.SetParameterValue("IS_DEBT", false);
            report1.SetParameterValue("PrintSummaryPage", false);
            report1.SetParameterValue("PrintBillsWithSubscription", true);
            report1.Prepare(true);
        }

        private void SetStatusText(string text)
        {
            txtStatus.Text = text;
            txtStatus.Refresh();
        }

        private void cbPeriod_SelectedValueChanged(object sender, EventArgs e)
        {
            if (initialized && cbPeriod.SelectedValue != null)
            {
                Cursor = Cursors.WaitCursor;
                clBillFilter.BeginUpdate();
                dtClientDetail.Clear();
                DAccess.DataModule.ClientDetailAccountSelect(dtClientDetail, idAccount, (int)cbPeriod.SelectedValue);
                UpdateLableCompanyName();
              //  CheckAllFilters(true);
                clBillFilter.EndUpdate();
                Cursor = Cursors.Default;
            }
        }

        private void FrmPrintBillAcc_Load(object sender, EventArgs e)
        {
            dtClientDetail = new MDataSet.ClientDetailDataTable();

            mDataSet.Period.Merge(DAccess.Instance.Periods);
            mDataSet.Period.Rows[0].Delete();

            DAccess.DataModule.ClientDetailAccountSelect(dtClientDetail, idAccount, DAccess.Instance.WorkPeriod);

            ((ListBox)clBillFilter).DataSource = dtClientDetail;
            ((ListBox)clBillFilter).ValueMember = "ID_Client_Detail";
            ((ListBox)clBillFilter).DisplayMember = "Bill_Filter";

            address = DAccess.DataModule.GetAccountAddress(idAccount);
            lblAddress.Text = "Адрес: " + address;
            txtStatus.Text = "";
            txtEmail.Text = DAccess.DataModule.GetAccountSubscriptionEmail(idAccount);
            UpdateLableCompanyName();

            cbPeriod.SelectedValue = DAccess.Instance.WorkPeriod;

          //  CheckAllFilters(true);
            initialized = true;
        }
    }
}
