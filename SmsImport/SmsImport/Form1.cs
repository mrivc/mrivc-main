﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MrivcHelpers;
using System.IO;
using MrivcControls.Data;

namespace SmsImport
{
    public partial class Form1 : Form
    {
        public string SheetName { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dt = ExcelHelper2.ImportExcelData($"{textBox2.Text}$", Path.GetFullPath(textBox1.Text));
            DAccess.DataModule.Connection1 = new System.Data.SqlClient.SqlConnection("Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Integrated Security=True");
            dt.TableName = textBox2.Text;
            DAccess.DataModule.CopyDataTableToTempDB(dt);
            DAccess.DataModule.ExecuteNonQueryCommand1($"insert meter.[dbo].[meter_measure_sms] select * from #{textBox2.Text}");
            MessageBox.Show("Готово","",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
            }
        }
    }
}
