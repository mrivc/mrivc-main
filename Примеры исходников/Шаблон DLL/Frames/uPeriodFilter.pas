Unit uPeriodFilter;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls,
  cxLookAndFeelPainters, cxGraphics, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls, cxContainer,
  cxEdit, cxGroupBox;

Type
  TPeriodFilter = Class(TFrame)
    gbPeriod: TcxGroupBox;
    jvdbcbPeriod: TcxLookupComboBox;
    adsPeriod: TADODataSet;
    jvdsPeriod: TDataSource;
    dfPeriod: TIntegerField;
    dfYear: TIntegerField;
    dfMonth: TIntegerField;
    dfPeriod_Name: TStringField;
    dfBegin_Date: TDateTimeField;
    dfEnd_Date: TDateTimeField;
    dfDay_Count: TIntegerField;
    dfIs_Work_Period: TBooleanField;
    Procedure jvdbcbPeriodChange(Sender: TObject);
  Private
    { Private declarations }
    FMyOnChange: TNotifyEvent;
    Function DateToQuotedStr(ADate: TDate): String;
  Public
    { Public declarations }
    Procedure Initialize(AConnection: TADOConnection; APeriod: Integer = 0);
    Function GetPeriod: Variant;
    Function GetPeriodID: String;
    Function GetPeriodStr: String;
    Function GetFirstDate: TDate;
    Function FirstDateFormat(Const Format: String): String;
    Function LastDateQS: String;
    Function GetLastDate: TDate;
    Function IsWorkPeriod: Boolean;
    Function DayCount: Integer;

    Procedure SetPeriod(APeriod: Integer = 0); Overload;
    Procedure SetPeriod(AYear, AMonth: Integer); Overload;
    Property MyOnChange: TNotifyEvent Read FMyOnChange Write FMyOnChange;
  End;

Implementation

{$R *.dfm}

Uses Math;

{ TPeriodFilter }

Function TPeriodFilter.DateToQuotedStr(ADate: TDate): String;
Begin
  Result := QuotedStr(FormatDateTime('DD.MM.YYYY', ADate));
End;

Function TPeriodFilter.DayCount: Integer;
Begin
  Result := dfDay_Count.Value;
End;

Function TPeriodFilter.FirstDateFormat(Const Format: String): String;
Begin
  Result := FormatDateTime(Format, GetFirstDate);
End;

Function TPeriodFilter.GetFirstDate: TDate;
Begin
  Result := dfBegin_Date.AsDateTime;
End;

Function TPeriodFilter.GetLastDate: TDate;
Begin
  Result := dfEnd_Date.AsDateTime;
End;

Function TPeriodFilter.GetPeriod: Variant;
Begin
  Result := dfPeriod.Value;
End;

Function TPeriodFilter.GetPeriodID: String;
Begin
  Result := dfPeriod.AsString;
End;

Function TPeriodFilter.GetPeriodStr: String;
Begin
  Result := dfPeriod_Name.AsString;
End;

Procedure TPeriodFilter.Initialize(AConnection: TADOConnection;
  APeriod: Integer);
Begin
  adsPeriod.Connection := AConnection;
  adsPeriod.Open;
  SetPeriod(APeriod);
End;

Function TPeriodFilter.IsWorkPeriod: Boolean;
Begin
  Result := dfIs_Work_Period.AsBoolean;
End;

Procedure TPeriodFilter.jvdbcbPeriodChange(Sender: TObject);
Begin
  If Assigned(FMyOnChange) Then FMyOnChange(Sender);
End;

Function TPeriodFilter.LastDateQS: String;
Begin
  Result := DateToQuotedStr(GetLastDate);
End;

Procedure TPeriodFilter.SetPeriod(APeriod: Integer);
Begin
  If APeriod <= 0 Then
    Begin
      adsPeriod.Locate('Is_Work_Period', 1, []);
      adsPeriod.Locate('Period', dfPeriod.Value + APeriod, []);
    End
  Else
    adsPeriod.Locate('Period', APeriod, []);

  jvdbcbPeriod.EditValue := dfPeriod.Value;
End;

Procedure TPeriodFilter.SetPeriod(AYear, AMonth: Integer);
Begin
  adsPeriod.Locate('Year;Month', VarArrayOf([AYear, AMonth]), []);
  jvdbcbPeriod.EditValue := dfPeriod.Value;
End;

End.

