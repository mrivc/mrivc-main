﻿namespace Payments
{
    partial class FrmConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConnection));
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnection = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Таймаут = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nudComNumber = new System.Windows.Forms.NumericUpDown();
            this.nudTimeout = new System.Windows.Forms.NumericUpDown();
            this.nudBaudRate = new System.Windows.Forms.NumericUpDown();
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.btnState = new System.Windows.Forms.Button();
            this.gbPrintCheck = new System.Windows.Forms.GroupBox();
            this.btnStateCheck = new System.Windows.Forms.Button();
            this.btnPrintOff = new System.Windows.Forms.Button();
            this.btnPrintOn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudComNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBaudRate)).BeginInit();
            this.gbPrintCheck.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "COM порт:";
            // 
            // btnConnection
            // 
            this.btnConnection.Location = new System.Drawing.Point(109, 171);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(88, 23);
            this.btnConnection.TabIndex = 4;
            this.btnConnection.Text = "Подключить";
            this.btnConnection.UseVisualStyleBackColor = true;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(203, 171);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(84, 128);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(194, 20);
            this.txtResult.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Скорость:";
            // 
            // Таймаут
            // 
            this.Таймаут.AutoSize = true;
            this.Таймаут.Location = new System.Drawing.Point(8, 72);
            this.Таймаут.Name = "Таймаут";
            this.Таймаут.Size = new System.Drawing.Size(50, 13);
            this.Таймаут.TabIndex = 9;
            this.Таймаут.Text = "Таймаут";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Состояние:";
            // 
            // nudComNumber
            // 
            this.nudComNumber.Location = new System.Drawing.Point(84, 11);
            this.nudComNumber.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudComNumber.Name = "nudComNumber";
            this.nudComNumber.Size = new System.Drawing.Size(194, 20);
            this.nudComNumber.TabIndex = 0;
            this.nudComNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudTimeout
            // 
            this.nudTimeout.Location = new System.Drawing.Point(84, 70);
            this.nudTimeout.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudTimeout.Name = "nudTimeout";
            this.nudTimeout.Size = new System.Drawing.Size(194, 20);
            this.nudTimeout.TabIndex = 2;
            this.nudTimeout.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // nudBaudRate
            // 
            this.nudBaudRate.Location = new System.Drawing.Point(84, 41);
            this.nudBaudRate.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudBaudRate.Name = "nudBaudRate";
            this.nudBaudRate.Size = new System.Drawing.Size(194, 20);
            this.nudBaudRate.TabIndex = 11;
            this.nudBaudRate.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // btnReset
            // 
            this.btnReset.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnReset.Location = new System.Drawing.Point(12, 171);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(62, 23);
            this.btnReset.TabIndex = 13;
            this.btnReset.Text = "Сброс";
            this.btnReset.ToolTip = "Сброс настроек подключения по умолчанию";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnState
            // 
            this.btnState.Location = new System.Drawing.Point(145, 96);
            this.btnState.Name = "btnState";
            this.btnState.Size = new System.Drawing.Size(133, 23);
            this.btnState.TabIndex = 14;
            this.btnState.Text = "Текущий таймаут";
            this.btnState.UseVisualStyleBackColor = true;
            this.btnState.Click += new System.EventHandler(this.btnState_Click);
            // 
            // gbPrintCheck
            // 
            this.gbPrintCheck.Controls.Add(this.btnStateCheck);
            this.gbPrintCheck.Controls.Add(this.btnPrintOff);
            this.gbPrintCheck.Controls.Add(this.btnPrintOn);
            this.gbPrintCheck.Location = new System.Drawing.Point(293, 13);
            this.gbPrintCheck.Name = "gbPrintCheck";
            this.gbPrintCheck.Size = new System.Drawing.Size(266, 87);
            this.gbPrintCheck.TabIndex = 15;
            this.gbPrintCheck.TabStop = false;
            this.gbPrintCheck.Text = "Печать ленты";
            // 
            // btnStateCheck
            // 
            this.btnStateCheck.Location = new System.Drawing.Point(127, 19);
            this.btnStateCheck.Name = "btnStateCheck";
            this.btnStateCheck.Size = new System.Drawing.Size(133, 23);
            this.btnStateCheck.TabIndex = 2;
            this.btnStateCheck.Text = "Состяние";
            this.btnStateCheck.UseVisualStyleBackColor = true;
            this.btnStateCheck.Click += new System.EventHandler(this.btnStateCheck_Click);
            // 
            // btnPrintOff
            // 
            this.btnPrintOff.Location = new System.Drawing.Point(6, 48);
            this.btnPrintOff.Name = "btnPrintOff";
            this.btnPrintOff.Size = new System.Drawing.Size(115, 23);
            this.btnPrintOff.TabIndex = 1;
            this.btnPrintOff.Text = "Не печатать чек";
            this.btnPrintOff.UseVisualStyleBackColor = true;
            this.btnPrintOff.Click += new System.EventHandler(this.btnPrintOff_Click);
            // 
            // btnPrintOn
            // 
            this.btnPrintOn.Location = new System.Drawing.Point(6, 19);
            this.btnPrintOn.Name = "btnPrintOn";
            this.btnPrintOn.Size = new System.Drawing.Size(115, 23);
            this.btnPrintOn.TabIndex = 0;
            this.btnPrintOn.Text = "Печатать чек";
            this.btnPrintOn.UseVisualStyleBackColor = true;
            this.btnPrintOn.Click += new System.EventHandler(this.btnPrintOn_Click);
            // 
            // FrmConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 209);
            this.Controls.Add(this.gbPrintCheck);
            this.Controls.Add(this.btnState);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.nudBaudRate);
            this.Controls.Add(this.nudTimeout);
            this.Controls.Add(this.nudComNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Таймаут);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConnection);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(606, 358);
            this.MinimizeBox = false;
            this.Name = "FrmConnection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройка подключения кассы";
            ((System.ComponentModel.ISupportInitialize)(this.nudComNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBaudRate)).EndInit();
            this.gbPrintCheck.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Таймаут;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudComNumber;
        private System.Windows.Forms.NumericUpDown nudTimeout;
        private System.Windows.Forms.NumericUpDown nudBaudRate;
        private DevExpress.XtraEditors.SimpleButton btnReset;
        private System.Windows.Forms.Button btnState;
        private System.Windows.Forms.GroupBox gbPrintCheck;
        private System.Windows.Forms.Button btnStateCheck;
        private System.Windows.Forms.Button btnPrintOff;
        private System.Windows.Forms.Button btnPrintOn;
    }
}