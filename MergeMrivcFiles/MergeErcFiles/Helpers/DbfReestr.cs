﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data.Common;

namespace Payments
{
    public class DbfReestr
    {
        DataTable template;
        public DbfReestr()
        {
            template = GetTemplateDataSet();
        }

        public void AddFile(string path, string fileName)
        {
            DataTable dt = null;
            if (fileName.ToUpper().IndexOf(".DBF") >= 0)
            {
                dt = ReadDbfFile(path, fileName);
                if (dt.Columns.Count == 8)
                    dt = ConvertDbfFileToMrivcFormat(dt, path + "\\" + fileName);
                else
                    dt = AddCommentToDbfFile(dt, path + "\\" + fileName);
            }
            else if (fileName.ToUpper().IndexOf(".TXT") >= 0)
            {
                dt = ConvertTxtFileToMrivcDbfFormat(path + "\\" + fileName);
            }

            if (dt != null)
                template.Merge(dt);
        }

        public void UnloadToDBF(string fileName, DataTable dataTable, string clientName)
        {
            string templatePath = Application.StartupPath + "\\" + "TEMPLATE.DBF";
            string UnloadedPath = Application.StartupPath + "\\";
            string unloadedFileName = fileName;
            if(fileName.Length > 8)
                unloadedFileName = fileName.Substring(0, 8); //уменьшаем имя файла до 8ми знаков

            if (!FileHelper.ExistFile(templatePath))
                throw new Exception("Не найден файл шаблона " + templatePath + ". Восстановите файл шаблона и повторите выгрузку.");

            if (!FileHelper.DirectoryExists(UnloadedPath))
                FileHelper.CreateDirectory(UnloadedPath);

            FileHelper.CopyFile(templatePath, UnloadedPath + unloadedFileName + ".DBF");

            DbConnection con = GetConnection(UnloadedPath);
            DbCommand cmd = new OleDbCommand();

            cmd.Connection = con;
            con.Open();
            cmd.CommandText = "delete from " + unloadedFileName;
            cmd.ExecuteNonQuery();

            foreach (DataRow row in dataTable.Rows)
            {
                string insertSql = "insert into " + unloadedFileName + " values("
                    + "'" + row["LS"].ToString() + "',"
                    + "'" + StringHelper.ReplaceEscape(row["ADRES"].ToString()) + "',"
                    + "'" + StringHelper.ReplaceEscape(row["UK"].ToString()) + "',"
                    + "'" + row["FAM"].ToString() + "',"
                    + "'" + row["IM"].ToString() + "',"
                    + "'" + row["OT"].ToString() + "',"
                    + "'" + row["AUTHCOD"].ToString() + "',"
                    + "'" + row["KVITID"].ToString() + "',"
                    + "'" + row["DATPLAT"].ToString() + "',"
                    + "" + row["SUMPLAT"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU1"].ToString() + "',"
                    + "" + row["POK1"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU2"].ToString() + "',"
                    + "" + row["POK2"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU3"].ToString() + "',"
                    + "" + row["POK3"].ToString().Replace(",", ".") + ","
                    + "'" + row["PU4"].ToString() + "',"
                    + "" + row["POK4"].ToString().Replace(",", ".") + ","
                    + "'" + StringHelper.ReplaceEscape(row["COMMENT"].ToString()) + "'" + ","
                    + "'" + StringHelper.ReplaceEscape(row["COMMENT2"].ToString()) + "'"
                    + ")";
                cmd.CommandText = insertSql;
                cmd.ExecuteNonQuery();
            }

            FileHelper.RewriteFileWithRename(UnloadedPath + unloadedFileName + ".DBF", UnloadedPath + fileName + ".DBF");
            con.Close();
        }

        private DataTable ConvertTxtFileToMrivcDbfFormat(string fileName)
        {
            
            string[] lines = System.IO.File.ReadAllLines(fileName, Encoding.GetEncoding(1251));
            DataTable convertedTable = template.Clone();

            foreach (string line in lines)
            {
                if (!line.StartsWith("#"))
                {
                    string[] s = line.Split(new char[] { ';', ':' }, StringSplitOptions.None);
                    if (s.Length == 23)
                    {
                        DataRow newRow = convertedTable.NewRow();

                        newRow["LS"] = s[0];
                        newRow["ADRES"] = s[8];
                        newRow["UK"] = s[9];
                        newRow["AUTHCOD"] = s[10];
                        newRow["DATPLAT"] = s[20];
                        newRow["SUMPLAT"] = s[3];
                        if (s[16].Length > 0 && s[17].Length>0)
                            newRow["POK3"] = s[17];
                        newRow["COMMENT"] = s[22];
                        newRow["COMMENT2"] = fileName;
                        convertedTable.Rows.Add(newRow);
                    }
                    else
                    {
                        throw new Exception("Неизвестный формат файла!");
                    }
                }
            }
            return convertedTable;
        }

        private DataTable AddCommentToDbfFile(DataTable dt, string fileName)
        {
            DataTable convertedTable = template.Clone();
            foreach (DataRow r in dt.Rows)
            {
                DataRow newRow = convertedTable.NewRow();
                newRow["LS"] = r["LS"];
                newRow["ADRES"] = r["ADRES"];
                newRow["UK"] = r["UK"];
                newRow["FAM"] = r["FAM"];
                newRow["AUTHCOD"] = r["AUTHCOD"];
                newRow["DATPLAT"] = r["DATPLAT"];
                newRow["SUMPLAT"] = r["SUMPLAT"];
                newRow["POK3"] = r["POK3"];
                if (dt.Columns.IndexOf("COMMENT") > 0)
                    newRow["COMMENT"] = r["COMMENT"].ToString();
                newRow["COMMENT2"] = fileName;
                convertedTable.Rows.Add(newRow);
            }
            return convertedTable;
        }

        private DataTable ConvertDbfFileToMrivcFormat(DataTable dt, string fileName)
        {
            DataTable convertedTable = template.Clone();
            foreach (DataRow r in dt.Rows)
            {
                DataRow newRow = convertedTable.NewRow();
                newRow["LS"] = r["LS"];
                newRow["AUTHCOD"] = r["LS"];
                newRow["DATPLAT"] = r["DP"];
                newRow["SUMPLAT"] = r["SP"];
                newRow["POK3"] = r["P3"];
                newRow["COMMENT"] = r["POST_TEXT"].ToString();
                newRow["COMMENT2"] = fileName;
                convertedTable.Rows.Add(newRow);
            }
            return convertedTable;
        }

        public DataTable ReadDbfFile(string path, string fileName)
        {
            OleDbConnection con = (OleDbConnection)GetConnection(path);
            string cmd = "select * from " + fileName.ToUpper().Replace(".DBF","");
            OleDbDataAdapter dbAdapter = new OleDbDataAdapter(cmd, con);

            DataTable dt = new DataTable();
            dbAdapter.Fill(dt);

            return dt;
        }

        public DataTable MergedFile
        {
            get {
                template.TableName = "AllFiles";
                return template;
            }
        }

        public DataTable GetTemplateDataSet()
        {
            OleDbConnection con = (OleDbConnection)GetConnection(Application.StartupPath);
            string cmd = "select * from TEMPLATE";
            OleDbDataAdapter dbAdapter = new OleDbDataAdapter(cmd, con);

            DataTable dt = new DataTable();
            dbAdapter.Fill(dt);

            return dt;
        }

        private DbConnection GetConnection(string path)
        {
            return new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=dBASE IV;");
        }
    }
}
