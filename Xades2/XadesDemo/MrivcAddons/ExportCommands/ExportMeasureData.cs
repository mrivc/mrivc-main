﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ExportMeasureData : MrivcExportCommand, ISingleHouseCommand
    {
        public string FiasHouseGuid { get; set; }

        public ExportMeasureData(string senderID) : base("exportMeteringDeviceHistory", "DeviceMeteringAsync", senderID, "exportMeteringDeviceHistoryResult")
        {
            MrivcData.CreateTempExportMeasureTables();
        }

        public void ExecuteToTempDB()
        {
            var deviceType = DAccess.DataModule.GetDataTableByQueryCommand1("select NsiItemCode as Code,GUID,Value Name from gis.vnsiitem where RegistryNumber = 27");

            var values = new List<Tuple<string[], string>>
                {
                      new Tuple<string[], string> (new [] { "FIASHouseGuid" }, FiasHouseGuid),
                };

            Values = values;

            foreach (DataRow row in deviceType.Rows)
            {
                values = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string> (new [] { "MeteringDeviceType","Code" }, row[0].ToString()),
                        new Tuple<string[], string> (new [] { "MeteringDeviceType","GUID" }, row[1].ToString()),
                        new Tuple<string[], string> (new [] { "MeteringDeviceType","Name" }, row[2].ToString())
                    };

                AddChildElement(MrivcStatic.GisConfig.Services[Service].Methods["exportMeteringDeviceHistory_add"].Template, values, "exportMeteringDeviceHistoryRequest");
            }

            base.ExecuteToTempDB(_exportNodeName);
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportMeasureData");
        }

        public new void Execute()
        {
            this.ExecuteToTempDB();
        }

        public DataSet PrepareToDataSet()
        {
            return new DataSet();
        }
    }
}
