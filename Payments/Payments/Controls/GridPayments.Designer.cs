﻿namespace Payments.Controls
{
    partial class GridPayments
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridPayment = new Payments.BaseControls.BaseGrid();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miReturnSale = new System.Windows.Forms.ToolStripMenuItem();
            this.miRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.miExport = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.paymentsDataSet = new Payments.PaymentsDataSet();
            this.gridViewPayment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Payment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Payment_Class = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riPaymentClass = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.paymentClassBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPayment_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayment_Cash = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayment_Summ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayment_Commission = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayment_SummTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayment_Charge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riUser = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colDepartment_Number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsStorno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperation_Number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSession_Number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Cash_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riCashType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cashTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colDocumentNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReturnDocumentNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ddPaymentClass = new DevExpress.XtraEditors.LookUpEdit();
            this.lblPaymentClass = new System.Windows.Forms.Label();
            this.btnInitPayment = new DevExpress.XtraEditors.SimpleButton();
            this.userTableAdapter = new Payments.PaymentsDataSetTableAdapters.UserTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tabPaymentDetail = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridPaymentParameter1 = new Payments.Controls.GridPaymentParameter();
            this.deBeginDate = new DevExpress.XtraEditors.DateEdit();
            this.deEndDate = new DevExpress.XtraEditors.DateEdit();
            this.lblPaymentPeriodBegin = new System.Windows.Forms.Label();
            this.lblPaymentPeriodEnd = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridPayment)).BeginInit();
            this.contextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.paymentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPaymentClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentClassBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCashType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPaymentClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPaymentDetail)).BeginInit();
            this.tabPaymentDetail.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridPayment
            // 
            this.gridPayment.ContextMenuStrip = this.contextMenu;
            this.gridPayment.DataSource = this.paymentBindingSource;
            this.gridPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPayment.Location = new System.Drawing.Point(0, 0);
            this.gridPayment.MainView = this.gridViewPayment;
            this.gridPayment.Name = "gridPayment";
            this.gridPayment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riPaymentClass,
            this.riUser,
            this.riCashType});
            this.gridPayment.Size = new System.Drawing.Size(730, 400);
            this.gridPayment.TabIndex = 3;
            this.gridPayment.TrimCellValueBeforeValidate = true;
            this.gridPayment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPayment});
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miReturnSale,
            this.miRefresh,
            this.miExport});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(170, 70);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            // 
            // miReturnSale
            // 
            this.miReturnSale.Image = global::Payments.Properties.Resources.pay_return;
            this.miReturnSale.Name = "miReturnSale";
            this.miReturnSale.Size = new System.Drawing.Size(169, 22);
            this.miReturnSale.Text = "Возврат продажи";
            this.miReturnSale.Click += new System.EventHandler(this.miStornoSale_Click);
            // 
            // miRefresh
            // 
            this.miRefresh.Image = global::Payments.Properties.Resources.refresh;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(169, 22);
            this.miRefresh.Text = "Обновить";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // miExport
            // 
            this.miExport.Image = global::Payments.Properties.Resources.save_export;
            this.miExport.Name = "miExport";
            this.miExport.Size = new System.Drawing.Size(169, 22);
            this.miExport.Text = "Экспорт";
            this.miExport.Click += new System.EventHandler(this.miExport_Click);
            // 
            // paymentBindingSource
            // 
            this.paymentBindingSource.DataMember = "Payment";
            this.paymentBindingSource.DataSource = this.paymentsDataSet;
            // 
            // paymentsDataSet
            // 
            this.paymentsDataSet.DataSetName = "PaymentsDataSet";
            this.paymentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewPayment
            // 
            this.gridViewPayment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Payment,
            this.colID_Payment_Class,
            this.colPayment_Date,
            this.colPayment_Cash,
            this.colPayment_Summ,
            this.colPayment_Commission,
            this.colPayment_SummTotal,
            this.colPayment_Charge,
            this.colComment,
            this.colCreatedBy,
            this.colDepartment_Number,
            this.colIsStorno,
            this.colOperation_Number,
            this.colSession_Number,
            this.colID_Cash_Type,
            this.colDocumentNumber,
            this.colReturnDocumentNumber});
            this.gridViewPayment.GridControl = this.gridPayment;
            this.gridViewPayment.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Payment_Summ", null, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "ID_Payment", null, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Payment_Summ", this.colPayment_Summ, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Payment_Cash", this.colPayment_Cash, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Payment_Charge", this.colPayment_Charge, "")});
            this.gridViewPayment.Name = "gridViewPayment";
            this.gridViewPayment.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPayment.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPayment.OptionsBehavior.Editable = false;
            this.gridViewPayment.OptionsSelection.MultiSelect = true;
            this.gridViewPayment.OptionsView.ShowFooter = true;
            this.gridViewPayment.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colID_Payment, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewPayment.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewPayment_RowStyle);
            this.gridViewPayment.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewPayment_FocusedRowChanged);
            // 
            // colID_Payment
            // 
            this.colID_Payment.Caption = "ИД";
            this.colID_Payment.FieldName = "ID_Payment";
            this.colID_Payment.Name = "colID_Payment";
            this.colID_Payment.OptionsColumn.ReadOnly = true;
            this.colID_Payment.Visible = true;
            this.colID_Payment.VisibleIndex = 0;
            // 
            // colID_Payment_Class
            // 
            this.colID_Payment_Class.Caption = "Тип платежа";
            this.colID_Payment_Class.ColumnEdit = this.riPaymentClass;
            this.colID_Payment_Class.FieldName = "ID_Payment_Class";
            this.colID_Payment_Class.Name = "colID_Payment_Class";
            this.colID_Payment_Class.Visible = true;
            this.colID_Payment_Class.VisibleIndex = 2;
            // 
            // riPaymentClass
            // 
            this.riPaymentClass.AutoHeight = false;
            this.riPaymentClass.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riPaymentClass.DataSource = this.paymentClassBindingSource;
            this.riPaymentClass.DisplayMember = "Name";
            this.riPaymentClass.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.riPaymentClass.Name = "riPaymentClass";
            this.riPaymentClass.ShowFooter = false;
            this.riPaymentClass.ShowHeader = false;
            this.riPaymentClass.ValueMember = "ID_Payment_Class";
            // 
            // paymentClassBindingSource
            // 
            this.paymentClassBindingSource.DataMember = "Payment_Class";
            this.paymentClassBindingSource.DataSource = this.paymentsDataSet;
            // 
            // colPayment_Date
            // 
            this.colPayment_Date.Caption = "Дата платежа";
            this.colPayment_Date.FieldName = "Payment_Date";
            this.colPayment_Date.Name = "colPayment_Date";
            this.colPayment_Date.Visible = true;
            this.colPayment_Date.VisibleIndex = 1;
            // 
            // colPayment_Cash
            // 
            this.colPayment_Cash.Caption = "Наличные";
            this.colPayment_Cash.DisplayFormat.FormatString = "c";
            this.colPayment_Cash.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPayment_Cash.FieldName = "Payment_Cash";
            this.colPayment_Cash.Name = "colPayment_Cash";
            this.colPayment_Cash.SummaryItem.DisplayFormat = "SUM={0:c}";
            this.colPayment_Cash.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPayment_Cash.Visible = true;
            this.colPayment_Cash.VisibleIndex = 3;
            // 
            // colPayment_Summ
            // 
            this.colPayment_Summ.Caption = "Сумма платежа";
            this.colPayment_Summ.DisplayFormat.FormatString = "c";
            this.colPayment_Summ.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPayment_Summ.FieldName = "Payment_Summ";
            this.colPayment_Summ.Name = "colPayment_Summ";
            this.colPayment_Summ.SummaryItem.DisplayFormat = "SUM={0:c}";
            this.colPayment_Summ.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPayment_Summ.Visible = true;
            this.colPayment_Summ.VisibleIndex = 4;
            // 
            // colPayment_Commission
            // 
            this.colPayment_Commission.Caption = "Комиссия";
            this.colPayment_Commission.DisplayFormat.FormatString = "C";
            this.colPayment_Commission.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPayment_Commission.FieldName = "Payment_Commission";
            this.colPayment_Commission.Name = "colPayment_Commission";
            this.colPayment_Commission.Visible = true;
            this.colPayment_Commission.VisibleIndex = 5;
            // 
            // colPayment_SummTotal
            // 
            this.colPayment_SummTotal.Caption = "Общая сумма";
            this.colPayment_SummTotal.DisplayFormat.FormatString = "C";
            this.colPayment_SummTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPayment_SummTotal.FieldName = "Payment_SummTotal";
            this.colPayment_SummTotal.Name = "colPayment_SummTotal";
            this.colPayment_SummTotal.Visible = true;
            this.colPayment_SummTotal.VisibleIndex = 6;
            // 
            // colPayment_Charge
            // 
            this.colPayment_Charge.Caption = "Сдача";
            this.colPayment_Charge.DisplayFormat.FormatString = "c";
            this.colPayment_Charge.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPayment_Charge.FieldName = "Payment_Charge";
            this.colPayment_Charge.Name = "colPayment_Charge";
            this.colPayment_Charge.SummaryItem.DisplayFormat = "SUM={0:c}";
            this.colPayment_Charge.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPayment_Charge.Visible = true;
            this.colPayment_Charge.VisibleIndex = 7;
            // 
            // colComment
            // 
            this.colComment.Caption = "Комментарий";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 8;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Оператор";
            this.colCreatedBy.ColumnEdit = this.riUser;
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 9;
            // 
            // riUser
            // 
            this.riUser.AutoHeight = false;
            this.riUser.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riUser.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_User", "ID_User", 62, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Login", "Login", 35, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fiscal_User_ID", "Fiscal_User_ID", 81, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fiscal_User_Password", "Fiscal_User_Password", 116, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CreatedDate", "Created Date", 75, DevExpress.Utils.FormatType.DateTime, "dd.MM.yyyy", false, DevExpress.Utils.HorzAlignment.Near)});
            this.riUser.DataSource = this.userBindingSource;
            this.riUser.DisplayMember = "Name";
            this.riUser.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.riUser.Name = "riUser";
            this.riUser.ShowFooter = false;
            this.riUser.ShowHeader = false;
            this.riUser.ValueMember = "ID_User";
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "User";
            this.userBindingSource.DataSource = this.paymentsDataSet;
            // 
            // colDepartment_Number
            // 
            this.colDepartment_Number.Caption = "Номер отдела";
            this.colDepartment_Number.FieldName = "Department_Number";
            this.colDepartment_Number.Name = "colDepartment_Number";
            // 
            // colIsStorno
            // 
            this.colIsStorno.Caption = "Сторно";
            this.colIsStorno.FieldName = "IsStorno";
            this.colIsStorno.Name = "colIsStorno";
            // 
            // colOperation_Number
            // 
            this.colOperation_Number.Caption = "Номер операции";
            this.colOperation_Number.FieldName = "Operation_Number";
            this.colOperation_Number.Name = "colOperation_Number";
            this.colOperation_Number.Visible = true;
            this.colOperation_Number.VisibleIndex = 11;
            // 
            // colSession_Number
            // 
            this.colSession_Number.Caption = "Номер смены";
            this.colSession_Number.FieldName = "Session_Number";
            this.colSession_Number.Name = "colSession_Number";
            this.colSession_Number.Visible = true;
            this.colSession_Number.VisibleIndex = 10;
            // 
            // colID_Cash_Type
            // 
            this.colID_Cash_Type.Caption = "Тип оплаты";
            this.colID_Cash_Type.ColumnEdit = this.riCashType;
            this.colID_Cash_Type.FieldName = "ID_Cash_Type";
            this.colID_Cash_Type.Name = "colID_Cash_Type";
            this.colID_Cash_Type.OptionsColumn.AllowEdit = false;
            this.colID_Cash_Type.OptionsColumn.ReadOnly = true;
            this.colID_Cash_Type.Visible = true;
            this.colID_Cash_Type.VisibleIndex = 12;
            // 
            // riCashType
            // 
            this.riCashType.AutoHeight = false;
            this.riCashType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riCashType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Cash_Type", "ID_Cash_Type", 94, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riCashType.DataSource = this.cashTypeBindingSource;
            this.riCashType.DisplayMember = "Name";
            this.riCashType.Name = "riCashType";
            this.riCashType.NullText = "";
            this.riCashType.ValueMember = "ID_Cash_Type";
            // 
            // cashTypeBindingSource
            // 
            this.cashTypeBindingSource.DataMember = "Cash_Type";
            this.cashTypeBindingSource.DataSource = this.paymentsDataSet;
            // 
            // colDocumentNumber
            // 
            this.colDocumentNumber.Caption = "Номер ФД";
            this.colDocumentNumber.FieldName = "DocumentNumber";
            this.colDocumentNumber.Name = "colDocumentNumber";
            this.colDocumentNumber.Visible = true;
            this.colDocumentNumber.VisibleIndex = 13;
            // 
            // colReturnDocumentNumber
            // 
            this.colReturnDocumentNumber.Caption = "Номер ФД (возврата)";
            this.colReturnDocumentNumber.FieldName = "ReturnDocumentNumber";
            this.colReturnDocumentNumber.Name = "colReturnDocumentNumber";
            // 
            // ddPaymentClass
            // 
            this.ddPaymentClass.Location = new System.Drawing.Point(89, 15);
            this.ddPaymentClass.Name = "ddPaymentClass";
            this.ddPaymentClass.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddPaymentClass.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Payment_Class", "ID_Payment_Class", 113, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MakeCheckCopy", "Make Check Copy", 95, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.ddPaymentClass.Properties.DataSource = this.paymentClassBindingSource;
            this.ddPaymentClass.Properties.DisplayMember = "Name";
            this.ddPaymentClass.Properties.NullText = "";
            this.ddPaymentClass.Properties.PopupWidth = 154;
            this.ddPaymentClass.Properties.ShowFooter = false;
            this.ddPaymentClass.Properties.ShowHeader = false;
            this.ddPaymentClass.Properties.ValueMember = "ID_Payment_Class";
            this.ddPaymentClass.Size = new System.Drawing.Size(154, 20);
            this.ddPaymentClass.TabIndex = 1;
            // 
            // lblPaymentClass
            // 
            this.lblPaymentClass.AutoSize = true;
            this.lblPaymentClass.Location = new System.Drawing.Point(8, 18);
            this.lblPaymentClass.Name = "lblPaymentClass";
            this.lblPaymentClass.Size = new System.Drawing.Size(75, 13);
            this.lblPaymentClass.TabIndex = 2;
            this.lblPaymentClass.Text = "Тип платежа:";
            // 
            // btnInitPayment
            // 
            this.btnInitPayment.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnInitPayment.Image = global::Payments.Properties.Resources.cash_register;
            this.btnInitPayment.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnInitPayment.Location = new System.Drawing.Point(255, 13);
            this.btnInitPayment.Name = "btnInitPayment";
            this.btnInitPayment.Size = new System.Drawing.Size(49, 23);
            this.btnInitPayment.TabIndex = 0;
            this.btnInitPayment.ToolTip = "Добавить платеж";
            this.btnInitPayment.Click += new System.EventHandler(this.btnInitPayment_Click);
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 53);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridPayment);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.tabPaymentDetail);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(730, 553);
            this.splitContainerControl1.SplitterPosition = 400;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tabPaymentDetail
            // 
            this.tabPaymentDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPaymentDetail.Location = new System.Drawing.Point(0, 0);
            this.tabPaymentDetail.Name = "tabPaymentDetail";
            this.tabPaymentDetail.SelectedTabPage = this.xtraTabPage1;
            this.tabPaymentDetail.Size = new System.Drawing.Size(730, 147);
            this.tabPaymentDetail.TabIndex = 0;
            this.tabPaymentDetail.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridPaymentParameter1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(723, 118);
            this.xtraTabPage1.Text = "Параметры платежа";
            // 
            // gridPaymentParameter1
            // 
            this.gridPaymentParameter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPaymentParameter1.Location = new System.Drawing.Point(0, 0);
            this.gridPaymentParameter1.Name = "gridPaymentParameter1";
            this.gridPaymentParameter1.Size = new System.Drawing.Size(723, 118);
            this.gridPaymentParameter1.TabIndex = 0;
            // 
            // deBeginDate
            // 
            this.deBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deBeginDate.EditValue = null;
            this.deBeginDate.Location = new System.Drawing.Point(484, 16);
            this.deBeginDate.Name = "deBeginDate";
            this.deBeginDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deBeginDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deBeginDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deBeginDate.Size = new System.Drawing.Size(97, 20);
            this.deBeginDate.TabIndex = 5;
            this.deBeginDate.EditValueChanged += new System.EventHandler(this.deBeginDate_EditValueChanged);
            // 
            // deEndDate
            // 
            this.deEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deEndDate.EditValue = null;
            this.deEndDate.Location = new System.Drawing.Point(613, 16);
            this.deEndDate.Name = "deEndDate";
            this.deEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEndDate.Size = new System.Drawing.Size(97, 20);
            this.deEndDate.TabIndex = 6;
            this.deEndDate.EditValueChanged += new System.EventHandler(this.deEndDate_EditValueChanged);
            // 
            // lblPaymentPeriodBegin
            // 
            this.lblPaymentPeriodBegin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPaymentPeriodBegin.AutoSize = true;
            this.lblPaymentPeriodBegin.Location = new System.Drawing.Point(416, 19);
            this.lblPaymentPeriodBegin.Name = "lblPaymentPeriodBegin";
            this.lblPaymentPeriodBegin.Size = new System.Drawing.Size(64, 13);
            this.lblPaymentPeriodBegin.TabIndex = 7;
            this.lblPaymentPeriodBegin.Text = "Платежи с:";
            // 
            // lblPaymentPeriodEnd
            // 
            this.lblPaymentPeriodEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPaymentPeriodEnd.AutoSize = true;
            this.lblPaymentPeriodEnd.Location = new System.Drawing.Point(589, 19);
            this.lblPaymentPeriodEnd.Name = "lblPaymentPeriodEnd";
            this.lblPaymentPeriodEnd.Size = new System.Drawing.Size(19, 13);
            this.lblPaymentPeriodEnd.TabIndex = 8;
            this.lblPaymentPeriodEnd.Text = "по";
            // 
            // GridPayments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblPaymentPeriodEnd);
            this.Controls.Add(this.lblPaymentPeriodBegin);
            this.Controls.Add(this.deEndDate);
            this.Controls.Add(this.deBeginDate);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnInitPayment);
            this.Controls.Add(this.lblPaymentClass);
            this.Controls.Add(this.ddPaymentClass);
            this.Name = "GridPayments";
            this.Size = new System.Drawing.Size(730, 609);
            ((System.ComponentModel.ISupportInitialize)(this.gridPayment)).EndInit();
            this.contextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.paymentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPaymentClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentClassBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCashType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPaymentClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPaymentDetail)).EndInit();
            this.tabPaymentDetail.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseControls.BaseGrid gridPayment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPayment;
        private System.Windows.Forms.BindingSource paymentBindingSource;
        private PaymentsDataSet paymentsDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Payment;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Payment_Class;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment_Cash;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment_Summ;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment_Charge;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riPaymentClass;
        private System.Windows.Forms.BindingSource paymentClassBindingSource;
        private DevExpress.XtraEditors.LookUpEdit ddPaymentClass;
        private System.Windows.Forms.Label lblPaymentClass;
        private DevExpress.XtraEditors.SimpleButton btnInitPayment;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riUser;
        private System.Windows.Forms.BindingSource userBindingSource;
        private PaymentsDataSetTableAdapters.UserTableAdapter userTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl tabPaymentDetail;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private GridPaymentParameter gridPaymentParameter1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem miReturnSale;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment_Number;
        private DevExpress.XtraGrid.Columns.GridColumn colIsStorno;
        private DevExpress.XtraGrid.Columns.GridColumn colOperation_Number;
        private DevExpress.XtraGrid.Columns.GridColumn colSession_Number;
        private DevExpress.XtraEditors.DateEdit deBeginDate;
        private DevExpress.XtraEditors.DateEdit deEndDate;
        private System.Windows.Forms.Label lblPaymentPeriodBegin;
        private System.Windows.Forms.Label lblPaymentPeriodEnd;
        private System.Windows.Forms.ToolStripMenuItem miRefresh;
        private System.Windows.Forms.ToolStripMenuItem miExport;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment_Commission;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment_SummTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Cash_Type;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riCashType;
        private System.Windows.Forms.BindingSource cashTypeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnDocumentNumber;
    }
}
