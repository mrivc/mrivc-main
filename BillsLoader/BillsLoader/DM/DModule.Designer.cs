﻿namespace BillsLoader.DM
{
    partial class DModule
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DModule));
            this.sqlMainConnection = new System.Data.SqlClient.SqlConnection();
            this.cmdAnyCommand = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            this.daTmpLoad = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand2 = new System.Data.SqlClient.SqlCommand();
            this.daErrorTable = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand3 = new System.Data.SqlClient.SqlCommand();
            this.daPackType = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlSubConnection = new System.Data.SqlClient.SqlConnection();
            this.daSberSaldoReestrType = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand5 = new System.Data.SqlClient.SqlCommand();
            this.daSberSaldo = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdCurrentDeriveCompany = new System.Data.SqlClient.SqlCommand();
            this.cmdLogBillLoader = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand6 = new System.Data.SqlClient.SqlCommand();
            this.daDetectBillFilter = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            this.cmdSubCommand = new System.Data.SqlClient.SqlCommand();
            // 
            // sqlMainConnection
            // 
            this.sqlMainConnection.ConnectionString = "Data Source=srv-MRIVC5-1;Initial Catalog=Murmansk;Persist Security Info=True;User" +
    " ID=bobrovsky;Password=speaker";
            this.sqlMainConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // cmdAnyCommand
            // 
            this.cmdAnyCommand.CommandTimeout = 0;
            this.cmdAnyCommand.Connection = this.sqlMainConnection;
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "select *\r\nfrom #tmpLoad";
            this.sqlSelectCommand1.Connection = this.sqlMainConnection;
            // 
            // daTmpLoad
            // 
            this.daTmpLoad.SelectCommand = this.sqlSelectCommand1;
            // 
            // sqlSelectCommand2
            // 
            this.sqlSelectCommand2.CommandText = "select *\r\nfrom #ErrT";
            this.sqlSelectCommand2.Connection = this.sqlMainConnection;
            // 
            // daErrorTable
            // 
            this.daErrorTable.SelectCommand = this.sqlSelectCommand2;
            // 
            // sqlSelectCommand3
            // 
            this.sqlSelectCommand3.CommandText = "select * from dbo.pack_type";
            this.sqlSelectCommand3.Connection = this.sqlMainConnection;
            // 
            // daPackType
            // 
            this.daPackType.SelectCommand = this.sqlSelectCommand3;
            this.daPackType.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Pack_Type", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Pack_Type", "ID_Pack_Type"),
                        new System.Data.Common.DataColumnMapping("Pack_Type", "Pack_Type"),
                        new System.Data.Common.DataColumnMapping("Percent", "Percent"),
                        new System.Data.Common.DataColumnMapping("Begin_Date", "Begin_Date"),
                        new System.Data.Common.DataColumnMapping("End_Date", "End_Date")})});
            // 
            // sqlSelectCommand4
            // 
            this.sqlSelectCommand4.CommandText = resources.GetString("sqlSelectCommand4.CommandText");
            this.sqlSelectCommand4.Connection = this.sqlSubConnection;
            // 
            // sqlSubConnection
            // 
            this.sqlSubConnection.ConnectionString = "Data Source=srv-MRIVC5-1;Initial Catalog=Murmansk;Persist Security Info=True;User" +
    " ID=bobrovsky;Password=speaker";
            this.sqlSubConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // daSberSaldoReestrType
            // 
            this.daSberSaldoReestrType.SelectCommand = this.sqlSelectCommand4;
            // 
            // sqlSelectCommand5
            // 
            this.sqlSelectCommand5.CommandText = "select *\r\nfrom town.Sber_saldo_201406";
            this.sqlSelectCommand5.Connection = this.sqlSubConnection;
            // 
            // daSberSaldo
            // 
            this.daSberSaldo.SelectCommand = this.sqlSelectCommand5;
            this.daSberSaldo.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Sber_saldo_201406", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_client", "id_client"),
                        new System.Data.Common.DataColumnMapping("id_derive_company", "id_derive_company"),
                        new System.Data.Common.DataColumnMapping("id_account", "id_account"),
                        new System.Data.Common.DataColumnMapping("account", "account"),
                        new System.Data.Common.DataColumnMapping("address", "address"),
                        new System.Data.Common.DataColumnMapping("bill_filter_code", "bill_filter_code"),
                        new System.Data.Common.DataColumnMapping("prefix", "prefix"),
                        new System.Data.Common.DataColumnMapping("sum_tariff_fact", "sum_tariff_fact")})});
            // 
            // cmdCurrentDeriveCompany
            // 
            this.cmdCurrentDeriveCompany.CommandText = resources.GetString("cmdCurrentDeriveCompany.CommandText");
            this.cmdCurrentDeriveCompany.CommandTimeout = 0;
            this.cmdCurrentDeriveCompany.Connection = this.sqlSubConnection;
            this.cmdCurrentDeriveCompany.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Account", System.Data.SqlDbType.VarChar, 25, "Account")});
            // 
            // cmdLogBillLoader
            // 
            this.cmdLogBillLoader.CommandText = "town.Log_BillLoader_Insert";
            this.cmdLogBillLoader.CommandTimeout = 0;
            this.cmdLogBillLoader.CommandType = System.Data.CommandType.StoredProcedure;
            this.cmdLogBillLoader.Connection = this.sqlSubConnection;
            this.cmdLogBillLoader.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Pack_Type", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Pack_Number", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@Pack_Date", System.Data.SqlDbType.DateTime),
            new System.Data.SqlClient.SqlParameter("@FileName", System.Data.SqlDbType.VarChar, 500),
            new System.Data.SqlClient.SqlParameter("@PayCount", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@PaySum", System.Data.SqlDbType.Money),
            new System.Data.SqlClient.SqlParameter("@StartLoadTime", System.Data.SqlDbType.DateTime),
            new System.Data.SqlClient.SqlParameter("@EndLoadTime", System.Data.SqlDbType.DateTime),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 500)});
            // 
            // sqlSelectCommand6
            // 
            this.sqlSelectCommand6.CommandText = "town.Detect_Bill_Filter_By_Account";
            this.sqlSelectCommand6.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand6.Connection = this.sqlSubConnection;
            this.sqlSelectCommand6.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@account", System.Data.SqlDbType.VarChar, 12),
            new System.Data.SqlClient.SqlParameter("@id_client", System.Data.SqlDbType.Int, 4)});
            // 
            // daDetectBillFilter
            // 
            this.daDetectBillFilter.SelectCommand = this.sqlSelectCommand6;
            this.daDetectBillFilter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Detect_Bill_Filter_By_Account", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("bill_filter_code", "bill_filter_code"),
                        new System.Data.Common.DataColumnMapping("derive_company_code", "derive_company_code")})});
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.ConnectionString = "Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Persist Security Info=True;User I" +
    "D=bobrovsky;Password=speaker";
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
            // 
            // cmdSubCommand
            // 
            this.cmdSubCommand.Connection = this.sqlSubConnection;

        }

        #endregion

        private System.Data.SqlClient.SqlConnection sqlMainConnection;
        private System.Data.SqlClient.SqlCommand cmdAnyCommand;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private System.Data.SqlClient.SqlDataAdapter daTmpLoad;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand2;
        private System.Data.SqlClient.SqlDataAdapter daErrorTable;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand3;
        private System.Data.SqlClient.SqlDataAdapter daPackType;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand4;
        private System.Data.SqlClient.SqlDataAdapter daSberSaldoReestrType;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand5;
        private System.Data.SqlClient.SqlDataAdapter daSberSaldo;
        private System.Data.SqlClient.SqlConnection sqlSubConnection;
        private System.Data.SqlClient.SqlCommand cmdCurrentDeriveCompany;
        private System.Data.SqlClient.SqlCommand cmdLogBillLoader;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand6;
        private System.Data.SqlClient.SqlConnection sqlConnection1;
        private System.Data.SqlClient.SqlDataAdapter daDetectBillFilter;
        private System.Data.SqlClient.SqlCommand cmdSubCommand;
    }
}
