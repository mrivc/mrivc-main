﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using Xades.Abstractions;
using Xades.Exceptions;
using Xades.Helpers;
using GisGkh.Configurations.Options;
using GisGkh.Configurations.Sections;
using GisGkh.Helpers;
using GisGkh.Infrastructure;

namespace GisGkh.Commands
{
    public abstract class GisCommandBase<TOption> : XadesCommandBase<TOption> where TOption : XadesOptionBase
    {
        static string _senderID;
        static string _serviceName;
        static string _methodName;

        private readonly GisServiceConfiguration _serviceConfig;
        protected abstract bool IsSignatureRequired { get; }

        protected GisCommandBase(TOption option, IXadesService xadesService, SigningConfiguration signingConfig, GisServiceConfiguration serviceConfig) 
            : base(option, xadesService, signingConfig)
        {
            _serviceConfig = serviceConfig;
        }

        protected string SendRequest(string senderID, string serviceName, string methodName, object values, string outputFile)
        {
            _senderID = senderID;
            _serviceName = serviceName;
            _methodName = methodName;

            var service = _serviceConfig.Services[serviceName];
            if (service == null)
            {
                throw new ConfigurationErrorsException(string.Format("Конфигурация для сервиса {0} не задана", serviceName));
            }

            var method = service.Methods[methodName];
            if (method == null)
            {
                throw new ConfigurationErrorsException(string.Format("Конфигурация для метода {0} не задана",methodName));
            }

            //if (method.RequiredBody && !values.Any())
            //{
            //    throw new ConfigurationErrorsException(string.Format("Метод {0} имеет обязательные параметры для запроса (-с файл)", methodName));
            //}


            Info("Создание запроса...");
            GisSoapFormatter soapFormatter = null;
            if (values is IEnumerable<Tuple<string, string>>)
            {
                soapFormatter = new GisSoapFormatter
                {
                    Template = PathHelper.ToAppAbsolutePath(method.Template),
                    SchemaVersion = _serviceConfig.SchemaVersion,
                    AddSenderId = service.AddSenderId,
                    ValuesDictionary = values as IEnumerable<Tuple<string, string>>,
                    SenderId = senderID,//_serviceConfig.SenderId,
                    Config = _serviceConfig.SoapConfiguration
                };
            }
            else if (values is string)
            {
                soapFormatter = new GisSoapFormatter
                {
                    Template = PathHelper.ToAppAbsolutePath(method.Template),
                    SchemaVersion = _serviceConfig.SchemaVersion,
                    AddSenderId = service.AddSenderId,
                    XmlBody = values as string,
                    SenderId = senderID,//_serviceConfig.SenderId,
                    Config = _serviceConfig.SoapConfiguration
                };
            }

            if (soapFormatter == null)
            {
                return "";
            }

            var soapString = soapFormatter.GetSoapRequest();

            if (IsSignatureRequired &&  service.AddSignature)
            {
                Info("Добавление подписи в запрос...");
                var soapXml = XmlDocumentHelper.Create(soapString);
                soapString = SignNode(soapXml, Constants.SoapContentXpath);
            }
            var response = "";//Буторин
            IEnumerable<Tuple<string, string>> resultValues;
            try
            {
                Info(string.Format("Отправка запроса по адресу: {0}{1}/{2}", _serviceConfig.BaseUrl, service.Path, method.Action));
                response = CallGisService(_serviceConfig.BaseUrl + service.Path, method.Action, soapString, Option.BasicAuthorization);
                //MRIVC
                //Info("Обработка ответа на запрос...");
                resultValues = ProcessSoapResponse(response);
            }
            catch (XadesBesValidationException ex)
            {
                //Error(string.Format("Подпись ответа от ГИС ЖКХ не прошла проверку: {0}", ex.Message), ex);
                return "";
            }
            catch (WebException ex)
            {
                var details = ex.Message;
                var code = "";

                if (ex.Message.Contains("(504)"))
                {
                    code = "504";
                }

                if (ex.Message.Contains("(502)"))
                {
                    code = "502";
                }

                if (ex.Message.Contains("(404)"))
                {
                    code = "404";
                }

                if (ex.Message.Contains("Unable to connect to the remote server"))
                {
                    code = "MRIVC_NO_CONNECTION";
                }

                string messageGuid = null;

                if (ex.Response != null)
                {
                    using (var streamWriter = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        response = streamWriter.ReadToEnd();

                        Tuple<string, string> err = null;

                        try
                        {
                            err = MrivcStatic.GetErrorFromResponse(response);
                        }
                        catch (Exception)
                        { }

                        if (err != null)
                            details = err.Item2;
                        else
                            details = ex.Message;

                        if (err != null)
                            code = err.Item1;

                        //resultValues = ProcessSoapResponse(response);
                    }

                    messageGuid = MrivcStatic.GetMessageGuidByGetStateResponse(soapString);
                }

                soapString = MrivcStatic.IndentXml(soapString);
                //response = MrivcStatic.IndentXml(response);

                var mcex = new MrivcCommandException(code, details, _senderID, _serviceName, _methodName);
                mcex.SendRequest = soapString;
                mcex.MessageGUID = messageGuid;
                mcex.Response = response;
                throw mcex;
            }

            Info(string.Format("Сохранение ответа в файл {0}...", outputFile));


            //foreach (Tuple<string, string> res in resultValues)
            //{
            //    MrivcStatic.AddToOutput($"{res.Item1}={res.Item2}");
            //}

            //Success("Запрос успешно выполнен");

            MrivcStatic.CurrentSoapRequest = MrivcStatic.IndentXml(soapString);

            return response;

            //Helpers.CsvHelper.WriteCsv(outputFile, resultValues);
            
        }

        private IEnumerable<Tuple<string, string>> ProcessSoapResponse(string response)
        {
            var soapXml = XmlDocumentHelper.Create(response);

            var manager = soapXml.CreateNamespaceManager();
            var bodyResponse = soapXml.SelectSingleNode(Constants.SoapContentXpathNew, manager);

            var idAttribute = bodyResponse.Attributes["Id"];

            //MRIVC
            //if (idAttribute != null && !string.IsNullOrEmpty(idAttribute.Value) && bodyResponse.ChildNodes.OfType<XmlNode>().Any(x => x.LocalName == Constants.SignatureName))
            //{
            //    Info("Проверка подписи ответа...");
            //    Validate(soapXml, idAttribute.Value);
            //}
            //else
            //{
            //    Info("Ответ не содержит подписи");
            //}

            return FindXmlDataNode(bodyResponse, string.Format("{0}/", bodyResponse.Name));
        }

        private static IEnumerable<Tuple<string, string>> FindXmlDataNode(XmlNode node, string currentPath = "")
        {
            var childs = node.ChildNodes.OfType<XmlNode>()
                .Where( x => x.NodeType == XmlNodeType.Element && x.LocalName != Constants.SignatureName).ToList();
            if (childs.Count == 0)
            {
                throw new InvalidOperationException("В ответе на запрос не найден узел с данными");
            }

            //TODO: временное решение
            return childs.Count > 1 
                ? ParseXmlDataNode(childs, currentPath)
                : FindXmlDataNode(childs.First(), string.Format("{0}{1}/", currentPath, childs.First().Name));
        }

        private static IEnumerable<Tuple<string, string>> ParseXmlDataNode(IEnumerable<XmlNode> nodeList, string currentPath)
        {
            foreach(var node in nodeList)
            {
                if (node.NodeType == XmlNodeType.Text)
                {
                    yield return Tuple.Create(currentPath.Trim('/'), node.InnerText);
                }
                else
                {
                    var childs = node.ChildNodes
                        .OfType<XmlNode>()
                        .Where(x => (x.NodeType == XmlNodeType.Element || x.NodeType == XmlNodeType.Text) && x.LocalName != Constants.SignatureName);
                    var data = ParseXmlDataNode(childs, string.Format("{0}{1}/", currentPath, node.Name));
                    foreach (var item in data)
                    {
                        yield return item;
                    }
                }
            }
        }

        private static string CallGisService(string url, string action, string body, string basicAuth)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            //webRequest.Headers.Add("X-Client-Cert-Fingerprint", "1FB83B616F1A0F33746200A61500CE05086307BE");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";

            var encodedAuth = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(basicAuth));
            webRequest.Headers.Add("Authorization", string.Format("Basic {0}", encodedAuth));
            //MRIVC Буторин
            webRequest.ContinueTimeout = 300000;
            webRequest.Timeout = 300000;

            //MRIVC Преобразуем Soap-запрос в читаемый вид с отступами
            XmlWriterSettings ws = new XmlWriterSettings(){ Indent = true };
            var xe = System.Xml.Linq.XElement.Parse(body);

            //var sb = new StringBuilder();
            //using (XmlWriter xw = XmlWriter.Create(sb, ws))
            //    xe.Save(xw);

            var mrivcXe = System.Xml.Linq.XElement.Parse(body);

            using (Stream stream = webRequest.GetRequestStream())
            {
                var buffer = Encoding.UTF8.GetBytes(body);
                stream.Write(buffer, 0, buffer.Length);
            }

            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()
                    //, Encoding.GetEncoding(1251)
                    ))
                {
                    var response = rd.ReadToEnd();
                    var jaxpError = "Error while serializing JAXB object";
                    if (response.Contains(jaxpError))
                    {
                        var mcex = new MrivcCommandException("JAXB", jaxpError, _senderID, _serviceName, _methodName);
                        var messageGuid = MrivcStatic.GetMessageGuidByGetStateResponse(mrivcXe.ToString());
                        mcex.SendRequest = mrivcXe.ToString();
                        mcex.MessageGUID = messageGuid;
                        throw mcex;
                    }
                    return response;
                }
            }
        }
    }
}