﻿namespace PaymentStatementParser
{
    public static class Consts
    {
        public enum RightAccountSize
        {
            ONE = 18,
            TWO = 24
        }

        public const int ACCOUNT_SIZE = 12;
        public const int ID_PROVIDER_POSITION = 0;
        public const int ID_PROVIDER_LENGHT = 4;
        public const int CODE_POSITION = 4;
        public const int CODE_LENGHT = 2;
    }
}
