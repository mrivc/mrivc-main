﻿namespace EllisBillFilter.Helpers
{
    using EllisBillFilter.DS;

    using FastReport;

    public static class FRHelper
    {
        public static void Execute(string reportName)
        {
            Report report1 = new Report();
            report1.LoadFromString(DAccess.Instance.GetEllisReport(reportName));
            report1.Show();
        }
    }
}
