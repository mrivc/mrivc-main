using System;
using System.Collections.Generic;
using Xades.Abstractions;
using GisGkh.Configurations.Options;
using GisGkh.Configurations.Sections;
using GisGkh.Infrastructure;

namespace GisGkh.Commands
{
    public class GetStateCommand : GisCommandBase<GetStateOptions>
    {
        public GetStateCommand(GetStateOptions option, IXadesService xadesService, SigningConfiguration signingConfig, GisServiceConfiguration serviceConfig)
            : base(option, xadesService, signingConfig, serviceConfig)
        {
        }

        protected override bool IsSignatureRequired { get { return false; } }

        protected override string OnExecute(GetStateOptions option, string xmlBody, string senderID)
        {
            //var valuesDictionary = new[] { new Tuple<string, string>(Constants.MessageGuidXpath, option.MessageGuid) };
            return SendRequest(senderID, option.ServiceName, Constants.GetStateMethodName, xmlBody, option.OutputFileName);
        }
    }
}