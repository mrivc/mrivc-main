﻿using System;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using MrivcControls.Data;

    public partial class FrmPrintHistoryPivotAnalisys : Form
    {
        public FrmPrintHistoryPivotAnalisys()
        {
            InitializeComponent();
            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
        }

        private void pivotPrintHistory1_Load(object sender, EventArgs e)
        {

            pivotPrintHistory1.LoadData();
        }

        private void FrmPrintHistoryPivotAnalisys_Load(object sender, EventArgs e)
        {

            gridPrintHistory1.LoadData();
        }
    }
}
