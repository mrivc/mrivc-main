﻿using System;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
        }

        public string Get(string uri, int start, int end)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            //request.ProtocolVersion = new Version("1.1");

            //request.Headers.Add("X-Upload-OrgPPAGUID", "09738CD5-4E04-4292-8992-4F95E563157A");

            request.Headers.Add("X-Upload-OrgPPAGUID", "09738cd5-4e04-4292-8992-4f95e563157a");

            //request.Headers.Add("X-Upload-Length", "5000000");
            request.AddRange(start, end);

            //request.Headers.Add("Range", "5000000");



            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (WebException wex)
            {

                Console.WriteLine(wex.ToString());
                var headers = wex.Response.Headers.ToString();

                Console.WriteLine(headers);
                if (wex.Response.Headers["X-Upload-Error"] != null)
                {
                    Console.WriteLine(wex.Response.Headers["X-Upload-Error"].ToString());
                }


                return null;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var get = Get("http://localhost:8080/ext-bus-file-store-service/rest/agreements/d7167ba5-31e2-47a2-b095-a60a4a41de04?getfile HTTP/1.1",0, 5242879);

            var get1 = Get("http://localhost:8080/ext-bus-file-store-service/rest/agreements/d7167ba5-31e2-47a2-b095-a60a4a41de04?getfile HTTP/1.1", 5242880, 10485759);

            var get2 = Get("http://localhost:8080/ext-bus-file-store-service/rest/agreements/d7167ba5-31e2-47a2-b095-a60a4a41de04?getfile HTTP/1.1", 10485760, 13624416);
        }
    }
}
