﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class MrivcCommandException : Exception
    {
        string _senderID;
        string _service;
        string _method;
        string _code;

        public string SenderID
        {
            get { return _senderID; }
        }
        public string Service
        {
            get { return _service; }
        }
        public string Method
        {
            get { return _method; }
        }
        public string Code
        {
            get { return _code; }
        }

        public string SendRequest { get; set; }
        public string Response { get; set; }
        public string MessageGUID { get; set; }

        public MrivcCommandException(string code,string message, string senderID, string service, string method) : base(message)
        {
            _senderID = senderID;
            _service = service;
            _method = method;
            _code = code;
        }

        public MrivcCommandException(string message, string senderID, string service, string method) : base(message)
        {
            _senderID = senderID;
            _service = service;
            _method = method;
        }
    }
}
