﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BillsLoader.DM;
using BillsLoader.Logic;
using System.Threading;
using System.Diagnostics;

namespace BillsLoader
{
    using MrivcHelpers.Helpers;
    using System.Data.SqlClient;
    using System.IO;

    public partial class FrmLoad : Form
    {
        ZXTimer timeView;

        string _connectionString;

        public FrmLoad()
        {
            InitializeComponent();
            txtPackNumber.ResetText();
        }

        public FrmLoad(string connectionString)
        {
            if (connectionString.Length > 0)
            {
                DAccess.DataModule.SetConnectionString(connectionString);
                _connectionString = connectionString;
                
            }

            InitializeComponent();
            this.Text = this.Text + " (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + ") - " + DAccess.Instance.PeriodName + " v:" + System.Windows.Forms.Application.ProductVersion;

            timeView = new ZXTimer();
            lblStatus.Text = timeView.StringValue;

            txtPackNumber.ResetText();
            dtpReestrDate.Value = DateTime.Now;
        }

        private void FrmLoad_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mDataSet.Pack_Type1' table. You can move, or remove it, as needed.
            var connString = DAccess.DataModule.GetConnectionString();
            this.pack_Type1TableAdapter.Connection.ConnectionString = connString;
            this.pack_Type1TableAdapter.Fill(this.mDataSet.Pack_Type1);
            SqlDataAdapter daBillFilter = new SqlDataAdapter("select code,bill_filter from dbo.bill_filter order by 1", new SqlConnection(DAccess.DataModule.Connection.ConnectionString));
            daBillFilter.Fill(mDataSet.Bill_Filter);

            mDataSet.Sber_Saldo_Reestr_Type.Merge(DAccess.Instance.SberReestrType);
            mDataSet.Pack_Type.Merge(DAccess.Instance.PackType);
            cbSberReestrType.SelectedValue = -1;
            //this.Text = this.pack_Type1TableAdapter.Connection.ConnectionString.ToString();
        }

        private void txtPackNumber_Enter(object sender, EventArgs e)
        {
            if(txtPackNumber.Value == 0)
                txtPackNumber.ResetText();
        }

        private void btnFilePath_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "(*.xls)|*.xls|All files (*.*)|*.*|(*.txt)|*.txt|(*.dbf)|*.dbf";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.Multiselect = false;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (!openFileDialog1.FileName.ToLower().EndsWith("dbf") && !openFileDialog1.FileName.ToLower().EndsWith("txt")
                        && !openFileDialog1.FileName.ToLower().EndsWith("xls"))
                    {
                        MessageBox.Show("Данный тип файла не поддерживается. Поддерживаются форматы: TXT, DBF, XLS.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    txtFilePath.Text = openFileDialog1.FileName;
                    tbReestrNumber.Text = Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (cbPackType.SelectedValue == null || txtPackNumber.Value == 0 || txtFilePath.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Необходимо указать:\n-путь к файлу реестра;\n-дату пачки;\n-номер пачки;\n-тип пачки.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (dtpReestrDate.Value < DAccess.DataModule.WorkPeriodBeginDate())
            {
                MessageBox.Show("Дата пачки должна быть больше либо равна дате начала рабочего периода.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbReestrNumber.Text.Trim() == "")
            {
                MessageBox.Show("Не заполнен номер реестра.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MeterLoadType meterForLoad = new MeterLoadType(cbHv.Checked, cbGv.Checked, cbGaz.Checked, cbEl.Checked);

            ReestrInfo r;

            var reestrNumber = tbReestrNumber.Text;

            if (cbSberReestrType.SelectedValue != null)
                r = new ReestrInfo((int)cbPackType.SelectedValue, (int)txtPackNumber.Value, dtpReestrDate.Value, txtFilePath.Text, int.Parse(cbSberReestrType.SelectedValue.ToString()), true, meterForLoad, reestrNumber);
            else
                r = new ReestrInfo((int)cbPackType.SelectedValue, (int)txtPackNumber.Value, dtpReestrDate.Value, txtFilePath.Text, -1, false, meterForLoad, reestrNumber);

            if (rbExcelLoad.Checked)
            {
                r.LoadFromExcel = true;
                r.BillFilter = cbBillFilter.SelectedValue.ToString();
                r.TempTableName = "#tempExcelLoading";
            }

            r.Peni = chbPeni.Checked;
           
            TimerStart();
            backgroundWorker1.RunWorkerAsync(r);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //try
            //{
                ReestrInfo r = (ReestrInfo)e.Argument;

                if (ReestrLoader.LoadSberReestr(r, true, true))
                {
                    e.Result = "Реестр загружен успешно."

                    //Добавил Буторин Д.А. 3.07.14
                        //Общая сумма платежей
                    + Environment.NewLine + "Загружено " + r.PayCountLoadedInEllis.ToString() + " платежей (-жа) "
                    + Environment.NewLine + "на сумму " + r.PaySummLoadedInEllis.ToString() + " руб.";
                    //End
                }
                else
                    e.Result = "Во время загрузки реестра возникли неизвестные ошибки!";

                DAccess.DataModule.Connection.Close();//Добавил Буторин Д.А. 3.07.14

            //}
            //catch (Exception ex)
            //{
            //    e.Result = "Ошибка загрузки реестра. \n" + ex.Message;
            //}
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            TimerStop();
            FlashWindow.Flash(this);
            MessageBox.Show(this, e.Result.ToString(), "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #region Timer
        public delegate void InvokeDelegate();
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.BeginInvoke(new InvokeDelegate(SetTimer));
        }

        public void SetTimer() {
            timeView.Tick();
            lblStatus.Text = "Загрузка реестра... " + timeView.StringValue;
        }

        private void TimerStart()
        {
            timeView.Reset();
            timer1.Enabled = true;
        }

        private void TimerStop()
        {
            lblStatus.Text = "Реестр загружен (" + timeView.StringValue + ")";
            timer1.Enabled = false;
        }
        #endregion

        private void btnOpenErrorFile_Click(object sender, EventArgs e)
        {
            if (ReestrLoader.LastErrorFile.Length == 0 || !FileHelper.ExistFile(ReestrLoader.LastErrorFile))
            {
                MessageBox.Show(this, "Файла с ошибками не найдено.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            System.Diagnostics.Process.Start(ReestrLoader.LastErrorFile);            
        }

        private void rbReestrLoad_CheckedChanged(object sender, EventArgs e)
        {
            cbBillFilter.Enabled = rbExcelLoad.Checked;
        }

       
    }
}
