﻿namespace EllisBillFilter
{
    partial class TreeHouseHierarhy
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TreeHouseHierarhy));
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.colname = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colhouse_number = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cmsHouseTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miShowHouseID = new System.Windows.Forms.ToolStripMenuItem();
            this.houseHierarhyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            this.cmsHouseTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.houseHierarhyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // treeList1
            // 
            this.treeList1.Appearance.FocusedCell.BackColor = System.Drawing.Color.LightSteelBlue;
            this.treeList1.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.treeList1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.treeList1.Appearance.FocusedCell.Options.UseFont = true;
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colname,
            this.colhouse_number,
            this.colId});
            this.treeList1.ContextMenuStrip = this.cmsHouseTree;
            this.treeList1.DataSource = this.houseHierarhyBindingSource;
            this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList1.ImageIndexFieldName = "type";
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(0, 0);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.Editable = false;
            this.treeList1.OptionsView.ShowColumns = false;
            this.treeList1.OptionsView.ShowHorzLines = false;
            this.treeList1.ParentFieldName = "id_parent";
            this.treeList1.PreviewFieldName = "name";
            this.treeList1.SelectImageList = this.imageList1;
            this.treeList1.Size = new System.Drawing.Size(699, 510);
            this.treeList1.TabIndex = 0;
            this.treeList1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeList1_KeyDown);
            this.treeList1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeList1_MouseDown);
            // 
            // colname
            // 
            this.colname.FieldName = "name";
            this.colname.Name = "colname";
            this.colname.Visible = true;
            this.colname.VisibleIndex = 0;
            this.colname.Width = 339;
            // 
            // colhouse_number
            // 
            this.colhouse_number.FieldName = "house_number";
            this.colhouse_number.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colhouse_number.Name = "colhouse_number";
            this.colhouse_number.Width = 339;
            // 
            // colId
            // 
            this.colId.FieldName = "id";
            this.colId.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colId.Name = "colId";
            // 
            // cmsHouseTree
            // 
            this.cmsHouseTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miShowHouseID});
            this.cmsHouseTree.Name = "cmsHouseTree";
            this.cmsHouseTree.Size = new System.Drawing.Size(170, 26);
            this.cmsHouseTree.Opening += new System.ComponentModel.CancelEventHandler(this.cmsHouseTree_Opening);
            // 
            // miShowHouseID
            // 
            this.miShowHouseID.Name = "miShowHouseID";
            this.miShowHouseID.Size = new System.Drawing.Size(169, 22);
            this.miShowHouseID.Text = "Показать ID дома";
            this.miShowHouseID.Click += new System.EventHandler(this.miShowHouseID_Click);
            // 
            // houseHierarhyBindingSource
            // 
            this.houseHierarhyBindingSource.DataMember = "HouseHierarhy";
            this.houseHierarhyBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "city.png");
            this.imageList1.Images.SetKeyName(1, "city 2.png");
            this.imageList1.Images.SetKeyName(2, "house_5840.png");
            // 
            // TreeHouseHierarhy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeList1);
            this.Name = "TreeHouseHierarhy";
            this.Size = new System.Drawing.Size(699, 510);
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            this.cmsHouseTree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.houseHierarhyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeList1;
        private System.Windows.Forms.BindingSource houseHierarhyBindingSource;
        private DS.MDataSet mDataSet;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colname;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colhouse_number;
        private System.Windows.Forms.ContextMenuStrip cmsHouseTree;
        private System.Windows.Forms.ToolStripMenuItem miShowHouseID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colId;
    }
}
