	replace(isnull(cast(dbo.roundto(sum(normative_ind),4) as varchar),''),',','.')normative_ind,
replace(isnull(cast(dbo.roundto(sum(normative_odn),4) as varchar),''),',','.')normative_odn,	
replace(isnull(cast(sum(meter_ind) as varchar),''),',','.') meter_ind,
replace(isnull(cast(sum(meter_odn) as varchar),''),',','.') meter_odn,
	
	
	
	(case when pbs.id_service not in (38,39,43,44,45,46,47,48,239)  then vs.normative end) as normative_ind,
	(case when pbs.id_service in (38,39,43,44,45,46,47,48,239) then vs.normative end) as normative_odn,
	(case when pbs.id_service=3 and pb.[СЧ_ХВ]=1
		  then vs.value
		  when pbs.id_service=4 and pb.[СЧ_ГВ]=1
		  then vs.value
		  when pbs.id_service=5 and pb.[СЧ_ГАЗ]=1
		  then vs.value
		  when pbs.id_service=14 and pb.[СЧ_ЭЛЭН]=1
		  then vs.value
		  when pbs.id_service=25 and pb.[СЧ_ЭЛЭН_НОЧЬ]=1
		  then vs.value
		  when pbs.id_service=238 and pb.[СЧ_ГВ]=1
		  then vs.value
		  end) as meter_ind,
	(case when pbs.id_service=2
		  then NULLIF(pb.[HOUSE_METER_TEPLO],0)
		  when pbs.id_service=38
		  then NULLIF(pb.[ХВ_РАСХОД_ДОМ],0)      
		  when pbs.id_service=46
		  then NULLIF(pb.[ТЕПЛО_ПОДОГРЕВ_РАСХОД_ДОМ],0)
		  when pbs.id_service=239
		  then NULLIF(pb.[ГВ_РАСХОД_ДОМ],0)
		  end)  as meter_odn,