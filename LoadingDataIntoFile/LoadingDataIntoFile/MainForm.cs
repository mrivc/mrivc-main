﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataBaseLibrary;
using System.Text;

namespace LoadingDataIntoFile
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            dateTimePicker.MaxDate = DateTime.Today;
        }

        /// <summary>
        /// Сброс формы
        /// </summary>
        private void FormReset()
        {
            foreach (int index in checkedListBox.CheckedIndices)
                checkedListBox.SetItemCheckState(index, CheckState.Unchecked);

            dateTimePicker.Value = DateTime.Now.Date;

            progressBar.Value = 0;

            loadBtn.Enabled = true;
        }

        private string CheckString(string str) => str == "" || str == null ? "NULL" : str;

        private string LoadCommandTextFromFile(string file, int period)
            => File.ReadAllText(file).Replace("#plug", period.ToString());
        private string LoadCommandTextFromFile(string file, DateTime date)
            => File.ReadAllText(file).Replace("#plug", date.ToString("yyyy/MM/dd").Replace(".", ""));

        private int GetPeriodFromDate()
            => 12 * dateTimePicker.Value.Year + dateTimePicker.Value.Month;

        /// <summary>
        /// Ассинхронный запуск выгрузки по нажатию кнопки
        /// </summary>
        private async void loadBtn_Click(object sender, EventArgs e)
        {
            loadBtn.Enabled = false;

            int progressCount = checkedListBox.CheckedItems.Count;

            if (progressCount == 0)
            {
                MessageBox.Show("Выберете как минимум одну таблицу для выгрузки!");
                FormReset();
                return;
            }

            string[,] loadStates = {
                { "#loadPersonalAcc.sql", "1_ls_web", "2", ".csv", "utf-8" },
                { "#loadMeters.sql", "2_meter_web", "2", ".csv", "utf-8" },
                { "#loadMetersMeasure.sql", "3_meter_measure_web", "2", ".csv", "utf-8" },
                { "#loadBalance.sql", "4_balance_web", "1", ".csv", "utf-8" },
                { "#loadBills.sql", dateTimePicker.Value.ToString("dd/MM/yyyy").Replace(".", ""), "2", ".txt", "windows-1251" },
                { "#loadAccGIS.sql", "5_ls_GISGKH_web", "2", ".csv", "utf-8" },
            };

            progressBar.Maximum = (progressCount + 1) * 25;
            progressBar.Step = 25;
            progressBar.PerformStep();

            string queryFile = null;
            string fileName = null;
            string commandFromFile = null;
            string expansion = null;
            string encoding = null;

            foreach (int index in checkedListBox.CheckedIndices)
            {
                queryFile = loadStates[index, 0];
                fileName = loadStates[index, 1];
                commandFromFile = int.Parse(loadStates[index, 2]) == 1 ? 
                    LoadCommandTextFromFile(queryFile, GetPeriodFromDate()) :
                    LoadCommandTextFromFile(queryFile, dateTimePicker.Value);
                expansion = loadStates[index, 3];
                encoding = loadStates[index, 4];

                try
                {
                    if (DataBase.CreateConnection("Murmansk") != null)
                    {
                        using (SqlConnection connection = DataBase.CreateConnection("Murmansk"))
                        {
                            if (queryFile == "#loadBills.sql")
                            {
                                await Task.Run(()
                                => WTFLoadFromTable(
                                       connection,
                                       commandFromFile,
                                       fileName,
                                       expansion,
                                       encoding,
                                       false));
                            }
                            else
                            {
                                await Task.Run(()
                                => DBLFile.LoadFromTable(
                                       connection,
                                       commandFromFile,
                                       fileName,
                                       expansion,
                                       encoding,
                                       false));
                            }
                        }
                    }
                    else
                        MessageBox.Show("Ошибка соединения с базой данных!");

                    progressBar.PerformStep();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    checkedListBox.SetItemCheckState(index, CheckState.Unchecked);
                }
            }

            MessageBox.Show("Выгрузка завершена!");
            FormReset();
        }

        /// <summary>
        /// Без комментариев
        /// </summary>
        public static void WTFLoadFromTable(
            SqlConnection connection,
            string commandText,
            string fileName,
            string expansion,
            string encoding,
            bool hasHeader = true)
        {
            SqlCommand command = connection.CreateCommand();
            command.CommandTimeout = 0;
            command.CommandText = commandText;

            SqlDataReader reader = command.ExecuteReader();

            ulong id = 1;
            using (StreamWriter sw = new StreamWriter($@"{fileName}" + expansion, false, Encoding.GetEncoding(encoding)))
            {
                string[] line = null;
                StringBuilder tempId = new StringBuilder();

                while (reader.Read())
                {
                    line = reader[0].ToString().Split(';');

                    if (id.ToString().Length < 15)
                    {
                        for (int i = 0; i < 10 - id.ToString().Length; i++)
                        {
                            tempId.Append("0");
                        }
                        tempId.Append(id.ToString());
                    }

                    line[line.Length - 2] = fileName.Replace(".", "") + "-" + tempId.ToString();

                    id++;

                    sw.WriteLine(string.Join(";", line));

                    line = null;
                    tempId = new StringBuilder();
                }
            }
        }
    }
}
