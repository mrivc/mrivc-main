﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;

namespace EllisBillFilter.Controls
{
    public partial class GridDataPreview : UserControl
    {
        private string databaseName = "Hlam";

        public GridDataPreview()
        {
            InitializeComponent();
            gridDataView.OptionsView.ColumnAutoWidth = false;
            dabaBaseTablePicker1.TablesDropDown.EditValueChanged += TablesDropDown_EditValueChanged;
        }

        public string TableName
        {
            get
            {
                return dabaBaseTablePicker1.SelectedTable;
            }
        }

        public DataTable TableData
        {
            get
            {
                return gridData.DataSource as DataTable;
            }
        }

        public void LoadData()
        {
            dabaBaseTablePicker1.LoadData(databaseName);
        }

        public void FillGrid()
        {
            if (string.IsNullOrEmpty(TableName))
                return;

            gridDataView.Columns.Clear();
            gridData.DataSource = null;
            gridData.DataSource = DAccess.DataModule.SelectDataTable(TableName, databaseName);
        }

        private void TablesDropDown_EditValueChanged(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void btnTableListReload_Click(object sender, EventArgs e)
        {
            dabaBaseTablePicker1.LoadData(databaseName);
        }
    }
}
