﻿namespace EllisBillFilter.Controls
{
    partial class GridExcelImport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.baseGrid1 = new EllisBillFilter.BaseGrid();
            this.gridExcelImportView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ddExcelSheets = new EllisBillFilter.BaseDropDown();
            this.ddExcelSheetsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOpen = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDbTableName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridExcelImportView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddExcelSheets.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddExcelSheetsView)).BeginInit();
            this.SuspendLayout();
            // 
            // baseGrid1
            // 
            this.baseGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.baseGrid1.Location = new System.Drawing.Point(0, 45);
            this.baseGrid1.MainView = this.gridExcelImportView;
            this.baseGrid1.Name = "baseGrid1";
            this.baseGrid1.Size = new System.Drawing.Size(945, 444);
            this.baseGrid1.TabIndex = 0;
            this.baseGrid1.TrimCellValueBeforeValidate = true;
            this.baseGrid1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridExcelImportView});
            // 
            // gridExcelImportView
            // 
            this.gridExcelImportView.GridControl = this.baseGrid1;
            this.gridExcelImportView.Name = "gridExcelImportView";
            this.gridExcelImportView.OptionsView.ShowGroupPanel = false;
            // 
            // ddExcelSheets
            // 
            this.ddExcelSheets.EditValue = "";
            this.ddExcelSheets.Location = new System.Drawing.Point(176, 14);
            this.ddExcelSheets.Name = "ddExcelSheets";
            this.ddExcelSheets.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddExcelSheets.Properties.NullText = "";
            this.ddExcelSheets.Properties.PopupFormSize = new System.Drawing.Size(0, 100);
            this.ddExcelSheets.Properties.PopupSizeable = false;
            this.ddExcelSheets.Properties.View = this.ddExcelSheetsView;
            this.ddExcelSheets.Size = new System.Drawing.Size(250, 20);
            this.ddExcelSheets.TabIndex = 1;
            this.ddExcelSheets.EditValueChanged += new System.EventHandler(this.ddExcelSheets_EditValueChanged);
            // 
            // ddExcelSheetsView
            // 
            this.ddExcelSheetsView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.ddExcelSheetsView.Name = "ddExcelSheetsView";
            this.ddExcelSheetsView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.ddExcelSheetsView.OptionsView.ShowColumnHeaders = false;
            this.ddExcelSheetsView.OptionsView.ShowGroupPanel = false;
            this.ddExcelSheetsView.OptionsView.ShowIndicator = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(134, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Лист:";
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(3, 10);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(113, 27);
            this.btnOpen.TabIndex = 3;
            this.btnOpen.Text = "Открыть файл";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(441, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Временная таблица:";
            // 
            // txtDbTableName
            // 
            this.txtDbTableName.Location = new System.Drawing.Point(558, 14);
            this.txtDbTableName.Name = "txtDbTableName";
            this.txtDbTableName.Size = new System.Drawing.Size(207, 20);
            this.txtDbTableName.TabIndex = 5;
            // 
            // GridExcelImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtDbTableName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddExcelSheets);
            this.Controls.Add(this.baseGrid1);
            this.Name = "GridExcelImport";
            this.Size = new System.Drawing.Size(945, 489);
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridExcelImportView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddExcelSheets.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddExcelSheetsView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseGrid baseGrid1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridExcelImportView;
        private BaseDropDown ddExcelSheets;
        private DevExpress.XtraGrid.Views.Grid.GridView ddExcelSheetsView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDbTableName;
    }
}
