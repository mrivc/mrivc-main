﻿namespace MrivcControls.Data
{
    partial class DModule
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DModule));
            this.sqlMainConnection = new System.Data.SqlClient.SqlConnection();
            this.cmdAnyCommand = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            this.daPeriod = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand4 = new System.Data.SqlClient.SqlCommand();
            this.daBillKind = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand5 = new System.Data.SqlClient.SqlCommand();
            this.daEmployee = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand6 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand2 = new System.Data.SqlClient.SqlCommand();
            this.daPrintHistoryDetail = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand3 = new System.Data.SqlClient.SqlCommand();
            this.daCompany = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand7 = new System.Data.SqlClient.SqlCommand();
            this.daPrintHistoryPivotData = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            this.sqlSelectCommand8 = new System.Data.SqlClient.SqlCommand();
            this.daDeliveryType = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand1 = new System.Data.SqlClient.SqlCommand();
            this.daPrintHistory = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlDeleteCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand9 = new System.Data.SqlClient.SqlCommand();
            this.daPrintHistoryLog = new System.Data.SqlClient.SqlDataAdapter();
            this.daAddressFilterTree = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlCommand1 = new System.Data.SqlClient.SqlCommand();
            this.daDefferedCalculation = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlCommand2 = new System.Data.SqlClient.SqlCommand();
            // 
            // sqlMainConnection
            // 
            this.sqlMainConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // cmdAnyCommand
            // 
            this.cmdAnyCommand.CommandTimeout = 0;
            this.cmdAnyCommand.Connection = this.sqlMainConnection;
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = resources.GetString("sqlSelectCommand1.CommandText");
            this.sqlSelectCommand1.Connection = this.sqlMainConnection;
            // 
            // daPeriod
            // 
            this.daPeriod.SelectCommand = this.sqlSelectCommand1;
            this.daPeriod.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Table", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("Period", "Period"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Month", "Month"),
                        new System.Data.Common.DataColumnMapping("Year", "Year"),
                        new System.Data.Common.DataColumnMapping("Is_Work_Period", "Is_Work_Period")})});
            // 
            // sqlSelectCommand4
            // 
            this.sqlSelectCommand4.CommandText = "select *\r\nfrom reports.bill_kind";
            this.sqlSelectCommand4.CommandTimeout = 0;
            this.sqlSelectCommand4.Connection = this.sqlMainConnection;
            // 
            // daBillKind
            // 
            this.daBillKind.SelectCommand = this.sqlSelectCommand4;
            this.daBillKind.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Bill_Kind", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Bill_Kind", "ID_Bill_Kind"),
                        new System.Data.Common.DataColumnMapping("Is_Debt", "Is_Debt"),
                        new System.Data.Common.DataColumnMapping("Position", "Position"),
                        new System.Data.Common.DataColumnMapping("Bill_Kind", "Bill_Kind"),
                        new System.Data.Common.DataColumnMapping("Filter_Codes", "Filter_Codes"),
                        new System.Data.Common.DataColumnMapping("Order_For_Cut", "Order_For_Cut")})});
            // 
            // sqlSelectCommand5
            // 
            this.sqlSelectCommand5.CommandText = "select ID_Employee, Employee_First_Name, Employee_Second_Name, Employee_Last_Name" +
    ", FIO\r\nfrom dbo.vemployee";
            this.sqlSelectCommand5.CommandTimeout = 0;
            this.sqlSelectCommand5.Connection = this.sqlMainConnection;
            // 
            // daEmployee
            // 
            this.daEmployee.SelectCommand = this.sqlSelectCommand5;
            this.daEmployee.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "VEmployee", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Employee", "ID_Employee"),
                        new System.Data.Common.DataColumnMapping("Employee_First_Name", "Employee_First_Name"),
                        new System.Data.Common.DataColumnMapping("Employee_Second_Name", "Employee_Second_Name"),
                        new System.Data.Common.DataColumnMapping("Employee_Last_Name", "Employee_Last_Name"),
                        new System.Data.Common.DataColumnMapping("FIO", "FIO")})});
            // 
            // sqlSelectCommand6
            // 
            this.sqlSelectCommand6.CommandText = "Reports.Print_History_Detail_Select";
            this.sqlSelectCommand6.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand6.Connection = this.sqlMainConnection;
            this.sqlSelectCommand6.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Print", System.Data.SqlDbType.BigInt, 8)});
            // 
            // sqlUpdateCommand2
            // 
            this.sqlUpdateCommand2.CommandText = "Reports.Print_History_Detail_Update";
            this.sqlUpdateCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand2.Connection = this.sqlMainConnection;
            this.sqlUpdateCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Print", System.Data.SqlDbType.BigInt, 8, "ID_Print"),
            new System.Data.SqlClient.SqlParameter("@ID_House", System.Data.SqlDbType.Int, 4, "ID_House"),
            new System.Data.SqlClient.SqlParameter("@ID_Client_Detail", System.Data.SqlDbType.Int, 4, "ID_Client_Detail"),
            new System.Data.SqlClient.SqlParameter("@Checked", System.Data.SqlDbType.Bit, 1, "Checked")});
            // 
            // daPrintHistoryDetail
            // 
            this.daPrintHistoryDetail.SelectCommand = this.sqlSelectCommand6;
            this.daPrintHistoryDetail.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Print_History_Detail_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Print", "ID_Print"),
                        new System.Data.Common.DataColumnMapping("ID_House", "ID_House"),
                        new System.Data.Common.DataColumnMapping("ID_Client_Detail", "ID_Client_Detail"),
                        new System.Data.Common.DataColumnMapping("Bill_Count", "Bill_Count"),
                        new System.Data.Common.DataColumnMapping("House_Balance", "House_Balance"),
                        new System.Data.Common.DataColumnMapping("House_Pays", "House_Pays"),
                        new System.Data.Common.DataColumnMapping("House_Tariff", "House_Tariff"),
                        new System.Data.Common.DataColumnMapping("House_Recalculations", "House_Recalculations"),
                        new System.Data.Common.DataColumnMapping("House_Benefit", "House_Benefit"),
                        new System.Data.Common.DataColumnMapping("Checked", "Checked"),
                        new System.Data.Common.DataColumnMapping("CheckedBy", "CheckedBy"),
                        new System.Data.Common.DataColumnMapping("house_address", "house_address"),
                        new System.Data.Common.DataColumnMapping("Derive_Company", "Derive_Company"),
                        new System.Data.Common.DataColumnMapping("Bill_Filter", "Bill_Filter")})});
            this.daPrintHistoryDetail.UpdateCommand = this.sqlUpdateCommand2;
            // 
            // sqlSelectCommand3
            // 
            this.sqlSelectCommand3.CommandText = "select ID_Company, Name, Is_Provider, Is_Derive_Company, Is_Pay_Receiver, Code, M" +
    "eter_Future_Period_Day, Load_Measure_In_Future\r\nfrom dbo.Company";
            this.sqlSelectCommand3.Connection = this.sqlMainConnection;
            // 
            // daCompany
            // 
            this.daCompany.SelectCommand = this.sqlSelectCommand3;
            this.daCompany.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Company", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Company", "ID_Company"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Is_Provider", "Is_Provider"),
                        new System.Data.Common.DataColumnMapping("Is_Derive_Company", "Is_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("Is_Pay_Receiver", "Is_Pay_Receiver"),
                        new System.Data.Common.DataColumnMapping("Code", "Code"),
                        new System.Data.Common.DataColumnMapping("Meter_Future_Period_Day", "Meter_Future_Period_Day"),
                        new System.Data.Common.DataColumnMapping("Load_Measure_In_Future", "Load_Measure_In_Future")})});
            // 
            // sqlSelectCommand7
            // 
            this.sqlSelectCommand7.CommandText = "Reports.Print_History_Pivot_Data";
            this.sqlSelectCommand7.CommandTimeout = 0;
            this.sqlSelectCommand7.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand7.Connection = this.sqlMainConnection;
            this.sqlSelectCommand7.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@ShowAllBill", System.Data.SqlDbType.Bit, 2)});
            // 
            // daPrintHistoryPivotData
            // 
            this.daPrintHistoryPivotData.SelectCommand = this.sqlSelectCommand7;
            this.daPrintHistoryPivotData.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "PrintPivotData", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("fio", "fio"),
                        new System.Data.Common.DataColumnMapping("derive_company", "derive_company"),
                        new System.Data.Common.DataColumnMapping("bill_filter", "bill_filter"),
                        new System.Data.Common.DataColumnMapping("house_address", "house_address"),
                        new System.Data.Common.DataColumnMapping("bill_count", "bill_count"),
                        new System.Data.Common.DataColumnMapping("PrintDate", "PrintDate"),
                        new System.Data.Common.DataColumnMapping("Issued_Date", "Issued_Date")})});
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.ConnectionString = "Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;User ID=bobrovsky;Password=speake" +
    "r";
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlSelectCommand8
            // 
            this.sqlSelectCommand8.CommandText = "select *\r\nfrom [Reports].[Print_Delivery_Type]";
            this.sqlSelectCommand8.Connection = this.sqlMainConnection;
            // 
            // daDeliveryType
            // 
            this.daDeliveryType.SelectCommand = this.sqlSelectCommand8;
            this.daDeliveryType.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Print_Delivery_Type", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Delivery_Type", "ID_Delivery_Type"),
                        new System.Data.Common.DataColumnMapping("Name", "Name")})});
            // 
            // sqlSelectCommand2
            // 
            this.sqlSelectCommand2.CommandText = "Reports.Print_History_Select";
            this.sqlSelectCommand2.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand2.Connection = this.sqlMainConnection;
            this.sqlSelectCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4),
            new System.Data.SqlClient.SqlParameter("@PrintBy", System.Data.SqlDbType.Int, 4)});
            // 
            // sqlUpdateCommand1
            // 
            this.sqlUpdateCommand1.CommandText = "Reports.Print_History_Update";
            this.sqlUpdateCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlUpdateCommand1.Connection = this.sqlMainConnection;
            this.sqlUpdateCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@ID_Print", System.Data.SqlDbType.BigInt, 8, "ID_Print"),
            new System.Data.SqlClient.SqlParameter("@Checked", System.Data.SqlDbType.Bit, 1, "Checked"),
            new System.Data.SqlClient.SqlParameter("@Issued_Date", System.Data.SqlDbType.DateTime, 8, "Issued_Date"),
            new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.VarChar, 2147483647, "Comment"),
            new System.Data.SqlClient.SqlParameter("@PrintedBackSide", System.Data.SqlDbType.Bit, 1, "PrintedBackSide"),
            new System.Data.SqlClient.SqlParameter("@ID_Delivery_Type", System.Data.SqlDbType.Int, 4, "ID_Delivery_Type")});
            // 
            // daPrintHistory
            // 
            this.daPrintHistory.DeleteCommand = this.sqlDeleteCommand1;
            this.daPrintHistory.SelectCommand = this.sqlSelectCommand2;
            this.daPrintHistory.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Print_History_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Print", "ID_Print"),
                        new System.Data.Common.DataColumnMapping("Period", "Period"),
                        new System.Data.Common.DataColumnMapping("PrintDate", "PrintDate"),
                        new System.Data.Common.DataColumnMapping("PrintBy", "PrintBy"),
                        new System.Data.Common.DataColumnMapping("Print_Count", "Print_Count"),
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Kind", "ID_Bill_Kind"),
                        new System.Data.Common.DataColumnMapping("ID_Debt_Company", "ID_Debt_Company"),
                        new System.Data.Common.DataColumnMapping("PrintedBackSide", "PrintedBackSide"),
                        new System.Data.Common.DataColumnMapping("Checked", "Checked"),
                        new System.Data.Common.DataColumnMapping("CheckedBy", "CheckedBy"),
                        new System.Data.Common.DataColumnMapping("Issued_Date", "Issued_Date"),
                        new System.Data.Common.DataColumnMapping("IssuedBy", "IssuedBy"),
                        new System.Data.Common.DataColumnMapping("ID_Delivery_Type", "ID_Delivery_Type"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("DeriveCompany", "DeriveCompany"),
                        new System.Data.Common.DataColumnMapping("IssuedDelay", "IssuedDelay")})});
            this.daPrintHistory.UpdateCommand = this.sqlUpdateCommand1;
            // 
            // sqlDeleteCommand1
            // 
            this.sqlDeleteCommand1.CommandText = "[Reports].[Print_History_Delete]";
            this.sqlDeleteCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlDeleteCommand1.Connection = this.sqlMainConnection;
            this.sqlDeleteCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID_Print", System.Data.SqlDbType.BigInt, 8, "ID_Print")});
            // 
            // sqlSelectCommand9
            // 
            this.sqlSelectCommand9.CommandText = "Reports.Print_History_Log_Select";
            this.sqlSelectCommand9.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand9.Connection = this.sqlMainConnection;
            this.sqlSelectCommand9.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@IdPrint", System.Data.SqlDbType.BigInt, 4)});
            // 
            // daPrintHistoryLog
            // 
            this.daPrintHistoryLog.SelectCommand = this.sqlSelectCommand9;
            this.daPrintHistoryLog.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Print_History_Log_Select", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Print", "ID_Print"),
                        new System.Data.Common.DataColumnMapping("Period", "Period"),
                        new System.Data.Common.DataColumnMapping("PrintDate", "PrintDate"),
                        new System.Data.Common.DataColumnMapping("PrintBy", "PrintBy"),
                        new System.Data.Common.DataColumnMapping("Print_Count", "Print_Count"),
                        new System.Data.Common.DataColumnMapping("ID_Client", "ID_Client"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Kind", "ID_Bill_Kind"),
                        new System.Data.Common.DataColumnMapping("ID_Debt_Company", "ID_Debt_Company"),
                        new System.Data.Common.DataColumnMapping("PrintedBackSide", "PrintedBackSide"),
                        new System.Data.Common.DataColumnMapping("Checked", "Checked"),
                        new System.Data.Common.DataColumnMapping("CheckedBy", "CheckedBy"),
                        new System.Data.Common.DataColumnMapping("Issued_Date", "Issued_Date"),
                        new System.Data.Common.DataColumnMapping("IssuedBy", "IssuedBy"),
                        new System.Data.Common.DataColumnMapping("ID_Delivery_Type", "ID_Delivery_Type"),
                        new System.Data.Common.DataColumnMapping("Comment", "Comment"),
                        new System.Data.Common.DataColumnMapping("ChangedBy", "ChangedBy"),
                        new System.Data.Common.DataColumnMapping("ChangeDate", "ChangeDate")})});
            // 
            // daAddressFilterTree
            // 
            this.daAddressFilterTree.SelectCommand = this.sqlCommand1;
            this.daAddressFilterTree.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "PrintPivotData", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("fio", "fio"),
                        new System.Data.Common.DataColumnMapping("derive_company", "derive_company"),
                        new System.Data.Common.DataColumnMapping("bill_filter", "bill_filter"),
                        new System.Data.Common.DataColumnMapping("house_address", "house_address"),
                        new System.Data.Common.DataColumnMapping("bill_count", "bill_count"),
                        new System.Data.Common.DataColumnMapping("PrintDate", "PrintDate"),
                        new System.Data.Common.DataColumnMapping("Issued_Date", "Issued_Date")})});
            // 
            // sqlCommand1
            // 
            this.sqlCommand1.CommandText = "town.Address_Filter_Tree";
            this.sqlCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlCommand1.Connection = this.sqlMainConnection;
            this.sqlCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("type", System.Data.SqlDbType.Int),
            new System.Data.SqlClient.SqlParameter("id_parent", System.Data.SqlDbType.Int),
            new System.Data.SqlClient.SqlParameter("id_company", System.Data.SqlDbType.Int)});
            // 
            // daDefferedCalculation
            // 
            this.daDefferedCalculation.SelectCommand = this.sqlCommand2;
            this.daDefferedCalculation.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "PrintPivotData", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("fio", "fio"),
                        new System.Data.Common.DataColumnMapping("derive_company", "derive_company"),
                        new System.Data.Common.DataColumnMapping("bill_filter", "bill_filter"),
                        new System.Data.Common.DataColumnMapping("house_address", "house_address"),
                        new System.Data.Common.DataColumnMapping("bill_count", "bill_count"),
                        new System.Data.Common.DataColumnMapping("PrintDate", "PrintDate"),
                        new System.Data.Common.DataColumnMapping("Issued_Date", "Issued_Date")})});
            // 
            // sqlCommand2
            // 
            this.sqlCommand2.CommandText = "select ID_Deffered_Calculation,\tCreatedBy,\tCreationDate,\tCalc_Parameters,\tCalc_Su" +
    "m_Tariff,\tID_Status,\tCaption,\tAccount_Query,\tHouse_Query from town.Deffered_Calc" +
    "ulation";
            this.sqlCommand2.Connection = this.sqlMainConnection;

        }

        #endregion

        private System.Data.SqlClient.SqlConnection sqlMainConnection;
        private System.Data.SqlClient.SqlCommand cmdAnyCommand;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private System.Data.SqlClient.SqlDataAdapter daPeriod;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand4;
        private System.Data.SqlClient.SqlDataAdapter daBillKind;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand5;
        private System.Data.SqlClient.SqlDataAdapter daEmployee;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand6;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand2;
        private System.Data.SqlClient.SqlDataAdapter daPrintHistoryDetail;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand3;
        private System.Data.SqlClient.SqlDataAdapter daCompany;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand7;
        private System.Data.SqlClient.SqlDataAdapter daPrintHistoryPivotData;
        private System.Data.SqlClient.SqlConnection sqlConnection1;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand8;
        private System.Data.SqlClient.SqlDataAdapter daDeliveryType;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand2;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand1;
        private System.Data.SqlClient.SqlDataAdapter daPrintHistory;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand1;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand9;
        private System.Data.SqlClient.SqlDataAdapter daPrintHistoryLog;
        private System.Data.SqlClient.SqlDataAdapter daAddressFilterTree;
        private System.Data.SqlClient.SqlCommand sqlCommand1;
        private System.Data.SqlClient.SqlDataAdapter daDefferedCalculation;
        private System.Data.SqlClient.SqlCommand sqlCommand2;
    }
}
