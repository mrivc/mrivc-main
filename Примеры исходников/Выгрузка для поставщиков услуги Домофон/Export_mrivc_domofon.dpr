Library Export_mrivc_domofon;
{$MESSAGE '�������� ��� ���������� ������ ������� �����'}
{$MESSAGE '����� �.�. 25.03.2010'}
uses
  Windows,
  SysUtils,
  Classes,
  uPropertyCollection,
  Forms,
  Variants,
  ADODB,
  ImgList,
  Controls,
  Export_mrivc_domofon_unit1 in 'Export_mrivc_domofon_unit1.pas' {frmUSZN},
  uAddrParam in 'C:\Components\Frames\uAddrParam.pas' {AddrParamFilter: TFrame},
  uSelectEditFilter in 'C:\Components\Frames\uSelectEditFilter.pas' {SelectEditFilter: TFrame},
  uCheckedFilter in 'C:\Components\Frames\uCheckedFilter.pas' {CheckedFilter: TFrame},
  uPeriodFilter in 'C:\Components\Frames\uPeriodFilter.pas' {PeriodFilter: TFrame},
  uTypes in 'C:\Components\EllisUnits\uTypes.pas';

{$R *.res}
Var
  OldScreen: TScreen;
  OldApplication: TApplication;
  OldHintWindowClass: THintWindowClass;

Procedure ActionProc(Reason: Integer);
Begin
  If (Reason = DLL_PROCESS_DETACH) Then
    Begin
      If Assigned(OldApplication) Then
        Application := OldApplication;
      If Assigned(OldScreen) Then
        Screen := OldScreen;
      If Assigned(OldHintWindowClass) Then
        HintWindowClass := OldHintWindowClass;
    End;
End;

Procedure Execute(Options: TPropertyCollection);
Var
  S: Variant;
  F: TForm;
  Owner: TComponent;
  O: TPropertyCollection;
Begin
  //O := TPropertyCollection.Create;
  //O.Assign(Options);
  O := Options;
  S := O['Screen'];
  If (Not Assigned(OldScreen)) Then OldScreen := Screen;
  If (Not VarIsNull(S)) Then Integer(Screen) := S;

  S := O['Application'];
  If (Not Assigned(OldApplication)) Then OldApplication := Application;
  If (Not VarIsNull(S)) Then Integer(Application) := S;

  S := O['HintWindowClass'];
  If (Not Assigned(OldHintWindowClass)) Then OldHintWindowClass := HintWindowClass;
  If (Not VarIsNull(S)) Then Integer(HintWindowClass) := S;

  If Assigned(Application) Then
    Owner := Application.MainForm
  Else
    Owner := Nil;
  Try
    F := TfrmUSZN.Create(Owner, O);
//    O.Free;
    F.Show;
    Options['ResultForm'] := Integer(F);
  Except
  End;
End;

Exports Execute;

Begin
  DllProc := @ActionProc;
End.
