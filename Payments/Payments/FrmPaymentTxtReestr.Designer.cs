﻿namespace Payments
{
    partial class FrmPaymentTxtReestr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPaymentTxtReestr));
            this.lblPaymentPeriodEnd = new System.Windows.Forms.Label();
            this.lblPaymentPeriodBegin = new System.Windows.Forms.Label();
            this.deEndDate = new DevExpress.XtraEditors.DateEdit();
            this.deBeginDate = new DevExpress.XtraEditors.DateEdit();
            this.btnCreateTxtReestr = new DevExpress.XtraEditors.SimpleButton();
            this.gridPaymentTxtReestr = new Payments.BaseControls.BaseGrid();
            this.txtReestrBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.paymentsDataSet = new Payments.PaymentsDataSet();
            this.gridViewPaymentTxtReestr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Payment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Payment_Class = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCLIENT_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCLIENT_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBARCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colADDRESS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayment_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSumm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Loaded_Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayment_Summ = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPaymentTxtReestr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReestrBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaymentTxtReestr)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPaymentPeriodEnd
            // 
            this.lblPaymentPeriodEnd.AutoSize = true;
            this.lblPaymentPeriodEnd.Location = new System.Drawing.Point(181, 15);
            this.lblPaymentPeriodEnd.Name = "lblPaymentPeriodEnd";
            this.lblPaymentPeriodEnd.Size = new System.Drawing.Size(19, 13);
            this.lblPaymentPeriodEnd.TabIndex = 16;
            this.lblPaymentPeriodEnd.Text = "по";
            // 
            // lblPaymentPeriodBegin
            // 
            this.lblPaymentPeriodBegin.AutoSize = true;
            this.lblPaymentPeriodBegin.Location = new System.Drawing.Point(8, 15);
            this.lblPaymentPeriodBegin.Name = "lblPaymentPeriodBegin";
            this.lblPaymentPeriodBegin.Size = new System.Drawing.Size(64, 13);
            this.lblPaymentPeriodBegin.TabIndex = 15;
            this.lblPaymentPeriodBegin.Text = "Платежи с:";
            // 
            // deEndDate
            // 
            this.deEndDate.EditValue = null;
            this.deEndDate.Location = new System.Drawing.Point(205, 12);
            this.deEndDate.Name = "deEndDate";
            this.deEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEndDate.Size = new System.Drawing.Size(97, 20);
            this.deEndDate.TabIndex = 14;
            this.deEndDate.EditValueChanged += new System.EventHandler(this.deEndDate_EditValueChanged);
            // 
            // deBeginDate
            // 
            this.deBeginDate.EditValue = null;
            this.deBeginDate.Location = new System.Drawing.Point(76, 12);
            this.deBeginDate.Name = "deBeginDate";
            this.deBeginDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deBeginDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deBeginDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deBeginDate.Size = new System.Drawing.Size(97, 20);
            this.deBeginDate.TabIndex = 13;
            this.deBeginDate.EditValueChanged += new System.EventHandler(this.deBeginDate_EditValueChanged);
            // 
            // btnCreateTxtReestr
            // 
            this.btnCreateTxtReestr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateTxtReestr.Location = new System.Drawing.Point(702, 12);
            this.btnCreateTxtReestr.Name = "btnCreateTxtReestr";
            this.btnCreateTxtReestr.Size = new System.Drawing.Size(195, 23);
            this.btnCreateTxtReestr.TabIndex = 17;
            this.btnCreateTxtReestr.Text = "Создать реестры платежей";
            this.btnCreateTxtReestr.Click += new System.EventHandler(this.btnCreateTxtReestr_Click);
            // 
            // gridPaymentTxtReestr
            // 
            this.gridPaymentTxtReestr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPaymentTxtReestr.DataSource = this.txtReestrBindingSource;
            this.gridPaymentTxtReestr.Location = new System.Drawing.Point(2, 46);
            this.gridPaymentTxtReestr.MainView = this.gridViewPaymentTxtReestr;
            this.gridPaymentTxtReestr.Name = "gridPaymentTxtReestr";
            this.gridPaymentTxtReestr.Size = new System.Drawing.Size(904, 520);
            this.gridPaymentTxtReestr.TabIndex = 18;
            this.gridPaymentTxtReestr.TrimCellValueBeforeValidate = true;
            this.gridPaymentTxtReestr.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPaymentTxtReestr});
            // 
            // txtReestrBindingSource
            // 
            this.txtReestrBindingSource.DataMember = "TxtReestr";
            this.txtReestrBindingSource.DataSource = this.paymentsDataSet;
            // 
            // paymentsDataSet
            // 
            this.paymentsDataSet.DataSetName = "PaymentsDataSet";
            this.paymentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewPaymentTxtReestr
            // 
            this.gridViewPaymentTxtReestr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Payment,
            this.colID_Payment_Class,
            this.colCLIENT_ID,
            this.colCLIENT_NAME,
            this.colBARCODE,
            this.colADDRESS,
            this.colPayment_Date,
            this.colTotalSumm,
            this.colPayment_Summ,
            this.colLine,
            this.colID_Loaded_Status});
            this.gridViewPaymentTxtReestr.CustomizationFormBounds = new System.Drawing.Rectangle(1055, 690, 216, 199);
            this.gridViewPaymentTxtReestr.GridControl = this.gridPaymentTxtReestr;
            this.gridViewPaymentTxtReestr.Name = "gridViewPaymentTxtReestr";
            this.gridViewPaymentTxtReestr.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPaymentTxtReestr.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPaymentTxtReestr.OptionsBehavior.Editable = false;
            this.gridViewPaymentTxtReestr.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewPaymentTxtReestr_RowStyle);
            // 
            // colID_Payment
            // 
            this.colID_Payment.Caption = "ИД";
            this.colID_Payment.FieldName = "ID_Payment";
            this.colID_Payment.Name = "colID_Payment";
            this.colID_Payment.Visible = true;
            this.colID_Payment.VisibleIndex = 0;
            // 
            // colID_Payment_Class
            // 
            this.colID_Payment_Class.Caption = "Класс платежа";
            this.colID_Payment_Class.FieldName = "ID_Payment_Class";
            this.colID_Payment_Class.Name = "colID_Payment_Class";
            // 
            // colCLIENT_ID
            // 
            this.colCLIENT_ID.FieldName = "CLIENT_ID";
            this.colCLIENT_ID.Name = "colCLIENT_ID";
            // 
            // colCLIENT_NAME
            // 
            this.colCLIENT_NAME.FieldName = "CLIENT_NAME";
            this.colCLIENT_NAME.Name = "colCLIENT_NAME";
            // 
            // colBARCODE
            // 
            this.colBARCODE.FieldName = "BARCODE";
            this.colBARCODE.Name = "colBARCODE";
            this.colBARCODE.Visible = true;
            this.colBARCODE.VisibleIndex = 1;
            // 
            // colADDRESS
            // 
            this.colADDRESS.FieldName = "ADDRESS";
            this.colADDRESS.Name = "colADDRESS";
            this.colADDRESS.Visible = true;
            this.colADDRESS.VisibleIndex = 2;
            // 
            // colPayment_Date
            // 
            this.colPayment_Date.FieldName = "Payment_Date";
            this.colPayment_Date.Name = "colPayment_Date";
            this.colPayment_Date.Visible = true;
            this.colPayment_Date.VisibleIndex = 4;
            // 
            // colTotalSumm
            // 
            this.colTotalSumm.FieldName = "TotalSumm";
            this.colTotalSumm.Name = "colTotalSumm";
            // 
            // colLine
            // 
            this.colLine.FieldName = "Line";
            this.colLine.Name = "colLine";
            this.colLine.Visible = true;
            this.colLine.VisibleIndex = 5;
            // 
            // colID_Loaded_Status
            // 
            this.colID_Loaded_Status.FieldName = "ID_Loaded_Status";
            this.colID_Loaded_Status.Name = "colID_Loaded_Status";
            // 
            // colPayment_Summ
            // 
            this.colPayment_Summ.Caption = "Payment Summ";
            this.colPayment_Summ.FieldName = "Payment_Summ";
            this.colPayment_Summ.Name = "colPayment_Summ";
            this.colPayment_Summ.Visible = true;
            this.colPayment_Summ.VisibleIndex = 3;
            // 
            // FrmPaymentTxtReestr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 567);
            this.Controls.Add(this.gridPaymentTxtReestr);
            this.Controls.Add(this.btnCreateTxtReestr);
            this.Controls.Add(this.lblPaymentPeriodEnd);
            this.Controls.Add(this.lblPaymentPeriodBegin);
            this.Controls.Add(this.deEndDate);
            this.Controls.Add(this.deBeginDate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPaymentTxtReestr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выгрузка реестра платежей по Капитальному ремонту";
            this.Load += new System.EventHandler(this.FrmPaymentTxtReestr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPaymentTxtReestr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReestrBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaymentTxtReestr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPaymentPeriodEnd;
        private System.Windows.Forms.Label lblPaymentPeriodBegin;
        private DevExpress.XtraEditors.DateEdit deEndDate;
        private DevExpress.XtraEditors.DateEdit deBeginDate;
        private DevExpress.XtraEditors.SimpleButton btnCreateTxtReestr;
        private BaseControls.BaseGrid gridPaymentTxtReestr;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPaymentTxtReestr;
        private System.Windows.Forms.BindingSource txtReestrBindingSource;
        private PaymentsDataSet paymentsDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Payment;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Payment_Class;
        private DevExpress.XtraGrid.Columns.GridColumn colCLIENT_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colCLIENT_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn colBARCODE;
        private DevExpress.XtraGrid.Columns.GridColumn colADDRESS;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSumm;
        private DevExpress.XtraGrid.Columns.GridColumn colLine;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Loaded_Status;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment_Summ;
    }
}