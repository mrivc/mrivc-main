﻿namespace Helper.Controls
{
    partial class GisErrorFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbGisError = new System.Windows.Forms.ComboBox();
            this.gbGisError = new System.Windows.Forms.GroupBox();
            this.gbGisError.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbGisError
            // 
            this.cbGisError.FormattingEnabled = true;
            this.cbGisError.Location = new System.Drawing.Point(6, 19);
            this.cbGisError.Name = "cbGisError";
            this.cbGisError.Size = new System.Drawing.Size(402, 21);
            this.cbGisError.TabIndex = 3;
            // 
            // gbGisError
            // 
            this.gbGisError.Controls.Add(this.cbGisError);
            this.gbGisError.Location = new System.Drawing.Point(3, 3);
            this.gbGisError.Name = "gbGisError";
            this.gbGisError.Size = new System.Drawing.Size(415, 50);
            this.gbGisError.TabIndex = 3;
            this.gbGisError.TabStop = false;
            this.gbGisError.Text = "Ошибки за период";
            // 
            // GisErrorFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbGisError);
            this.Name = "GisErrorFilter";
            this.Size = new System.Drawing.Size(422, 59);
            this.gbGisError.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbGisError;
        private System.Windows.Forms.GroupBox gbGisError;
    }
}
