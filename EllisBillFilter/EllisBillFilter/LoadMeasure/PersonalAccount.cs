﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace EllisBillFilter.LoadMeasure
{    /// <summary>
     /// Класс для хранения ЛС к которым привязаны счетчики
     /// </summary>
    public class PersonalAccount
    {
        #region Constants

        private const int DAY = 26;

        private const int MEASURE_TYPE = 15;
        private const bool IF_EXISTS = false;
        private const int METER_STATUS = 1;

        #endregion

        public PersonalAccount(string text, SqlCommand getWorkPeriodCommand, SqlCommand getPeriodBeginDateCommand, char separator = ';')
        {
            _measures = new List<Meter>();
            Parse(text, getWorkPeriodCommand, getPeriodBeginDateCommand, separator);
        }

        public PersonalAccount(string account, List<Meter> measures)
        {
            _account = account;
            _measures = measures;
        }

        public void Parse(string text, char separator = ';')
        {
            string[] splitedText = text.TrimEnd(separator).Split(new char[] { separator });
            _account = splitedText[0];

            DateTime date = _ChangeDate(DateTime.Parse(splitedText[1]), DAY, DateTime.Now.Month);

            AddMetersFromLine(splitedText, date);
        }

        public void Parse(string text, SqlCommand getWorkPeriodCommand, SqlCommand getPeriodBeginDateCommand, char separator = ';')
        {
            string[] splitedText = text.TrimEnd(separator).Split(new char[] { separator });
            _account = splitedText[0];

            //DateTime date = _ChangeDate(DateTime.Parse(splitedText[1]), DAY, DateTime.Now.Month);
            DateTime date = DateTime.Parse(splitedText[1]);

            int workPeriod = int.Parse(getWorkPeriodCommand.ExecuteScalar().ToString());
            SetupCommand(getPeriodBeginDateCommand, "@Period", workPeriod);
            DateTime beginPeriodDate = DateTime.Parse(getPeriodBeginDateCommand.ExecuteScalar().ToString());

            date = date > beginPeriodDate ? date : beginPeriodDate;

            AddMetersFromLine(splitedText, date);
        }

        public void Load(SqlCommand loadCommand)
        {
            foreach (Meter m in _measures)
            {
                SetupLoadCommand(loadCommand, m);
                m.Load(loadCommand);
            }
        }

        public void Load(SqlCommand loadCommand, SqlCommand validationCommand)
        {
            foreach (Meter m in _measures)
            {
                SetupLoadCommand(loadCommand, m);
                SetupCommand(validationCommand, "@ID_Meter", m.IdMeter);

                bool isValide = !(bool)validationCommand.ExecuteScalar();

                if (isValide) m.Load(loadCommand);
            }
        }

        public void Load(SqlCommand loadCommand, SqlCommand validationCommand, SqlCommand getLastMeasureValueCommand)
        {
            foreach (Meter m in _measures)
            {
                SetupLoadCommand(loadCommand, m);
                SetupCommand(validationCommand, "@ID_Meter", m.IdMeter);
                SetupCommand(getLastMeasureValueCommand, "@ID_Meter", m.IdMeter);

                bool isValide = !(bool)validationCommand.ExecuteScalar();

                decimal lastMeasureValue = m.GetLastMeasureValue(getLastMeasureValueCommand);

                if (isValide && m.MeasureValue >= lastMeasureValue) m.Load(loadCommand);
            }
        }

        #region Private

        private string _account;
        private List<Meter> _measures;

        private SqlParameter[] _SetSqlLoadParameters(Meter m)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@ID_Meter", m.IdMeter),
                new SqlParameter("@Measure_Date", m.SqlFormattedDate),
                new SqlParameter("@Measure_Value", m.MeasureValue.ToString().Replace(',', '.')),
                new SqlParameter("@ID_Meter_Measure_Type", MEASURE_TYPE),
                new SqlParameter("@Change_IF_Exists", IF_EXISTS),
                new SqlParameter("@ID_Meter_Controller", null),
                new SqlParameter("@ID_Meter_Status", METER_STATUS)
            };
        }

        private DateTime _ChangeDate(DateTime date, int day, int mounth)
        {
            int diffDay = day - date.Day;
            int diffMounth = mounth - date.Month;
            DateTime newDate = date.AddDays(diffDay).AddMonths(diffMounth);

            return newDate;
        }

        private void SetupLoadCommand(SqlCommand command, Meter meter)
        {
            command.Parameters.Clear();
            command.Parameters.AddRange(_SetSqlLoadParameters(meter));
        }

        private void SetupCommand(SqlCommand command, string parameter, int value)
        {
            command.Parameters.Clear();
            command.Parameters.AddRange(
                new SqlParameter[]
                {
                        new SqlParameter($"{parameter}", value)
                }
            );
        }

        private void AddMetersFromLine(string[] splitedText, DateTime date)
        {
            for (int i = 2; i < splitedText.Length; i += 5)
            {
                string idString = splitedText[i + 2];
                int idMeter = int.Parse(new string(idString.Where(c => char.IsDigit(c)).ToArray()));
                decimal measure = Math.Round(decimal.Parse(splitedText[i + 4].Replace('.', ',')), 3);

                _measures.Add(new Meter(idMeter, date, measure));
            }
        }

        #endregion
    }
}
