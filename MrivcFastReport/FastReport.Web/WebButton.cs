﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using FastReport.Dialog;
using System.Windows.Forms;

namespace FastReport.Web.Handlers
{
    public partial class WebExport : IHttpHandler
    {
        private void ButtonClick(ButtonControl button)
        {
            if (button.DialogResult == DialogResult.OK)
            {
                FormClose(button, CloseReason.None);
                webReport.Prop.CurrentForm++;
                dialogRefresh = false;
            }
            else if (button.DialogResult == DialogResult.Cancel)
            {
                FormClose(button, CloseReason.UserClosing);
                webReport.Prop.State = ReportState.Canceled;
                dialogRefresh = false;
            }
            button.OnClick(null);
        }

        private void FormClose(ButtonControl button, CloseReason reason)
        {
            DialogPage dialog = button.Report.Pages[Properties.CurrentForm] as DialogPage;
            FormClosingEventArgs closingArgs = new FormClosingEventArgs(reason, false);
            dialog.OnFormClosing(closingArgs);
            FormClosedEventArgs closedArgs = new FormClosedEventArgs(reason);
            dialog.OnFormClosed(closedArgs);
            dialog.ActiveInWeb = false;
        }

        private string GetButtonHtml(ButtonControl control, WebReport webReport)
        {
            return string.Format("<input class=\"{0}\" style=\"{1}\" type=\"button\" name=\"{2}\" value=\"{3}\" onclick=\"{4}\" title=\"{5}\"/>",
                // class
                "",
                // style
                GetButtonStyle(control),
                // name
                control.Name,
                // value
                control.Text,
                // onclick
                GetEvent("onclick", control, webReport, ""),
                // title
                control.Text
                );
        }

        private string GetButtonStyle(ButtonControl control)
        {
            return string.Format("{0}{1}{2}padding:0;margin:0;", GetControlPosition(control), GetControlFont(control), GetControlAlign(control));
        }

    }
}
