﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace EllisBillFilter.LoadMeasure
{
    /// <summary>
    /// SINGLETON и Facade для работы с базой
    /// </summary>
    public sealed class SqlDatabase
    {
        #region Public

        public static SqlDatabase Create(string connectionString)
        {
            if (_instance == null) { _instance = new SqlDatabase(connectionString); }

            return _instance;
        }

        public bool HasConnection() => _connection != null;

        public void Open()
        {
            if (!HasConnection()) return;
            if (_connection.State != ConnectionState.Closed) return;

            try { _connection.Open(); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Close()
        {
            if (!HasConnection()) return;
            if (_connection.State == ConnectionState.Closed) return;

            try { _connection.Close(); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public SqlCommand CreateCommand(string commandText, CommandType commandType, int timeout = 0)
        {
            if (!HasConnection()) throw new Exception(NO_CONNECTION);

            SqlCommand command = _CommandSetup(commandText, commandType, timeout);

            return command;
        }

        public SqlCommand CreateCommand(string commandText, CommandType commandType, SqlTransaction transaction, int timeout = 0)
        {
            if (!HasConnection()) throw new Exception(NO_CONNECTION);

            SqlCommand command = _CommandSetup(commandText, commandType, timeout);

            transaction = _connection.BeginTransaction();
            command.Transaction = transaction;

            return command;
        }

        #endregion

        #region Private

        private const string NO_CONNECTION = "No connection, unable to create command.";

        private static SqlDatabase _instance = null;

        private SqlConnection _connection = null;

        private SqlCommand _CommandSetup(string commandText, CommandType commandType, int timeout = 0)
        {
            SqlCommand command = _connection.CreateCommand();
            command.CommandTimeout = timeout;

            command.CommandText = commandText;
            command.CommandType = commandType;

            return command;
        }

        private SqlDatabase(string connectionString)
        {
            _connection = new SqlConnection();
            _connection.ConnectionString = connectionString;
        }

        #endregion
    }
}
