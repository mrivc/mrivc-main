﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Payments
{
    using Payments.DS;
    using Payments.Helpers;

    public partial class FrmPaymentTxtReestr : Form
    {
        private bool initialized = false;

        public FrmPaymentTxtReestr()
        {
            InitializeComponent();
            deBeginDate.DateTime = DateTime.Now;
            deEndDate.DateTime = DateTime.Now;
            initialized = true;
        }

        public void LoadData()
        {
            if (!initialized)
                return;

            DAccess.DataModule.PaymentTxtReestrSelect(paymentsDataSet.TxtReestr, deBeginDate.DateTime, deEndDate.DateTime);
            gridPaymentTxtReestr.InitEx();
        }

        public void ReLoadData()
        {
            if (!initialized)
                return;

            paymentsDataSet.TxtReestr.Clear();
            DAccess.DataModule.PaymentTxtReestrSelect(paymentsDataSet.TxtReestr, deBeginDate.DateTime, deEndDate.DateTime);
        }

        private void btnCreateTxtReestr_Click(object sender, EventArgs e)
        {
            string fileName = "pays_" + DateTime.Today.Year.ToString() + StringHelper.ZeroCode(DateTime.Today.Month)
                              + StringHelper.ZeroCode(DateTime.Today.Day) + "_"
                              + StringHelper.ZeroCode(DateTime.Now.Hour) + StringHelper.ZeroCode(DateTime.Now.Minute)
                              + ".txt";

            TxtReestr.UnloadToTxt(fileName, paymentsDataSet.TxtReestr);

            string zipedFile = TxtReestr.CompressLastUploadedFiles(true);
            MessageBox.Show("Реестры платежей были созданы успешно.\n\nФайл реестров находится тут:\n" + zipedFile, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

            TxtReestr.ClearUnloadedFiles();
        }

        private void deBeginDate_EditValueChanged(object sender, EventArgs e)
        {
            this.ReLoadData();
        }

        private void deEndDate_EditValueChanged(object sender, EventArgs e)
        {
            this.ReLoadData();
        }

        private void gridViewPaymentTxtReestr_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0 && (int)gridViewPaymentTxtReestr.GetDataRow(e.RowHandle)["ID_Loaded_Status"] == 1)
                e.Appearance.BackColor = Color.LightGreen;
        }

        private void FrmPaymentTxtReestr_Load(object sender, EventArgs e)
        {
            this.LoadData();
        }
    }
}
