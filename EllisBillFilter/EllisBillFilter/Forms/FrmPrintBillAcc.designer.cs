﻿namespace EllisBillFilter
{
    partial class FrmPrintBillAcc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrintBillAcc));
            this.clBillFilter = new System.Windows.Forms.CheckedListBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblAddress = new System.Windows.Forms.Label();
            this.cbPeriod = new System.Windows.Forms.ComboBox();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.lblUK = new System.Windows.Forms.Label();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.cbEmailSend = new System.Windows.Forms.CheckBox();
            this.cbPrintFio = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // clBillFilter
            // 
            this.clBillFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clBillFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.clBillFilter.FormattingEnabled = true;
            this.clBillFilter.Location = new System.Drawing.Point(4, 51);
            this.clBillFilter.Name = "clBillFilter";
            this.clBillFilter.Size = new System.Drawing.Size(533, 152);
            this.clBillFilter.TabIndex = 0;
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(6, 267);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(114, 23);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "Сформировать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(126, 267);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAddress.Location = new System.Drawing.Point(2, 7);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 13);
            this.lblAddress.TabIndex = 3;
            this.lblAddress.Text = "Адрес: ";
            // 
            // cbPeriod
            // 
            this.cbPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPeriod.BackColor = System.Drawing.Color.White;
            this.cbPeriod.DataSource = this.periodBindingSource;
            this.cbPeriod.DisplayMember = "Name";
            this.cbPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbPeriod.FormattingEnabled = true;
            this.cbPeriod.Location = new System.Drawing.Point(416, 27);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Size = new System.Drawing.Size(121, 21);
            this.cbPeriod.TabIndex = 4;
            this.cbPeriod.ValueMember = "Period";
            this.cbPeriod.SelectedValueChanged += new System.EventHandler(this.cbPeriod_SelectedValueChanged);
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblUK
            // 
            this.lblUK.AutoSize = true;
            this.lblUK.Location = new System.Drawing.Point(3, 30);
            this.lblUK.Name = "lblUK";
            this.lblUK.Size = new System.Drawing.Size(139, 13);
            this.lblUK.TabIndex = 5;
            this.lblUK.Text = "Управляющая компания: ";
            // 
            // txtStatus
            // 
            this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatus.Location = new System.Drawing.Point(207, 261);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(330, 37);
            this.txtStatus.TabIndex = 6;
            // 
            // txtEmail
            // 
            this.txtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmail.Location = new System.Drawing.Point(207, 233);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(330, 20);
            this.txtEmail.TabIndex = 7;
            // 
            // cbEmailSend
            // 
            this.cbEmailSend.AutoSize = true;
            this.cbEmailSend.Location = new System.Drawing.Point(6, 233);
            this.cbEmailSend.Name = "cbEmailSend";
            this.cbEmailSend.Size = new System.Drawing.Size(183, 17);
            this.cbEmailSend.TabIndex = 8;
            this.cbEmailSend.Text = "отправить квитанции по E-mail:";
            this.cbEmailSend.UseVisualStyleBackColor = true;
            // 
            // cbPrintFio
            // 
            this.cbPrintFio.AutoSize = true;
            this.cbPrintFio.Checked = true;
            this.cbPrintFio.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPrintFio.Location = new System.Drawing.Point(6, 209);
            this.cbPrintFio.Name = "cbPrintFio";
            this.cbPrintFio.Size = new System.Drawing.Size(227, 17);
            this.cbPrintFio.TabIndex = 9;
            this.cbPrintFio.Text = "Печатать персональные данные (ФИО)";
            this.cbPrintFio.UseVisualStyleBackColor = true;
            // 
            // FrmPrintBillAcc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 299);
            this.Controls.Add(this.cbPrintFio);
            this.Controls.Add(this.cbEmailSend);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.lblUK);
            this.Controls.Add(this.cbPeriod);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.clBillFilter);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPrint);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrintBillAcc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Печать китанций";
            this.Load += new System.EventHandler(this.FrmPrintBillAcc_Load);
            this.Shown += new System.EventHandler(this.FrmPrintBillAcc_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox clBillFilter;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.ComboBox cbPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private DS.MDataSet mDataSet;
        private System.Windows.Forms.Label lblUK;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.CheckBox cbEmailSend;
        private System.Windows.Forms.CheckBox cbPrintFio;
    }
}