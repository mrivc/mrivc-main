using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using FastReport;
using System.Data.SqlClient;
using FastReport.Data;
using System.Collections;
using System.Xml;
using System.Diagnostics;
using DevExpress.XtraEditors.Controls;
using System.IO;
using FastReport.Design.StandardDesigner;

namespace MassBillNew
{
    public partial class XtraFrxDialog : DevExpress.XtraEditors.XtraForm
    {
        public XtraFrxDialog(string fileName, bool reportLoadedFromFile)
        {
            InitializeComponent();

            this.tableLayoutPanel2.ColumnStyles[1].Width = 0;

            this.FileName = fileName;
            this.ReportLoadedFromFile = reportLoadedFromFile;

            parameters = new List<SpParameter>();

            ShowParameters();

            OpenInDesignerBtnVisibilitySet();
        }

        void OpenInDesignerBtnVisibilitySet()
        {
            if (SqlClass.EllisAdminRole())
            {
                openInDesignerBtn.Visible = true;
            }
        }

        List<SpParameter> parameters;
        public List<SpParameter> Parameters
        {
            get
            {
                return parameters;
            }
            set
            {
                parameters = value;
            }
        }

        ArrayList clearParameters = new ArrayList();

        ArrayList createName = new ArrayList();

        ArrayList commonParameters = new ArrayList();



        DataSet ds = new DataSet("ParameterSet");


        private string fileName = "";
        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        private bool reportLoadedFromFile = false;
        public bool ReportLoadedFromFile
        {
            get
            {
                return reportLoadedFromFile;
            }
            set
            {
                reportLoadedFromFile = value;
            }
        }

        private Report returnedReport;
        public Report ReturnedReport
        {
            get
            {
                return returnedReport;
            }
        }

        public bool CreateManyReports
        {
            get
            {
                return checkEdit1.Checked;
            }
        }

        private void ReportPrepareByRowIndex()
        {
            EnvironmentSettings environmentSettings = new EnvironmentSettings();
            environmentSettings.ReportSettings.ShowProgress = false;

            

            if (checkEdit1.Checked)
            {
                string path = string.Empty;

                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    this.DialogResult = DialogResult.OK;

                    path = folderBrowserDialog1.SelectedPath ;
                

                foreach (DataRow row in ds.Tables[fileName].Rows)
                {
                    Report report1 = new Report();
                    report1 = LoadReport();

            
                        foreach (Parameter item in report1.Parameters)
                    {
                        if (item.Name != "MainConnection"
                            & item.Name != "CreateName"
                            & item.Name != "CommonParameters"
                            & item.Name != "ClearParameters"
                            & item.Name != "FilterHouse")
                        {
                            item.Value = DBNull.Value;
                        }
                        
                    }

                    foreach (DataColumn col in ds.Tables[fileName].Columns)
                    {
                        if (col.ColumnName != "FileName")
                        {
                            object rowValue = DBNull.Value;

                            if (row[col.ColumnName] != null)
                                if (row[col.ColumnName].ToString() != "" )
                                        rowValue = row[col.ColumnName];

                            if (rowValue.ToString() == "False") rowValue = false;
                            if (rowValue.ToString() == "True") rowValue = true;

                            try
                            {
                                report1.Parameters.FindByName(col.ColumnName).Value = rowValue;
                            }
                            catch (Exception)
                            {
                                

                            }
                            

                    

                        } 
                    }


                    string ExportFileName = row["FileName"].ToString();

                 

                    foreach (SpParameter p in parameters)
                    {
                        if (commonParameters.Contains(p.Name))//���� �������� ����� - ����� �������� �� �� ������ ����������, � �� ���������
                        {
                            object controlValue = DBNull.Value;

                            if (p.GetControlValue() != null)
                                if (p.GetControlValue().ToString() != "")
              
                                    controlValue = p.GetControlValue();

                            report1.Parameters.FindByName(p.Name).Value = controlValue;

                            if(createName.Contains(p.Name))
                                ExportFileName += " " + p.GetTextValue();
                        }



                    }

                    ExportFileName = ExportFileName.Trim();

                    ExportFileName = ExportFileName.Replace("/", " ");
                    ExportFileName = ExportFileName.Replace("\\", " ");
                    ExportFileName = ExportFileName.Replace(":", " ");
                    ExportFileName = ExportFileName.Replace("*", " ");
                    ExportFileName = ExportFileName.Replace("?", " ");
                    ExportFileName = ExportFileName.Replace("\"", " ");
                    ExportFileName = ExportFileName.Replace("<", " ");
                    ExportFileName = ExportFileName.Replace(">", " ");
                    ExportFileName = ExportFileName.Replace("|", " ");
                    ExportFileName = ExportFileName.Replace("�", " ");
                    ExportFileName = ExportFileName.Replace("�", " ");

                        // ������� �����
                        PrepareReport(report1);

                        switch (comboBoxEdit1.SelectedIndex)
                        {
                            case 0:
                                {
                                    // ������� ��������� �������� � Excel 2003
                                    FastReport.Export.Xml.XMLExport export = new FastReport.Export.Xml.XMLExport();

                                    export.PageBreaks = cePageBreaks.Checked;
                                    export.Wysiwyg = true;

                                    export.Export(report1, path + "\\" + ExportFileName + ".xls");
                                }
                                break;
                            case 1:
                                {
                                    // ������� ��������� �������� � Excel 2007
                                    FastReport.Export.OoXML.Excel2007Export export = new FastReport.Export.OoXML.Excel2007Export();

                                    export.PageBreaks = cePageBreaks.Checked;
                                    export.Wysiwyg = true;

                                    export.Export(report1, path + "\\" + ExportFileName + ".xlsx");
                                }
                                break;
                            case 2:
                                {
                                    // ������� ��������� �������� � pdf
                                    FastReport.Export.Pdf.PDFExport export = new FastReport.Export.Pdf.PDFExport();

                                    export.Export(report1, path + "\\" + ExportFileName + ".pdf");
                                }
                                break;
                            case 3:
                                {
                                    // ������� ��������� �������� � dbf
                                    FastReport.Export.Dbf.DBFExport export = new FastReport.Export.Dbf.DBFExport();

                                    string fields = string.Empty;

                                    TableDataSource qMaster = report1.GetDataSource("qMaster") as TableDataSource;

                                    foreach (Column column in qMaster.Columns)
                                    {
                                        fields += column.Name + ";";
                                    }

                                    fields = fields.Trim(';');

                                    export.FieldNames = fields;

                                    export.Export(report1, path + "\\" + ExportFileName + ".dbf");
                                }
                                break;
                            case 4:
                                {
                                    // ������� ��������� �������� � Excel 2003
                                    FastReport.Export.Xml.XMLExport export = new FastReport.Export.Xml.XMLExport();

                                    export.PageBreaks = cePageBreaks.Checked;
                                    export.Wysiwyg = true;

                                    export.Export(report1, path + "\\" + ExportFileName + ".xls");

                                    // ������� ��������� �������� � pdf
                                    FastReport.Export.Pdf.PDFExport export1 = new FastReport.Export.Pdf.PDFExport();

                                    export1.Export(report1, path + "\\" + ExportFileName + ".pdf");
                                }
                                break;
                        }


                        this.Close();

                        
                    
                }

               

                MessageBox.Show("������ ������� ������������!","",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }

                

            }
            else
            {
                Report report1 = new Report();
                report1 = LoadReport();

                foreach (SpParameter p in parameters)
	            {
                    object controlValue = DBNull.Value;

                    if (p.GetControlValue() != null)
                        if (p.GetControlValue().ToString() != "" )
           
                            controlValue = p.GetControlValue();

                    report1.Parameters.FindByName(p.Name).Value = controlValue;

                    
	            }

                if (filterHouse)
                {
                    XtraHouseFilterDialog houseFilterDialog = new XtraHouseFilterDialog(AccountFilter.FilterType.House,true);
                    if (houseFilterDialog.ShowDialog() == DialogResult.OK)
                    {
                        report1.Parameters.FindByName("FilterHouse").Value = houseFilterDialog.StringOfQuery;

                        // ������� �����
                        PrepareReport(report1);

                        returnedReport = report1;
                    }
                    else
                    {
                        MessageBox.Show("�� ������ �����");
                    }
                    
                }
                else
                {
                    if (filterAccount)
                    {
                        XtraHouseFilterDialog houseFilterDialog = new XtraHouseFilterDialog(AccountFilter.FilterType.Account,true);
                        if (houseFilterDialog.ShowDialog() == DialogResult.OK)
                        {
                            report1.Parameters.FindByName("FilterAccount").Value = houseFilterDialog.StringOfQuery;

                            // ������� �����
                            PrepareReport(report1);

                            returnedReport = report1;
                        }
                        else
                        {
                            MessageBox.Show("�� ������ �����");
                        }

                    }
                    else
                    {

                        this.Cursor = Cursors.WaitCursor;

                        // ������� �����
                        PrepareReport(report1);

                        returnedReport = report1;

                      
                    }
                }

                
               

            }
            
        }


        private void PrepareReport(Report report)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            report.Prepare();

            stopWatch.Stop();

            if (reportLoadedFromFile == false) //���� ���� ��� �������� �� ������ ����� ����� ����, �� ������� � ��� ����� ����������
            {
                SqlConnection conn = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.CommandText = "town.FRX_log_add";
                cmd.Parameters.Add("Full_File_Name",SqlDbType.VarChar);
                cmd.Parameters.Add("Execution_Time", SqlDbType.BigInt);
                cmd.Parameters["Full_File_Name"].Value = fileName;
                cmd.Parameters["Execution_Time"].Value = stopWatch.ElapsedMilliseconds;
                cmd.Connection = conn;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

        }


        private void button1_Click(object sender, EventArgs e)
        {
                ReportPrepareByRowIndex();
        }

        DataTable AddressSet = new DataTable();

        private void AddParametersButton_Click(object sender, EventArgs e)
        {

            string HouseString = string.Empty;
            

            if (filterHouse)
            {
                XtraHouseFilterDialog houseFilterDialog = new XtraHouseFilterDialog(AccountFilter.FilterType.House, false,AddressSet);

                if (houseFilterDialog.ShowDialog() == DialogResult.OK)
                {
                    HouseString = houseFilterDialog.StringOfQuery;
                    AddressSet = houseFilterDialog.AddressFilterSet;
                }
                else
                {
                    MessageBox.Show("�� ������ �����");
                    return;
                }

            }

            if (filterAccount)
            {
                XtraHouseFilterDialog houseFilterDialog = new XtraHouseFilterDialog(AccountFilter.FilterType.Account,false,AddressSet);

                if (houseFilterDialog.ShowDialog() == DialogResult.OK)
                {
                    HouseString = houseFilterDialog.StringOfQuery;
                    AddressSet = houseFilterDialog.AddressFilterSet;
                }
                else
                {
                    MessageBox.Show("�� ������ �����");
                    return;
                }

            }

            string ExportFileName = string.Empty;

            foreach (string item in createName)
            {
                foreach (SpParameter p in parameters)
                {
                    if(commonParameters.Contains(p.Name) == false)
                        if (item.ToLower() == p.Name.ToLower())
                            ExportFileName = ExportFileName + p.GetTextValue().ToString() + " ";
                }
            }

            ExportFileName = ExportFileName.Trim();

            ExportFileName = ExportFileName.Replace("/", " ");
            ExportFileName = ExportFileName.Replace("\\", " ");
            ExportFileName = ExportFileName.Replace(":", " ");
            ExportFileName = ExportFileName.Replace("*", " ");
            ExportFileName = ExportFileName.Replace("?", " ");
            ExportFileName = ExportFileName.Replace("\"", " ");
            ExportFileName = ExportFileName.Replace("<", " ");
            ExportFileName = ExportFileName.Replace(">", " ");
            ExportFileName = ExportFileName.Replace("|", " ");
            ExportFileName = ExportFileName.Replace("�", " ");
            ExportFileName = ExportFileName.Replace("�", " ");

            XtraFileName fileNameDialog = new XtraFileName(ExportFileName);

            fileNameDialog.FileName = ExportFileName;

            if (fileNameDialog.ShowDialog() == DialogResult.OK)
            {
                ExportFileName = fileNameDialog.FileName;
            }
            else
            {
                return;
            }






            if (!ds.Tables.Contains(fileName))
            {
                ds.Tables.Add(fileName);

                ds.Tables[fileName].Columns.Add("FileName");

                if (filterHouse)
                {
                    ds.Tables[fileName].Columns.Add("FilterHouse");
                    ds.Tables[fileName].Columns.Add("AddressSet");
                }

                if (filterAccount)
                {
                    ds.Tables[fileName].Columns.Add("FilterAccount");
                    ds.Tables[fileName].Columns.Add("AddressSet");
                }

                foreach (SpParameter p in parameters)
                {
                    if(commonParameters.Contains(p.Name) == false)//���� �������� �� ����� - �������� ��� � ������ ��� ����������
                    ds.Tables[fileName].Columns.Add(p.Name);
                }

                listBoxControl1.DataSource = ds.Tables[fileName];

                listBoxControl1.DisplayMember = "FileName";

                //parameterGridControl.DataSource = ds.Tables[fileName];




            }

            DataRow dr = ds.Tables[fileName].Rows.Add();

            dr["FileName"] = ExportFileName;

            if (filterAccount | filterHouse)
            {
                try
                {
                    StringWriter writer = new StringWriter();
                    AddressSet.WriteXml(writer, XmlWriteMode.WriteSchema, false);
                    dr["AddressSet"] = writer.ToString();
                    AddressSet.Clear();
                }
                catch (Exception)
                {
                    
                    
                }
                
            }

            if (filterHouse)
            {
                dr["FilterHouse"] = HouseString;
            }

            if (filterAccount)
            {
                dr["FilterAccount"] = HouseString;
            }

            foreach (SpParameter p in parameters)
            {
                if (commonParameters.Contains(p.Name) == false)
                {
                    object controlValue = p.GetControlValue();
                    dr[p.Name] = controlValue;
                }

                if (clearParameters.Contains(p.Name))
                {
                    p.SetNullValue();
                    p.RefreshEditValue();
                }
            }

        }



        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (CreateManyReports)
            {
                simpleButton1.DialogResult = DialogResult.None;

                foreach (SpParameter p in parameters)
                {
                    if (commonParameters.Contains(p.Name) == true)
                    {
                        p.RelatedGroupControl.Text = p.Description + "\n(����� ��������)";
                    }
                    //else
                    //{
                    //    p.SetNullValue();
                    //}

                }

                //this.FormBorderStyle = FormBorderStyle.SizableToolWindow;

                if (height < 380)
                    this.Size = new Size(731, 470);
                else
                    this.Size = new Size(731, height);

                tableLayoutPanel2.ColumnStyles[1].Width = 275;
                panelControl2.Width = 270;
                fileGroupControl.Width = 240;

                labelControl1.Visible = true;
                comboBoxEdit1.Visible = true;
                cePageBreaks.Visible = true;
            }
            else
            {
                foreach (SpParameter p in parameters)
                {
                    if (commonParameters.Contains(p.Name) == true)
                    {
                        p.RelatedGroupControl.Text = p.Description;
                    }


                }

                simpleButton1.DialogResult = DialogResult.OK;
                //this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                tableLayoutPanel2.ColumnStyles[1].Width = 0;

                this.Size = new Size(456, height);

                labelControl1.Visible = false;
                comboBoxEdit1.Visible = false;
                cePageBreaks.Visible = false;
            }
        }


        private Report LoadReport()
        {
            Report report = new Report();

            //���� ����� �������� �� �����
            if (this.ReportLoadedFromFile)
            {
                report.Load(fileName);
                report.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);
                
            }
            else //���� ����� �������� � ��
            {

                SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                cmd.CommandText = string.Format("select convert(varbinary(max),[file]) from dbo.module where name ='{0}' ", fileName);

                sqlConnection1.Open();

                byte[] module = (byte[])cmd.ExecuteScalar();

                sqlConnection1.Close();

                report.LoadFromString(Encoding.UTF8.GetString(module));

                report.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);

               
            }

            return report;
        }

        
        class CheckedChangeEvents
        {
            private CheckEdit checkEdit1;
            public CheckEdit CheckEdit1
            {
                get
                {
                    return checkEdit1;
                }
                set
                {
                    checkEdit1 = value;
                }
            }

            private CheckedComboBoxEdit checkedComboBoxEdit;
            public CheckedComboBoxEdit CheckedComboBoxEdit
            {
                get
                {
                    return checkedComboBoxEdit;
                }
                set
                {
                    checkedComboBoxEdit = value;
                }
            }

            private CheckEdit checkEdit;
            public CheckEdit CheckEdit
            {
                get
                {
                    return checkEdit;
                }
                set
                {
                    checkEdit = value;
                }
            }

            

             public void checkedit_Click(object sender, EventArgs e)
             {
                 //if (!checkEdit1.Checked)
                 //{
                     int checkedCount = 0;

                     foreach (CheckedListBoxItem item in checkedComboBoxEdit.Properties.Items)
                     {
                         if (item.CheckState == CheckState.Checked)
                         {
                             checkedCount += 1;
                         }
                     }

                     if (checkedCount != 0)
                         if (checkedComboBoxEdit.Properties.Items.Count != checkedCount)
                         {
                             object all = string.Empty;
                             foreach (CheckedListBoxItem item in checkedComboBoxEdit.Properties.Items)
                             {
                                 all += item.Value+",";
                             }

                             checkedComboBoxEdit.EditValue = all;
                             checkedComboBoxEdit.RefreshEditValue();
                         }
                         else
                         {
                             checkedComboBoxEdit.EditValue = string.Empty;
                             checkedComboBoxEdit.RefreshEditValue();

                         } 
                 //}
             }



             public void chcb_EditValueChanged(object sender, EventArgs e)
             {
                
                     if (checkedComboBoxEdit.EditValue.ToString() != "")
                         checkEdit.Checked = false;
                     else
                         checkEdit.Checked = true;

             }

             public void checkedit_CheckedChanged(object sender, EventArgs e)
             {
                 //if (!checkEdit1.Checked)
                 //{
                     if (checkedComboBoxEdit.EditValue.ToString() != "")
                         checkEdit.Checked = false;
                     else
                         checkEdit.Checked = true;
                 //}
                 //else
                 //{
                 //    if (checkEdit.Checked)
                 //    {
                 //        checkedComboBoxEdit.EditValue = "";
                 //        checkedComboBoxEdit.RefreshEditValue();
                 //    }
                 //}
             }
            
        }

        int height = 200;

        bool filterHouse = false; //�������� ���� � ������� ����� 

        bool filterAccount = false;

        //������ ��������� ������������ �������� � ������� ���������
        Point point = new Point(0, 0);

        private string ParameterSqlQuery(string name, Report report)
        {
            string sqlQuery = string.Empty;

            SqlConnection sqlConnection1 = new SqlConnection(StaticConfig.MainConnection.ConnectionString);

            foreach (Parameter item in report.Parameters.FindByName(name).Parameters)
            {
                if (item.Name.ToLower() == "sql")
                {
                    sqlQuery = item.Expression.Trim('"');

                }

            }

            if (sqlQuery == string.Empty)

            {
                sqlConnection1.Open();

                SqlCommand cmd = new SqlCommand("select query from [town].[FRX_Parameter_SQL_Query] where @name like '%'+name+'%'", sqlConnection1);
                cmd.Parameters.AddWithValue("name", name);

                if (cmd.ExecuteScalar() != null)
                sqlQuery = cmd.ExecuteScalar().ToString();

                sqlConnection1.Close();
            }

            return sqlQuery;
        }

        //����� ���������� �� ���������� ����
        private void ShowParameters()
        {
            this.Size = new Size(0, 0);

            Report report1 = new Report();
            report1 = LoadReport();

            int n = 0;

                        foreach (Parameter p in report1.Parameters)
                        {
                            if (p.Name == "CommonParameters")
                            {
                                commonParameters.AddRange(p.Expression.Trim('"').Replace(" ", "").Split(';'));
                            }
                        }

                        foreach ( Parameter p in report1.Parameters)
                        {
                            if (p.Name == "FilterHouse")
                                filterHouse = true;

                            if (p.Name == "FilterAccount")
                                filterAccount = true;

                            

                            if (p.Name == "ClearParameters")
                            {
                                clearParameters.AddRange(p.Expression.Trim('"').Replace(" ", "").Split(';'));
                            }

                            if (p.Name == "CreateName")
                            {
                                createName.AddRange(p.Expression.Trim('"').Replace(" ","").Split(';'));
                            }




                            if (p.Name != "XmlDataSource" & p.Name != "DbfDataSource" & p.Name != "MainConnection" & p.Name != "FilterAccount" & p.Name != "FilterHouse" & p.Name != "CreateName" & p.Name != "ClearParameters" & p.Name != "CommonParameters" & p.DataType.Name != "Boolean")
                            {
                                n += 1;

                                

                                GroupControl groupControl = new GroupControl();
                                
                                groupControl.Text = p.Description;

                                groupControl.LookAndFeel.UseDefaultLookAndFeel = false;
                                groupControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
                                groupControl.Size = new Size(214, 60);

                                if (n > 1)
                                if (n % 2 != 0)
                                {
                                    point.X = 0;
                                    point.Y += 65;
                                    height += 65;


                                }
                                else
                                {
                                    point.X = 219;
                                    
                                }

                                if (n > 1)
                                {
                                    this.Size = new Size(456, height);
                                    
                                }
                                else
                                    this.Size = new Size(233, height);

                                groupControl.Location = point;

                                this.panel1.Controls.Add(groupControl);

                                if (parameters.Find(item => item.Name == p.Name) != null)
                                    continue;

                                //��������� �� ������� ������� ���� ����, ������� ����� �������� ������
                                switch (p.DataType.Name)
                                {

                                    case "String":
                                        {
                                            SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                                            CheckEdit checkedit = new CheckEdit();
                                            checkedit.Text = "���";
                                            checkedit.Size = new Size(45, 19);
                                            checkedit.Location =new Point(6,30);
                                            

                                            checkedit.Checked = true;


                                            CustomEdit chcb = new CustomEdit();
                                            chcb.Location = new Point(checkedit.Width+6,30);
                                            chcb.Properties.SelectAllItemVisible = false;

                                            CheckedChangeEvents checkedChangeEvents = new CheckedChangeEvents();
                                            checkedChangeEvents.CheckedComboBoxEdit = chcb;
                                            checkedChangeEvents.CheckEdit = checkedit;
                                            checkedChangeEvents.CheckEdit1 = checkEdit1;
                                            //checkedit.CheckedChanged += new EventHandler(checkedChangeEvents.checkedit_CheckedChanged);
                                            chcb.EditValueChanged += new EventHandler(checkedChangeEvents.chcb_EditValueChanged);
                                            checkedit.Click += new EventHandler(checkedChangeEvents.checkedit_Click);
                                            checkedit.CheckedChanged += new EventHandler(checkedChangeEvents.checkedit_CheckedChanged);

                                            chcb.Size = new Size(150, 0);

                                            string sqlQuery = ParameterSqlQuery(p.Name,report1);

                                            DataTable dt = new DataTable();
                                            sqlConnection1.Open();
                                            SqlDataAdapter da = new SqlDataAdapter(sqlQuery,sqlConnection1);

                                            da.Fill(dt);

                                            sqlConnection1.Close();

                                            chcb.Properties.DataSource = dt;
                                            chcb.Properties.DisplayMember = dt.Columns[1].ColumnName;
                                            chcb.Properties.ValueMember = dt.Columns[0].ColumnName;
                                            chcb.Properties.IncrementalSearch = true;
   
                                            

                                            foreach (Parameter item in p.Parameters)
                                            {
                                                if (item.Name == "default")
                                                {
                                                    chcb.EditValue = item.Expression.Trim('"');
                                                    chcb.RefreshEditValue();
                                                }
                                                else
                                                {
                                                    chcb.EditValue = string.Empty;
                                                }
                                            }

                                            if (p.Parameters.Count == 0)
                                            {
                                                chcb.EditValue = string.Empty;
                                            }
                        
                                            chcb.Properties.PopupFormSize = new Size(300,400);

                                            
                                            chcb.Properties.ShowButtons = false;

                                            chcb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));

                                            groupControl.Controls.Add(checkedit);
                                            groupControl.Controls.Add(chcb);

                                            parameters.Add(new SpParameter(p.Name,p.Description,chcb.EditValue,groupControl, chcb, SpParameterControlType.CheckedComboBoxList,checkedit));
                                        }
                                        break;
                                    case "Int32":
                                        {
                                            SqlConnection sqlConnection1 = StaticConfig.MainConnection;

                                            LookUpEdit cb = new LookUpEdit();
                                            cb.Location = new Point(6, 30);

                                                cb.Size = new Size(200, 0);

                                                string sqlQuery = ParameterSqlQuery(p.Name, report1);

                                            DataTable dt = new DataTable();
                                            sqlConnection1.Open();
                                            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, sqlConnection1);

                                            da.Fill(dt);

                                            sqlConnection1.Close();

                                            cb.Properties.DataSource = dt;
                                            cb.Properties.DisplayMember = dt.Columns[1].ColumnName;
                                            cb.Properties.ValueMember = dt.Columns[0].ColumnName;

                                            cb.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(dt.Columns[1].ColumnName, dt.Columns[1].ColumnName)});

                                            cb.Properties.NullText = "";
                                            cb.Properties.ShowHeader = false;

                                            

                                            cb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));

                                           
                                            cb.Properties.DropDownRows = 20;
                                            cb.Properties.PopupWidth = 300;

                                            groupControl.Controls.Add(cb);

                                            sqlConnection1.Open();

                                            SqlCommand cmd = new SqlCommand("select dbo.Work_Period()", sqlConnection1);

                                            

                                            foreach (Parameter item in p.Parameters)
                                            {
                                                if (item.Name == "default")
                                                {
                                                    cb.EditValue = item.Expression.Trim('"');

                                                }

                                            }

                                                    if (p.Name.Contains("period") & cb.EditValue == null)
                                                        cb.EditValue = cmd.ExecuteScalar();



                                            sqlConnection1.Close();

                                            parameters.Add(new SpParameter(p.Name,p.Description,cb.EditValue, groupControl, cb, SpParameterControlType.LookUpEdit));
                                        }
                                        break;
                                    case "DateTime":
                                        {

                                            DateEdit de = new DateEdit();
                                            de.Location = new Point(6, 30);

                                            de.Size = new Size(194, 0);

                                            de.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));



                                            groupControl.Controls.Add(de);

                                            foreach (Parameter item in p.Parameters)
                                            {
                                                if (item.Name == "default")
                                                {
                                                    de.EditValue = item.Expression.Trim('"');
                                                }
                                                else
                                                {
                                                    de.EditValue = string.Empty;
                                                }
                                            }

                                            parameters.Add(new SpParameter(p.Name,p.Description,de.EditValue, groupControl, de, SpParameterControlType.DateTime));
                                        }
                                        break;
                                }

                                

                                
                            }
                        }

                        n = 0;

                        foreach (Parameter p in report1.Parameters)
                        {
                            if (p.Name != "MainConnection" & p.Name != "FilterAccount" & p.Name != "FilterHouse" & p.Name != "CreateName" & p.Name != "ClearParameters" & p.DataType.Name == "Boolean")
                            {
                                

                                CheckEdit checkEdit = new CheckEdit();

                                

                                checkEdit.Width = 18;

                                LabelControl labelControl = new LabelControl();
                                labelControl.Text = p.Description;

                                labelControl.Width = 30;

                                if (n == 0)
                                    
                                    {
                                        point.X = 0;
                                        point.Y += 65;
                                        height += 18;


                                    }
                                else
                                {
                                    point.X = 0;
                                    point.Y += 18;
                                    height += 18;
                                }
                                   

                                //if (n > 1)
                                //{
                                    this.Size = new Size(456, height);

                                //}
                                //else
                                //    this.Size = new Size(223, height);

                                checkEdit.Location = point;

                                labelControl.Location = new Point(point.X +20,point.Y+2);

                                this.panel1.Controls.Add(checkEdit);
                                this.panel1.Controls.Add(labelControl);

                                foreach (Parameter item in p.Parameters)
                                {
                                    if (item.Name == "default")
                                    {
                                        checkEdit.EditValue = item.Expression.Trim('"');
                                    }
                                    else
                                    {
                                        checkEdit.EditValue = string.Empty;
                                    }
                                }

                                parameters.Add(new SpParameter(p.Name, p.Description,checkEdit.EditValue.ToString(), checkEdit, SpParameterControlType.CheckEdit));

                                n += 1;
                            }

                        }







        }

        private void EmptyFileListMessage()
        {
            MessageBox.Show("������ ������ ����", "����������", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (checkEdit1.Checked)
            {
                if(ds.Tables.Count == 0)
                {
                    EmptyFileListMessage();
                    return;
                }
                else if (ds.Tables[fileName].Rows.Count == 0)
                {
                    EmptyFileListMessage();
                    return;
                }
                    
            }


            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                XmlDocument doc = new XmlDocument();

                XmlElement element1 = doc.CreateElement(string.Empty, "FrxReportsParameters", string.Empty);
                doc.AppendChild(element1);


                int n = 0;

                if (!checkEdit1.Checked)
                {
                    element1.SetAttribute("Single", "1");

                    XmlElement element2 = doc.CreateElement(string.Empty, "ParameterSet" + n, string.Empty);

                    foreach (SpParameter p in parameters)
                    {
                        object controlValue = DBNull.Value;

                        if (p.GetControlValue() != null)
                            if (p.GetControlValue().ToString() != "")

                                controlValue = p.GetControlValue();

                        element2.SetAttribute(p.Name,controlValue.ToString());
                        element1.AppendChild(element2);

                    }
                }
                else
                {

                    foreach (DataRow row in ds.Tables[fileName].Rows)
                    {

                        XmlElement element2 = doc.CreateElement(string.Empty, "ParameterSet" + n, string.Empty);



                        foreach (DataColumn col in ds.Tables[fileName].Columns)
                        {

                            element2.SetAttribute(col.ColumnName, row[col.ColumnName].ToString());
                            element1.AppendChild(element2);


                        }

                        n += 1;
                    }
                }


                doc.Save(saveFileDialog1.FileName);


            }//if (saveFileDialog1.ShowDialog() == DialogResult.OK)


        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {

          
        }

        private void LoadManyReports(XmlDocument xmlDoc)
        {
            foreach (XmlNode item in xmlDoc)
            {
                if (item.Name == "FrxReportsParameters")
                {
                    if (!ds.Tables.Contains(fileName))
                    {
                        ds.Tables.Add(fileName);
                    }
                    else
                    {
                        ds.Tables[fileName].Clear();
                        //ds.Tables.Add(fileName);
                    }

                    foreach (XmlNode item1 in item.ChildNodes)
                    {
                        if (item1.Name.Substring(0, 12) == "ParameterSet")
                        {


                            DataRow dr = ds.Tables[fileName].Rows.Add();

                            foreach (XmlAttribute attr in item1.Attributes)
                            {
                                if (!ds.Tables[fileName].Columns.Contains(attr.Name))
                                {
                                    ds.Tables[fileName].Columns.Add(attr.Name);
                                }
                                dr[attr.Name] = attr.Value;
                            }

                        }

                    }

                }

            }




            listBoxControl1.DataSource = ds.Tables[fileName];

            listBoxControl1.DisplayMember = "FileName";
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(openFileDialog1.FileName);


                if (!checkEdit1.Checked)
                {
                    foreach (XmlNode item in xmlDoc)
                    {
                        bool single = false;

                        foreach (XmlAttribute attrS in item.Attributes)
                        {
                            if (attrS.Name == "Single" & attrS.Value == "1")
                            {
                                single = true;
                            }
                        }

                        if (item.Name == "FrxReportsParameters" & single)
                        {


                            foreach (XmlNode item1 in item.ChildNodes)
                            {
                                if (item1.Name.Substring(0, 12) == "ParameterSet")
                                {


                                    foreach (XmlAttribute attr in item1.Attributes)
                                    {

                                        foreach (SpParameter p in parameters)
                                        {
                                            object controlValue = DBNull.Value;

                                            if (p.Name == attr.Name)
                                                p.SetControlValue(attr.Value);
                                                   

                                        }

                                    }

                                }

                            }

                        }
                        else
                        {
                            checkEdit1.Checked = !checkEdit1.Checked;
                            LoadManyReports(xmlDoc);
                        }

                    
                    }
                }

                else
                {
                    LoadManyReports(xmlDoc);
                   
                }

            }//if (openFileDialog1.ShowDialog() == DialogResult.OK)


        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
         
            
        }

        private void listBoxControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if(ds.Tables.Contains(fileName))
                if(ds.Tables[fileName].Rows.Count != 0)
                {

                    if (!listBoxControl1.SelectedIndices.Contains(listBoxControl1.IndexFromPoint(e.Location)))
                        listBoxControl1.SelectedIndex = listBoxControl1.IndexFromPoint(e.Location);
                    
                    if (listBoxControl1.SelectedItems.Count == 0)
                        popupMenu2.ShowPopup(Control.MousePosition);
                    else
                        popupMenu1.ShowPopup(Control.MousePosition); 
        
                }
                
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MessageBox.Show("�� �������, ��� ������ �������� ������?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                ds.Tables[fileName].Clear();
            }
        }

        private void deleteListBoxItems()
        {
            ArrayList indicies = new ArrayList();
            //ds.Tables[fileName].Rows.Remove(((DataRowView)listBoxControl1.SelectedItem).Row);
            foreach (object item in listBoxControl1.SelectedItems)
            {
                indicies.Add(item);
            }

            foreach (DataRowView item in indicies)
            {
                ds.Tables[fileName].Rows.Remove(item.Row);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (listBoxControl1.SelectedItems.Count != 0)
                if (listBoxControl1.SelectedItems.Count == 1)
                {
                    if (MessageBox.Show("�� �������, ��� ������ ������� ������ " + ((DataRowView)listBoxControl1.SelectedItem)[0].ToString() + "?", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        deleteListBoxItems();
                    }
                }
                else
                {
                    if (MessageBox.Show("�� �������, ��� ������ ������� ���������� ������?", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        deleteListBoxItems();
                    }
                }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            DataRowView dr = (DataRowView)listBoxControl1.SelectedItem;

           
            
                foreach (SpParameter p in parameters)
                {
                    if (commonParameters.Contains(p.Name) == false)
                    {
                        try
                        {
                            p.SetControlValue(dr[p.Name]);
                            p.RefreshEditValue();
                        }
                        catch (Exception)
                        {
                            
                           
                        }
                        
                    }

                }

                if (filterHouse | filterAccount)
                {
                    try
                    {
                        StringReader reader = new StringReader(dr["AddressSet"].ToString());
                        AddressSet.ReadXml(reader);
                    }
                    catch (Exception)
                    {
                        
                       
                    }
                    

                    
                }

                dr.Delete();
        }

        private void openInDesignerBtn_Click(object sender, EventArgs e)
        {
            StaticConfig.OpenInDesigner(this.reportLoadedFromFile, fileName).Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            foreach (string item in commonParameters)
            {
                MessageBox.Show(item);
            }
        }
    }
}