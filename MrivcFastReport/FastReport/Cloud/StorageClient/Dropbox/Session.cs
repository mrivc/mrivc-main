using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using FastReport.Cloud.OAuth;

namespace FastReport.Cloud.StorageClient.Dropbox
{
    /// <summary>
    /// Represents the Dropbox session.
    /// </summary>
    public class Session
    {
        #region Fields

        private CloudProxySettings proxySettings;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Session"/> class.
        /// </summary>
        public Session(CloudProxySettings proxySettings)
        {
            this.proxySettings = proxySettings;
        }

        #endregion // Constructors

        #region Private Methods

        private string BuildRequestTokenUrl(ServiceContext service, ConsumerContext consumer)
        {
            Auth auth = new Auth();
            return auth.BuildSignedUrl(service.RequestTokenUrl, HttpMethod.Get, consumer, new Token("", ""));
        }

        private string BuildUserAuthorizationUrl(string baseUserAuthorizationUrl, string callbackUrl, Token token)
        {
            StringBuilder sb = new StringBuilder(baseUserAuthorizationUrl);
            sb.AppendFormat("?oauth_token={0}", HttpUtils.UrlEncode(token.TokenKey));
            sb.AppendFormat("&oauth_callback={0}", HttpUtils.UrlEncode(callbackUrl));
            return sb.ToString();
        }

        private string BuildAccessTokenUrl(string baseAccessTokenUrl, ConsumerContext consumer, Token token)
        {
            Auth auth = new Auth();
            return auth.BuildSignedUrl(baseAccessTokenUrl, HttpMethod.Get, consumer, token);
        }

        private bool CopyStreamData(Stream source, Stream target)
        {
            if (source == null || target == null)
            {
                return false;
            }
            if (!source.CanRead || !target.CanWrite)
            {
                return false;
            }
            byte[] buffer = new byte[source.Length];
            int bytes = source.Read(buffer, 0, Convert.ToInt32(source.Length));
            target.Write(buffer, 0, bytes);
            source.Position = 0;
            target.Position = 0;
            return true;
        }

        private Token ObtainToken(string requestTokenUrl)
        {
            Token token = null;
            try
            {
                Uri uri = new Uri(requestTokenUrl);
                WebRequest request = WebRequest.Create(uri);
                request.Method = HttpMethod.Get;
                RequestUtils.SetProxySettings(request, proxySettings);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                token = Parser.ParseToken(stream);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            return token;
        }

        private Token ObtainRequestToken(ServiceContext service, ConsumerContext consumer)
        {
            string requestTokenUrl = BuildRequestTokenUrl(service, consumer);
            return ObtainToken(requestTokenUrl);
        }

        private Token ObtainAccessToken(ServiceContext service, ConsumerContext consumer, Token requestToken)
        {
            string accessTokenUrl = BuildAccessTokenUrl(service.AccessTokenUrl, consumer, requestToken);
            return ObtainToken(accessTokenUrl);
        }

        #endregion // Private Methods

        #region Public Methods

        /// <summary>
        /// Obtains the request token.
        /// </summary>
        /// <param name="config">The Dropbox storage configuration.</param>
        /// <param name="consumer">The consumer context.</param>
        /// <returns>The request token.</returns>
        public Token ObtainDropboxRequestToken(DropboxStorageConfig config, ConsumerContext consumer)
        {
            ServiceContext service = new ServiceContext(config.RequestTokenUrl, config.UserAuthorizationUrl, config.CallbackUrl, config.AccessTokenUrl);
            return ObtainRequestToken(service, consumer);
        }

        /// <summary>
        /// Obtains the authorization URL.
        /// </summary>
        /// <param name="config">The Dropbox storage configuration.</param>
        /// <param name="requestToken">The request token.</param>
        /// <returns></returns>
        public string ObtainAuthorizationUrl(DropboxStorageConfig config, Token requestToken)
        {
            return BuildUserAuthorizationUrl(config.UserAuthorizationUrl, config.CallbackUrl, requestToken);
        }

        /// <summary>
        /// Obtains the access token.
        /// </summary>
        /// <param name="config">The Dropbox storage configuration.</param>
        /// <param name="consumer">The consumer.</param>
        /// <param name="requestToken">The request token string.</param>
        /// <returns>The access token.</returns>
        public Token ObtainDropboxAccessToken(DropboxStorageConfig config, ConsumerContext consumer, Token requestToken)
        {
            ServiceContext service = new ServiceContext(config.RequestTokenUrl, config.UserAuthorizationUrl, config.CallbackUrl, config.AccessTokenUrl);
            return ObtainAccessToken(service, consumer, requestToken);
        }

        /// <summary>
        /// Obtains the URL for uploading a file using PUT semantics.
        /// </summary>
        /// <param name="baseFilesPutUrl">The base URL for files_put command.</param>
        /// <param name="root">The root relative to which path is specified.</param>
        /// <param name="filename">The name of file you want to write to.</param>
        /// <param name="consumer">The consumer.</param>
        /// <param name="accessToken">The access token.</param>
        /// <returns>The URL for uploading a file.</returns>
        public string ObtainFilesPutUrl(string baseFilesPutUrl, string root, string filename, ConsumerContext consumer, Token accessToken)
        {
            StringBuilder sb = new StringBuilder(baseFilesPutUrl);
            sb.AppendFormat("/{0}/{1}", root, filename);
            Auth auth = new Auth();
            return auth.BuildSignedUrl(sb.ToString(), HttpMethod.Put, consumer, accessToken);
        }

        #endregion // Public Methods
    }
}
