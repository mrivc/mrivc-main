﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;

namespace ErcTxtConvert
{
    public class FileHelper
    {
        public static string RenameFile(string fileFullName, string newFileName)
        {
            int ind = fileFullName.LastIndexOf("\\");
            
            string path = fileFullName.Substring(0, ind);
            
            return path + "\\" + newFileName;
        }

        public static string GetFileExt(string file)
        {
            return file.Substring(file.LastIndexOf(".") + 1);
        }

        public static bool ExistFile(string fileName)
        {
            FileInfo _fi = new FileInfo(fileName);
            return _fi.Exists;
        }

        public static void DeleteFileIfExist(string fileName)
        {
            FileInfo _fi = new FileInfo(fileName);
            if (_fi.Exists)
                _fi.Delete();
        }

        public static void DeleteFiles(IEnumerable<string> collection)
        {
            FileInfo _fi;
            IEnumerator<string> en = collection.GetEnumerator();

            while (en.MoveNext())
            {
                _fi = new FileInfo(en.Current);
                if (_fi.Exists)
                    _fi.Delete();
            }
        }

        public static void WriteTextFile(string fileName, string fileContent)
        {
            CreateDirectory(GetFilePath(fileName));
            StreamWriter _sr = new StreamWriter(fileName, false, Encoding.Default);
            _sr.Write(fileContent);
            _sr.Close();
        }

        public static void WriteTextFile(string fileName, string fileContent, Encoding encoding)
        {
            CreateDirectory(GetFilePath(fileName));
            StreamWriter _sr = new StreamWriter(fileName, false, encoding);
            _sr.Write(fileContent);
            _sr.Close();
        }

        public static string CreateDirectory(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);
            return directoryPath;
        }

        public static bool DirectoryExists(string directoryPath)
        {
            return Directory.Exists(directoryPath);
        }

        public static string GetFilePath(string filePath)
        {
            return Path.GetDirectoryName(filePath) + "\\";
        }

        public static string GetFileName(string filePath)
        {
            return Path.GetFileName(filePath);
        }

        public static void CopyFile(string copyFile, string destFilePath)
        {
            FileInfo _fi = new FileInfo(copyFile);
            _fi.CopyTo(destFilePath, true);
        }

        public static void RewriteFileWithRename(string copyFile, string destFilePath)
        {
            FileInfo _fi = new FileInfo(copyFile);
            _fi.CopyTo(destFilePath, true);
            _fi.Delete();
        }

        public static void GetDirectoryTree(System.IO.DirectoryInfo root, List<System.IO.FileInfo> resultFilesList)
        {
                System.IO.FileInfo[] files = null;
                System.IO.DirectoryInfo[] subDirs = null;

                // First, process all the files directly under this folder
                files = root.GetFiles("*.txt");

                if (files != null)
                {
                    foreach (System.IO.FileInfo fi in files)
                        resultFilesList.Add(fi);

                    // Now find all the subdirectories under this directory.
                    subDirs = root.GetDirectories();

                    foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                        // Resursive call for each subdirectory.
                        GetDirectoryTree(dirInfo, resultFilesList);
                }
            

            }
    }
}
