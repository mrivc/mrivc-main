﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace EllisBillFilter.DS
{
    using MrivcHelpers.Helpers;
    using System.Data.SqlClient;

    public class DAccess
    {
        private static DAccess instance;
        private static DModule dModule;

        private DAccess() {}

        //private ApplicationUser currentUser;
        //public ApplicationUser CurrentUser
        //{
        //    get
        //    {
        //        return currentUser;
        //    }
        //}

        //public void SetCurrentUser(ApplicationUser user)
        //{
        //    currentUser = user;
        //}

        public static DAccess Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DAccess();
                }
                return instance;
            }
        }

        public static DModule DataModule
        {
            get
            {
                if (dModule == null)
                {
                    dModule = new DModule();
                }
                return dModule;
            }
        }

        int workPeriod = -1;
        public int WorkPeriod
        {
            get
            {
                if (workPeriod == -1)
                    workPeriod = DataModule.GetWorkPeriod();
                return workPeriod;
            }
        }

        int userID = -1;
        public int UserId
        {
            get
            {
                if (userID == -1)
                    userID = DataModule.GetCurrentUserId();
                return userID;
            }
        }

        MDataSet.PeriodDataTable periods;
        public MDataSet.PeriodDataTable Periods
        {
            get
            {
                if (periods == null)
                {
                    periods = new MDataSet.PeriodDataTable();
                    DAccess.DataModule.PeriodSelect(periods);
                }
                return periods;
            }
        }

        MDataSet.FilterTypeDataTable filterType;
        public MDataSet.FilterTypeDataTable FilterType
        {
            get
            {
                if (filterType == null)
                {
                    filterType = new MDataSet.FilterTypeDataTable();
                    DAccess.DataModule.FilterTypeSelect(filterType);
                }
                return filterType;
            }
        }

        MDataSet.BillFilterDataTable billFilter;
        public MDataSet.BillFilterDataTable BillFilter
        {
            get
            {
                if (billFilter == null)
                {
                    billFilter = new MDataSet.BillFilterDataTable();
                    DAccess.DataModule.BillFilterSelect(billFilter);
                }
                return billFilter;
            }
        }

        MDataSet.PayReceiverCompanyDataTable payReceiverCompany;
        public MDataSet.PayReceiverCompanyDataTable PayReceiverCompany
        {
            get
            {
                if (payReceiverCompany == null)
                {
                    payReceiverCompany = new MDataSet.PayReceiverCompanyDataTable();
                }

                payReceiverCompany.Clear();
                DAccess.DataModule.PayReceiverSelect(payReceiverCompany);
                return payReceiverCompany;
            }
            set
            {
                if (payReceiverCompany != null)
                    payReceiverCompany.Clear();
                payReceiverCompany = value;
            }
        }

        MDataSet.DeriveCompanyDataTable deriveCompany;
        public MDataSet.DeriveCompanyDataTable DeriveCompany
        {
            get
            {
                if (deriveCompany == null)
                {
                    deriveCompany = new MDataSet.DeriveCompanyDataTable();
                    DAccess.DataModule.DeriveCompanySelect(deriveCompany);
                }
                return deriveCompany;
            }
        }


        MDataSet.Agreement_TypeDataTable agreementType;
        public MDataSet.Agreement_TypeDataTable AgreementType
        {
            get
            {
                if (agreementType == null)
                {
                    agreementType = new MDataSet.Agreement_TypeDataTable();
                    DAccess.DataModule.AgreementTypeSelect(agreementType);
                }
                return agreementType;
            }
        }

        MDataSet.ProviderGroupFilterDataTable providerGroupFilter;
        public MDataSet.ProviderGroupFilterDataTable ProviderGroupFilter
        {
            get
            {
                if (providerGroupFilter == null)
                {
                    providerGroupFilter = new MDataSet.ProviderGroupFilterDataTable();
                    DAccess.DataModule.ProviderGroupFilterSelect(providerGroupFilter);
                }
                return providerGroupFilter;
            }
        }

        MDataSet.ProviderGroupDataTable providerGroup;
        public MDataSet.ProviderGroupDataTable ProviderGroup
        {
            get
            {
                if (providerGroup == null)
                {
                    providerGroup = new MDataSet.ProviderGroupDataTable();
                    DAccess.DataModule.ProviderGroupSelect(providerGroup);
                }
                return providerGroup;
            }
        }

        public long GenerateIdPrint()
        {
            string idPrint = WorkPeriod.ToString() + StringHelper.ZeroCode(DateTime.Now.Month) + StringHelper.ZeroCode(DateTime.Now.Day) +
                                StringHelper.ZeroCode(DateTime.Now.Hour) + StringHelper.ZeroCode(DateTime.Now.Minute) + StringHelper.ZeroCode(DateTime.Now.Millisecond);
            return long.Parse(idPrint);
        }

        public string GetEllisReport(string moduleName)
        {
            byte[] module = (byte[])DAccess.DataModule.LoadEllisReportTemplate(moduleName);
            return Encoding.UTF8.GetString(module);
        }

        public string GetBillStdReport()
        {
            byte[] module = (byte[])DAccess.DataModule.LoadBillStdTemplate();
            return Encoding.UTF8.GetString(module);
        }

        public string GetDeriveCompanyName(int idDeriveCompany)
        {
            DataRow[] rows = DeriveCompany.Select("[ID_Company] = " + idDeriveCompany.ToString());
            if (rows.Length > 0)
                return rows[0]["Name"].ToString();

            return "NN";
        }

        public string GetCompanyName(int idCompany)
        {
            DataRow[] rows = PayReceiverCompany.Select("[ID_Company] = " + idCompany.ToString());
            if (rows.Length > 0)
                return rows[0]["Name"].ToString();

            return "NN";
        }

        public string GetBillFilterName(int idBillFilter)
        {
            DataRow[] rows = BillFilter.Select("[ID_Bill_Filter] = " + idBillFilter.ToString());
            if (rows.Length > 0)
                return rows[0]["Bill_Filter"].ToString();

            return "NN";
        }

        public DataRow GetPeriodInfo(int period)
        {
            DataRow[] rows = Periods.Select("[Period] = " + period.ToString());
            return rows[0];
        }

        public DataRow GetProviderGroupFilter(int idProviderGroupFilter)
        {
            DataRow[] rows = ProviderGroupFilter.Select("[ID_Provider_Group_Filter] = " + idProviderGroupFilter.ToString());
            return rows[0];
        }

        ///// <summary>
        ///// Проверка, принимаем ли мы платежи на расчетный счет данного клиента
        ///// клиенты описаны в БД ЭЛЛИС Reports.Client_Detail
        ///// </summary>
        ///// <param name="idClient">Клиенты описаны в БД ЭЛЛИС Reports.Client_Detail</param>
        //public bool CheckClientFilter(int idClient)
        //{
        //    DataRow[] rows = FilterClient.Select("[ID_Company] = " + idClient.ToString());
        //    if (rows.Length > 0)
        //        return true;

        //    return false;
        //}
    }
}
