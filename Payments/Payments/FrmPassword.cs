﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Payments
{
    public partial class FrmPassword : Form
    {
        private PasswordType passwordType;

        public FrmPassword()
        {
            InitializeComponent();
        }

        public FrmPassword(PasswordType passwordType)
        {
            InitializeComponent();
            this.passwordType = passwordType;
            Init();
        }

        public FrmPassword(PasswordType passwordType, string formName)
        {
            InitializeComponent();
            this.passwordType = passwordType;
            Text = formName;
            Init();
        }

        private void Init()
        {
            switch (passwordType)
            {
                case PasswordType.SysAdmin:
                    lblPassword.Text = "Пароль Системного Администратора";
                    break;
                case PasswordType.User:
                    lblPassword.Text = "Пароль Кассира";
                    break;
            }
        }

        public string Password
        {
            get { return txtPassword.Text; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
