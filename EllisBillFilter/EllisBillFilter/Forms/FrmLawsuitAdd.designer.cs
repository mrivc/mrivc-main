﻿namespace EllisBillFilter.Forms
{
    partial class FrmLawsuitAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLawsuitAdd));
            this.dCompanyLabel = new System.Windows.Forms.Label();
            this.dCompanyComboBox = new System.Windows.Forms.ComboBox();
            this.dateDebtBeginLabel = new System.Windows.Forms.Label();
            this.dateDebtEndLabel = new System.Windows.Forms.Label();
            this.dateProceedingsBeginLabel = new System.Windows.Forms.Label();
            this.dateDebtBeginPicker = new System.Windows.Forms.DateTimePicker();
            this.dateDebtEndPicker = new System.Windows.Forms.DateTimePicker();
            this.dateProceedingsBeginPicker = new System.Windows.Forms.DateTimePicker();
            this.dateProceedingsEndPicker = new System.Windows.Forms.DateTimePicker();
            this.dateProceedingsEndLabel = new System.Windows.Forms.Label();
            this.courtComboBox = new System.Windows.Forms.ComboBox();
            this.courtLabel = new System.Windows.Forms.Label();
            this.summLabel = new System.Windows.Forms.Label();
            this.summNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.summFineNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.legalCostsNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.summFineLabel = new System.Windows.Forms.Label();
            this.legalCostsLabel = new System.Windows.Forms.Label();
            this.caseNumberLabel = new System.Windows.Forms.Label();
            this.caseNumberTextBox = new System.Windows.Forms.TextBox();
            this.accLabel = new System.Windows.Forms.Label();
            this.stadyComboBox = new System.Windows.Forms.ComboBox();
            this.stadyLabel = new System.Windows.Forms.Label();
            this.innerIndexComboBox = new System.Windows.Forms.ComboBox();
            this.innerIndexLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.accCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.shareLabel = new System.Windows.Forms.Label();
            this.calcSumButton = new System.Windows.Forms.Button();
            this.receivingOrderLabel = new System.Windows.Forms.Label();
            this.receivingOrderTextBox = new System.Windows.Forms.TextBox();
            this.providerLabel = new System.Windows.Forms.Label();
            this.dateDebtBeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this.dateDebtEndTextBox = new System.Windows.Forms.MaskedTextBox();
            this.providerCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.commLabel = new System.Windows.Forms.Label();
            this.commTextBox = new System.Windows.Forms.TextBox();
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.denominatorNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.slashLabel = new System.Windows.Forms.Label();
            this.numeratorNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.detailButton = new System.Windows.Forms.Button();
            this.calcLegalCostsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.summNumUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summFineNumUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.legalCostsNumUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.denominatorNumUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeratorNumUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // dCompanyLabel
            // 
            this.dCompanyLabel.AutoSize = true;
            this.dCompanyLabel.Location = new System.Drawing.Point(12, 174);
            this.dCompanyLabel.Name = "dCompanyLabel";
            this.dCompanyLabel.Size = new System.Drawing.Size(136, 13);
            this.dCompanyLabel.TabIndex = 2;
            this.dCompanyLabel.Text = "Управляющая компания:";
            // 
            // dCompanyComboBox
            // 
            this.dCompanyComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dCompanyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dCompanyComboBox.FormattingEnabled = true;
            this.dCompanyComboBox.Location = new System.Drawing.Point(169, 171);
            this.dCompanyComboBox.Name = "dCompanyComboBox";
            this.dCompanyComboBox.Size = new System.Drawing.Size(403, 21);
            this.dCompanyComboBox.TabIndex = 9;
            this.dCompanyComboBox.SelectedIndexChanged += new System.EventHandler(this.dCompanyComboBox_SelectedIndexChanged);
            // 
            // dateDebtBeginLabel
            // 
            this.dateDebtBeginLabel.AutoSize = true;
            this.dateDebtBeginLabel.Location = new System.Drawing.Point(12, 122);
            this.dateDebtBeginLabel.Name = "dateDebtBeginLabel";
            this.dateDebtBeginLabel.Size = new System.Drawing.Size(151, 13);
            this.dateDebtBeginLabel.TabIndex = 4;
            this.dateDebtBeginLabel.Text = "Начало периода взыскания:";
            // 
            // dateDebtEndLabel
            // 
            this.dateDebtEndLabel.AutoSize = true;
            this.dateDebtEndLabel.Location = new System.Drawing.Point(12, 148);
            this.dateDebtEndLabel.Name = "dateDebtEndLabel";
            this.dateDebtEndLabel.Size = new System.Drawing.Size(145, 13);
            this.dateDebtEndLabel.TabIndex = 5;
            this.dateDebtEndLabel.Text = "Конец периода взыскания:";
            // 
            // dateProceedingsBeginLabel
            // 
            this.dateProceedingsBeginLabel.AutoSize = true;
            this.dateProceedingsBeginLabel.Location = new System.Drawing.Point(12, 244);
            this.dateProceedingsBeginLabel.Name = "dateProceedingsBeginLabel";
            this.dateProceedingsBeginLabel.Size = new System.Drawing.Size(104, 13);
            this.dateProceedingsBeginLabel.TabIndex = 6;
            this.dateProceedingsBeginLabel.Text = "Дата принятия СП:";
            // 
            // dateDebtBeginPicker
            // 
            this.dateDebtBeginPicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateDebtBeginPicker.Location = new System.Drawing.Point(552, 119);
            this.dateDebtBeginPicker.Name = "dateDebtBeginPicker";
            this.dateDebtBeginPicker.Size = new System.Drawing.Size(20, 20);
            this.dateDebtBeginPicker.TabIndex = 6;
            this.dateDebtBeginPicker.ValueChanged += new System.EventHandler(this.dateDebtBeginPicker_ValueChanged);
            // 
            // dateDebtEndPicker
            // 
            this.dateDebtEndPicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateDebtEndPicker.Location = new System.Drawing.Point(552, 145);
            this.dateDebtEndPicker.Name = "dateDebtEndPicker";
            this.dateDebtEndPicker.Size = new System.Drawing.Size(20, 20);
            this.dateDebtEndPicker.TabIndex = 8;
            this.dateDebtEndPicker.ValueChanged += new System.EventHandler(this.dateDebtEndPicker_ValueChanged);
            // 
            // dateProceedingsBeginPicker
            // 
            this.dateProceedingsBeginPicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateProceedingsBeginPicker.Checked = false;
            this.dateProceedingsBeginPicker.Location = new System.Drawing.Point(169, 238);
            this.dateProceedingsBeginPicker.Name = "dateProceedingsBeginPicker";
            this.dateProceedingsBeginPicker.ShowCheckBox = true;
            this.dateProceedingsBeginPicker.Size = new System.Drawing.Size(403, 20);
            this.dateProceedingsBeginPicker.TabIndex = 11;
            // 
            // dateProceedingsEndPicker
            // 
            this.dateProceedingsEndPicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateProceedingsEndPicker.Checked = false;
            this.dateProceedingsEndPicker.Location = new System.Drawing.Point(169, 264);
            this.dateProceedingsEndPicker.Name = "dateProceedingsEndPicker";
            this.dateProceedingsEndPicker.ShowCheckBox = true;
            this.dateProceedingsEndPicker.Size = new System.Drawing.Size(403, 20);
            this.dateProceedingsEndPicker.TabIndex = 12;
            // 
            // dateProceedingsEndLabel
            // 
            this.dateProceedingsEndLabel.AutoSize = true;
            this.dateProceedingsEndLabel.Location = new System.Drawing.Point(12, 270);
            this.dateProceedingsEndLabel.Name = "dateProceedingsEndLabel";
            this.dateProceedingsEndLabel.Size = new System.Drawing.Size(96, 13);
            this.dateProceedingsEndLabel.TabIndex = 10;
            this.dateProceedingsEndLabel.Text = "Дата отмены СП:";
            // 
            // courtComboBox
            // 
            this.courtComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.courtComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.courtComboBox.FormattingEnabled = true;
            this.courtComboBox.Location = new System.Drawing.Point(169, 290);
            this.courtComboBox.MaxDropDownItems = 9;
            this.courtComboBox.Name = "courtComboBox";
            this.courtComboBox.Size = new System.Drawing.Size(403, 21);
            this.courtComboBox.TabIndex = 13;
            // 
            // courtLabel
            // 
            this.courtLabel.AutoSize = true;
            this.courtLabel.Location = new System.Drawing.Point(12, 293);
            this.courtLabel.Name = "courtLabel";
            this.courtLabel.Size = new System.Drawing.Size(28, 13);
            this.courtLabel.TabIndex = 12;
            this.courtLabel.Text = "Суд:";
            // 
            // summLabel
            // 
            this.summLabel.AutoSize = true;
            this.summLabel.Location = new System.Drawing.Point(12, 319);
            this.summLabel.Name = "summLabel";
            this.summLabel.Size = new System.Drawing.Size(71, 13);
            this.summLabel.TabIndex = 14;
            this.summLabel.Text = "Сумма иска:";
            // 
            // summNumUpDown
            // 
            this.summNumUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.summNumUpDown.DecimalPlaces = 2;
            this.summNumUpDown.Location = new System.Drawing.Point(169, 317);
            this.summNumUpDown.Name = "summNumUpDown";
            this.summNumUpDown.Size = new System.Drawing.Size(403, 20);
            this.summNumUpDown.TabIndex = 14;
            // 
            // summFineNumUpDown
            // 
            this.summFineNumUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.summFineNumUpDown.DecimalPlaces = 2;
            this.summFineNumUpDown.Location = new System.Drawing.Point(169, 369);
            this.summFineNumUpDown.Name = "summFineNumUpDown";
            this.summFineNumUpDown.Size = new System.Drawing.Size(403, 20);
            this.summFineNumUpDown.TabIndex = 19;
            // 
            // legalCostsNumUpDown
            // 
            this.legalCostsNumUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.legalCostsNumUpDown.DecimalPlaces = 2;
            this.legalCostsNumUpDown.Location = new System.Drawing.Point(169, 395);
            this.legalCostsNumUpDown.Name = "legalCostsNumUpDown";
            this.legalCostsNumUpDown.Size = new System.Drawing.Size(403, 20);
            this.legalCostsNumUpDown.TabIndex = 20;
            // 
            // summFineLabel
            // 
            this.summFineLabel.AutoSize = true;
            this.summFineLabel.Location = new System.Drawing.Point(12, 371);
            this.summFineLabel.Name = "summFineLabel";
            this.summFineLabel.Size = new System.Drawing.Size(71, 13);
            this.summFineLabel.TabIndex = 19;
            this.summFineLabel.Text = "Сумма пени:";
            // 
            // legalCostsLabel
            // 
            this.legalCostsLabel.AutoSize = true;
            this.legalCostsLabel.Location = new System.Drawing.Point(12, 397);
            this.legalCostsLabel.Name = "legalCostsLabel";
            this.legalCostsLabel.Size = new System.Drawing.Size(78, 13);
            this.legalCostsLabel.TabIndex = 20;
            this.legalCostsLabel.Text = "Гос. пошлина:";
            // 
            // caseNumberLabel
            // 
            this.caseNumberLabel.AutoSize = true;
            this.caseNumberLabel.Location = new System.Drawing.Point(12, 15);
            this.caseNumberLabel.Name = "caseNumberLabel";
            this.caseNumberLabel.Size = new System.Drawing.Size(71, 13);
            this.caseNumberLabel.TabIndex = 22;
            this.caseNumberLabel.Text = "Номер дела:";
            // 
            // caseNumberTextBox
            // 
            this.caseNumberTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.caseNumberTextBox.Location = new System.Drawing.Point(169, 12);
            this.caseNumberTextBox.Name = "caseNumberTextBox";
            this.caseNumberTextBox.Size = new System.Drawing.Size(403, 20);
            this.caseNumberTextBox.TabIndex = 1;
            // 
            // accLabel
            // 
            this.accLabel.AutoSize = true;
            this.accLabel.Location = new System.Drawing.Point(12, 67);
            this.accLabel.Name = "accLabel";
            this.accLabel.Size = new System.Drawing.Size(128, 39);
            this.accLabel.TabIndex = 24;
            this.accLabel.Text = "ФИО:\r\n\r\n*Редактирование - ПКМ";
            // 
            // stadyComboBox
            // 
            this.stadyComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stadyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stadyComboBox.FormattingEnabled = true;
            this.stadyComboBox.Location = new System.Drawing.Point(169, 421);
            this.stadyComboBox.Name = "stadyComboBox";
            this.stadyComboBox.Size = new System.Drawing.Size(403, 21);
            this.stadyComboBox.TabIndex = 21;
            // 
            // stadyLabel
            // 
            this.stadyLabel.AutoSize = true;
            this.stadyLabel.Location = new System.Drawing.Point(12, 424);
            this.stadyLabel.Name = "stadyLabel";
            this.stadyLabel.Size = new System.Drawing.Size(122, 13);
            this.stadyLabel.TabIndex = 26;
            this.stadyLabel.Text = "Стадия рассмотрения:";
            // 
            // innerIndexComboBox
            // 
            this.innerIndexComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.innerIndexComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.innerIndexComboBox.FormattingEnabled = true;
            this.innerIndexComboBox.Location = new System.Drawing.Point(169, 448);
            this.innerIndexComboBox.Name = "innerIndexComboBox";
            this.innerIndexComboBox.Size = new System.Drawing.Size(403, 21);
            this.innerIndexComboBox.TabIndex = 22;
            // 
            // innerIndexLabel
            // 
            this.innerIndexLabel.AutoSize = true;
            this.innerIndexLabel.Location = new System.Drawing.Point(12, 451);
            this.innerIndexLabel.Name = "innerIndexLabel";
            this.innerIndexLabel.Size = new System.Drawing.Size(44, 13);
            this.innerIndexLabel.TabIndex = 28;
            this.innerIndexLabel.Text = "Статус:";
            // 
            // addButton
            // 
            this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addButton.Location = new System.Drawing.Point(497, 584);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 35);
            this.addButton.TabIndex = 24;
            this.addButton.Text = "Добавить";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // accCheckedListBox
            // 
            this.accCheckedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.accCheckedListBox.FormattingEnabled = true;
            this.accCheckedListBox.Location = new System.Drawing.Point(169, 64);
            this.accCheckedListBox.Name = "accCheckedListBox";
            this.accCheckedListBox.Size = new System.Drawing.Size(403, 49);
            this.accCheckedListBox.TabIndex = 3;
            // 
            // shareLabel
            // 
            this.shareLabel.AutoSize = true;
            this.shareLabel.Location = new System.Drawing.Point(166, 345);
            this.shareLabel.Name = "shareLabel";
            this.shareLabel.Size = new System.Drawing.Size(37, 13);
            this.shareLabel.TabIndex = 62;
            this.shareLabel.Text = "Доля:";
            // 
            // calcSumButton
            // 
            this.calcSumButton.Location = new System.Drawing.Point(293, 343);
            this.calcSumButton.Name = "calcSumButton";
            this.calcSumButton.Size = new System.Drawing.Size(75, 20);
            this.calcSumButton.TabIndex = 17;
            this.calcSumButton.Text = "Рассчитать";
            this.calcSumButton.UseVisualStyleBackColor = true;
            this.calcSumButton.Click += new System.EventHandler(this.calcSumButton_Click);
            // 
            // receivingOrderLabel
            // 
            this.receivingOrderLabel.AutoSize = true;
            this.receivingOrderLabel.Location = new System.Drawing.Point(12, 41);
            this.receivingOrderLabel.Name = "receivingOrderLabel";
            this.receivingOrderLabel.Size = new System.Drawing.Size(123, 13);
            this.receivingOrderLabel.TabIndex = 67;
            this.receivingOrderLabel.Text = "Исполнительный лист:";
            // 
            // receivingOrderTextBox
            // 
            this.receivingOrderTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.receivingOrderTextBox.Location = new System.Drawing.Point(169, 38);
            this.receivingOrderTextBox.Name = "receivingOrderTextBox";
            this.receivingOrderTextBox.Size = new System.Drawing.Size(403, 20);
            this.receivingOrderTextBox.TabIndex = 2;
            // 
            // providerLabel
            // 
            this.providerLabel.AutoSize = true;
            this.providerLabel.Location = new System.Drawing.Point(12, 201);
            this.providerLabel.Name = "providerLabel";
            this.providerLabel.Size = new System.Drawing.Size(68, 13);
            this.providerLabel.TabIndex = 69;
            this.providerLabel.Text = "Поставщик:";
            // 
            // dateDebtBeginTextBox
            // 
            this.dateDebtBeginTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateDebtBeginTextBox.Location = new System.Drawing.Point(169, 119);
            this.dateDebtBeginTextBox.Mask = "00/00/0000";
            this.dateDebtBeginTextBox.Name = "dateDebtBeginTextBox";
            this.dateDebtBeginTextBox.Size = new System.Drawing.Size(377, 20);
            this.dateDebtBeginTextBox.TabIndex = 5;
            this.dateDebtBeginTextBox.ValidatingType = typeof(System.DateTime);
            this.dateDebtBeginTextBox.TextChanged += new System.EventHandler(this.dateDebtBeginTextBox_TextChanged);
            // 
            // dateDebtEndTextBox
            // 
            this.dateDebtEndTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateDebtEndTextBox.Location = new System.Drawing.Point(169, 145);
            this.dateDebtEndTextBox.Mask = "00/00/0000";
            this.dateDebtEndTextBox.Name = "dateDebtEndTextBox";
            this.dateDebtEndTextBox.Size = new System.Drawing.Size(377, 20);
            this.dateDebtEndTextBox.TabIndex = 7;
            this.dateDebtEndTextBox.ValidatingType = typeof(System.DateTime);
            this.dateDebtEndTextBox.TextChanged += new System.EventHandler(this.dateDebtEndTextBox_TextChanged);
            // 
            // providerCheckedListBox
            // 
            this.providerCheckedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.providerCheckedListBox.CheckOnClick = true;
            this.providerCheckedListBox.FormattingEnabled = true;
            this.providerCheckedListBox.Location = new System.Drawing.Point(169, 198);
            this.providerCheckedListBox.Name = "providerCheckedListBox";
            this.providerCheckedListBox.Size = new System.Drawing.Size(403, 34);
            this.providerCheckedListBox.TabIndex = 10;
            // 
            // commLabel
            // 
            this.commLabel.AutoSize = true;
            this.commLabel.Location = new System.Drawing.Point(12, 478);
            this.commLabel.Name = "commLabel";
            this.commLabel.Size = new System.Drawing.Size(80, 13);
            this.commLabel.TabIndex = 75;
            this.commLabel.Text = "Комментарий:";
            // 
            // commTextBox
            // 
            this.commTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commTextBox.Location = new System.Drawing.Point(12, 494);
            this.commTextBox.Multiline = true;
            this.commTextBox.Name = "commTextBox";
            this.commTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.commTextBox.Size = new System.Drawing.Size(560, 80);
            this.commTextBox.TabIndex = 23;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // denominatorNumUpDown
            // 
            this.denominatorNumUpDown.Location = new System.Drawing.Point(257, 343);
            this.denominatorNumUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.denominatorNumUpDown.Name = "denominatorNumUpDown";
            this.denominatorNumUpDown.Size = new System.Drawing.Size(30, 20);
            this.denominatorNumUpDown.TabIndex = 16;
            this.denominatorNumUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // slashLabel
            // 
            this.slashLabel.AutoSize = true;
            this.slashLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.slashLabel.Location = new System.Drawing.Point(242, 343);
            this.slashLabel.Margin = new System.Windows.Forms.Padding(0);
            this.slashLabel.Name = "slashLabel";
            this.slashLabel.Size = new System.Drawing.Size(12, 16);
            this.slashLabel.TabIndex = 112;
            this.slashLabel.Text = "/";
            // 
            // numeratorNumUpDown
            // 
            this.numeratorNumUpDown.Location = new System.Drawing.Point(209, 343);
            this.numeratorNumUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeratorNumUpDown.Name = "numeratorNumUpDown";
            this.numeratorNumUpDown.Size = new System.Drawing.Size(30, 20);
            this.numeratorNumUpDown.TabIndex = 15;
            this.numeratorNumUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // detailButton
            // 
            this.detailButton.Location = new System.Drawing.Point(455, 343);
            this.detailButton.Name = "detailButton";
            this.detailButton.Size = new System.Drawing.Size(85, 20);
            this.detailButton.TabIndex = 18;
            this.detailButton.Text = "Детализация";
            this.detailButton.UseVisualStyleBackColor = true;
            this.detailButton.Click += new System.EventHandler(this.detailButton_Click);
            // 
            // calcLegalCostsButton
            // 
            this.calcLegalCostsButton.Location = new System.Drawing.Point(374, 343);
            this.calcLegalCostsButton.Name = "calcLegalCostsButton";
            this.calcLegalCostsButton.Size = new System.Drawing.Size(75, 20);
            this.calcLegalCostsButton.TabIndex = 117;
            this.calcLegalCostsButton.Text = "Г. Пошлина";
            this.calcLegalCostsButton.UseVisualStyleBackColor = true;
            this.calcLegalCostsButton.Click += new System.EventHandler(this.calcLegalCostsButton_Click);
            // 
            // FrmLawsuitAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 631);
            this.Controls.Add(this.calcLegalCostsButton);
            this.Controls.Add(this.detailButton);
            this.Controls.Add(this.denominatorNumUpDown);
            this.Controls.Add(this.slashLabel);
            this.Controls.Add(this.numeratorNumUpDown);
            this.Controls.Add(this.commTextBox);
            this.Controls.Add(this.commLabel);
            this.Controls.Add(this.providerCheckedListBox);
            this.Controls.Add(this.dateDebtEndTextBox);
            this.Controls.Add(this.dateDebtBeginTextBox);
            this.Controls.Add(this.providerLabel);
            this.Controls.Add(this.receivingOrderLabel);
            this.Controls.Add(this.receivingOrderTextBox);
            this.Controls.Add(this.shareLabel);
            this.Controls.Add(this.calcSumButton);
            this.Controls.Add(this.accCheckedListBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.innerIndexComboBox);
            this.Controls.Add(this.innerIndexLabel);
            this.Controls.Add(this.stadyComboBox);
            this.Controls.Add(this.stadyLabel);
            this.Controls.Add(this.accLabel);
            this.Controls.Add(this.caseNumberLabel);
            this.Controls.Add(this.caseNumberTextBox);
            this.Controls.Add(this.legalCostsLabel);
            this.Controls.Add(this.summFineLabel);
            this.Controls.Add(this.legalCostsNumUpDown);
            this.Controls.Add(this.summFineNumUpDown);
            this.Controls.Add(this.summNumUpDown);
            this.Controls.Add(this.summLabel);
            this.Controls.Add(this.courtComboBox);
            this.Controls.Add(this.courtLabel);
            this.Controls.Add(this.dateProceedingsEndPicker);
            this.Controls.Add(this.dateProceedingsEndLabel);
            this.Controls.Add(this.dateProceedingsBeginPicker);
            this.Controls.Add(this.dateDebtEndPicker);
            this.Controls.Add(this.dateDebtBeginPicker);
            this.Controls.Add(this.dateProceedingsBeginLabel);
            this.Controls.Add(this.dateDebtEndLabel);
            this.Controls.Add(this.dateDebtBeginLabel);
            this.Controls.Add(this.dCompanyComboBox);
            this.Controls.Add(this.dCompanyLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(490, 625);
            this.Name = "FrmLawsuitAdd";
            this.Text = "Добавить иск";
            ((System.ComponentModel.ISupportInitialize)(this.summNumUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summFineNumUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.legalCostsNumUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.denominatorNumUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeratorNumUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label dCompanyLabel;
        private System.Windows.Forms.ComboBox dCompanyComboBox;
        private System.Windows.Forms.Label dateDebtBeginLabel;
        private System.Windows.Forms.Label dateDebtEndLabel;
        private System.Windows.Forms.Label dateProceedingsBeginLabel;
        private System.Windows.Forms.DateTimePicker dateDebtBeginPicker;
        private System.Windows.Forms.DateTimePicker dateDebtEndPicker;
        private System.Windows.Forms.DateTimePicker dateProceedingsBeginPicker;
        private System.Windows.Forms.DateTimePicker dateProceedingsEndPicker;
        private System.Windows.Forms.Label dateProceedingsEndLabel;
        private System.Windows.Forms.ComboBox courtComboBox;
        private System.Windows.Forms.Label courtLabel;
        private System.Windows.Forms.Label summLabel;
        private System.Windows.Forms.NumericUpDown summNumUpDown;
        private System.Windows.Forms.NumericUpDown summFineNumUpDown;
        private System.Windows.Forms.NumericUpDown legalCostsNumUpDown;
        private System.Windows.Forms.Label summFineLabel;
        private System.Windows.Forms.Label legalCostsLabel;
        private System.Windows.Forms.Label caseNumberLabel;
        private System.Windows.Forms.TextBox caseNumberTextBox;
        private System.Windows.Forms.Label accLabel;
        private System.Windows.Forms.ComboBox stadyComboBox;
        private System.Windows.Forms.Label stadyLabel;
        private System.Windows.Forms.ComboBox innerIndexComboBox;
        private System.Windows.Forms.Label innerIndexLabel;
        private System.Windows.Forms.Button addButton;
        private DS.MDataSet mDataSet;
        private System.Windows.Forms.CheckedListBox accCheckedListBox;
        private System.Windows.Forms.Label shareLabel;
        private System.Windows.Forms.Button calcSumButton;
        private System.Windows.Forms.Label receivingOrderLabel;
        private System.Windows.Forms.TextBox receivingOrderTextBox;
        private System.Windows.Forms.Label providerLabel;
        private System.Windows.Forms.MaskedTextBox dateDebtBeginTextBox;
        private System.Windows.Forms.MaskedTextBox dateDebtEndTextBox;
        private System.Windows.Forms.CheckedListBox providerCheckedListBox;
        private System.Windows.Forms.Label commLabel;
        private System.Windows.Forms.TextBox commTextBox;
        private System.Windows.Forms.NumericUpDown denominatorNumUpDown;
        private System.Windows.Forms.Label slashLabel;
        private System.Windows.Forms.NumericUpDown numeratorNumUpDown;
        private System.Windows.Forms.Button detailButton;
        private System.Windows.Forms.Button calcLegalCostsButton;
    }
}