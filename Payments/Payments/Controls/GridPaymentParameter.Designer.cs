﻿namespace Payments.Controls
{
    partial class GridPaymentParameter
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.paymentsDataSet = new Payments.PaymentsDataSet();
            this.paymentParameterDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.paymentParameterDetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gridPaymentParameterDetail = new DevExpress.XtraGrid.GridControl();
            this.paymentParameterDetailBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewPaymentParameterDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Payment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplay_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Parameter_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefault_Value = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefault_Quantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory_Type_Rank = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParameter_Rank = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentParameterDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentParameterDetailBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPaymentParameterDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentParameterDetailBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaymentParameterDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // paymentsDataSet
            // 
            this.paymentsDataSet.DataSetName = "PaymentsDataSet";
            this.paymentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // paymentParameterDetailBindingSource
            // 
            this.paymentParameterDetailBindingSource.DataMember = "PaymentParameterDetail";
            this.paymentParameterDetailBindingSource.DataSource = this.paymentsDataSet;
            // 
            // paymentParameterDetailBindingSource1
            // 
            this.paymentParameterDetailBindingSource1.DataMember = "PaymentParameterDetail";
            this.paymentParameterDetailBindingSource1.DataSource = this.paymentsDataSet;
            // 
            // gridPaymentParameterDetail
            // 
            this.gridPaymentParameterDetail.DataSource = this.paymentParameterDetailBindingSource2;
            this.gridPaymentParameterDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPaymentParameterDetail.Location = new System.Drawing.Point(0, 0);
            this.gridPaymentParameterDetail.MainView = this.gridViewPaymentParameterDetail;
            this.gridPaymentParameterDetail.Name = "gridPaymentParameterDetail";
            this.gridPaymentParameterDetail.Size = new System.Drawing.Size(429, 485);
            this.gridPaymentParameterDetail.TabIndex = 0;
            this.gridPaymentParameterDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPaymentParameterDetail});
            this.gridPaymentParameterDetail.Enter += new System.EventHandler(this.gridPaymentParameterDetail_Enter);
            // 
            // paymentParameterDetailBindingSource2
            // 
            this.paymentParameterDetailBindingSource2.DataMember = "PaymentParameterDetail";
            this.paymentParameterDetailBindingSource2.DataSource = this.paymentsDataSet;
            // 
            // gridViewPaymentParameterDetail
            // 
            this.gridViewPaymentParameterDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Payment,
            this.colCode,
            this.colDisplay_Name,
            this.colID_Parameter_Type,
            this.colDefault_Value,
            this.colDefault_Quantity,
            this.colCategory_Type,
            this.colCategory_Type_Rank,
            this.colParameter_Rank});
            this.gridViewPaymentParameterDetail.GridControl = this.gridPaymentParameterDetail;
            this.gridViewPaymentParameterDetail.GroupCount = 1;
            this.gridViewPaymentParameterDetail.Name = "gridViewPaymentParameterDetail";
            this.gridViewPaymentParameterDetail.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewPaymentParameterDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewPaymentParameterDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCategory_Type, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewPaymentParameterDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewPaymentParameterDetail_CellValueChanged);
            this.gridViewPaymentParameterDetail.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewPaymentParameterDetail_CellValueChanging);
            // 
            // colID_Payment
            // 
            this.colID_Payment.Caption = "ИД";
            this.colID_Payment.FieldName = "ID_Payment";
            this.colID_Payment.Name = "colID_Payment";
            this.colID_Payment.OptionsColumn.ReadOnly = true;
            // 
            // colCode
            // 
            this.colCode.Caption = "Код параметра";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            // 
            // colDisplay_Name
            // 
            this.colDisplay_Name.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colDisplay_Name.AppearanceCell.Options.UseBackColor = true;
            this.colDisplay_Name.Caption = "Наименование";
            this.colDisplay_Name.FieldName = "Display_Name";
            this.colDisplay_Name.MinWidth = 140;
            this.colDisplay_Name.Name = "colDisplay_Name";
            this.colDisplay_Name.OptionsColumn.AllowEdit = false;
            this.colDisplay_Name.OptionsColumn.AllowFocus = false;
            this.colDisplay_Name.OptionsColumn.ReadOnly = true;
            this.colDisplay_Name.Visible = true;
            this.colDisplay_Name.VisibleIndex = 0;
            this.colDisplay_Name.Width = 420;
            // 
            // colID_Parameter_Type
            // 
            this.colID_Parameter_Type.Caption = "Тип параметра";
            this.colID_Parameter_Type.FieldName = "ID_Parameter_Type";
            this.colID_Parameter_Type.Name = "colID_Parameter_Type";
            // 
            // colDefault_Value
            // 
            this.colDefault_Value.Caption = "Значение";
            this.colDefault_Value.FieldName = "Parameter_Value";
            this.colDefault_Value.MinWidth = 50;
            this.colDefault_Value.Name = "colDefault_Value";
            this.colDefault_Value.Visible = true;
            this.colDefault_Value.VisibleIndex = 1;
            this.colDefault_Value.Width = 391;
            // 
            // colDefault_Quantity
            // 
            this.colDefault_Quantity.Caption = "Количество";
            this.colDefault_Quantity.FieldName = "Parameter_Quantity";
            this.colDefault_Quantity.MinWidth = 35;
            this.colDefault_Quantity.Name = "colDefault_Quantity";
            this.colDefault_Quantity.Visible = true;
            this.colDefault_Quantity.VisibleIndex = 2;
            this.colDefault_Quantity.Width = 279;
            // 
            // colCategory_Type
            // 
            this.colCategory_Type.Caption = "Класс";
            this.colCategory_Type.FieldName = "Category_Type";
            this.colCategory_Type.Name = "colCategory_Type";
            // 
            // colCategory_Type_Rank
            // 
            this.colCategory_Type_Rank.Caption = "Раг типа сортировка";
            this.colCategory_Type_Rank.FieldName = "Category_Type_Rank";
            this.colCategory_Type_Rank.Name = "colCategory_Type_Rank";
            // 
            // colParameter_Rank
            // 
            this.colParameter_Rank.Caption = "Ранг параметра сортировки";
            this.colParameter_Rank.FieldName = "Parameter_Rank";
            this.colParameter_Rank.Name = "colParameter_Rank";
            // 
            // GridPaymentParameter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridPaymentParameterDetail);
            this.Name = "GridPaymentParameter";
            this.Size = new System.Drawing.Size(429, 485);
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentParameterDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentParameterDetailBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPaymentParameterDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentParameterDetailBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaymentParameterDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PaymentsDataSet paymentsDataSet;
        private System.Windows.Forms.BindingSource paymentParameterDetailBindingSource;
        private System.Windows.Forms.BindingSource paymentParameterDetailBindingSource1;
        private DevExpress.XtraGrid.GridControl gridPaymentParameterDetail;
        private System.Windows.Forms.BindingSource paymentParameterDetailBindingSource2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPaymentParameterDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Payment;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplay_Name;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Parameter_Type;
        private DevExpress.XtraGrid.Columns.GridColumn colDefault_Value;
        private DevExpress.XtraGrid.Columns.GridColumn colDefault_Quantity;
        private DevExpress.XtraGrid.Columns.GridColumn colCategory_Type;
        private DevExpress.XtraGrid.Columns.GridColumn colCategory_Type_Rank;
        private DevExpress.XtraGrid.Columns.GridColumn colParameter_Rank;
    }
}
