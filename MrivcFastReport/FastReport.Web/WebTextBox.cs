﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using FastReport.Dialog;

namespace FastReport.Web.Handlers
{
    public partial class WebExport : IHttpHandler
    {
        private void TextBoxChange(TextBoxControl tb, string data)
        {
            tb.Text = data;
            tb.FilterData();
            tb.OnTextChanged(null);
        }

        private string GetTextBoxHtml(TextBoxControl control, WebReport webReport)
        {
            string id = webReport.Prop.ControlID + control.Name;
            return string.Format("<input class=\"{0}\" style=\"{1}\" type=\"text\" name=\"{2}\" value=\"{3}\" onchange=\"{4}\" id=\"{5}\"/>",
                // class
                "",
                // style
                GetTextBoxStyle(control),
                // name
                control.Name,
                // value
                control.Text,
                // onclick
                GetEvent("onchange", control, webReport, string.Format("document.getElementById('{0}').value", id)),
                // title
                id
                );
        }

        private string GetTextBoxStyle(TextBoxControl control)
        {
            return string.Format("{0}{1}{2}", GetControlPosition(control), GetControlFont(control), GetControlAlign(control));
        }
    }
}
