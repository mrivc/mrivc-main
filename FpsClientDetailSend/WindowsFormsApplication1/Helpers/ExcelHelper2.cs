﻿#region Using directives 

using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using Microsoft.Office.Interop;
using Microsoft.Office.Interop.Excel;
using DataTable = System.Data.DataTable;

#endregion Using directives 

 

namespace WindowsFormsApplication1.Helpers
{
    public static class ExcelHelper2
    {
        // Export data to an Excel spreadsheet via ADO.NET 
        public static void ExportToExcel(DataTable dtSQL, string excelFilePath)
        {
            string strDownloadFileName = "";
            string strConn = "";

            //if (rblExtension.SelectedValue == "2003") 
            //{ 
            // Excel 97-2003 
            strDownloadFileName = excelFilePath;
            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strDownloadFileName +
                      ";Extended Properties='Excel 8.0;HDR=Yes'";
            //} 
            //else 
            //{ 
            //    // Excel 2007 
            //    strDownloadFileName = "~/DownloadFiles/" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx"; 
            //    strExcelConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Server.MapPath(strDownloadFileName) + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'"; 
            //} 

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                // Create a new sheet in the Excel spreadsheet. 
                OleDbCommand cmd = new OleDbCommand(StaticQuery.ExcelTable, conn);

                // Open the connection. 
                conn.Open();

                // Execute the OleDbCommand. 
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO ExcelTable ([Код клиента], [Наименование клиента], [Город], [Управляющая компания], [Код УК (1-4)], [Код вида ПД (5-6)], [Вид платежного документа]) values (?,?,?,?,?,?,?)";

                // Add the parameters. 
                cmd.Parameters.Add("[Код клиента]", OleDbType.VarChar, 150, "Client_Code");
                cmd.Parameters.Add("[Наименование клиента]", OleDbType.VarChar, 150, "Client_Name");
                cmd.Parameters.Add("[Город]", OleDbType.VarChar, 150, "Settlement");
                cmd.Parameters.Add("[Управляющая компания]", OleDbType.VarChar, 150, "Derive_Company_Name");
                cmd.Parameters.Add("[Код УК (1-4)]", OleDbType.VarChar, 150, "Derive_Company_Code");
                cmd.Parameters.Add("[Код вида ПД (5-6)]", OleDbType.VarChar, 150, "Bill_Filter_Code");
                cmd.Parameters.Add("[Вид платежного документа]", OleDbType.VarChar, 150, "Bill_Filter_Name");

                // Initialize an OleDBDataAdapter object. 
                OleDbDataAdapter da = new OleDbDataAdapter("select * from ExcelTable", conn);

                // Set the InsertCommand of OleDbDataAdapter,  
                // which is used to insert data. 
                da.InsertCommand = cmd;

                foreach (DataRow dr in dtSQL.Rows)
                {
                    dr.SetAdded();
                }

                // Insert the data into the Excel spreadsheet. 
                da.Update(dtSQL);
            }
        }

        public static void ExportToExcel2(DataTable dtSQL, string file)
        {
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Microsoft.Office.Interop.Excel.Range chartRange;

            xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            xlWorkSheet.Cells[1, 1] = "Код клиента";
            xlWorkSheet.Cells[1, 2] = "Наименование клиента";
            xlWorkSheet.Cells[1, 3] = "Город";
            xlWorkSheet.Cells[1, 4] = "Управляющая компания";
            xlWorkSheet.Cells[1, 5] = "Код УК (1-4)";
            xlWorkSheet.Cells[1, 6] = "Код вида ПД (5-6)";
            xlWorkSheet.Cells[1, 7] = "Вид платежного документа";

            for (int i = 0; i < dtSQL.Rows.Count; i++)
            {
                xlWorkSheet.Cells[i + 2, 1] = "'" + dtSQL.Rows[i]["Client_Code"].ToString();
                xlWorkSheet.Cells[i + 2, 2] = dtSQL.Rows[i]["Client_Name"].ToString();
                xlWorkSheet.Cells[i + 2, 3] = dtSQL.Rows[i]["Settlement"].ToString();
                xlWorkSheet.Cells[i + 2, 4] = dtSQL.Rows[i]["Derive_Company_Name"].ToString();
                xlWorkSheet.Cells[i + 2, 5] = "'" + dtSQL.Rows[i]["Derive_Company_Code"].ToString();
                xlWorkSheet.Cells[i + 2, 6] = "'" + dtSQL.Rows[i]["Bill_Filter_Code"].ToString();
                xlWorkSheet.Cells[i + 2, 7] = dtSQL.Rows[i]["Bill_Filter_Name"].ToString();
            }

            chartRange = xlWorkSheet.get_Range("a1", "g1");
            chartRange.Font.Bold = true;

            FileHelper.DeleteFileIfExist(file);

            xlWorkBook.SaveAs(file, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlApp);
            releaseObject(xlWorkBook);
            releaseObject(xlWorkSheet);
        }

        public static void FormatExcelFile(string file)
        {

            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Microsoft.Office.Interop.Excel.Range chartRange;

            xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(file);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            chartRange = xlWorkSheet.get_Range("a1", "g1");
            chartRange.Font.Bold = true;

            xlWorkBook.SaveAs(file, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlApp);
            releaseObject(xlWorkBook);
            releaseObject(xlWorkSheet);
        }

        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        } 


        public static DataTable ImportExcelData(string sheetName, string excelFilePath)
        {
            DataTable dt = new DataTable();
            string strDownloadFileName = "";
            string strConn = "";

            strDownloadFileName = excelFilePath;
            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strDownloadFileName +
                      ";Extended Properties='Excel 8.0;HDR=Yes'";

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                using (OleDbDataAdapter da = new OleDbDataAdapter())
                {
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                        new object[] {null, null, null, "TABLE"});
                    if (sheetName.Length == 0) sheetName = schemaTable.Rows[0]["TABLE_NAME"].ToString();

                    da.SelectCommand = new OleDbCommand("select * from [" + sheetName + "]", conn);
                    da.Fill(dt);
                }
            }
            return dt;
        }
    }
} 
