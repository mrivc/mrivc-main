﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using FastReport.Dialog;

namespace FastReport.Web.Handlers
{
    public partial class WebExport : IHttpHandler
    {
        private string GetLabelHtml(LabelControl control, WebReport webReport)
        {
            return string.Format("<div style=\"{0}\">{1}</div>",
                GetLabelStyle(control),
                control.Text
                );
        }

        private string GetLabelStyle(LabelControl control)
        {
            return string.Format("{0}{1}{2}", GetControlPosition(control), GetControlFont(control), GetControlAlign(control));
        }

    }
}
