﻿namespace DataBaseLibrary
{
    public static class Config
    {
        public static string CheckString(string str)
            => str == "" || str == null ? "NULL" : str;

        public static string ConvertNullStringInRealNull(string str)
            => str.ToLower() == "null" ? null : str;
    }
}
