﻿using CommandLine;
using CommandLine.Text;
using Helper.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using Xades.Implementations;
using GisGkh.Commands;
using GisGkh.Configurations.Options;
using GisGkh.Configurations.Sections;
using GisGkh.Infrastructure;
using Helper;
using System.Timers;
using System.Diagnostics;

namespace GisGkh
{
    public class MrivcCommand : ICloneable
    {
        protected MrivcXmlFormatter mrivcXmlFormatter = new MrivcXmlFormatter();
        List<Tuple<string, string>> _messageGuidSet = new List<Tuple<string,string>>();

        public bool HasErrors { get; set; }
        
        public string Method { get; set; }
        public string Service { get; set; }
        public string Description { get; set; }
        public virtual string SenderID { get; set; }
        public int GetStateCommandWaitCount { get; set; } = 1000;
        public float GetStateCommandWaitDelay { get; set; } = 1F;
        public bool AddToReport { get; set; } = false;

        public int IdAsyncSession { get; set; } = -1;

        public IEnumerable<Tuple<string[], string>> Values
        {
            set
            {
                mrivcXmlFormatter.Values = value;
            }
        }

        private MrivcCommand()
        {

        }

        public MrivcCommand(string method, string service, string senderID)
        {
            Method = method;
            Service = service;
            SenderID = senderID;

            LoadRoot();
        }

        public MrivcCommand(string method, string service)
        {
            Method = method;
            Service = service;

            LoadRoot();
        }

        void LoadRoot()
        {
            var rootPath = File.ReadAllText(MrivcStatic.GisConfig.Services[Service].Methods[Method].Template);
            mrivcXmlFormatter.LoadRoot(rootPath);
        }

        string ExecuteCommand(string[] args, string xmlBody, string senderID)
        {
            var gisConfig = (GisServiceConfiguration)ConfigurationManager.GetSection(Constants.GisServicesConfigSectionName);
            var xadesConfig = (SigningConfiguration)ConfigurationManager.GetSection(Constants.XadesConfigSectionName);
            var xadesService = new GostXadesBesService();
            var s = "";
            foreach (var item in args)
            {
                s += item;
            }

            SentenceBuilder.UseSentenceBuilder(new RussianSentenceBuilder());

            ICommand command = Parser.Default.ParseArguments<SignOptions, VerifyOptions, CertificateOptions, GetStateOptions, SendOptions>(args)
                    .MapResult(
                        (SignOptions o) => new SignCommand(o, xadesService, xadesConfig),
                        (VerifyOptions o) => new VerifyCommand(o, xadesService, xadesConfig),
                        (SendOptions o) => new SendCommand(o, xadesService, xadesConfig, gisConfig),
                        (GetStateOptions o) => new GetStateCommand(o, xadesService, xadesConfig, gisConfig),
                        (CertificateOptions o) => new CertificateCommand(o),
                        errors => (ICommand)null);

            if (command != null)
            {
                return command.Execute(xmlBody, senderID);
            }
            else
            {
                return "";
            }
        }

        string ExecuteGetStateCommand(string messageGuid)
        {
            var getStateParameterSet = new GetStateParameterSet() { MessageGUID = messageGuid };

            //if (!Static.RunWithTimeout(DoGetStateCommandIteration, TimeSpan.FromMilliseconds(300), getStateParameterSet))
            //{
            //    throw new MrivcCommandException("MRIVC_TIMEOUT", $"МРИВЦ: Превышено время ожидания обработки команды", SenderID, Service, Method);
            //}
            //else
            //{
            //    return getStateParameterSet.Response;
            //}

            DoGetStateCommandIteration(getStateParameterSet);

            return getStateParameterSet.Response;
        }

        void DoGetStateCommandIteration(object getStateParameterSet)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var messageGuid = ((GetStateParameterSet)getStateParameterSet).MessageGUID;

            var getStateArgs = MrivcStatic.PrepareGetStateCommandArgs(Service, Method, messageGuid);
            var getStateXmlBody = MrivcStatic.PrepareXmlBody(Service, "getState", new[] { new Tuple<string[], string>(new[] { "MessageGUID" }, messageGuid) });

            //var n = 2;// GetStateCommandWaitCount;
            var t = GetStateCommandWaitDelay;
            //var i = 0;
            var status = 1;

            //if (MrivcStatic.GetStateWaitMin == -1)
            //{
            //    Console.WriteLine("Получение ответа команды get state до статуса \"обработан\". Для отмены нажмите \"s\".");
            //}

            while (status == 1 || status == 2 )  //Если статус "в обработке", то выполнить get-stae снова n раз пока не будет полчучен статус "обработан"
            {
                TimeSpan elapsed = stopWatch.Elapsed;

                if (elapsed.TotalMinutes > MrivcStatic.GetStateWaitMin && MrivcStatic.GetStateWaitMin != -1 )
                {
                    stopWatch.Stop();
                    throw new MrivcCommandException("MRIVC_TIMEOUT", $"МРИВЦ: Превышено время ожидания обработки команды", SenderID, Service, Method);
                }

                ((GetStateParameterSet)getStateParameterSet).Response = ExecuteCommand(getStateArgs, getStateXmlBody, SenderID);

                XmlNode res = null;

                //try
                //{
                //res = MrivcStatic.SelectNodeByName("//ns4:RequestState", ((GetStateParameterSet)getStateParameterSet).Response);
                res = MrivcStatic.SelectNodeByName(MrivcStatic.ResultNodeName("RequestState"), ((GetStateParameterSet)getStateParameterSet).Response);
                //}
                //catch (Exception ex)
                //{
                //    try
                //    {
                //        res = MrivcStatic.SelectNodeByName("//ns5:RequestState", ((GetStateParameterSet)getStateParameterSet).Response);
                //    }
                //    catch (Exception ex1)
                //    {
                //        res = MrivcStatic.SelectNodeByName("//base:RequestState", ((GetStateParameterSet)getStateParameterSet).Response);
                //    }

                //}


                if (res == null)
                {
                    return;
                }

                status = int.Parse(res.InnerText);

                if (status == 1 || status == 2)
                {
                    var sleepTimeMs = (int)(t * 1000);
                    Thread.Sleep(sleepTimeMs);
                }

                //i++;
                //if (i == n)
                //{
                //    throw new MrivcCommandException("MRIVC_TIMEOUT", $"МРИВЦ: Превышено время ожидания обработки команды", SenderID, Service, Method);
                //}
            }
        }

        protected virtual XmlNode AddChildElement(string childElementTemplatePath, IEnumerable<Tuple<string[],string>> values, string elementToAdd)
        {
            var child = File.ReadAllText(childElementTemplatePath);
            return mrivcXmlFormatter.AddChildElement(child,  new[] { elementToAdd } , values);
        }

        protected virtual XmlNode AddChildElement(string childElementTemplatePath, string elementToAdd)
        {
            var child = File.ReadAllText(childElementTemplatePath);
            return mrivcXmlFormatter.AddChildElement(child, new[] { elementToAdd });
        }

        protected virtual XmlNode AddChildElement(string childElementTemplatePath, XmlNode xmlNodeToAdd)
        {
            var child = File.ReadAllText(childElementTemplatePath);
            return mrivcXmlFormatter.AddChildElement(child, xmlNodeToAdd);
        }

        protected XmlNode AddChildElement(string childElementTemplatePath, IEnumerable<Tuple<string[], string>> values, XmlNode xmlNodeToAdd)
        {
            var child = File.ReadAllText(childElementTemplatePath);
            return mrivcXmlFormatter.AddChildElement(child, xmlNodeToAdd, values);
        }

        protected void RemoveElement(string[] elementName, XmlNode xmlNode)
        {
            mrivcXmlFormatter.RemoveNodesByName(elementName, xmlNode);
        }

        public enum AsyncCommandType
        {
            Send=2, GetState=3, All=-1
        }

        public virtual string Execute()
        {
            string result = null;

            if (CommandType == AsyncCommandType.All || CommandType == AsyncCommandType.Send)
            {
                result = ExecuteBase("","");
            }

            if (CommandType == AsyncCommandType.GetState)
            {
                Tuple<string,string>[] messageGuidSet = new Tuple<string, string>[_messageGuidSet.Count];

                _messageGuidSet.CopyTo(messageGuidSet);

                _messageGuidSet.Clear();

                foreach (Tuple<string, string> messageGuidItem in messageGuidSet)
                {
                    result = ExecuteBase(messageGuidItem.Item1,messageGuidItem.Item2);
                }
            }

            return result;
        }

        //public string ExecuteSendCommandAsync()
        //{
        //    return ExecuteBase();
        //}

        //public string ExecuteGetStateCommandAsync()
        //{
        //    return ExecuteBase();
        //}

        public AsyncCommandType CommandType { get; set; } = AsyncCommandType.All;

        public virtual string ExecuteBase(string messageGuid, string soapRequest)
        {
            try
            {

                //var soapRequest = "";
                string result = null;

                if (CommandType == AsyncCommandType.All || CommandType == AsyncCommandType.Send)
                {
                    //var messageGuid = "";

                    HasErrors = false;

                    var xmlBody = mrivcXmlFormatter.XmlBody;
                    xmlBody = MrivcStatic.ReplaceTransportGUID(xmlBody);

                    xmlBody = MrivcStatic.IndentXml(xmlBody);

                    var sendArgs = MrivcStatic.PrepareSendCommandArgs(Service, Method);
                    //MrivcStatic.AddToOutput("-------------Send result---------------");
                    MrivcStatic.CurrentSoapRequest = "";
                    var sendResponse = ExecuteCommand(sendArgs, xmlBody, SenderID);

                    messageGuid = MrivcStatic.GetMessageGuidBySendResponse(sendResponse);

                    soapRequest = MrivcStatic.CurrentSoapRequest;

                    ErrorCheck(sendResponse, soapRequest, messageGuid);

                    if (this is ISingleHouseCommand)
                        MrivcStatic.WriteLogWithMessageGuid(this, ((ISingleHouseCommand)this).FiasHouseGuid, messageGuid);
                    else
                        MrivcStatic.WriteLogWithMessageGuid(this, messageGuid);

                    result = MrivcStatic.IndentXml(sendResponse);

                    _messageGuidSet.Add(new Tuple<string,string>(messageGuid, soapRequest));
                }

                if (CommandType == AsyncCommandType.All || CommandType == AsyncCommandType.GetState)
                {

                    if (messageGuid == "")
                    {
                        var ex = new MrivcCommandException("MRIVC_MGUID_MISSING", "МРИВЦ: Не удалось выполнить команду get-state: MessageGUID отсутствует", SenderID, Service, Method);
                        ex.SendRequest = soapRequest;
                        throw ex;
                    }

                    var getStateResponse = ExecuteGetStateCommand(messageGuid);



                    ErrorCheck(getStateResponse, soapRequest, messageGuid);

                    if (this is ISingleHouseCommand)
                        MrivcStatic.WriteLogWithMessageGuid(this, ((ISingleHouseCommand)this).FiasHouseGuid, messageGuid);
                    else
                        MrivcStatic.WriteLogWithMessageGuid(this, messageGuid);

                    result = MrivcStatic.IndentXml(getStateResponse);

                    //result = result + result;

                }

                var resXml = "<mrivcRoot>";
                resXml += result;
                resXml += "</mrivcRoot>";

                return resXml;
        }
            catch (MrivcCommandException mcex)
            {
                if (this is ISingleHouseCommand)
                    MrivcStatic.WriteLog(this, mcex, AddToReport, ((ISingleHouseCommand)this).FiasHouseGuid);
                else
                    MrivcStatic.WriteLog(this, mcex, AddToReport);

                HasErrors = true;
                return null;
            }
            catch (Exception ex)
            {
                if (this is ISingleHouseCommand)
                    MrivcStatic.WriteLog(this, ex, ((ISingleHouseCommand)this).FiasHouseGuid);
                else
                    MrivcStatic.WriteLog(this, ex);

                HasErrors = true;
                return null;
    }
}

        public string Execute(IEnumerable<Tuple<string[],string>> values)
        {
            mrivcXmlFormatter.LoadRoot(MrivcStatic.PrepareXmlBody(Service, Method, values));
            return Execute();
        }

        DataSet ExecuteToDataSetBase(string response, string nodeName)
        {
            if (!(response == null || response == ""))
            {
                var res = MrivcStatic.SelectNodesByName(MrivcStatic.ResultNodeName(nodeName), response);

                var resXml = "<mrivcResult>";
                foreach (XmlNode item in res)
                {
                    resXml += item.OuterXml;
                }
                resXml += "</mrivcResult>";
                var resXml1 = MrivcStatic.IndentXml(resXml);

                try
                {
                    MrivcStatic.RemoveNodesByName("Terminate", ref resXml);
                }
                catch (Exception)
                {}

                try
                {
                    MrivcStatic.RemoveNodesByName("Volume", ref resXml);
                }
                catch (Exception)
                { }

                var ds = new DataSet();
                ds.ReadXml(new StringReader(resXml));
                return ds;
            }
            else
                return new DataSet();
        }

        public DataSet ExecuteToDataSet(IEnumerable<Tuple<string[], string>> values, string nodeName)
        {
            var response = Execute(values);
            return ExecuteToDataSetBase(response, nodeName);
        }

        public DataSet ExecuteToDataSet(string nodeName)
        {
            var response = Execute();
            return ExecuteToDataSetBase(response, nodeName);
        }

        protected void ErrorCheck(string response, string sendRequest, string messageGUID)
        {
            var err = MrivcStatic.GetErrorFromResponse(response);

            if (err != null)
                if(!(this is MrivcExportCommand && err.Item1 == "INT002012"))
                    if (!(err.Item1 == "AUT011003"))
                    {
                        response = MrivcStatic.IndentXml(response);

                        var ex = new MrivcCommandException(err.Item1, err.Item2, SenderID, Service, Method);
                        ex.SendRequest = sendRequest;
                        ex.MessageGUID = messageGUID;
                        ex.Response = response;
                        throw ex;
                    }
        }

        protected void ErrorCheck(string response, string sendRequest)
        {
            var err = MrivcStatic.GetErrorFromResponse(response);

            if (err != null)
                if (!(this is MrivcExportCommand && err.Item1 == "INT002012"))
                    if (!(err.Item1 == "AUT011003"))
                    {
                        response = MrivcStatic.IndentXml(response);

                        var ex = new MrivcCommandException(err.Item1, err.Item2, SenderID, Service, Method);
                        ex.SendRequest = sendRequest;
                        ex.Response = response;
                        throw ex; 
                    }
        }

        public virtual void ExecuteToTempDB(IEnumerable<Tuple<string[], string>> values, string nodeName)
        {
            var ds = ExecuteToDataSet(values, nodeName);
            DAccess.DataModule.CopyDataSetToTempDB(ds);
        }


        public virtual void ExecuteToTempDB(string nodeName)
        {
            DAccess.DataModule.CopyDataSetToTempDB(ExecuteToDataSet(nodeName));
        }

        public virtual object Clone()
        {
            //MrivcCommand mrivcCommand = new MrivcCommand();
            //mrivcCommand = this;
            //return mrivcCommand;
            var clone = this.MemberwiseClone();
            MrivcXmlFormatter mrivcXmlFormatterClone = (MrivcXmlFormatter)this.mrivcXmlFormatter.Clone();
            ((MrivcCommand)clone).mrivcXmlFormatter = mrivcXmlFormatterClone;
            return clone;
        }

    }
}
