﻿namespace FastReport.Design.StandardDesigner
{
    partial class DesignerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemContainer24 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.barEllis = new FastReport.DevComponents.DotNetBar.RibbonBar();

            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignerForm));
            this.ribbonControl = new FastReport.DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel1 = new FastReport.DevComponents.DotNetBar.RibbonPanel();
            this.barEditing = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.itemContainer14 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnFind = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnReplace = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSelectAll = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.barStyles = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.itemContainer15 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.itemContainer16 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnStyles = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.barFormat = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.itemContainer13 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnHighlight = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFormat = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.barBorderAndFill = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.itemContainer10 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.itemContainer11 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnAllLines = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnNoLines = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnTopLine = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnBottomLine = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnLeftLine = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnRightLine = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnBorderProps = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.itemContainer12 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnFillProps = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.barText = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.itemContainer7 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.itemContainer8 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnBold = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnItalic = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnUnderline = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.itemContainer9 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnAlignLeft = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignCenter = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignRight = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnJustify = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignTop = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignMiddle = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignBottom = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnTextRotation = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.barClipboard = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.btnPaste = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.itemContainer6 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnCopy = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnCut = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFormatPainter = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.barUndo = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.itemContainer5 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnUndo = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnRedo = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel4 = new FastReport.DevComponents.DotNetBar.RibbonPanel();
            this.barView = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.btnViewGrid = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnViewGuides = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.itemContainer22 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnAutoGuides = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnDeleteHGuides = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnDeleteVGuides = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnUnits = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnUnitsMillimeters = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnUnitsCentimeters = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnUnitsInches = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnUnitsHundrethsOfInch = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnPanels = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnProperties = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnData = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnReportTree = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnMessages = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel3 = new FastReport.DevComponents.DotNetBar.RibbonPanel();
            this.barLayout = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.btnAlignToGrid = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFitToGrid = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignment = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignLefts = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignCenters = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignRights = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignTops = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignMiddles = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAlignBottoms = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnCenterHorizontally = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnCenterVertically = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSize = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSameWidth = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSameHeight = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSameSize = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSpacing = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSpaceHorizontally = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnIncreaseHorizontalSpacing = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnDecreaseHorizontalSpacing = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnRemoveHorizontalSpacing = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSpaceVertically = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnIncreaseVerticalSpacing = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnDecreaseVerticalSpacing = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnRemoveVerticalSpacing = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnBringToFront = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnSendToBack = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnGroup = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnUngroup = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel2 = new FastReport.DevComponents.DotNetBar.RibbonPanel();
            this.barBands = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.btnConfigureBands = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnGroupExpert = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.itemContainer18 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.itemContainer19 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.itemContainer20 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnReportTitle = new FastReport.DevComponents.DotNetBar.CheckBoxItem();
            this.btnReportSummary = new FastReport.DevComponents.DotNetBar.CheckBoxItem();
            this.btnPageHeader = new FastReport.DevComponents.DotNetBar.CheckBoxItem();
            this.itemContainer21 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnColumnHeader = new FastReport.DevComponents.DotNetBar.CheckBoxItem();
            this.btnColumnFooter = new FastReport.DevComponents.DotNetBar.CheckBoxItem();
            this.btnOverlay = new FastReport.DevComponents.DotNetBar.CheckBoxItem();
            this.btnPageFooter = new FastReport.DevComponents.DotNetBar.CheckBoxItem();
            this.barPages = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.itemContainer17 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnAddPage = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAddDialog = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnDeletePage = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnPageSetup = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.barData = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.btnDataChoose = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnDataAdd = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.barReport = new FastReport.DevComponents.DotNetBar.RibbonBar();
            this.btnPreview = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnReportOptions = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFile = new FastReport.DevComponents.DotNetBar.ApplicationButton();
            this.itemContainer1 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.itemContainer2 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.itemContainer3 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnFileNew = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFileOpen = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFileSave = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFileSaveAs = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFileSaveAll = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFilePrinterSetup = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFilePreview = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFileSelectLanguage = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFileClose = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.itemContainer23 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.lblRecent = new FastReport.DevComponents.DotNetBar.LabelItem();
            this.itemContainer4 = new FastReport.DevComponents.DotNetBar.ItemContainer();
            this.btnOptions = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnFileExit = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.tabHome = new FastReport.DevComponents.DotNetBar.RibbonTabItem();
            this.tabReport = new FastReport.DevComponents.DotNetBar.RibbonTabItem();
            this.tabLayout = new FastReport.DevComponents.DotNetBar.RibbonTabItem();
            this.tabView = new FastReport.DevComponents.DotNetBar.RibbonTabItem();
            this.buttonItem15 = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.styleManager = new FastReport.DevComponents.DotNetBar.StyleManager(this.components);
            this.statusBar = new FastReport.DevComponents.DotNetBar.Bar();
            this.location = new FastReport.DevComponents.DotNetBar.LabelItem();
            this.size = new FastReport.DevComponents.DotNetBar.LabelItem();
            this.text = new FastReport.DevComponents.DotNetBar.LabelItem();
            this.zoom1 = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.zoom2 = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.zoom3 = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.zoomLabel = new FastReport.DevComponents.DotNetBar.LabelItem();
            this.slider = new FastReport.DevComponents.DotNetBar.SliderItem();
            this.btnHelp = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.btnAbout = new FastReport.DevComponents.DotNetBar.ButtonItem();
            this.cbxStyles = new FastReport.Controls.StyleComboBoxItem();
            this.btnFillColor = new FastReport.Controls.ColorButtonItem();
            this.btnLineColor = new FastReport.Controls.ColorButtonItem();
            this.btnLineStyle = new FastReport.Controls.LineStyleButtonItem();
            this.btnLineWidth = new FastReport.Controls.LineWidthButtonItem();
            this.cbxFontName = new FastReport.Controls.FontComboBoxItem();
            this.cbxFontSize = new FastReport.Controls.FontSizeComboBoxItem();
            this.btnTextColor = new FastReport.Controls.ColorButtonItem();
            this.ribbonControl.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusBar)).BeginInit();
            this.SuspendLayout();


            // 
            // ribbonControl
            // 
            this.ribbonControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.ribbonControl.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl.CanCustomize = false;
            this.ribbonControl.Controls.Add(this.ribbonPanel1);
            this.ribbonControl.Controls.Add(this.ribbonPanel2);
            this.ribbonControl.Controls.Add(this.ribbonPanel4);
            this.ribbonControl.Controls.Add(this.ribbonPanel3);
            this.ribbonControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl.EnableQatPlacement = false;
            this.ribbonControl.ForeColor = System.Drawing.Color.Black;
            this.ribbonControl.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnFile,
            this.tabHome,
            this.tabReport,
            this.tabLayout,
            this.tabView});
            this.ribbonControl.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.Margin = new System.Windows.Forms.Padding(0);
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Size = new System.Drawing.Size(969, 130);
            this.ribbonControl.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl.TabGroupHeight = 14;
            this.ribbonControl.TabIndex = 0;
            this.ribbonControl.UseCustomizeDialog = false;
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel1.Controls.Add(this.barEditing);
            this.ribbonPanel1.Controls.Add(this.barStyles);
            this.ribbonPanel1.Controls.Add(this.barFormat);
            this.ribbonPanel1.Controls.Add(this.barBorderAndFill);
            this.ribbonPanel1.Controls.Add(this.barText);
            this.ribbonPanel1.Controls.Add(this.barClipboard);
            this.ribbonPanel1.Controls.Add(this.barUndo);
            this.ribbonPanel1.Controls.Add(this.barEllis);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 26);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel1.Size = new System.Drawing.Size(969, 104);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            // 
            // barEditing
            // 
            this.barEditing.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barEditing.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barEditing.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barEditing.ContainerControlProcessDialogKey = true;
            this.barEditing.Dock = System.Windows.Forms.DockStyle.Left;
            this.barEditing.DragDropSupport = true;
            this.barEditing.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer14});
            this.barEditing.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barEditing.Location = new System.Drawing.Point(841, 0);
            this.barEditing.Name = "barEditing";
            this.barEditing.Size = new System.Drawing.Size(62, 102);
            this.barEditing.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barEditing.TabIndex = 6;
            this.barEditing.Text = "Editing";
            // 
            // 
            // 
            this.barEditing.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barEditing.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // Буторин 07-04-16 Контейнер с кнопкой загрузки в Эллис
            // barEllis
            // 
            this.barEllis.AutoOverflowEnabled = true;
            this.barEllis.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barEllis.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barEllis.ContainerControlProcessDialogKey = true;
            this.barEllis.Dock = System.Windows.Forms.DockStyle.Left;
            this.barEllis.DragDropSupport = true;
            this.barEllis.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer24});
            this.barEllis.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barEllis.Location = new System.Drawing.Point(1200, 0);
            this.barEllis.Name = "barEllis";
            this.barEllis.Size = new System.Drawing.Size(62, 102);
            this.barEllis.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barEllis.TabIndex = 6;
            this.barEllis.Text = "Эллис";

            // itemContainer24

            this.itemContainer24.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer24.Name = "itemContainer24";
            this.itemContainer24.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer24.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;

            // 
            // itemContainer14
            // 
            // 
            // 
            // 
            this.itemContainer14.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer14.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer14.Name = "itemContainer14";
            this.itemContainer14.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnFind,
            this.btnReplace,
            this.btnSelectAll});
            // 
            // 
            // 
            this.itemContainer14.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer14.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnFind
            // 
            this.btnFind.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFind.Name = "btnFind";
            this.btnFind.Text = "Find";
            // 
            // btnReplace
            // 
            this.btnReplace.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Text = "Replace";
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Text = "Select All";
            // 
            // barStyles
            // 
            this.barStyles.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barStyles.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barStyles.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barStyles.ContainerControlProcessDialogKey = true;
            this.barStyles.Dock = System.Windows.Forms.DockStyle.Left;
            this.barStyles.DragDropSupport = true;
            this.barStyles.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer15});
            this.barStyles.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barStyles.Location = new System.Drawing.Point(732, 0);
            this.barStyles.Name = "barStyles";
            this.barStyles.Size = new System.Drawing.Size(109, 102);
            this.barStyles.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barStyles.TabIndex = 5;
            this.barStyles.Text = "Styles";
            // 
            // 
            // 
            this.barStyles.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barStyles.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer15
            // 
            // 
            // 
            // 
            this.itemContainer15.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer15.HorizontalItemAlignment = FastReport.DevComponents.DotNetBar.eHorizontalItemsAlignment.Center;
            this.itemContainer15.ItemSpacing = 8;
            this.itemContainer15.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer15.Name = "itemContainer15";
            this.itemContainer15.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.cbxStyles,
            this.itemContainer16});
            // 
            // 
            // 
            this.itemContainer15.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer15.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // itemContainer16
            // 
            // 
            // 
            // 
            this.itemContainer16.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer16.HorizontalItemAlignment = FastReport.DevComponents.DotNetBar.eHorizontalItemsAlignment.Center;
            this.itemContainer16.Name = "itemContainer16";
            this.itemContainer16.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnStyles});
            // 
            // 
            // 
            this.itemContainer16.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnStyles
            // 
            this.btnStyles.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStyles.Name = "btnStyles";
            this.btnStyles.Text = "Styles";
            // 
            // barFormat
            // 
            this.barFormat.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barFormat.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barFormat.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barFormat.ContainerControlProcessDialogKey = true;
            this.barFormat.Dock = System.Windows.Forms.DockStyle.Left;
            this.barFormat.DragDropSupport = true;
            this.barFormat.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer13});
            this.barFormat.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barFormat.Location = new System.Drawing.Point(565, 0);
            this.barFormat.Name = "barFormat";
            this.barFormat.Size = new System.Drawing.Size(167, 102);
            this.barFormat.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barFormat.TabIndex = 4;
            this.barFormat.Text = "Format";

            // 
            // 
            // 
            this.barFormat.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barFormat.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;



           
            // 
            // itemContainer13
            // 
            // 
            // 
            // 
            this.itemContainer13.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer13.Name = "itemContainer13";
            this.itemContainer13.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnHighlight,
            this.btnFormat});
            // 
            // 
            // 
            this.itemContainer13.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer13.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnHighlight
            // 
            this.btnHighlight.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnHighlight.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnHighlight.Name = "btnHighlight";
            this.btnHighlight.Text = "Conditional Highlight";
            // 
            // btnFormat
            // 
            this.btnFormat.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFormat.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnFormat.Name = "btnFormat";
            this.btnFormat.Text = "Format";
            // 
            // barBorderAndFill
            // 
            this.barBorderAndFill.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barBorderAndFill.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barBorderAndFill.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barBorderAndFill.ContainerControlProcessDialogKey = true;
            this.barBorderAndFill.Dock = System.Windows.Forms.DockStyle.Left;
            this.barBorderAndFill.DragDropSupport = true;
            this.barBorderAndFill.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer10});
            this.barBorderAndFill.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barBorderAndFill.Location = new System.Drawing.Point(407, 0);
            this.barBorderAndFill.Name = "barBorderAndFill";
            this.barBorderAndFill.Size = new System.Drawing.Size(158, 102);
            this.barBorderAndFill.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barBorderAndFill.TabIndex = 3;
            this.barBorderAndFill.Text = "Border and Fill";
            // 
            // 
            // 
            this.barBorderAndFill.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barBorderAndFill.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer10
            // 
            // 
            // 
            // 
            this.itemContainer10.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer10.ItemSpacing = 10;
            this.itemContainer10.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer10.Name = "itemContainer10";
            this.itemContainer10.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer11,
            this.itemContainer12});
            // 
            // 
            // 
            this.itemContainer10.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer10.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // itemContainer11
            // 
            // 
            // 
            // 
            this.itemContainer11.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer11.Name = "itemContainer11";
            this.itemContainer11.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnAllLines,
            this.btnNoLines,
            this.btnTopLine,
            this.btnBottomLine,
            this.btnLeftLine,
            this.btnRightLine,
            this.btnBorderProps});
            // 
            // 
            // 
            this.itemContainer11.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnAllLines
            // 
            this.btnAllLines.Name = "btnAllLines";
            this.btnAllLines.Text = "1";
            // 
            // btnNoLines
            // 
            this.btnNoLines.Name = "btnNoLines";
            this.btnNoLines.Text = "2";
            // 
            // btnTopLine
            // 
            this.btnTopLine.BeginGroup = true;
            this.btnTopLine.Name = "btnTopLine";
            this.btnTopLine.Text = "3";
            // 
            // btnBottomLine
            // 
            this.btnBottomLine.Name = "btnBottomLine";
            this.btnBottomLine.Text = "4";
            // 
            // btnLeftLine
            // 
            this.btnLeftLine.Name = "btnLeftLine";
            this.btnLeftLine.Text = "5";
            // 
            // btnRightLine
            // 
            this.btnRightLine.Name = "btnRightLine";
            this.btnRightLine.Text = "6";
            // 
            // btnBorderProps
            // 
            this.btnBorderProps.BeginGroup = true;
            this.btnBorderProps.Name = "btnBorderProps";
            this.btnBorderProps.Text = "7";
            // 
            // itemContainer12
            // 
            // 
            // 
            // 
            this.itemContainer12.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer12.Name = "itemContainer12";
            this.itemContainer12.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnFillColor,
            this.btnFillProps,
            this.btnLineColor,
            this.btnLineStyle,
            this.btnLineWidth});
            // 
            // 
            // 
            this.itemContainer12.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnFillProps
            // 
            this.btnFillProps.Name = "btnFillProps";
            this.btnFillProps.Text = "2";
            // 
            // barText
            // 
            this.barText.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barText.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barText.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barText.ContainerControlProcessDialogKey = true;
            this.barText.Dock = System.Windows.Forms.DockStyle.Left;
            this.barText.DragDropSupport = true;
            this.barText.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer7});
            this.barText.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barText.Location = new System.Drawing.Point(178, 0);
            this.barText.Name = "barText";
            this.barText.Size = new System.Drawing.Size(229, 102);
            this.barText.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barText.TabIndex = 2;
            this.barText.Text = "Text";
            // 
            // 
            // 
            this.barText.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barText.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer7
            // 
            // 
            // 
            // 
            this.itemContainer7.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer7.ItemSpacing = 10;
            this.itemContainer7.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer7.Name = "itemContainer7";
            this.itemContainer7.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer8,
            this.itemContainer9});
            // 
            // 
            // 
            this.itemContainer7.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer7.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // itemContainer8
            // 
            // 
            // 
            // 
            this.itemContainer8.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer8.Name = "itemContainer8";
            this.itemContainer8.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.cbxFontName,
            this.cbxFontSize,
            this.btnBold,
            this.btnItalic,
            this.btnUnderline});
            // 
            // 
            // 
            this.itemContainer8.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnBold
            // 
            this.btnBold.Name = "btnBold";
            this.btnBold.Text = "B";
            // 
            // btnItalic
            // 
            this.btnItalic.Name = "btnItalic";
            this.btnItalic.Text = "I";
            // 
            // btnUnderline
            // 
            this.btnUnderline.Name = "btnUnderline";
            this.btnUnderline.Text = "U";
            // 
            // itemContainer9
            // 
            // 
            // 
            // 
            this.itemContainer9.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer9.Name = "itemContainer9";
            this.itemContainer9.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnAlignLeft,
            this.btnAlignCenter,
            this.btnAlignRight,
            this.btnJustify,
            this.btnAlignTop,
            this.btnAlignMiddle,
            this.btnAlignBottom,
            this.btnTextColor,
            this.btnTextRotation});
            // 
            // 
            // 
            this.itemContainer9.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnAlignLeft
            // 
            this.btnAlignLeft.Name = "btnAlignLeft";
            this.btnAlignLeft.Text = "<|";
            // 
            // btnAlignCenter
            // 
            this.btnAlignCenter.Name = "btnAlignCenter";
            this.btnAlignCenter.Text = "||";
            // 
            // btnAlignRight
            // 
            this.btnAlignRight.Name = "btnAlignRight";
            this.btnAlignRight.Text = "|>";
            // 
            // btnJustify
            // 
            this.btnJustify.Name = "btnJustify";
            this.btnJustify.Text = "<>";
            // 
            // btnAlignTop
            // 
            this.btnAlignTop.BeginGroup = true;
            this.btnAlignTop.Name = "btnAlignTop";
            this.btnAlignTop.Text = "^";
            // 
            // btnAlignMiddle
            // 
            this.btnAlignMiddle.Name = "btnAlignMiddle";
            this.btnAlignMiddle.Text = "-";
            // 
            // btnAlignBottom
            // 
            this.btnAlignBottom.Name = "btnAlignBottom";
            this.btnAlignBottom.Text = "_";
            // 
            // btnTextRotation
            // 
            this.btnTextRotation.Name = "btnTextRotation";
            this.btnTextRotation.Text = "R";
            // 
            // barClipboard
            // 
            this.barClipboard.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barClipboard.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barClipboard.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barClipboard.ContainerControlProcessDialogKey = true;
            this.barClipboard.Dock = System.Windows.Forms.DockStyle.Left;
            this.barClipboard.DragDropSupport = true;
            this.barClipboard.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnPaste,
            this.itemContainer6});
            this.barClipboard.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barClipboard.Location = new System.Drawing.Point(49, 0);
            this.barClipboard.Name = "barClipboard";
            this.barClipboard.Size = new System.Drawing.Size(129, 102);
            this.barClipboard.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barClipboard.TabIndex = 1;
            this.barClipboard.Text = "Clipboard";
            // 
            // 
            // 
            this.barClipboard.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barClipboard.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barClipboard.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnPaste
            // 
            this.btnPaste.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaste.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPaste.Name = "btnPaste";
            this.btnPaste.SubItemsExpandWidth = 14;
            this.btnPaste.Text = "Paste";
            // 
            // itemContainer6
            // 
            // 
            // 
            // 
            this.itemContainer6.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer6.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer6.Name = "itemContainer6";
            this.itemContainer6.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnCopy,
            this.btnCut,
            this.btnFormatPainter});
            // 
            // 
            // 
            this.itemContainer6.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer6.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnCopy
            // 
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Text = "Copy";
            // 
            // btnCut
            // 
            this.btnCut.Name = "btnCut";
            this.btnCut.Text = "Cut";
            // 
            // btnFormatPainter
            // 
            this.btnFormatPainter.AutoCheckOnClick = true;
            this.btnFormatPainter.Name = "btnFormatPainter";
            this.btnFormatPainter.Text = "Format Painter";
            // 
            // barUndo
            // 
            this.barUndo.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barUndo.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barUndo.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barUndo.ContainerControlProcessDialogKey = true;
            this.barUndo.Dock = System.Windows.Forms.DockStyle.Left;
            this.barUndo.DragDropSupport = true;
            this.barUndo.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer5});
            this.barUndo.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barUndo.Location = new System.Drawing.Point(3, 0);
            this.barUndo.Name = "barUndo";
            this.barUndo.Size = new System.Drawing.Size(46, 102);
            this.barUndo.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barUndo.TabIndex = 0;
            // 
            // 
            // 
            this.barUndo.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barUndo.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barUndo.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // itemContainer5
            // 
            // 
            // 
            // 
            this.itemContainer5.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer5.HorizontalItemAlignment = FastReport.DevComponents.DotNetBar.eHorizontalItemsAlignment.Center;
            this.itemContainer5.ItemSpacing = 8;
            this.itemContainer5.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer5.Name = "itemContainer5";
            this.itemContainer5.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnUndo,
            this.btnRedo});
            // 
            // 
            // 
            this.itemContainer5.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer5.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnUndo
            // 
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Text = "Undo";
            // 
            // btnRedo
            // 
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Text = "Redo";
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ColorSchemeStyle = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel4.Controls.Add(this.barView);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 26);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel4.Size = new System.Drawing.Size(969, 104);
            // 
            // 
            // 
            this.ribbonPanel4.Style.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseDown.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel4.TabIndex = 4;
            this.ribbonPanel4.Visible = false;
            // 
            // barView
            // 
            this.barView.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barView.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barView.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barView.ContainerControlProcessDialogKey = true;
            this.barView.Dock = System.Windows.Forms.DockStyle.Left;
            this.barView.DragDropSupport = true;
            this.barView.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnViewGrid,
            this.btnViewGuides,
            this.itemContainer22,
            this.btnUnits,
            this.btnPanels});
            this.barView.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barView.Location = new System.Drawing.Point(3, 0);
            this.barView.Name = "barView";
            this.barView.Size = new System.Drawing.Size(355, 102);
            this.barView.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barView.TabIndex = 0;
            this.barView.Text = "View";
            // 
            // 
            // 
            this.barView.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barView.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnViewGrid
            // 
            this.btnViewGrid.AutoCheckOnClick = true;
            this.btnViewGrid.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnViewGrid.Name = "btnViewGrid";
            this.btnViewGrid.SubItemsExpandWidth = 14;
            this.btnViewGrid.Text = "Grid";
            // 
            // btnViewGuides
            // 
            this.btnViewGuides.AutoCheckOnClick = true;
            this.btnViewGuides.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnViewGuides.Name = "btnViewGuides";
            this.btnViewGuides.SubItemsExpandWidth = 14;
            this.btnViewGuides.Text = "Guides";
            // 
            // itemContainer22
            // 
            // 
            // 
            // 
            this.itemContainer22.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer22.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer22.Name = "itemContainer22";
            this.itemContainer22.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnAutoGuides,
            this.btnDeleteHGuides,
            this.btnDeleteVGuides});
            // 
            // 
            // 
            this.itemContainer22.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer22.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnAutoGuides
            // 
            this.btnAutoGuides.AutoCheckOnClick = true;
            this.btnAutoGuides.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAutoGuides.Name = "btnAutoGuides";
            this.btnAutoGuides.Text = "Automatic Guides";
            // 
            // btnDeleteHGuides
            // 
            this.btnDeleteHGuides.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDeleteHGuides.Name = "btnDeleteHGuides";
            this.btnDeleteHGuides.Text = "Delete Horizontal Guides";
            // 
            // btnDeleteVGuides
            // 
            this.btnDeleteVGuides.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDeleteVGuides.Name = "btnDeleteVGuides";
            this.btnDeleteVGuides.Text = "Delete Vertical Guides";
            // 
            // btnUnits
            // 
            this.btnUnits.AutoExpandOnClick = true;
            this.btnUnits.BeginGroup = true;
            this.btnUnits.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUnits.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUnits.Name = "btnUnits";
            this.btnUnits.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnUnitsMillimeters,
            this.btnUnitsCentimeters,
            this.btnUnitsInches,
            this.btnUnitsHundrethsOfInch});
            this.btnUnits.SubItemsExpandWidth = 14;
            this.btnUnits.Text = "Units";
            // 
            // btnUnitsMillimeters
            // 
            this.btnUnitsMillimeters.AutoCheckOnClick = true;
            this.btnUnitsMillimeters.Name = "btnUnitsMillimeters";
            this.btnUnitsMillimeters.Text = "Millimeters";
            // 
            // btnUnitsCentimeters
            // 
            this.btnUnitsCentimeters.AutoCheckOnClick = true;
            this.btnUnitsCentimeters.Name = "btnUnitsCentimeters";
            this.btnUnitsCentimeters.Text = "Centimeters";
            // 
            // btnUnitsInches
            // 
            this.btnUnitsInches.AutoCheckOnClick = true;
            this.btnUnitsInches.Name = "btnUnitsInches";
            this.btnUnitsInches.Text = "Inches";
            // 
            // btnUnitsHundrethsOfInch
            // 
            this.btnUnitsHundrethsOfInch.AutoCheckOnClick = true;
            this.btnUnitsHundrethsOfInch.Name = "btnUnitsHundrethsOfInch";
            this.btnUnitsHundrethsOfInch.Text = "Hundreths of inch";
            // 
            // btnPanels
            // 
            this.btnPanels.AutoExpandOnClick = true;
            this.btnPanels.BeginGroup = true;
            this.btnPanels.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPanels.Name = "btnPanels";
            this.btnPanels.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnProperties,
            this.btnData,
            this.btnReportTree,
            this.btnMessages});
            this.btnPanels.SubItemsExpandWidth = 14;
            this.btnPanels.Text = "Panels";
            // 
            // btnProperties
            // 
            this.btnProperties.AutoCheckOnClick = true;
            this.btnProperties.Name = "btnProperties";
            this.btnProperties.Text = "Properties";
            // 
            // btnData
            // 
            this.btnData.AutoCheckOnClick = true;
            this.btnData.Name = "btnData";
            this.btnData.Text = "Data";
            // 
            // btnReportTree
            // 
            this.btnReportTree.AutoCheckOnClick = true;
            this.btnReportTree.Name = "btnReportTree";
            this.btnReportTree.Text = "Report Tree";
            // 
            // btnMessages
            // 
            this.btnMessages.AutoCheckOnClick = true;
            this.btnMessages.Name = "btnMessages";
            this.btnMessages.Text = "Messages";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel3.Controls.Add(this.barLayout);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 26);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel3.Size = new System.Drawing.Size(969, 104);
            // 
            // 
            // 
            this.ribbonPanel3.Style.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            this.ribbonPanel3.Visible = false;
            // 
            // barLayout
            // 
            this.barLayout.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barLayout.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barLayout.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barLayout.ContainerControlProcessDialogKey = true;
            this.barLayout.Dock = System.Windows.Forms.DockStyle.Left;
            this.barLayout.DragDropSupport = true;
            this.barLayout.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnAlignToGrid,
            this.btnFitToGrid,
            this.btnAlignment,
            this.btnSize,
            this.btnSpacing,
            this.btnBringToFront,
            this.btnSendToBack,
            this.btnGroup,
            this.btnUngroup});
            this.barLayout.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barLayout.Location = new System.Drawing.Point(3, 0);
            this.barLayout.Name = "barLayout";
            this.barLayout.Size = new System.Drawing.Size(596, 102);
            this.barLayout.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barLayout.TabIndex = 0;
            this.barLayout.Text = "Layout";
            // 
            // 
            // 
            this.barLayout.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barLayout.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnAlignToGrid
            // 
            this.btnAlignToGrid.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAlignToGrid.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAlignToGrid.Name = "btnAlignToGrid";
            this.btnAlignToGrid.SubItemsExpandWidth = 14;
            this.btnAlignToGrid.Text = "Align to Grid";
            // 
            // btnFitToGrid
            // 
            this.btnFitToGrid.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFitToGrid.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnFitToGrid.Name = "btnFitToGrid";
            this.btnFitToGrid.SubItemsExpandWidth = 14;
            this.btnFitToGrid.Text = "Fit to Grid";
            // 
            // btnAlignment
            // 
            this.btnAlignment.AutoExpandOnClick = true;
            this.btnAlignment.BeginGroup = true;
            this.btnAlignment.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAlignment.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAlignment.Name = "btnAlignment";
            this.btnAlignment.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnAlignLefts,
            this.btnAlignCenters,
            this.btnAlignRights,
            this.btnAlignTops,
            this.btnAlignMiddles,
            this.btnAlignBottoms,
            this.btnCenterHorizontally,
            this.btnCenterVertically});
            this.btnAlignment.SubItemsExpandWidth = 14;
            this.btnAlignment.Text = "Alignment";
            // 
            // btnAlignLefts
            // 
            this.btnAlignLefts.Name = "btnAlignLefts";
            this.btnAlignLefts.Text = "Align Lefts";
            // 
            // btnAlignCenters
            // 
            this.btnAlignCenters.Name = "btnAlignCenters";
            this.btnAlignCenters.Text = "Align Centers";
            // 
            // btnAlignRights
            // 
            this.btnAlignRights.Name = "btnAlignRights";
            this.btnAlignRights.Text = "Align Rights";
            // 
            // btnAlignTops
            // 
            this.btnAlignTops.BeginGroup = true;
            this.btnAlignTops.Name = "btnAlignTops";
            this.btnAlignTops.Text = "Align Tops";
            // 
            // btnAlignMiddles
            // 
            this.btnAlignMiddles.Name = "btnAlignMiddles";
            this.btnAlignMiddles.Text = "Align Middles";
            // 
            // btnAlignBottoms
            // 
            this.btnAlignBottoms.Name = "btnAlignBottoms";
            this.btnAlignBottoms.Text = "Align Bottoms";
            // 
            // btnCenterHorizontally
            // 
            this.btnCenterHorizontally.BeginGroup = true;
            this.btnCenterHorizontally.Name = "btnCenterHorizontally";
            this.btnCenterHorizontally.Text = "Center Horizontally";
            // 
            // btnCenterVertically
            // 
            this.btnCenterVertically.Name = "btnCenterVertically";
            this.btnCenterVertically.Text = "Center Vertically";
            // 
            // btnSize
            // 
            this.btnSize.AutoExpandOnClick = true;
            this.btnSize.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSize.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSize.Name = "btnSize";
            this.btnSize.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnSameWidth,
            this.btnSameHeight,
            this.btnSameSize});
            this.btnSize.SubItemsExpandWidth = 14;
            this.btnSize.Text = "Size";
            // 
            // btnSameWidth
            // 
            this.btnSameWidth.Name = "btnSameWidth";
            this.btnSameWidth.Text = "Same Width";
            // 
            // btnSameHeight
            // 
            this.btnSameHeight.Name = "btnSameHeight";
            this.btnSameHeight.Text = "Same Height";
            // 
            // btnSameSize
            // 
            this.btnSameSize.Name = "btnSameSize";
            this.btnSameSize.Text = "Same Size";
            // 
            // btnSpacing
            // 
            this.btnSpacing.AutoExpandOnClick = true;
            this.btnSpacing.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSpacing.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSpacing.Name = "btnSpacing";
            this.btnSpacing.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnSpaceHorizontally,
            this.btnIncreaseHorizontalSpacing,
            this.btnDecreaseHorizontalSpacing,
            this.btnRemoveHorizontalSpacing,
            this.btnSpaceVertically,
            this.btnIncreaseVerticalSpacing,
            this.btnDecreaseVerticalSpacing,
            this.btnRemoveVerticalSpacing});
            this.btnSpacing.SubItemsExpandWidth = 14;
            this.btnSpacing.Text = "Spacing";
            // 
            // btnSpaceHorizontally
            // 
            this.btnSpaceHorizontally.Name = "btnSpaceHorizontally";
            this.btnSpaceHorizontally.Text = "Space Horizontally";
            // 
            // btnIncreaseHorizontalSpacing
            // 
            this.btnIncreaseHorizontalSpacing.Name = "btnIncreaseHorizontalSpacing";
            this.btnIncreaseHorizontalSpacing.Text = "Increase Horizontal Spacing";
            // 
            // btnDecreaseHorizontalSpacing
            // 
            this.btnDecreaseHorizontalSpacing.Name = "btnDecreaseHorizontalSpacing";
            this.btnDecreaseHorizontalSpacing.Text = "Decrease Horizontal Spacing";
            // 
            // btnRemoveHorizontalSpacing
            // 
            this.btnRemoveHorizontalSpacing.Name = "btnRemoveHorizontalSpacing";
            this.btnRemoveHorizontalSpacing.Text = "Remove Horizontal Spacing";
            // 
            // btnSpaceVertically
            // 
            this.btnSpaceVertically.BeginGroup = true;
            this.btnSpaceVertically.Name = "btnSpaceVertically";
            this.btnSpaceVertically.Text = "Space Vertically";
            // 
            // btnIncreaseVerticalSpacing
            // 
            this.btnIncreaseVerticalSpacing.Name = "btnIncreaseVerticalSpacing";
            this.btnIncreaseVerticalSpacing.Text = "Increase Vertical Spacing";
            // 
            // btnDecreaseVerticalSpacing
            // 
            this.btnDecreaseVerticalSpacing.Name = "btnDecreaseVerticalSpacing";
            this.btnDecreaseVerticalSpacing.Text = "Decrease Vertical Spacing";
            // 
            // btnRemoveVerticalSpacing
            // 
            this.btnRemoveVerticalSpacing.Name = "btnRemoveVerticalSpacing";
            this.btnRemoveVerticalSpacing.Text = "Remove Vertical Spacing";
            // 
            // btnBringToFront
            // 
            this.btnBringToFront.BeginGroup = true;
            this.btnBringToFront.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnBringToFront.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBringToFront.Name = "btnBringToFront";
            this.btnBringToFront.SubItemsExpandWidth = 14;
            this.btnBringToFront.Text = "Bring to Front";
            // 
            // btnSendToBack
            // 
            this.btnSendToBack.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSendToBack.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSendToBack.Name = "btnSendToBack";
            this.btnSendToBack.SubItemsExpandWidth = 14;
            this.btnSendToBack.Text = "Send to Back";
            // 
            // btnGroup
            // 
            this.btnGroup.BeginGroup = true;
            this.btnGroup.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnGroup.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGroup.Name = "btnGroup";
            this.btnGroup.SubItemsExpandWidth = 14;
            this.btnGroup.Text = "Group";
            // 
            // btnUngroup
            // 
            this.btnUngroup.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUngroup.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUngroup.Name = "btnUngroup";
            this.btnUngroup.SubItemsExpandWidth = 14;
            this.btnUngroup.Text = "Ungroup";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel2.Controls.Add(this.barBands);
            this.ribbonPanel2.Controls.Add(this.barPages);
            this.ribbonPanel2.Controls.Add(this.barData);
            this.ribbonPanel2.Controls.Add(this.barReport);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 0);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel2.Size = new System.Drawing.Size(969, 130);
            // 
            // 
            // 
            this.ribbonPanel2.Style.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 2;
            this.ribbonPanel2.Visible = false;
            // 
            // barBands
            // 
            this.barBands.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barBands.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barBands.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barBands.ContainerControlProcessDialogKey = true;
            this.barBands.Dock = System.Windows.Forms.DockStyle.Left;
            this.barBands.DragDropSupport = true;
            this.barBands.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnConfigureBands,
            this.btnGroupExpert,
            this.itemContainer18});
            this.barBands.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barBands.Location = new System.Drawing.Point(518, 0);
            this.barBands.Name = "barBands";
            this.barBands.Size = new System.Drawing.Size(391, 128);
            this.barBands.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barBands.TabIndex = 3;
            this.barBands.Text = "Bands";
            // 
            // 
            // 
            this.barBands.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barBands.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnConfigureBands
            // 
            this.btnConfigureBands.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnConfigureBands.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnConfigureBands.Name = "btnConfigureBands";
            this.btnConfigureBands.SubItemsExpandWidth = 14;
            this.btnConfigureBands.Text = "Configure Bands";
            // 
            // btnGroupExpert
            // 
            this.btnGroupExpert.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnGroupExpert.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGroupExpert.Name = "btnGroupExpert";
            this.btnGroupExpert.SubItemsExpandWidth = 14;
            this.btnGroupExpert.Text = "Group Expert";
            // 
            // itemContainer18
            // 
            // 
            // 
            // 
            this.itemContainer18.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer18.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer18.Name = "itemContainer18";
            this.itemContainer18.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer19,
            this.btnPageFooter});
            // 
            // 
            // 
            this.itemContainer18.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer18.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // itemContainer19
            // 
            // 
            // 
            // 
            this.itemContainer19.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer19.Name = "itemContainer19";
            this.itemContainer19.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer20,
            this.itemContainer21});
            // 
            // 
            // 
            this.itemContainer19.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer20
            // 
            // 
            // 
            // 
            this.itemContainer20.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer20.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer20.Name = "itemContainer20";
            this.itemContainer20.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnReportTitle,
            this.btnReportSummary,
            this.btnPageHeader});
            // 
            // 
            // 
            this.itemContainer20.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnReportTitle
            // 
            this.btnReportTitle.Name = "btnReportTitle";
            this.btnReportTitle.Text = "Report Title";
            // 
            // btnReportSummary
            // 
            this.btnReportSummary.Name = "btnReportSummary";
            this.btnReportSummary.Text = "Report Summary";
            // 
            // btnPageHeader
            // 
            this.btnPageHeader.Name = "btnPageHeader";
            this.btnPageHeader.Text = "Page Header";
            // 
            // itemContainer21
            // 
            // 
            // 
            // 
            this.itemContainer21.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer21.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer21.Name = "itemContainer21";
            this.itemContainer21.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnColumnHeader,
            this.btnColumnFooter,
            this.btnOverlay});
            // 
            // 
            // 
            this.itemContainer21.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnColumnHeader
            // 
            this.btnColumnHeader.Name = "btnColumnHeader";
            this.btnColumnHeader.Text = "Column Header";
            // 
            // btnColumnFooter
            // 
            this.btnColumnFooter.Name = "btnColumnFooter";
            this.btnColumnFooter.Text = "Column Footer";
            // 
            // btnOverlay
            // 
            this.btnOverlay.Name = "btnOverlay";
            this.btnOverlay.Text = "Overlay";
            // 
            // btnPageFooter
            // 
            this.btnPageFooter.Name = "btnPageFooter";
            this.btnPageFooter.Text = "Page Footer";
            // 
            // barPages
            // 
            this.barPages.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barPages.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barPages.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barPages.ContainerControlProcessDialogKey = true;
            this.barPages.Dock = System.Windows.Forms.DockStyle.Left;
            this.barPages.DragDropSupport = true;
            this.barPages.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer17,
            this.btnPageSetup});
            this.barPages.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barPages.Location = new System.Drawing.Point(364, 0);
            this.barPages.Name = "barPages";
            this.barPages.Size = new System.Drawing.Size(154, 128);
            this.barPages.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barPages.TabIndex = 2;
            this.barPages.Text = "Pages";
            // 
            // 
            // 
            this.barPages.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barPages.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer17
            // 
            // 
            // 
            // 
            this.itemContainer17.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer17.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer17.Name = "itemContainer17";
            this.itemContainer17.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnAddPage,
            this.btnAddDialog,
            this.btnDeletePage});
            // 
            // 
            // 
            this.itemContainer17.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer17.VerticalItemAlignment = FastReport.DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnAddPage
            // 
            this.btnAddPage.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddPage.Name = "btnAddPage";
            this.btnAddPage.Text = "Add Page";
            // 
            // btnAddDialog
            // 
            this.btnAddDialog.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddDialog.Name = "btnAddDialog";
            this.btnAddDialog.Text = "Add Dialog";
            // 
            // btnDeletePage
            // 
            this.btnDeletePage.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDeletePage.Name = "btnDeletePage";
            this.btnDeletePage.Text = "Delete Page";
            // 
            // btnPageSetup
            // 
            this.btnPageSetup.BeginGroup = true;
            this.btnPageSetup.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPageSetup.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPageSetup.Name = "btnPageSetup";
            this.btnPageSetup.SubItemsExpandWidth = 14;
            this.btnPageSetup.Text = "Page Setup";
            // 
            // barData
            // 
            this.barData.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barData.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barData.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barData.ContainerControlProcessDialogKey = true;
            this.barData.Dock = System.Windows.Forms.DockStyle.Left;
            this.barData.DragDropSupport = true;
            this.barData.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnDataChoose,
            this.btnDataAdd});
            this.barData.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barData.Location = new System.Drawing.Point(151, 0);
            this.barData.Name = "barData";
            this.barData.Size = new System.Drawing.Size(213, 128);
            this.barData.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barData.TabIndex = 1;
            this.barData.Text = "Data";
            // 
            // 
            // 
            this.barData.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barData.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnDataChoose
            // 
            this.btnDataChoose.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDataChoose.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDataChoose.Name = "btnDataChoose";
            this.btnDataChoose.SubItemsExpandWidth = 14;
            this.btnDataChoose.Text = "Choose Report Data";
            // 
            // btnDataAdd
            // 
            this.btnDataAdd.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDataAdd.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDataAdd.Name = "btnDataAdd";
            this.btnDataAdd.SubItemsExpandWidth = 14;
            this.btnDataAdd.Text = "Add Data Source";
            // 
            // barReport
            // 
            this.barReport.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barReport.BackgroundMouseOverStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barReport.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.barReport.ContainerControlProcessDialogKey = true;
            this.barReport.Dock = System.Windows.Forms.DockStyle.Left;
            this.barReport.DragDropSupport = true;
            this.barReport.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnPreview,
            this.btnReportOptions});
            this.barReport.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.barReport.Location = new System.Drawing.Point(3, 0);
            this.barReport.Name = "barReport";
            this.barReport.Size = new System.Drawing.Size(148, 128);
            this.barReport.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barReport.TabIndex = 0;
            this.barReport.Text = "Report";
            // 
            // 
            // 
            this.barReport.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barReport.TitleStyleMouseOver.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnPreview
            // 
            this.btnPreview.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPreview.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.SubItemsExpandWidth = 14;
            this.btnPreview.Text = "Preview";
            // 
            // btnReportOptions
            // 
            this.btnReportOptions.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnReportOptions.ImagePosition = FastReport.DevComponents.DotNetBar.eImagePosition.Top;
            this.btnReportOptions.Name = "btnReportOptions";
            this.btnReportOptions.SubItemsExpandWidth = 14;
            this.btnReportOptions.Text = "Report Options";
            // 
            // btnFile
            // 
            this.btnFile.AutoExpandOnClick = true;
            this.btnFile.CanCustomize = false;
            this.btnFile.HotTrackingStyle = FastReport.DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.btnFile.Image = ((System.Drawing.Image)(resources.GetObject("btnFile.Image")));
            this.btnFile.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.btnFile.ImagePaddingHorizontal = 0;
            this.btnFile.ImagePaddingVertical = 1;
            this.btnFile.Name = "btnFile";
            this.btnFile.PopupSide = FastReport.DevComponents.DotNetBar.ePopupSide.Bottom;
            this.btnFile.ShowSubItems = false;
            this.btnFile.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer1});
            this.btnFile.Text = "&File";
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.itemContainer1.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer1.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer2,
            this.itemContainer4});
            // 
            // 
            // 
            this.itemContainer1.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.itemContainer2.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer2.ItemSpacing = 0;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3,
            this.itemContainer23});
            // 
            // 
            // 
            this.itemContainer2.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.itemContainer3.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer3.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer3.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnFileNew,
            this.btnFileOpen,
            this.btnFileSave,
            this.btnFileSaveAs,
            this.btnFileSaveAll,
            this.btnFilePreview,
            this.btnFilePrinterSetup,
            this.btnFileSelectLanguage,
            this.btnHelp,
            this.btnAbout,
            this.btnFileClose});
            // 
            // 
            // 
            this.itemContainer3.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnFileNew
            // 
            this.btnFileNew.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFileNew.Image = ((System.Drawing.Image)(resources.GetObject("btnFileNew.Image")));
            this.btnFileNew.Name = "btnFileNew";
            this.btnFileNew.SubItemsExpandWidth = 24;
            this.btnFileNew.Text = "New";
            // 
            // btnFileOpen
            // 
            this.btnFileOpen.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFileOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnFileOpen.Image")));
            this.btnFileOpen.Name = "btnFileOpen";
            this.btnFileOpen.SubItemsExpandWidth = 24;
            this.btnFileOpen.Text = "Open";
            // 
            // btnFileSave
            // 
            this.btnFileSave.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFileSave.Image = ((System.Drawing.Image)(resources.GetObject("btnFileSave.Image")));
            this.btnFileSave.Name = "btnFileSave";
            this.btnFileSave.SubItemsExpandWidth = 24;
            this.btnFileSave.Text = "Save";
            // 
            // btnFileSaveAs
            // 
            this.btnFileSaveAs.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFileSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("btnFileSaveAs.Image")));
            this.btnFileSaveAs.Name = "btnFileSaveAs";
            this.btnFileSaveAs.SubItemsExpandWidth = 24;
            this.btnFileSaveAs.Text = "Save As";
            // 
            // btnFileSaveAll
            // 
            this.btnFileSaveAll.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFileSaveAll.Image = ((System.Drawing.Image)(resources.GetObject("btnFileSaveAll.Image")));
            this.btnFileSaveAll.Name = "btnFileSaveAll";
            this.btnFileSaveAll.SubItemsExpandWidth = 24;
            this.btnFileSaveAll.Text = "Save All";
            // 
            // btnFilePrinterSetup
            // 
            this.btnFilePrinterSetup.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFilePrinterSetup.Image = ((System.Drawing.Image)(resources.GetObject("btnFilePrinterSetup.Image")));
            this.btnFilePrinterSetup.Name = "btnFilePrinterSetup";
            this.btnFilePrinterSetup.SubItemsExpandWidth = 24;
            this.btnFilePrinterSetup.Text = "Printer Setup";
            // 
            // btnFilePreview
            // 
            this.btnFilePreview.BeginGroup = true;
            this.btnFilePreview.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFilePreview.Image = ((System.Drawing.Image)(resources.GetObject("btnFilePreview.Image")));
            this.btnFilePreview.Name = "btnFilePreview";
            this.btnFilePreview.SubItemsExpandWidth = 24;
            this.btnFilePreview.Text = "Preview";
            // 
            // btnFileSelectLanguage
            // 
            this.btnFileSelectLanguage.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFileSelectLanguage.Image = ((System.Drawing.Image)(resources.GetObject("btnFileSelectLanguage.Image")));
            this.btnFileSelectLanguage.Name = "btnFileSelectLanguage";
            this.btnFileSelectLanguage.SubItemsExpandWidth = 24;
            this.btnFileSelectLanguage.Text = "Select Language";
            // 
            // btnFileClose
            // 
            this.btnFileClose.BeginGroup = true;
            this.btnFileClose.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFileClose.Image = ((System.Drawing.Image)(resources.GetObject("btnFileClose.Image")));
            this.btnFileClose.Name = "btnFileClose";
            this.btnFileClose.SubItemsExpandWidth = 24;
            this.btnFileClose.Text = "Close";
            // 
            // itemContainer23
            // 
            // 
            // 
            // 
            this.itemContainer23.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.itemContainer23.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer23.LayoutOrientation = FastReport.DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer23.MinimumSize = new System.Drawing.Size(150, 0);
            this.itemContainer23.Name = "itemContainer23";
            this.itemContainer23.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.lblRecent});
            // 
            // 
            // 
            this.itemContainer23.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // lblRecent
            // 
            this.lblRecent.BorderSide = FastReport.DevComponents.DotNetBar.eBorderSide.Bottom;
            this.lblRecent.BorderType = FastReport.DevComponents.DotNetBar.eBorderType.Etched;
            this.lblRecent.CanCustomize = false;
            this.lblRecent.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblRecent.Name = "lblRecent";
            this.lblRecent.PaddingBottom = 2;
            this.lblRecent.PaddingTop = 2;
            this.lblRecent.Stretch = true;
            this.lblRecent.Text = "Recent Documents";
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer4.BackgroundStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer4.HorizontalItemAlignment = FastReport.DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.itemContainer4.Name = "itemContainer4";
            this.itemContainer4.SubItems.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.btnOptions,
            this.btnFileExit});
            // 
            // 
            // 
            this.itemContainer4.TitleStyle.CornerType = FastReport.DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnOptions
            // 
            this.btnOptions.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnOptions.ColorTable = FastReport.DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOptions.Image")));
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.SubItemsExpandWidth = 24;
            this.btnOptions.Text = "Options";
            // 
            // btnFileExit
            // 
            this.btnFileExit.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFileExit.ColorTable = FastReport.DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnFileExit.Image = ((System.Drawing.Image)(resources.GetObject("btnFileExit.Image")));
            this.btnFileExit.Name = "btnFileExit";
            this.btnFileExit.SubItemsExpandWidth = 24;
            this.btnFileExit.Text = "Exit";
            // 
            // tabHome
            // 
            this.tabHome.Checked = true;
            this.tabHome.Name = "tabHome";
            this.tabHome.Panel = this.ribbonPanel1;
            this.tabHome.Text = "Home";
            // 
            // tabReport
            // 
            this.tabReport.Name = "tabReport";
            this.tabReport.Panel = this.ribbonPanel2;
            this.tabReport.Text = "Report";
            // 
            // tabLayout
            // 
            this.tabLayout.Name = "tabLayout";
            this.tabLayout.Panel = this.ribbonPanel3;
            this.tabLayout.Text = "Layout";
            // 
            // tabView
            // 
            this.tabView.Name = "tabView";
            this.tabView.Panel = this.ribbonPanel4;
            this.tabView.Text = "View";
            // 
            // buttonItem15
            // 
            this.buttonItem15.Name = "buttonItem15";
            this.buttonItem15.Text = "-->";
            // 
            // styleManager
            // 
            this.styleManager.ManagerStyle = FastReport.DevComponents.DotNetBar.eStyle.VisualStudio2012Light;
            this.styleManager.MetroColorParameters = new FastReport.DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // statusBar
            // 
            this.statusBar.AccessibleDescription = "DotNetBar Bar (statusBar)";
            this.statusBar.AccessibleName = "DotNetBar Bar";
            this.statusBar.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.statusBar.AntiAlias = true;
            this.statusBar.BarType = FastReport.DevComponents.DotNetBar.eBarType.StatusBar;
            this.statusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statusBar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.statusBar.GrabHandleStyle = FastReport.DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.statusBar.Items.AddRange(new FastReport.DevComponents.DotNetBar.BaseItem[] {
            this.location,
            this.size,
            this.text,
            this.zoom1,
            this.zoom2,
            this.zoom3,
            this.zoomLabel,
            this.slider});
            this.statusBar.Location = new System.Drawing.Point(0, 425);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(969, 26);
            this.statusBar.Stretch = true;
            this.statusBar.Style = FastReport.DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.statusBar.TabIndex = 1;
            this.statusBar.TabStop = false;
            // 
            // location
            // 
            this.location.Name = "location";
            this.location.Width = 160;
            // 
            // size
            // 
            this.size.Name = "size";
            this.size.Width = 160;
            // 
            // text
            // 
            this.text.Name = "text";
            // 
            // zoom1
            // 
            this.zoom1.ItemAlignment = FastReport.DevComponents.DotNetBar.eItemAlignment.Far;
            this.zoom1.Name = "zoom1";
            this.zoom1.Text = "[1]";
            // 
            // zoom2
            // 
            this.zoom2.ItemAlignment = FastReport.DevComponents.DotNetBar.eItemAlignment.Far;
            this.zoom2.Name = "zoom2";
            this.zoom2.Text = "[2]";
            // 
            // zoom3
            // 
            this.zoom3.ItemAlignment = FastReport.DevComponents.DotNetBar.eItemAlignment.Far;
            this.zoom3.Name = "zoom3";
            this.zoom3.Text = "[3]";
            // 
            // zoomLabel
            // 
            this.zoomLabel.ItemAlignment = FastReport.DevComponents.DotNetBar.eItemAlignment.Far;
            this.zoomLabel.Name = "zoomLabel";
            this.zoomLabel.Text = "100%";
            this.zoomLabel.TextAlignment = System.Drawing.StringAlignment.Far;
            this.zoomLabel.Width = 40;
            // 
            // slider
            // 
            this.slider.ItemAlignment = FastReport.DevComponents.DotNetBar.eItemAlignment.Far;
            this.slider.LabelVisible = false;
            this.slider.Maximum = 200;
            this.slider.Name = "slider";
            this.slider.Step = 5;
            this.slider.Value = 100;
            // 
            // btnHelp
            // 
            this.btnHelp.BeginGroup = true;
            this.btnHelp.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.SubItemsExpandWidth = 24;
            this.btnHelp.Text = "Help";
            // 
            // btnAbout
            // 
            this.btnAbout.ButtonStyle = FastReport.DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnAbout.Image")));
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.SubItemsExpandWidth = 24;
            this.btnAbout.Text = "About";
            // 
            // cbxStyles
            // 
            this.cbxStyles.ComboWidth = 100;
            this.cbxStyles.DropDownHeight = 300;
            this.cbxStyles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cbxStyles.DropDownWidth = 150;
            this.cbxStyles.ItemHeight = 14;
            this.cbxStyles.Name = "cbxStyles";
            this.cbxStyles.Report = null;
            this.cbxStyles.Style = "Select style";
            this.cbxStyles.Text = "Select style";
            // 
            // btnFillColor
            // 
            this.btnFillColor.Color = System.Drawing.Color.Transparent;
            this.btnFillColor.DefaultColor = System.Drawing.Color.Empty;
            this.btnFillColor.Name = "btnFillColor";
            this.btnFillColor.PopupType = FastReport.DevComponents.DotNetBar.ePopupType.ToolBar;
            this.btnFillColor.Text = "1";
            // 
            // btnLineColor
            // 
            this.btnLineColor.Color = System.Drawing.Color.Transparent;
            this.btnLineColor.DefaultColor = System.Drawing.Color.Empty;
            this.btnLineColor.Name = "btnLineColor";
            this.btnLineColor.PopupType = FastReport.DevComponents.DotNetBar.ePopupType.ToolBar;
            this.btnLineColor.Text = "3";
            // 
            // btnLineStyle
            // 
            this.btnLineStyle.AutoExpandOnClick = true;
            this.btnLineStyle.LineStyle = FastReport.LineStyle.Solid;
            this.btnLineStyle.Name = "btnLineStyle";
            this.btnLineStyle.PopupType = FastReport.DevComponents.DotNetBar.ePopupType.ToolBar;
            this.btnLineStyle.Text = "4";
            // 
            // btnLineWidth
            // 
            this.btnLineWidth.AutoExpandOnClick = true;
            this.btnLineWidth.LineWidth = 0F;
            this.btnLineWidth.Name = "btnLineWidth";
            this.btnLineWidth.PopupType = FastReport.DevComponents.DotNetBar.ePopupType.ToolBar;
            this.btnLineWidth.Text = "5";
            // 
            // cbxFontName
            // 
            this.cbxFontName.ComboWidth = 103;
            this.cbxFontName.DropDownHeight = 300;
            this.cbxFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cbxFontName.DropDownWidth = 370;
            this.cbxFontName.ItemHeight = 14;
            this.cbxFontName.Name = "cbxFontName";
            // 
            // cbxFontSize
            // 
            this.cbxFontSize.ComboWidth = 50;
            this.cbxFontSize.DropDownHeight = 300;
            this.cbxFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cbxFontSize.ItemHeight = 14;
            this.cbxFontSize.Name = "cbxFontSize";
            this.cbxFontSize.Text = "0";
            // 
            // btnTextColor
            // 
            this.btnTextColor.BeginGroup = true;
            this.btnTextColor.Color = System.Drawing.Color.Transparent;
            this.btnTextColor.DefaultColor = System.Drawing.Color.Black;
            this.btnTextColor.Image = ((System.Drawing.Image)(resources.GetObject("btnTextColor.Image")));
            this.btnTextColor.Name = "btnTextColor";
            this.btnTextColor.PopupType = FastReport.DevComponents.DotNetBar.ePopupType.ToolBar;
            this.btnTextColor.Text = "A";
            // 
            // DesignerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 451);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.ribbonControl);
            this.Name = "DesignerForm";
            this.Text = "DesignerRibbonForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DesignerForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DesignerForm_FormClosed);
            this.Load += new System.EventHandler(this.DesignerForm_Load);
            this.ribbonControl.ResumeLayout(false);
            this.ribbonControl.PerformLayout();
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel4.ResumeLayout(false);
            this.ribbonPanel3.ResumeLayout(false);
            this.ribbonPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statusBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private FastReport.DevComponents.DotNetBar.RibbonBar barEllis;

        private FastReport.DevComponents.DotNetBar.RibbonControl ribbonControl;
        private FastReport.DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private FastReport.DevComponents.DotNetBar.RibbonBar barUndo;
        private FastReport.DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private FastReport.DevComponents.DotNetBar.ApplicationButton btnFile;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer1;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer2;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer3;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFileNew;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFileOpen;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFileSave;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFileSelectLanguage;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFilePrinterSetup;
        private FastReport.DevComponents.DotNetBar.LabelItem lblRecent;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer4;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnOptions;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFileExit;
        private FastReport.DevComponents.DotNetBar.RibbonTabItem tabHome;
        private FastReport.DevComponents.DotNetBar.RibbonTabItem tabReport;
        private FastReport.DevComponents.DotNetBar.StyleManager styleManager;
        private FastReport.DevComponents.DotNetBar.Bar statusBar;
        private FastReport.DevComponents.DotNetBar.ButtonItem buttonItem15;
        private FastReport.DevComponents.DotNetBar.LabelItem zoomLabel;
        private FastReport.DevComponents.DotNetBar.SliderItem slider;
        private FastReport.DevComponents.DotNetBar.ButtonItem zoom1;
        private FastReport.DevComponents.DotNetBar.ButtonItem zoom2;
        private FastReport.DevComponents.DotNetBar.ButtonItem zoom3;
        private FastReport.DevComponents.DotNetBar.LabelItem location;
        private FastReport.DevComponents.DotNetBar.LabelItem size;
        private FastReport.DevComponents.DotNetBar.LabelItem text;
        private FastReport.DevComponents.DotNetBar.RibbonBar barClipboard;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer5;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnUndo;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnRedo;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnPaste;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer6;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnCopy;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnCut;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFormatPainter;
        private FastReport.DevComponents.DotNetBar.RibbonBar barText;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer7;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer8;
        private FastReport.Controls.FontComboBoxItem cbxFontName;
        private FastReport.Controls.FontSizeComboBoxItem cbxFontSize;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer9;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnBold;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnItalic;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnUnderline;
        private FastReport.DevComponents.DotNetBar.RibbonBar barBorderAndFill;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignLeft;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignCenter;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignRight;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnJustify;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignTop;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignMiddle;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignBottom;
        private FastReport.Controls.ColorButtonItem btnTextColor;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnTextRotation;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer10;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer11;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAllLines;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnNoLines;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnTopLine;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnBottomLine;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnLeftLine;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnRightLine;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnBorderProps;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer12;
        private FastReport.Controls.ColorButtonItem btnFillColor;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFillProps;
        private FastReport.Controls.ColorButtonItem btnLineColor;
        private FastReport.Controls.LineStyleButtonItem btnLineStyle;
        private FastReport.Controls.LineWidthButtonItem btnLineWidth;
        private FastReport.DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private FastReport.DevComponents.DotNetBar.RibbonPanel ribbonPanel4;
        private FastReport.DevComponents.DotNetBar.RibbonTabItem tabLayout;
        private FastReport.DevComponents.DotNetBar.RibbonTabItem tabView;
        private FastReport.DevComponents.DotNetBar.RibbonBar barFormat;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFormat;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnHighlight;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer13;
        private FastReport.DevComponents.DotNetBar.RibbonBar barEditing;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer14;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFind;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnReplace;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSelectAll;
        private FastReport.DevComponents.DotNetBar.RibbonBar barStyles;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer15;
        private FastReport.Controls.StyleComboBoxItem cbxStyles;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnStyles;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer16;
        private FastReport.DevComponents.DotNetBar.RibbonBar barReport;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnReportOptions;
        private FastReport.DevComponents.DotNetBar.RibbonBar barBands;
        private FastReport.DevComponents.DotNetBar.RibbonBar barPages;
        private FastReport.DevComponents.DotNetBar.RibbonBar barData;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnDataChoose;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnDataAdd;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer17;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAddPage;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAddDialog;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnDeletePage;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnPageSetup;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnConfigureBands;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnGroupExpert;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer18;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer19;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer20;
        private FastReport.DevComponents.DotNetBar.CheckBoxItem btnReportTitle;
        private FastReport.DevComponents.DotNetBar.CheckBoxItem btnReportSummary;
        private FastReport.DevComponents.DotNetBar.CheckBoxItem btnPageHeader;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer21;
        private FastReport.DevComponents.DotNetBar.CheckBoxItem btnColumnHeader;
        private FastReport.DevComponents.DotNetBar.CheckBoxItem btnColumnFooter;
        private FastReport.DevComponents.DotNetBar.CheckBoxItem btnOverlay;
        private FastReport.DevComponents.DotNetBar.CheckBoxItem btnPageFooter;
        private FastReport.DevComponents.DotNetBar.RibbonBar barView;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnViewGrid;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnViewGuides;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer22;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAutoGuides;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnDeleteHGuides;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnDeleteVGuides;
        private FastReport.DevComponents.DotNetBar.RibbonBar barLayout;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignToGrid;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFitToGrid;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignment;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignLefts;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignCenters;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignRights;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignTops;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignMiddles;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAlignBottoms;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnCenterHorizontally;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnCenterVertically;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSize;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSameWidth;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSameHeight;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSameSize;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSpacing;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSpaceHorizontally;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnIncreaseHorizontalSpacing;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnDecreaseHorizontalSpacing;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnRemoveHorizontalSpacing;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSpaceVertically;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnIncreaseVerticalSpacing;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnDecreaseVerticalSpacing;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnRemoveVerticalSpacing;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnBringToFront;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnSendToBack;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnGroup;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnUngroup;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnPreview;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnPanels;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnProperties;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnData;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnReportTree;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnMessages;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnUnits;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnUnitsMillimeters;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnUnitsCentimeters;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnUnitsInches;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnUnitsHundrethsOfInch;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFileSaveAs;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFileSaveAll;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFilePreview;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnFileClose;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer23;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnHelp;
        private FastReport.DevComponents.DotNetBar.ButtonItem btnAbout;
        private FastReport.DevComponents.DotNetBar.ItemContainer itemContainer24;
    }
}