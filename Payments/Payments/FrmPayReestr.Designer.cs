﻿namespace Payments
{
    partial class FrmPayReestr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpBeginDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.btnSelectPacks = new System.Windows.Forms.Button();
            this.btnPayPacks = new System.Windows.Forms.Button();
            this.dgvPacks = new System.Windows.Forms.DataGridView();
            this.paymentsDataSet = new Payments.PaymentsDataSet();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpBeginDate
            // 
            this.dtpBeginDate.Location = new System.Drawing.Point(23, 23);
            this.dtpBeginDate.Name = "dtpBeginDate";
            this.dtpBeginDate.Size = new System.Drawing.Size(200, 20);
            this.dtpBeginDate.TabIndex = 0;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Location = new System.Drawing.Point(23, 60);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(200, 20);
            this.dtpEndDate.TabIndex = 1;
            // 
            // btnSelectPacks
            // 
            this.btnSelectPacks.Location = new System.Drawing.Point(242, 57);
            this.btnSelectPacks.Name = "btnSelectPacks";
            this.btnSelectPacks.Size = new System.Drawing.Size(109, 23);
            this.btnSelectPacks.TabIndex = 2;
            this.btnSelectPacks.Text = "Выбрать пачки";
            this.btnSelectPacks.UseVisualStyleBackColor = true;
            this.btnSelectPacks.Click += new System.EventHandler(this.btnSelectPacks_Click);
            // 
            // btnPayPacks
            // 
            this.btnPayPacks.Location = new System.Drawing.Point(613, 61);
            this.btnPayPacks.Name = "btnPayPacks";
            this.btnPayPacks.Size = new System.Drawing.Size(136, 23);
            this.btnPayPacks.TabIndex = 3;
            this.btnPayPacks.Text = "Провести через кассу";
            this.btnPayPacks.UseVisualStyleBackColor = true;
            this.btnPayPacks.Click += new System.EventHandler(this.btnPayPacks_Click);
            // 
            // dgvPacks
            // 
            this.dgvPacks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPacks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPacks.Location = new System.Drawing.Point(12, 90);
            this.dgvPacks.Name = "dgvPacks";
            this.dgvPacks.Size = new System.Drawing.Size(737, 377);
            this.dgvPacks.TabIndex = 4;
            // 
            // paymentsDataSet
            // 
            this.paymentsDataSet.DataSetName = "PaymentsDataSet";
            this.paymentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // FrmPayReestr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 518);
            this.Controls.Add(this.dgvPacks);
            this.Controls.Add(this.btnPayPacks);
            this.Controls.Add(this.btnSelectPacks);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.dtpBeginDate);
            this.Name = "FrmPayReestr";
            this.Text = "FrmPayReestr";
            this.Load += new System.EventHandler(this.FrmPayReestr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpBeginDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Button btnSelectPacks;
        private System.Windows.Forms.Button btnPayPacks;
        private System.Windows.Forms.DataGridView dgvPacks;
        private PaymentsDataSet paymentsDataSet;
    }
}