﻿namespace MrivcControls
{
    partial class FrmPrintHistoryChanges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrintHistoryChanges));
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Print = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riEpmloyee = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.employeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cDataSet = new MrivcControls.Data.CDataSet();
            this.colPrint_Count = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Bill_Kind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Debt_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintedBackSide = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssued_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Delivery_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChangedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChangeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.baseGrid1 = new MrivcControls.BaseGrid();
            this.printHistoryLogBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cardView1 = new DevExpress.XtraGrid.Views.Card.CardView();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riEpmloyee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printHistoryLogBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Print,
            this.colPeriod,
            this.colPrintDate,
            this.colPrintBy,
            this.colPrint_Count,
            this.colID_Client,
            this.colID_Bill_Kind,
            this.colID_Debt_Company,
            this.colPrintedBackSide,
            this.colChecked,
            this.colCheckedBy,
            this.colIssued_Date,
            this.colIssuedBy,
            this.colID_Delivery_Type,
            this.colComment,
            this.colChangedBy,
            this.colChangeDate});
            this.gridView1.GridControl = this.baseGrid1;
            this.gridView1.Name = "gridView1";
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colChangeDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID_Print
            // 
            this.colID_Print.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Print.AppearanceCell.Options.UseBackColor = true;
            this.colID_Print.FieldName = "ID_Print";
            this.colID_Print.Name = "colID_Print";
            this.colID_Print.OptionsColumn.AllowEdit = false;
            this.colID_Print.OptionsColumn.ReadOnly = true;
            // 
            // colPeriod
            // 
            this.colPeriod.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPeriod.AppearanceCell.Options.UseBackColor = true;
            this.colPeriod.FieldName = "Period";
            this.colPeriod.Name = "colPeriod";
            this.colPeriod.OptionsColumn.AllowEdit = false;
            this.colPeriod.OptionsColumn.ReadOnly = true;
            // 
            // colPrintDate
            // 
            this.colPrintDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrintDate.AppearanceCell.Options.UseBackColor = true;
            this.colPrintDate.Caption = "Дата печати";
            this.colPrintDate.FieldName = "PrintDate";
            this.colPrintDate.Name = "colPrintDate";
            this.colPrintDate.OptionsColumn.AllowEdit = false;
            this.colPrintDate.OptionsColumn.ReadOnly = true;
            this.colPrintDate.Visible = true;
            this.colPrintDate.VisibleIndex = 0;
            this.colPrintDate.Width = 112;
            // 
            // colPrintBy
            // 
            this.colPrintBy.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrintBy.AppearanceCell.Options.UseBackColor = true;
            this.colPrintBy.Caption = "Сотрудник";
            this.colPrintBy.ColumnEdit = this.riEpmloyee;
            this.colPrintBy.FieldName = "PrintBy";
            this.colPrintBy.Name = "colPrintBy";
            this.colPrintBy.OptionsColumn.AllowEdit = false;
            this.colPrintBy.OptionsColumn.ReadOnly = true;
            this.colPrintBy.Visible = true;
            this.colPrintBy.VisibleIndex = 1;
            this.colPrintBy.Width = 270;
            // 
            // riEpmloyee
            // 
            this.riEpmloyee.AutoHeight = false;
            this.riEpmloyee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riEpmloyee.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Employee", "ID_Employee", 86, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Employee_First_Name", "Employee_First_Name", 116, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Employee_Second_Name", "Employee_Second_Name", 130, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Employee_Last_Name", "Employee_Last_Name", 115, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FIO", "FIO", 28, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riEpmloyee.DataSource = this.employeeBindingSource;
            this.riEpmloyee.DisplayMember = "FIO";
            this.riEpmloyee.Name = "riEpmloyee";
            this.riEpmloyee.ValueMember = "ID_Employee";
            // 
            // employeeBindingSource
            // 
            this.employeeBindingSource.DataMember = "Employee";
            this.employeeBindingSource.DataSource = this.cDataSet;
            // 
            // cDataSet
            // 
            this.cDataSet.DataSetName = "CDataSet";
            this.cDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colPrint_Count
            // 
            this.colPrint_Count.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrint_Count.AppearanceCell.Options.UseBackColor = true;
            this.colPrint_Count.Caption = "Количество квитанций";
            this.colPrint_Count.FieldName = "Print_Count";
            this.colPrint_Count.Name = "colPrint_Count";
            this.colPrint_Count.OptionsColumn.AllowEdit = false;
            this.colPrint_Count.OptionsColumn.ReadOnly = true;
            this.colPrint_Count.Width = 92;
            // 
            // colID_Client
            // 
            this.colID_Client.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Client.AppearanceCell.Options.UseBackColor = true;
            this.colID_Client.FieldName = "ID_Client";
            this.colID_Client.Name = "colID_Client";
            this.colID_Client.OptionsColumn.AllowEdit = false;
            this.colID_Client.OptionsColumn.ReadOnly = true;
            // 
            // colID_Bill_Kind
            // 
            this.colID_Bill_Kind.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Bill_Kind.AppearanceCell.Options.UseBackColor = true;
            this.colID_Bill_Kind.FieldName = "ID_Bill_Kind";
            this.colID_Bill_Kind.Name = "colID_Bill_Kind";
            this.colID_Bill_Kind.OptionsColumn.AllowEdit = false;
            this.colID_Bill_Kind.OptionsColumn.ReadOnly = true;
            this.colID_Bill_Kind.Width = 94;
            // 
            // colID_Debt_Company
            // 
            this.colID_Debt_Company.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Debt_Company.AppearanceCell.Options.UseBackColor = true;
            this.colID_Debt_Company.FieldName = "ID_Debt_Company";
            this.colID_Debt_Company.Name = "colID_Debt_Company";
            this.colID_Debt_Company.OptionsColumn.AllowEdit = false;
            this.colID_Debt_Company.OptionsColumn.ReadOnly = true;
            // 
            // colPrintedBackSide
            // 
            this.colPrintedBackSide.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colPrintedBackSide.AppearanceCell.Options.UseBackColor = true;
            this.colPrintedBackSide.Caption = "Печать обратной стороны";
            this.colPrintedBackSide.FieldName = "PrintedBackSide";
            this.colPrintedBackSide.Name = "colPrintedBackSide";
            this.colPrintedBackSide.OptionsColumn.AllowEdit = false;
            this.colPrintedBackSide.OptionsColumn.ReadOnly = true;
            this.colPrintedBackSide.Visible = true;
            this.colPrintedBackSide.VisibleIndex = 2;
            // 
            // colChecked
            // 
            this.colChecked.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colChecked.AppearanceCell.Options.UseBackColor = true;
            this.colChecked.Caption = "Проверено";
            this.colChecked.FieldName = "Checked";
            this.colChecked.Name = "colChecked";
            this.colChecked.OptionsColumn.AllowEdit = false;
            this.colChecked.OptionsColumn.ReadOnly = true;
            this.colChecked.Visible = true;
            this.colChecked.VisibleIndex = 3;
            this.colChecked.Width = 117;
            // 
            // colCheckedBy
            // 
            this.colCheckedBy.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colCheckedBy.AppearanceCell.Options.UseBackColor = true;
            this.colCheckedBy.FieldName = "CheckedBy";
            this.colCheckedBy.Name = "colCheckedBy";
            this.colCheckedBy.OptionsColumn.AllowEdit = false;
            this.colCheckedBy.OptionsColumn.ReadOnly = true;
            // 
            // colIssued_Date
            // 
            this.colIssued_Date.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colIssued_Date.AppearanceCell.Options.UseBackColor = true;
            this.colIssued_Date.Caption = "Выданы клиенту";
            this.colIssued_Date.FieldName = "Issued_Date";
            this.colIssued_Date.Name = "colIssued_Date";
            this.colIssued_Date.OptionsColumn.AllowEdit = false;
            this.colIssued_Date.OptionsColumn.ReadOnly = true;
            this.colIssued_Date.Visible = true;
            this.colIssued_Date.VisibleIndex = 4;
            this.colIssued_Date.Width = 125;
            // 
            // colIssuedBy
            // 
            this.colIssuedBy.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colIssuedBy.AppearanceCell.Options.UseBackColor = true;
            this.colIssuedBy.FieldName = "IssuedBy";
            this.colIssuedBy.Name = "colIssuedBy";
            this.colIssuedBy.OptionsColumn.AllowEdit = false;
            this.colIssuedBy.OptionsColumn.ReadOnly = true;
            // 
            // colID_Delivery_Type
            // 
            this.colID_Delivery_Type.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Delivery_Type.AppearanceCell.Options.UseBackColor = true;
            this.colID_Delivery_Type.Caption = "Способ доставки";
            this.colID_Delivery_Type.FieldName = "ID_Delivery_Type";
            this.colID_Delivery_Type.Name = "colID_Delivery_Type";
            this.colID_Delivery_Type.OptionsColumn.AllowEdit = false;
            this.colID_Delivery_Type.OptionsColumn.ReadOnly = true;
            this.colID_Delivery_Type.Visible = true;
            this.colID_Delivery_Type.VisibleIndex = 5;
            this.colID_Delivery_Type.Width = 119;
            // 
            // colComment
            // 
            this.colComment.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colComment.AppearanceCell.Options.UseBackColor = true;
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.OptionsColumn.ReadOnly = true;
            // 
            // colChangedBy
            // 
            this.colChangedBy.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colChangedBy.AppearanceCell.Options.UseBackColor = true;
            this.colChangedBy.Caption = "Сотрудник сделавший изменения";
            this.colChangedBy.ColumnEdit = this.riEpmloyee;
            this.colChangedBy.FieldName = "ChangedBy";
            this.colChangedBy.Name = "colChangedBy";
            this.colChangedBy.OptionsColumn.AllowEdit = false;
            this.colChangedBy.OptionsColumn.ReadOnly = true;
            this.colChangedBy.Visible = true;
            this.colChangedBy.VisibleIndex = 6;
            this.colChangedBy.Width = 220;
            // 
            // colChangeDate
            // 
            this.colChangeDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colChangeDate.AppearanceCell.Options.UseBackColor = true;
            this.colChangeDate.Caption = "Дата изменения";
            this.colChangeDate.FieldName = "ChangeDate";
            this.colChangeDate.Name = "colChangeDate";
            this.colChangeDate.OptionsColumn.AllowEdit = false;
            this.colChangeDate.OptionsColumn.ReadOnly = true;
            this.colChangeDate.Visible = true;
            this.colChangeDate.VisibleIndex = 7;
            this.colChangeDate.Width = 149;
            // 
            // baseGrid1
            // 
            this.baseGrid1.DataSource = this.printHistoryLogBindingSource;
            this.baseGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baseGrid1.Location = new System.Drawing.Point(0, 0);
            this.baseGrid1.MainView = this.gridView1;
            this.baseGrid1.Name = "baseGrid1";
            this.baseGrid1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riEpmloyee});
            this.baseGrid1.Size = new System.Drawing.Size(1183, 506);
            this.baseGrid1.TabIndex = 0;
            this.baseGrid1.TrimCellValueBeforeValidate = true;
            this.baseGrid1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.cardView1});
            this.baseGrid1.Load += new System.EventHandler(this.baseGrid1_Load);
            // 
            // printHistoryLogBindingSource
            // 
            this.printHistoryLogBindingSource.DataMember = "Print_History_Log";
            this.printHistoryLogBindingSource.DataSource = this.cDataSet;
            // 
            // cardView1
            // 
            this.cardView1.FocusedCardTopFieldIndex = 0;
            this.cardView1.GridControl = this.baseGrid1;
            this.cardView1.Name = "cardView1";
            // 
            // FrmPrintHistoryChanges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1183, 506);
            this.Controls.Add(this.baseGrid1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrintHistoryChanges";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPrintHistoryChanges";
            this.Load += new System.EventHandler(this.FrmPrintHistoryChanges_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riEpmloyee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printHistoryLogBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private BaseGrid baseGrid1;
        private DevExpress.XtraGrid.Views.Card.CardView cardView1;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Print;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintBy;
        private DevExpress.XtraGrid.Columns.GridColumn colPrint_Count;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Bill_Kind;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Debt_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintedBackSide;
        private DevExpress.XtraGrid.Columns.GridColumn colChecked;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colIssued_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Delivery_Type;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colChangedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colChangeDate;
        private System.Windows.Forms.BindingSource printHistoryLogBindingSource;
        private Data.CDataSet cDataSet;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riEpmloyee;
        private System.Windows.Forms.BindingSource employeeBindingSource;

    }
}