﻿namespace WindowsFormsApplication1.DS
{
    partial class DModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            this.mainConnection = new System.Data.SqlClient.SqlConnection();
            this.daClientDetailBank = new System.Data.SqlClient.SqlDataAdapter();
            this.cmdAnyCommand = new System.Data.SqlClient.SqlCommand();
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "town.Get_Client_Detail_Bank_All";
            this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand1.Connection = this.mainConnection;
            this.sqlSelectCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.SqlClient.SqlParameter("@Period", System.Data.SqlDbType.Int, 4)});
            // 
            // mainConnection
            // 
            this.mainConnection.ConnectionString = "Data Source=srv-mrivc7;Initial Catalog=Murmansk;User ID=bobrovsky";
            this.mainConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // daClientDetailBank
            // 
            this.daClientDetailBank.SelectCommand = this.sqlSelectCommand1;
            this.daClientDetailBank.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Get_Client_Detail_Bank_All", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID_Derive_Company", "ID_Derive_Company"),
                        new System.Data.Common.DataColumnMapping("ID_Bill_Filter", "ID_Bill_Filter"),
                        new System.Data.Common.DataColumnMapping("Client_Code", "Client_Code"),
                        new System.Data.Common.DataColumnMapping("Client_Name", "Client_Name"),
                        new System.Data.Common.DataColumnMapping("Settlement", "Settlement"),
                        new System.Data.Common.DataColumnMapping("Derive_Company_Name", "Derive_Company_Name"),
                        new System.Data.Common.DataColumnMapping("Derive_Company_Code", "Derive_Company_Code"),
                        new System.Data.Common.DataColumnMapping("Bill_Filter_Code", "Bill_Filter_Code"),
                        new System.Data.Common.DataColumnMapping("Bill_Filter_Name", "Bill_Filter_Name")})});
            // 
            // cmdAnyCommand
            // 
            this.cmdAnyCommand.Connection = this.mainConnection;

        }

        #endregion

        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private System.Data.SqlClient.SqlConnection mainConnection;
        private System.Data.SqlClient.SqlDataAdapter daClientDetailBank;
        private System.Data.SqlClient.SqlCommand cmdAnyCommand;
    }
}
