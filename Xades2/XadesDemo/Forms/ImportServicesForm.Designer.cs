﻿namespace XadesDemo
{
    partial class ImportServicesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportServicesForm));
            this.prepareBtn = new System.Windows.Forms.Button();
            this.importBtn = new System.Windows.Forms.Button();
            this.municipalServicesDataGrid = new System.Windows.Forms.DataGridView();
            this.additionalServicesDataGrid = new System.Windows.Forms.DataGridView();
            this.recoverBtn = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.municipalServicesDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.additionalServicesDataGrid)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // prepareBtn
            // 
            this.prepareBtn.Location = new System.Drawing.Point(320, 12);
            this.prepareBtn.Name = "prepareBtn";
            this.prepareBtn.Size = new System.Drawing.Size(162, 23);
            this.prepareBtn.TabIndex = 1;
            this.prepareBtn.Text = "Просмотр";
            this.prepareBtn.UseVisualStyleBackColor = true;
            this.prepareBtn.Click += new System.EventHandler(this.prepareBtn_Click);
            // 
            // importBtn
            // 
            this.importBtn.Enabled = false;
            this.importBtn.Location = new System.Drawing.Point(320, 41);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(162, 23);
            this.importBtn.TabIndex = 2;
            this.importBtn.Text = "Импорт";
            this.importBtn.UseVisualStyleBackColor = true;
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // municipalServicesDataGrid
            // 
            this.municipalServicesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.municipalServicesDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.municipalServicesDataGrid.Location = new System.Drawing.Point(3, 3);
            this.municipalServicesDataGrid.Name = "municipalServicesDataGrid";
            this.municipalServicesDataGrid.Size = new System.Drawing.Size(737, 376);
            this.municipalServicesDataGrid.TabIndex = 3;
            // 
            // additionalServicesDataGrid
            // 
            this.additionalServicesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.additionalServicesDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalServicesDataGrid.Location = new System.Drawing.Point(3, 3);
            this.additionalServicesDataGrid.Name = "additionalServicesDataGrid";
            this.additionalServicesDataGrid.Size = new System.Drawing.Size(737, 376);
            this.additionalServicesDataGrid.TabIndex = 4;
            // 
            // recoverBtn
            // 
            this.recoverBtn.Location = new System.Drawing.Point(488, 41);
            this.recoverBtn.Name = "recoverBtn";
            this.recoverBtn.Size = new System.Drawing.Size(163, 23);
            this.recoverBtn.TabIndex = 5;
            this.recoverBtn.Text = "Восстановить";
            this.recoverBtn.UseVisualStyleBackColor = true;
            this.recoverBtn.Click += new System.EventHandler(this.recoverBtn_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 73);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(751, 408);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.municipalServicesDataGrid);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(743, 382);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Коммунальные";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.additionalServicesDataGrid);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(743, 382);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Дополнительные";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ImportServicesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 481);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.recoverBtn);
            this.Controls.Add(this.importBtn);
            this.Controls.Add(this.prepareBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportServicesForm";
            this.Text = "Синхронизация услуг";
            this.Load += new System.EventHandler(this.ImportServicesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.municipalServicesDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.additionalServicesDataGrid)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button prepareBtn;
        private System.Windows.Forms.Button importBtn;
        private System.Windows.Forms.DataGridView municipalServicesDataGrid;
        private System.Windows.Forms.DataGridView additionalServicesDataGrid;
        private System.Windows.Forms.Button recoverBtn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}