﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Payments.DS;
using System.Data.SqlClient;
using System.IO;

namespace Payments.Controls
{
    public partial class GridPaymentPoverka : UserControl
    {
        public GridPaymentPoverka()
        {
            InitializeComponent();
        }

        DataTable SelectReestrDataService (int type, int id_payment_class)
        {
            var beginDate = dtpBegin.Value.Date;
            var endDate = dtpEnd.Value.Date;
            DAccess.DataModule.Connection1 = new SqlConnection(Properties.Settings.Default.PaymentsConnectionString);
            return DAccess.DataModule.GetDataTableByStoredProcedure1("dbo.SelectReestrDataPoverka", new object[,] { { "beginDate", beginDate }, { "endDate", endDate }, { "type", type }, { "id_payment_class", id_payment_class }/*, { "user", comboBox1.SelectedValue.ToString() }*/ });
        }

        int IdPaymentClass()
        {
            if (cbService.SelectedItem.ToString() == "Поверка инд. приборов учета воды")
            {
                return 4;
            }
            else if (cbService.SelectedItem.ToString() == "Алые Паруса")
            {
                return 6;
            }
            else if (cbService.SelectedItem.ToString() == "Сервис ЖКХ")
            {
                return 7;
            }

            return -1;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var dt = SelectReestrDataService(0, IdPaymentClass());
            dataGridView1.DataSource = dt;
            dataGridView1.Refresh();
        }

        private void btnSaveToFile_Click(object sender, EventArgs e)
        {
            var dt = SelectReestrDataService(1, IdPaymentClass());

            var date = DateTime.Now;
            var year = date.Year.ToString();
            var month = date.Month.ToString();
            var day = date.Day.ToString();

            var h = date.Hour.ToString();
            var m = date.Minute.ToString();
            var s = date.Second.ToString();

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {

                string text = "";
                foreach (DataRow row in dt.Rows)
                {
                    foreach (var col in row.ItemArray)
                    {
                        text += col.ToString() + ';';
                    }
                    text += Environment.NewLine;
                }
                File.WriteAllText(folderBrowserDialog1.SelectedPath + "\\rirc2csm_" + year + month + day + "-" + h + m + s + ".csv", text,Encoding.GetEncoding(1251));
            }

        }

        private void GridPaymentPoverka_Load(object sender, EventArgs e)
        {
            var date = DateTime.Today;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            dtpBegin.Value = firstDayOfMonth;
            dtpEnd.Value = lastDayOfMonth;

            DAccess.DataModule.Connection1 = new SqlConnection(Properties.Settings.Default.PaymentsConnectionString);
            var dt = DAccess.DataModule.GetDataTableByQuery1("select * from Program.[User]");
            comboBox1.DataSource = dt;
            comboBox1.Refresh();
        }
    }
}
