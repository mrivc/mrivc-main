﻿namespace GisGkh
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.integrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportNsiMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.эспортДоговоровУправленияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортВБДToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TotalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отозватьПлатежныеДокументыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.закрытьЛицевыеСчетаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьДополнительныеУслугиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузкаИндивидуальныхУслугToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетОВыполненныхКомандахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LogSaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.integrationToolStripMenuItem,
            this.импортВБДToolStripMenuItem,
            this.serviceToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip.Size = new System.Drawing.Size(2187, 35);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // integrationToolStripMenuItem
            // 
            this.integrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportNsiMenuItem,
            this.эспортДоговоровУправленияToolStripMenuItem});
            this.integrationToolStripMenuItem.Name = "integrationToolStripMenuItem";
            this.integrationToolStripMenuItem.Size = new System.Drawing.Size(193, 29);
            this.integrationToolStripMenuItem.Text = "Экспорт из ГИС ЖКХ";
            // 
            // exportNsiMenuItem
            // 
            this.exportNsiMenuItem.Name = "exportNsiMenuItem";
            this.exportNsiMenuItem.Size = new System.Drawing.Size(391, 30);
            this.exportNsiMenuItem.Text = "Экспорт справочников НСИ и услуг";
            this.exportNsiMenuItem.Click += new System.EventHandler(this.exportNsiMenuItem_Click);
            // 
            // эспортДоговоровУправленияToolStripMenuItem
            // 
            this.эспортДоговоровУправленияToolStripMenuItem.Name = "эспортДоговоровУправленияToolStripMenuItem";
            this.эспортДоговоровУправленияToolStripMenuItem.Size = new System.Drawing.Size(391, 30);
            this.эспортДоговоровУправленияToolStripMenuItem.Text = "Эспорт договоров управления";
            this.эспортДоговоровУправленияToolStripMenuItem.Click += new System.EventHandler(this.эспортДоговоровУправленияToolStripMenuItem_Click);
            // 
            // импортВБДToolStripMenuItem
            // 
            this.импортВБДToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TotalToolStripMenuItem,
            this.отозватьПлатежныеДокументыToolStripMenuItem,
            this.закрытьЛицевыеСчетаToolStripMenuItem,
            this.удалитьДополнительныеУслугиToolStripMenuItem,
            this.загрузкаИндивидуальныхУслугToolStripMenuItem,
            this.debtToolStripMenuItem});
            this.импортВБДToolStripMenuItem.Name = "импортВБДToolStripMenuItem";
            this.импортВБДToolStripMenuItem.Size = new System.Drawing.Size(152, 29);
            this.импортВБДToolStripMenuItem.Text = "Синхронизация";
            // 
            // TotalToolStripMenuItem
            // 
            this.TotalToolStripMenuItem.Name = "TotalToolStripMenuItem";
            this.TotalToolStripMenuItem.Size = new System.Drawing.Size(361, 30);
            this.TotalToolStripMenuItem.Text = "Общая синхронизация";
            this.TotalToolStripMenuItem.Click += new System.EventHandler(this.TotalToolStripMenuItem_Click);
            // 
            // отозватьПлатежныеДокументыToolStripMenuItem
            // 
            this.отозватьПлатежныеДокументыToolStripMenuItem.Name = "отозватьПлатежныеДокументыToolStripMenuItem";
            this.отозватьПлатежныеДокументыToolStripMenuItem.Size = new System.Drawing.Size(361, 30);
            this.отозватьПлатежныеДокументыToolStripMenuItem.Text = "Отозвать платежные документы";
            this.отозватьПлатежныеДокументыToolStripMenuItem.Click += new System.EventHandler(this.отозватьПлатежныеДокументыToolStripMenuItem_Click);
            // 
            // закрытьЛицевыеСчетаToolStripMenuItem
            // 
            this.закрытьЛицевыеСчетаToolStripMenuItem.Name = "закрытьЛицевыеСчетаToolStripMenuItem";
            this.закрытьЛицевыеСчетаToolStripMenuItem.Size = new System.Drawing.Size(361, 30);
            this.закрытьЛицевыеСчетаToolStripMenuItem.Text = "Закрыть лицевые счета";
            this.закрытьЛицевыеСчетаToolStripMenuItem.Click += new System.EventHandler(this.закрытьЛицевыеСчетаToolStripMenuItem_Click);
            // 
            // удалитьДополнительныеУслугиToolStripMenuItem
            // 
            this.удалитьДополнительныеУслугиToolStripMenuItem.Name = "удалитьДополнительныеУслугиToolStripMenuItem";
            this.удалитьДополнительныеУслугиToolStripMenuItem.Size = new System.Drawing.Size(361, 30);
            this.удалитьДополнительныеУслугиToolStripMenuItem.Text = "Удалить дополнительные услуги";
            this.удалитьДополнительныеУслугиToolStripMenuItem.Click += new System.EventHandler(this.удалитьДополнительныеУслугиToolStripMenuItem_Click);
            // 
            // загрузкаИндивидуальныхУслугToolStripMenuItem
            // 
            this.загрузкаИндивидуальныхУслугToolStripMenuItem.Name = "загрузкаИндивидуальныхУслугToolStripMenuItem";
            this.загрузкаИндивидуальныхУслугToolStripMenuItem.Size = new System.Drawing.Size(361, 30);
            this.загрузкаИндивидуальныхУслугToolStripMenuItem.Text = "Загрузка индивидуальных услуг";
            this.загрузкаИндивидуальныхУслугToolStripMenuItem.Click += new System.EventHandler(this.загрузкаИндивидуальныхУслугToolStripMenuItem_Click);
            // 
            // serviceToolStripMenuItem
            // 
            this.serviceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отчетОВыполненныхКомандахToolStripMenuItem,
            this.LogSaveToolStripMenuItem,
            this.clearToolStripMenuItem,
            this.testToolStripMenuItem});
            this.serviceToolStripMenuItem.Name = "serviceToolStripMenuItem";
            this.serviceToolStripMenuItem.Size = new System.Drawing.Size(83, 29);
            this.serviceToolStripMenuItem.Text = "Сервис";
            // 
            // отчетОВыполненныхКомандахToolStripMenuItem
            // 
            this.отчетОВыполненныхКомандахToolStripMenuItem.Name = "отчетОВыполненныхКомандахToolStripMenuItem";
            this.отчетОВыполненныхКомандахToolStripMenuItem.Size = new System.Drawing.Size(644, 30);
            this.отчетОВыполненныхКомандахToolStripMenuItem.Text = "Отчет о выполненных командах";
            this.отчетОВыполненныхКомандахToolStripMenuItem.Click += new System.EventHandler(this.отчетОВыполненныхКомандахToolStripMenuItem_Click);
            // 
            // LogSaveToolStripMenuItem
            // 
            this.LogSaveToolStripMenuItem.Name = "LogSaveToolStripMenuItem";
            this.LogSaveToolStripMenuItem.Size = new System.Drawing.Size(644, 30);
            this.LogSaveToolStripMenuItem.Text = "Выгрузка текстов запроса и ответа по ошибкам для техподдержки";
            this.LogSaveToolStripMenuItem.Click += new System.EventHandler(this.LogSaveToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(644, 30);
            this.clearToolStripMenuItem.Text = "Очистить лог ошибок";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(644, 30);
            this.testToolStripMenuItem.Text = "Test";
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // debtToolStripMenuItem
            // 
            this.debtToolStripMenuItem.Name = "debtToolStripMenuItem";
            this.debtToolStripMenuItem.Size = new System.Drawing.Size(361, 30);
            this.debtToolStripMenuItem.Text = "Задолженность";
            this.debtToolStripMenuItem.Click += new System.EventHandler(this.debtToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2187, 1251);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "ГИС \"ЖКХ\" Интеграция";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem integrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportNsiMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортВБДToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отозватьПлатежныеДокументыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетОВыполненныхКомандахToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem эспортДоговоровУправленияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LogSaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TotalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem закрытьЛицевыеСчетаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьДополнительныеУслугиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загрузкаИндивидуальныхУслугToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debtToolStripMenuItem;
    }
}