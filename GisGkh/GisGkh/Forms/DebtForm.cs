﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh.Forms
{
    public partial class DebtForm : Form
    {
        public DebtForm()
        {
            InitializeComponent();
        }

        private void buttonExportDept_Click(object sender, EventArgs e)
        {
            var sd = dateTimePicker1.Value;
            var ed = dateTimePicker2.Value;

            ExportDebtSubrequests edr = new ExportDebtSubrequests(senderIdOnlyFilter1.SenderID, sd, ed);

            MrivcStatic.HouseIteration(edr, false);

            //edr.Execute();
        }

        private void DebtForm_Load(object sender, EventArgs e)
        {
            senderIdOnlyFilter1.Fill();
            dateTimePicker1.Value = new DateTime(2022, 1, 1);
            dateTimePicker2.Value = new DateTime(2022, 1, 31);
        }

        private void buttonImportDebt_Click(object sender, EventArgs e)
        {
            ImportResponses ir = new ImportResponses(senderIdOnlyFilter1.SenderID);
            ir.Execute();
        }
    }
}
