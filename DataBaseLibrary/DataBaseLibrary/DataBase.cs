﻿using System.Data.SqlClient;

namespace DataBaseLibrary
{
    public static class DataBase
    {
        /// <summary>
        /// Метод для создания соединения с БД
        /// </summary>
        public static SqlConnection CreateConnection(string dataBaseName)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString =
                $@"Data Source=194.41.41.2;Initial Catalog={dataBaseName};Persist Security Info=True;User ID=komzolchev;Password=kOpyn2";
            try
            {
                connection.Open();
            }
            catch
            {
                return null;
            }

            return connection;
        }

        /// <summary>
        /// Проверяет существует ли таблица в базе
        /// </summary>
        public static bool IsTableExist(SqlConnection connection, string tableName)
        {
            SqlCommand isExist = connection.CreateCommand();
            isExist.CommandText =
                    $"if object_id('{tableName}') is null" +
                    @"begin
                        select 0
                    end
					else
					begin
						select 1
					end";
            return int.Parse(isExist.ExecuteScalar().ToString()) == 1 ? true : false;
        }
    }
}
