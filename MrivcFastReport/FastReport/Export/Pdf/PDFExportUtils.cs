﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using FastReport.Utils;

namespace FastReport.Export.Pdf
{
    public partial class PDFExport : ExportBase
    {
        private float GetTop(float p)
        {
            return FMarginWoBottom - p * PDF_DIVIDER;
        }

        private float GetLeft(float p)
        {
            return FMarginLeft + p * PDF_DIVIDER;
        }

        private string FloatToString(double value)
        {
            return Convert.ToString(Math.Round(value, 2), FNumberFormatInfo);
        }

        private string FloatToString(double value, int digits)
        {
          return Convert.ToString(Math.Round(value, digits), FNumberFormatInfo);
        }

        private string StringToPdfUnicode(string s)
        {
            StringBuilder sb = new StringBuilder(s.Length * 2);
            sb.Append((char)254).Append((char)255);
            foreach (char c in s)
                sb.Append((char)(c >> 8)).Append((char)(c & 0xFF));
            return sb.ToString();
        }

        private string PrepareString(string text, byte[] key, bool encode, long id)
        {
            StringBuilder result = new StringBuilder(text.Length * 2 + 2);
            string s = encode ? RC4CryptString(StringToPdfUnicode(text), key, id) : StringToPdfUnicode(text);
            result.Append("(").Append(EscapeSpecialChar(s)).Append(")");
            return result.ToString();
        }

        private void Write(Stream stream, string value)
        {
            stream.Write(ExportUtils.StringToByteArray(value), 0, value.Length);
        }

        private void WriteLn(Stream stream, string value)
        {
            stream.Write(ExportUtils.StringToByteArray(value), 0, value.Length);
            stream.WriteByte(0x0d);
            stream.WriteByte(0x0a);
        }

        private string StrToUTF16(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                StringBuilder sb = new StringBuilder(str.Length * 4 + 4);
                sb.Append("FEFF");
                foreach (char c in str)
                    sb.Append(((int)c).ToString("X4"));
                return sb.ToString();
            }
            else
                return str;
        }

        private string EscapeSpecialChar(string input)
        {
            StringBuilder sb = new StringBuilder(input.Length);
            for (int i = 0; i < input.Length; i++)
            {
                switch (input[i])
                {
                    case '(':
                        sb.Append(@"\(");
                        break;
                    case ')':
                        sb.Append(@"\)");
                        break;
                    case '\\':
                        sb.Append(@"\\");
                        break;
                    case '\r':
                        sb.Append(@"\r");
                        break;
                    case '\n':
                        sb.Append(@"\n");
                        break;
                    default:
                        sb.Append(input[i]);
                        break;
                }
            }
            return sb.ToString();
        }

        private float GetBaseline(Font f)
        {
            float baselineOffset = f.SizeInPoints / f.FontFamily.GetEmHeight(f.Style) * f.FontFamily.GetCellAscent(f.Style);
            return DrawUtils.ScreenDpi / 72f * baselineOffset;
        }

        private string GetPDFFillColor(Color color)
        {
            return String.Format("{0}{1} rg\n", GetPDFFillTransparent(color), GetPDFColor(color));
        }

        private string GetPDFFillTransparent(Color color)
        {
            string value = FloatToString((float)color.A / 255f);
            int i = FTrasparentStroke.IndexOf(value);
            if (i == -1)
            {
                FTrasparentStroke.Add(value);
                i = FTrasparentStroke.Count - 1;
            }
            return String.Format("/GS{0}S gs\n", i.ToString());
        }

        private string GetPDFStrokeColor(Color color)
        {
            return String.Format("{0}{1} RG\n", GetPDFStrokeTransparent(color), GetPDFColor(color)); 
        }

        private string GetPDFStrokeTransparent(Color color)
        {
            string value = FloatToString((float)color.A / 255f);
            int i = FTrasparentFill.IndexOf(value);
            if (i == -1)
            {
                FTrasparentFill.Add(value);
                i = FTrasparentFill.Count - 1;
            }
            return String.Format("/GS{0}F gs\n", i.ToString()); 
        }

        private string GetPDFColor(Color color)
        {
            if (color == Color.Black)
                return "0 0 0";
            else if (color == Color.White)
                return "1 1 1";
            else
                return String.Format("{0} {1} {2}", 
                    FloatToString((float)color.R / 255f, 3),
                    FloatToString((float)color.G / 255f, 3),
                    FloatToString((float)color.B / 255f, 3));
        }

        private bool FontEquals(Font font1, Font font2)
        {
            return (font1.Name == font2.Name) && font1.Style.Equals(font2.Style);
        }

        private string PrepXRefPos(long p)
        {
            string pos = p.ToString();
            return new string('0', 10 - pos.Length) + pos;
        }

        private string ObjNumber(long FNumber)
        {
            return String.Concat(FNumber.ToString(), " 0 obj");
        }

        private string ObjNumberRef(long FNumber)
        {            
            return String.Concat(FNumber.ToString(), " 0 R");
        }

        private long UpdateXRef()
        {
            FXRef.Add(pdf.Position);
            return FXRef.Count;
        }

        private void WritePDFStream(Stream target, MemoryStream source, long id, bool compressed, bool encrypted, bool startingBrackets, bool endingBrackets, bool enableLength2)
        {
            MemoryStream tempStream;
            if (startingBrackets)
                Write(target, "<<");
            if (enableLength2)
                Write(target, "/Length " + source.Length.ToString());
            if (compressed)
            {
                tempStream = new MemoryStream();
                ExportUtils.ZLibDeflate(source, tempStream);
                if (enableLength2)
                    Write(pdf, "/Length1 " + tempStream.Length.ToString());
                else
                    Write(target, "/Length " + tempStream.Length.ToString());
                Write(pdf, "/Filter/FlateDecode");
            }
            else
                tempStream = source;
            if (endingBrackets)
                WriteLn(target, ">>");
            else
                WriteLn(target, "");
            WriteLn(target, "stream");
            if (encrypted)
                RC4CryptStream(tempStream, target, FEncKey, id);
            else
                tempStream.WriteTo(target);
            target.WriteByte(0x0a);
            WriteLn(target, "endstream");
            WriteLn(target, "endobj");
            source.Dispose();
            source = null;

            if (compressed)
            {
                tempStream.Dispose();
                tempStream = null;
            }
        }

    }
}
