﻿
using Helper.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class CommandReportForm : Form
    {
        public CommandReportForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string senderId = senderIdOnlyFilter1.SenderID;
            var dates = MrivcStatic.CurrentDates();

            textBox1.Text = DAccess.DataModule.GisCommandReport(senderId, dates.Item1, false, dates.Item2);
            textBox2.Text = MrivcStatic.CreateMailText(chbIntermediate.Checked, dates.Item1, dates.Item2, senderId);
        }

        private void CommandReportForm_Load(object sender, EventArgs e)
        {
            senderIdOnlyFilter1.Fill();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (string si in MrivcData.SenderList())
            {
                MrivcStatic.SendCommandReport(si, chbIntermediate.Checked);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
