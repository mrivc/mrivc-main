﻿using FastReport;
using MrivcControls.Data;
using System.IO;
using System.Text;

namespace MrivcHelpers.Helpers
{
    public static class ReportHelper
    {
        public static void ExportFastReportToPdf(Report report, string fileName)
        {
            fileName = Path.ChangeExtension(fileName, "pdf");
            var pdfExport = new FastReport.Export.Pdf.PDFExport();
            report.Export(pdfExport, fileName);
        }

        public static Report PrepareBillReport(int period, int id_bill_kind, int[] id_accounts, bool saveHistory, bool backSide, bool printBillsWithSubscription, bool isDebt)
        {
            Report report = new Report();

            byte[] module = (byte[])DAccess.DataModule.ExecuteScalarQueryCommand($"exec Reports.Bill_Binary_Convert {period}");

            report.LoadFromString(Encoding.UTF8.GetString(module));

            long idPrint = DAccess.DataModule.PrepareRepotData(period, id_bill_kind, id_accounts, saveHistory, backSide, printBillsWithSubscription);

            report.SetParameterValue("MainConnection", DAccess.DataModule.ConnectionString);
            report.SetParameterValue("IDPrint", idPrint);
            report.SetParameterValue("PrintBillsWithSubscription", printBillsWithSubscription);
            report.SetParameterValue("IS_DEBT", isDebt);

            var settings = new EnvironmentSettings();
            settings.ReportSettings.ShowProgress = false;

            report.Prepare();

            return report;
        }
    }
}
