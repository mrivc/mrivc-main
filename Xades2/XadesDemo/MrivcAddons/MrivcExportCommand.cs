﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public abstract class MrivcExportCommand : MrivcCommand
    {
        protected string _exportNodeName;

        public MrivcExportCommand(string method, string service, string senderID, string exportNodeName) : base(method, service, senderID)
        {
            _exportNodeName = exportNodeName;
        }

        public MrivcExportCommand(string method, string service, string exportNodeName) : base(method, service)
        {
            _exportNodeName = exportNodeName;
        }
    }
}
