﻿namespace EllisBillFilter.Forms
{
    partial class FrmPivotSataticticByServicedAccounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPivotSataticticByServicedAccounts));
            this.pivotSataticticByServicedAccounts1 = new MrivcControls.PivotSataticticByServicedAccounts();
            this.SuspendLayout();
            // 
            // pivotSataticticByServicedAccounts1
            // 
            this.pivotSataticticByServicedAccounts1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotSataticticByServicedAccounts1.Location = new System.Drawing.Point(0, 0);
            this.pivotSataticticByServicedAccounts1.Name = "pivotSataticticByServicedAccounts1";
            this.pivotSataticticByServicedAccounts1.Size = new System.Drawing.Size(1142, 516);
            this.pivotSataticticByServicedAccounts1.TabIndex = 0;
            // 
            // FrmPivotSataticticByServicedAccounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 516);
            this.Controls.Add(this.pivotSataticticByServicedAccounts1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPivotSataticticByServicedAccounts";
            this.Text = "Статистика обсулживаемых ЛС";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPivotSataticticByServicedAccounts_FormClosing);
            this.Load += new System.EventHandler(this.FrmPivotSataticticByServicedAccounts_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MrivcControls.PivotSataticticByServicedAccounts pivotSataticticByServicedAccounts1;
    }
}