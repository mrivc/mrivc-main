﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MrivcControls.Data;
using EllisBillFilter;
using DevExpress.XtraGrid.Views.Grid;

namespace MrivcControls.DataPickers
{
    public partial class DabaBaseTablePicker : UserControl
    {
        public DabaBaseTablePicker()
        {
            InitializeComponent();
        }

        public void LoadData(string databaseName, string selectedTable = "")
        {
            ddTabelsPicker.EditValue = string.Empty;
            ddTabelsPicker.Properties.DataSource = null;
            ddTabelsPicker.Properties.DisplayMember = "Name";
            ddTabelsPicker.Properties.ValueMember = "Name";
            ddTabelsPicker.Properties.DataSource = DAccess.DataModule.GetDatabaseTables(databaseName);

            if(!string.IsNullOrEmpty(selectedTable))
                this.SelectedTable = selectedTable;
        }

        public string SelectedTable
        {
            get
            {
                return ddTabelsPicker.EditValue.ToString();
            }
            set
            {
                ddTabelsPicker.EditValue = value;
            }
        }

        public BaseDropDown TablesDropDown
        {
            get
            {
                return ddTabelsPicker;
            }
        }

        public GridView TablesDropDownView
        {
            get
            {
                return ddTabelsView;
            }
        }
    }
}
