﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public abstract class MrivcExportCommand : MrivcCommand
    {
        protected string _exportNodeName;

        public MrivcExportCommand(string method, string service, string senderID, string exportNodeName) : base(method, service, senderID)
        {
            _exportNodeName = exportNodeName;
        }

        public MrivcExportCommand(string method, string service, string exportNodeName) : base(method, service)
        {
            _exportNodeName = exportNodeName;
        }

        //protected Tuple<string[], string>[] values;

        //public new virtual void Execute()
        //{
            
   

            
        //}

        //public abstract void ExecuteToTempDB();

    }
}
