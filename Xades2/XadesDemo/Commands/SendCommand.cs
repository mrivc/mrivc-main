﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xades.Abstractions;
using XadesDemo.Configurations.Options;
using XadesDemo.Configurations.Sections;

namespace XadesDemo.Commands
{
    public class SendCommand : GisCommandBase<SendOptions>
    {
        public SendCommand(SendOptions option, IXadesService xadesService,  SigningConfiguration signingConfig, GisServiceConfiguration serviceConfig) 
            : base(option, xadesService, signingConfig, serviceConfig)
        {
        }

        protected override bool IsSignatureRequired
        {
            get { return true; }
        }

        protected override string OnExecute(SendOptions option, string xmlBody, string senderID)
        {
            return SendRequest(senderID, option.ServiceName, option.MethodName, xmlBody, option.OutputFileName);
        }
    }
}