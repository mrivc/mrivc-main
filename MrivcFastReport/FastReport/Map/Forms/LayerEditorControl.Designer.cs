namespace FastReport.Map.Forms
{
    partial class LayerEditorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.pcLayer = new FastReport.Controls.PageControl();
          this.pgData = new FastReport.Controls.PageControlPage();
          this.panel4 = new System.Windows.Forms.Panel();
          this.lblAnalyticalData = new FastReport.Controls.LabelLine();
          this.tbAnalyticalValue = new FastReport.Controls.TextBoxButton();
          this.cbxFunction = new System.Windows.Forms.ComboBox();
          this.lblFunction = new System.Windows.Forms.Label();
          this.lblAnalyticalValue = new System.Windows.Forms.Label();
          this.panAppDataLayer = new System.Windows.Forms.Panel();
          this.lblLabelValue = new System.Windows.Forms.Label();
          this.tbLatitudeValue = new FastReport.Controls.TextBoxButton();
          this.lblLongitudeValue = new System.Windows.Forms.Label();
          this.tbLongitudeValue = new FastReport.Controls.TextBoxButton();
          this.lblLatitudeValue = new System.Windows.Forms.Label();
          this.tbLabelValue = new FastReport.Controls.TextBoxButton();
          this.panShpLayer = new System.Windows.Forms.Panel();
          this.cbxSpatialColumn = new System.Windows.Forms.ComboBox();
          this.lblSpatialValue = new System.Windows.Forms.Label();
          this.lblSpatialColumn = new System.Windows.Forms.Label();
          this.tbSpatialValue = new FastReport.Controls.TextBoxButton();
          this.panel1 = new System.Windows.Forms.Panel();
          this.cbxDataSource = new System.Windows.Forms.ComboBox();
          this.lblDataSource = new System.Windows.Forms.Label();
          this.lblFilter = new System.Windows.Forms.Label();
          this.lblSpatialData = new FastReport.Controls.LabelLine();
          this.tbFilter = new FastReport.Controls.TextBoxButton();
          this.pgAppearance = new FastReport.Controls.PageControlPage();
          this.udBorderWidth = new System.Windows.Forms.NumericUpDown();
          this.cbxPalette = new System.Windows.Forms.ComboBox();
          this.cbxBorderStyle = new System.Windows.Forms.ComboBox();
          this.lblPalette = new System.Windows.Forms.Label();
          this.lblBorderWidth = new System.Windows.Forms.Label();
          this.lblBorderStyle = new System.Windows.Forms.Label();
          this.lblFillColor = new System.Windows.Forms.Label();
          this.lblBorderColor = new System.Windows.Forms.Label();
          this.cbxFillColor = new FastReport.Controls.ColorComboBox();
          this.cbxBorderColor = new FastReport.Controls.ColorComboBox();
          this.labelLine2 = new FastReport.Controls.LabelLine();
          this.cbVisible = new System.Windows.Forms.CheckBox();
          this.pgColorRanges = new FastReport.Controls.PageControlPage();
          this.udNumberOfRanges = new System.Windows.Forms.NumericUpDown();
          this.lblNumberOfRanges = new System.Windows.Forms.Label();
          this.lblEndColor = new System.Windows.Forms.Label();
          this.lblEnd = new System.Windows.Forms.Label();
          this.lblMiddleColor = new System.Windows.Forms.Label();
          this.lblStart = new System.Windows.Forms.Label();
          this.lblColor = new System.Windows.Forms.Label();
          this.lblStartColor = new System.Windows.Forms.Label();
          this.cbxEndColor = new FastReport.Controls.ColorComboBox();
          this.cbxMiddleColor = new FastReport.Controls.ColorComboBox();
          this.cbxStartColor = new FastReport.Controls.ColorComboBox();
          this.labelLine4 = new FastReport.Controls.LabelLine();
          this.labelLine5 = new FastReport.Controls.LabelLine();
          this.cbShowInColorScale = new System.Windows.Forms.CheckBox();
          this.pgSizeRanges = new FastReport.Controls.PageControlPage();
          this.lblEnd1 = new System.Windows.Forms.Label();
          this.lblStart1 = new System.Windows.Forms.Label();
          this.lblSize = new System.Windows.Forms.Label();
          this.udSizeRanges = new System.Windows.Forms.NumericUpDown();
          this.lblSizeRanges = new System.Windows.Forms.Label();
          this.udEndSize = new System.Windows.Forms.NumericUpDown();
          this.lblEndSize = new System.Windows.Forms.Label();
          this.udStartSize = new System.Windows.Forms.NumericUpDown();
          this.lblStartSize = new System.Windows.Forms.Label();
          this.pgLabels = new FastReport.Controls.PageControlPage();
          this.udVisibleAtZoom = new System.Windows.Forms.NumericUpDown();
          this.tbFont = new FastReport.Controls.TextBoxButton();
          this.lblTextColor = new System.Windows.Forms.Label();
          this.lblFont = new System.Windows.Forms.Label();
          this.cbxTextColor = new FastReport.Controls.ColorComboBox();
          this.rbNameAndValue = new System.Windows.Forms.RadioButton();
          this.rbValue = new System.Windows.Forms.RadioButton();
          this.rbName = new System.Windows.Forms.RadioButton();
          this.rbNone = new System.Windows.Forms.RadioButton();
          this.lblLabelKind = new FastReport.Controls.LabelLine();
          this.tbLabelFormat = new System.Windows.Forms.TextBox();
          this.cbxLabelColumn = new System.Windows.Forms.ComboBox();
          this.lblVisibleAtZoom = new System.Windows.Forms.Label();
          this.lblLabelColumn = new System.Windows.Forms.Label();
          this.lblLabelFormat = new System.Windows.Forms.Label();
          this.lblZoomPolygon = new FastReport.Controls.LabelLine();
          this.lblZoomPolygonValue = new System.Windows.Forms.Label();
          this.tbZoomPolygon = new FastReport.Controls.TextBoxButton();
          this.pcLayer.SuspendLayout();
          this.pgData.SuspendLayout();
          this.panel4.SuspendLayout();
          this.panAppDataLayer.SuspendLayout();
          this.panShpLayer.SuspendLayout();
          this.panel1.SuspendLayout();
          this.pgAppearance.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.udBorderWidth)).BeginInit();
          this.pgColorRanges.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.udNumberOfRanges)).BeginInit();
          this.pgSizeRanges.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.udSizeRanges)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.udEndSize)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.udStartSize)).BeginInit();
          this.pgLabels.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.udVisibleAtZoom)).BeginInit();
          this.SuspendLayout();
          // 
          // pcLayer
          // 
          this.pcLayer.Controls.Add(this.pgData);
          this.pcLayer.Controls.Add(this.pgAppearance);
          this.pcLayer.Controls.Add(this.pgColorRanges);
          this.pcLayer.Controls.Add(this.pgSizeRanges);
          this.pcLayer.Controls.Add(this.pgLabels);
          this.pcLayer.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pcLayer.HighlightPageIndex = -1;
          this.pcLayer.Location = new System.Drawing.Point(0, 0);
          this.pcLayer.Name = "pcLayer";
          this.pcLayer.SelectorWidth = 120;
          this.pcLayer.Size = new System.Drawing.Size(436, 504);
          this.pcLayer.TabIndex = 0;
          this.pcLayer.Text = "pageControl1";
          // 
          // pgData
          // 
          this.pgData.BackColor = System.Drawing.SystemColors.Window;
          this.pgData.Controls.Add(this.panel4);
          this.pgData.Controls.Add(this.panAppDataLayer);
          this.pgData.Controls.Add(this.panShpLayer);
          this.pgData.Controls.Add(this.panel1);
          this.pgData.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pgData.Location = new System.Drawing.Point(120, 1);
          this.pgData.Name = "pgData";
          this.pgData.Size = new System.Drawing.Size(315, 502);
          this.pgData.TabIndex = 0;
          this.pgData.Text = "Data";
          // 
          // panel4
          // 
          this.panel4.Controls.Add(this.lblZoomPolygon);
          this.panel4.Controls.Add(this.lblAnalyticalData);
          this.panel4.Controls.Add(this.tbZoomPolygon);
          this.panel4.Controls.Add(this.tbAnalyticalValue);
          this.panel4.Controls.Add(this.cbxFunction);
          this.panel4.Controls.Add(this.lblZoomPolygonValue);
          this.panel4.Controls.Add(this.lblFunction);
          this.panel4.Controls.Add(this.lblAnalyticalValue);
          this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
          this.panel4.Location = new System.Drawing.Point(0, 272);
          this.panel4.Name = "panel4";
          this.panel4.Size = new System.Drawing.Size(315, 148);
          this.panel4.TabIndex = 8;
          // 
          // lblAnalyticalData
          // 
          this.lblAnalyticalData.Location = new System.Drawing.Point(16, 0);
          this.lblAnalyticalData.Name = "lblAnalyticalData";
          this.lblAnalyticalData.Size = new System.Drawing.Size(284, 16);
          this.lblAnalyticalData.TabIndex = 4;
          this.lblAnalyticalData.Text = "Analytical data:";
          // 
          // tbAnalyticalValue
          // 
          this.tbAnalyticalValue.Image = null;
          this.tbAnalyticalValue.Location = new System.Drawing.Point(128, 24);
          this.tbAnalyticalValue.Name = "tbAnalyticalValue";
          this.tbAnalyticalValue.Size = new System.Drawing.Size(172, 21);
          this.tbAnalyticalValue.TabIndex = 3;
          this.tbAnalyticalValue.ButtonClick += new System.EventHandler(this.tbAnalyticalValue_ButtonClick);
          this.tbAnalyticalValue.Leave += new System.EventHandler(this.tbAnalyticalValue_Leave);
          // 
          // cbxFunction
          // 
          this.cbxFunction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbxFunction.FormattingEnabled = true;
          this.cbxFunction.Location = new System.Drawing.Point(128, 52);
          this.cbxFunction.Name = "cbxFunction";
          this.cbxFunction.Size = new System.Drawing.Size(172, 21);
          this.cbxFunction.TabIndex = 2;
          this.cbxFunction.SelectedIndexChanged += new System.EventHandler(this.cbxFunction_SelectedIndexChanged);
          // 
          // lblFunction
          // 
          this.lblFunction.AutoSize = true;
          this.lblFunction.Location = new System.Drawing.Point(16, 56);
          this.lblFunction.Name = "lblFunction";
          this.lblFunction.Size = new System.Drawing.Size(51, 13);
          this.lblFunction.TabIndex = 0;
          this.lblFunction.Text = "Function:";
          // 
          // lblAnalyticalValue
          // 
          this.lblAnalyticalValue.AutoSize = true;
          this.lblAnalyticalValue.Location = new System.Drawing.Point(16, 28);
          this.lblAnalyticalValue.Name = "lblAnalyticalValue";
          this.lblAnalyticalValue.Size = new System.Drawing.Size(37, 13);
          this.lblAnalyticalValue.TabIndex = 1;
          this.lblAnalyticalValue.Text = "Value:";
          // 
          // panAppDataLayer
          // 
          this.panAppDataLayer.Controls.Add(this.lblLabelValue);
          this.panAppDataLayer.Controls.Add(this.tbLatitudeValue);
          this.panAppDataLayer.Controls.Add(this.lblLongitudeValue);
          this.panAppDataLayer.Controls.Add(this.tbLongitudeValue);
          this.panAppDataLayer.Controls.Add(this.lblLatitudeValue);
          this.panAppDataLayer.Controls.Add(this.tbLabelValue);
          this.panAppDataLayer.Dock = System.Windows.Forms.DockStyle.Top;
          this.panAppDataLayer.Location = new System.Drawing.Point(0, 176);
          this.panAppDataLayer.Name = "panAppDataLayer";
          this.panAppDataLayer.Size = new System.Drawing.Size(315, 96);
          this.panAppDataLayer.TabIndex = 7;
          // 
          // lblLabelValue
          // 
          this.lblLabelValue.AutoSize = true;
          this.lblLabelValue.Location = new System.Drawing.Point(16, 60);
          this.lblLabelValue.Name = "lblLabelValue";
          this.lblLabelValue.Size = new System.Drawing.Size(36, 13);
          this.lblLabelValue.TabIndex = 0;
          this.lblLabelValue.Text = "Label:";
          // 
          // tbLatitudeValue
          // 
          this.tbLatitudeValue.Image = null;
          this.tbLatitudeValue.Location = new System.Drawing.Point(128, 0);
          this.tbLatitudeValue.Name = "tbLatitudeValue";
          this.tbLatitudeValue.Size = new System.Drawing.Size(172, 21);
          this.tbLatitudeValue.TabIndex = 3;
          this.tbLatitudeValue.ButtonClick += new System.EventHandler(this.tbLatitude_ButtonClick);
          this.tbLatitudeValue.Leave += new System.EventHandler(this.tbLatitude_Leave);
          // 
          // lblLongitudeValue
          // 
          this.lblLongitudeValue.AutoSize = true;
          this.lblLongitudeValue.Location = new System.Drawing.Point(16, 32);
          this.lblLongitudeValue.Name = "lblLongitudeValue";
          this.lblLongitudeValue.Size = new System.Drawing.Size(57, 13);
          this.lblLongitudeValue.TabIndex = 0;
          this.lblLongitudeValue.Text = "Longitude:";
          // 
          // tbLongitudeValue
          // 
          this.tbLongitudeValue.Image = null;
          this.tbLongitudeValue.Location = new System.Drawing.Point(128, 28);
          this.tbLongitudeValue.Name = "tbLongitudeValue";
          this.tbLongitudeValue.Size = new System.Drawing.Size(172, 21);
          this.tbLongitudeValue.TabIndex = 3;
          this.tbLongitudeValue.ButtonClick += new System.EventHandler(this.tbLongitude_ButtonClick);
          this.tbLongitudeValue.Leave += new System.EventHandler(this.tbLongitude_Leave);
          // 
          // lblLatitudeValue
          // 
          this.lblLatitudeValue.AutoSize = true;
          this.lblLatitudeValue.Location = new System.Drawing.Point(16, 4);
          this.lblLatitudeValue.Name = "lblLatitudeValue";
          this.lblLatitudeValue.Size = new System.Drawing.Size(48, 13);
          this.lblLatitudeValue.TabIndex = 0;
          this.lblLatitudeValue.Text = "Latitude:";
          // 
          // tbLabelValue
          // 
          this.tbLabelValue.Image = null;
          this.tbLabelValue.Location = new System.Drawing.Point(128, 56);
          this.tbLabelValue.Name = "tbLabelValue";
          this.tbLabelValue.Size = new System.Drawing.Size(172, 21);
          this.tbLabelValue.TabIndex = 3;
          this.tbLabelValue.ButtonClick += new System.EventHandler(this.tbLabel_ButtonClick);
          this.tbLabelValue.Leave += new System.EventHandler(this.tbLabel_Leave);
          // 
          // panShpLayer
          // 
          this.panShpLayer.Controls.Add(this.cbxSpatialColumn);
          this.panShpLayer.Controls.Add(this.lblSpatialValue);
          this.panShpLayer.Controls.Add(this.lblSpatialColumn);
          this.panShpLayer.Controls.Add(this.tbSpatialValue);
          this.panShpLayer.Dock = System.Windows.Forms.DockStyle.Top;
          this.panShpLayer.Location = new System.Drawing.Point(0, 108);
          this.panShpLayer.Name = "panShpLayer";
          this.panShpLayer.Size = new System.Drawing.Size(315, 68);
          this.panShpLayer.TabIndex = 6;
          // 
          // cbxSpatialColumn
          // 
          this.cbxSpatialColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbxSpatialColumn.FormattingEnabled = true;
          this.cbxSpatialColumn.Location = new System.Drawing.Point(128, 0);
          this.cbxSpatialColumn.Name = "cbxSpatialColumn";
          this.cbxSpatialColumn.Size = new System.Drawing.Size(172, 21);
          this.cbxSpatialColumn.TabIndex = 2;
          this.cbxSpatialColumn.SelectedIndexChanged += new System.EventHandler(this.cbxSpatialColumn_SelectedIndexChanged);
          // 
          // lblSpatialValue
          // 
          this.lblSpatialValue.AutoSize = true;
          this.lblSpatialValue.Location = new System.Drawing.Point(16, 32);
          this.lblSpatialValue.Name = "lblSpatialValue";
          this.lblSpatialValue.Size = new System.Drawing.Size(37, 13);
          this.lblSpatialValue.TabIndex = 1;
          this.lblSpatialValue.Text = "Value:";
          // 
          // lblSpatialColumn
          // 
          this.lblSpatialColumn.AutoSize = true;
          this.lblSpatialColumn.Location = new System.Drawing.Point(16, 4);
          this.lblSpatialColumn.Name = "lblSpatialColumn";
          this.lblSpatialColumn.Size = new System.Drawing.Size(45, 13);
          this.lblSpatialColumn.TabIndex = 0;
          this.lblSpatialColumn.Text = "Column:";
          // 
          // tbSpatialValue
          // 
          this.tbSpatialValue.Image = null;
          this.tbSpatialValue.Location = new System.Drawing.Point(128, 28);
          this.tbSpatialValue.Name = "tbSpatialValue";
          this.tbSpatialValue.Size = new System.Drawing.Size(172, 21);
          this.tbSpatialValue.TabIndex = 3;
          this.tbSpatialValue.ButtonClick += new System.EventHandler(this.tbSpatialValue_ButtonClick);
          this.tbSpatialValue.Leave += new System.EventHandler(this.tbSpatialValue_Leave);
          // 
          // panel1
          // 
          this.panel1.Controls.Add(this.cbxDataSource);
          this.panel1.Controls.Add(this.lblDataSource);
          this.panel1.Controls.Add(this.lblFilter);
          this.panel1.Controls.Add(this.lblSpatialData);
          this.panel1.Controls.Add(this.tbFilter);
          this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
          this.panel1.Location = new System.Drawing.Point(0, 0);
          this.panel1.Name = "panel1";
          this.panel1.Size = new System.Drawing.Size(315, 108);
          this.panel1.TabIndex = 5;
          // 
          // cbxDataSource
          // 
          this.cbxDataSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbxDataSource.FormattingEnabled = true;
          this.cbxDataSource.Location = new System.Drawing.Point(128, 16);
          this.cbxDataSource.Name = "cbxDataSource";
          this.cbxDataSource.Size = new System.Drawing.Size(172, 21);
          this.cbxDataSource.TabIndex = 2;
          this.cbxDataSource.SelectedIndexChanged += new System.EventHandler(this.cbxDataSource_SelectedIndexChanged);
          // 
          // lblDataSource
          // 
          this.lblDataSource.AutoSize = true;
          this.lblDataSource.Location = new System.Drawing.Point(16, 20);
          this.lblDataSource.Name = "lblDataSource";
          this.lblDataSource.Size = new System.Drawing.Size(68, 13);
          this.lblDataSource.TabIndex = 0;
          this.lblDataSource.Text = "Data source:";
          // 
          // lblFilter
          // 
          this.lblFilter.AutoSize = true;
          this.lblFilter.Location = new System.Drawing.Point(16, 48);
          this.lblFilter.Name = "lblFilter";
          this.lblFilter.Size = new System.Drawing.Size(32, 13);
          this.lblFilter.TabIndex = 1;
          this.lblFilter.Text = "Filter:";
          // 
          // lblSpatialData
          // 
          this.lblSpatialData.Location = new System.Drawing.Point(16, 84);
          this.lblSpatialData.Name = "lblSpatialData";
          this.lblSpatialData.Size = new System.Drawing.Size(284, 16);
          this.lblSpatialData.TabIndex = 4;
          this.lblSpatialData.Text = "Spatial data:";
          // 
          // tbFilter
          // 
          this.tbFilter.Image = null;
          this.tbFilter.Location = new System.Drawing.Point(128, 44);
          this.tbFilter.Name = "tbFilter";
          this.tbFilter.Size = new System.Drawing.Size(172, 21);
          this.tbFilter.TabIndex = 3;
          this.tbFilter.ButtonClick += new System.EventHandler(this.tbFilter_ButtonClick);
          this.tbFilter.Leave += new System.EventHandler(this.tbFilter_Leave);
          // 
          // pgAppearance
          // 
          this.pgAppearance.BackColor = System.Drawing.SystemColors.Window;
          this.pgAppearance.Controls.Add(this.udBorderWidth);
          this.pgAppearance.Controls.Add(this.cbxPalette);
          this.pgAppearance.Controls.Add(this.cbxBorderStyle);
          this.pgAppearance.Controls.Add(this.lblPalette);
          this.pgAppearance.Controls.Add(this.lblBorderWidth);
          this.pgAppearance.Controls.Add(this.lblBorderStyle);
          this.pgAppearance.Controls.Add(this.lblFillColor);
          this.pgAppearance.Controls.Add(this.lblBorderColor);
          this.pgAppearance.Controls.Add(this.cbxFillColor);
          this.pgAppearance.Controls.Add(this.cbxBorderColor);
          this.pgAppearance.Controls.Add(this.labelLine2);
          this.pgAppearance.Controls.Add(this.cbVisible);
          this.pgAppearance.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pgAppearance.Location = new System.Drawing.Point(120, 1);
          this.pgAppearance.Name = "pgAppearance";
          this.pgAppearance.Size = new System.Drawing.Size(315, 502);
          this.pgAppearance.TabIndex = 1;
          this.pgAppearance.Text = "Appearance";
          // 
          // udBorderWidth
          // 
          this.udBorderWidth.DecimalPlaces = 2;
          this.udBorderWidth.Location = new System.Drawing.Point(128, 124);
          this.udBorderWidth.Name = "udBorderWidth";
          this.udBorderWidth.Size = new System.Drawing.Size(64, 20);
          this.udBorderWidth.TabIndex = 12;
          this.udBorderWidth.ValueChanged += new System.EventHandler(this.udBorderWidth_ValueChanged);
          // 
          // cbxPalette
          // 
          this.cbxPalette.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbxPalette.FormattingEnabled = true;
          this.cbxPalette.Location = new System.Drawing.Point(128, 180);
          this.cbxPalette.Name = "cbxPalette";
          this.cbxPalette.Size = new System.Drawing.Size(172, 21);
          this.cbxPalette.TabIndex = 11;
          this.cbxPalette.SelectedIndexChanged += new System.EventHandler(this.cbxPalette_SelectedIndexChanged);
          // 
          // cbxBorderStyle
          // 
          this.cbxBorderStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbxBorderStyle.FormattingEnabled = true;
          this.cbxBorderStyle.Location = new System.Drawing.Point(128, 96);
          this.cbxBorderStyle.Name = "cbxBorderStyle";
          this.cbxBorderStyle.Size = new System.Drawing.Size(172, 21);
          this.cbxBorderStyle.TabIndex = 11;
          this.cbxBorderStyle.SelectedIndexChanged += new System.EventHandler(this.cbxBorderStyle_SelectedIndexChanged);
          // 
          // lblPalette
          // 
          this.lblPalette.AutoSize = true;
          this.lblPalette.Location = new System.Drawing.Point(16, 184);
          this.lblPalette.Name = "lblPalette";
          this.lblPalette.Size = new System.Drawing.Size(43, 13);
          this.lblPalette.TabIndex = 10;
          this.lblPalette.Text = "Palette:";
          // 
          // lblBorderWidth
          // 
          this.lblBorderWidth.AutoSize = true;
          this.lblBorderWidth.Location = new System.Drawing.Point(16, 128);
          this.lblBorderWidth.Name = "lblBorderWidth";
          this.lblBorderWidth.Size = new System.Drawing.Size(69, 13);
          this.lblBorderWidth.TabIndex = 10;
          this.lblBorderWidth.Text = "Border width:";
          // 
          // lblBorderStyle
          // 
          this.lblBorderStyle.AutoSize = true;
          this.lblBorderStyle.Location = new System.Drawing.Point(16, 100);
          this.lblBorderStyle.Name = "lblBorderStyle";
          this.lblBorderStyle.Size = new System.Drawing.Size(65, 13);
          this.lblBorderStyle.TabIndex = 10;
          this.lblBorderStyle.Text = "Border style:";
          // 
          // lblFillColor
          // 
          this.lblFillColor.AutoSize = true;
          this.lblFillColor.Location = new System.Drawing.Point(16, 156);
          this.lblFillColor.Name = "lblFillColor";
          this.lblFillColor.Size = new System.Drawing.Size(48, 13);
          this.lblFillColor.TabIndex = 10;
          this.lblFillColor.Text = "Fill color:";
          // 
          // lblBorderColor
          // 
          this.lblBorderColor.AutoSize = true;
          this.lblBorderColor.Location = new System.Drawing.Point(16, 72);
          this.lblBorderColor.Name = "lblBorderColor";
          this.lblBorderColor.Size = new System.Drawing.Size(67, 13);
          this.lblBorderColor.TabIndex = 10;
          this.lblBorderColor.Text = "Border color:";
          // 
          // cbxFillColor
          // 
          this.cbxFillColor.Color = System.Drawing.Color.Transparent;
          this.cbxFillColor.Location = new System.Drawing.Point(128, 152);
          this.cbxFillColor.Name = "cbxFillColor";
          this.cbxFillColor.ShowColorName = true;
          this.cbxFillColor.Size = new System.Drawing.Size(172, 21);
          this.cbxFillColor.TabIndex = 9;
          this.cbxFillColor.ColorSelected += new System.EventHandler(this.cbxFillColor_ColorSelected);
          // 
          // cbxBorderColor
          // 
          this.cbxBorderColor.Color = System.Drawing.Color.Transparent;
          this.cbxBorderColor.Location = new System.Drawing.Point(128, 68);
          this.cbxBorderColor.Name = "cbxBorderColor";
          this.cbxBorderColor.ShowColorName = true;
          this.cbxBorderColor.Size = new System.Drawing.Size(172, 21);
          this.cbxBorderColor.TabIndex = 9;
          this.cbxBorderColor.ColorSelected += new System.EventHandler(this.cbxBorderColor_ColorSelected);
          // 
          // labelLine2
          // 
          this.labelLine2.Location = new System.Drawing.Point(16, 44);
          this.labelLine2.Name = "labelLine2";
          this.labelLine2.Size = new System.Drawing.Size(284, 12);
          this.labelLine2.TabIndex = 8;
          // 
          // cbVisible
          // 
          this.cbVisible.AutoSize = true;
          this.cbVisible.Location = new System.Drawing.Point(16, 20);
          this.cbVisible.Name = "cbVisible";
          this.cbVisible.Size = new System.Drawing.Size(56, 17);
          this.cbVisible.TabIndex = 7;
          this.cbVisible.Text = "Visible";
          this.cbVisible.UseVisualStyleBackColor = true;
          this.cbVisible.CheckedChanged += new System.EventHandler(this.cbVisible_CheckedChanged);
          // 
          // pgColorRanges
          // 
          this.pgColorRanges.BackColor = System.Drawing.SystemColors.Window;
          this.pgColorRanges.Controls.Add(this.udNumberOfRanges);
          this.pgColorRanges.Controls.Add(this.lblNumberOfRanges);
          this.pgColorRanges.Controls.Add(this.lblEndColor);
          this.pgColorRanges.Controls.Add(this.lblEnd);
          this.pgColorRanges.Controls.Add(this.lblMiddleColor);
          this.pgColorRanges.Controls.Add(this.lblStart);
          this.pgColorRanges.Controls.Add(this.lblColor);
          this.pgColorRanges.Controls.Add(this.lblStartColor);
          this.pgColorRanges.Controls.Add(this.cbxEndColor);
          this.pgColorRanges.Controls.Add(this.cbxMiddleColor);
          this.pgColorRanges.Controls.Add(this.cbxStartColor);
          this.pgColorRanges.Controls.Add(this.labelLine4);
          this.pgColorRanges.Controls.Add(this.labelLine5);
          this.pgColorRanges.Controls.Add(this.cbShowInColorScale);
          this.pgColorRanges.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pgColorRanges.Location = new System.Drawing.Point(120, 1);
          this.pgColorRanges.Name = "pgColorRanges";
          this.pgColorRanges.Size = new System.Drawing.Size(315, 502);
          this.pgColorRanges.TabIndex = 2;
          this.pgColorRanges.Text = "Color ranges";
          // 
          // udNumberOfRanges
          // 
          this.udNumberOfRanges.Location = new System.Drawing.Point(128, 180);
          this.udNumberOfRanges.Name = "udNumberOfRanges";
          this.udNumberOfRanges.Size = new System.Drawing.Size(64, 20);
          this.udNumberOfRanges.TabIndex = 21;
          this.udNumberOfRanges.ValueChanged += new System.EventHandler(this.udNumberOfRanges_ValueChanged);
          // 
          // lblNumberOfRanges
          // 
          this.lblNumberOfRanges.AutoSize = true;
          this.lblNumberOfRanges.Location = new System.Drawing.Point(16, 184);
          this.lblNumberOfRanges.Name = "lblNumberOfRanges";
          this.lblNumberOfRanges.Size = new System.Drawing.Size(47, 13);
          this.lblNumberOfRanges.TabIndex = 20;
          this.lblNumberOfRanges.Text = "Ranges:";
          // 
          // lblEndColor
          // 
          this.lblEndColor.AutoSize = true;
          this.lblEndColor.Location = new System.Drawing.Point(16, 128);
          this.lblEndColor.Name = "lblEndColor";
          this.lblEndColor.Size = new System.Drawing.Size(55, 13);
          this.lblEndColor.TabIndex = 12;
          this.lblEndColor.Text = "End color:";
          // 
          // lblEnd
          // 
          this.lblEnd.AutoSize = true;
          this.lblEnd.Location = new System.Drawing.Point(80, 220);
          this.lblEnd.Name = "lblEnd";
          this.lblEnd.Size = new System.Drawing.Size(29, 13);
          this.lblEnd.TabIndex = 12;
          this.lblEnd.Text = "End:";
          // 
          // lblMiddleColor
          // 
          this.lblMiddleColor.AutoSize = true;
          this.lblMiddleColor.Location = new System.Drawing.Point(16, 100);
          this.lblMiddleColor.Name = "lblMiddleColor";
          this.lblMiddleColor.Size = new System.Drawing.Size(67, 13);
          this.lblMiddleColor.TabIndex = 12;
          this.lblMiddleColor.Text = "Middle color:";
          // 
          // lblStart
          // 
          this.lblStart.AutoSize = true;
          this.lblStart.Location = new System.Drawing.Point(16, 220);
          this.lblStart.Name = "lblStart";
          this.lblStart.Size = new System.Drawing.Size(32, 13);
          this.lblStart.TabIndex = 12;
          this.lblStart.Text = "Start:";
          // 
          // lblColor
          // 
          this.lblColor.AutoSize = true;
          this.lblColor.Location = new System.Drawing.Point(144, 220);
          this.lblColor.Name = "lblColor";
          this.lblColor.Size = new System.Drawing.Size(34, 13);
          this.lblColor.TabIndex = 12;
          this.lblColor.Text = "Color:";
          // 
          // lblStartColor
          // 
          this.lblStartColor.AutoSize = true;
          this.lblStartColor.Location = new System.Drawing.Point(16, 72);
          this.lblStartColor.Name = "lblStartColor";
          this.lblStartColor.Size = new System.Drawing.Size(58, 13);
          this.lblStartColor.TabIndex = 12;
          this.lblStartColor.Text = "Start color:";
          // 
          // cbxEndColor
          // 
          this.cbxEndColor.Color = System.Drawing.Color.Transparent;
          this.cbxEndColor.Location = new System.Drawing.Point(128, 124);
          this.cbxEndColor.Name = "cbxEndColor";
          this.cbxEndColor.ShowColorName = true;
          this.cbxEndColor.Size = new System.Drawing.Size(172, 21);
          this.cbxEndColor.TabIndex = 11;
          this.cbxEndColor.ColorSelected += new System.EventHandler(this.cbxEndColor_ColorSelected);
          // 
          // cbxMiddleColor
          // 
          this.cbxMiddleColor.Color = System.Drawing.Color.Transparent;
          this.cbxMiddleColor.Location = new System.Drawing.Point(128, 96);
          this.cbxMiddleColor.Name = "cbxMiddleColor";
          this.cbxMiddleColor.ShowColorName = true;
          this.cbxMiddleColor.Size = new System.Drawing.Size(172, 21);
          this.cbxMiddleColor.TabIndex = 11;
          this.cbxMiddleColor.ColorSelected += new System.EventHandler(this.cbxMiddleColor_ColorSelected);
          // 
          // cbxStartColor
          // 
          this.cbxStartColor.Color = System.Drawing.Color.Transparent;
          this.cbxStartColor.Location = new System.Drawing.Point(128, 68);
          this.cbxStartColor.Name = "cbxStartColor";
          this.cbxStartColor.ShowColorName = true;
          this.cbxStartColor.Size = new System.Drawing.Size(172, 21);
          this.cbxStartColor.TabIndex = 11;
          this.cbxStartColor.ColorSelected += new System.EventHandler(this.cbxStartColor_ColorSelected);
          // 
          // labelLine4
          // 
          this.labelLine4.Location = new System.Drawing.Point(16, 156);
          this.labelLine4.Name = "labelLine4";
          this.labelLine4.Size = new System.Drawing.Size(284, 12);
          this.labelLine4.TabIndex = 10;
          // 
          // labelLine5
          // 
          this.labelLine5.Location = new System.Drawing.Point(16, 44);
          this.labelLine5.Name = "labelLine5";
          this.labelLine5.Size = new System.Drawing.Size(284, 12);
          this.labelLine5.TabIndex = 10;
          // 
          // cbShowInColorScale
          // 
          this.cbShowInColorScale.AutoSize = true;
          this.cbShowInColorScale.Location = new System.Drawing.Point(16, 20);
          this.cbShowInColorScale.Name = "cbShowInColorScale";
          this.cbShowInColorScale.Size = new System.Drawing.Size(148, 17);
          this.cbShowInColorScale.TabIndex = 9;
          this.cbShowInColorScale.Text = "Show in map\'s color scale";
          this.cbShowInColorScale.UseVisualStyleBackColor = true;
          this.cbShowInColorScale.CheckedChanged += new System.EventHandler(this.cbShowInColorScale_CheckedChanged);
          // 
          // pgSizeRanges
          // 
          this.pgSizeRanges.BackColor = System.Drawing.SystemColors.Window;
          this.pgSizeRanges.Controls.Add(this.lblEnd1);
          this.pgSizeRanges.Controls.Add(this.lblStart1);
          this.pgSizeRanges.Controls.Add(this.lblSize);
          this.pgSizeRanges.Controls.Add(this.udSizeRanges);
          this.pgSizeRanges.Controls.Add(this.lblSizeRanges);
          this.pgSizeRanges.Controls.Add(this.udEndSize);
          this.pgSizeRanges.Controls.Add(this.lblEndSize);
          this.pgSizeRanges.Controls.Add(this.udStartSize);
          this.pgSizeRanges.Controls.Add(this.lblStartSize);
          this.pgSizeRanges.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pgSizeRanges.Location = new System.Drawing.Point(120, 1);
          this.pgSizeRanges.Name = "pgSizeRanges";
          this.pgSizeRanges.Size = new System.Drawing.Size(315, 502);
          this.pgSizeRanges.TabIndex = 4;
          this.pgSizeRanges.Text = "Size ranges";
          // 
          // lblEnd1
          // 
          this.lblEnd1.AutoSize = true;
          this.lblEnd1.Location = new System.Drawing.Point(80, 112);
          this.lblEnd1.Name = "lblEnd1";
          this.lblEnd1.Size = new System.Drawing.Size(29, 13);
          this.lblEnd1.TabIndex = 15;
          this.lblEnd1.Text = "End:";
          // 
          // lblStart1
          // 
          this.lblStart1.AutoSize = true;
          this.lblStart1.Location = new System.Drawing.Point(16, 112);
          this.lblStart1.Name = "lblStart1";
          this.lblStart1.Size = new System.Drawing.Size(32, 13);
          this.lblStart1.TabIndex = 14;
          this.lblStart1.Text = "Start:";
          // 
          // lblSize
          // 
          this.lblSize.AutoSize = true;
          this.lblSize.Location = new System.Drawing.Point(144, 112);
          this.lblSize.Name = "lblSize";
          this.lblSize.Size = new System.Drawing.Size(30, 13);
          this.lblSize.TabIndex = 13;
          this.lblSize.Text = "Size:";
          // 
          // udSizeRanges
          // 
          this.udSizeRanges.Location = new System.Drawing.Point(128, 72);
          this.udSizeRanges.Name = "udSizeRanges";
          this.udSizeRanges.Size = new System.Drawing.Size(64, 20);
          this.udSizeRanges.TabIndex = 1;
          this.udSizeRanges.ValueChanged += new System.EventHandler(this.udSizeRanges_ValueChanged);
          // 
          // lblSizeRanges
          // 
          this.lblSizeRanges.AutoSize = true;
          this.lblSizeRanges.Location = new System.Drawing.Point(16, 76);
          this.lblSizeRanges.Name = "lblSizeRanges";
          this.lblSizeRanges.Size = new System.Drawing.Size(47, 13);
          this.lblSizeRanges.TabIndex = 0;
          this.lblSizeRanges.Text = "Ranges:";
          // 
          // udEndSize
          // 
          this.udEndSize.Location = new System.Drawing.Point(128, 44);
          this.udEndSize.Name = "udEndSize";
          this.udEndSize.Size = new System.Drawing.Size(64, 20);
          this.udEndSize.TabIndex = 1;
          this.udEndSize.ValueChanged += new System.EventHandler(this.udEndSize_ValueChanged);
          // 
          // lblEndSize
          // 
          this.lblEndSize.AutoSize = true;
          this.lblEndSize.Location = new System.Drawing.Point(16, 48);
          this.lblEndSize.Name = "lblEndSize";
          this.lblEndSize.Size = new System.Drawing.Size(50, 13);
          this.lblEndSize.TabIndex = 0;
          this.lblEndSize.Text = "End size:";
          // 
          // udStartSize
          // 
          this.udStartSize.Location = new System.Drawing.Point(128, 16);
          this.udStartSize.Name = "udStartSize";
          this.udStartSize.Size = new System.Drawing.Size(64, 20);
          this.udStartSize.TabIndex = 1;
          this.udStartSize.ValueChanged += new System.EventHandler(this.udStartSize_ValueChanged);
          // 
          // lblStartSize
          // 
          this.lblStartSize.AutoSize = true;
          this.lblStartSize.Location = new System.Drawing.Point(16, 20);
          this.lblStartSize.Name = "lblStartSize";
          this.lblStartSize.Size = new System.Drawing.Size(53, 13);
          this.lblStartSize.TabIndex = 0;
          this.lblStartSize.Text = "Start size:";
          // 
          // pgLabels
          // 
          this.pgLabels.BackColor = System.Drawing.SystemColors.Window;
          this.pgLabels.Controls.Add(this.udVisibleAtZoom);
          this.pgLabels.Controls.Add(this.tbFont);
          this.pgLabels.Controls.Add(this.lblTextColor);
          this.pgLabels.Controls.Add(this.lblFont);
          this.pgLabels.Controls.Add(this.cbxTextColor);
          this.pgLabels.Controls.Add(this.rbNameAndValue);
          this.pgLabels.Controls.Add(this.rbValue);
          this.pgLabels.Controls.Add(this.rbName);
          this.pgLabels.Controls.Add(this.rbNone);
          this.pgLabels.Controls.Add(this.lblLabelKind);
          this.pgLabels.Controls.Add(this.tbLabelFormat);
          this.pgLabels.Controls.Add(this.cbxLabelColumn);
          this.pgLabels.Controls.Add(this.lblVisibleAtZoom);
          this.pgLabels.Controls.Add(this.lblLabelColumn);
          this.pgLabels.Controls.Add(this.lblLabelFormat);
          this.pgLabels.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pgLabels.Location = new System.Drawing.Point(120, 1);
          this.pgLabels.Name = "pgLabels";
          this.pgLabels.Size = new System.Drawing.Size(315, 502);
          this.pgLabels.TabIndex = 3;
          this.pgLabels.Text = "Labels";
          // 
          // udVisibleAtZoom
          // 
          this.udVisibleAtZoom.Location = new System.Drawing.Point(236, 136);
          this.udVisibleAtZoom.Name = "udVisibleAtZoom";
          this.udVisibleAtZoom.Size = new System.Drawing.Size(64, 20);
          this.udVisibleAtZoom.TabIndex = 27;
          this.udVisibleAtZoom.ValueChanged += new System.EventHandler(this.udVisibleAtZoom_ValueChanged);
          // 
          // tbFont
          // 
          this.tbFont.Image = null;
          this.tbFont.Location = new System.Drawing.Point(128, 192);
          this.tbFont.Name = "tbFont";
          this.tbFont.Size = new System.Drawing.Size(172, 21);
          this.tbFont.TabIndex = 26;
          this.tbFont.ButtonClick += new System.EventHandler(this.tbFont_ButtonClick);
          // 
          // lblTextColor
          // 
          this.lblTextColor.AutoSize = true;
          this.lblTextColor.Location = new System.Drawing.Point(16, 224);
          this.lblTextColor.Name = "lblTextColor";
          this.lblTextColor.Size = new System.Drawing.Size(57, 13);
          this.lblTextColor.TabIndex = 25;
          this.lblTextColor.Text = "Text color:";
          // 
          // lblFont
          // 
          this.lblFont.AutoSize = true;
          this.lblFont.Location = new System.Drawing.Point(16, 196);
          this.lblFont.Name = "lblFont";
          this.lblFont.Size = new System.Drawing.Size(31, 13);
          this.lblFont.TabIndex = 24;
          this.lblFont.Text = "Font:";
          // 
          // cbxTextColor
          // 
          this.cbxTextColor.Color = System.Drawing.Color.Transparent;
          this.cbxTextColor.Location = new System.Drawing.Point(128, 220);
          this.cbxTextColor.Name = "cbxTextColor";
          this.cbxTextColor.ShowColorName = true;
          this.cbxTextColor.Size = new System.Drawing.Size(172, 21);
          this.cbxTextColor.TabIndex = 23;
          this.cbxTextColor.ColorSelected += new System.EventHandler(this.cbxTextColor_ColorSelected);
          // 
          // rbNameAndValue
          // 
          this.rbNameAndValue.AutoSize = true;
          this.rbNameAndValue.Location = new System.Drawing.Point(128, 104);
          this.rbNameAndValue.Name = "rbNameAndValue";
          this.rbNameAndValue.Size = new System.Drawing.Size(103, 17);
          this.rbNameAndValue.TabIndex = 20;
          this.rbNameAndValue.TabStop = true;
          this.rbNameAndValue.Text = "Name and value";
          this.rbNameAndValue.UseVisualStyleBackColor = true;
          this.rbNameAndValue.CheckedChanged += new System.EventHandler(this.rbNone_CheckedChanged);
          // 
          // rbValue
          // 
          this.rbValue.AutoSize = true;
          this.rbValue.Location = new System.Drawing.Point(128, 84);
          this.rbValue.Name = "rbValue";
          this.rbValue.Size = new System.Drawing.Size(52, 17);
          this.rbValue.TabIndex = 19;
          this.rbValue.TabStop = true;
          this.rbValue.Text = "Value";
          this.rbValue.UseVisualStyleBackColor = true;
          this.rbValue.CheckedChanged += new System.EventHandler(this.rbNone_CheckedChanged);
          // 
          // rbName
          // 
          this.rbName.AutoSize = true;
          this.rbName.Location = new System.Drawing.Point(128, 64);
          this.rbName.Name = "rbName";
          this.rbName.Size = new System.Drawing.Size(53, 17);
          this.rbName.TabIndex = 22;
          this.rbName.TabStop = true;
          this.rbName.Text = "Name";
          this.rbName.UseVisualStyleBackColor = true;
          this.rbName.CheckedChanged += new System.EventHandler(this.rbNone_CheckedChanged);
          // 
          // rbNone
          // 
          this.rbNone.AutoSize = true;
          this.rbNone.Location = new System.Drawing.Point(128, 44);
          this.rbNone.Name = "rbNone";
          this.rbNone.Size = new System.Drawing.Size(51, 17);
          this.rbNone.TabIndex = 21;
          this.rbNone.TabStop = true;
          this.rbNone.Text = "None";
          this.rbNone.UseVisualStyleBackColor = true;
          this.rbNone.CheckedChanged += new System.EventHandler(this.rbNone_CheckedChanged);
          // 
          // lblLabelKind
          // 
          this.lblLabelKind.Location = new System.Drawing.Point(16, 20);
          this.lblLabelKind.Name = "lblLabelKind";
          this.lblLabelKind.Size = new System.Drawing.Size(284, 16);
          this.lblLabelKind.TabIndex = 18;
          this.lblLabelKind.Text = "Label kind:";
          // 
          // tbLabelFormat
          // 
          this.tbLabelFormat.Location = new System.Drawing.Point(128, 164);
          this.tbLabelFormat.Name = "tbLabelFormat";
          this.tbLabelFormat.Size = new System.Drawing.Size(172, 20);
          this.tbLabelFormat.TabIndex = 17;
          this.tbLabelFormat.Leave += new System.EventHandler(this.tbLabelFormat_Leave);
          // 
          // cbxLabelColumn
          // 
          this.cbxLabelColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbxLabelColumn.FormattingEnabled = true;
          this.cbxLabelColumn.Location = new System.Drawing.Point(128, 136);
          this.cbxLabelColumn.Name = "cbxLabelColumn";
          this.cbxLabelColumn.Size = new System.Drawing.Size(172, 21);
          this.cbxLabelColumn.TabIndex = 16;
          this.cbxLabelColumn.SelectedIndexChanged += new System.EventHandler(this.cbxLabelColumn_SelectedIndexChanged);
          // 
          // lblVisibleAtZoom
          // 
          this.lblVisibleAtZoom.AutoSize = true;
          this.lblVisibleAtZoom.Location = new System.Drawing.Point(16, 140);
          this.lblVisibleAtZoom.Name = "lblVisibleAtZoom";
          this.lblVisibleAtZoom.Size = new System.Drawing.Size(80, 13);
          this.lblVisibleAtZoom.TabIndex = 14;
          this.lblVisibleAtZoom.Text = "Visible at zoom:";
          // 
          // lblLabelColumn
          // 
          this.lblLabelColumn.AutoSize = true;
          this.lblLabelColumn.Location = new System.Drawing.Point(16, 140);
          this.lblLabelColumn.Name = "lblLabelColumn";
          this.lblLabelColumn.Size = new System.Drawing.Size(73, 13);
          this.lblLabelColumn.TabIndex = 14;
          this.lblLabelColumn.Text = "Label column:";
          // 
          // lblLabelFormat
          // 
          this.lblLabelFormat.AutoSize = true;
          this.lblLabelFormat.Location = new System.Drawing.Point(16, 168);
          this.lblLabelFormat.Name = "lblLabelFormat";
          this.lblLabelFormat.Size = new System.Drawing.Size(68, 13);
          this.lblLabelFormat.TabIndex = 15;
          this.lblLabelFormat.Text = "Label format:";
          // 
          // lblZoomPolygon
          // 
          this.lblZoomPolygon.Location = new System.Drawing.Point(16, 92);
          this.lblZoomPolygon.Name = "lblZoomPolygon";
          this.lblZoomPolygon.Size = new System.Drawing.Size(284, 16);
          this.lblZoomPolygon.TabIndex = 4;
          this.lblZoomPolygon.Text = "Zoom the polygon:";
          // 
          // lblZoomPolygonValue
          // 
          this.lblZoomPolygonValue.AutoSize = true;
          this.lblZoomPolygonValue.Location = new System.Drawing.Point(16, 120);
          this.lblZoomPolygonValue.Name = "lblZoomPolygonValue";
          this.lblZoomPolygonValue.Size = new System.Drawing.Size(37, 13);
          this.lblZoomPolygonValue.TabIndex = 1;
          this.lblZoomPolygonValue.Text = "Value:";
          // 
          // tbZoomPolygon
          // 
          this.tbZoomPolygon.Image = null;
          this.tbZoomPolygon.Location = new System.Drawing.Point(128, 116);
          this.tbZoomPolygon.Name = "tbZoomPolygon";
          this.tbZoomPolygon.Size = new System.Drawing.Size(172, 21);
          this.tbZoomPolygon.TabIndex = 3;
          this.tbZoomPolygon.ButtonClick += new System.EventHandler(this.tbZoomPolygon_ButtonClick);
          this.tbZoomPolygon.Leave += new System.EventHandler(this.tbZoomPolygon_Leave);
          // 
          // LayerEditorControl
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
          this.BackColor = System.Drawing.SystemColors.Control;
          this.Controls.Add(this.pcLayer);
          this.Name = "LayerEditorControl";
          this.Size = new System.Drawing.Size(436, 504);
          this.pcLayer.ResumeLayout(false);
          this.pgData.ResumeLayout(false);
          this.panel4.ResumeLayout(false);
          this.panel4.PerformLayout();
          this.panAppDataLayer.ResumeLayout(false);
          this.panAppDataLayer.PerformLayout();
          this.panShpLayer.ResumeLayout(false);
          this.panShpLayer.PerformLayout();
          this.panel1.ResumeLayout(false);
          this.panel1.PerformLayout();
          this.pgAppearance.ResumeLayout(false);
          this.pgAppearance.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.udBorderWidth)).EndInit();
          this.pgColorRanges.ResumeLayout(false);
          this.pgColorRanges.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.udNumberOfRanges)).EndInit();
          this.pgSizeRanges.ResumeLayout(false);
          this.pgSizeRanges.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.udSizeRanges)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.udEndSize)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.udStartSize)).EndInit();
          this.pgLabels.ResumeLayout(false);
          this.pgLabels.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.udVisibleAtZoom)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

      private FastReport.Controls.PageControl pcLayer;
      private FastReport.Controls.PageControlPage pgData;
      private FastReport.Controls.PageControlPage pgAppearance;
      private System.Windows.Forms.Label lblFilter;
      private System.Windows.Forms.Label lblDataSource;
      private FastReport.Controls.TextBoxButton tbFilter;
      private System.Windows.Forms.ComboBox cbxDataSource;
      private FastReport.Controls.TextBoxButton tbAnalyticalValue;
      private FastReport.Controls.TextBoxButton tbSpatialValue;
      private System.Windows.Forms.ComboBox cbxSpatialColumn;
      private System.Windows.Forms.ComboBox cbxFunction;
      private System.Windows.Forms.Label lblSpatialColumn;
      private System.Windows.Forms.Label lblAnalyticalValue;
      private System.Windows.Forms.Label lblSpatialValue;
      private System.Windows.Forms.Label lblFunction;
      private FastReport.Controls.LabelLine lblSpatialData;
      private FastReport.Controls.PageControlPage pgColorRanges;
      private FastReport.Controls.LabelLine labelLine2;
      private System.Windows.Forms.CheckBox cbVisible;
      private FastReport.Controls.PageControlPage pgLabels;
      private System.Windows.Forms.ComboBox cbxBorderStyle;
      private System.Windows.Forms.Label lblBorderStyle;
      private System.Windows.Forms.Label lblBorderColor;
      private FastReport.Controls.ColorComboBox cbxBorderColor;
      private System.Windows.Forms.NumericUpDown udBorderWidth;
      private System.Windows.Forms.Label lblBorderWidth;
      private System.Windows.Forms.Label lblFillColor;
      private FastReport.Controls.ColorComboBox cbxFillColor;
      private System.Windows.Forms.RadioButton rbNameAndValue;
      private System.Windows.Forms.RadioButton rbValue;
      private System.Windows.Forms.RadioButton rbName;
      private System.Windows.Forms.RadioButton rbNone;
      private FastReport.Controls.LabelLine lblLabelKind;
      private System.Windows.Forms.TextBox tbLabelFormat;
      private System.Windows.Forms.ComboBox cbxLabelColumn;
      private System.Windows.Forms.Label lblLabelColumn;
      private System.Windows.Forms.Label lblLabelFormat;
      private System.Windows.Forms.Label lblEndColor;
      private System.Windows.Forms.Label lblMiddleColor;
      private System.Windows.Forms.Label lblStartColor;
      private FastReport.Controls.ColorComboBox cbxEndColor;
      private FastReport.Controls.ColorComboBox cbxMiddleColor;
      private FastReport.Controls.ColorComboBox cbxStartColor;
      private System.Windows.Forms.CheckBox cbShowInColorScale;
      private System.Windows.Forms.Label lblEnd;
      private System.Windows.Forms.Label lblStart;
      private System.Windows.Forms.Label lblColor;
      private FastReport.Controls.LabelLine labelLine5;
      private System.Windows.Forms.NumericUpDown udNumberOfRanges;
      private System.Windows.Forms.Label lblNumberOfRanges;
      private FastReport.Controls.LabelLine labelLine4;
      private FastReport.Controls.TextBoxButton tbFont;
      private System.Windows.Forms.Label lblTextColor;
      private System.Windows.Forms.Label lblFont;
      private FastReport.Controls.ColorComboBox cbxTextColor;
      private FastReport.Controls.LabelLine lblAnalyticalData;
      private System.Windows.Forms.Panel panAppDataLayer;
      private System.Windows.Forms.Panel panShpLayer;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.Panel panel4;
      private System.Windows.Forms.Label lblLabelValue;
      private FastReport.Controls.TextBoxButton tbLatitudeValue;
      private System.Windows.Forms.Label lblLongitudeValue;
      private FastReport.Controls.TextBoxButton tbLongitudeValue;
      private System.Windows.Forms.Label lblLatitudeValue;
      private FastReport.Controls.TextBoxButton tbLabelValue;
      private FastReport.Controls.PageControlPage pgSizeRanges;
      private System.Windows.Forms.Label lblEnd1;
      private System.Windows.Forms.Label lblStart1;
      private System.Windows.Forms.Label lblSize;
      private System.Windows.Forms.NumericUpDown udSizeRanges;
      private System.Windows.Forms.Label lblSizeRanges;
      private System.Windows.Forms.NumericUpDown udEndSize;
      private System.Windows.Forms.Label lblEndSize;
      private System.Windows.Forms.NumericUpDown udStartSize;
      private System.Windows.Forms.Label lblStartSize;
      private System.Windows.Forms.NumericUpDown udVisibleAtZoom;
      private System.Windows.Forms.Label lblVisibleAtZoom;
      private System.Windows.Forms.ComboBox cbxPalette;
      private System.Windows.Forms.Label lblPalette;
      private FastReport.Controls.LabelLine lblZoomPolygon;
      private FastReport.Controls.TextBoxButton tbZoomPolygon;
      private System.Windows.Forms.Label lblZoomPolygonValue;

      }
}
