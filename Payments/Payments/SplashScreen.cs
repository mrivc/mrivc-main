﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Payments.DS;
using System.Threading;

namespace Payments
{
    public partial class SplashScreen : Form
    {
        //Delegate for cross thread call to close
        private delegate void CloseDelegate();

        //The type of form to be displayed as the splash screen.
        private static SplashScreen splashForm;

        public SplashScreen()
        {
            InitializeComponent();

            txtAppVersion.Text = "(" + Application.ProductVersion + ")";
            lblDataBase.Text = "База данных: " + DAccess.DataModule.ConnectionPaymentsDBName;
            lblServer.Text = "Сервер: " + DAccess.DataModule.ConnectionPaymentsServerName;
        }

        static public void ShowSplashScreen()
        {
            // Make sure it is only launched once.

            if (splashForm != null)
                return;
            Thread thread = new Thread(new ThreadStart(SplashScreen.ShowForm));
            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        static private void ShowForm()
        {
            splashForm = new SplashScreen();
            Application.Run(splashForm);
        }

        static public void CloseForm()
        {
            try
            {
                splashForm.Invoke(new CloseDelegate(SplashScreen.CloseFormInternal));
            } catch { }
        }

        static private void CloseFormInternal()
        {
            splashForm.Close();
        }
    }
}
