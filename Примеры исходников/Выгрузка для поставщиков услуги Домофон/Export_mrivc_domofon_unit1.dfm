object frmUSZN: TfrmUSZN
  Left = 293
  Top = 0
  ClientHeight = 378
  ClientWidth = 822
  Color = clBtnFace
  Constraints.MinHeight = 205
  Constraints.MinWidth = 598
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  DesignSize = (
    822
    378)
  PixelsPerInch = 96
  TextHeight = 13
  inline AddrFilter: TAddrParamFilter
    Left = 475
    Top = 40
    Width = 139
    Height = 57
    AutoSize = True
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 475
    ExplicitTop = 40
    inherited gbAddressParam: TGroupBox
      inherited btnSelect: TButton
        OnClick = AddrFilterbtnSelectClick
      end
      inherited cbAll: TCheckBox
        Left = 5
        Top = 26
        OnClick = AddrFiltercbAllClick
        ExplicitLeft = 5
        ExplicitTop = 26
      end
    end
  end
  object JvFooter1: TJvFooter
    Left = 0
    Top = 334
    Width = 822
    Height = 44
    Align = alBottom
    BevelVisible = True
    DesignSize = (
      822
      44)
    object JvFooterBtn1: TJvFooterBtn
      Left = 8
      Top = 5
      Width = 137
      Height = 34
      Anchors = [akLeft, akBottom]
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100
      TabOrder = 0
      OnClick = JvFooterBtn1Click
      Alignment = taLeftJustify
      Flat = True
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -11
      HotTrackFont.Name = 'Tahoma'
      HotTrackFont.Style = []
      ImageIndex = 15
      ButtonIndex = 0
      SpaceInterval = 6
    end
    object JvFooterBtn2: TJvFooterBtn
      Left = 677
      Top = 5
      Width = 137
      Height = 34
      Anchors = [akRight, akBottom]
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 1
      OnClick = JvFooterBtn2Click
      Flat = True
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -11
      HotTrackFont.Name = 'Tahoma'
      HotTrackFont.Style = []
      ImageIndex = 13
      ButtonIndex = 1
      SpaceInterval = 6
      ExplicitLeft = 425
    end
  end
  object JvEdit2: TJvEdit
    Left = 8
    Top = 8
    Width = 742
    Height = 21
    Modified = False
    Anchors = [akLeft, akTop, akRight]
    Enabled = False
    TabOrder = 2
  end
  object JvBitBtn2: TJvBitBtn
    Left = 756
    Top = 8
    Width = 58
    Height = 23
    Anchors = [akTop, akRight]
    Caption = #1092#1072#1081#1083
    TabOrder = 3
    OnClick = JvBitBtn2Click
    HotTrackFont.Charset = DEFAULT_CHARSET
    HotTrackFont.Color = clWindowText
    HotTrackFont.Height = -11
    HotTrackFont.Name = 'Tahoma'
    HotTrackFont.Style = []
  end
  inline SelectEditFilter1: TSelectEditFilter
    Left = 240
    Top = 40
    Width = 225
    Height = 57
    AutoSize = True
    TabOrder = 4
    ExplicitLeft = 240
    ExplicitTop = 40
    inherited GB: TcxGroupBox
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    end
  end
  inline PeriodFilter1: TPeriodFilter
    Left = 8
    Top = 40
    Width = 225
    Height = 57
    AutoSize = True
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 8
    ExplicitTop = 40
  end
  object dbf1: TDbf
    DateTimeHandling = dtDateTime
    IndexDefs = <>
    OpenMode = omAutoCreate
    TableLevel = 3
    Left = 624
    Top = 40
  end
  object JvSaveDialog1: TJvSaveDialog
    DefaultExt = '.dbf'
    Filter = 'DBF File|*.dbf'
    Height = 419
    Width = 563
    Left = 408
    Top = 57
  end
  object ADOStoredProc1: TADOStoredProc
    CommandTimeout = 0
    ProcedureName = 'reports.Export_mrivc_domofon'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@id_Provider_Group'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@period'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 624
    Top = 80
  end
  object JvThread1: TJvThread
    Exclusive = True
    MaxCount = 0
    RunOnCreate = True
    FreeOnTerminate = True
    ThreadDialog = JvThreadSimpleDialog1
    OnExecute = JvThreadExecute
    Left = 664
    Top = 40
  end
  object JvThreadSimpleDialog1: TJvThreadSimpleDialog
    DialogOptions.FormStyle = fsNormal
    DialogOptions.ShowDialog = True
    DialogOptions.CancelButtonCaption = 'Cancel'
    DialogOptions.Caption = #1042#1099#1075#1088#1091#1079#1082#1072' '#1076#1072#1085#1085#1099#1093
    DialogOptions.ShowCancelButton = False
    Left = 704
    Top = 40
  end
  object ADOQuery1: TADOQuery
    CommandTimeout = 0
    Parameters = <>
    Left = 664
    Top = 80
  end
end
