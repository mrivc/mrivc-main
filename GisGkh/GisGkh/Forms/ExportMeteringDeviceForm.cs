﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class ExportMeteringDeviceForm : Form
    {
        public ExportMeteringDeviceForm()
        {
            InitializeComponent();
        }

        private void ExportMeteringDeviceForm_Load(object sender, EventArgs e)
        {

        }

        private void exportBtn_Click(object sender, EventArgs e)
        {
            var ds = MrivcStatic.HouseIteration(new ExportMeteringDevice(""), true);

            meterDataGrid.DataSource = ds.Tables["Meter"];
        }

        private void exportToDbBtn_Click(object sender, EventArgs e)
        {
            var exportMeter = new ExportMeteringDevice("");
            exportMeter.Execute();
        }
    }
}
