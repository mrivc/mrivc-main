﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ImportPayments : MrivcImportCommand
    {
        bool _isRSO;
        bool _preview = true;

        public ImportPayments(string senderID) : base("importHouseUOData", "HomeManagementAsync", senderID)
        {
            _importValueName = "NotificationOfOrderExecutionType";
            Description = "Загрузка платежей";
        }

        public new void Execute()
        {
            base.Execute();
        }

        public DataSet PrepareToDataSet()
        {
            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.Prepare_Import_Payments", new object[,] { { "@preview", _preview } });
        }

        protected override void CreateXmlBody()
        {
            _preview = false;
            var ds = PrepareToDataSet();
            _preview = true;

            var commonValues = new List<Tuple<string[], string>>();

                foreach (DataRow row in ds.Tables["House"].Rows)
                {
                    commonValues = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "FIASHouseGuid" },"FIASHouseGuid"),
                        new Tuple<string[], string>( new [] { "OKTMO","code" },"OKTMO_code"),
                        new Tuple<string[], string>( new [] { "OKTMO","name" },"OKTMO_name"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","Code" },"OlsonTZ_Code"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","GUID" },"OlsonTZ_GUID"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","Name" },"OlsonTZ_Name"),
                    };

                    var addTemplate = "";
                    var updateTemplate = "";

                    if (row["CadastralNumber"] == DBNull.Value)
                    {
                        addTemplate = "importHouseRSO_add_house";
                        updateTemplate = "importHouseRSO_update_house";
                    }
                    else
                    {
                        addTemplate = "importHouseRSO_add_house_cn";
                        updateTemplate = "importHouseRSO_update_house_cn";
                        commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                    }

                    AddChildElement(row, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, null, "importHouseRSORequest");
                }

                commonValues = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "EntranceNum" }, "EntranceNum")
                };

                var updateValues = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "EntranceGUID" }, "EntranceGUID")
                };

                foreach (DataRow row in ds.Tables["Entrance"].Rows)
                {
                    AddChildElement(row, "HomeManagementAsync", "importHouseRSO_add_entrance", "importHouseRSO_update_entrance", commonValues, null, updateValues, "ApartmentHouse");
                }

                foreach (DataRow pr in ds.Tables["NrPremises"].Rows)
                {
                    commonValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "PremisesNum" }, "PremisesNum"),
                            new Tuple<string[], string>( new [] { "TotalArea" }, "TotalArea"),
                        };

                    updateValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "PremisesGUID" }, "PremisesGUID")
                        };

                    var addTemplate = "";
                    var updateTemplate = "";

                    if (pr["CadastralNumber"] == DBNull.Value)
                    {
                        addTemplate = "importHouseRSO_add_nrp";
                        updateTemplate = "importHouseRSO_update_nrp";
                    }
                    else
                    {
                        addTemplate = "importHouseRSO_add_nrp_cn";
                        updateTemplate = "importHouseRSO_update_nrp_cn";
                        commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                    }

                    AddChildElement(pr, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, updateValues, "ApartmentHouse");
                }

                foreach (DataRow row in ds.Tables["Premises"].Rows)
                {
                    commonValues = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "PremisesNum" }, "PremisesNum"),
                        new Tuple<string[], string>( new [] { "EntranceNum" }, "EntranceNum"),
                        new Tuple<string[], string>( new [] { "PremisesCharacteristic", "Code" }, "PremisesCharacteristicCode"),
                        new Tuple<string[], string>( new [] { "PremisesCharacteristic", "GUID" }, "PremisesCharacteristicGUID"),
                        new Tuple<string[], string>( new [] { "PremisesCharacteristic", "Name" }, "PremisesCharacteristicName"),
                        new Tuple<string[], string>( new [] { "TotalArea" }, "TotalArea"),
                    };

                    updateValues = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "PremisesGUID" }, "PremisesGUID")
                    };

                    var addTemplate = "";
                    var updateTemplate = "";

                    if (row["CadastralNumber"] == DBNull.Value)
                    {
                        addTemplate = "importHouseRSO_add_flat";
                        updateTemplate = "importHouseRSO_update_flat";
                    }
                    else
                    {
                        addTemplate = "importHouseRSO_add_flat_cn";
                        updateTemplate = "importHouseRSO_update_flat_cn";
                        commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                    }

                    AddChildElement(row, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, updateValues, "ApartmentHouse");
                }
        }
    }
}
