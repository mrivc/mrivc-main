﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MtecExcelConverter
{
    using System.Data;
    using System.Data.SqlClient;

    using MtecExcelConverter.DataBase;

    public static class DBManager
    {
        public static string MainConnection
        {
            get
            {
                return Properties.Settings.Default.MurmanskConnectionString;
            }
        }

        private static SqlConnection connection;
        public static SqlConnection Connection
        {
            get
            {
                if (connection != null) return connection;
                
                connection = new SqlConnection(MainConnection);
                return connection;
            }
        }

        public static void InserData(DataTable sourceDataTable, string distanationTableName)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(MainConnection))
            {
                bulkCopy.DestinationTableName = distanationTableName;
                bulkCopy.WriteToServer(sourceDataTable);
            }
        }
    }
}
