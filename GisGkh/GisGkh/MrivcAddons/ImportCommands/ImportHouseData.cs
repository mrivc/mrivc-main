﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ImportHouseData : MrivcImportCommand, ISingleHouseCommand, IMrivcImportCommandAsync
    {
        bool _isRSO;
        bool _preview = true;

        public string FiasHouseGuid { get; set; } = null;

        ExportHouseData exportHouseData;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                if (_isRSO)
                    Method = "importHouseRSOData";
                else
                    Method = "importHouseUOData";
                base.SenderID = value;
            }
        }

        public ImportHouseData(string senderID ) : base("importHouseUOData", "HomeManagementAsync", senderID)
        {
            _importValueName = "ApartmentHouse";
            _importValueName2 = "LivingHouse";
            Description = "Синхронизация информации о домах, подъездах, помещениях";

            exportHouseData = new ExportHouseData(SenderID);
        }

        public void ExportData()
        {
            if (!this.IsEmpty && !HasErrors)
            {
                exportHouseData.FiasHouseGuid = FiasHouseGuid;
                exportHouseData.Description = Description;
                exportHouseData.AddToReport = AddToReport;
                exportHouseData.CommandType = CommandType;
                exportHouseData.Execute();

                HasErrors = exportHouseData.HasErrors;
            }
        }

        public void ImportData()
        {
            //bool check =  (bool)DAccess.DataModule.ExecuteScalarStoredProcedure1("gis.CheckHouseDataFull", new[,] { { "@senderID", SenderID }, { "@fiasHouseGuid", FiasHouseGuid } });
            bool check = true;
            base.Execute();
        }

        public new void Execute()
        {
            ExportData();
            ImportData();
            ExportData(); 
        }

        public override object Clone()
        {
            var exportHouseDataClone = (ExportHouseData)exportHouseData.Clone();
            var baseClone = (MrivcCommand)base.Clone();
            ((ImportHouseData)baseClone).exportHouseData = exportHouseDataClone;
            return baseClone;
        }

        public DataSet PrepareToDataSet()
        {
            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.Prepare_Import_House_Full", new object[,] { { "@SenderID", SenderID }, { "@preview", _preview }, { "@empty", exportHouseData.HasErrors }, { "@FiasHouseGuid", FiasHouseGuid } }, "House", "Entrance", "Premises", "NrPremises");
        }

        protected override void CreateXmlBody()
        {
            _preview = false;
            var ds = PrepareToDataSet();
            _preview = true;

            var commonValues = new List<Tuple<string[], string>>();
            var updateValues = new List<Tuple<string[], string>>();

            if (_isRSO)
            {
                foreach (DataRow row in ds.Tables["House"].Rows)
                {
                    commonValues = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "FIASHouseGuid" },"FIASHouseGuid"),
                        new Tuple<string[], string>( new [] { "OKTMO","code" },"OKTMO_code"),
                        new Tuple<string[], string>( new [] { "OKTMO","name" },"OKTMO_name"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","Code" },"OlsonTZ_Code"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","GUID" },"OlsonTZ_GUID"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","Name" },"OlsonTZ_Name"),
                    };

                    var addTemplate = "";
                    var updateTemplate = "";

                    if (row["ObjectTypeID"].ToString() == "3")
                    {
                        if (row["CadastralNumber"] == DBNull.Value)
                        {
                            addTemplate = "importHouseRSO_add_house_wo_gkn";
                            updateTemplate = "importHouseRSO_update_house_wo_gkn";
                        }
                        else
                        {
                            addTemplate = "importHouseRSO_add_house_cn";
                            updateTemplate = "importHouseRSO_update_house_cn";
                            commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                        }
                    }
                    else if (row["ObjectTypeID"].ToString() == "4")
                    {
                        if (row["CadastralNumber"] == DBNull.Value)
                        {
                            addTemplate = "importHouseRSO_add_living_house_wo_gkn";
                            updateTemplate = "importHouseRSO_update_living_house_wo_gkn";
                        }
                        else
                        {
                            addTemplate = "importHouseRSO_add_living_house_cn";
                            updateTemplate = "importHouseRSO_update_living_house_cn";
                            commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                        }
                    }

                    

                    var addedHouse = AddChildElement(row, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, null, "importHouseRSORequest");

                    if (row["ObjectTypeID"].ToString() == "3")
                    {
                        foreach (DataRow pr in ds.Tables["NrPremises"].Rows)
                        {
                            commonValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "PremisesNum" }, "PremisesNum"),
                            //new Tuple<string[], string>( new [] { "TotalArea" }, "TotalArea"),
                        };

                            updateValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "PremisesGUID" }, "PremisesGUID")
                        };

                            addTemplate = "";
                            updateTemplate = "";

                            if (pr["CadastralNumber"] == DBNull.Value)
                            {
                                addTemplate = "importHouseRSO_add_nrp";
                                updateTemplate = "importHouseRSO_update_nrp";
                            }
                            else
                            {
                                addTemplate = "importHouseRSO_add_nrp_cn";
                                updateTemplate = "importHouseRSO_update_nrp_cn";
                                commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                            }

                            AddChildElement(pr, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, updateValues, addedHouse);
                        }

                        commonValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "EntranceNum" }, "EntranceNum")
                        };

                        updateValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "EntranceGUID" }, "EntranceGUID")
                        };

                        foreach (DataRow ent in ds.Tables["Entrance"].Rows)
                        {
                            AddChildElement(ent, "HomeManagementAsync", "importHouseRSO_add_entrance", "importHouseRSO_update_entrance", commonValues, null, updateValues, addedHouse);
                        }



                        foreach (DataRow pr in ds.Tables["Premises"].Rows)
                        {
                            commonValues = new List<Tuple<string[], string>>
                            {
                                new Tuple<string[], string>( new [] { "PremisesNum" }, "PremisesNum"),
                                new Tuple<string[], string>( new [] { "EntranceNum" }, "EntranceNum"),
                                new Tuple<string[], string>( new [] { "PremisesCharacteristic", "Code" }, "PremisesCharacteristicCode"),
                                new Tuple<string[], string>( new [] { "PremisesCharacteristic", "GUID" }, "PremisesCharacteristicGUID"),
                                new Tuple<string[], string>( new [] { "PremisesCharacteristic", "Name" }, "PremisesCharacteristicName"),
                                //new Tuple<string[], string>( new [] { "TotalArea" }, "TotalArea"),
                            };

                            updateValues = new List<Tuple<string[], string>>
                            {
                                new Tuple<string[], string>( new [] { "PremisesGUID" }, "PremisesGUID")
                            };

                            addTemplate = "";
                            updateTemplate = "";

                            if (pr["CadastralNumber"] == DBNull.Value)
                            {
                                addTemplate = "importHouseRSO_add_flat";
                                updateTemplate = "importHouseRSO_update_flat";
                            }
                            else
                            {
                                addTemplate = "importHouseRSO_add_flat_cn";
                                updateTemplate = "importHouseRSO_update_flat_cn";
                                commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                            }

                            var addedPremise = AddChildElement(pr, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, updateValues, addedHouse);

                            if (pr["EntranceNum"] == DBNull.Value)
                            {
                                RemoveElement(new[] { "EntranceNum" }, addedPremise);
                            }
                            else
                            {
                                RemoveElement(new[] { "HasNoEntrance" }, addedPremise);
                            }
                        }
                    }
                    else if (row["ObjectTypeID"].ToString() == "4")
                    {
                        foreach (DataRow pr in ds.Tables["Premises"].Rows)
                        {
                            commonValues = new List<Tuple<string[], string>>
                            {
                                new Tuple<string[], string>( new [] { "RoomNumber" }, "PremisesNum"),
                                //new Tuple<string[], string>( new [] { "Square" }, "TotalArea"),
                            };

                            updateValues = new List<Tuple<string[], string>>
                            {
                                new Tuple<string[], string>( new [] { "LivingRoomGUID" }, "PremisesGUID")
                            };

                            addTemplate = "";
                            updateTemplate = "";

                            if (pr["CadastralNumber"] == DBNull.Value)
                            {
                                addTemplate = "importHouseRSO_add_room";
                                updateTemplate = "importHouseRSO_update_room";
                            }
                            else
                            {
                                addTemplate = "importHouseRSO_add_room_cn";
                                updateTemplate = "importHouseRSO_update_room_cn";
                                commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                            }

                            AddChildElement(pr, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, updateValues, addedHouse);
                        }
                    }
                }
            }
            else
            {

                foreach (DataRow row in ds.Tables["House"].Rows)
                {
                    var addTemplate = "";
                    var updateTemplate = "";

                    var createValues = new List<Tuple<string[], string>>{ };

                    if (row["ObjectTypeID"].ToString() == "3")
                    {

                        commonValues = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "FIASHouseGuid" },"FIASHouseGuid"),
                        new Tuple<string[], string>( new [] { "State","Code" },"State_Code"),
                        new Tuple<string[], string>( new [] { "State","GUID" },"State_GUID"),
                        new Tuple<string[], string>( new [] { "State","Name" },"State_Name"),
                        new Tuple<string[], string>( new [] { "UsedYear" },"UsedYear"),
                        new Tuple<string[], string>( new [] { "FloorCount" },"FloorCount"),
                        //new Tuple<string[], string>( new [] { "OKTMO","code" },"OKTMO_code"),
                        //new Tuple<string[], string>( new [] { "OKTMO","name" },"OKTMO_name"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","Code" },"OlsonTZ_Code"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","GUID" },"OlsonTZ_GUID"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","Name" },"OlsonTZ_Name"),
                        new Tuple<string[], string>( new [] { "CulturalHeritage" },"CulturalHeritage"),
                        new Tuple<string[], string>( new [] { "UndergroundFloorCount" },"UndergroundFloorCount"),
                        new Tuple<string[], string>( new [] { "MinFloorCount" },"MinFloorCount"),
                        new Tuple<string[], string>( new [] { "TotalSquare" },"TotalSquare")
                    };                        

                        if (row["CadastralNumber"] == DBNull.Value)
                        {
                            addTemplate = "importHouseUO_add_house";
                            updateTemplate = "importHouseUO_update_house";
                        }
                        else
                        {
                            addTemplate = "importHouseUO_add_house_cn";
                            updateTemplate = "importHouseUO_update_house_cn";
                            commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                        }
                    }
                    else if(row["ObjectTypeID"].ToString() == "4")
                    {
                        commonValues = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "FIASHouseGuid" },"FIASHouseGuid"),
                        new Tuple<string[], string>( new [] { "TotalSquare" },"TotalSquare"),
                        new Tuple<string[], string>( new [] { "State","Code" },"State_Code"),
                        new Tuple<string[], string>( new [] { "State","GUID" },"State_GUID"),
                        new Tuple<string[], string>( new [] { "State","Name" },"State_Name"),
                        new Tuple<string[], string>( new [] { "UsedYear" },"UsedYear"),
                        new Tuple<string[], string>( new [] { "FloorCount" },"FloorCount"),
                        //new Tuple<string[], string>( new [] { "OKTMO","code" },"OKTMO_code"),
                        //new Tuple<string[], string>( new [] { "OKTMO","name" },"OKTMO_name"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","Code" },"OlsonTZ_Code"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","GUID" },"OlsonTZ_GUID"),
                        new Tuple<string[], string>( new [] { "OlsonTZ","Name" },"OlsonTZ_Name"),
                        
                    };

                        if (row["CadastralNumber"] == DBNull.Value)
                        {
                            addTemplate = "importHouseUO_add_living_house";
                            updateTemplate = "importHouseUO_update_living_house";
                        }
                        else
                        {
                            addTemplate = "importHouseUO_add_living_house_cn";
                            updateTemplate = "importHouseUO_update_living_house_cn";
                            commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                        }
                    }

                    var addedHouse = AddChildElement(row, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, null, "importHouseUORequest");

                    if (row["ObjectTypeID"].ToString() == "3")
                    {

                        foreach (DataRow pr in ds.Tables["NrPremises"].Rows)
                        {
                            commonValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "PremisesNum" }, "PremisesNum"),
                            new Tuple<string[], string>( new [] { "TotalArea" }, "TotalArea"),
                            //new Tuple<string[], string>( new [] { "IsCommonProperty" }, "IsCommonProperty")
                        };

                            updateValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "PremisesGUID" }, "PremisesGUID")
                        };

                            addTemplate = "";
                            updateTemplate = "";

                            if (pr["CadastralNumber"] == DBNull.Value)
                            {
                                addTemplate = "importHouseUO_add_nrp";
                                updateTemplate = "importHouseUO_update_nrp";
                            }
                            else
                            {
                                addTemplate = "importHouseUO_add_nrp_cn";
                                updateTemplate = "importHouseUO_update_nrp_cn";
                                commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                            }

                            AddChildElement(pr, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, updateValues, addedHouse);
                        }

                        commonValues = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "EntranceNum" }, "EntranceNum")

                    };

                        createValues = new List<Tuple<string[], string>>
                    {
                       new Tuple<string[], string>( new [] { "CreationYear" }, "CreationYear")
                    };

                        updateValues = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "EntranceGUID" }, "EntranceGUID")
                    };

                        foreach (DataRow ent in ds.Tables["Entrance"].Rows)
                        {
                            AddChildElement(ent, "HomeManagementAsync", "importHouseUO_add_entrance", "importHouseUO_update_entrance", commonValues, createValues, updateValues, addedHouse);
                        }


                        foreach (DataRow pr in ds.Tables["Premises"].Rows)
                        {
                            commonValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "PremisesNum" }, "PremisesNum"),
                            new Tuple<string[], string>( new [] { "EntranceNum" }, "EntranceNum"),
                            new Tuple<string[], string>( new [] { "PremisesCharacteristic", "Code" }, "PremisesCharacteristicCode"),
                            new Tuple<string[], string>( new [] { "PremisesCharacteristic", "GUID" }, "PremisesCharacteristicGUID"),
                            new Tuple<string[], string>( new [] { "PremisesCharacteristic", "Name" }, "PremisesCharacteristicName"),
                            new Tuple<string[], string>( new [] { "TotalArea" }, "TotalArea"),
                            new Tuple<string[], string>( new [] { "GrossArea" }, "GrossArea"),
                        };

                            updateValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "PremisesGUID" }, "PremisesGUID")
                        };

                            addTemplate = "";
                            updateTemplate = "";

                            if (pr["CadastralNumber"] == DBNull.Value)
                            {
                                addTemplate = "importHouseUO_add_flat";
                                updateTemplate = "importHouseUO_update_flat";
                            }
                            else
                            {
                                addTemplate = "importHouseUO_add_flat_cn";
                                updateTemplate = "importHouseUO_update_flat_cn";
                                commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                            }

                            var addedPremise = AddChildElement(pr, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, updateValues, addedHouse);

                            if (pr["EntranceNum"] == DBNull.Value)
                            {
                                RemoveElement(new[] { "EntranceNum" }, addedPremise);
                            }
                            else
                            {
                                RemoveElement(new[] { "HasNoEntrance" }, addedPremise);
                            }
                        }
                    }
                    else if (row["ObjectTypeID"].ToString() == "4")
                    {
                        foreach (DataRow pr in ds.Tables["Premises"].Rows)
                        {
                            commonValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "RoomNumber" }, "PremisesNum"),
                            //new Tuple<string[], string>( new [] { "Square" }, "TotalArea"),
                        };

                            updateValues = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new [] { "LivingRoomGUID" }, "PremisesGUID")
                        };

                            addTemplate = "";
                            updateTemplate = "";

                            if (pr["CadastralNumber"] == DBNull.Value)
                            {
                                addTemplate = "importHouseUO_add_room";
                                updateTemplate = "importHouseUO_update_room";
                            }
                            else
                            {
                                addTemplate = "importHouseUO_add_room_cn";
                                updateTemplate = "importHouseUO_update_room_cn";
                                commonValues.Add(new Tuple<string[], string>(new[] { "CadastralNumber" }, "CadastralNumber"));
                            }

                            AddChildElement(pr, "HomeManagementAsync", addTemplate, updateTemplate, commonValues, null, updateValues, addedHouse);
                        }
                    }
                }
            }
        }


    }
}
