﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public enum MrivcCommandType
    {
        ImportAccountData,
        ImportHouseData,
        ImportAdditionalServices,
        ImportMeteringDevice,
        ImportMeasureData,
        ImportMunicipalServices,
        ImportPaymentDocument2
    }
}
