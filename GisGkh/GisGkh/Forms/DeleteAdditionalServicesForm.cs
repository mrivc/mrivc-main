﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class DeleteAdditionalServicesForm : Form
    {
        public DeleteAdditionalServicesForm()
        {
            InitializeComponent();
        }

        private void btnDeleteAddServ_Click(object sender, EventArgs e)
        {
            var deleteAddServ = new DeleteAdditionalServices(senderIdOnlyFilter1.SenderID);
            deleteAddServ.AddServiceGuid = tbAddServGuid.Text;
            deleteAddServ.Execute();
        }

        private void DeleteAdditionalServicesForm_Load(object sender, EventArgs e)
        {
            senderIdOnlyFilter1.Fill();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var deleteAddServ = new DeleteMunicipalServices(senderIdOnlyFilter1.SenderID);
            deleteAddServ.MunServiceGuid = tbAddServGuid.Text;
            deleteAddServ.Execute();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            var recoverMun = new RecoverMunicipalServices(senderIdOnlyFilter1.SenderID);
            recoverMun.Execute();
        }
    }
}
