﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ImportAccountData : MrivcImportCommand, ISingleHouseCommand
    {
        bool _isRSO;
        bool _preview = true;

        public string FiasHouseGuid { get; set; } = null;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = (bool)DAccess.DataModule.ExecuteScalarQueryCommand1("select isRso from gis.Provider where senderID = @senderID", new[,] { { "senderID", value } });
                base.SenderID = value;
            }
        }

        public ImportAccountData(string senderID) : base("importAccountData", "HomeManagementAsync", senderID)
        {
            _maxImportValue = 100;
            _importValueName = "Account";
            Description = "Синхронизация информации о лицевых счетах";
        }

        public void Execute()
        {
            bool check = (bool)DAccess.DataModule.ExecuteScalarStoredProcedure1("gis.CheckAccountDataFull", new[,] { { "@senderID", SenderID }, { "@fiasHouseGuid", FiasHouseGuid } });

            base.Execute(check);

            if (!this.IsEmpty && !HasErrors)
            {
                var exportAccountData = new ExportAccountData(SenderID);
                exportAccountData.FiasHouseGuid = FiasHouseGuid;
                exportAccountData.Description = Description;
                exportAccountData.AddToReport = AddToReport;
                exportAccountData.Execute();
            }
        }

        public DataSet PrepareToDataSet()
        {
            var exportAccountData = new ExportAccountData(SenderID);
            exportAccountData.FiasHouseGuid = FiasHouseGuid;
            exportAccountData.Description = Description;
            exportAccountData.AddToReport = AddToReport;
            exportAccountData.Execute();

            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.Prepare_Import_Account", new object[,] { { "@SenderID", SenderID }, { "@empty",exportAccountData.HasErrors }, { "@preview", _preview }, { "@FiasHouseGuid", FiasHouseGuid } }, "Account");
        }

        protected override void CreateXmlBody()
        {
            _preview = false;
            var _preparedDataSet = PrepareToDataSet();
            _preview = true;

            var updateValues = new List<Tuple<string[], string>>
            {
                 new Tuple<string[], string>(new [] { "AccountGUID" }, "AccountGUID" )
            };

            if (!_isRSO)
            {
                var commonValues = new List<Tuple<string[], string>>
                {
                        new Tuple<string[], string>(new [] { "isUOAccount" }, "isUOAccount" ),
                        new Tuple<string[], string>(new [] { "TotalSquare" }, "TotalSquare" ),
                        new Tuple<string[], string>(new [] { "Accommodation", "PremisesGUID" }, "PremisesGUID" ),
                        new Tuple<string[], string>(new [] { "PayerInfo","Ind","Surname" }, "Surname" ),
                        new Tuple<string[], string>(new [] { "PayerInfo", "Ind", "FirstName" }, "FirstName" ),
                        new Tuple<string[], string>(new [] { "AccountNumber" }, "AccountNumber" ),
                        new Tuple<string[], string>(new [] { "CreationDate" }, "CreationDate" )
                };

                foreach (DataRow row in _preparedDataSet.Tables["Account"].Select("isUOAccount = 'true'"))
                {
                    AddChildElement(row, "HomeManagementAsync", "importAccount_add", "importAccount_update", commonValues, null, updateValues, "importAccountRequest");
                    CounterCheck();
                }

                commonValues = new List<Tuple<string[], string>>
                {
                        new Tuple<string[], string>(new [] { "isCRAccount" }, "isCRAccount" ),
                        new Tuple<string[], string>(new [] { "TotalSquare" }, "TotalSquare" ),
                        new Tuple<string[], string>(new [] { "Accommodation", "PremisesGUID" }, "PremisesGUID" ),
                        new Tuple<string[], string>(new [] { "PayerInfo","Ind","Surname" }, "Surname" ),
                        new Tuple<string[], string>(new [] { "PayerInfo", "Ind", "FirstName" }, "FirstName" ),
                        new Tuple<string[], string>(new [] { "AccountNumber" }, "AccountNumber" ),
                        new Tuple<string[], string>(new [] { "CreationDate" }, "CreationDate" )
                };

                foreach (DataRow row in _preparedDataSet.Tables["Account"].Select("isCRAccount = 'true'"))
                {
                    AddChildElement(row, "HomeManagementAsync", "importAccount_cr_add", "importAccount_cr_update", commonValues, null, updateValues, "importAccountRequest");
                    CounterCheck();
                }
            }
            else
            {
                var commonValues = new List<Tuple<string[], string>>
                {
                        new Tuple<string[], string>(new [] { "isRSOAccount" }, "isRSOAccount" ),
                        new Tuple<string[], string>(new [] { "TotalSquare" }, "TotalSquare" ),
                        new Tuple<string[], string>(new [] { "Accommodation", "PremisesGUID" }, "PremisesGUID" ),
                        new Tuple<string[], string>(new [] { "PayerInfo","Ind","Surname" }, "Surname" ),
                        new Tuple<string[], string>(new [] { "PayerInfo", "Ind", "FirstName" }, "FirstName" ),
                        new Tuple<string[], string>(new [] { "AccountNumber" }, "AccountNumber" ),
                        new Tuple<string[], string>(new [] { "CreationDate" }, "CreationDate" )
                };

                foreach (DataRow row in _preparedDataSet.Tables["Account"].Select())
                {
                    AddChildElement(row, "HomeManagementAsync", "importAccount_rso_add", "importAccount_rso_update", commonValues, null, updateValues, "importAccountRequest");
                    CounterCheck();
                }
            }
        }
    }
}
