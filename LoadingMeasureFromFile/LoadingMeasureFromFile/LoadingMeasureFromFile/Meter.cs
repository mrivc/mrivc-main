﻿using System;

namespace LoadingMeasureFromFile
{
    /// <summary>
    /// Класс для хранения данных по счетчику из файла
    /// </summary>
    public class Meter
    {
        public int IdMeter { get; }
        public DateTime MeasureDate { get; }
        public double MeasureValue { get; }

        public string SqlFormattedDate => MeasureDate.ToString("dd-MM-yyyy HH:mm:ss");

        public Meter(int id, DateTime date, double value)
        {
            IdMeter = id;
            MeasureDate = date;
            MeasureValue = value;
        }
    }
}
