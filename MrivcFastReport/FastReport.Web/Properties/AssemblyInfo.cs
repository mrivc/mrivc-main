﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FastReport.Web")]
[assembly: AssemblyDescription("FastReport.Net Web part")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fast Reports Inc.")]
[assembly: AssemblyProduct("FastReport.Net")]
[assembly: AssemblyCopyright("Copyright © Fast Reports Inc. 2007-2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AllowPartiallyTrustedCallers]
[assembly: CLSCompliant(true)]
//[assembly: SecurityRules(SecurityRuleSet.Level1)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e76d6d7c-6c6d-40cb-ab14-6d986267ea0c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2015.2.8.0")]

// Resources
[assembly: WebResource("FastReport.Web.Resources.Images.Progress.gif", "image/gif")]
[assembly: WebResource("FastReport.Web.Resources.Images.Checkbox.gif", "image/gif")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_background_dark.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_background_dark_32.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_background_light.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_background_light_32.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_background_medium.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_background_medium_32.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_black.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_black_32.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_blue.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_blue_32.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_disabled.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_disabled_32.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_green.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_green_32.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_red.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.Images.toolbar_red_32.png", "image/png")]

// Toolbar
[assembly: WebResource("FastReport.Web.Resources.Templates.fr_util.js", "text/js")]
[assembly: WebResource("FastReport.Web.Resources.Templates.toolbar.html", "text/html")]
[assembly: WebResource("FastReport.Web.Resources.Templates.styles.css", "text/css")]
[assembly: WebResource("FastReport.Web.Resources.Templates.styles_big.css", "text/css")]

// jquery js
[assembly: WebResource("FastReport.Web.Resources.jquery.jquery-ui.custom.min.js", "text/js")]
[assembly: WebResource("FastReport.Web.Resources.jquery.jquery.min.js", "text/js")]

// jquery css
[assembly: WebResource("FastReport.Web.Resources.jquery.jquery-ui.min.css", "text/css")]

// jquery images
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_diagonals-thick_18_b81900_40x40.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_diagonals-thick_20_666666_40x40.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_flat_10_000000_40x100.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_glass_100_f6f6f6_1x400.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_glass_100_fdf5ce_1x400.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_glass_65_ffffff_1x400.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_gloss-wave_35_f6a828_500x100.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_highlight-soft_100_eeeeee_1x100.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-bg_highlight-soft_75_ffe45c_1x100.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-icons_222222_256x240.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-icons_228ef1_256x240.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-icons_ef8c08_256x240.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-icons_ffd27a_256x240.png", "image/png")]
[assembly: WebResource("FastReport.Web.Resources.jquery.images.ui-icons_ffffff_256x240.png", "image/png")]
