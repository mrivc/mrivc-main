﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using EllisBillFilter.DS;

namespace EllisBillFilter.Logic
{
    using System.Globalization;
    using System.Net.NetworkInformation;

    using EllisBillFilter.Helpers;
    using EllisBillFilter.Properties;

    using MrivcHelpers.Helpers;
    using static MDataSet;
    using System.Threading;
    public static class ReestrGenerator
    {
        private static DataTable data;
        // Таблица для данных из процедуры для Сбера
        private static DataTable dataSber;
        // Таблица для данных из процедуры для Сбера с показаниями
        private static DataTable dataSberMeasure;
        private static int period;
        private static bool isDebt;
        private static string path;

        private static bool useClientNameInFilePath = false;

        /// <summary>
        /// Авторассылка реестров сальдо по настроечной таблицы [Reports].[Client_Detail_Delivery_Settings]
        /// </summary>
        public static void SendReestNow()
        {
            ClientDetailDeliverySettingsDataTable clients = new ClientDetailDeliverySettingsDataTable();
            DAccess.DataModule.ClientDetailDeliverySettingsSelect(clients, DAccess.Instance.WorkPeriod, true);

            int period = DAccess.DataModule.GetWorkPeriod();
            DataRow periodInfo = DAccess.Instance.GetPeriodInfo(period);
            string periodName = periodInfo["Name"].ToString();

            foreach (var row in clients)
            {
                string path = Properties.Settings.Default.SberSaldoPath;

                MailSettings.CopyTo = Settings.Default.SendCopyTo;
                MailSettings.BodyText = Settings.Default.MailBody.Replace("@Period@", periodName);
                MailSettings.Subject = Settings.Default.MailSubject.Replace("@Period@", periodName).Replace("@CurrentDate@", string.Format("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString()));
                ReestrGenerator.GenerateAndSend(period, path, row.ID_Client, row.ID_Bill_Filter, row.EmailsAddresses.Replace(" ", ""), Settings.Default.IsSendReestrViaEmail, row.Is_Debt);
                Thread.Sleep(500);
                row.Last_Send = DateTime.Now;
                row.Last_Send_By = DAccess.Instance.UserId; //------------------------
                DAccess.DataModule.SberReestrLog(row.ID_Delivery, DateTime.Now, DAccess.Instance.UserId);
            }
        }

        public static bool GenerateAndSend(int period, string basePathToSave, int idClient, int idBillFilter, string emails, bool isSend, bool isDebt)
        {
            bool success = false;
            // 03.06.2022 Завьялова: Добавила параметр и исключила МРИВЦ, МТЭЦ и МЭС из формирования DBF реестров. Сделано для ускорения автоформирования.
            bool generateDBF = ((idClient == 11842 || idClient == 12026 || idClient == 11931) ? false : true);

            try
            {
                ReestrGenerator.path = basePathToSave;
                FileHelper.CreateDirectory(path);
                DataRow periodInfo = DAccess.Instance.GetPeriodInfo(period);
                int periodYear = (int)periodInfo["Year"];
                int periodMonth = (int)periodInfo["Month"];
                string periodName = periodInfo["Name"].ToString();
                string monthPeriod = string.Format("{0}{1}", periodYear, StringHelper.ZeroCode(periodMonth));
                string clientName = DAccess.Instance.GetCompanyName(idClient).ReplaceQuote().Replace(" ", "_");
                path += string.Format("{0}\\{1}\\{0}{2}{3}{4}\\", monthPeriod, clientName, StringHelper.ZeroCode(DateTime.Now.Day), StringHelper.ZeroCode(DateTime.Now.Hour), StringHelper.ZeroCode(DateTime.Now.Minute));
                FileHelper.WriteLog(string.Format("начато формирование реестра по клиенту {0}  ...", clientName));


                if (!ReestrGenerator.Generate(period, idClient.ToString(CultureInfo.InvariantCulture), -1, idBillFilter, string.Empty, true, generateDBF, isDebt, path, true, true, false))
                {
                    FileHelper.WriteLog(string.Format("ПРЕДУПРЕЖДЕНИЕ! По клиенту {0} нет данных ...", clientName));
                    return false;
                }

                List<string> attachmentsList = new List<string>();

                string zipFileName = string.Format("{0}{1}_{2}_100501.zip", path + $@"{clientName}\" + $@"{clientName}_100501\", monthPeriod, clientName.ToLower());
                string zipFileNameSber = string.Format("{0}{1}_{2}_формат_14_1.zip", path + $@"{clientName}\" + $@"{clientName}_формат_14_1\", monthPeriod, clientName.ToLower());
                string zipFileNameOtherBanks = string.Format("{0}{1}_{2}_100501_с_ЕЛС_ФИАС.zip", path + $@"{clientName}\" + $@"{clientName}_100501_с_ЕЛС_ФИАС\", monthPeriod, clientName.ToLower());
                string zipFileNameTinkoff = string.Format("{0}{1}_{2}_Тинькофф.zip", path + $@"{clientName}\" + $@"{clientName}_Тинькофф\", monthPeriod, clientName.ToLower());

                Zip.CreateFromDirectory(path + $@"{clientName}\" + $"{clientName}_100501", zipFileName);
                Zip.CreateFromDirectory(path + $@"{clientName}\" + $"{clientName}_формат_14_1", zipFileNameSber);
                Zip.CreateFromDirectory(path + $@"{clientName}\" + $"{clientName}_100501_с_ЕЛС_ФИАС", zipFileNameOtherBanks);
                Zip.CreateFromDirectory(path + $@"{clientName}\" + $"{clientName}_Тинькофф", zipFileNameTinkoff);

                attachmentsList.Add(zipFileName);
                attachmentsList.Add(zipFileNameSber);
                attachmentsList.Add(zipFileNameOtherBanks);
                attachmentsList.Add(zipFileNameTinkoff);
                if (isSend)
                {
                    string result = MailManager.SendMail(
                        Settings.Default.SmtpServerHost,
                        Settings.Default.SmtpServerPort,
                        Settings.Default.SmtpServerSSL,
                        Settings.Default.SmtpServerMailFrom,
                        Settings.Default.SmtpServerPswd,
                        emails,
                        MailSettings.CopyTo,
                        MailSettings.Subject,
                        MailSettings.BodyText,
                        attachmentsList);
                    success = string.IsNullOrEmpty(result);
                }
                else
                {
                    success = true;
                }
                FileHelper.WriteLog(string.Format("закончено формирование реестра по клиенту {0}  ...", clientName));
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog(string.Format("ОШИБКА: {0}", ex.Message));
            }
            return success;
        }

        public static bool Generate(int period, string clients, int idDeriveCompany, int idBillFilter, string housefilterString, bool generateTXT, bool generateDBF, bool isDebt, string path, bool includeClientNameInFilePath, bool addPokFieldsToDBFFile, bool onlyUninhabitedAccounts)
        {
            data = DAccess.DataModule.RSaldo(period, clients, idDeriveCompany, idBillFilter, housefilterString, isDebt, onlyUninhabitedAccounts);

            // Добавлено для Сбера
            dataSber = DAccess.DataModule.RSaldoSber(period, clients, idDeriveCompany, idBillFilter, housefilterString, isDebt, onlyUninhabitedAccounts);

            // Для Сбера с показаниями
            dataSberMeasure = DAccess.DataModule.RSaldoSberMeasure(period, clients, idDeriveCompany, idBillFilter, housefilterString, isDebt, onlyUninhabitedAccounts);

            ReestrGenerator.period = period;
            ReestrGenerator.isDebt = isDebt;
            ReestrGenerator.path = path;
            ReestrGenerator.useClientNameInFilePath = includeClientNameInFilePath;

            if (data.Rows.Count > 0)
            {
                if (generateDBF)
                    GenerateDbfReestr(addPokFieldsToDBFFile);
                if (generateTXT)
                    GenerateTxtReestrNew(clients);
            }

            // Добавлено для Сбера
            if (dataSber.Rows.Count > 0)
            {
                if (generateTXT)
                {
                    GenerateTxtReestrNewSber(clients);
                    GenerateTxtReestrNewOtherBanks(clients);
                    GenerateTxtReestrTinkoff(clients);
                }
            }

            // Добавлено для Сбера с показаниями
            if (dataSberMeasure.Rows.Count > 0)
            {
                if (generateTXT)
                    GenerateTxtReestrNewSberMeasure(clients);
            }

            return data.Rows.Count > 0 && dataSber.Rows.Count > 0;
        }

        public static string GetFilePath(int idClient)
        {
            if (!useClientNameInFilePath) return path;

            string clientName = DAccess.Instance.GetCompanyName(idClient).ReplaceQuote().Replace(" ", "_");
            return string.Format("{0}\\{1}\\{1}\\", path, clientName);
        }

        // Папка для Сбера
        public static string GetFilePath(int idClient, string add)
        {
            if (!useClientNameInFilePath) return path;

            string clientName = DAccess.Instance.GetCompanyName(idClient).ReplaceQuote().Replace(" ", "_");
            return string.Format("{0}\\{1}\\{1}_{2}\\", path, clientName, add);
        }

        /// <summary>
        /// Формирование DBF реестров и сохранение их в папки "Имя_Клиента_100501"
        /// </summary>
        private static void GenerateDbfReestr(bool addPokFields)
        {
            string prevClient = "";
            string key = "";
            int prevIdClient = 0;
            int idClient = 0;

            DataTable fileData = data.Clone();
            foreach (DataRow r in data.Rows)
            {
                key = r["VR"].ToString() + r["ID_Client"].ToString();
                idClient = int.Parse(r["ID_Client"].ToString());

                if (!String.Equals(prevClient, key, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (fileData.Rows.Count != 0)
                        DbfReestr.UnloadToDBF(GetFilePath(prevIdClient, "100501"), MakeFileName(prevClient, false, ""), fileData);

                    fileData = data.Clone();
                    fileData.ImportRow(r);

                    prevIdClient = int.Parse(r["ID_Client"].ToString());
                    prevClient = r["VR"].ToString() + r["ID_Client"].ToString();
                }
                else
                    fileData.ImportRow(r);
            }

            if (fileData.Rows.Count != 0)
            {
                string fileName = MakeFileName(key, false, "");
                DbfReestr.UnloadToDBF(GetFilePath(idClient, "100501"), fileName, fileData, string.Empty, addPokFields);
            }
        }

        /// <summary>
        /// Формирование TXT реестров и сохранение их в папки "Имя_Клиента_100501"
        /// </summary>
        private static void GenerateTxtReestrNew(string clients)
        {
            string[] clientList = clients.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            ReestrNumerator numerator = new ReestrNumerator();

            foreach (var client in clientList)
            {
                var clientServiceGroups = data.AsEnumerable().Where(row => row["ID_Client"].ToString() == client).GroupBy(row => row["NS"]);

                foreach (var rows in clientServiceGroups)
                {
                    var sum = rows.Sum(x => x.Field<decimal>("SUMM"));

                    StringBuilder fileData = new StringBuilder("");
                    StringBuilder fileDataOblgaz = new StringBuilder("");
                    int i = 0;

                    string head = "#FILESUM " + Math.Round(sum, 2).ToString().Replace(",", ".") + Environment.NewLine +
                                                            "#TYPE 7" + Environment.NewLine +
                                                            "#SERVICE " + rows.First()["NS"].ToString() + Environment.NewLine +
                                                            "#NOTE " + DateTime.Now.ToShortDateString();
                    fileData.AppendLine(head);

                    foreach (var r in rows)
                    {
                        fileData.AppendLine(CreateSberLine(r, i++));
                    }

                    string filename = string.IsNullOrEmpty(rows.First()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "100501") + MakeFileName(filename, true, ".txt", numerator.GetReestrNumber(filename)), fileData.ToString());
                }
            }

            //отдельная ветка для МТЭЦ, чтобы разбить на файлы по договорам 
            string[] mtecList = Settings.Default.MtecClientIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> contacts = data.AsEnumerable().Select(row => row["CONTRACT_HEAT_MTEC"].ToString()).Distinct().ToList();
            foreach (var client in mtecList)
            {
                foreach (var contact in contacts)
                {
                    var rows = data.AsEnumerable().Where(row => row["ID_Client"].ToString() == client
                                                                && row["CONTRACT_HEAT_MTEC"].ToString() == contact);

                    if (rows.Count<DataRow>() == 0)
                        continue;

                    StringBuilder fileData = new StringBuilder("");
                    int i = 0;

                    foreach (var r in rows)
                        fileData.AppendLine(CreateSberLine(r, i++));

                    string filename = string.IsNullOrEmpty(rows.FirstOrDefault()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "100501") + "ПО ДОГОВОРАМ\\" + MakeFileName(contact + "_" + filename, true, ".txt"), fileData.ToString());
                }
            }

            //отдельная ветка для МЭСа, чтобы разбить файлы по городам 
            string[] mesList = Settings.Default.MesClientIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> cities = data.AsEnumerable().Select(row => row["CITY"].ToString()).Distinct().ToList();
            foreach (var client in mesList)
            {
                foreach (var city in cities)
                {
                    var rows = data.AsEnumerable().Where(row => row["ID_Client"].ToString() == client
                                                                && row["CITY"].ToString() == city);

                    if (rows.Count<DataRow>() == 0)
                        continue;

                    StringBuilder fileData = new StringBuilder("");
                    int i = 0;

                    foreach (var r in rows)
                        fileData.AppendLine(CreateSberLine(r, i++));

                    string filename = string.IsNullOrEmpty(rows.FirstOrDefault()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "100501") + "ПО ГОРОДАМ\\" + MakeFileName(city + "_" + filename, true, ".txt"), fileData.ToString());
                }
            }
        }

        /// <summary>
        /// Формирование TXT реестров и сохранение их в папки "Имя_Клиента_формат_14_1"
        /// </summary>
        private static void GenerateTxtReestrNewSber(string clients)
        {
            string[] clientList = clients.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            ReestrNumerator numerator = new ReestrNumerator();

            foreach (var client in clientList)
            {
                var clientServiceGroups = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client).GroupBy(row => row["NS"]);

                foreach (var rows in clientServiceGroups)
                {
                    var sum = rows.Sum(x => x.Field<decimal>("SUMM"));

                    StringBuilder fileData = new StringBuilder("");
                    StringBuilder fileDataOblgaz = new StringBuilder("");
                    int i = 0;

                    string head = "#FILESUM " + Math.Round(sum, 2).ToString().Replace(",", ".") + Environment.NewLine +
                                                            "#TYPE 7" + Environment.NewLine +
                                                            "#SERVICE " + rows.First()["NS"].ToString() + Environment.NewLine +
                                                            "#NOTE " + DateTime.Now.ToShortDateString();
                    fileData.AppendLine(head);

                    foreach (var r in rows)  fileData.AppendLine(CreateSberLineNew(r, i++));

                    string filename = string.IsNullOrEmpty(rows.First()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "формат_14_1") + MakeFileName(filename, true, ".txt", numerator.GetReestrNumber(filename)), fileData.ToString());
                }
            }

            //отдельная ветка для МТЭЦ, чтобы разбить на файлы по договорам 
            string[] mtecList = Settings.Default.MtecClientIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> contacts = dataSber.AsEnumerable().Select(row => row["CONTRACT_HEAT_MTEC"].ToString()).Distinct().ToList();
            foreach (var client in mtecList)
            {
                foreach (var contact in contacts)
                {
                    var rows = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client
                                                                && row["CONTRACT_HEAT_MTEC"].ToString() == contact);

                    if (rows.Count<DataRow>() == 0)
                        continue;

                    StringBuilder fileData = new StringBuilder("");
                    int i = 0;

                    foreach (var r in rows)  fileData.AppendLine(CreateSberLineNew(r, i++));


                    string filename = string.IsNullOrEmpty(rows.FirstOrDefault()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "формат_14_1") + "ПО ДОГОВОРАМ\\" + MakeFileName(contact + "_" + filename, true, ".txt"), fileData.ToString());
                }
            }

            //отдельная ветка для МЭСа, чтобы разбить файлы по городам 
            string[] mesList = Settings.Default.MesClientIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> cities = dataSber.AsEnumerable().Select(row => row["CITY"].ToString()).Distinct().ToList();
            List<string> NSes = dataSber.AsEnumerable().Select(row => row["NS"].ToString()).Distinct().ToList();
            foreach (var client in mesList)
            {
                foreach (var city in cities)
                {
                    foreach (var NS in NSes)
                    {
                        var rows = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client
                                                                && row["CITY"].ToString() == city
                                                                && row["NS"].ToString() == NS);

                        if (rows.Count<DataRow>() == 0)
                            continue;

                        StringBuilder fileData = new StringBuilder("");
                        int i = 0;

                        foreach (var r in rows)  fileData.AppendLine(CreateSberLineNew(r, i++));


                        string filename = string.IsNullOrEmpty(rows.FirstOrDefault()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                        FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "формат_14_1") + "ПО ГОРОДАМ\\" + MakeFileName(city + "_" + filename, true, ".txt"), fileData.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Формирование TXT реестров и сохранение их в папки "Имя_Клиента_Тинькофф"
        /// </summary>
        private static void GenerateTxtReestrTinkoff(string clients)
        {
            string[] clientList = clients.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            ReestrNumerator numerator = new ReestrNumerator();

            foreach (var client in clientList)
            {
                var clientServiceGroups = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client).GroupBy(row => row["NS"]);

                foreach (var rows in clientServiceGroups)
                {
                    var sum = rows.Sum(x => x.Field<decimal>("SUMM"));

                    StringBuilder fileData = new StringBuilder("");
                    StringBuilder fileDataOblgaz = new StringBuilder("");
                    int i = 0;

                    foreach (var r in rows) fileData.AppendLine(CreateTinkoffLine(r, i++));

                    string filename = string.IsNullOrEmpty(rows.First()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "Тинькофф") + MakeFileName(filename, false, ".txt", numerator.GetReestrNumber(filename)), fileData.ToString());
                }
            }

            //отдельная ветка для МТЭЦ, чтобы разбить на файлы по договорам 
            string[] mtecList = Settings.Default.MtecClientIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> contacts = dataSber.AsEnumerable().Select(row => row["CONTRACT_HEAT_MTEC"].ToString()).Distinct().ToList();
            foreach (var client in mtecList)
            {
                foreach (var contact in contacts)
                {
                    var rows = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client && row["CONTRACT_HEAT_MTEC"].ToString() == contact);

                    if (rows.Count<DataRow>() == 0) continue;

                    StringBuilder fileData = new StringBuilder("");
                    int i = 0;

                    foreach (var r in rows) fileData.AppendLine(CreateTinkoffLine(r, i++));


                    string filename = string.IsNullOrEmpty(rows.FirstOrDefault()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "Тинькофф") + "ПО ДОГОВОРАМ\\" + MakeFileName(contact + "_" + filename, false, ".txt"), fileData.ToString());
                }
            }

            //отдельная ветка для МЭСа, чтобы разбить файлы по городам 
            string[] mesList = Settings.Default.MesClientIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> cities = dataSber.AsEnumerable().Select(row => row["CITY"].ToString()).Distinct().ToList();
            List<string> NSes = dataSber.AsEnumerable().Select(row => row["NS"].ToString()).Distinct().ToList();
            foreach (var client in mesList)
            {
                foreach (var city in cities)
                {
                    foreach (var NS in NSes)
                    {
                        var rows = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client
                                                                && row["CITY"].ToString() == city
                                                                && row["NS"].ToString() == NS);

                        if (rows.Count<DataRow>() == 0) continue;

                        StringBuilder fileData = new StringBuilder("");
                        int i = 0;

                        foreach (var r in rows) fileData.AppendLine(CreateTinkoffLine(r, i++));


                        string filename = string.IsNullOrEmpty(rows.FirstOrDefault()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                        FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "Тинькофф") + "ПО ГОРОДАМ\\" + MakeFileName(city + "_" + filename, false, ".txt"), fileData.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Формирование TXT реестров и сохранение их в папки "Имя_Клиента_100501_с_ЕЛС_ФИАС"
        /// </summary>
        private static void GenerateTxtReestrNewOtherBanks(string clients)
        {
            string[] clientList = clients.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            ReestrNumerator numerator = new ReestrNumerator();

            foreach (var client in clientList)
            {
                var clientServiceGroups = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client).GroupBy(row => row["NS"]);

                foreach (var rows in clientServiceGroups)
                {
                    var sum = rows.Sum(x => x.Field<decimal>("SUMM"));

                    StringBuilder fileData = new StringBuilder("");
                    StringBuilder fileDataOblgaz = new StringBuilder("");
                    int i = 0;

                    string head = "#FILESUM " + Math.Round(sum, 2).ToString().Replace(",", ".") + Environment.NewLine +
                                                            "#TYPE 7" + Environment.NewLine +
                                                            "#SERVICE " + rows.First()["NS"].ToString() + Environment.NewLine +
                                                            "#NOTE " + DateTime.Now.ToShortDateString();
                    fileData.AppendLine(head);

                    foreach (var r in rows) fileData.AppendLine(CreateELSAndFIASLineOld(r, i++));
                    

                    string filename = string.IsNullOrEmpty(rows.First()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "100501_с_ЕЛС_ФИАС") + MakeFileName(filename, true, ".txt", numerator.GetReestrNumber(filename)), fileData.ToString());
                }
            }

            //отдельная ветка для МТЭЦ, чтобы разбить на файлы по договорам 
            string[] mtecList = Settings.Default.MtecClientIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> contacts = dataSber.AsEnumerable().Select(row => row["CONTRACT_HEAT_MTEC"].ToString()).Distinct().ToList();
            foreach (var client in mtecList)
            {
                foreach (var contact in contacts)
                {
                    var rows = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client
                                                                && row["CONTRACT_HEAT_MTEC"].ToString() == contact);

                    if (rows.Count<DataRow>() == 0)
                        continue;

                    StringBuilder fileData = new StringBuilder("");
                    int i = 0;

                    foreach (var r in rows) fileData.AppendLine(CreateELSAndFIASLineOld(r, i++));

                    string filename = string.IsNullOrEmpty(rows.FirstOrDefault()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                    FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "100501_с_ЕЛС_ФИАС") + "ПО ДОГОВОРАМ\\" + MakeFileName(contact + "_" + filename, true, ".txt"), fileData.ToString());
                }
            }

            //отдельная ветка для МЭСа, чтобы разбить файлы по городам 
            string[] mesList = Settings.Default.MesClientIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> cities = dataSber.AsEnumerable().Select(row => row["CITY"].ToString()).Distinct().ToList();
            List<string> NSes = dataSber.AsEnumerable().Select(row => row["NS"].ToString()).Distinct().ToList();
            foreach (var client in mesList)
            {
                foreach (var city in cities)
                {
                    foreach (var NS in NSes)
                    {
                        var rows = dataSber.AsEnumerable().Where(row => row["ID_Client"].ToString() == client
                                                                && row["CITY"].ToString() == city
                                                                && row["NS"].ToString() == NS);

                        if (rows.Count<DataRow>() == 0)
                            continue;

                        StringBuilder fileData = new StringBuilder("");
                        int i = 0;

                        foreach (var r in rows) fileData.AppendLine(CreateELSAndFIASLineOld(r, i++));

                        string filename = string.IsNullOrEmpty(rows.FirstOrDefault()["NS"].ToString()) ? client : rows.First()["NS"].ToString();
                        FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "100501_с_ЕЛС_ФИАС") + "ПО ГОРОДАМ\\" + MakeFileName(city + "_" + filename, true, ".txt"), fileData.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Формирование TXT реестров показаний и сохранение их в папки "Имя_Клиента_Для_Сбера_Показания"
        /// </summary>
        private static void GenerateTxtReestrNewSberMeasure(string clients)
        {
            string[] clientList = clients.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var client in clientList)
            {
                var clientServiceGroups = dataSberMeasure.AsEnumerable().Where(row => row["ID_Client"].ToString() == client);

                if (clientServiceGroups.Count<DataRow>() == 0) continue;

                StringBuilder fileData = new StringBuilder("");
                int i = 0;

                foreach (var row in clientServiceGroups)
                {
                    fileData.AppendLine(CreateSberLineNewMeasure(row, i++));
                }


                string filename = string.IsNullOrEmpty(clientServiceGroups.FirstOrDefault()["NS"].ToString()) ? client : clientServiceGroups.First()["NS"].ToString();
                FileHelper.WriteTextFile(GetFilePath(Convert.ToInt32(client), "Для_Сбера_Показания") + MakeFileName(filename, true, ".txt"), fileData.ToString());
            }
        }

        private static string MakeFileName(string fileName, bool isSber, string ext, int reestrNumber = 0)
        {
            DataRow r = DAccess.Instance.GetPeriodInfo(period);

            string s = Guid.NewGuid().ToString();

            if (!isSber)
            {
                s = fileName + "_" + StringHelper.ZeroCode(int.Parse(r["month"].ToString()))
                           + r["year"].ToString().Substring(2, 2) + ext;
                s = s.Replace(" ", "_");
                if (isDebt)
                {
                    s = s.Replace("EK", "ДОЛГ_").Replace("EK", "ДОЛГ_").Replace("EP", "ДОЛГ_").Replace("PP", "ДОЛГ_");
                    s = s + "_" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second;
                    if (s.Contains(".txt")) s = s.ToLower().Replace(".txt", "") + ".txt";
                }
            }
            else
            {
                s = string.Format(
                    "{0}_{1}{2}{3}{4}{5}",
                    fileName,
                    StringHelper.ZeroCode(DateTime.Now.Day),
                    StringHelper.ZeroCode(DateTime.Now.Month),
                    StringHelper.ZeroCode(DateTime.Now.Year),
                    reestrNumber.ToString(), // (DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString()),
                    ext);
            }

            return s;
        }

        /// <summary>
        /// Строка реестра для банков (старый формат 100501)
        /// </summary>
        private static string CreateSberLine(DataRow r, int lineIndex)
        {
            string str =
                r["NUMBER_AGENT_RKA"].ToString() + ";" +
                r["ADRES"].ToString() + ";" +
                r["AUTHCOD_SHORT"].ToString() + ";" +
                Math.Round(decimal.Parse(r["Summ"].ToString()), 2).ToString().Replace(",", ".") + ";;;;" +
                "100501" + ":" +
                r["AUTHCOD_LONG"].ToString() + ":" +
                r["ADRES"].ToString() + ":" +
                "0.00" + ":" +
                (r["PU1"].ToString().Length > 1 ? "PU1:" + r["PU1"].ToString() + ":" + r["POK1"].ToString() + ":" : ":::") +
                (r["PU2"].ToString().Length > 1 ? "PU2:" + r["PU2"].ToString() + ":" + r["POK2"].ToString() + ":" : ":::") +
                (r["PU3"].ToString().Length > 1 ? "PU3:" + r["PU3"].ToString() + ":" + r["POK3"].ToString() + ":" : ":::") +
                (r["PU4"].ToString().Length > 1 ? "PU4:" + r["PU4"].ToString() + ":" + r["POK4"].ToString() + ":" : ":::") +
                (r["PU5"].ToString().Length > 1 ? "PU5:" + r["PU5"].ToString() + ":" + r["POK5"].ToString() + ":" : ":::") +
                (r["PU6"].ToString().Length > 1 ? "PU6:" + r["PU6"].ToString() + ":" + r["POK6"].ToString() + ":" : ":::") +
                (r["PU7"].ToString().Length > 1 ? "PU7:" + r["PU7"].ToString() + ":" + r["POK7"].ToString() + ":" : ":::") +
                (r["PU8"].ToString().Length > 1 ? "PU8:" + r["PU8"].ToString() + ":" + r["POK8"].ToString() + ":" : ":::");

            return str;
        }

        /// <summary>
        /// Строка реестра для банков с ЕЛС и ФИАС (федеральный стандарт до 2022)
        /// </summary>
        private static string CreateELSAndFIASLineOld(DataRow r, int lineIndex)
        {
            string str =
                r["NUMBER_AGENT_RKA"].ToString() + ";" +
                r["ELS"].ToString() + ";" +
                r["FIAS"].ToString() + ";" +
                r["ADRES"].ToString() + ";" +
                r["AUTHCOD_SHORT"].ToString() + ";" +
                Math.Round(decimal.Parse(r["Summ"].ToString()), 2).ToString().Replace(",", ".") + ";;;;" +
                "100501" + ":" +
                r["AUTHCOD_LONG"].ToString() + ":" +
                r["ADRES"].ToString() + ":" +
                "0.00" + ":" +
                (r["PU1"].ToString().Length > 1 ? "PU1:" + r["PU1"].ToString() + ":" + r["POK1"].ToString() + ":" : ":::") +
                (r["PU2"].ToString().Length > 1 ? "PU2:" + r["PU2"].ToString() + ":" + r["POK2"].ToString() + ":" : ":::") +
                (r["PU3"].ToString().Length > 1 ? "PU3:" + r["PU3"].ToString() + ":" + r["POK3"].ToString() + ":" : ":::") +
                (r["PU4"].ToString().Length > 1 ? "PU4:" + r["PU4"].ToString() + ":" + r["POK4"].ToString() + ":" : ":::") +
                (r["PU5"].ToString().Length > 1 ? "PU5:" + r["PU5"].ToString() + ":" + r["POK5"].ToString() + ":" : ":::") +
                (r["PU6"].ToString().Length > 1 ? "PU6:" + r["PU6"].ToString() + ":" + r["POK6"].ToString() + ":" : ":::") +
                (r["PU7"].ToString().Length > 1 ? "PU7:" + r["PU7"].ToString() + ":" + r["POK7"].ToString() + ":" : ":::") +
                (r["PU8"].ToString().Length > 1 ? "PU8:" + r["PU8"].ToString() + ":" + r["POK8"].ToString() + ":" : ":::");

            return str;
        }

        /// <summary>
        /// Строка реестра для Сбера и ВТБ с ЕЛС и ФИАС (федеральный стандарт 14.1 с 2022)
        /// </summary>
        private static string CreateSberLineNew(DataRow r, int lineIndex)
        {
            string str =
                   r["AUTHCOD_SHORT"].ToString() + ";" +
                   r["ELS"].ToString() + ";" +
                   r["FIAS"].ToString() + ";" +
                   r["ADRES"].ToString() + ";" +
                   r["ADRES"].ToString() + ";" +
                   r["NUMBER_DOP"].ToString() + ";" +
                   Math.Round(decimal.Parse(r["Summ"].ToString()), 2).ToString().Replace(",", ".") + ";" +
                   (r["PU1"].ToString().Length > 1 ? r["PU1"].ToString() + ";" + r["POK1"].ToString() + ";" : ";;") +
                   (r["PU2"].ToString().Length > 1 ? r["PU2"].ToString() + ";" + r["POK2"].ToString() + ";" : ";;") +
                   (r["PU3"].ToString().Length > 1 ? r["PU3"].ToString() + ";" + r["POK3"].ToString() + ";" : ";;") +
                   (r["PU4"].ToString().Length > 1 ? r["PU4"].ToString() + ";" + r["POK4"].ToString() + ";" : ";;") +
                   (r["PU5"].ToString().Length > 1 ? r["PU5"].ToString() + ";" + r["POK5"].ToString() + ";" : ";;") +
                   (r["PU6"].ToString().Length > 1 ? r["PU6"].ToString() + ";" + r["POK6"].ToString() + ";" : ";;") +
                   (r["PU7"].ToString().Length > 1 ? r["PU7"].ToString() + ";" + r["POK7"].ToString() + ";" : ";;") +
                   (r["PU8"].ToString().Length > 1 ? r["PU8"].ToString() + ";" + r["POK8"].ToString() + ";" : ";;") + ";;;;;;;;";

            return str;
        }

        /// <summary>
        /// Строка реестра для Тинькофф
        /// </summary>
        private static string CreateTinkoffLine(DataRow r, int lineIndex)
        {
            string str =
                   r["AUTHCOD_SHORT"].ToString() + ";" +
                   r["ELS"].ToString() + ";" +
                   r["FIAS"].ToString() + ";" +
                   r["NUMBER_DOP"].ToString() + ";" +
                   Math.Round(decimal.Parse(r["Summ"].ToString()), 2).ToString().Replace(",", ".") + ";" +
                   r["NS"].ToString();

            return str;
        }

        // Создает строку по счетчику, либо если счетчика нет пустая строка
        private static string GetMeasureForLine(DataRow r, int num, char separator)
        {
            return (
                (r[$"PU{num}TYPE"].ToString().Length > 0)
                ? r[$"PU{num}TYPE"].ToString() + separator +
                    r[$"PU{num}"].ToString() + separator +
                    r[$"PU{num}IDMETER"].ToString() + separator +
                    r[$"PU{num}RemoveDate"].ToString() + separator +
                    r[$"POK{num}"].ToString() + separator +
                    r[$"PU{num}Measure_Date"].ToString() + separator +
                    r[$"PU{num}DOP"].ToString() + separator
                : ""
            );
        }

        // Файл для нового Сбера с показаниями
        private static string CreateSberLineNewMeasure(DataRow r, int lineIndex)
        {
            const char SEPARATOR = ';';

            string str =
                r["AUTHCOD_SHORT"].ToString() + SEPARATOR +
                r["fio"].ToString() + SEPARATOR +
                r["els"].ToString() + SEPARATOR +
                r["fias"].ToString() + SEPARATOR +
                r["Adres"].ToString() + SEPARATOR +
                GetMeasureForLine(r, 1, SEPARATOR) +
                GetMeasureForLine(r, 2, SEPARATOR) +
                GetMeasureForLine(r, 3, SEPARATOR) +
                GetMeasureForLine(r, 4, SEPARATOR) +
                GetMeasureForLine(r, 5, SEPARATOR);

            return str;
        }

        
    }
}