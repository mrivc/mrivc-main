﻿using EllisBillFilter.DS;
using EllisBillFilter.Properties;
using MrivcHelpers.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter.Forms
{
    public partial class FrmPrintBillDelivery : Form
    {
        public FrmPrintBillDelivery(int idAccount)
        {
            InitializeComponent();

            this.IdAccount = idAccount;
            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
            gridPrintDeliveryBuild.InitEx();
        }

        public int IdAccount { get; private set; }

        public void LoadData()
        {
            mDataSet.Clear();

            mDataSet.Period.Merge(DAccess.Instance.Periods);
            mDataSet.DeriveCompany.Merge(DAccess.Instance.DeriveCompany);
            mDataSet.PayReceiverCompany.Merge(DAccess.Instance.PayReceiverCompany);
            mDataSet.BillFilter.Merge(DAccess.Instance.BillFilter);

            DAccess.DataModule.PrintBillDeliverySelect(mDataSet.PrintDeliveryBill, IdAccount);
        }

        private void FrmPrintBillDelivery_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        List<FileInfo> files;
        public List<FileInfo> Files
        {
            get
            {
                if (files == null)
                {
                    files = FileHelper.GetDirectoryTree(Settings.Default.PathToBill, "*.pdf");
                }

                return files;
            }
        }

        private void gvPrintDeliveryBill_DoubleClick(object sender, EventArgs e)
        {
            object fileName = gridPrintDeliveryBuild.GetValue("File_Name");
            if (fileName != null)
            {
                this.Cursor = Cursors.WaitCursor;
                
                var file = this.Files.FirstOrDefault<FileInfo>(f => f.Name.Contains(fileName.ToString()));
                if (file != null)
                {
                    System.Diagnostics.Process.Start(file.FullName);
                }
                this.Cursor = Cursors.Default;
            }
        }
    }
}
