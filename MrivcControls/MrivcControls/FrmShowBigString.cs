﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MrivcControls
{
    public partial class FrmShowBigString : Form
    {
        public FrmShowBigString(string caption, string text, bool readOnly = false)
        {
            InitializeComponent();
            txtText.Text = text;
            txtText.ReadOnly = readOnly;
        }

        public static void Show(string caption, string text, bool readOnly = true)
        {
            var f = new FrmShowBigString(caption, text);
            f.ShowDialog();
        }
    }
}
