﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace GuidMagicComparer
{
    public class Program
    {
        static void Main(string[] args)
        {
            Guid a;
            Guid b;

            var t = new Stopwatch();
            t.Start();

            long n = 0;

            while (1==1)
            {
                a = Guid.NewGuid();
                b = Guid.NewGuid();
                if (a.ToString() == b.ToString())
                    break;

                n = n + 1;

                Console.Write($"n={n} hours={t.ElapsedMilliseconds / 1000 / 60 / 60}");
                Console.CursorLeft = 0;
                if (n == long.MaxValue)
                {
                    n = 0;
                    Console.CursorTop++;
                }

            }

            t.Stop();

            var time = t.ElapsedMilliseconds;

            var days = time / 1000 / 60 / 60 / 24;

            Console.WriteLine($"{a.ToString()}={b.ToString()} Time(s)= {days}");

            Console.ReadLine();
        }
    }
}
