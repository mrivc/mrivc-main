﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MrivcControls
{
    public sealed class DeriveCompany : MultiValueDataPicker
    {
        public DeriveCompany()
        {
            ParameterName = "Управляющая компания";
            ValueMember = "id_company";
            DisplayMember = "name";
        }

        public void Fill(string connectionString)
        {
            DAccess.DataModule.SetConnection(connectionString);
            DataSource = DAccess.DataModule.GetDataTableByQueryCommand("select id_company,name from dbo.Company where is_derive_company = 1 order by name");
        }
    }
}
