﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraGrid;
using System.Data;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using System.Drawing;
using DevExpress.XtraPrinting.Export.XLS;
using DevExpress.Utils;

namespace Payments.BaseControls
{
    public class BaseGrid : GridControl
    {
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private int beforeSelectedRowHandle;
        private bool trimCellValueBeforeValidate = true;

        public bool MultiSelect
        {
            get { return gridView1.OptionsSelection.MultiSelect; }
            //set { gridView1.OptionsSelection.MultiSelect = value; }
        }

        public delegate void SaveDataEventHandler();
        [System.ComponentModel.Browsable(true)]
        public event SaveDataEventHandler SaveData;

        [System.ComponentModel.Browsable(true)]
        public bool TrimCellValueBeforeValidate
        {
            set { trimCellValueBeforeValidate = value; }
            get { return trimCellValueBeforeValidate; }
        }

        public void ucGrid()
        {
            //   InitializeComponent(); без этого работает дизайнер как надо
        }

        public void SaveLayout()
        {
            if (gridView1 == null)
                return;
            
            gridView1.SaveLayoutToXml(gridView1.Name+".xml");
        }

        public void LoadLayout()
        {
            if (gridView1 == null)
                return;

            if (FileHelper.ExistFile(gridView1.Name + ".xml"))
                gridView1.RestoreLayoutFromXml(gridView1.Name + ".xml");
        }

        public void InitEx()
        {
            gridView1 = (GridView)this.MainView;
            
            gridView1.GroupPanelText = "Перетащите сюда наименование столбцов по которым хотите сгруппировать записи.";
            gridView1.NewItemRowText = "Чтобы добавить новую запись, нажмите кнопку «Добавить» в контекстном меню или на панели навигации";
            this.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(EmbeddedNavigator_ButtonClick);

            gridView1.OptionsView.ShowDetailButtons = false;
            gridView1.OptionsNavigation.AutoFocusNewRow = true;
            gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(gridView1_ValidateRow);
            gridView1.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(gridView1_InvalidRowException);
            gridView1.KeyDown += new KeyEventHandler(gridView1_KeyDown);
            gridView1.InitNewRow += new InitNewRowEventHandler(gridView1_InitNewRow);
            gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(gridView1_RowUpdated);
            gridView1.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(gridView1_BeforeLeaveRow);
            gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridView1_CellValueChanged);
            gridView1.RowCellClick += new RowCellClickEventHandler(gridView1_RowCellClick);
            gridView1.RowClick += new RowClickEventHandler(gridView1_RowClick);
            gridView1.OptionsLayout.StoreAllOptions = true;
            LoadLayout();
            gridView1.OptionsSelection.MultiSelect = true;
        }

        protected override void Dispose(bool disposing)
        {
            SaveLayout();
            base.Dispose(disposing);
        }

        void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            if (gridView1.IsNewItemRow(e.RowHandle) && gridView1.OptionsView.NewItemRowPosition == NewItemRowPosition.Top)
                AddNewRow();
        }

        void gridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Column.Caption == "Д")
            {
                gridView1.SetFocusedValue(!(bool)e.CellValue);
            }
        }

        void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //сохраняем изменение датасета, чтобы не шло обновление на сервак
            //это всего лишь поле с галочкой
            if (e.Column.Caption == "Д")
                ((DataSet)(((BindingSource)gridView1.DataSource)).DataSource).AcceptChanges();
        }

        public void DisableGrid()
        {
            gridView1.OptionsBehavior.Editable = false;
            foreach (DevExpress.XtraGrid.Columns.GridColumn _col in gridView1.Columns)
                _col.AppearanceCell.BackColor = Color.LightGray;
        }

        public void EnableGrid()
        {
            gridView1.OptionsBehavior.Editable = true;
            foreach (DevExpress.XtraGrid.Columns.GridColumn _col in gridView1.Columns)
                _col.AppearanceCell.BackColor = Color.White;
        }

        public int BeforeSelectedRowHandle
        {
            get { return beforeSelectedRowHandle; }
        }

        public void RecalculateRank()
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                gridView1.GetDataRow(i)["Rank"] = i + 1;
            }
            AcceptChanges();
        }

        void gridView1_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
        {
            beforeSelectedRowHandle = e.RowHandle;
        }

        //вызываем событие сохранения записи
        void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            AcceptChanges();
            //((BindingSource)this.DataSource).EndEdit();

            //if (SaveData != null)
            //    SaveData();
        }

        public void AcceptChanges()
        {
            if (SaveData != null)
            {
                ((BindingSource)this.DataSource).EndEdit();
                SaveData();

                //if (AfterAddNewRowFocuseTopRow)
                //gridView1.FocusedRowHandle = 0;
                //gridView1.MakeRowVisible(0, true);
            }
        }

        //обрабатываем добавление строки при установленной группировки
        //устанавливаем фокус на первую ячеку, которая обязательна для заполения
        void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            if (beforeSelectedRowHandle < 0)
                beforeSelectedRowHandle = gridView1.GetDataRowHandleByGroupRowHandle(beforeSelectedRowHandle);

            gridView1.FocusedRowHandle = e.RowHandle;

            //обрабатываем добавление строки при установленной группировки
            if (gridView1.GroupedColumns.Count > 0)
            {
                foreach (GridColumn groupColumn in gridView1.GroupedColumns)
                {
                    object value = gridView1.GetRowCellValue(beforeSelectedRowHandle, groupColumn);
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, groupColumn, value);
                }
                gridView1.MakeRowVisible(gridView1.FocusedRowHandle, true);
            }

            //устанавливаем фокус на первую ячеку, которая обязательна для заполения
            foreach (GridColumn _c in gridView1.Columns)
                if (_c.Caption.IndexOf("*") > 0)
                {
                    gridView1.FocusedColumn = _c;
                    gridView1.SelectCell(e.RowHandle, _c);
                    gridView1.ShowEditor();
                    return;
                }
        }

        //обрабатываем кнопки Insert и Delete
        void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert)
                this.AddNewRow();
            if (e.KeyCode == Keys.Delete)
                this.DeleteActiveRow();
        }

        //не позволяем вставлять больше одной записи, по не заполнены все ячейки
        void EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove)
            {
                e.Handled = true;
                DeleteActiveRow();
            }
        }

        void gridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.WindowCaption = "Ошибка";
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.DisplayError;
        }

        //провека правильности заполнения обязательных полей
        void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                foreach (GridColumn _c in gridView1.Columns)
                {
                    if (_c.Caption.IndexOf("*") > 0)
                    {
                        string val = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, _c.FieldName).ToString();
                        if (val.Trim() == "")
                        {
                            e.Valid = false;
                            e.ErrorText = "Поле '" + _c.Caption + "' является обязательным\nВы хотите исправить значение?";
                            return;
                            //gridView1.SetColumnError(_c, "Поле '" + _c.Caption + "' является обязательным\n");
                        }
                        else
                            e.Valid = true;
                    }
                }
            }

        }

        private void InitializeComponent()
        {
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this;
            this.gridView1.Name = "gridView1";
            // 
            // ucGrid
            // 
            this.MainView = this.gridView1;
            this.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
        }

        public void DeleteActiveRow()
        {
            gridView1 = (GridView)this.FocusedView;
            if (gridView1 == null || gridView1.SelectedRowsCount == 0) return;

            if (gridView1.SelectedRowsCount > 1)
            {
                MessageBox.Show("Удаление более одной строки запрещено!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if ((MessageBox.Show("Вы действительно хотите удалить строку?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
            {

                DataRow[] rows = new DataRow[gridView1.SelectedRowsCount];
                for (int i = 0; i < gridView1.SelectedRowsCount; i++)
                    rows[i] = gridView1.GetDataRow(gridView1.GetSelectedRows()[i]);

                foreach (DataRow row in rows)
                    row.Delete();

                AcceptChanges();
            }
        }

        public void AddNewRow()
        {
            try
            {
                gridView1 = (GridView)this.FocusedView;
                int currentRow;
                currentRow = gridView1.FocusedRowHandle;
                if (currentRow < 0)
                    currentRow = gridView1.GetDataRowHandleByGroupRowHandle(currentRow);

                //if(currentRow >=0 && gridView1.RowCount > 0)
                gridView1.AddNewRow();


                /*          if (gridView1.GroupedColumns.Count == 0)
                  return;

                  // Initialize group values
                  foreach (GridColumn groupColumn in gridView1.GroupedColumns)
                  {
                      object value = gridView1.GetRowCellValue(currentRow, groupColumn);
                      gridView1.SetRowCellValue(gridView1.FocusedRowHandle, groupColumn, value);
                  }*/
                //gridView1.UpdateCurrentRow();
                //gridView1.MakeRowVisible(gridView1.FocusedRowHandle, true);
                //gridView1.ShowEditor();

            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message);
            }
        }

        public void DeleteCheckedRows()
        {
            gridView1 = (GridView)this.FocusedView;

            if ((MessageBox.Show("Вы действительно хотите удалить выбранные строки?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
            {

                DataTable _dt = ((DataSet)(((BindingSource)gridView1.DataSource)).DataSource).Tables[((BindingSource)gridView1.DataSource).DataMember];

                foreach (DataRow _dr in _dt.Rows)
                    if ((bool)_dr["GridAction"] == true)
                        _dr.Delete();

                AcceptChanges();
            }
        }

        public void MyExport()
        {

            SaveFileDialog _sfd = new SaveFileDialog();
            _sfd.RestoreDirectory = true;
            _sfd.Filter = "xls files (*.xls)|*.xls|xlsx files (*.xlsx)|*.xlsx|pdf files (*.pdf)|*.pdf";
            _sfd.AddExtension = true;
            _sfd.DefaultExt = "xls";

            if (_sfd.ShowDialog() == DialogResult.OK)
            {
                if (FileHelper.GetFileExt(_sfd.FileName).ToLower() == "xls")
                    gridView1.ExportToXls(_sfd.FileName);
                if (FileHelper.GetFileExt(_sfd.FileName).ToLower() == "pdf")
                    gridView1.ExportToPdf(_sfd.FileName);
                if (FileHelper.GetFileExt(_sfd.FileName).ToLower() == "xlsx")
                    gridView1.ExportToXlsx(_sfd.FileName);
            }
        }

        //gridView1.BeginSort();
        //try
        //{
        //    foreach (DataRow row in rows)
        //        row.Delete();
        //}
        //finally
        //{
        //    gridView1.EndSort();
        //}
    }

}
