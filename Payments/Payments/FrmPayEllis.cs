﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Payments.DS;
using Payments.Controls;
using Payments.LogicClasses;
using System.Data.SqlClient;

namespace Payments
{
    public partial class FrmPayEllis : Form, IPayForm
    {
        PaymentClass paymentClass;

        public FrmPayEllis(PaymentClass paymentClass)
        {
            this.paymentClass = paymentClass;
            InitializeComponent();
        }

        public PanelPayment PaymentInfo
        {
            get
            {
                return panelPayment1;
            }
        }

        public Form Window {
            get { return this; }
        }

        public PaymentClass PaymentClass
        {
            get { return paymentClass; }
        }

        private bool panelPayment1_PayEvent(Controls.PanelPayment sender, EventArgs e)
        {
            if (!ValidatePayment())
                return false;

            //если оплата кредиткой
            if (sender.CashType == 2)
            {
                return PayByVisaCard();
            }

           
            List<string> freeStrings = new List<string>();
            //freeStrings.Add("Услуга: " + ellisTableinfo.Rows[0]["Bill_Filter_Name"].ToString());
            //freeStrings.Add("");
            freeStrings.Add("ПОЛУЧАТЕЛЬ: " + ellisTableinfo.Rows[0]["КОМ_НАИМ"].ToString());
            freeStrings.Add("Услуга: " + ellisTableinfo.Rows[0]["Bill_Filter_Name"].ToString());
            //freeStrings.Add("ИНН: " + ellisTableinfo.Rows[0]["КОМ_ИНН"].ToString());
            //freeStrings.Add("КПП: " + ellisTableinfo.Rows[0]["КОМ_КПП"].ToString());
            //freeStrings.Add("СЧЕТ: " + ellisTableinfo.Rows[0]["КОМ_РС"].ToString());
            //freeStrings.Add("БАНК: " + ellisTableinfo.Rows[0]["КОМ_БАНК"].ToString());
            freeStrings.Add("");
            freeStrings.Add("Код авторизации: " + txtAuthcod.Text.Replace(" ", ""));

            if (gridPaymentParameter1.GetValue("ADRES").ToString().Length > 27)
            {
                freeStrings.Add("Адрес: " + gridPaymentParameter1.GetValue("ADRES").ToString().Substring(0, 27));
                freeStrings.Add("" + gridPaymentParameter1.GetValue("ADRES").ToString().Substring(27));
            }
            else
                freeStrings.Add("Адрес: " + gridPaymentParameter1.GetValue("ADRES"));

            freeStrings.Add("");

            DAccess.DataModule.Connection1 = new SqlConnection(Properties.Settings.Default.PaymentsConnectionString);

            var dop = DAccess.DataModule.ExecuteScalar("select Phone from dbo.Filter_Client where id_company = " + ellisTableinfo.Rows[0]["ID_Company"].ToString());

            if (dop!=null)
            {
                freeStrings.Add("Телефон постащика " + dop);
            }

            
            freeStrings.Add("Телефон АО МРИВЦ 455091");

            freeStrings.Add("");

            freeStrings.Add("Сумма платежа: " + panelPayment1.PaymentSumm.ToString());
            freeStrings.Add("Комиссия с платежа: " + panelPayment1.PaymentSummCommission.ToString());

            //freeStrings.Add("Управляющая компания:");
            //freeStrings.Add(gridPaymentParameter1.GetValue("UK").ToString());
            freeStrings.Add("");
            string hv = gridPaymentParameter1.GetValue("HV").ToString().Trim();
            string gv = gridPaymentParameter1.GetValue("GV").ToString().Trim();
            string gaz = gridPaymentParameter1.GetValue("GAZ").ToString().Trim();
            string el = gridPaymentParameter1.GetValue("ET").ToString().Trim();

            if(hv.Length > 0)
                freeStrings.Add("СЧЕТЧИК Х/ВОДЫ: " + hv);
            if (gv.Length > 0)
                freeStrings.Add("СЧЕТЧИК Г/ВОДЫ: " + gv);
            if (el.Length > 0)
                freeStrings.Add("СЧЕТЧИК ЭЛ/ЭН: " + el);
            if (gaz.Length > 0)
                freeStrings.Add("СЧЕТЧИК ГАЗ: " + gaz);

            freeStrings.Add("");
            freeStrings.Add("");

            string ofdProductName = StringHelper.GenerateOfdProductName(txtAuthcod.Text.Replace(" ", ""), ellisTableinfo.Rows[0]["КОМ_РС"].ToString());

            if (DAccess.CashTerminal.Sale(panelPayment1.PaymentSummTotal, panelPayment1.PaymentCash, 1,
                paymentClass.Name, freeStrings, ofdProductName, panelPayment1.PhoneEmail, Properties.Settings.Default.Department_Number, paymentClass.MakeCheckCopy))
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
                return true;
            }
            else
                return false;
        }

        private bool PayByVisaCard()
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
            return true;
        }

        public void SavePaymentDetail()
        {
            if (paymentClass.IdPayment == -1)
                throw new Exception("Нельзя сохранить делали платежа в таблицу \"dbo.Payment_Parameter_Detail\", т.к. идентификатор платежа ID_Payment = -1");
            gridPaymentParameter1.SaveData();
        }

        private void FrmPayEllis_Shown(object sender, EventArgs e)
        {
            gridPaymentParameter1.LoadLayout("gridPaymentParameterEllis");
            gridPaymentParameter1.LoadData(paymentClass);
        }

        DataTable ellisTableinfo;
        private void LoadEllisInformation()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                ellisTableinfo = DAccess.DataModule.GetPaymentEllisInfo(txtAuthcod.Text.Replace(" ", ""));

                if (!ValidatePayment())
                {
                    this.Cursor = Cursors.Default;
                    return;
                }

                txtClientName.Text = ellisTableinfo.Rows[0]["КОМ_НАИМ"].ToString();
                txtClientBill.Text = ellisTableinfo.Rows[0]["КОМ_РС"].ToString();
                panelPayment1.PaymentSumm = (decimal)ellisTableinfo.Rows[0]["Sum_Tariff"];
                gridPaymentParameter1.SetValue("CLIENT_ID", ellisTableinfo.Rows[0]["ID_Company"].ToString());
                gridPaymentParameter1.SetValue("AUTHCOD", txtAuthcod.Text.Replace(" ", ""));
                gridPaymentParameter1.SetValue("ADRES", ellisTableinfo.Rows[0]["Address"].ToString());
                gridPaymentParameter1.SetValue("UK", ellisTableinfo.Rows[0]["Derive_Company_Name"].ToString());
                
            }
            catch (Exception ex)
            {
                panelPayment1.SetAllowPayment(false);
                MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally {
                this.Cursor = Cursors.Default;
            }
        }

        private bool ValidatePayment()
        {
            if (ellisTableinfo == null || ellisTableinfo.Rows.Count == 0)
            {
                MessageBox.Show("По данному коду авторизации нет информации в базе данных. Прием платежа запрещен.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                panelPayment1.SetAllowPayment(false);
                return false;
            }

            if (!DAccess.Instance.CheckClientFilter((int)ellisTableinfo.Rows[0]["ID_Company"]))
            {
                MessageBox.Show("Прием платежей на расчетный счет \"" + ellisTableinfo.Rows[0]["КОМ_НАИМ"].ToString() + "\" не производится.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                panelPayment1.SetAllowPayment(false);
                return false;
            }

            panelPayment1.SetAllowPayment(true);
            return true;
        }

        private void txtAuthcod_Leave(object sender, EventArgs e)
        {
            panelPayment1.SetDefaultFocus();
        }

        private void panelPayment1_CommentFieldLostFocusEvent(PanelPayment sender, EventArgs e)
        {
            gridPaymentParameter1.Focus();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Enter | Keys.Control))
            {
                panelPayment1.DoPay(new EventArgs());
                return true;
            }
            if (keyData == (Keys.D1 | Keys.Control))
            {
                DAccess.CashTerminal.OpenDrawer();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void txtAuthcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
            {
                if ((txtAuthcod.Text.Replace(" ", "")).Length != 24)
                {
                    MessageBox.Show("Код авторизации указан неверно.  Длина кода авторизации должна быть 24 знака.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                LoadEllisInformation();
                panelPayment1.SetDefaultFocus();
            }
        }

        private void txtAuthcod_TextChanged(object sender, EventArgs e)
        {
            if ((txtAuthcod.EditValue.ToString().Replace(" ","")).Length == 24)
            {
                LoadEllisInformation();
                panelPayment1.SetDefaultFocus();
            }
        }

        private void FrmPayEllis_FormClosing(object sender, FormClosingEventArgs e)
        {
            gridPaymentParameter1.SaveLayout();
        }
    }
}
