﻿using System;
using System.Data.SqlClient;

namespace EllisBillFilter.LoadMeasure
{
    /// <summary>
    /// Класс для хранения данных по счетчику из файла
    /// </summary>
    public class Meter
    {
        private const string DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";

        private int _idMeter;
        private DateTime _measureDate;
        private decimal _measureValue;

        public int IdMeter
        {
            get { return _idMeter; }
            //private set { _idMeter = value; }
        }

        public DateTime MeasureDate
        {
            get { return _measureDate; }
            //private set { _measureDate = value; }
        }

        public decimal MeasureValue
        {
            get { return _measureValue; }
            //private set { _measureValue = value; }
        }

        public string SqlFormattedDate => MeasureDate.ToString(DATE_FORMAT);

        public Meter(int id, DateTime date, decimal value)
        {
            _idMeter = id;
            _measureDate = date;
            _measureValue = value;
        }

        public void Load(SqlCommand command)
        {
            try { command.ExecuteNonQuery(); }
            catch (Exception ex)
            {
                command.Transaction?.Rollback();
                using (var sw = new System.IO.StreamWriter("MeterErrors.txt", true, System.Text.Encoding.Default))
                {
                    string errorLine = $"{DateTime.Now}:: ({_idMeter};{_measureDate};{_measureValue}) -- {ex.Message}";
                    sw.WriteLine(errorLine);
                }
            }
        }

        public decimal GetLastMeasureValue(SqlCommand command)
        {
            decimal lastMeasureValue = decimal.Parse(command.ExecuteScalar().ToString());
            return lastMeasureValue;
        }
    }
}
