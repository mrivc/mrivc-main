﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;
using WindowsFormsApplication1.DS;
using WindowsFormsApplication1.Helpers;
using WindowsFormsApplication1.Properties;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            PrepareAndSend();
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
        }

        public static void PrepareAndSend()
        {
            FileHelper.WriteLogEmptyLine();
            FileHelper.WriteLog("Отправка файла.");

            try
            {
                DataTable dt = DModule.Instance.GetClientDetailBank();

                FileHelper.CreateDirectory(Settings.Default.AttachmentFilesPath);
                string attachmentFile = (Settings.Default.AttachmentFilesPath.EndsWith("\\") ? Settings.Default.AttachmentFilesPath : Settings.Default.AttachmentFilesPath + "\\") +
                    "Приложение №1 от " + DateTime.Now.ToShortDateString() + ".xls";

                FileHelper.WriteLog("Файл:" + attachmentFile);

                ExcelHelper2.ExportToExcel2(dt, attachmentFile);

                SendMail(Settings.Default.SmtpServerHost,
                    Settings.Default.SmtpServerMailFrom,
                    Settings.Default.SmtpServerPswd,
                    Settings.Default.MailTo,
                    Settings.Default.MailCopyTo,
                    Settings.Default.MailSubject + " (" + DateTime.Now.ToShortDateString() + ")",
                    Settings.Default.MailBody,
                    attachmentFile);

                FileHelper.RewriteFileWithRename(attachmentFile, "Приложение №1 от " + DateTime.Now.ToShortDateString() + "_отправлено.xls");

                FileHelper.WriteLog("Файл отправлен успешно.");
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog("ОШИБКА:" + ex.Message);
                MessageBox.Show("ОШБИКА В МОДУЛЕ РАССЫЛКИ ПРИЛОЖЕНИЯ 1 (FpsClientDetailSend)" + Environment.NewLine + ex.Message);
            }
        }


        public static void SendMail(string smtpServer, string from, string password, string mailto, string mailcopyto, string caption, string message, string attachFile = null)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);

                string[] mailsTo = mailto.Split(';');
                string[] mailsCopyTo = mailcopyto.Split(';');

                foreach (var s in mailsTo)
                    if (s.Length > 0)
                        mail.To.Add(new MailAddress(s));

                foreach (var s in mailsCopyTo)
                    if (s.Length > 0)
                        mail.CC.Add(new MailAddress(s));

                mail.Subject = caption;
                mail.Body = message;
                if (!string.IsNullOrEmpty(attachFile))
                    mail.Attachments.Add(new Attachment(attachFile));

                var smtpClient = new SmtpClient(smtpServer, 25);
                smtpClient.Credentials = new NetworkCredential(from, password);
                smtpClient.EnableSsl = false;
                smtpClient.Send(mail);

                mail.Dispose();
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog("Mail.Send: " + ex.Message);
                MessageBox.Show("ОШБИКА В МОДУЛЕ РАССЫЛКИ ПРИЛОЖЕНИЯ 1 (FpsClientDetailSend)" + Environment.NewLine + ex.Message);
            }
        }
    }
}
