﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using FastReport.Utils;
using System.IO;
using FastReport.Export.Html;
using FastReport.Dialog;
using FastReport.Data;

namespace FastReport.Web.Handlers
{
    public partial class WebExport : IHttpHandler
    {

        private void StartReport(StringBuilder sb, HttpContext context)
        {
            sb.AppendLine(webReport.Toolbar.GetHtmlHeader());
        }

        private void GetReportTopToolbar(StringBuilder sb)
        {
            if (webReport.Prop.ShowToolbar)
                GetToolbar(sb);
        }

        private void GetReportBottomToolbar(StringBuilder sb)
        {
            if (webReport.Prop.ShowBottomToolbar)
                GetToolbar(sb);
        }

        private void GetToolbar(StringBuilder sb)
        {
            string s = webReport.Toolbar.GetHtmlBody();
            sb.AppendLine(s);
        }

        private void EndReport(StringBuilder sb)
        {
            sb.AppendLine(webReport.Toolbar.GetHtmlFooter());
        }

        private void GetReportHTML(StringBuilder sb, WebReport webReport, HttpContext context)
        {
            if (webReport != null)
            {
                if (Properties.State == ReportState.Empty)
                {
                    ReportLoad(context);
                    ReportRegisterDataAndRunEvent();
                    if (!Properties.ReportDone)
                        ReportPrepare();
//                    else if (Report.PreparedPages != null)
//                        Properties.TotalPages = Report.PreparedPages.Count;
                }

                if (Properties.State == ReportState.Forms)
                    ProcessDialogs(sb, webReport, context);

                if (Properties.State == ReportState.Report)
                {
                    Report.PreparePhase2();
                    if (Report.PreparedPages != null)
                    {
                        Properties.State = ReportState.Done;
//                        Properties.TotalPages = Report.PreparedPages.Count;
                    }
                }

                if (Properties.State == ReportState.Done)
                {
                    Properties.TotalPages = Report.PreparedPages.Count;
                    ReportInHtmlAndSave(sb, webReport, context);
                }
            }
            else
                log.Add("Object not found");
        }

        private void ReportPrepare()
        {
            Report.PreparePhase1();
            Properties.State = ReportState.Forms;
        }

        private void ReportInHtmlAndSave(StringBuilder sb, WebReport webReport, HttpContext context)
        {
            HTMLExport html = new HTMLExport();
            html.StylePrefix = webReport.Prop.ControlID;
            html.Init_WebMode();
            html.Pictures = Properties.Pictures;

            // calc zoom
            CalcHtmlZoom(html);

            html.Layers = webReport.Layers; 
            html.PageNumbers = webReport.SinglePage ? "" : (Properties.CurrentPage + 1).ToString();

            if (Properties.AutoWidth)
                html.WidthUnits = HtmlSizeUnits.Percent;
            if (Properties.AutoHeight)
                html.HeightUnits = HtmlSizeUnits.Percent;

            html.WebImagePrefix = String.Concat(context.Response.ApplyAppPathModifier(WebUtils.HandlerFileName), "?", WebUtils.PicsPrefix);
            html.Export(webReport.Report, (Stream)null);

            sb.Append("<div id=\"frbody\">");
            if (html.Count > 0)
            {
                if (webReport.SinglePage)
                    for (int i = 0; i < html.Count; i++)
                        DoHtmlPage(sb, html, i);
                else
                    DoHtmlPage(sb, html, 0);
            }
            sb.Append("</div>");
        }

        private void DoHtmlPage(StringBuilder sb, HTMLExport html, int pageN)
        {
            if (html.PreparedPages[pageN].PageText == null)
            {
                html.ProcessPage(pageN, Properties.CurrentPage);
                Properties.CurrentWidth = html.PreparedPages[pageN].Width;
                Properties.CurrentHeight = html.PreparedPages[pageN].Height;
            }
            if (html.PreparedPages[pageN].CSSText != null
                && html.PreparedPages[pageN].PageText != null)
            {
                sb.Append(html.PreparedPages[pageN].CSSText);
                sb.Append(html.PreparedPages[pageN].PageText);
                StoreHtmlPictures(html);
            }
        }

        private void StoreHtmlPictures(HTMLExport html)
        {
            for (int i = 0; i < html.PreparedPages[0].Pictures.Count; i++)
            {
                Stream stream = html.PreparedPages[0].Pictures[i];
                byte[] image = new byte[stream.Length];
                stream.Position = 0;
                int n = stream.Read(image, 0, (int)stream.Length);
                PutStorage(String.Empty, html.PreparedPages[0].Guids[i], image, Properties.CacheDelay, Properties.CachePriority);
            }
        }

        private void CalcHtmlZoom(HTMLExport html)
        {
            if (Properties.ZoomMode == ZoomMode.Width)
                html.Zoom = (float)Math.Round((float)(webReport.Width.Value - webReport.Padding.Horizontal - 20) / Properties.CurrentWidth, 2);
            else if (Properties.ZoomMode == ZoomMode.Page)
                html.Zoom = (float)Math.Round(Math.Min((float)(webReport.Width.Value - webReport.Padding.Horizontal) / Properties.CurrentWidth,
                    (float)(webReport.Height.Value - webReport.Padding.Vertical - 
                    (Properties.ShowToolbar ? Properties.ToolbarHeight : 0) - 
                    (Properties.ShowBottomToolbar ? Properties.ToolbarHeight : 0)) / Properties.CurrentHeight), 2);
            else
                html.Zoom = Properties.Zoom;
        }

        private void ReportRegisterDataAndRunEvent()
        {
            webReport.RegisterData(Report);
            webReport.OnStartReport(EventArgs.Empty);
            Config.ReportSettings.ShowProgress = false;
        }

        private void ReportLoad(HttpContext context)
        {
            Config.WebMode = true;
            ParameterCollection preParams = new ParameterCollection(null);
            Report.Parameters.CopyTo(preParams);
            if (!String.IsNullOrEmpty(Properties.ReportFile))
            {
                string fileName = Properties.ReportFile;
                if (!WebUtils.IsAbsolutePhysicalPath(fileName))
                    fileName = context.Request.MapPath(fileName,
                        Path.GetDirectoryName(context.Request.CurrentExecutionFilePath), true);
                Report.Load(fileName);
            }
            else if (!String.IsNullOrEmpty(Properties.ReportResourceString))
            {
                Report.ReportResourceString = Properties.ReportResourceString;
            }
            foreach (Parameter param in preParams)
            {
                Report.SetParameterValue(param.Name, param.Value);
            }
        }
    }
}
 