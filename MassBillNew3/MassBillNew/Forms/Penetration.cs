﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using FastReport;

namespace MassBillNew
{
    public partial class Penetration : Form
    {
        Report report;

        public Penetration()
        {
            
            
            InitializeComponent();
            
            
        }

        private void Penetration_Load(object sender, EventArgs e)
        {
            report = new Report();

            SqlClass.Open();
            byte[] module = (byte[])SqlClass.ExecuteScalar(string.Format("select convert(varbinary(max),[file]) from dbo.module where name ='{0}' ", "_Penetration_NET"));
            SqlClass.Close();

            report.LoadFromString(Encoding.UTF8.GetString(module));

             string sql = report.Parameters.FindByName("ServiceSqlQuery").Expression.Trim('"');

            SqlClass.Open();
            DataTable servTable = SqlClass.ExecuteToDataTable(sql);
            SqlClass.Close();

            checkedComboBoxEdit1.Properties.DataSource = servTable;
            checkedComboBoxEdit1.Properties.ValueMember = "id";
            checkedComboBoxEdit1.Properties.DisplayMember = "name";


            EnvironmentSettings environmentSettings = new EnvironmentSettings();
            environmentSettings.ReportSettings.ShowProgress = false;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.qPenetrTableAdapter.cmdTimeout = 0;
                this.qPenetrTableAdapter.Fill(this.penetrDataSet.qPenetr,checkedComboBoxEdit1.EditValue.ToString());
                
                report.SetParameterValue("MainConnection", StaticConfig.MainConnection.ConnectionString);

                foreach (DataRow row in penetrDataSet.qPenetr.Rows)
                {
                    report.Parameters.FindByName("id_provider").Value = row["id_provider"];
                    report.Parameters.FindByName("id_company").Value = row["id_company"];
                    report.Parameters.FindByName("accept_date").Value = row["accept_date"];
                    report.Parameters.FindByName("comp").Value = row["comp"];
                    report.Parameters.FindByName("prov").Value = row["prov"];
                    report.Prepare();

                    string ExportFileName = row["name"].ToString() ;

                    ExportFileName = ExportFileName.Replace("/", " ");
                    ExportFileName = ExportFileName.Replace("\\", " ");
                    ExportFileName = ExportFileName.Replace(":", " ");
                    ExportFileName = ExportFileName.Replace("*", " ");
                    ExportFileName = ExportFileName.Replace("?", " ");
                    ExportFileName = ExportFileName.Replace("\"", " ");
                    ExportFileName = ExportFileName.Replace("<", " ");
                    ExportFileName = ExportFileName.Replace(">", " ");
                    ExportFileName = ExportFileName.Replace("|", " ");
                    ExportFileName = ExportFileName.Replace("«", " ");
                    ExportFileName = ExportFileName.Replace("»", " ");

                    if (radioButton1.Checked)
                    {
                        FastReport.Export.Xml.XMLExport export = new FastReport.Export.Xml.XMLExport();

                        export.PageBreaks = false;
                        export.Wysiwyg = true;

                        export.Export(report, folderBrowserDialog1.SelectedPath + "\\" + ExportFileName + ".xls");
                    }
                    else
                    {
                        FastReport.Export.OoXML.Excel2007Export export = new FastReport.Export.OoXML.Excel2007Export();

                        export.PageBreaks = false;
                        export.Wysiwyg = true;

                        export.Export(report, folderBrowserDialog1.SelectedPath + "\\" + ExportFileName + ".xlsx");
                    }
                    
                }

                MessageBox.Show("Отчеты сформированы","Готово",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }
    }
}
