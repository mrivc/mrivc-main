﻿namespace Payments
{
    partial class FrmPayServices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPayServices));
            this.cbSplitPaymentByServices = new System.Windows.Forms.CheckBox();
            this.gridPaymentParameter1 = new Payments.Controls.GridPaymentParameter();
            this.panelPayment1 = new Payments.Controls.PanelPayment();
            this.SuspendLayout();
            // 
            // cbSplitPaymentByServices
            // 
            this.cbSplitPaymentByServices.AutoSize = true;
            this.cbSplitPaymentByServices.Checked = true;
            this.cbSplitPaymentByServices.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSplitPaymentByServices.Enabled = false;
            this.cbSplitPaymentByServices.Location = new System.Drawing.Point(6, 194);
            this.cbSplitPaymentByServices.Name = "cbSplitPaymentByServices";
            this.cbSplitPaymentByServices.Size = new System.Drawing.Size(299, 17);
            this.cbSplitPaymentByServices.TabIndex = 2;
            this.cbSplitPaymentByServices.Text = "Разбить платеж по услугам (печать разными чеками)";
            this.cbSplitPaymentByServices.UseVisualStyleBackColor = true;
            this.cbSplitPaymentByServices.CheckedChanged += new System.EventHandler(this.cbSplitPaymentByServices_CheckedChanged);
            // 
            // gridPaymentParameter1
            // 
            this.gridPaymentParameter1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPaymentParameter1.Location = new System.Drawing.Point(6, 217);
            this.gridPaymentParameter1.Name = "gridPaymentParameter1";
            this.gridPaymentParameter1.Size = new System.Drawing.Size(439, 393);
            this.gridPaymentParameter1.TabIndex = 1;
            // 
            // panelPayment1
            // 
            this.panelPayment1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPayment1.CashType = 1;
            this.panelPayment1.Location = new System.Drawing.Point(3, 2);
            this.panelPayment1.Name = "panelPayment1";
            this.panelPayment1.PaymentCash = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.panelPayment1.PaymentComment = "";
            this.panelPayment1.PaymentDate = new System.DateTime(2019, 6, 7, 13, 49, 16, 0);
            this.panelPayment1.PaymentSumm = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.panelPayment1.PhoneEmail = "";
            this.panelPayment1.Size = new System.Drawing.Size(447, 189);
            this.panelPayment1.TabIndex = 0;
            this.panelPayment1.PayEvent += new Payments.Controls.PanelPayment.PayEventHandler(this.panelPayment1_PayEvent);
            this.panelPayment1.CommentFieldLostFocusEvent += new Payments.Controls.PanelPayment.CommentFieldEventHandler(this.panelPayment1_CommentFieldLostFocusEvent);
            this.panelPayment1.Load += new System.EventHandler(this.panelPayment1_Load);
            // 
            // FrmPayServices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 612);
            this.Controls.Add(this.cbSplitPaymentByServices);
            this.Controls.Add(this.gridPaymentParameter1);
            this.Controls.Add(this.panelPayment1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(471, 651);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(471, 651);
            this.Name = "FrmPayServices";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оплата прочих услуг";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPayServices_FormClosing);
            this.Shown += new System.EventHandler(this.FrmPayServices_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.PanelPayment panelPayment1;
        private Controls.GridPaymentParameter gridPaymentParameter1;
        private System.Windows.Forms.CheckBox cbSplitPaymentByServices;
    }
}