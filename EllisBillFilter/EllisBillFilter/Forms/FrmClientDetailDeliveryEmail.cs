﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using EllisBillFilter.DS;

    public partial class FrmClientDetailDeliveryEmail : Form
    {
        public FrmClientDetailDeliveryEmail(string checkedEmailIDs)
        {
            InitializeComponent();
            this.emailIDs = checkedEmailIDs;
        }

        public FrmClientDetailDeliveryEmail()
        {
            InitializeComponent();
        }

        private string emailNames;
        public string EmailNames {
            get
            {
                return emailNames;
            }
        }

        private string emailIDs;
        public string EmailIDs {
            get
            {
                return emailIDs;
            }
        }

        private string emails;
        public string Emails
        {
            get
            {
                return emails;
            }
        }

        private void LoadData()
        {
            DAccess.DataModule.ClientDetailDeliveryEmailSelect(mDataSet.ClientDetailDeliveryEmail);
            gridEmail.InitEx();
            gridEmail.DisableGrid();
            CheckEmails();
        }

        private void CheckEmails()
        {
            if(emailIDs.Length == 0)
                return;

            string[] emails = emailIDs.Split(',');

            foreach (DataRow row in mDataSet.ClientDetailDeliveryEmail.Rows)
            {
                if (emails.Any(id => (int)row["ID_Email"] == int.Parse(id)))
                {
                    row["Checked"] = true;
                }
            }
        }

        private void FrmClientDetailDeliveryEmail_Load(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            emailIDs = gridEmail.GetCheckedRows("ID_Email", ",");
            emailNames = gridEmail.GetCheckedRows("Name", ";");
            emails = gridEmail.GetCheckedRows("Emails", ";");
            DialogResult = DialogResult.OK;
        }

        private void gridEmail_SaveData()
        {
            try
            {
                DAccess.DataModule.ClientDetailDeliveryEmailUpdate(mDataSet.ClientDetailDeliveryEmail);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbGridEditable_CheckStateChanged(object sender, EventArgs e)
        {
            if (cbGridEditable.Checked)
            {
                gridEmail.EnableGrid();
                miAddRecord.Enabled = true;
                miDeleteRecord.Enabled = true;
            }
            else
            {
                gridEmail.DisableGrid();
            }
        }

        private void gridViewEMail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            string s = "";
        }

        private void miAddRecord_Click(object sender, EventArgs e)
        {
            gridEmail.AddNewRow();
        }

        private void miDeleteRecord_Click(object sender, EventArgs e)
        {
            gridEmail.DeleteActiveRow(true);
        }
    }
}
