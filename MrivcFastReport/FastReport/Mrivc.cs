﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.Common;
using FastReport.Data;
using System.Data;

namespace FastReport
{
    class Mrivc
    {
        public static void UploadToEllis(Report report, string connectionString)
        {
            if (report == null)
                return;

            string reportString = report.SaveToString();

            byte[] bytes = Encoding.UTF8.GetBytes(reportString);
            reportString = Encoding.GetEncoding(1251).GetString(bytes);

            string reportFileName = Path.GetFileNameWithoutExtension(report.FileName);
            string reportName = report.ReportInfo.Name;
            string reportInfo = report.ReportInfo.Description;

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = "select 1 from module where name = case when substring(@reportFileName,1,1) = '_' then @reportFileName else '_'+@reportFileName end";
            comm.Parameters.AddWithValue("reportFileName", reportFileName);
            conn.Open();

            if (comm.ExecuteScalar() == null)
            {
                DialogResult res = MessageBox.Show(string.Format("Отчета нет в базе. Создать отчет {0}?", reportFileName), "Загрузка в Эллис-ЖКХ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No)
                    return;
            }

            comm.Parameters.Clear();
            comm.CommandText = "town.Upload_Frx_Report";
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("new_name", reportFileName);
            comm.Parameters.AddWithValue("new_file", reportString);
            comm.Parameters.AddWithValue("new_display_name", reportName);
            comm.Parameters.AddWithValue("new_info", reportInfo);

            comm.Parameters.Add("new_version_out", SqlDbType.Int);
            comm.Parameters["new_version_out"].Direction = ParameterDirection.Output;

            comm.ExecuteNonQuery();

            object ver = comm.Parameters["new_version_out"].Value;
            conn.Close();

            MessageBox.Show("Отчет успешно загружен.\n\rНовая версия: "+ver.ToString(), "Загрузка в Эллис-ЖКХ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static bool TestSqlConnection(string connectionString, out string error)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }

            error = "OK";
            return true;
        }
    }
}
