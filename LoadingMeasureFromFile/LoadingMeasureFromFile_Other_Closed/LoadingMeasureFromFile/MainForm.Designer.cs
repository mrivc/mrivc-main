﻿namespace LoadingMeasureFromFile_Other
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.filePath = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openFile = new System.Windows.Forms.Button();
            this.btnLoadFromFile = new System.Windows.Forms.Button();
            this.openLogBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // filePath
            // 
            this.filePath.AllowDrop = true;
            this.filePath.Location = new System.Drawing.Point(12, 18);
            this.filePath.Name = "filePath";
            this.filePath.Size = new System.Drawing.Size(200, 20);
            this.filePath.TabIndex = 0;
            this.filePath.DragDrop += new System.Windows.Forms.DragEventHandler(this.filePath_DragDrop);
            this.filePath.DragOver += new System.Windows.Forms.DragEventHandler(this.filePath_DragOver);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "source";
            // 
            // openFile
            // 
            this.openFile.Location = new System.Drawing.Point(222, 12);
            this.openFile.Name = "openFile";
            this.openFile.Size = new System.Drawing.Size(100, 30);
            this.openFile.TabIndex = 1;
            this.openFile.Text = "Открыть файл";
            this.openFile.UseVisualStyleBackColor = true;
            this.openFile.Click += new System.EventHandler(this.openFile_Click);
            // 
            // btnLoadFromFile
            // 
            this.btnLoadFromFile.Location = new System.Drawing.Point(127, 69);
            this.btnLoadFromFile.Name = "btnLoadFromFile";
            this.btnLoadFromFile.Size = new System.Drawing.Size(80, 30);
            this.btnLoadFromFile.TabIndex = 2;
            this.btnLoadFromFile.Text = "Загрузить";
            this.btnLoadFromFile.UseVisualStyleBackColor = true;
            this.btnLoadFromFile.Click += new System.EventHandler(this.btnLoadFromFile_Click);
            // 
            // openLogBtn
            // 
            this.openLogBtn.Location = new System.Drawing.Point(287, 74);
            this.openLogBtn.Name = "openLogBtn";
            this.openLogBtn.Size = new System.Drawing.Size(35, 25);
            this.openLogBtn.TabIndex = 3;
            this.openLogBtn.Text = "Log";
            this.openLogBtn.UseVisualStyleBackColor = true;
            this.openLogBtn.Click += new System.EventHandler(this.openLogBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 111);
            this.Controls.Add(this.openLogBtn);
            this.Controls.Add(this.btnLoadFromFile);
            this.Controls.Add(this.openFile);
            this.Controls.Add(this.filePath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "Загрузка показаний из файла";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox filePath;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button openFile;
        private System.Windows.Forms.Button btnLoadFromFile;
        private System.Windows.Forms.Button openLogBtn;
    }
}

