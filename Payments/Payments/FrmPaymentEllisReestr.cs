﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Payments
{
    public partial class FrmPaymentEllisReestr : Form
    {
        public FrmPaymentEllisReestr()
        {
            InitializeComponent();
        }

        private void FrmPaymentEllisReestr_Load(object sender, EventArgs e)
        {
            var s = Properties.Settings.Default.PaymentsConnectionString;

            gridPaymentEllisReestr1.LoadData();
        }
    }
}
