﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MassBillNew
{
    public partial class Dialog_test : UserControl
    {
        public Dialog_test()
        {
            InitializeComponent();
        }

        private void Dialog_test_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);

            SqlDataAdapter da = new SqlDataAdapter("select id_company,name from dbo.company where is_derive_company = 1 order by name",conn);

            conn.Open();

            da.Fill(dt);

            conn.Close();

            checkedComboBoxEdit1.Properties.DataSource = dt;

            checkedComboBoxEdit1.Properties.DisplayMember = "name";

            checkedComboBoxEdit1.Properties.ValueMember = "id_company";
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }
    }
}
