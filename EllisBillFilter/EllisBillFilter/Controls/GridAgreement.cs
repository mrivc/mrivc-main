﻿using System;
using System.Windows.Forms;
using EllisBillFilter.DS;

using DevExpress.XtraGrid.Views.Grid;

namespace EllisBillFilter
{
    public partial class GridAgreement : UserControl
    {
        private DateTime beginDate;
        private DateTime endDate;

        public GridAgreement()
        {
            InitializeComponent();
        }

        public void LoadData(DateTime beginDate, DateTime endDate, bool allowEdit)
        {
            this.beginDate = beginDate;
            this.endDate = endDate;

            DAccess.DataModule.AgreementSelect(mDataSet.Agreement, beginDate, endDate);
            mDataSet.PayReceiverCompany.Merge(DAccess.Instance.PayReceiverCompany);
            mDataSet.Agreement_Type.Merge(DAccess.Instance.AgreementType);
            
            gridAgreements.InitEx();
            gridViewAgreement.ClearColumnsFilter();

            if (!allowEdit)
                gridAgreements.DisableGrid();
        }

        public void ReLoadData(DateTime beginDate, DateTime endDate)
        {
            this.Cursor = Cursors.WaitCursor;
            this.beginDate = beginDate;
            this.endDate = endDate;

            mDataSet.Agreement.Clear();
            gridViewAgreement.ClearColumnsFilter();
            DAccess.DataModule.AgreementSelect(mDataSet.Agreement, beginDate, endDate);
            this.Cursor = Cursors.Default;
        }

        public BaseGrid Grid
        {
            get { return gridAgreements; }
        }

        public GridView GridViewAgreement
        {
            get { return gridViewAgreement; }
        }

        private void gridAgreements_SaveData()
        {
            UpdateData();
        }

        private void UpdateData()
        {
            try
            {
                DAccess.DataModule.AgreementUpdate(mDataSet.Agreement);
            } catch (Exception ex)
            {
                if (ex.Message.Contains("FK_Client_Detail_Agreement_Agreement"))
                {
                    MessageBox.Show("Данный договор связан с кодом фильтра. Удаление запрещено.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ReLoadData(this.beginDate, this.endDate);
                }
                else
                    throw ex;
            }
        }

        private void gridViewAgreement_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridViewAgreement.GetDataRow(e.RowHandle)["ID_Agreement"] = -1;
            gridViewAgreement.GetDataRow(e.RowHandle)["CreationDate"] = DateTime.Now;
        }

        private void splitContainerControl1_SizeChanged(object sender, EventArgs e)
        {
            splitContainerControl1.SplitterPosition = (int)(splitContainerControl1.Size.Height / 1.3);
        }

        private void txtDescription_Leave(object sender, EventArgs e)
        {
            if (gridViewAgreement.FocusedRowHandle >= 0)
            {
                gridViewAgreement.GetDataRow(gridViewAgreement.FocusedRowHandle)["Description"] = txtDescription.Text;
                UpdateData();
            }
        }

        private void txtComment_Leave(object sender, EventArgs e)
        {
            if (gridViewAgreement.FocusedRowHandle >= 0)
            {
                gridViewAgreement.GetDataRow(gridViewAgreement.FocusedRowHandle)["Comment"] = txtComment.Text;
                UpdateData();
            }
        }

        private void gridViewAgreement_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewAgreement.FocusedRowHandle >= 0)
            {
                txtDescription.Text = gridViewAgreement.GetDataRow(gridViewAgreement.FocusedRowHandle)["Description"].ToString();
                txtComment.Text = gridViewAgreement.GetDataRow(gridViewAgreement.FocusedRowHandle)["Comment"].ToString();
            }
        }

        private void gridViewAgreement_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "Description")
                txtDescription.Text = e.Value.ToString();

            if (e.Column.FieldName == "Comment")
                txtComment.Text = e.Value.ToString();

        }

        private void miAgreementAdd_Click(object sender, EventArgs e)
        {
            gridAgreements.AddNewRow();
        }

        private void miAgreementDelete_Click(object sender, EventArgs e)
        {
            gridAgreements.DeleteActiveRow();
        }

        private void miAgreementRefresh_Click(object sender, EventArgs e)
        {
            ReLoadData(beginDate, endDate);
        }

        private void miAgreementExport_Click(object sender, EventArgs e)
        {
            gridAgreements.MyExport();
        }

        private void riAgent_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                gridViewAgreement.ActiveEditor.EditValue = DBNull.Value;
        }

        private void gridViewAgreement_DoubleClick(object sender, EventArgs e)
        {
            if (gridViewAgreement.FocusedColumn.FieldName.StartsWith("ID_Agent"))
            {
                FrmCompany f = new FrmCompany();
                if (f.ShowDialog() == DialogResult.OK)
                    gridViewAgreement.GetDataRow(gridViewAgreement.FocusedRowHandle)[gridViewAgreement.FocusedColumn.FieldName] = f.IdCompany;
            }
        }

        private void miAgreementDeleteCellValue_Click(object sender, EventArgs e)
        {
            if (gridViewAgreement.FocusedColumn.Caption.Contains("*"))
            {
                MessageBox.Show("Значение данной графы не может быть пустым!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gridViewAgreement.GetDataRow(gridViewAgreement.FocusedRowHandle)[gridViewAgreement.FocusedColumn.FieldName] = DBNull.Value;
            UpdateData();
        }
    }
}
