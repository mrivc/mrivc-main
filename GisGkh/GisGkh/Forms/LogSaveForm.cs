﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh.Forms
{
    public partial class LogSaveForm : Form
    {
        public LogSaveForm()
        {
            InitializeComponent();
        }

        private void btnSaveLog_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                var folderPath = folderBrowserDialog1.SelectedPath;
                var dt = new DataTable();

                string senderID = null;

                if (chbSender.Checked == true)
                {
                    senderID = senderIdOnlyFilter1.SenderID;
                }

                if (chbSaveSeveralErrors.Checked)
                {
                    dt = DAccess.DataModule.GetDataTableByStoredProcedure1("[GIS].[SaveLogByPeriodAndCode]", new object[,] { { "period", periodFilter.Period }, { "errorCode", gisErrorFilter.ErrorCode }, { "senderID", senderID } });
                }
                else
                {
                    var ID = int.Parse(tbLogID.Text);
                    dt = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.SaveLog_Test", new object[,] { { "ID", ID } });
                }

                if (dt.Rows.Count != 0)
                    foreach (DataRow r in dt.Rows)
                    {
                        //var path = Application.StartupPath.ToString() + "\\Errors\\" + r["RequestName"].ToString() + ".xml";
                        var path = folderPath + "\\" + r["RequestName"].ToString() + ".xml";
                        File.WriteAllText(path, r["SendRequest"].ToString());

                        //path = Application.StartupPath.ToString() + "\\Errors\\" + r["ResponseName"].ToString() + ".xml";
                        path = folderPath + "\\" + r["ResponseName"].ToString() + ".xml";
                        File.WriteAllText(path, r["Response"].ToString());
                    }
            }
        }

        private void LogSaveForm_Load(object sender, EventArgs e)
        {
            periodFilter.Fill();
            gisErrorFilter.Period = periodFilter.Period;
            gisErrorFilter.Fill();
            senderIdOnlyFilter1.Fill();
        }

        private void btnRefreshErrors_Click(object sender, EventArgs e)
        {
            gisErrorFilter.Period = periodFilter.Period;
            gisErrorFilter.Fill();
        }

        private void chbSender_CheckedChanged(object sender, EventArgs e)
        {
            senderIdOnlyFilter1.Enabled = chbSender.Checked;
        }
    }
}
