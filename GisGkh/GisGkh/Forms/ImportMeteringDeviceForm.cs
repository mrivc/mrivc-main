﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class ImportMeteringDeviceForm : Form
    {
        public string SenderID
        {
            get { return ""; }
        }

        public ImportMeteringDeviceForm()
        {
            InitializeComponent();
        }

        private void prepareBtn_Click(object sender, EventArgs e)
        {
            var importMeteringDevice = new ImportMeteringDevice(SenderID);
            importMeteringDevice.UpdateEnabled = checkBox1.Checked;
            var ds = MrivcStatic.HouseIteration(importMeteringDevice, true);

            meterDataGrid.DataSource = ds.Tables[0];
            flatMeterDataGridView.DataSource = ds.Tables[1];
            houseMeterDataGridView.DataSource = ds.Tables[2];

            importBtn.Enabled = true;
        }

        private void ImportMeteringDeviceForm_Load(object sender, EventArgs e)
        {
            importBtn.Enabled = true;

        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            var importMeteringDevice = new ImportMeteringDevice(SenderID);
            importMeteringDevice.UpdateEnabled = checkBox1.Checked;
            MrivcStatic.HouseIteration(importMeteringDevice, false);
        }
    }
}
