using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using FastReport.Preview;
using FastReport;
using System.Data.SqlClient;
using FastReport.Data;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using System.IO;
using System.Xml;

namespace MassBillNew
{
    public partial class XtraFrxPreview : DevExpress.XtraEditors.XtraForm
    {
        PreviewControl previewControl = new PreviewControl();

        public XtraFrxPreview(string file ,bool fromFile )
        {
            InitializeComponent();

            reportLoadedFromFile = fromFile;
            fileName = file;

            if(reportLoadedFromFile == false)
            this.Text = GetReportDisplayName(file);

            previewControl.Dock = DockStyle.Fill;
            previewControl.UIStyle = FastReport.Utils.UIStyle.VisualStudio2005;

            previewControl.ToolbarVisible = false;
            previewControl.StatusbarVisible = false;


            this.tableLayoutPanel1.Controls.Add(previewControl, 0, 0); ;

          
            
        }

        private string GetReportDisplayName(string fileName)
        {
            SqlConnection conn = new SqlConnection(StaticConfig.MainConnection.ConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select display_name from dbo.module where name = @file";
            cmd.Parameters.Add("file",SqlDbType.VarChar);
            cmd.Parameters[0].Value = fileName;
            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            string display = string.Empty;

            conn.Open();
            display = cmd.ExecuteScalar().ToString();
            conn.Close();

            return display;
        }

        private bool reportLoadedFromFile = false;
        public bool ReportLoadedFromFile
        {
            get
            {
                return reportLoadedFromFile;
            }
            set
            {
                reportLoadedFromFile = value;
            }
        }

        private string fileName = "";
        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public void ShowReport(Report report)
        {
            if (report != null)
            {
                report.Preview = previewControl;
                report.ShowPrepared();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            XtraFrxMenu.FrxDialogShow(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            previewControl.Print();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            // ������� ��������� �������� � HTML
            FastReport.Export.Xml.XMLExport export = new FastReport.Export.Xml.XMLExport();

            export.Wysiwyg = true;
            export.OpenAfterExport = true;

            // ���������� ������ � ����������� �������� � ������������ �����
            if (export.ShowDialog())
            {
                saveFileDialog1.DefaultExt = "xls";
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = "�������� Excel 2003|*.xls";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {


                    export.Export(previewControl.Report, saveFileDialog1.FileName);
                }



            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void XtraFrxPreview_Load(object sender, EventArgs e)
        {
            // TODO: ������ ������ ���� ��������� ��������� ������ � ������� "murmanskDataSet.VPeriod". ��� ������������� ��� ����� ���� ���������� ��� �������.
            this.vPeriodTableAdapter.Fill(this.murmanskDataSet.VPeriod);
            
        }

        private void XtraFrxPreview_Shown(object sender, EventArgs e)
        {
            XtraFrxDialog frxDialog = new XtraFrxDialog(fileName, reportLoadedFromFile);
            //frxDialog.Parent = this;



            if (frxDialog.ShowDialog() == DialogResult.OK)
            {
                if (!frxDialog.CreateManyReports)//���� �� ������� ��������� �������, �� ���������� ����� �� ����� ���������
                {
                    this.ShowReport(frxDialog.ReturnedReport);

                }
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {


            // ������� ��������� �������� � HTML
            FastReport.Export.Pdf.PDFExport export = new FastReport.Export.Pdf.PDFExport();

            export.OpenAfterExport = true;

            // ���������� ������ � ����������� �������� � ������������ �����
            if (export.ShowDialog())
            {
                saveFileDialog1.DefaultExt = "pdf";
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = "�������� PDF|*.pdf";


                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {

                    export.Export(previewControl.Report, saveFileDialog1.FileName);
                }



            }
        }

        void ExportToDbf(Report report,Parameter par)
        {
            // ������� ��������� �������� � HTML
            FastReport.Export.Dbf.DBFExport export = new FastReport.Export.Dbf.DBFExport();

            string fields = string.Empty;

            TableDataSource qMaster = report.GetDataSource(par.Expression.Trim('"')) as TableDataSource;

            foreach (Column column in qMaster.Columns)
            {
                fields += column.Name + ";";
            }

            fields = fields.Trim(';');

            export.FieldNames = fields;

            // ���������� ������ � ����������� �������� � ������������ �����
            if (export.ShowDialog())
            {
                saveFileDialog1.DefaultExt = "dbf";
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = "������� DBF|*.dbf";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {

                    export.Export(report, saveFileDialog1.FileName);
                }
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Report report = this.previewControl.Report;

            bool f = false;

            foreach (Parameter par in report.Parameters)
            {
                if (par.Name.ToLower() == "dbfdatasource")
                {
                    f = true;
                    ExportToDbf(report,par);
                }

            }

            if (f == false)
            {
                MessageBox.Show("��� �������� � dbf ���������� ������� �������� DbfDataSource, ���������� ��� ������� � ������� (��������: qMaster)", "�������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        private void customEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                MessageBox.Show("Test");
            }
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {

            // ������� ��������� �������� � HTML
            FastReport.Export.OoXML.Excel2007Export export = new FastReport.Export.OoXML.Excel2007Export();

            export.Wysiwyg = true;
            export.OpenAfterExport = true;

            // ���������� ������ � ����������� �������� � ������������ �����
            if (export.ShowDialog())
            {
                saveFileDialog1.DefaultExt = "xlsx";
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = "�������� Excel 2007|*.xlsx";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    export.Export(previewControl.Report, saveFileDialog1.FileName);
                }
            }
            
        }

        void ExportToXml(Report report, Parameter par)
        {
            string[] dataSourceList = par.Expression.Trim('"').Split(';');

            saveFileDialog1.DefaultExt = "xml";
            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "���� Xml|*.xml";

            SqlClass.Open();

            foreach (Parameter p in par.Parameters)
            {
                if (p.Name.ToLower() == "createtable")
                    SqlClass.ExecuteNonQuery(p.Expression.Trim('"'));
            }

            foreach (string dataSource in dataSourceList)
            {
                TableDataSource tableDataSource = report.GetDataSource(dataSource) as TableDataSource;

                DataTable localTempTable = tableDataSource.Table;

                localTempTable.AcceptChanges();

                SqlClass.CopyData(localTempTable, "#" + dataSource);
            }

            object[] result = new object[2];

            object xmlFileName = null;

            foreach (Parameter p in par.Parameters)
            {
                if (p.Name.ToLower() == "procedure")
                {
                    object[,] prms = new object[p.Parameters.Count,2];

                    for (int i = 0; i < p.Parameters.Count; i++)
                    {
                        prms[i,0] = p.Parameters[i].Name;
                        prms[i,1] = p.Parameters[i].Value;
                    }

                    result = SqlClass.ExecuteScalarSP(p.Expression.Trim('"'), prms, xmlFileName, "filename", SqlDbType.VarChar,100);

                }



            }

            SqlClass.Close();

            //if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            //{
            //    string path = saveFileDialog1.FileName;

            //    using (StreamWriter sw = new StreamWriter(path, false, Encoding.GetEncoding(1251)))
            //    {
            //        sw.WriteLine(result[0].ToString());
            //    }

            //}

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;

                XmlDocument xmldoc = new XmlDocument();

                xmldoc.LoadXml(result[0].ToString());

                using (StreamWriter sw = new StreamWriter(path + "\\" + result[1].ToString() + ".xml", false, Encoding.GetEncoding(1251)))
                {
                    xmldoc.Save(sw);
                    //sw.WriteLine(result[0].ToString());
                }

            }
        }


        public enum ExcelFormat{ 
        xls,
        xlsx
        }

        void ExportToExcel(Report report, Parameter par, ExcelFormat format)
        {
            string ext = string.Empty;
            string filter = string.Empty;

            switch (format)
            {
                case ExcelFormat.xls:
                    {
                        ext = "xls";
                        filter = "�������� Excel 2003|*.xls";
                    }
                    break;
                case ExcelFormat.xlsx:
                    {
                        ext = "xlsx";
                        filter = "�������� Excel 2007|*.xlsx";
                    }
                    break;
            }
            
            // ������� ��������� �������� � HTML
            FastReport.Export.Xml.XMLExport export = new FastReport.Export.Xml.XMLExport();

            export.Wysiwyg = true;
            export.OpenAfterExport = true;

            // ���������� ������ � ����������� �������� � ������������ �����
            if (export.ShowDialog())
            {
                saveFileDialog1.DefaultExt = ext;
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = filter;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    export.Export(previewControl.Report, saveFileDialog1.FileName);
                }
            }
        }

        void ExportToXmlWithExcel(ExcelFormat format)
        {
            Report report = this.previewControl.Report;

            bool f = false;

            foreach (Parameter par in report.Parameters)
            {
                if (par.Name.ToLower() == "xmldatasource")
                {
                    f = true;
                    ExportToXml(report, par);
                    //ExportToExcel(report, par, format);
                }

            }

            if (f == false)
            {
                MessageBox.Show("��� �������� � xml ���������� ������� �������� XmlDataSource, ���������� ������ ���� �������� � ������� ����� \";\" (��������: qMaster;qDetail)", "�������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToXmlWithExcel(ExcelFormat.xls);
        }

        private void btnExportToWord_ItemClick(object sender, ItemClickEventArgs e)
        {
            string ext = "docx";
            string filter = "�������� MS Word 2007|*.docx";

            // ������� ��������� �������� � HTML
            FastReport.Export.OoXML.Word2007Export export = new FastReport.Export.OoXML.Word2007Export();

            export.Wysiwyg = true;
            export.OpenAfterExport = true;

            // ���������� ������ � ����������� �������� � ������������ �����
            if (export.ShowDialog())
            {
                saveFileDialog1.DefaultExt = ext;
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = filter;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    export.Export(previewControl.Report, saveFileDialog1.FileName);
                }
            }
        }

        private void btnExportToRTF_ItemClick(object sender, ItemClickEventArgs e)
        {
            string ext = "rtf";
            string filter = "�������� RTF|*.rtf";

            // ������� ��������� �������� � HTML
            FastReport.Export.RichText.RTFExport export = new FastReport.Export.RichText.RTFExport();

            export.Wysiwyg = true;
            export.OpenAfterExport = true;

            // ���������� ������ � ����������� �������� � ������������ �����
            if (export.ShowDialog())
            {
                saveFileDialog1.DefaultExt = ext;
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = filter;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    export.Export(previewControl.Report, saveFileDialog1.FileName);
                }
            }
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            string ext = "csv";
            string filter = "csv |*.csv";

            // ������� ��������� �������� � HTML
            FastReport.Export.Csv.CSVExport export = new FastReport.Export.Csv.CSVExport();

            export.DataOnly = true;
            export.OpenAfterExport = true;

            // ���������� ������ � ����������� �������� � ������������ �����
            if (export.ShowDialog())
            {
                saveFileDialog1.DefaultExt = ext;
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = filter;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    export.Export(previewControl.Report, saveFileDialog1.FileName);
                }
            }
        }
    }
}