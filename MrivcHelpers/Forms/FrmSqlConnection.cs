﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MrivcHelpers.Forms
{
    public partial class FrmSqlConnection : Form
    {
        public FrmSqlConnection(string server = "", string database = "", string login = "", string password = "")
        {
            InitializeComponent();

            txtServer.Text = server;
            txtDatabase.Text = database;
            txtLogin.Text = login;
            txtPassword.Text = password;
        }

        public SqlConnectionStringBuilder ConnectionBuilder
        {
            get
            {
                SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder();
                connectionBuilder = new SqlConnectionStringBuilder();
                connectionBuilder.ConnectTimeout = 10;
                connectionBuilder.DataSource = txtServer.Text;
                connectionBuilder.InitialCatalog = txtDatabase.Text;
                connectionBuilder.UserID = txtLogin.Text;
                connectionBuilder.Password = txtPassword.Text;
                connectionBuilder.PersistSecurityInfo = true;
                return connectionBuilder;
            }
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            string result;
            TestSqlConnection(out result);
            MessageBox.Show(result, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public bool TestSqlConnection(out string error)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionBuilder.ConnectionString))
                {
                    connection.Open();
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }

            error = "OK";
            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string error;
            if (!TestSqlConnection(out error))
            {
                MessageBox.Show(error, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
