﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ExportContractsStatus : MrivcExportCommand
    {
        bool _isRSO;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                base.SenderID = value;
            }
        }

        public ExportContractsStatus(string senderID) : base("exportStatusCACh", "HomeManagementAsync", senderID, "exportStatusCAChResult")
        {
            Description = "Экспорт статусов договоров управления";
        }

        new void ExecuteToTempDB()
        {
            var dt = DAccess.DataModule.GetDataTableByQueryCommand1("select distinct ContractGuid from gis.Contract where SenderID = @senderID", new[,] { { "senderID", SenderID } });

            foreach (DataRow row in dt.Rows)
            {
                this.AddChildElement(MrivcStatic.GisConfig.Services[Service].Methods["exportStatusCACh_add"].Template, new[] { new Tuple<string[], string>(new[] { "Criteria", "ContractGUID" }, row[0].ToString()) }, "exportStatusCAChRequest");
            }

            base.ExecuteToTempDB(_exportNodeName);
        }

        public new void Execute()
        {
            this.ExecuteToTempDB();
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportContractStatusData", new[,] { { "SenderID", SenderID } });
        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            //return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.ExportHouseData", new object[,] { { "houseGUID", FiasHouseGuid } }, "Houses", "Entrance", "Premises");
            return new DataSet();
        }
    }
}
