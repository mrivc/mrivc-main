﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ExportDataProvider : MrivcExportCommand
    {
        public ExportDataProvider(string senderID) : base("exportDataProviderNsiItem", "NsiAsync", senderID, "NsiItem")
        {}

        public new void ExecuteToDataBase(string[] nsiRegistryNumbers)
        {
            var exportDataProviderItem = new ExportDataProviderItem(SenderID);
            exportDataProviderItem.AddToReport = this.AddToReport;
            exportDataProviderItem.Description = this.Description;

            foreach (var regNumber in nsiRegistryNumbers)
            {
                try
                {
                    exportDataProviderItem.ExecuteToTempDB(regNumber);
                    var sql = "GIS.ExportDataProviderNsiItems";
                    DAccess.DataModule.ExecuteNonQueryStoredProcedure1(sql, new object[,] { { "@senderID", SenderID }, { "@registryNumber", regNumber } });
                }
                catch (InvalidOperationException)
                {}
            }

            this.HasErrors = exportDataProviderItem.HasErrors;
        }
    }
}
