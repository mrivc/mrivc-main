﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MrivcControls
{
    public class PeriodPicker : SingleValueDataPicker
    {
        public void Fill(string connectionString)
        {
            DAccess.DataModule.Connection1 = new SqlConnection(connectionString);
            DataSource = DAccess.DataModule.GetDataTableByQueryCommand1("select period,period_name from dbo.vperiod order by 1 desc");
        }

        public PeriodPicker() 
        {
            ParameterName = "Период";
            ValueMember = "period";
            DisplayMember = "period_name";
        }

        public int Period
        {
            get {
                return int.Parse(SelectedValue.ToString());
            }
        }
    }
}
