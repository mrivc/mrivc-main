﻿namespace Payments
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripConnectionInfo = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripConnectionInfoDBPaymentItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripConnectionInfoDBEllisItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgramVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripUserInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripTerminalState = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripTerminalSubStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripOfdDocs = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miFileTerminalConnect = new System.Windows.Forms.ToolStripMenuItem();
            this.miFileTerminalTableSettingsReset = new System.Windows.Forms.ToolStripMenuItem();
            this.miFileTerminalUserUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.miFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminal = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalOpenSession = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miTerminalReportWithCleaning = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalPrintReportWithoutCleaning = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miTerminalOpenDrawer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.miTerminalCashOutcome = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.miTerminalCashIncome = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.miTerminalCorrectionReceipt = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalCorrectionReceiptSale = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalCorrectionReceiptBuy = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalCorrectionReceiptReturnSale = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalCorrectionReceiptReturnBuy = new System.Windows.Forms.ToolStripMenuItem();
            this.miService = new System.Windows.Forms.ToolStripMenuItem();
            this.miServiceTerminalStatusRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.miServiceTerminalEllisReestr = new System.Windows.Forms.ToolStripMenuItem();
            this.miServiceTerminalTxtReestr = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemReestrPorverka = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.miServiceTerminalLoadRegistryVerification = new System.Windows.Forms.ToolStripMenuItem();
            this.miServiceTerminalLoadRegistryVerificationSingle = new System.Windows.Forms.ToolStripMenuItem();
            this.miServiceTerminalLoadRegistryVerificationMass = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.miServiceTerminalContinuePrint = new System.Windows.Forms.ToolStripMenuItem();
            this.miServiceTerminalRepeatDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.miCancelCheck = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.miServiceTerminalSetDateAndTime = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.miOfdStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalOperationRegister = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalOperationRegister_244 = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalOperationRegister_241 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miTerminalOperationRegister_242 = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalOperationRegister_243 = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalOperationRegister_245 = new System.Windows.Forms.ToolStripMenuItem();
            this.miTerminalOperationRegister_247 = new System.Windows.Forms.ToolStripMenuItem();
            this.miCalc = new System.Windows.Forms.ToolStripMenuItem();
            this.miDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.miDocumentControlFormPay = new System.Windows.Forms.ToolStripMenuItem();
            this.miDocumentControlFormPayFreeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.miHelpTerminal = new System.Windows.Forms.ToolStripMenuItem();
            this.miHelpUserGuide = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.gridPayments1 = new Payments.Controls.GridPayments();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripConnectionInfo,
            this.toolStripStatusLabel1,
            this.toolStripProgramVersion,
            this.toolStripStatusLabel4,
            this.toolStripUserInfo,
            this.toolStripStatusLabel2,
            this.toolStripTerminalState,
            this.toolStripStatusLabel3,
            this.toolStripTerminalSubStatus,
            this.toolStripStatusLabel5,
            this.toolStripOfdDocs});
            this.statusStrip1.Location = new System.Drawing.Point(0, 638);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(957, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripConnectionInfo
            // 
            this.toolStripConnectionInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripConnectionInfo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripConnectionInfoDBPaymentItem,
            this.toolStripConnectionInfoDBEllisItem});
            this.toolStripConnectionInfo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripConnectionInfo.Image")));
            this.toolStripConnectionInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripConnectionInfo.Name = "toolStripConnectionInfo";
            this.toolStripConnectionInfo.Size = new System.Drawing.Size(29, 20);
            this.toolStripConnectionInfo.Text = "Настройки соединения с БД";
            this.toolStripConnectionInfo.DropDownOpening += new System.EventHandler(this.toolStripConnectionInfo_DropDownOpening);
            // 
            // toolStripConnectionInfoDBPaymentItem
            // 
            this.toolStripConnectionInfoDBPaymentItem.Name = "toolStripConnectionInfoDBPaymentItem";
            this.toolStripConnectionInfoDBPaymentItem.Size = new System.Drawing.Size(405, 22);
            this.toolStripConnectionInfoDBPaymentItem.Text = "Соединение с БД платежей: <SERVER_NAME> - <DB_NAME>";
            // 
            // toolStripConnectionInfoDBEllisItem
            // 
            this.toolStripConnectionInfoDBEllisItem.Name = "toolStripConnectionInfoDBEllisItem";
            this.toolStripConnectionInfoDBEllisItem.Size = new System.Drawing.Size(405, 22);
            this.toolStripConnectionInfoDBEllisItem.Text = "Соединение с БД ЭЛЛИС: <SERVER_NAME> - <DB_NAME>";
            this.toolStripConnectionInfoDBEllisItem.Click += new System.EventHandler(this.toolStripConnectionInfoDBEllisItem_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel1.Text = "|";
            // 
            // toolStripProgramVersion
            // 
            this.toolStripProgramVersion.Name = "toolStripProgramVersion";
            this.toolStripProgramVersion.Size = new System.Drawing.Size(106, 17);
            this.toolStripProgramVersion.Text = "Версия: <version>";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel4.Text = "|";
            // 
            // toolStripUserInfo
            // 
            this.toolStripUserInfo.Name = "toolStripUserInfo";
            this.toolStripUserInfo.Size = new System.Drawing.Size(90, 17);
            this.toolStripUserInfo.Text = "Пользователь: ";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // toolStripTerminalState
            // 
            this.toolStripTerminalState.Name = "toolStripTerminalState";
            this.toolStripTerminalState.Size = new System.Drawing.Size(85, 17);
            this.toolStripTerminalState.Text = "Статус кассы: ";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel3.Text = "|";
            // 
            // toolStripTerminalSubStatus
            // 
            this.toolStripTerminalSubStatus.Name = "toolStripTerminalSubStatus";
            this.toolStripTerminalSubStatus.Size = new System.Drawing.Size(84, 17);
            this.toolStripTerminalSubStatus.Text = "Режим кассы:";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel5.Text = "|";
            // 
            // toolStripOfdDocs
            // 
            this.toolStripOfdDocs.Name = "toolStripOfdDocs";
            this.toolStripOfdDocs.Size = new System.Drawing.Size(128, 17);
            this.toolStripOfdDocs.Text = "Документов для ОФД:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miTerminal,
            this.miService,
            this.miTerminalOperationRegister,
            this.miCalc,
            this.miDocument,
            this.miHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(957, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFileTerminalConnect,
            this.miFileTerminalTableSettingsReset,
            this.miFileTerminalUserUpdate,
            this.miFileExit});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(48, 20);
            this.miFile.Text = "Файл";
            // 
            // miFileTerminalConnect
            // 
            this.miFileTerminalConnect.Image = global::Payments.Properties.Resources.terminal_connect;
            this.miFileTerminalConnect.Name = "miFileTerminalConnect";
            this.miFileTerminalConnect.Size = new System.Drawing.Size(249, 22);
            this.miFileTerminalConnect.Text = "Настройки подключения кассы";
            this.miFileTerminalConnect.Click += new System.EventHandler(this.miFileTerminalConnect_Click);
            // 
            // miFileTerminalTableSettingsReset
            // 
            this.miFileTerminalTableSettingsReset.Name = "miFileTerminalTableSettingsReset";
            this.miFileTerminalTableSettingsReset.Size = new System.Drawing.Size(249, 22);
            this.miFileTerminalTableSettingsReset.Text = "Сбросить настройки кассы";
            this.miFileTerminalTableSettingsReset.Click += new System.EventHandler(this.miFileTerminalTableSettingsReset_Click);
            // 
            // miFileTerminalUserUpdate
            // 
            this.miFileTerminalUserUpdate.Name = "miFileTerminalUserUpdate";
            this.miFileTerminalUserUpdate.Size = new System.Drawing.Size(249, 22);
            this.miFileTerminalUserUpdate.Text = "Обновить пользователя кассы";
            this.miFileTerminalUserUpdate.Click += new System.EventHandler(this.miFileTerminalUserUpdate_Click);
            // 
            // miFileExit
            // 
            this.miFileExit.Name = "miFileExit";
            this.miFileExit.Size = new System.Drawing.Size(249, 22);
            this.miFileExit.Text = "Выход";
            this.miFileExit.Click += new System.EventHandler(this.miFileExit_Click);
            // 
            // miTerminal
            // 
            this.miTerminal.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miTerminalOpenSession,
            this.toolStripSeparator1,
            this.miTerminalReportWithCleaning,
            this.miTerminalPrintReportWithoutCleaning,
            this.toolStripSeparator2,
            this.miTerminalOpenDrawer,
            this.toolStripSeparator4,
            this.miTerminalCashOutcome,
            this.toolStripMenuItem1,
            this.toolStripSeparator9,
            this.miTerminalCashIncome,
            this.toolStripSeparator11,
            this.miTerminalCorrectionReceipt});
            this.miTerminal.Name = "miTerminal";
            this.miTerminal.Size = new System.Drawing.Size(50, 20);
            this.miTerminal.Text = "Касса";
            // 
            // miTerminalOpenSession
            // 
            this.miTerminalOpenSession.Image = global::Payments.Properties.Resources.open_session;
            this.miTerminalOpenSession.Name = "miTerminalOpenSession";
            this.miTerminalOpenSession.Size = new System.Drawing.Size(297, 22);
            this.miTerminalOpenSession.Text = "Открыть смену";
            this.miTerminalOpenSession.Click += new System.EventHandler(this.miTerminalOpenSession_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(294, 6);
            // 
            // miTerminalReportWithCleaning
            // 
            this.miTerminalReportWithCleaning.Image = global::Payments.Properties.Resources.chart_z;
            this.miTerminalReportWithCleaning.Name = "miTerminalReportWithCleaning";
            this.miTerminalReportWithCleaning.Size = new System.Drawing.Size(297, 22);
            this.miTerminalReportWithCleaning.Text = "Суточный отчет с гашением (Z - отчет)";
            this.miTerminalReportWithCleaning.Click += new System.EventHandler(this.miTerminalReportWithCleaning_Click);
            // 
            // miTerminalPrintReportWithoutCleaning
            // 
            this.miTerminalPrintReportWithoutCleaning.Image = global::Payments.Properties.Resources.chart_x;
            this.miTerminalPrintReportWithoutCleaning.Name = "miTerminalPrintReportWithoutCleaning";
            this.miTerminalPrintReportWithoutCleaning.Size = new System.Drawing.Size(297, 22);
            this.miTerminalPrintReportWithoutCleaning.Text = "Суточный отчет без гашения (X - отчет)";
            this.miTerminalPrintReportWithoutCleaning.Click += new System.EventHandler(this.miTerminalPrintReportWithoutCleaning_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(294, 6);
            // 
            // miTerminalOpenDrawer
            // 
            this.miTerminalOpenDrawer.Name = "miTerminalOpenDrawer";
            this.miTerminalOpenDrawer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D1)));
            this.miTerminalOpenDrawer.Size = new System.Drawing.Size(297, 22);
            this.miTerminalOpenDrawer.Text = "Открыть денежный ящик";
            this.miTerminalOpenDrawer.Click += new System.EventHandler(this.miTerminalOpenDrawer_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(294, 6);
            // 
            // miTerminalCashOutcome
            // 
            this.miTerminalCashOutcome.Image = global::Payments.Properties.Resources.outbox;
            this.miTerminalCashOutcome.Name = "miTerminalCashOutcome";
            this.miTerminalCashOutcome.Size = new System.Drawing.Size(297, 22);
            this.miTerminalCashOutcome.Text = "Выдать деньги из кассы (ИНКАССАЦИЯ)";
            this.miTerminalCashOutcome.Click += new System.EventHandler(this.miTerminalCashOutcome_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(297, 22);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(294, 6);
            // 
            // miTerminalCashIncome
            // 
            this.miTerminalCashIncome.Name = "miTerminalCashIncome";
            this.miTerminalCashIncome.Size = new System.Drawing.Size(297, 22);
            this.miTerminalCashIncome.Text = "Внести деньги в кассу";
            this.miTerminalCashIncome.Click += new System.EventHandler(this.miTerminalCashIncome_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(294, 6);
            // 
            // miTerminalCorrectionReceipt
            // 
            this.miTerminalCorrectionReceipt.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miTerminalCorrectionReceiptSale,
            this.miTerminalCorrectionReceiptBuy,
            this.miTerminalCorrectionReceiptReturnSale,
            this.miTerminalCorrectionReceiptReturnBuy});
            this.miTerminalCorrectionReceipt.Name = "miTerminalCorrectionReceipt";
            this.miTerminalCorrectionReceipt.Size = new System.Drawing.Size(297, 22);
            this.miTerminalCorrectionReceipt.Text = "Сформировать чек коррекции";
            // 
            // miTerminalCorrectionReceiptSale
            // 
            this.miTerminalCorrectionReceiptSale.Name = "miTerminalCorrectionReceiptSale";
            this.miTerminalCorrectionReceiptSale.Size = new System.Drawing.Size(169, 22);
            this.miTerminalCorrectionReceiptSale.Tag = "0";
            this.miTerminalCorrectionReceiptSale.Text = "Продажа";
            this.miTerminalCorrectionReceiptSale.ToolTipText = "Чек коррекции может оформляться в случае необходимости с\r\nкорректировать данные о" +
    " продажах, переданные в ФНС. \r\nЧек коррекции может оформляться по предписанию \r\n" +
    "инспектора налоговой службы.";
            this.miTerminalCorrectionReceiptSale.Click += new System.EventHandler(this.miTerminalCorrectionReceipt_Click);
            // 
            // miTerminalCorrectionReceiptBuy
            // 
            this.miTerminalCorrectionReceiptBuy.Name = "miTerminalCorrectionReceiptBuy";
            this.miTerminalCorrectionReceiptBuy.Size = new System.Drawing.Size(169, 22);
            this.miTerminalCorrectionReceiptBuy.Tag = "1";
            this.miTerminalCorrectionReceiptBuy.Text = "Покупка";
            this.miTerminalCorrectionReceiptBuy.ToolTipText = "Чек коррекции может оформляться в случае необходимости с";
            this.miTerminalCorrectionReceiptBuy.Click += new System.EventHandler(this.miTerminalCorrectionReceipt_Click);
            // 
            // miTerminalCorrectionReceiptReturnSale
            // 
            this.miTerminalCorrectionReceiptReturnSale.Name = "miTerminalCorrectionReceiptReturnSale";
            this.miTerminalCorrectionReceiptReturnSale.Size = new System.Drawing.Size(169, 22);
            this.miTerminalCorrectionReceiptReturnSale.Tag = "2";
            this.miTerminalCorrectionReceiptReturnSale.Text = "Возврат продажи";
            this.miTerminalCorrectionReceiptReturnSale.ToolTipText = "Чек коррекции может оформляться в случае необходимости с";
            this.miTerminalCorrectionReceiptReturnSale.Click += new System.EventHandler(this.miTerminalCorrectionReceipt_Click);
            // 
            // miTerminalCorrectionReceiptReturnBuy
            // 
            this.miTerminalCorrectionReceiptReturnBuy.Name = "miTerminalCorrectionReceiptReturnBuy";
            this.miTerminalCorrectionReceiptReturnBuy.Size = new System.Drawing.Size(169, 22);
            this.miTerminalCorrectionReceiptReturnBuy.Tag = "3";
            this.miTerminalCorrectionReceiptReturnBuy.Text = "Возврат покупки";
            this.miTerminalCorrectionReceiptReturnBuy.ToolTipText = "Чек коррекции может оформляться в случае необходимости с";
            this.miTerminalCorrectionReceiptReturnBuy.Click += new System.EventHandler(this.miTerminalCorrectionReceipt_Click);
            // 
            // miService
            // 
            this.miService.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miServiceTerminalStatusRefresh,
            this.toolStripSeparator7,
            this.miServiceTerminalEllisReestr,
            this.miServiceTerminalTxtReestr,
            this.menuItemReestrPorverka,
            this.toolStripSeparator8,
            this.miServiceTerminalLoadRegistryVerification,
            this.toolStripSeparator5,
            this.miServiceTerminalContinuePrint,
            this.miServiceTerminalRepeatDocument,
            this.miCancelCheck,
            this.toolStripSeparator6,
            this.miServiceTerminalSetDateAndTime,
            this.toolStripSeparator10,
            this.miOfdStatus});
            this.miService.Name = "miService";
            this.miService.Size = new System.Drawing.Size(59, 20);
            this.miService.Text = "Сервис";
            // 
            // miServiceTerminalStatusRefresh
            // 
            this.miServiceTerminalStatusRefresh.Image = global::Payments.Properties.Resources.refresh;
            this.miServiceTerminalStatusRefresh.Name = "miServiceTerminalStatusRefresh";
            this.miServiceTerminalStatusRefresh.Size = new System.Drawing.Size(377, 22);
            this.miServiceTerminalStatusRefresh.Text = "Обновить состояние кассы";
            this.miServiceTerminalStatusRefresh.Click += new System.EventHandler(this.miServiceTerminalStatusRefresh_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(374, 6);
            // 
            // miServiceTerminalEllisReestr
            // 
            this.miServiceTerminalEllisReestr.Image = global::Payments.Properties.Resources.zip_16;
            this.miServiceTerminalEllisReestr.Name = "miServiceTerminalEllisReestr";
            this.miServiceTerminalEllisReestr.Size = new System.Drawing.Size(377, 22);
            this.miServiceTerminalEllisReestr.Text = "Выгрузить реестр платежей";
            this.miServiceTerminalEllisReestr.Click += new System.EventHandler(this.miServiceTerminalEllisReestr_Click);
            // 
            // miServiceTerminalTxtReestr
            // 
            this.miServiceTerminalTxtReestr.Image = global::Payments.Properties.Resources.zip_16;
            this.miServiceTerminalTxtReestr.Name = "miServiceTerminalTxtReestr";
            this.miServiceTerminalTxtReestr.Size = new System.Drawing.Size(377, 22);
            this.miServiceTerminalTxtReestr.Text = "Выгрузить реестр платежей по Капитальному ремонту";
            this.miServiceTerminalTxtReestr.Click += new System.EventHandler(this.miServiceTerminalTxtReestr_Click);
            // 
            // menuItemReestrPorverka
            // 
            this.menuItemReestrPorverka.Name = "menuItemReestrPorverka";
            this.menuItemReestrPorverka.Size = new System.Drawing.Size(377, 22);
            this.menuItemReestrPorverka.Text = "Выгрузить реестр патежей по услугам";
            this.menuItemReestrPorverka.Click += new System.EventHandler(this.menuItemReestrPorverka_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(374, 6);
            // 
            // miServiceTerminalLoadRegistryVerification
            // 
            this.miServiceTerminalLoadRegistryVerification.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miServiceTerminalLoadRegistryVerificationSingle,
            this.miServiceTerminalLoadRegistryVerificationMass});
            this.miServiceTerminalLoadRegistryVerification.Name = "miServiceTerminalLoadRegistryVerification";
            this.miServiceTerminalLoadRegistryVerification.Size = new System.Drawing.Size(377, 22);
            this.miServiceTerminalLoadRegistryVerification.Text = "Загрузить реестр сальдо";
            // 
            // miServiceTerminalLoadRegistryVerificationSingle
            // 
            this.miServiceTerminalLoadRegistryVerificationSingle.Name = "miServiceTerminalLoadRegistryVerificationSingle";
            this.miServiceTerminalLoadRegistryVerificationSingle.Size = new System.Drawing.Size(229, 22);
            this.miServiceTerminalLoadRegistryVerificationSingle.Text = "Загрузить один реестр";
            this.miServiceTerminalLoadRegistryVerificationSingle.Click += new System.EventHandler(this.miServiceTerminalLoadRegistryVerificationSingle_Click);
            // 
            // miServiceTerminalLoadRegistryVerificationMass
            // 
            this.miServiceTerminalLoadRegistryVerificationMass.Name = "miServiceTerminalLoadRegistryVerificationMass";
            this.miServiceTerminalLoadRegistryVerificationMass.Size = new System.Drawing.Size(229, 22);
            this.miServiceTerminalLoadRegistryVerificationMass.Text = "Загрузить массово из папки";
            this.miServiceTerminalLoadRegistryVerificationMass.Click += new System.EventHandler(this.miServiceTerminalLoadRegistryVerificationMass_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(374, 6);
            // 
            // miServiceTerminalContinuePrint
            // 
            this.miServiceTerminalContinuePrint.Name = "miServiceTerminalContinuePrint";
            this.miServiceTerminalContinuePrint.Size = new System.Drawing.Size(377, 22);
            this.miServiceTerminalContinuePrint.Text = "Продожить печать";
            this.miServiceTerminalContinuePrint.Click += new System.EventHandler(this.miServiceTerminalContinuePrint_Click);
            // 
            // miServiceTerminalRepeatDocument
            // 
            this.miServiceTerminalRepeatDocument.Image = global::Payments.Properties.Resources.copy_check;
            this.miServiceTerminalRepeatDocument.Name = "miServiceTerminalRepeatDocument";
            this.miServiceTerminalRepeatDocument.Size = new System.Drawing.Size(377, 22);
            this.miServiceTerminalRepeatDocument.Text = "Печать дубликата последнего чека";
            this.miServiceTerminalRepeatDocument.Click += new System.EventHandler(this.miServiceTerminalRepeatDocument_Click);
            // 
            // miCancelCheck
            // 
            this.miCancelCheck.Name = "miCancelCheck";
            this.miCancelCheck.Size = new System.Drawing.Size(377, 22);
            this.miCancelCheck.Text = "Отменить текущую продажу (аннулировать чек)";
            this.miCancelCheck.Click += new System.EventHandler(this.miCancelCheck_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(374, 6);
            // 
            // miServiceTerminalSetDateAndTime
            // 
            this.miServiceTerminalSetDateAndTime.Name = "miServiceTerminalSetDateAndTime";
            this.miServiceTerminalSetDateAndTime.Size = new System.Drawing.Size(377, 22);
            this.miServiceTerminalSetDateAndTime.Text = "Синхронизировать дату и время кассы";
            this.miServiceTerminalSetDateAndTime.Click += new System.EventHandler(this.miServiceTerminalSetDateAndTime_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(374, 6);
            // 
            // miOfdStatus
            // 
            this.miOfdStatus.Name = "miOfdStatus";
            this.miOfdStatus.Size = new System.Drawing.Size(377, 22);
            this.miOfdStatus.Text = "ОФД";
            this.miOfdStatus.Click += new System.EventHandler(this.miOfdStatus_Click);
            // 
            // miTerminalOperationRegister
            // 
            this.miTerminalOperationRegister.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miTerminalOperationRegister_244,
            this.miTerminalOperationRegister_241,
            this.toolStripSeparator3,
            this.miTerminalOperationRegister_242,
            this.miTerminalOperationRegister_243,
            this.miTerminalOperationRegister_245,
            this.miTerminalOperationRegister_247});
            this.miTerminalOperationRegister.Name = "miTerminalOperationRegister";
            this.miTerminalOperationRegister.Size = new System.Drawing.Size(130, 20);
            this.miTerminalOperationRegister.Text = "Статистика по кассе";
            this.miTerminalOperationRegister.DropDownOpening += new System.EventHandler(this.miTerminalOperationRegister_DropDownOpening);
            // 
            // miTerminalOperationRegister_244
            // 
            this.miTerminalOperationRegister_244.Name = "miTerminalOperationRegister_244";
            this.miTerminalOperationRegister_244.Size = new System.Drawing.Size(378, 22);
            this.miTerminalOperationRegister_244.Text = "Необнуляемая сумма продаж на нач. смены: <summ>";
            // 
            // miTerminalOperationRegister_241
            // 
            this.miTerminalOperationRegister_241.Name = "miTerminalOperationRegister_241";
            this.miTerminalOperationRegister_241.Size = new System.Drawing.Size(378, 22);
            this.miTerminalOperationRegister_241.Text = "Накопление наличности в кассе: <summ>";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(375, 6);
            // 
            // miTerminalOperationRegister_242
            // 
            this.miTerminalOperationRegister_242.Name = "miTerminalOperationRegister_242";
            this.miTerminalOperationRegister_242.Size = new System.Drawing.Size(378, 22);
            this.miTerminalOperationRegister_242.Text = "Внесенные суммы за смену: <summ>";
            // 
            // miTerminalOperationRegister_243
            // 
            this.miTerminalOperationRegister_243.Name = "miTerminalOperationRegister_243";
            this.miTerminalOperationRegister_243.Size = new System.Drawing.Size(378, 22);
            this.miTerminalOperationRegister_243.Text = "Выплаченные суммы за смену (инкассация): <summ>";
            // 
            // miTerminalOperationRegister_245
            // 
            this.miTerminalOperationRegister_245.Name = "miTerminalOperationRegister_245";
            this.miTerminalOperationRegister_245.Size = new System.Drawing.Size(378, 22);
            this.miTerminalOperationRegister_245.Text = "Cумма продаж в смене: <summ>";
            // 
            // miTerminalOperationRegister_247
            // 
            this.miTerminalOperationRegister_247.Name = "miTerminalOperationRegister_247";
            this.miTerminalOperationRegister_247.Size = new System.Drawing.Size(378, 22);
            this.miTerminalOperationRegister_247.Text = "Сумма возвратов продаж в смене: <summ>";
            // 
            // miCalc
            // 
            this.miCalc.Name = "miCalc";
            this.miCalc.Size = new System.Drawing.Size(89, 20);
            this.miCalc.Text = "Калькулятор";
            this.miCalc.Click += new System.EventHandler(this.miCalc_Click);
            // 
            // miDocument
            // 
            this.miDocument.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miDocumentControlFormPay,
            this.miDocumentControlFormPayFreeToolStripMenuItem});
            this.miDocument.Name = "miDocument";
            this.miDocument.Size = new System.Drawing.Size(82, 20);
            this.miDocument.Text = "Документы";
            // 
            // miDocumentControlFormPay
            // 
            this.miDocumentControlFormPay.Name = "miDocumentControlFormPay";
            this.miDocumentControlFormPay.Size = new System.Drawing.Size(263, 22);
            this.miDocumentControlFormPay.Text = "Формы контроля платежа";
            this.miDocumentControlFormPay.Click += new System.EventHandler(this.miDocumentControlFormPay_Click);
            // 
            // miDocumentControlFormPayFreeToolStripMenuItem
            // 
            this.miDocumentControlFormPayFreeToolStripMenuItem.Name = "miDocumentControlFormPayFreeToolStripMenuItem";
            this.miDocumentControlFormPayFreeToolStripMenuItem.Size = new System.Drawing.Size(263, 22);
            this.miDocumentControlFormPayFreeToolStripMenuItem.Text = "Формы контроля платежа (Бланк)";
            this.miDocumentControlFormPayFreeToolStripMenuItem.Click += new System.EventHandler(this.miDocumentControlFormPayFreeToolStripMenuItem_Click);
            // 
            // miHelp
            // 
            this.miHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miHelpTerminal,
            this.miHelpUserGuide});
            this.miHelp.Name = "miHelp";
            this.miHelp.Size = new System.Drawing.Size(68, 20);
            this.miHelp.Text = "Помощь";
            // 
            // miHelpTerminal
            // 
            this.miHelpTerminal.Name = "miHelpTerminal";
            this.miHelpTerminal.Size = new System.Drawing.Size(281, 22);
            this.miHelpTerminal.Text = "Инструкция по эксплуатации Кассы";
            this.miHelpTerminal.Click += new System.EventHandler(this.miHelpTerminal_Click);
            // 
            // miHelpUserGuide
            // 
            this.miHelpUserGuide.Name = "miHelpUserGuide";
            this.miHelpUserGuide.Size = new System.Drawing.Size(281, 22);
            this.miHelpUserGuide.Text = "Инструкция по работе с программой";
            this.miHelpUserGuide.Click += new System.EventHandler(this.miHelpUserGuide_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // gridPayments1
            // 
            this.gridPayments1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPayments1.Location = new System.Drawing.Point(0, 24);
            this.gridPayments1.Name = "gridPayments1";
            this.gridPayments1.Size = new System.Drawing.Size(957, 614);
            this.gridPayments1.TabIndex = 3;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 660);
            this.Controls.Add(this.gridPayments1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Касса";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Shown += new System.EventHandler(this.FrmMain_Shown);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripConnectionInfo;
        private System.Windows.Forms.ToolStripMenuItem toolStripConnectionInfoDBPaymentItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripConnectionInfoDBEllisItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripUserInfo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miFileExit;
        private System.Windows.Forms.ToolStripMenuItem miTerminal;
        private System.Windows.Forms.ToolStripStatusLabel toolStripTerminalState;
        private System.Windows.Forms.ToolStripMenuItem miService;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalStatusRefresh;
        private System.Windows.Forms.ToolStripMenuItem miTerminalReportWithCleaning;
        private System.Windows.Forms.ToolStripMenuItem miTerminalPrintReportWithoutCleaning;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOpenSession;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOpenDrawer;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalSetDateAndTime;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalContinuePrint;
        private System.Windows.Forms.ToolStripStatusLabel toolStripTerminalSubStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOperationRegister;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOperationRegister_241;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOperationRegister_242;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOperationRegister_243;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOperationRegister_244;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOperationRegister_245;
        private System.Windows.Forms.ToolStripMenuItem miTerminalOperationRegister_247;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem miTerminalCashIncome;
        private System.Windows.Forms.ToolStripMenuItem miTerminalCashOutcome;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalRepeatDocument;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripStatusLabel toolStripProgramVersion;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripMenuItem miFileTerminalConnect;
        private System.Windows.Forms.ToolStripMenuItem miFileTerminalTableSettingsReset;
        private Controls.GridPayments gridPayments1;
        private System.Windows.Forms.ToolStripMenuItem miHelp;
        private System.Windows.Forms.ToolStripMenuItem miHelpTerminal;
        private System.Windows.Forms.ToolStripMenuItem miCalc;
        private System.Windows.Forms.ToolStripMenuItem miHelpUserGuide;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalEllisReestr;
        private System.Windows.Forms.ToolStripMenuItem miFileTerminalUserUpdate;
        private System.Windows.Forms.ToolStripMenuItem miDocument;
        private System.Windows.Forms.ToolStripMenuItem miDocumentControlFormPay;
        private System.Windows.Forms.ToolStripMenuItem miDocumentControlFormPayFreeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalLoadRegistryVerification;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalTxtReestr;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalLoadRegistryVerificationSingle;
        private System.Windows.Forms.ToolStripMenuItem miServiceTerminalLoadRegistryVerificationMass;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem miOfdStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripOfdDocs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem miTerminalCorrectionReceipt;
        private System.Windows.Forms.ToolStripMenuItem miTerminalCorrectionReceiptSale;
        private System.Windows.Forms.ToolStripMenuItem miTerminalCorrectionReceiptBuy;
        private System.Windows.Forms.ToolStripMenuItem miTerminalCorrectionReceiptReturnSale;
        private System.Windows.Forms.ToolStripMenuItem miTerminalCorrectionReceiptReturnBuy;
        private System.Windows.Forms.ToolStripMenuItem miCancelCheck;
        private System.Windows.Forms.ToolStripMenuItem menuItemReestrPorverka;
    }
}