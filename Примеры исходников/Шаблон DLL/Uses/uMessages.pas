Unit uMessages;
//������ �� ����� �����������, ������� ����� ���������
Interface

Uses SysUtils, Messages;

Const
  WM_Refresh_Data = WM_User + 1000;
  WM_Execute = WM_User + 1101;
  WM_User_Local = WM_User + 10000;

  MDIChildForm_Message_Offset = 1000;
  Address_Frame_Message_Offset = 2000;
  //��������� �� ��������� ������
  WM_Address_Frame_Item_Change = WM_User + Address_Frame_Message_Offset + 1;
  Address_Frame_Account_Changed = 0;
  Address_Frame_Flat_Changed = 1;
  Address_Frame_House_Changed = 2;
  Address_Frame_Street_Changed = 3;
  Address_Frame_Settlement_Changed = 4;

  //��������� �� ������������ ����� ���� ���� ���� MDIChild
  WM_MDIChildForm_Created = WM_User + MDIChildForm_Message_Offset + 1;
  WM_MDIChildForm_Destroyed = WM_User + MDIChildForm_Message_Offset + 2;
  WM_MDIChildForm_Update_Caption = WM_User + MDIChildForm_Message_Offset + 3;
  WM_MDIChildForm_Update_TabIndex = WM_User + MDIChildForm_Message_Offset + 4;
  WM_MDIChildForm_Activated = WM_User + MDIChildForm_Message_Offset + 5;
  WM_MDIChildForm_LocateAccount = WM_User + MDIChildForm_Message_Offset + 6;
  WM_MDIChildForm_EditPersonal = WM_User + MDIChildForm_Message_Offset + 7;
  WM_MDIChildForm_ParamsChange = WM_User + MDIChildForm_Message_Offset + 8;
  WM_MDIChildForm_Reconnect = WM_User + MDIChildForm_Message_Offset + 9;
  WM_MDIChildForm_RecreateModuleMenu = WM_User + MDIChildForm_Message_Offset + 10;

  // �������� ������� ��������� ����������
  WM_OpenDefferedCalcQueue = WM_User + MDIChildForm_Message_Offset + 11;
  // �������� ������� ������� ���������� ����������
  WM_OpenDefferedAccountCalcQueue = WM_User + MDIChildForm_Message_Offset + 12;

  // ��������� � ���������� ���������� ��������
  WM_MassOperationCompleted = WM_User + MDIChildForm_Message_Offset + 13;

Resourcestring
  //����� ������
  E_Filter_Empty_Address='�� ������� �� ������ ������!';
  E_Filter_Empty_Address_Table_Name='#Empty_Address';


  E_Empty_Parameter_List = '������ ���������� ����';
  E_Reason_Is_Too_Short = '������� �� ������� ��� ������� ������������ ��������';
  //  E_Recalculation_Empty_Reason = '������� ����������� �� ������� ��� ������� �� ���������� ��������';
  E_File_Not_Found = '���� �� ������';
  E_Closed_Period_Action = '������ �������� ���������� � �������� �������!';

  E_Incorrect_Document_Params = '����������� ��������� ��������� ���������(��).';
                 
  E_MIN_SQL_Date='����������� ��������� ���� 01.01.1753!';

  //������ ��� ����������� ������� �������
  E_Module_Caption = '������ ����������� ������';
  E_Module_Cant_Load_Library = '���������� ��������� ����������';
  E_Module_Cant_Find_Procedure = '���������� ����� ���������';
  E_Module_Invalid_Name = '������������ ��� ������';
  E_Module_Loading_By_Another_Process = '������ ����������� ������ ��������. ���������� ��������� ������ �����.';

  //
  E_Registration_add_Exists_Error = '��������� ��� ���� � �����������!';
  E_Registration_Edit_Only_Current_Registration=
    '����� ������ ������ ��������� ����������� ����������!';
  E_Residence_Add_Exists_Error = '��������� ��� ���� � �����������!';
  E_RESTRICT_REGISTRATION_CHANGE_FROM_ACCOUNT=
    '��������� ����������� ��������� ������ �� ��������� ����������� �����!';
  //������ ��� ������������
  E_Recalculation_Caption = '������ ��� ���������� �����������';
  E_Recalculation_Invalid_Parameters = '�������� ������� ���������';
  E_Recalculation_Invalid_Date_Diapason = '������������ �������� ���';
  E_Recalculation_Invalid_Date_Period ='������ �������������� ������!'; 
  E_Recalculation_Invalid_Sum_Diapason = '������������ �������� ����';

  //������ ��� ������ �������
  E_Filter_Caption = '������';
  E_Filter_Unknown_Logic_Connection = '����������� ��� ����������� ����������';
  E_Filter_IsEmpty = '������ ������ ������ �������!';

  //������ ��� ������ � ��������������
  E_Employee_Delete_Error =
    '������� ������������ ������ �������, ��� ��� � ���� ���� ������� �� ���������!';

  //������ ��� ��������� ������
  E_Formula_Caption = '������ ��� ������������ ������� ����������';
  E_Formula_Unknown_Error = '������ �������������� ����';
  E_Formula_Unexpected_End = '����������� ���������� ����� End';
  E_Formula_Unexpected_Then = '����������� ���������� ����� THEN';
  E_Formula_Unexpected_When = '����������� ���������� ����� WHEN';
  E_Formula_Cant_Find_End = '�� ������� ����� END';
  E_Formula_When_Expected = '��������� ����� WHEN';
  E_Formula_Then_Expected = '��������� ����� THEN';
  E_Formula_Condition_Expected = '��������� �������';
  E_Formula_Result_Expected = '��������� ���������';

  E_LowQuality_Setup_Need='��� ������������� ����������� �� ������� �������� ������'^M+
  '���������� �������� ��������� � ������ ���� '^M+
  '�����������������-> ��������� ������������ �� ������� �������� .'; 

  //������ ��� ������ � ����������
  E_Meter_Caption = '������ ���������� ����������';
  E_Meter_Invalid_Digit_Capacity = '�������� ����������� �������� �� ����� ��������� 9!';
  E_Meter_Invalid_Change_Capacity = '�������� ����������� �������� �� ����� ���� ' +
    '������ ���� ����� ���� � ���� ������ ������������ ��������� �� ������� ��������!';
  E_Meter_Invalid_Remove_Date =
    '���� ��������� ������� ������ ���� ������ ���� ��������� � ������ ��� ����� ���� ���������� ��������� !';
  E_Meter_Invalid_First_Measure_Date =
    '���� ������� ��������� ������ ���� ����� ������ ��������� � ������� �������� ������������!';
  E_Meter_Invalid_Measure_Date =
    '���� ��������� ������ ���� ������ ��� ���� ��������� �������� � ������ ���� ����� ���� �������';

  //������ ��� ��������� ���������
  E_Closed_Period = '����� ������ ��� ��������������';
  E_Parameter_Read_Only = '���� �������� ��������� �������������';
  E_Cant_Change_Parameter = '����� �� �� ������ �������� ��������';
  E_Cant_Change_House_Parameter = '�������� ���� ����� ��������'^M'������ �� ������ ���� "������ �������"!';
  E_Cant_Change_Flat_Group_Parameter = '����� �� �� ������ �������� �������� ������ �������';
  //������ ����� ��������� ����������
  E_Connection_Option_Caption = '������ ����� ����������';
  E_Connection_Option_File_Missing = '�� ������ ���������� ���� � ����������� ����������';
  E_Connection_Option_Host_Missing = '� ���������� ���������� ������� ��������� ���������� � �������';
  E_Connection_Option_Database_Missing = '� ���������� ���������� ������� ��������� ���������� � ���� ������';

  E_Incorrect_Date = '���� ������ �� ����� ���� ������ ���� ���������';

  E_RequiredFieldNotSet = '�� ��������� ���� "%s".'^M'��� ���� �� ������ "*" ������ ���� ���������.';

  E_BenefitOwnerNotFound = '�������� ������ �� ������';
  E_BenefitOwnerAlreadyExist = '�������� ������ ��� ����������';
  E_BenefitDocumentAlreadyExist = '����� �������� ��� ����������';
  E_RESTRICT_MULTIPLE_DOCUMENTS_BY_BENEFIT_CATEGORY =
    '��������� ������� ����� ������ ��������� �� ���� ��������� �����!';
  E_BENEFIT_DOCS_DATES_1_31 = '��������� �� ������ ����� ������� 1-� ������ ,' +
    ' � ��������� ��������� ������ ������!';

  //������ ��������� ������
  E_Grid_Customize_Caption = '���������� ��������� ��������� ������� �� ���� ������.';
  E_Grid_Customize_Invalide_ParameterList =
    '������ ���������� �� ������������� ���������� �������. �������� ������ ����������?';
  E_Grid_Customize_Invalide_Type = '�������: �������������� �����';

  // ���� ��������
  E_Prev_Period_Bill_Denied = '���� �������� � ���������� ������ ��������!';
  E_Fine_Greather_Then_Sum = 'C���� ���� �� ����� ���� ������ ����� �������!';

  // �����-����
  E_Barcode_Invalid_Name='������������ �����-���� ������ ���� �� ������!';
  E_Barcode_Max_Length='��������� ����� �����-���� �� ����� ��������� %d ��������!';

  //������� �� ����� �����
  I_Period_New = '�������� �������� ������ �������� ������� ������� ���������. ' +
    '������������� ��������� � �� �������� ��������� �������� ���� ����������.';

  //��������� �������
  I_Formula_Correct = '������� ������ ������������';
  I_Formula_Updated = '������� ������� ���������.'^M +
    '�� �������� ��������� ��������� ��������� ���� ���������� (������ - ���������� ����������).';
  I_ReCreate_Procedures_Need=
    '�� �������� ��������� ������������ �������� �������'^M +
    '(����������������� - ��������� - ��������� ������������ �������� �������)'^M+
    '� ����������� ��� ���������� (������ - ���������� ����������).';

  I_Formula_Saved_No_Recreate = '������� ������� ���������.'^M  +
    '�� �������� ��������� ������������ �������� �������'^M +
    '(����������������� - ��������� - ��������� ������������ �������� �������)'^M+
    '� ����������� ��� ���������� (������ - ���������� ����������).';
  //������������ ��������
  I_Procedures_Recreated = '������������ �������� �������� ������� ������� ���������.'^M +
  '�� �������� ��������� �������� ���� ���������� (������ - ���������� ����������).';
  I_Procedures_Recreated_Error = '��� ������������ �������� �������� ������� ��������� ������!';

  //����������/��������� ������������
  I_Owner_Add_Success = '������������ ������� ��������� � ����!';
  I_Account_With_Owners_Add_Success = '����� ������� ���� � �������������� ������� ��������!';
  I_Account_Add_Success='������� ���� ������� �������� � ����!';

  // ����������
  EAgreement_Signed='���������� ��� ���������!';
  E_Account_Status_Change_Error=
    '������ �������� ����� ����� ������ ������ � ������� ��� ������� ������� !';


  //����� �������� ���������
  I_Splash_Connecting_Start_User = '������������ ����������� � ������� ��';
  I_Splash_Loading_Login_List = '�������� ������ �������������';
  I_Splash_Login_List_Loaded = '������ ������������� ��������';
  I_Splash_Login_Success = '������������ ������� ������ ������������';
  I_Splash_Login_Failed = '������������ �� ������ ��������������.';
  I_Splash_Error_SubSystem = '������ ��� ������������� ���������� : ';
  I_Splash_Connecting_Main = '��������� ��������� ����������';
  I_Splash_COM_Servers = '�������� COM ��������';
  I_Splash_Load_Pictures = '������������� ������� ��������';
  I_Splash_Loading_External_Modules = '�������� ������� �������';

  //��������� ��� ����������� �����
  H_PersonData_Change = '������� ������� ��������� ������ ������ ����������';
  H_Document_Delete = '������� ������� �������� ���������';

  //��������� ��� ����� ��������� ��������� PCH - Parameter Change Header
  PCH_Person_UnRegister = '�������� �������� �����������';
  PCH_Residence_End_Date = '�������� ������� ����������';

  PCH_Document_End_Date = '���� ��������� �������� ���������';
  PCH_Benefit_User_End_Date = '��������� ����� ����������� �������';

  PCH_Module_File = '���� ��� ��������';

  PCH_Pack_Number = '����� �����';
  PCH_Pack_Date = '���� �����';
  PCH_Pack_Type = '��� �����';
  PCH_Pay_Date = '���� ����������� �����';

  PCH_Bill_Type = '��� ���������';
  PCH_Bill_Period = '������ ���������';
  PCH_Bill_Owner = '�������� ���������';
  PCH_Bill_Status = '������ ���������';
  PCH_Bill_Account = '������� ����';
  PCH_Bill_Date = '���� ���������';
  PCH_Bill_PayDate = '���� ������ ���������';

  PCH_Tariff_Name = '�������� ������';

  PCH_SubService_Name = '�������� ���������';

  PCH_Normative_Name = '�������� ���������';

  PCH_Meter_Digit_Capacity = '����������� ��������';
  PCH_Meter_Info = '���������� � ��������';

  PCH_Tariff_Display_Change = '��������� ������ ��� ������';
  PCH_Normative_Display_Change = '��������� ��������� ��� ������';
  PCH_Value_Display_Change = '��������� �������� ��� ������';
  PCH_CalcCond_Display_Change = '��������� ������� ������� ��� ������ ';
  PCH_ValUnit_Display_Change = '��������� ������� ��������� ��� ������ ';

  PCH_Tariff_Delete = '�������� ������';
  PCH_Normative_Delete = '�������� ���������';
  PCH_SubService_Delete = '�������� ���������';

  PCH_Settlement_Delete = '�������� ���������� ������';
  PCH_Street_Delete = '�������� �����';
  PCH_House_Delete = '�������� ����';
  PCH_Flat_Delete = '�������� ��������';

  PCH_Attention_Long_Time_Execution = '������ �������� ����� ������ ��������������� �����! �� �������?';

Type
  EMessage = Class(Exception)
  Protected
    FCaption: String;
    FExtraInfo: String;
  Public
    Property Caption: String Read FCaption Write FCaption;
    Property ExtraInfo: String Read FExtraInfo Write FExtraInfo;
    Constructor Create(Caption, Message: String; ExtraInfo: String = ''); Overload;
  End;
  
Implementation

{ EMessage }

Constructor EMessage.Create(Caption, Message: String; ExtraInfo: String = '');
Begin
  Inherited Create(Message);
  FCaption := Caption;
  FExtraInfo := ExtraInfo;
End;

End.
