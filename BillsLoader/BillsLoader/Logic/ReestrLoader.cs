﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using BillsLoader.DM;
using BillsLoader.Logic;
using System.Data;

namespace BillsLoader
{
    using System.Data.Common;
    using System.Windows.Forms;

    using MrivcHelpers.Helpers;

    public delegate void LoadedEventHandler(int totalReestr, int loadedCount, ReestrInfo reestrInfo, string loadedStatus);

    public class ReestrLoader
    {
        public static event LoadedEventHandler Loaded;

        public static string LastErrorFile { private set; get; } = "";

        private static bool ReadTxtReestrIntoTmpLoadTable(ReestrInfo r)
        {
            string sql = string.Empty;

            string[] lines;

            if (!r.LoadFromExcel)
            {
                if (!FileHelper.ExistFile(r.SourceFile.FullName))
                    return false;

                lines = File.ReadAllLines(r.SourceFile.FullName, Encoding.Default);
            }
            else
            {
                var reestrText = DAccess.DataModule.ExecuteScalarCommand1($"exec town.Create_Pay_Reestr '{r.TempTableName}',{r.BillFilter}").ToString();
                lines = reestrText.Split('\r');

                var errorText = DAccess.DataModule.ExecuteScalarCommand1($"exec town.Create_Pay_Reestr '{r.TempTableName}',{r.BillFilter},1").ToString();
                if (!string.IsNullOrEmpty(errorText))
                {
                    r.LogWrongData(DAccess.DataModule.ExecuteScalarCommand1($"exec town.Create_Pay_Reestr '{r.TempTableName}',{r.BillFilter},1").ToString());
                }
            }

            foreach (string line in lines)
            {
                if (line.Trim().Length == 0) continue;

                string[] s = line.Split(new Char[] { ';', ':' });

                //Буторин Д.А. 3.07.14, добавил игнорирование строк, начинающихся с "SIGN" и "<" 
                //Завьялова А.А. 25.04.22 добавила игнорирование строк, начинающихся с "=" (формат реестров 14_1)
                if (s[0].StartsWith("#") || s[0].StartsWith("SIGN") || s[0].StartsWith("<") || s[0].StartsWith("=")) continue;
                r.PayCountLoaded++;

                DateTime dt;
                //старая структура СБ РФ до 29.05.2014
                if (s.Length == 23 && !DateTime.TryParse(s[0], out dt))
                {
                    sql = MakeSqlString(s[10], s);
                }
                //старая структура СБ РФ c 30.05.2014
                else if ((s.Length == 38 || s.Length == 39) && !DateTime.TryParse(s[0], out dt) && !r.AutoCompliteAccount) 
                {
                    if (s[2].Length != 18 && s[2].Length != 24)
                    {
                        sql = string.Empty;
                        r.LogWrongData(line);
                    }
                    else
                    {
                        string authcod = (s[2].Length == 24 ? s[2] : s[2].Substring(0, 6) + DAccess.Instance.PrevPeriod.AuthcodPeriod + s[2].Substring(6, 12));
                        sql = MakeSqlString(authcod, s);
                    }
                }
                //старая структура СБ РФ c 30.05.2014
                else if ((s.Length == 38 || s.Length == 39) && !DateTime.TryParse(s[0], out dt) && r.AutoCompliteAccount)
                {
                    string ls = s[2];
                    if (ls.Length == 18)
                    {
                        string authcod = (s[8].Length == 24 ? s[8] : s[2].Substring(0, 6) + DAccess.Instance.PrevPeriod.AuthcodPeriod + ls.Substring(6));
                        sql = MakeSqlString(authcod, s);
                    }
                    else if (ls.Length == 12)
                    {
                        DataRow[] billFilter = DAccess.Instance.GetBillFilter(ls, r.IdClient);

                        if (billFilter == null || billFilter.Length != 1)
                        {
                            sql = string.Empty;
                            r.LogWrongData(line);
                        }
                        else
                        {
                            string uk = StringHelper.ZeroCode4(int.Parse(billFilter[0]["derive_company_code"].ToString()));
                            string bill_filter_code = StringHelper.ZeroCode(int.Parse(billFilter[0]["bill_filter_code"].ToString()));

                            string authcod = uk + bill_filter_code + DAccess.Instance.PrevPeriod.AuthcodPeriod + ls;
                            sql = MakeSqlString(authcod, s);
                        }
                    }
                    else

                        //Буторин Д.А. 3.07.14, закомментировал строку и добавил вывод строки с
                        //неопределенным ЛС в файл с ошибкой

                        //throw new Exception("Ошибка разбора файла, не удалось определить ЛС, строка в файле:" + line);
                    {
                        sql = string.Empty;
                        r.LogWrongData(line);
                    }
                    //End

                }
                //новая структура СБ РФ c 01.01.2022
                else if (s.Length <= 38 && DateTime.TryParse(s[0], out dt))
                {
                    string ls = s[5];
                    if (ls.Length == 18)
                    {
                        string authcod = ls.Substring(0, 6) + DAccess.Instance.PrevPeriod.AuthcodPeriod + ls.Substring(6);
                        sql = MakeSqlString(authcod, s);
                    }
                    else
                    {
                        sql = string.Empty;
                        r.LogWrongData(line);
                    }
                }
                else
                {
                    throw new Exception("Нераспознанный формат файла! Строка в файле: " + line);
                }

                if (sql.Length > 1)
                    DAccess.DataModule.ExecuteNonQueryCommand(sql);
            }

            return true;
        }

        private static bool ReadExcelReestrIntoTmpLoadTable(ReestrInfo r)
        {
            string sql = string.Empty;

            if (!FileHelper.ExistFile(r.SourceFile.FullName))
                return false;

            DataTable dt = ExcelHelper2.ImportExcelData("", r.SourceFile.FullName);

            WriteDataToDB(dt.CreateDataReader(), r);

            if (r.LoadFromExcel)
            {
                return ReadTxtReestrIntoTmpLoadTable(r);
            }

            return true;
        }

        private static bool ReadDbfReestrIntoTmpLoadTable(ReestrInfo r)
        {
            if (!FileHelper.ExistFile(r.SourceFile.FullName))
                return false;

            // Обрезаем файл до 8-ми символов
            FileInfo newFileName = r.SourceFile;
            if (r.SourceFile.Name.Length > 12)
            {
                newFileName = FileHelper.CopyFile(
                                r.SourceFile.FullName,
                                FileHelper.GetFilePath(r.SourceFile.FullName) + "L" + r.SourceFile.Name.Substring(0, 7) + ".dbf");
            }

            DbDataReader dr = DbfReestr.GetDataReader(newFileName.FullName);
            WriteDataToDB(dr, r);

            // Удаляем файл используемый для загрузки
            if (r.SourceFile.Name.Length > 12)
            {
                newFileName.Delete();
            }

            return true;
        }

        private static void WriteDataToDB(DbDataReader dr, ReestrInfo r)
        {
            string sql = string.Empty;
            if (!r.LoadFromExcel)
            {
                while (dr.Read())
                {
                    if (dr["AUTHCOD"].ToString().Length == 24 && dr["DATPLAT"].ToString().Contains("."))
                    {
                        sql = MakeSqlString(dr, r.MeterForLoad);
                    }
                    else
                    {
                        sql = string.Empty;
                        r.LogWrongData(string.Format("{0};{1};{2}", dr["AUTHCOD"].ToString(), dr["SUMPLAT"].ToString(), dr["DATPLAT"].ToString()));
                    }

                    if (sql.Length > 1)
                    {
                        DAccess.DataModule.ExecuteNonQueryCommand(sql);
                    }
                }
            }
            else
            {
                DAccess.DataModule.ExecuteNonQueryCommand($"if object_id('tempdb..{r.TempTableName}') is not null drop table {r.TempTableName} create table {r.TempTableName} (account varchar(100),summ float,date_plat datetime)");
                while (dr.Read())
                {

                    if (dr["Account"].ToString().Length != 0 && 12 >= dr["Account"].ToString().Length && dr["Datplat"].ToString().Contains("."))
                    {
                        sql = $"insert {r.TempTableName} values ([town].[CreateFullAccount] ('{dr["Account"].ToString()}') ,replace('{dr["Sumplat"].ToString()}',',','.') ,'{dr["Datplat"].ToString()}' )";
                    }
                    else
                    {
                        sql = string.Empty;
                        r.LogWrongData(string.Format("{0};{1};{2}", dr["Account"].ToString(), dr["Sumplat"].ToString(), dr["Datplat"].ToString()));
                    }

                    if (sql.Length > 1)
                    {
                        DAccess.DataModule.ExecuteNonQueryCommand(sql);
                    }
                }
            }
           
            dr.Close();
        }

        private static string StringFromDataRow(DataRow row)
        {
            string str = string.Empty;
            for (int i = 0; i < row.Table.Columns.Count; i++)
                str += row[i] + ";";
            return str;
        }

        private static string MakeSqlString(string authcod, string[] s)
        {
            string sql = string.Empty;
            DateTime dt;
            if ((s.Length == 23) && (!DateTime.TryParse(s[0], out dt)))
            {
                string[] date = s[20].Replace(".","/").Split(new Char[] { '/' });
                sql = "insert into #tmpLoad(LS,ADRES, PU, SUMPLAT, ND, ADDR_NT, UK, AUTHCOD, KVITID, PU1, POK1, PU2, POK2, PU3, POK3, PU4, POK4, DATPLAT, UNO)" + Environment.NewLine +
                      "values (" +
                      "'" + (s[0].Length > 12 ? s[0].Substring(0, 11) : s[0]) + "'," +
                      "'" + s[1] + "'," +
                      "'" + (s[0].Length > 12 ? s[0].Substring(0, 11) : s[0]) + "'," +
                      "" + s[3] + "," +
                      "" + s[7] + "," +
                      "'" + s[8] + "'," +
                      "'" + s[9] + "'," +
                      "'" + s[10] + "'," +
                      "'" + s[0] + "'," +
                      "" + (s[12].Length < 2 ? ("Null") : ("'" + s[12] + "'")) + "," +
                      "" + (s[13].Length == 0 ? ("Null") : s[13]) + "," +
                      "" + (s[14].Length < 2 ? ("Null") : ("'" + s[14] + "'")) + "," +
                      "" + (s[15].Length == 0 ? ("Null") : s[15]) + "," +
                      "" + (s[16].Length < 2 ? ("Null") : ("'" + s[16] + "'")) + "," +
                      "" + (s[17].Length == 0 ? ("Null") : s[17]) + "," +
                      "" + (s[18].Length < 2 ? ("Null") : ("'" + s[18] + "'")) + "," +
                      "" + (s[19].Length == 0 ? ("Null") : s[19]) + "," +
                      "'" + date[2] + date[1] + date[0] + "'," +
                      "'" + s[22] + "'" +
                      ")";
            }
            else if ((s.Length == 38 || s.Length == 39) && (!DateTime.TryParse(s[0], out dt)))
            {
                string[] date = s[37].Replace(".", "/").Split(new Char[] { '/' });
                sql = "insert into #tmpLoad(LS, ADRES, PU, SUMPLAT, ND, ADDR_NT, UK, AUTHCOD, KVITID, PU1, POK1, PU2, POK2, PU3, POK3, PU4, POK4, DATPLAT, UNO)" + Environment.NewLine +
                                  "values (" +
                                  "'" + (s[0].Length > 12 ? s[0].Substring(0, 11) : s[0]) + "'," +
                                  "'" + s[1] + "'," +
                                  "'" + (s[0].Length > 12 ? s[0].Substring(0, 11) : s[0]) + "'," +
                                  "" + s[3] + "," +
                                  "" + s[7] + "," +
                                  "'" + s[1] + "'," +
                                  "'" + "" + "'," +
                                  "'" + authcod + "'," +
                                  "'" + "" + "'," +
                                  "" + (s[13].Length < 2 ? ("Null") : ("'" + s[13] + "'")) + "," +
                                  "" + (s[14].Length == 0 ? ("Null") : s[14]) + "," +
                                  "" + (s[16].Length < 2 ? ("Null") : ("'" + s[16] + "'")) + "," +
                                  "" + (s[17].Length == 0 ? ("Null") : s[17]) + "," +
                                  "" + (s[19].Length < 2 ? ("Null") : ("'" + s[19] + "'")) + "," +
                                  "" + (s[20].Length == 0 ? ("Null") : s[20]) + "," +
                                  "" + (s[22].Length < 2 ? ("Null") : ("'" + s[22] + "'")) + "," +
                                  "" + (s[23].Length == 0 ? ("Null") : s[23]) + "," +
                                  "'" + date[2] + date[1] + date[0] + "'," +
                                  "'" + s[36] + "'" +
                                  ")";
            }
            //Формирование sql-запроса для реестров формата 14_1 с "блуждающими" полями для счетчиков:
            else if ((s.Length <= 38) && (DateTime.TryParse(s[0], out dt)))
            {
                string[] date = s[0].Replace("-", "/").Split(new Char[] { '/' });
                //string kanal = s[s.Length - 1];
                string pu1 = "ХВС", pu2 = "ГВС", pu3 = "ГАЗ", pu4 = "ЭЛ/ЭН", 
                       pok1 = "Null", pok2 = "Null", pok3 = "Null", pok4 = "Null"; 
                for (int i = 11; i < s.Length; i++)
                {
                    if (s[i] == pu1) pok1 = s[i + 1].Replace(",", ".");
                    if (s[i] == pu2) pok2 = s[i + 1].Replace(",", ".");
                    if (s[i] == pu3) pok3 = s[i + 1].Replace(",", ".");
                    if (s[i] == pu4) pok4 = s[i + 1].Replace(",", ".");
                }

                sql = "insert into #tmpLoad(LS, ADRES, PU, SUMPLAT, ND, ADDR_NT, UK, AUTHCOD, KVITID, PU1, POK1, PU2, POK2, PU3, POK3, PU4, POK4, DATPLAT, UNO)" + Environment.NewLine +
                                  "values (" +
                                  "'" + (s[5].Length > 12 ? s[5].Substring(6) : s[5]) + "'," +                                  //LS
                                  "'" + s[6] + "'," +                                                                           //ADRES
                                  "'" + (s[5].Length > 12 ? s[5].Substring(6) : s[5]) + "'," +                                  //PU
                                  //"" + (kanal.StartsWith("5") ? s[9].Replace(",", ".") : s[10].Replace(",", ".")) + "," +     //SUMPLAT
                                  "" + s[9].Replace(",", ".") + "," +                                                           //SUMPLAT                            
                                  "" + "8627" + "," +                                                                           //ND
                                  "'" + s[6] + "'," +                                                                           //ADDR_NT
                                  "'" + "" + "'," +                                                                             //UK
                                  "'" + authcod + "'," +                                                                        //AUTHCOD
                                  "'" + "" + "'," +                                                                             //KVITID
                                  "'" + pu1  + "'," +                                                                           //PU1 - ХВС
                                  "" + (pok1.Length == 0 ? ("Null") : pok1) + "," +                                             //POK1
                                  "'" + pu2 + "'," +                                                                            //PU2 - ГВС
                                  "" + (pok2.Length == 0 ? ("Null") : pok2) + "," +                                             //POK2
                                  "'" + pu3 + "'," +                                                                            //PU3 - ГАЗ
                                  "" + (pok3.Length == 0 ? ("Null") : pok3) + "," +                                             //POK3
                                  "'" + pu4 + "'," +                                                                            //PU4 - ЭЛ/ЭН
                                  "" + (pok4.Length == 0 ? ("Null") : pok4) + "," +                                             //POK4
                                  "'" + date[2] + date[1] + date[0] + "'," +                                                    //DATPLAT
                                  "'" + s[4] + "'" +                                                                            //UNO
                                  ")";
            }
            return sql;
        }

        private static string MakeSqlString(string authcod, DataRow s)
        {
            string sql = string.Empty;
            string[] date = s[4].ToString().Split(new Char[] { '.' });
            sql = "insert into #tmpLoad(LS,ADRES, PU, SUMPLAT, ND, ADDR_NT, UK, AUTHCOD, KVITID, PU1, POK1, PU2, POK2, PU3, POK3, PU4, POK4, DATPLAT, UNO)" + Environment.NewLine +
                        "values (" +
                        "'" + s[3] + "'," +
                        "'" + s[2] + "'," +
                        "'" + s[0] + "'," +
                        "" + s[5] + "," +
                        "" + s[0] + "," +
                        "'" + s[2] + "'," +
                        "'" + "" + "'," +
                        "'" + authcod + "'," +
                        "'" + "" + "'," +
                        "Null," +
                        "Null," +
                        "Null," +
                        "Null," +
                        "Null," +
                        "Null," +
                        "Null," +
                        "Null," +
                        "'" + (date[2].Length == 2 ? "20"+date[2]:date[2]) + date[1] + date[0] + "'," +
                        "''" +
                        ")";
            return sql;
        }

        private static string MakeSqlString(DbDataReader dr, MeterLoadType meterLoadType)
        {
            string sql = string.Empty;
            string[] date = dr["DATPLAT"].ToString().Split(new Char[] { '.', ' ', ':' });
            sql = "insert into #tmpLoad(LS,ADRES, PU, SUMPLAT, ND, ADDR_NT, UK, AUTHCOD, KVITID, PU1, POK1, PU2, POK2, PU3, POK3, PU4, POK4, DATPLAT, UNO)" + Environment.NewLine +
                        "values (" +
                        "'" + dr["LS"] + "'," +
                        "'" + dr["ADRES"] + "'," +
                        "'" + dr["LS"] + "'," +
                        "" + (dr["SUMPLAT"] == DBNull.Value ? "0" : dr["SUMPLAT"].ToString().Replace(",", ".")) + "," +
                        "Null," +
                        "'" + dr["ADRES"] + "'," +
                        "''," +
                        "'" + dr["AUTHCOD"] + "'," +
                        "''," +
                        "0," +
                        "" + (!meterLoadType.Vh || dr["POK1"] == DBNull.Value ? "Null" : dr["POK1"].ToString().Replace(",", ".")) + "," +
                        "0," +
                        "" + (!meterLoadType.Vg || dr["POK2"] == DBNull.Value ? "Null" : dr["POK2"].ToString().Replace(",", ".")) + "," +
                        "0," +
                        "" + (!meterLoadType.Gaz || dr["POK3"] == DBNull.Value ? "Null" : dr["POK3"].ToString().Replace(",", ".")) + "," +
                        "0," +
                        "" + (!meterLoadType.El || dr["POK4"] == DBNull.Value ? "Null" : dr["POK4"].ToString().Replace(",", ".")) + "," +
                        "'" + date[2] + date[1] + date[0] + "'," +
                        "''" +
                        ")";
            return sql;
        }

        public static void ScanAndLoadAllReestr()
        {
            List<FileInfo> files = new List<FileInfo>();
            FileHelper.GetDirectoryTree(new DirectoryInfo(Properties.Settings.Default.RootReestrPath), files, "*.txt");
            int total = files.Count;
            int i = 1;

            foreach (FileInfo file in files)
            {
                if (file.Name.IndexOf("_loaded") > -1)
                    continue;

                ReestrInfo r = new ReestrInfo(file, new MeterLoadType(true, true, true, true));
                if (r.Check())
                {
                    FileHelper.WriteLog(file.FullName + " - начата загрузка.......");
                    if (ReestrLoader.LoadSberReestr(r, true, true))
                    {
                        ReestrLoaded(total, i, r, "загружен");
                    } else
                        ReestrLoaded(total, i, r, "ошибка");
                } else
                    ReestrLoaded(total, i, r, "ошибка");
                i++;
            }
        }

        private static void ReestrLoaded(int totalReestr, int loadedCount, ReestrInfo reestrInfo, string loadedStatus)
        {
            if (loadedStatus == "загружен")
                FileHelper.WriteLog("(" + reestrInfo.PayCountLoaded.ToString() +") " + reestrInfo.SourceFile.FullName + " - обработан успешно");
            else
                FileHelper.WriteLog("(" + reestrInfo.PayCountLoaded.ToString() + ") " + reestrInfo.SourceFile.FullName + " - не загружен, ошибка");

            if (Loaded != null)
                Loaded(totalReestr, loadedCount, reestrInfo, loadedStatus);
        }

        public static void LoadSberReestr(ReestrInfo r)
        {
            LoadSberReestr(r, false, false);
        }

        public static bool LoadSberReestr(ReestrInfo r, bool renameAfterLoading, bool saveErrors)
        {
            bool result = true;
            r.StartLoadTime = DateTime.Now;

            string file = r.SourceFile.FullName;
            string fpath = FileHelper.GetFilePath(file);
            string fname = FileHelper.GetFileName(file);

            if (DAccess.DataModule.Connection.State == System.Data.ConnectionState.Closed)
                DAccess.DataModule.Connection.Open();

            DAccess.DataModule.CreateTmpTables();

            bool success = false;

            try
            {
                switch (r.SourceFile.Extension.ToLower())
                {
                    case ".xls":
                        success = ReadExcelReestrIntoTmpLoadTable(r);
                        break;
                    case ".txt":
                        success = ReadTxtReestrIntoTmpLoadTable(r);
                        break;
                    case ".dbf":
                        success = ReadDbfReestrIntoTmpLoadTable(r);
                        break;
                }
            }
            catch (Exception ex)
            {
                success = false;
                MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                FileHelper.WriteLog(ex.Message + ex.StackTrace);
                FileHelper.WriteLog(" --- загрузка закончена --- ");
            }

            if (success)
            {
                try
                {
                    DAccess.DataModule.ReestrSumPaySber(r.PackNumber, r.IdPackType, r.PackDate, r.ReestrNumber, r.Peni);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                    FileHelper.WriteLog(ex.Message + ex.StackTrace);
                    FileHelper.WriteLog(" --- загрузка закончена --- ");
                }
                finally
                {
                    if (saveErrors)
                    {
                        ReestrLoader.LastErrorFile = file.ToLower().Replace(".txt", "").Replace(".xls", "").Replace(".dbf", "") + "_errors.xls";
                        ExcelHelper2.ExportToExcel(
                            DAccess.DataModule.GetErrorTable(),
                            ReestrLoader.LastErrorFile);
                    }

                    if (renameAfterLoading) RenameFileAfterLoad(r);

                    r.SaveWrongDataInSeparetedFiles();
                }
            }
            else
                result = false;

            r.EndLoadTime = DateTime.Now;

            r.LogLoadInfo(result.ToString());
            
            DAccess.DataModule.Connection.Close();
            return result;
        }

        private static void RenameFileAfterLoad(ReestrInfo r)
        {
            string file = r.SourceFile.FullName;
            switch (r.SourceFile.Extension.ToLower())
            {
                case ".xls":
                    FileHelper.RewriteFileWithRename(file, file.ToLower().Replace(".xls", "") + "_loaded.xls");
                    break;
                case ".txt":
                    FileHelper.RewriteFileWithRename(file, file.ToLower().Replace(".txt", "") + "_loaded.txt");
                    break;
                case ".dbf":
                    FileHelper.RewriteFileWithRename(file, file.ToLower().Replace(".dbf", "") + "_loaded.dbf");
                    break;
            }
        }
    }
}
