﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MrivcHelpers.Helpers;
using EllisBillFilter.DS;

namespace EllisBillFilter.Controls
{
    public partial class GridExcelImport : UserControl
    {
        ExcelImport excelHelper;
        private readonly string importTablePrefix = "Import_";

        public GridExcelImport()
        {
            InitializeComponent();
            gridExcelImportView.OptionsView.ColumnAutoWidth = false;
        }

        public DataTable ImportTableData { get; private set;}

        public string ImportTableName
        {
            get
            {
                return txtDbTableName.Text;
            }
        }

        public void OpenFile(string fileName)
        {
            excelHelper = new ExcelImport(fileName);

            txtDbTableName.Text = $"{importTablePrefix}" + StringHelper.GetShortTimeDateString();

            ddExcelSheets.EditValue = string.Empty;
            ddExcelSheets.Properties.DataSource = null;
            ddExcelSheets.Properties.DataSource = excelHelper.ExcelSheets;
            ddExcelSheets.EditValue = excelHelper.ExcelSheets[0];
        }

        private void ddExcelSheets_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                gridExcelImportView.Columns.Clear();
                baseGrid1.DataSource = null;

                if (string.IsNullOrEmpty(ddExcelSheets.EditValue.ToString()))
                    return;

                ImportTableData = excelHelper.ImportToDataTable(ddExcelSheets.EditValue.ToString());
                baseGrid1.DataSource = ImportTableData;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Error in GridExcelImport.ddExcelSheets_EditValueChanged: " + ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "xls files (*.xls;*.xlsx)|*.xls;*.xlsx";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.Multiselect = false;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                OpenFile(openFileDialog1.FileName);
            }
        }

        public void ImportExcelDataInDataBase(string dataBaseName)
        {
            if (ImportTableData == null || ImportTableData.Rows.Count == 0)
            {
                throw new ApplicationException("Данных для импорта не найдено.");
            }

            if (ImportTableName.Length == 0 || importTablePrefix.Equals(ImportTableName, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new ApplicationException("Пожалуйста укажите имя временной таблицы.");
            }

            DAccess.DataModule.BulkCopyData(ImportTableData, ImportTableName, dataBaseName);
        }
    }
}
