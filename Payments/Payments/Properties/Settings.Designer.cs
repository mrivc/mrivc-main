﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Payments.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int LastUserID {
            get {
                return ((int)(this["LastUserID"]));
            }
            set {
                this["LastUserID"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int DrawerNumber {
            get {
                return ((int)(this["DrawerNumber"]));
            }
            set {
                this["DrawerNumber"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int Department_Number {
            get {
                return ((int)(this["Department_Number"]));
            }
            set {
                this["Department_Number"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("comkey.exe")]
        public string BarcodeScannerDriverExe {
            get {
                return ((string)(this["BarcodeScannerDriverExe"]));
            }
            set {
                this["BarcodeScannerDriverExe"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool OpenDrawerAfterPayment {
            get {
                return ((bool)(this["OpenDrawerAfterPayment"]));
            }
            set {
                this["OpenDrawerAfterPayment"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool UseBarcodeScaner {
            get {
                return ((bool)(this["UseBarcodeScaner"]));
            }
            set {
                this["UseBarcodeScaner"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3000")]
        public int TimeoutBetweenPrintCopy {
            get {
                return ((int)(this["TimeoutBetweenPrintCopy"]));
            }
            set {
                this["TimeoutBetweenPrintCopy"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=FileNameHere;Extended Properties=dBA" +
            "SE IV;")]
        public string DbfReestrOleDbConnection {
            get {
                return ((string)(this["DbfReestrOleDbConnection"]));
            }
            set {
                this["DbfReestrOleDbConnection"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Driver={Microsoft dBASE Driver (*.dbf)};DriverID=277;Dbq=FileNameHere;")]
        public string DbfReestrOdbcConnection {
            get {
                return ((string)(this["DbfReestrOdbcConnection"]));
            }
            set {
                this["DbfReestrOdbcConnection"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool DbfReestrUseOleDbConnection {
            get {
                return ((bool)(this["DbfReestrUseOleDbConnection"]));
            }
            set {
                this["DbfReestrUseOleDbConnection"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SRV-MRIVC5-1\\INSTANCE2;Initial Catalog=Payments;Integrated Security=T" +
            "rue")]
        public string PaymentsConnectionString1 {
            get {
                return ((string)(this["PaymentsConnectionString1"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("6")]
        public int KKTBaudRate {
            get {
                return ((int)(this["KKTBaudRate"]));
            }
            set {
                this["KKTBaudRate"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3")]
        public int KKTComPort {
            get {
                return ((int)(this["KKTComPort"]));
            }
            set {
                this["KKTComPort"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public int KKTTimeout {
            get {
                return ((int)(this["KKTTimeout"]));
            }
            set {
                this["KKTTimeout"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SRV-MRIVC6;Initial Catalog=Payments;Persist Security Info=True;User I" +
            "D=bobrovsky;Password=speakermrivc")]
        public string PaymentsConnectionString {
            get {
                return ((string)(this["PaymentsConnectionString"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Persist Security Info=True;User I" +
            "D=bobrovsky;Password=speakermrivc")]
        public string MurmanskConnectionString {
            get {
                return ((string)(this["MurmanskConnectionString"]));
            }
            set {
                this["MurmanskConnectionString"] = value;
            }
        }
    }
}
