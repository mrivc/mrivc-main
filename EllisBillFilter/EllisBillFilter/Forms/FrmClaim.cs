﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using System.Globalization;
    using System.Xml.Serialization;

    using EllisBillFilter.DS;

    public partial class FrmClaim : Form
    {
        private int idAccount;

        private int period;

        public FrmClaim(int idAccount, int period)
        {
            InitializeComponent();

            this.idAccount = idAccount;
            this.period = period;

            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
            //this.idAccount = 344699;
            //this.period = 24193;
        }

        private void FrmClaim_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        public void LoadData()
        {
            DAccess.DataModule.ClaimStatusSelect(mDataSet.PaperStatus);
            DAccess.DataModule.ClaimDeriveCompany(mDataSet.DeriveCompany, period, idAccount);
            DAccess.DataModule.ClaimProviderGroup(mDataSet.ProviderGroup, period, idAccount);
           
            SetInitValues();
        }

        private void SetInitValues()
        {
            int countDays = Convert.ToInt32(seDaysCount.EditValue);

            if (mDataSet.PaperStatus.Count > 0) ddPaperStatus.EditValue = mDataSet.PaperStatus[0].ID_Paper_Status;
            if (mDataSet.DeriveCompany.Count > 0) ddDeriveCompany.EditValue = mDataSet.DeriveCompany[0].ID_Company;
            if (mDataSet.ProviderGroup.Count > 0) ddProviderGroup.EditValue = -1;
            if (deClaimStartDate.EditValue == null) deClaimStartDate.EditValue = DateTime.Now;
           
            if (countDays == 0)
            {
                countDays = (int)(this.seDaysCount.EditValue = Convert.ToInt32(DAccess.DataModule.GetSystemParameter("#CLAIM_DURATION", false)));
                DateTime startDate = (DateTime)deClaimStartDate.EditValue;
                deClaimEndDate.EditValue = startDate.AddDays(countDays);
            }

            UpdateData();
        }

        private void UpdateData()
        {
            int idDeriveCompany = (int)ddDeriveCompany.EditValue;
            int idProviderGroup = (int)ddProviderGroup.EditValue;

            DataTable data = DAccess.DataModule.ClaimCalcDebt(idAccount, period, idDeriveCompany, idProviderGroup, 0, 0, "NULL", "NULL", "balanceTableDebt");

            if (data.Rows.Count == 0)
                return;

            txtSum.EditValue = decimal.Parse(data.Rows[0]["Debt"].ToString());
            txtSum_Fine.EditValue = decimal.Parse(data.Rows[0]["Debt_Fine"].ToString());
            txtSum_Full.EditValue = (decimal)txtSum.EditValue + (decimal)txtSum_Fine.EditValue;
        }

        private void cbFine_CheckedChanged(object sender, EventArgs e)
        {
            if (txtSum_Fine.EditValue == null || txtSum.EditValue == null)
                return;

            if (cbFine.Checked)
            {
                txtSum_Full.EditValue = decimal.Parse(txtSum.EditValue.ToString()) + (decimal)txtSum_Fine.EditValue;
            }
            else
            {
                txtSum_Full.EditValue = (decimal)txtSum.EditValue;
            }
        }

        private void txtSum_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (txtSum_Fine.EditValue == null || txtSum.EditValue == null)
                return;

            if (cbFine.Checked)
            {
                txtSum_Full.EditValue = Decimal.Parse(e.NewValue.ToString(), CultureInfo.InvariantCulture) + (decimal)txtSum_Fine.EditValue;
            }
            else
            {
                txtSum_Full.EditValue = decimal.Parse(e.NewValue.ToString());
            }
        }

        private void txtSum_Fine_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (txtSum_Fine.EditValue == null || txtSum.EditValue == null)
                return;

            if (cbFine.Checked)
            {
                txtSum_Full.EditValue = (decimal)txtSum.EditValue + Decimal.Parse(e.NewValue.ToString(), CultureInfo.InvariantCulture);
            }
            else
            {
                txtSum_Full.EditValue = (decimal)txtSum.EditValue;
            }
        }

        private void seDaysCount_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            DateTime startDate = (DateTime)deClaimStartDate.EditValue;
            deClaimEndDate.EditValue = startDate.AddDays(Convert.ToInt32(e.NewValue));
        }

        private void deClaimStartDate_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            DateTime startDate = Convert.ToDateTime(e.NewValue);
            deClaimEndDate.EditValue = startDate.AddDays(Convert.ToInt32(seDaysCount.EditValue));
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            int idDeriveCompany = (int)ddDeriveCompany.EditValue;
            int idProviderGroup = (int)ddProviderGroup.EditValue;
            decimal sumFine = cbFine.Checked ? decimal.Parse(txtSum_Fine.EditValue.ToString()) : 0;
            DateTime dateClaim = (DateTime)deClaimStartDate.EditValue;
            DateTime datePlanRepayment = (DateTime)deClaimEndDate.EditValue;

            if (tabControl1.SelectedTab == tabPage1)
            {
                if (this.CanClaimAdd())
                {
                    DAccess.DataModule.ClaimAddEdit(this.idAccount, idDeriveCompany, idProviderGroup, decimal.Parse(txtSum.EditValue.ToString()), sumFine, datePlanRepayment,
                        Convert.ToInt32(ddPaperStatus.EditValue), txtNote.Text, dateClaim, null, this.period, false);

                    MessageBox.Show("Претензия создана успешно. Нажмите кнопку \"Обновить\" на лицевом счете.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }
            else if (tabControl1.SelectedTab == tabPage2)
            {
                DAccess.DataModule.LawsuitAddEdit(this.idAccount, idDeriveCompany, idProviderGroup, decimal.Parse(txtSum.EditValue.ToString()), sumFine, datePlanRepayment,
                    Convert.ToInt32(ddPaperStatus.EditValue), txtNote.Text, dateClaim, null, this.period, false);
            }
        }

        private bool CanClaimAdd()
        {
            int answer = DAccess.DataModule.ClaimCanAdd(Convert.ToInt32(ddDeriveCompany.EditValue), -1, idAccount);

            switch (answer)
            {
                case 1:
                    MessageBox.Show("По данному лицевому счету существует соглашение. Для создания претензии необходимо завершить текущее соглашение.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                case 2:
                    MessageBox.Show("По данному лицевому счету существует иск. Для создания претензии необходимо закрыть текущий иск.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
            }

            if (txtNote.Text.Length == 0)
            {
                MessageBox.Show("Необходимо указать примечание.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;
        }
    }
}
