﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class RecoverMunicipalServices : MrivcImportCommand
    {
        public RecoverMunicipalServices(string senderID) : base("importMunicipalServices", "NsiAsync", senderID)
        {
        }

        protected override void CreateXmlBody()
        {
            var recoverMunServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.RecoverMunicipalServices @SenderID", new[,] { { "@SenderID", SenderID } });

            foreach (DataRow row in recoverMunServices.Rows)
            {
                var values = new[]
                {
                    new Tuple<string[],string>(new [] { "ElementGuid" }, row["ElementGuid"].ToString())
                };

                AddChildElement(MrivcStatic.GisConfig.Services["NsiAsync"].Methods["importMunicipalServices_recover"].Template, values, "importMunicipalServicesRequest");
            }

        }
    }
}
