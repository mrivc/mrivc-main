﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MrivcControls
{
    public class GisSenderProvider : MultiValueDataPicker
    {
        public void Fill(string connectionString)
        {
            DAccess.DataModule.SetConnection(connectionString);
            DataSource = DAccess.DataModule.GetDataTableByQueryCommand("select distinct SenderID,SenderName from GIS.Provider order by 2");
        }

        public object SenderID()
        {
            return SelectedValue;
        }

        public GisSenderProvider()
        {
            ParameterName = "Поставщик информации ГИС ЖКХ";
            ValueMember = "SenderID";
            DisplayMember = "SenderName";
        }
    }
}
