using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace FastReport.Map
{
    /// <summary>
    /// Represents a distance scale.
    /// </summary>
    public class DistanceScale : ScaleBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DistanceScale"/> class.
        /// </summary>
        public DistanceScale()
        {
        }

        #endregion // Constructors
    }
}
