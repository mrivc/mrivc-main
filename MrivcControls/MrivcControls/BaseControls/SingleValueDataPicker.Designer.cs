﻿namespace MrivcControls
{
    partial class SingleValueDataPicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parameterComboBox = new System.Windows.Forms.ComboBox();
            this.parameterGroupBox = new System.Windows.Forms.GroupBox();
            this.parameterGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // parameterComboBox
            // 
            this.parameterComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.parameterComboBox.FormattingEnabled = true;
            this.parameterComboBox.Location = new System.Drawing.Point(16, 19);
            this.parameterComboBox.Name = "parameterComboBox";
            this.parameterComboBox.Size = new System.Drawing.Size(175, 21);
            this.parameterComboBox.TabIndex = 0;
            // 
            // parameterGroupBox
            // 
            this.parameterGroupBox.Controls.Add(this.parameterComboBox);
            this.parameterGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parameterGroupBox.Location = new System.Drawing.Point(0, 0);
            this.parameterGroupBox.Name = "parameterGroupBox";
            this.parameterGroupBox.Size = new System.Drawing.Size(209, 55);
            this.parameterGroupBox.TabIndex = 1;
            this.parameterGroupBox.TabStop = false;
            this.parameterGroupBox.Text = "parameterGroupBox";
            // 
            // SingleValueDataPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.parameterGroupBox);
            this.Name = "SingleValueDataPicker";
            this.Size = new System.Drawing.Size(209, 55);
            this.parameterGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox parameterComboBox;
        private System.Windows.Forms.GroupBox parameterGroupBox;
    }
}
