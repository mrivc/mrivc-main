﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.OleDb;

namespace loadmeasure
{
    class Program
    {
        static void Main()
        {
            //try
            //{
                Console.WriteLine("Идет загрузка показаний...");
              
                MySqlConnection mcon = new MySqlConnection(Properties.Settings.Default.MySqlConnection);
                MySqlCommand mcmd = new MySqlCommand();
                MySqlDataReader mreader;
                
                mcon.Open();
                mcmd.Connection = mcon;
                mcmd.CommandType = CommandType.Text;
                mcmd.CommandTimeout = 0;

                SqlConnection sqlConnection1 = new SqlConnection(Properties.Settings.Default.MeterConnection);
                SqlCommand cmd = new SqlCommand();
                
                cmd.CommandText = "";
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;
                sqlConnection1.Open();

            //    //Буторин Д.А. 13.12.2016 - УДАЛЯТЬ ВСЕ ПРЕДЫДУЩИЕ ПОКАЗАНИЯ
            //    cmd.CommandText = " delete from [meter].[dbo].meter_measure";
            //Console.WriteLine(cmd.CommandText);
            //cmd.ExecuteNonQuery();

                mcmd.CommandText = "set net_write_timeout=99999; set net_read_timeout=99999; select measure_date,id_meter,measure_value from meter_measure";
                mreader = mcmd.ExecuteReader();
                    
                MySqlConnection mcon1 = new MySqlConnection(Properties.Settings.Default.MySqlConnection);
                MySqlCommand mcmd1 = new MySqlCommand();


                mcon1.Open();
                mcmd1.Connection = mcon1;
                mcmd1.CommandType = CommandType.Text;
                mcmd1.CommandTimeout = 0;

            while (mreader.Read())
            {
               
                    DateTime date = new DateTime();
                    date = (DateTime)mreader[0];
                    string dat = date.ToString("yyyy-MM-dd HH:mm:ss");

                    string val = mreader[2].ToString();

                    val = val.Replace(",", ".");

                    cmd.CommandText = "insert into meter_measure values('" + mreader[0] + "'," + mreader[1] + "," + val + ")";
                    cmd.ExecuteNonQuery();

                    Console.WriteLine(cmd.CommandText);

                    cmd.CommandText = "insert into meter_measure_a values('" + mreader[0] + "'," + mreader[1] + "," + val + ")";
                    cmd.ExecuteNonQuery();

                    mcmd1.CommandText = "insert into meter_measure_a values(" + mreader[1] + "," + val + ",'" + dat + "')";
                    mcmd1.ExecuteNonQuery();
               

            }

            mcon1.Close();

                mcon.Close();
                mcon.Open();

                mcmd.CommandText = "delete from meter_measure";
                mcmd.ExecuteNonQuery();

                mcon.Close();
                mcon.Open();

            try
            {

                cmd.CommandText = "exec meter_filter";
                cmd.ExecuteNonQuery();


                sqlConnection1.Close();

                SqlConnection sqlConnection2 = new SqlConnection(Properties.Settings.Default.MurmConnection);
                cmd.Connection = sqlConnection2;
                cmd.Connection.Open();

                cmd.CommandText = "exec town.Meter_Site_Add";

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();


                cmd.Connection = sqlConnection1;
                cmd.Connection.Open();

                cmd.CommandText =
                " delete from [meter].[dbo].meter_measure";
                Console.WriteLine(cmd.CommandText);

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                Console.WriteLine("Показания успешно загружены");
            }
            catch(Exception ex)
            {
                Console.WriteLine("Ошибка: " + ex.Message);
            }
            Console.ReadLine();

            //}

            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    Console.ReadLine();
            //}

        }
    }
}
