﻿namespace EllisBillFilter
{
    partial class FrmSberSaldoSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSberSaldoSettings));
            this.gridClientDetailDeliverySettings1 = new EllisBillFilter.GridClientDetailDeliverySettings();
            this.SuspendLayout();
            // 
            // gridClientDetailDeliverySettings1
            // 
            this.gridClientDetailDeliverySettings1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridClientDetailDeliverySettings1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridClientDetailDeliverySettings1.Location = new System.Drawing.Point(0, 0);
            this.gridClientDetailDeliverySettings1.Name = "gridClientDetailDeliverySettings1";
            this.gridClientDetailDeliverySettings1.Size = new System.Drawing.Size(1011, 508);
            this.gridClientDetailDeliverySettings1.TabIndex = 0;
            this.gridClientDetailDeliverySettings1.Load += new System.EventHandler(this.gridClientDetailDeliverySettings1_Load);
            // 
            // FrmSberSaldoSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 508);
            this.Controls.Add(this.gridClientDetailDeliverySettings1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmSberSaldoSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторассылка реестров сальдо";
            this.ResumeLayout(false);

        }

        #endregion

        private GridClientDetailDeliverySettings gridClientDetailDeliverySettings1;
    }
}