﻿namespace XadesDemo
{
    partial class ImportMeteringDeviceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportMeteringDeviceForm));
            this.meterDataGrid = new System.Windows.Forms.DataGridView();
            this.prepareBtn = new System.Windows.Forms.Button();
            this.importBtn = new System.Windows.Forms.Button();
            this.countLabel = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.flatMeterDataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.houseMeterDataGridView = new System.Windows.Forms.DataGridView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.meterDataGrid)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flatMeterDataGridView)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.houseMeterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // meterDataGrid
            // 
            this.meterDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.meterDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meterDataGrid.Location = new System.Drawing.Point(3, 3);
            this.meterDataGrid.Name = "meterDataGrid";
            this.meterDataGrid.Size = new System.Drawing.Size(1097, 485);
            this.meterDataGrid.TabIndex = 1;
            // 
            // prepareBtn
            // 
            this.prepareBtn.Location = new System.Drawing.Point(388, 12);
            this.prepareBtn.Name = "prepareBtn";
            this.prepareBtn.Size = new System.Drawing.Size(134, 23);
            this.prepareBtn.TabIndex = 2;
            this.prepareBtn.Text = "Просмотр";
            this.prepareBtn.UseVisualStyleBackColor = true;
            this.prepareBtn.Click += new System.EventHandler(this.prepareBtn_Click);
            // 
            // importBtn
            // 
            this.importBtn.Enabled = false;
            this.importBtn.Location = new System.Drawing.Point(388, 41);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(134, 23);
            this.importBtn.TabIndex = 3;
            this.importBtn.Text = "Импорт";
            this.importBtn.UseVisualStyleBackColor = true;
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // countLabel
            // 
            this.countLabel.AutoSize = true;
            this.countLabel.Location = new System.Drawing.Point(12, 393);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(13, 13);
            this.countLabel.TabIndex = 4;
            this.countLabel.Text = "0";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 73);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1111, 517);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.meterDataGrid);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1103, 491);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Счетчики";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.flatMeterDataGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1103, 491);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Квартирные";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // flatMeterDataGridView
            // 
            this.flatMeterDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.flatMeterDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flatMeterDataGridView.Location = new System.Drawing.Point(3, 3);
            this.flatMeterDataGridView.Name = "flatMeterDataGridView";
            this.flatMeterDataGridView.Size = new System.Drawing.Size(1097, 485);
            this.flatMeterDataGridView.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.houseMeterDataGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1103, 491);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Общедомовые";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // houseMeterDataGridView
            // 
            this.houseMeterDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.houseMeterDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.houseMeterDataGridView.Location = new System.Drawing.Point(3, 3);
            this.houseMeterDataGridView.Name = "houseMeterDataGridView";
            this.houseMeterDataGridView.Size = new System.Drawing.Size(1097, 485);
            this.houseMeterDataGridView.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(538, 16);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(128, 17);
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "Обновление данных";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // ImportMeteringDeviceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 591);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.countLabel);
            this.Controls.Add(this.importBtn);
            this.Controls.Add(this.prepareBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportMeteringDeviceForm";
            this.Text = "Синхронизация приборов учета";
            this.Load += new System.EventHandler(this.ImportMeteringDeviceForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.meterDataGrid)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flatMeterDataGridView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.houseMeterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView meterDataGrid;
        private System.Windows.Forms.Button prepareBtn;
        private System.Windows.Forms.Button importBtn;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView flatMeterDataGridView;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView houseMeterDataGridView;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}