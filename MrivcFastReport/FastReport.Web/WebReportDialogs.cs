﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using FastReport.Utils;
using System.IO;
using FastReport.Export.Html;
using FastReport.Dialog;
using System.Windows.Forms;
using System.Drawing;

namespace FastReport.Web.Handlers
{
    public partial class WebExport : IHttpHandler
    {

        private bool dialogRefresh = false;

        private void ProcessDialogs(StringBuilder sb, WebReport webReport, HttpContext context)
        {
            Report report = webReport.Report;

            while (Properties.CurrentForm < report.Pages.Count && !(report.Pages[Properties.CurrentForm] is DialogPage && report.Pages[Properties.CurrentForm].Visible == true))
                Properties.CurrentForm++;

            if (Properties.CurrentForm < report.Pages.Count)
            {
                Properties.State = ReportState.Forms;
                DialogPage dialog = report.Pages[Properties.CurrentForm] as DialogPage;
                if (!dialog.ActiveInWeb)
                {
                    dialog.ActiveInWeb = true;
                    dialog.OnLoad(EventArgs.Empty);
                    dialog.OnShown(EventArgs.Empty);
                }
                GetDialogHtml(sb, dialog, webReport, context);
            }
            else
                Properties.State = ReportState.Report;
        }

        private void SetUpDialogs(WebReport webReport, HttpContext context)
        {
            string dialogN = context.Request.Params["dialog"];
            string controlName = context.Request.Params["control"];
            string eventName = context.Request.Params["event"];
            string data = context.Request.Params["data"];

            if (!string.IsNullOrEmpty(dialogN))
            {
                int dialogIndex = Convert.ToInt16(dialogN);
                if (dialogIndex >= 0 && dialogIndex < webReport.Report.Pages.Count)
                {
                    DialogPage dialog = webReport.Report.Pages[dialogIndex] as DialogPage;                    

                    DialogControl control = dialog.FindObject(controlName) as DialogControl;
                    if (control != null)
                    {
                        //dialogRefresh = true;
                        if (eventName == "onchange")
                        {
                            if (!string.IsNullOrEmpty(data))
                            {
                                if (control is TextBoxControl)
                                    TextBoxChange(control as TextBoxControl, data);
                                else if (control is ComboBoxControl)
                                    ComboBoxChange(control as ComboBoxControl, Convert.ToInt16(data));
                                else if (control is ListBoxControl)
                                    ListBoxChange(control as ListBoxControl, Convert.ToInt16(data));
                                else if (control is CheckedListBoxControl)
                                    CheckedListBoxChange(control as CheckedListBoxControl, data);
                                else if (control is DateTimePickerControl)
                                    DateTimePickerChange(control as DateTimePickerControl, data);
                                else if (control is MonthCalendarControl)
                                    MonthCalendarChange(control as MonthCalendarControl, data);
                            }
                        }
                        else if (eventName == "onclick")
                        {
                            if (control is ButtonControl)
                                ButtonClick(control as ButtonControl);
                            else if (control is CheckBoxControl)
                                CheckBoxClick(control as CheckBoxControl, data);
                            else if (control is RadioButtonControl)
                                RadioButtonClick(control as RadioButtonControl, data);
                        }
                    }
                }
            }
        }

        private void ControlFilterRefresh(DataFilterBaseControl control)
        {            
            control.FilterData();            
            if (control.DetailControl != null)
            {
                control.DetailControl.ResetFilter();
                control.DetailControl.FillData(control);
            }
        }

        private string GetDialogID(WebReport webReport)
        {
            return String.Concat(webReport.Prop.ControlID, "Dialog");
        }

        private void GetDialogHtml(StringBuilder sb, DialogPage dialog, WebReport webReport, HttpContext context)
        {
            //if (!dialogRefresh)
            //{
                // dialog window
                //GetDialogWindow(sb, dialog, webReport);
                sb.Append(String.Format("<div id=\"{0}\" style=\"position:relative;\" title=\"{1}\">", GetDialogID(webReport), dialog.Text));
            //}

            foreach (DialogControl control in dialog.Controls)
            {
                if (control.Visible)
                {
                    // button
                    if (control is ButtonControl)
                        sb.Append(GetButtonHtml(control as ButtonControl, webReport));
                    // label
                    else if (control is LabelControl)
                        sb.Append(GetLabelHtml(control as LabelControl, webReport));
                    // textbox
                    else if (control is TextBoxControl)
                        sb.Append(GetTextBoxHtml(control as TextBoxControl, webReport));
                    // checkbox
                    else if (control is CheckBoxControl)
                        sb.Append(GetCheckBoxHtml(control as CheckBoxControl, webReport));
                    // radio button
                    else if (control is RadioButtonControl)
                        sb.Append(GetRadioButtonHtml(control as RadioButtonControl, webReport));
                    // combo box
                    else if (control is ComboBoxControl)
                        sb.Append(GetComboBoxHtml(control as ComboBoxControl, webReport));
                    // list box
                    else if (control is ListBoxControl)
                        sb.Append(GetListBoxHtml(control as ListBoxControl, webReport));
                    // checked list box
                    else if (control is CheckedListBoxControl)
                        sb.Append(GetCheckedListBoxHtml(control as CheckedListBoxControl, webReport));
                    // datetime
                    else if (control is DateTimePickerControl)
                        sb.Append(GetDateTimePickerHtml(control as DateTimePickerControl, webReport));
                    // monthcalendar
                    else if (control is MonthCalendarControl)
                        sb.Append(GetMonthCalendarHtml(control as MonthCalendarControl, webReport));

                    // add new controls here

                }
            }
            if (!dialogRefresh)
                sb.Append("</div>");
        }

        private void GetDialogWindow(StringBuilder sb, DialogPage dialog, WebReport webReport)
        {
            sb.Append("<script>").
                Append("$(function() {").
                Append("$(\"#").Append(GetDialogID(webReport)).Append("\").dialog({").
                Append("width:").Append((int)dialog.Width + 5).Append(", ").
                Append("height:").Append((int)dialog.Height + 20).
                Append(" }); });").
                Append("</script>");
        }
        
        private string GetEvent(string eventName, DialogControl control, WebReport webReport, string data)
        {
            return string.Format("{0}frRequestServer('{1}?object={2}&dialog={3}&control={4}&event={5}&data=' + {6}){7}",  
                "setTimeout(function(){",
                webReport.Prop.HandlerURL,
                webReport.Prop.ControlID,
                webReport.Prop.CurrentForm.ToString(),
                control.Name,
                eventName,
                string.IsNullOrEmpty(data) ? "''" : string.Format("{0}", data),
                "},250)"
                );
        }

        private string GetControlFont(DialogControl control)
        {
            return string.Format("font-size:{0}pt;font-weight:normal;display:inline-block;", control.Font.Size);
        }

        private string GetControlPosition(DialogControl control)
        {            
            return string.Format("position:absolute;left:{0}px;top:{1}px;width:{2}px;height:{3}px;padding:0px;margin:0px;", 
                control.Left, 
                control.Top, 
                control.Width, 
                control.Height);
        }

        private string GetControlAlign(DialogControl control)
        {
            if (control is LabelControl)
                return GetAlign((control as LabelControl).TextAlign);
            else if (control is ButtonControl)
                return GetAlign((control as ButtonControl).TextAlign);
            else if (control is TextBoxControl)
                return GetEditAlign((control as TextBoxControl).TextAlign);
            else
                return ""; 
        }

        private string GetEditAlign(HorizontalAlignment align)
        {
            if (align == HorizontalAlignment.Left)
                return "text-align:left;";
            else if (align == HorizontalAlignment.Center)
                return "text-align:center;";
            else if (align == HorizontalAlignment.Right)
                return "text-align:right;";
            else
                return "";
        }

        private string GetAlign(ContentAlignment align)
        {            
            if (align == ContentAlignment.TopLeft)
                return "text-align:left;vertical-align:top;";
            else if (align == ContentAlignment.TopCenter)
                return "text-align:center;vertical-align:top;";
            else if (align == ContentAlignment.TopRight)
                return "text-align:right;vertical-align:top;";
            else if (align == ContentAlignment.BottomLeft)
                return "text-align:left;vertical-align:bottom;";
            else if (align == ContentAlignment.BottomCenter)
                return "text-align:center;vertical-align:bottom;";
            else if (align == ContentAlignment.TopRight)
                return "text-align:right;vertical-align:bottom;";
            else if (align == ContentAlignment.MiddleLeft)
                return "text-align:left;vertical-align:middle;";
            else if (align == ContentAlignment.MiddleCenter)
                return "text-align:center;vertical-align:middle;";
            else if (align == ContentAlignment.MiddleRight)
                return "text-align:right;vertical-align:middle;";
            else
                return "";
        }
    }
}
