﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GisGkh;
using Helper.Data;
using System.Data.SqlClient;
using Helper;
using System.Runtime.InteropServices;

namespace GisGkh
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            DAccess.DataModule.Connection1 = new SqlConnection(MrivcData.ConnectionString);

            if (args.Length > 0)
            {
                RunConsole(args);
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
        }

        static void RunConsole(string[] args)
        {
            bool syncHouseData = false;
            bool syncAccountData = false;
            bool syncNsi = false;
            bool syncServices = false;
            bool sendServices = false;
            bool syncPaymentDocument = false;
            bool syncMeterData = false;
            bool syncMeterMeasure = false;
            bool syncSenderData = false;

            bool sendReport = false;

            foreach (var arg in args)
            {
                switch (arg)
                {
                    case "-si":
                        syncSenderData = true;
                        break;
                    case "-hd":
                        syncHouseData = true;
                        break;
                    case "-ad":
                        syncAccountData = true;
                        break;
                    case "-nsi":
                        syncNsi = true;
                        break;
                    case "-s":
                        syncServices = true;
                        break;
                    case "-ss":
                        sendServices = true;
                        break;
                    case "-pd":
                        syncPaymentDocument = true;
                        break;
                    case "-m":
                        syncMeterData = true;
                        break;
                    case "-mm":
                        syncMeterMeasure = true;
                        break;
                    case "-sr":
                        sendReport = true;
                        break;
                }
            }

            if (syncSenderData || syncNsi || syncServices || sendServices || syncHouseData || syncAccountData || syncMeterData || syncMeterMeasure || syncPaymentDocument)
            {
                var beginDate = DateTime.Now;

                DAccess.DataModule.ExecuteNonQueryCommand1("insert GIS.SyncBeginDateLog (SyncBeginDate,SyncSenderData,SyncNsi,SyncServices,SyncHouseData,SyncAccountData,SyncMeterData,SyncMeterMeasure,SyncPaymentDocument) values (@bd,@si,@n,@s,@hd,@ad,@md,@mm,@pd)", new object[,] { { "@bd", beginDate }, { "@si", syncSenderData }, { "@n", syncNsi }, { "@s", syncServices }, { "@hd", syncHouseData }, { "@ad", syncAccountData }, { "@md", syncMeterData }, { "@mm", syncMeterMeasure }, { "@pd", syncPaymentDocument } });

                Console.WriteLine($"Начата синхронизация за {beginDate}");

                var siError = false;
                var nsiError = false;

                beginDate = DateTime.Now;

                //try
                //{
                    var exportSenderID = new ExportOrgRegistry();
                    if (syncSenderData)
                    {
                        exportSenderID.Execute();
                    }

                    siError = exportSenderID.HasErrors;


                //}
                //catch (Exception ex)
                //{
                //    siError = true;
                //    MrivcStatic.WriteLog(ex);

                //}

                var SenderList = MrivcData.SenderList();

                beginDate = DateTime.Now;

                if (!siError)
                {
                    try
                    {
                        var exportNsiList = new ExportNsiList();
                        if (syncNsi)
                        {
                            var nsiRegNumbers = MrivcStatic.GisConfig.ExportNsiRegistryNumbers;
                            exportNsiList.NsiRegistryNumbers = nsiRegNumbers;
                            exportNsiList.Execute();
                        }

                        nsiError = exportNsiList.HasErrors;

                    }
                    catch (Exception ex)
                    {
                        nsiError = true;
                        MrivcStatic.WriteLog(ex);

                    }

                    MrivcStatic.WriteLogToConsole(beginDate);
                }

                if (syncServices)
                {
                    DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.FillServices2");
                }

                foreach (string senderID in SenderList)
                {

                    if (!nsiError && !siError)
                    {
                        var serviceError = false;

                        try
                        {
                            var importService = new ImportMunicipalServices(senderID);
                            var importAddService = new ImportAdditionalServices(senderID);
                            if (syncServices)
                            {
                                importService.AddToReport = true;
                                importService.Execute();


                                importAddService.AddToReport = true;
                                importAddService.Execute();
                            }
                            serviceError = importService.HasErrors || importAddService.HasErrors;
                        }
                        catch (Exception ex)
                        {
                            serviceError = true;
                            MrivcStatic.WriteLog(ex);
                        }

                        var singleCommandList = new List<ISingleHouseCommand>();

                        if (syncHouseData)
                            singleCommandList.Add(new ImportHouseData(senderID) { AddToReport = true });

                        if (syncAccountData)
                            singleCommandList.Add(new ImportAccountData(senderID) { AddToReport = true });

                        if (syncMeterData && !serviceError)
                            singleCommandList.Add(new ImportMeteringDevice(senderID) { AddToReport = true });

                        if (syncMeterMeasure && !serviceError)
                            singleCommandList.Add(new ImportMeasureData(senderID) { AddToReport = true });

                        if (syncPaymentDocument)
                        {
                            int period = (int)DAccess.DataModule.ExecuteScalarQueryCommand1("select dbo.work_period()-1");
                            singleCommandList.Add(new ImportPaymentDocument(senderID, period) { AddToReport = true });
                        }

                        MrivcStatic.HouseIteration(singleCommandList, senderID, false);
                    }

                    if (sendReport)
                    {
                        try
                        {
                            MrivcStatic.SendCommandReport(senderID, beginDate);

                        }
                        catch (Exception ex)
                        {
                            FileHelper.WriteLog("Ошибка при отправке отчетов по email: " + ex.Message);
                        }
                    }


                }

                Console.WriteLine("Синхронизация завершена");
                Console.ReadLine();
            }
        }
    }
}
