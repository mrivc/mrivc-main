﻿namespace EllisBillFilter
{
    partial class GridProviderGroup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridProviderGroup1 = new MrivcControls.BaseGrid();
            this.providerGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gridViewProviderGroup = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Provider_Group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProvider_Group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode_Group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridProviderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerGroupBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProviderGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // gridProviderGroup1
            // 
            this.gridProviderGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridProviderGroup1.DataSource = this.providerGroupBindingSource;
            this.gridProviderGroup1.Location = new System.Drawing.Point(0, 35);
            this.gridProviderGroup1.MainView = this.gridViewProviderGroup;
            this.gridProviderGroup1.Name = "gridProviderGroup1";
            this.gridProviderGroup1.Size = new System.Drawing.Size(322, 463);
            this.gridProviderGroup1.TabIndex = 0;
            this.gridProviderGroup1.TrimCellValueBeforeValidate = true;
            this.gridProviderGroup1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProviderGroup});
            // 
            // providerGroupBindingSource
            // 
            this.providerGroupBindingSource.DataMember = "ProviderGroup";
            this.providerGroupBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewProviderGroup
            // 
            this.gridViewProviderGroup.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Provider_Group,
            this.colProvider_Group,
            this.colID_Company,
            this.colCode_Group});
            this.gridViewProviderGroup.GridControl = this.gridProviderGroup1;
            this.gridViewProviderGroup.Name = "gridViewProviderGroup";
            this.gridViewProviderGroup.DoubleClick += new System.EventHandler(this.gridViewProviderGroup_DoubleClick);
            // 
            // colID_Provider_Group
            // 
            this.colID_Provider_Group.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Provider_Group.AppearanceCell.Options.UseBackColor = true;
            this.colID_Provider_Group.Caption = "ИД";
            this.colID_Provider_Group.FieldName = "ID_Provider_Group";
            this.colID_Provider_Group.Name = "colID_Provider_Group";
            this.colID_Provider_Group.OptionsColumn.AllowEdit = false;
            this.colID_Provider_Group.OptionsColumn.ReadOnly = true;
            this.colID_Provider_Group.Visible = true;
            this.colID_Provider_Group.VisibleIndex = 0;
            this.colID_Provider_Group.Width = 141;
            // 
            // colProvider_Group
            // 
            this.colProvider_Group.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colProvider_Group.AppearanceCell.Options.UseBackColor = true;
            this.colProvider_Group.Caption = "Группа поставщиков";
            this.colProvider_Group.FieldName = "Provider_Group";
            this.colProvider_Group.Name = "colProvider_Group";
            this.colProvider_Group.OptionsColumn.AllowEdit = false;
            this.colProvider_Group.OptionsColumn.ReadOnly = true;
            this.colProvider_Group.Visible = true;
            this.colProvider_Group.VisibleIndex = 1;
            this.colProvider_Group.Width = 949;
            // 
            // colID_Company
            // 
            this.colID_Company.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colID_Company.AppearanceCell.Options.UseBackColor = true;
            this.colID_Company.FieldName = "ID_Company";
            this.colID_Company.Name = "colID_Company";
            this.colID_Company.OptionsColumn.AllowEdit = false;
            this.colID_Company.OptionsColumn.ReadOnly = true;
            // 
            // colCode_Group
            // 
            this.colCode_Group.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colCode_Group.AppearanceCell.Options.UseBackColor = true;
            this.colCode_Group.FieldName = "Code_Group";
            this.colCode_Group.Name = "colCode_Group";
            this.colCode_Group.OptionsColumn.AllowEdit = false;
            this.colCode_Group.OptionsColumn.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Поиск:";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(57, 8);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(252, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // GridProviderGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gridProviderGroup1);
            this.Name = "GridProviderGroup";
            this.Size = new System.Drawing.Size(322, 498);
            ((System.ComponentModel.ISupportInitialize)(this.gridProviderGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerGroupBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProviderGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MrivcControls.BaseGrid gridProviderGroup1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProviderGroup;
        private System.Windows.Forms.BindingSource providerGroupBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Provider_Group;
        private DevExpress.XtraGrid.Columns.GridColumn colProvider_Group;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Company;
        private DevExpress.XtraGrid.Columns.GridColumn colCode_Group;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
    }
}
