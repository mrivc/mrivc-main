﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Payments.DS;
using Payments.LogicClasses;
using DevExpress.XtraEditors;

namespace Payments.Controls
{
    public partial class GridPayments : UserControl
    {
        private bool initialized = false;

        public GridPayments()
        {
            InitializeComponent();
            deBeginDate.DateTime = DateTime.Now;
            deEndDate.DateTime = DateTime.Now;
            initialized = true;
        }

        public SplitContainerControl SplitContainer
        {
            get { return splitContainerControl1; }
        }

        public void LoadData()
        {
            if (!initialized)
                return;

            paymentsDataSet.Cash_Type.Merge(DAccess.Instance.CashType);
            DAccess.DataModule.PaymentClassSelect(paymentsDataSet.Payment_Class);
            DAccess.DataModule.UserSelect(paymentsDataSet.User);
            DAccess.DataModule.PaymentSelect(paymentsDataSet.Payment, -1, deBeginDate.DateTime, deEndDate.DateTime);

            gridPaymentParameter1.DisableGrid();
            ddPaymentClass.EditValue = 1;
            gridPayment.InitEx();
        }

        public void ReLoadData()
        {
            if (!initialized)
                return;
            paymentsDataSet.Payment.Clear();
            DAccess.DataModule.PaymentSelect(paymentsDataSet.Payment, -1, deBeginDate.DateTime, deEndDate.DateTime);
        }

        private int SaveData()
        {
            return DAccess.DataModule.PaymentUpdate(paymentsDataSet.Payment);
        }

        private void btnInitPayment_Click(object sender, EventArgs e)
        {
            if (!DAccess.Instance.CurrentUser.HasPriviligy())
                return;

            IPayForm frm = null;

            if (!DAccess.CashTerminal.CanPrintSale())
                return;

            PaymentClass paymentClass = new PaymentClass(paymentsDataSet.Payment_Class.Select("[ID_Payment_Class] = " + ddPaymentClass.EditValue.ToString())[0]);

            if (paymentClass.IdPaymentClass == 1) //форма оплаты коммунальных услуг
            {
                if (DAccess.DataModule.ConnectedEllis)
                    frm = new FrmPayEllis(paymentClass);
                else {
                    MessageBox.Show("Нет соединения с базой данных ЭЛЛИС ЖКХ (Сервер: " + DAccess.Instance.ProgramSettings.ELLIS_DB_SERVER + ", база данных: " + DAccess.Instance.ProgramSettings.ELLIS_DB_NAME + ").\n" +
                           "\nПрием коммунальных платежей не доступен."
                           , "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else if (paymentClass.IdPaymentClass == 2) //форма оплаты платных услуг МУП МРИВЦ
            {
                frm = new FrmPayServices(paymentClass);
            }
            else if (paymentClass.IdPaymentClass == 3) //форма оплаты квитанций за Капитальный ремонт
            {
                frm = new FrmPayCapitalRepair(paymentClass);
            }
            else if (paymentClass.IdPaymentClass == 4) //поверка
            {
                frm = new FrmPayServices(paymentClass);
            }
            else if (paymentClass.IdPaymentClass == 5) //форма оплаты реестров
            {
                frm = new FrmPayReestr(paymentClass);
            }
            else if (paymentClass.IdPaymentClass == 6) //алые паруса
            {
                frm = new FrmPayServices(paymentClass);
            }
            else if (paymentClass.IdPaymentClass == 7) //север сервис
            {
                frm = new FrmPayServices(paymentClass);
            }

            if (frm != null && frm.Window.ShowDialog() == DialogResult.OK)
                if (paymentClass.IdPaymentClass != 5) //БДА 13-06-19 если не оплата реестров то доб-м единичный платеж, иначе цикл платежей в форме FrmPayReestr
                {
                    AddPayment(frm);
                }
                else
                {
                    LoadPaymentDetail();
                }
        }

        /// <summary>
        /// Добавляем платеж в базу данных
        /// </summary>
        /// <param name="frm">Форма платежа с данными о платеже</param>
        private void AddPayment(IPayForm frm)
        {
            DAccess.CashTerminal.FNGetStatus();

            PaymentsDataSet.PaymentRow row = paymentsDataSet.Payment.NewPaymentRow();
            row.ID_Payment = -1;
            row.ID_Payment_Class = (int)ddPaymentClass.EditValue;
            row.IsStorno = false;
            row.Operation_Number = DAccess.CashTerminal.OperationRegister.LastSaleNumber;
            //row.Session_Number = DAccess.CashTerminal.SessionNumber;
            row.Department_Number = Properties.Settings.Default.Department_Number;
            row.Payment_Cash = frm.PaymentInfo.PaymentCash;
            row.Payment_Summ = frm.PaymentInfo.PaymentSumm;
            row.Payment_Commission = frm.PaymentInfo.PaymentSummCommission;
            row.Payment_SummTotal = row.Payment_Summ + row.Payment_Commission;
            row.Payment_Charge = frm.PaymentInfo.PaymentCharge;
            row.Comment = frm.PaymentInfo.PaymentComment;
            row.Payment_Date = frm.PaymentInfo.PaymentDate;
            row.CreatedBy = DAccess.Instance.CurrentUser.Id_User;
            row.ID_Cash_Type = frm.PaymentInfo.CashType;
            row.Creation_Date = DateTime.Now;
            if (DAccess.CashTerminal.Terminal != null)
            {
                row.DocumentNumber = DAccess.CashTerminal.Terminal.DocumentNumber;
            }
            
            paymentsDataSet.Payment.AddPaymentRow(row);
            frm.PaymentClass.IdPayment = SaveData(); //(int)gridViewPayment.GetDataRow(gridViewPayment.FocusedRowHandle)["ID_Payment"];
            frm.SavePaymentDetail();
            LoadPaymentDetail();
        }

        private void gridViewPayment_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            LoadPaymentDetail();
        }

        private void LoadPaymentDetail()
        {
            if (gridViewPayment.FocusedRowHandle < 0)
                return;
            PaymentClass paymentClass = new PaymentClass((int)gridViewPayment.GetDataRow(gridViewPayment.FocusedRowHandle)["ID_Payment"],
                paymentsDataSet.Payment_Class.Select("[ID_Payment_Class] = " + gridViewPayment.GetDataRow(gridViewPayment.FocusedRowHandle)["ID_Payment_Class"].ToString())[0]);

            gridPaymentParameter1.LoadData(paymentClass);
        }

        private void miStornoSale_Click(object sender, EventArgs e)
        {
            if (!DAccess.Instance.CurrentUser.HasPriviligy())
                return;

            //PaymentsDataSet.PaymentRow row = (PaymentsDataSet.PaymentRow)gridViewPayment.GetDataRow(gridViewPayment.FocusedRowHandle);

            var selectedRows = gridViewPayment.GetSelectedRows();

            List<PaymentsDataSet.PaymentRow> listRows = new List<PaymentsDataSet.PaymentRow>();

            foreach (var rowHandle in selectedRows)
            {
                PaymentsDataSet.PaymentRow row = (PaymentsDataSet.PaymentRow)gridViewPayment.GetDataRow(rowHandle);
                listRows.Add(row);
            }


            foreach (var row in listRows)
            {
                switch (row.ID_Cash_Type)
                {
                    case 1: //при оплате наличными делаем возврат платежа через кассу
                        StornoCashViaTerminal(null);
                        break;
                    case 3: //реестр
                        StornoCashViaTerminal(row);
                        break;
                    case 2: //при оплате картой делаем простой возврат платежа
                        StornoCashViaVisaCard();
                        break;
                }
            }

            
        }

        /// <summary>
        /// Возврат платежа, если оплата производилась кредитной картой
        /// </summary>
        private void StornoCashViaVisaCard()
        {
            PaymentsDataSet.PaymentRow row = (PaymentsDataSet.PaymentRow)gridViewPayment.GetDataRow(gridViewPayment.FocusedRowHandle);
            decimal returnSumm = row.Payment_Summ + row.Payment_Commission;

            if (MessageBox.Show("Вы уверены, что хотите выполнить операцию возврата продажи на сумму " + Math.Round(returnSumm,2).ToString() + "руб. ?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.StornoPay(row, returnSumm);
            }
        }

        /// <summary>
        /// Возврат платежа через кассу, уменьшаем накопленную наличность в кассе
        /// </summary>
        private void StornoCashViaTerminal(PaymentsDataSet.PaymentRow paymentRow)
        {
            bool showMbox = true;
            PaymentsDataSet.PaymentRow row;
            if (paymentRow == null)
            {
                row = (PaymentsDataSet.PaymentRow)gridViewPayment.GetDataRow(gridViewPayment.FocusedRowHandle);
            }
            else
            {
                row = paymentRow;
                showMbox = false;
            }
            
            decimal cashValue = decimal.Parse(DAccess.CashTerminal.CashRegister.GetRegisterValue(241).ToString());

            if (paymentRow == null)
                if (cashValue < row.Payment_Summ)
                {
                    MessageBox.Show("Сумма накопленных денег в кассе меньше суммы возврата. Операция возврата отменена. ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            if (row["Payment_Commission"] == DBNull.Value) row.Payment_Commission = 0;

            decimal returnSumm = row.Payment_Summ + row.Payment_Commission;

            bool yes = false;
            if (showMbox == true)
                yes = MessageBox.Show("Вы уверены, что хотите выполнить операцию возврата продажи на сумму " + Math.Round(returnSumm, 2).ToString() + "руб. ?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
            else
                yes = true;

            if (yes)
            {
                if (DAccess.CashTerminal.ReturnSale(returnSumm, 1, row.Department_Number, "Возврат прихода ФД № " + row.DocumentNumber.ToString()))
                {
                    this.StornoPay(row, returnSumm, !showMbox);
                }
            }
        }

        /// <summary>
        /// Возврат платежа
        /// </summary>
        /// <param name="row">Срочка текущего платежа</param>
        private void StornoPay(PaymentsDataSet.PaymentRow row, decimal returnSumm, bool leaveComment = false)
        {
            string oldComment = "";
            if (leaveComment)
            {
                if (row["Comment"] != DBNull.Value)
                    oldComment = row["Comment"].ToString();
            }
            else
            {
                if (row["Comment"] != DBNull.Value)
                    oldComment = row["Comment"].ToString();
            }


            DAccess.CashTerminal.FNGetStatus();

            row["Comment"] = oldComment + " Возврат продажи на сумму: " + returnSumm.ToString() + " руб.";
            if (leaveComment)
                row["Comment"] = oldComment + " Возврат продажи";
            row["Payment_Summ"] = 0;
            row["Payment_Charge"] = 0;
            row["Payment_Cash"] = 0;
            row["Payment_Commission"] = 0;
            row["IsStorno"] = true;
            row["ReturnDocumentNumber"] = DAccess.CashTerminal.Terminal.DocumentNumber;
            SaveData();
        }

        private void deBeginDate_EditValueChanged(object sender, EventArgs e)
        {
            ReLoadData();
        }

        private void deEndDate_EditValueChanged(object sender, EventArgs e)
        {
            ReLoadData();
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            ReLoadData();
        }

        private void miExport_Click(object sender, EventArgs e)
        {
            gridPayment.MyExport();
        }

        private void gridViewPayment_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0 && (bool)gridViewPayment.GetDataRow(e.RowHandle)["IsStorno"] == true)
                e.Appearance.BackColor = Color.LightCoral;
        }

        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {
            if (gridViewPayment.FocusedRowHandle < 0)
            {
                miReturnSale.Enabled = false;
                return;
            }

            if ((bool)gridViewPayment.GetDataRow(gridViewPayment.FocusedRowHandle)["IsStorno"])
                miReturnSale.Enabled = false;
            else
                miReturnSale.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(gridPayment.MultiSelect.ToString());  
        }
    }
}
