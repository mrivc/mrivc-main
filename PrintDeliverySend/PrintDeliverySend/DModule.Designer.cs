﻿namespace PrintDeliverySend
{
    partial class DModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sqlMainConnection = new System.Data.SqlClient.SqlConnection();
            this.cmdAnyCommand = new System.Data.SqlClient.SqlCommand();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            this.daPrintDeliveryData = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            // 
            // sqlMainConnection
            // 
            this.sqlMainConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // cmdAnyCommand
            // 
            this.cmdAnyCommand.Connection = this.sqlMainConnection;
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "Reports.Print_Delivery_Bill_Select";
            this.sqlSelectCommand1.CommandTimeout = 0;
            this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand1.Connection = this.sqlMainConnection;
            this.sqlSelectCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // daPrintDeliveryData
            // 
            this.daPrintDeliveryData.SelectCommand = this.sqlSelectCommand1;
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.ConnectionString = "Data Source=srv-mrivc7;Initial Catalog=Murmansk;Persist Security Info=True;User I" +
    "D=bobrovsky;Password=speaker";
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;

        }

        #endregion

        private System.Data.SqlClient.SqlConnection sqlMainConnection;
        private System.Data.SqlClient.SqlCommand cmdAnyCommand;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private System.Data.SqlClient.SqlDataAdapter daPrintDeliveryData;
        private System.Data.SqlClient.SqlConnection sqlConnection1;
    }
}
