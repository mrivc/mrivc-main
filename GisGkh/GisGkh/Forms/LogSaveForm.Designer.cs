﻿namespace GisGkh.Forms
{
    partial class LogSaveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaveLog = new System.Windows.Forms.Button();
            this.tbLogID = new System.Windows.Forms.TextBox();
            this.lbLogID = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.gbSaveSeveralErrors = new System.Windows.Forms.GroupBox();
            this.btnRefreshErrors = new System.Windows.Forms.Button();
            this.chbSaveSeveralErrors = new System.Windows.Forms.CheckBox();
            this.gisErrorFilter = new Helper.Controls.GisErrorFilter();
            this.periodFilter = new Helper.Controls.PeriodFilter();
            this.senderIdOnlyFilter1 = new Helper.Controls.SenderIdOnlyFilter();
            this.chbSender = new System.Windows.Forms.CheckBox();
            this.gbSaveSeveralErrors.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSaveLog
            // 
            this.btnSaveLog.Location = new System.Drawing.Point(423, 322);
            this.btnSaveLog.Name = "btnSaveLog";
            this.btnSaveLog.Size = new System.Drawing.Size(131, 23);
            this.btnSaveLog.TabIndex = 0;
            this.btnSaveLog.Text = "Выгрузить";
            this.btnSaveLog.UseVisualStyleBackColor = true;
            this.btnSaveLog.Click += new System.EventHandler(this.btnSaveLog_Click);
            // 
            // tbLogID
            // 
            this.tbLogID.Location = new System.Drawing.Point(32, 12);
            this.tbLogID.Name = "tbLogID";
            this.tbLogID.Size = new System.Drawing.Size(120, 20);
            this.tbLogID.TabIndex = 1;
            // 
            // lbLogID
            // 
            this.lbLogID.AutoSize = true;
            this.lbLogID.Location = new System.Drawing.Point(8, 15);
            this.lbLogID.Name = "lbLogID";
            this.lbLogID.Size = new System.Drawing.Size(18, 13);
            this.lbLogID.TabIndex = 2;
            this.lbLogID.Text = "ID";
            // 
            // gbSaveSeveralErrors
            // 
            this.gbSaveSeveralErrors.Controls.Add(this.chbSender);
            this.gbSaveSeveralErrors.Controls.Add(this.senderIdOnlyFilter1);
            this.gbSaveSeveralErrors.Controls.Add(this.btnRefreshErrors);
            this.gbSaveSeveralErrors.Controls.Add(this.chbSaveSeveralErrors);
            this.gbSaveSeveralErrors.Controls.Add(this.gisErrorFilter);
            this.gbSaveSeveralErrors.Controls.Add(this.periodFilter);
            this.gbSaveSeveralErrors.Location = new System.Drawing.Point(12, 53);
            this.gbSaveSeveralErrors.Name = "gbSaveSeveralErrors";
            this.gbSaveSeveralErrors.Size = new System.Drawing.Size(542, 253);
            this.gbSaveSeveralErrors.TabIndex = 3;
            this.gbSaveSeveralErrors.TabStop = false;
            this.gbSaveSeveralErrors.Text = "Выгрузка пачки запросов";
            // 
            // btnRefreshErrors
            // 
            this.btnRefreshErrors.Location = new System.Drawing.Point(434, 198);
            this.btnRefreshErrors.Name = "btnRefreshErrors";
            this.btnRefreshErrors.Size = new System.Drawing.Size(102, 23);
            this.btnRefreshErrors.TabIndex = 3;
            this.btnRefreshErrors.Text = "Обновить";
            this.btnRefreshErrors.UseVisualStyleBackColor = true;
            this.btnRefreshErrors.Click += new System.EventHandler(this.btnRefreshErrors_Click);
            // 
            // chbSaveSeveralErrors
            // 
            this.chbSaveSeveralErrors.AutoSize = true;
            this.chbSaveSeveralErrors.Location = new System.Drawing.Point(6, 29);
            this.chbSaveSeveralErrors.Name = "chbSaveSeveralErrors";
            this.chbSaveSeveralErrors.Size = new System.Drawing.Size(322, 17);
            this.chbSaveSeveralErrors.TabIndex = 2;
            this.chbSaveSeveralErrors.Text = "Выгрузка нескольких запросов по периоду и типу ошибки";
            this.chbSaveSeveralErrors.UseVisualStyleBackColor = true;
            // 
            // gisErrorFilter
            // 
            this.gisErrorFilter.Location = new System.Drawing.Point(6, 177);
            this.gisErrorFilter.Name = "gisErrorFilter";
            this.gisErrorFilter.Period = 0;
            this.gisErrorFilter.Size = new System.Drawing.Size(422, 59);
            this.gisErrorFilter.TabIndex = 1;
            // 
            // periodFilter
            // 
            this.periodFilter.Location = new System.Drawing.Point(6, 119);
            this.periodFilter.Name = "periodFilter";
            this.periodFilter.Size = new System.Drawing.Size(279, 61);
            this.periodFilter.TabIndex = 0;
            // 
            // senderIdOnlyFilter1
            // 
            this.senderIdOnlyFilter1.Enabled = false;
            this.senderIdOnlyFilter1.Location = new System.Drawing.Point(6, 54);
            this.senderIdOnlyFilter1.Name = "senderIdOnlyFilter1";
            this.senderIdOnlyFilter1.Size = new System.Drawing.Size(395, 59);
            this.senderIdOnlyFilter1.TabIndex = 4;
            // 
            // chbSender
            // 
            this.chbSender.AutoSize = true;
            this.chbSender.Location = new System.Drawing.Point(348, 29);
            this.chbSender.Name = "chbSender";
            this.chbSender.Size = new System.Drawing.Size(104, 17);
            this.chbSender.TabIndex = 5;
            this.chbSender.Text = "По поставщику";
            this.chbSender.UseVisualStyleBackColor = true;
            this.chbSender.CheckedChanged += new System.EventHandler(this.chbSender_CheckedChanged);
            // 
            // LogSaveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 361);
            this.Controls.Add(this.gbSaveSeveralErrors);
            this.Controls.Add(this.lbLogID);
            this.Controls.Add(this.tbLogID);
            this.Controls.Add(this.btnSaveLog);
            this.Name = "LogSaveForm";
            this.Text = "LogSaveForm";
            this.Load += new System.EventHandler(this.LogSaveForm_Load);
            this.gbSaveSeveralErrors.ResumeLayout(false);
            this.gbSaveSeveralErrors.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSaveLog;
        private System.Windows.Forms.TextBox tbLogID;
        private System.Windows.Forms.Label lbLogID;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.GroupBox gbSaveSeveralErrors;
        private Helper.Controls.PeriodFilter periodFilter;
        private System.Windows.Forms.CheckBox chbSaveSeveralErrors;
        private Helper.Controls.GisErrorFilter gisErrorFilter;
        private System.Windows.Forms.Button btnRefreshErrors;
        private Helper.Controls.SenderIdOnlyFilter senderIdOnlyFilter1;
        private System.Windows.Forms.CheckBox chbSender;
    }
}