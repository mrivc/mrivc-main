﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MrivcHelpers.Forms
{
    public partial class FrmStringInput : Form
    {
        public FrmStringInput(string caption)
        {
            InitializeComponent();
            this.Text = caption;
        }

        public string InputValue
        {
            get
            {
                return txtInput.Text;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
