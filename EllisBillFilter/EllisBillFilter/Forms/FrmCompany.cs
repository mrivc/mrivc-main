﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using DevExpress.XtraGrid.Views.Base;

namespace EllisBillFilter
{
    public partial class FrmCompany : Form
    {
        int idCompany;

        public FrmCompany()
        {
            InitializeComponent();        
        }

        public FrmCompany(bool showTypeCompanyColumns)
        {
            InitializeComponent();
            colIs_Derive_Company.Visible = showTypeCompanyColumns;
            colIs_Pay_Receiver.Visible = showTypeCompanyColumns;
            colIs_Provider.Visible = showTypeCompanyColumns;
        }

        private void FrmCompany_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        public void LoadData()
        {
            DAccess.Instance.PayReceiverCompany = null;
            mDataSet.PayReceiverCompany.Merge(DAccess.Instance.PayReceiverCompany);

            gridCompany.InitEx();
            gridViewCompany.OptionsView.ShowGroupPanel = false;
            gridViewCompany.ClearColumnsFilter();
            gridCompany.DisableGrid();
        }

        public int IdCompany
        {
            get { return idCompany; }
        }

        public string SberServiceNumber
        {
            get; private set;
        }

        private void txtFilterName_TextChanged(object sender, EventArgs e)
        {
            if (txtFilterName.Text.Length > 1)
            {
                gridViewCompany.Columns["Name"].ClearFilter();
                gridViewCompany.ActiveFilter.Add(new ViewColumnFilterInfo(gridViewCompany.Columns["Name"], new DevExpress.XtraGrid.Columns.ColumnFilterInfo("[Name] like '%" + txtFilterName.Text + "%'")));
            }
            else
                gridViewCompany.Columns["Name"].ClearFilter();
        }

        private void gridCompany_DoubleClick(object sender, EventArgs e)
        {
            DataRow dr = gridViewCompany.GetDataRow(gridViewCompany.FocusedRowHandle);

            idCompany = (int)dr["ID_Company"];
            SberServiceNumber = dr["SberServiceNumber"] == DBNull.Value ? string.Empty : dr["SberServiceNumber"].ToString();
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
