﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    public abstract class DynamicParameterBaseControl
    {
        protected Label label;

        public DynamicParameter Parameter { get; private set; }

        public DynamicParameterBaseControl(DynamicParameter parameter)
        {
            this.Parameter = parameter;
            Init();
        }

        public readonly static Size ControlPanelDefaultSize = new System.Drawing.Size(316, 48);

        public Control Control { get; private set; }

        public Panel PanelControlHolder { get; private set; }

        private void Init()
        {
            this.Control = CreateControl();

            PanelControlHolder = new Panel();
            label = new Label();

            label.Size = new System.Drawing.Size(300, 13);
            this.Control.Size = new Size(300, 13);

            this.label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left))));
            this.Control.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));

            this.label.Location = new System.Drawing.Point(5, 5);
            this.Control.Location = new System.Drawing.Point(5, 23);

            this.label.Text = Parameter.Caption;
            this.PanelControlHolder.Size = ControlPanelDefaultSize;

            PanelControlHolder.Controls.Add(label);
            PanelControlHolder.Controls.Add(this.Control);
        }

        public abstract object EditValue { get; }

        protected abstract Control CreateControl();
    }
}
