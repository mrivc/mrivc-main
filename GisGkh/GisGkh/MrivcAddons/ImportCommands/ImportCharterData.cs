﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Xml;
using Xades.Helpers;
using System.IO;

namespace GisGkh
{
    public class ImportCharterData : MrivcImportCommand
    {
        bool _preview = true;

        bool empty;

        public ImportCharterData(string senderID) : base("importCharterData", "HomeManagementAsync", senderID)
        {
            _maxImportValue = 1;
            _importValueName = "EditCharter";
            Description = "Загрузка уставов";
        }

        public DataSet PrepareToDataSet()
        {
            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.Prepare_Import_Charter", new object[,] { { "senderid", SenderID } }, "Charter", "Object", "House", "Add");
        }

        protected override void CreateXmlBody()
        {
            _preview = false;
            var _preparedDataSet = PrepareToDataSet();
            _preview = true;

            foreach (DataRow charter in _preparedDataSet.Tables["Charter"].Rows)
            {
                var values1 = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "Date" },charter["Date"].ToString()),
                    new Tuple<string[], string>( new [] { "AttachmentCharter","Name" },charter["Name"].ToString()),
                    new Tuple<string[], string>( new [] { "AttachmentCharter","Description" },charter["Description"].ToString()),
                    new Tuple<string[], string>( new [] { "AttachmentCharter","Attachment","AttachmentGUID" },charter["AttachmentGUID"].ToString()),
                    new Tuple<string[], string>( new [] { "AttachmentCharter","AttachmentHASH" },charter["AttachmentHASH"].ToString())
                };

               var addedCharter =  AddChildElement(MrivcStatic.GisConfig.Services["HomeManagementAsync"].Methods["importCharterRequest_edit"].Template, values1, "importCharterRequest");



                foreach (DataRow obj in _preparedDataSet.Tables["Object"].Select("CharterGUID = '" +charter["CharterGUID"].ToString() + "' and CharterVersionGUID = '" + charter["CharterVersionGUID"].ToString() + "'"))
                {
                    var added = AddChildElement(MrivcStatic.GisConfig.Services["HomeManagementAsync"].Methods["importCharterRequest_co_edit_end"].Template, addedCharter);

                    values1 = new List<Tuple<string[], string>>
                    {
                        new Tuple<string[], string>( new [] { "Edit","FIASHouseGuid" },"FIASHouseGuid"),
                        new Tuple<string[], string>( new [] { "Edit","StartDate" },"StartDate"),
                        new Tuple<string[], string>( new [] { "Edit","ContractObjectVersionGUID" },"ContractObjectVersionGUID")
                        
                    };

                    var addedObj = AddChildElement(obj, Service, null, "importCharterRequest_co_edit", values1, null, null, added);

                    var valuesServices = new List<Tuple<string[], string>>
                        {
                            new Tuple<string[], string>( new[] { "ServiceType", "Code" },"Code"),
                            new Tuple<string[], string>( new [] { "ServiceType", "GUID" },"GUID"),
                            new Tuple<string[], string>( new [] { "ServiceType", "Name" },"Name"),
                            new Tuple<string[], string>( new [] { "StartDate" },"StartDate"),
                            new Tuple<string[], string>( new [] { "EndDate" },"EndDate")
                        };

                    //var serviceRows = _preparedDataSet.Tables["Services"].Select($@"IsCapitalRepair = 0 and RegistryNumber = 50 and PaymentDocumentNumber = '{bill["PaymentDocumentNumber"].ToString()}'");
                    var houseRows = _preparedDataSet.Tables["House"].Select("CharterGUID = '" + charter["CharterGUID"].ToString() + "' and CharterVersionGUID = '" + charter["CharterVersionGUID"].ToString() + "'");
                    foreach (DataRow row in houseRows)
                    {
                        AddChildElement(row, "HomeManagementAsync", "importCharterRequest_co_hs", null, valuesServices, null, null, addedObj);
                    }

                    var addRows = _preparedDataSet.Tables["Add"].Select("CharterGUID = '" + charter["CharterGUID"].ToString() + "' and CharterVersionGUID = '" + charter["CharterVersionGUID"].ToString() + "'");
                    foreach (DataRow row in addRows)
                    {
                        AddChildElement(row, "HomeManagementAsync", "importCharterRequest_co_as", null, valuesServices, null, null, addedObj);
                    }
                }

                var values2 = new List<Tuple<string[], string>>
                {
                     new Tuple<string[], string>( new string[] { "CharterVersionGUID" },charter["CharterVersionGUID"].ToString())
                };

                AddChildElement(MrivcStatic.GisConfig.Services["HomeManagementAsync"].Methods["importCharterRequest_edit_end"].Template, values2, addedCharter);

                CounterCheck();
            }
        }

       
    }
}
