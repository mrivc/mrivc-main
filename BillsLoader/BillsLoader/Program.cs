﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BillsLoader.DM;
using BillsLoader.Logic;
using System.Text;
using BillsLoader.Properties;

namespace BillsLoader
{
    using MrivcHelpers.Helpers;

    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                //string conn = "Password=sr4kx54jj;PersistSecurityInfo=True;UserID=butorin;InitialCatalog=murmansk;DataSource=srv-mrivc6;UseProcedureforPrepare=1;AutoTranslate=True;PacketSize=4096;WorkstationID=Andrey;UseEncryptionforData=False;Tagwithcolumncollationwhenpossible=False;MARSConnection=False;DataTypeCompatibility=0;TrustServerCertificate=False";
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //Application.Run(new FrmLoad(conn)); /////////////////////// БДА 14.04.2020 Чтобы запустить прогу для дебага, раскомментировать строку, в conn добавить логин с паролем, не забыть убрать при релизе
                if (args.Length > 0)
                {
                    string[] prms = string.Concat(args).Split(new Char[] { '|' });
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new FrmLoad(prms[0]));
                }
                else
                {
                    DAccess.DataModule.SetSimpleConnectionString(Settings.Default.MainConnection);
                    LoadReestr();
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog(ex.Message + ex.StackTrace);
                FileHelper.WriteLog(" --- загрузка закончена --- ");
            }
        }

        private static void LoadReestr()
        {
            FileHelper.WriteLogEmptyLine();
            FileHelper.WriteLogEmptyLine();
            FileHelper.WriteLog(" --- загрузка начата --- ");
            ReestrLoader.ScanAndLoadAllReestr();
            FileHelper.WriteLog(" --- загрузка закончена --- ");
        }
    }
}
