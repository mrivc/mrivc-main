﻿namespace EllisBillFilter
{
    partial class HouseClientDetailFilter
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeHouseHierarhy1 = new EllisBillFilter.TreeHouseHierarhy();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.lblHousesWithoutBF = new System.Windows.Forms.Label();
            this.cbHouseWithoutBF = new DevExpress.XtraEditors.LookUpEdit();
            this.houseWithoutBillFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.lblAddress = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.baseGrid1 = new EllisBillFilter.BaseGrid();
            this.cmsClientDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miCheckUncheckFilterForHouse = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miGoToActiveFilters = new System.Windows.Forms.ToolStripMenuItem();
            this.clientDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Bill_Filter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riBillFilter = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.billFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Client_Detail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riClient = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.payReceiverCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Derive_Company = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riDeriveCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.deriveCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colID_Filter_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riFilterType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.filterTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colBegin_Period = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colEnd_Period = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdvance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoundTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrefix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShow_Meter_For_Services = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDerive_Company_Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHouseWithoutBF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.houseWithoutBillFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).BeginInit();
            this.cmsClientDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeriveCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFilterType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // treeHouseHierarhy1
            // 
            this.treeHouseHierarhy1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeHouseHierarhy1.Location = new System.Drawing.Point(0, 55);
            this.treeHouseHierarhy1.Name = "treeHouseHierarhy1";
            this.treeHouseHierarhy1.Size = new System.Drawing.Size(258, 396);
            this.treeHouseHierarhy1.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.lblHousesWithoutBF);
            this.splitContainerControl1.Panel1.Controls.Add(this.cbHouseWithoutBF);
            this.splitContainerControl1.Panel1.Controls.Add(this.treeHouseHierarhy1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.lblAddress);
            this.splitContainerControl1.Panel2.Controls.Add(this.label1);
            this.splitContainerControl1.Panel2.Controls.Add(this.baseGrid1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(786, 451);
            this.splitContainerControl1.SplitterPosition = 258;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // lblHousesWithoutBF
            // 
            this.lblHousesWithoutBF.AutoSize = true;
            this.lblHousesWithoutBF.Location = new System.Drawing.Point(3, 10);
            this.lblHousesWithoutBF.Name = "lblHousesWithoutBF";
            this.lblHousesWithoutBF.Size = new System.Drawing.Size(199, 13);
            this.lblHousesWithoutBF.TabIndex = 14;
            this.lblHousesWithoutBF.Text = "Перешедшие дома без кода фильтра:";
            // 
            // cbHouseWithoutBF
            // 
            this.cbHouseWithoutBF.Location = new System.Drawing.Point(6, 27);
            this.cbHouseWithoutBF.Name = "cbHouseWithoutBF";
            this.cbHouseWithoutBF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbHouseWithoutBF.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_House", "ID_House", 70, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("House_Address", "Адрес", 300, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Curr_Derive_Company", "Управляющая компания", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Prev_Derive_Company", "Prev_Derive_Company", 120, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Prev_ID_Company", "Prev_ID_Company", 100, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Curr_ID_Company", "Curr_ID_Company", 99, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Prev_Period", "Prev_Period", 68, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Curr_Period", "Curr_Period", 67, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Display_Address_Name", "Display_Address_Name", 250, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.cbHouseWithoutBF.Properties.DataSource = this.houseWithoutBillFilterBindingSource;
            this.cbHouseWithoutBF.Properties.DisplayMember = "Display_Address_Name";
            this.cbHouseWithoutBF.Properties.DropDownRows = 25;
            this.cbHouseWithoutBF.Properties.NullText = "";
            this.cbHouseWithoutBF.Properties.PopupWidth = 450;
            this.cbHouseWithoutBF.Properties.ValueMember = "ID_House";
            this.cbHouseWithoutBF.Size = new System.Drawing.Size(244, 20);
            this.cbHouseWithoutBF.TabIndex = 13;
            this.cbHouseWithoutBF.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cbHouseWithoutBF_EditValueChanging);
            // 
            // houseWithoutBillFilterBindingSource
            // 
            this.houseWithoutBillFilterBindingSource.DataMember = "HouseWithoutBillFilter";
            this.houseWithoutBillFilterBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAddress.Location = new System.Drawing.Point(94, 10);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(44, 13);
            this.lblAddress.TabIndex = 2;
            this.lblAddress.Text = "address";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Текущий адрес:";
            // 
            // baseGrid1
            // 
            this.baseGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.baseGrid1.ContextMenuStrip = this.cmsClientDetail;
            this.baseGrid1.DataSource = this.clientDetailBindingSource;
            this.baseGrid1.Location = new System.Drawing.Point(0, 30);
            this.baseGrid1.MainView = this.gridView1;
            this.baseGrid1.Name = "baseGrid1";
            this.baseGrid1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riClient,
            this.riDeriveCompany,
            this.riBillFilter,
            this.riPeriod,
            this.riFilterType});
            this.baseGrid1.Size = new System.Drawing.Size(522, 421);
            this.baseGrid1.TabIndex = 0;
            this.baseGrid1.TrimCellValueBeforeValidate = true;
            this.baseGrid1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // cmsClientDetail
            // 
            this.cmsClientDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCheckUncheckFilterForHouse,
            this.toolStripSeparator1,
            this.miGoToActiveFilters});
            this.cmsClientDetail.Name = "cmsClientDetail";
            this.cmsClientDetail.Size = new System.Drawing.Size(246, 76);
            // 
            // miCheckUncheckFilterForHouse
            // 
            this.miCheckUncheckFilterForHouse.Image = global::EllisBillFilter.Properties.Resources.check;
            this.miCheckUncheckFilterForHouse.Name = "miCheckUncheckFilterForHouse";
            this.miCheckUncheckFilterForHouse.Size = new System.Drawing.Size(245, 22);
            this.miCheckUncheckFilterForHouse.Text = "Установить/Cнять код фильтра";
            this.miCheckUncheckFilterForHouse.Click += new System.EventHandler(this.miCheckUncheckFilterForHouse_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(242, 6);
            // 
            // miGoToActiveFilters
            // 
            this.miGoToActiveFilters.Image = global::EllisBillFilter.Properties.Resources.filter_enable;
            this.miGoToActiveFilters.Name = "miGoToActiveFilters";
            this.miGoToActiveFilters.Size = new System.Drawing.Size(245, 22);
            this.miGoToActiveFilters.Text = "Перейти к коду фильтра";
            this.miGoToActiveFilters.Click += new System.EventHandler(this.miGoToActiveFilters_Click);
            // 
            // clientDetailBindingSource
            // 
            this.clientDetailBindingSource.DataMember = "ClientDetail";
            this.clientDetailBindingSource.DataSource = this.mDataSet;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIsChecked,
            this.colID_Bill_Filter,
            this.colID_Client_Detail,
            this.colID_Client,
            this.colID_Derive_Company,
            this.colID_Filter_Type,
            this.colBegin_Period,
            this.colEnd_Period,
            this.colAdvance,
            this.colRoundTo,
            this.colPrefix,
            this.colShow_Meter_For_Services,
            this.colComment,
            this.colDerive_Company_Code});
            this.gridView1.GridControl = this.baseGrid1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            // 
            // colID_Bill_Filter
            // 
            this.colID_Bill_Filter.Caption = "Код фильтра";
            this.colID_Bill_Filter.ColumnEdit = this.riBillFilter;
            this.colID_Bill_Filter.FieldName = "ID_Bill_Filter";
            this.colID_Bill_Filter.Name = "colID_Bill_Filter";
            this.colID_Bill_Filter.Visible = true;
            this.colID_Bill_Filter.VisibleIndex = 1;
            // 
            // riBillFilter
            // 
            this.riBillFilter.AutoHeight = false;
            this.riBillFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riBillFilter.DataSource = this.billFilterBindingSource;
            this.riBillFilter.DisplayMember = "Bill_Filter";
            this.riBillFilter.Name = "riBillFilter";
            this.riBillFilter.NullText = "";
            this.riBillFilter.ValueMember = "ID_Bill_Filter";
            // 
            // billFilterBindingSource
            // 
            this.billFilterBindingSource.DataMember = "BillFilter";
            this.billFilterBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Client_Detail
            // 
            this.colID_Client_Detail.Caption = "ИД";
            this.colID_Client_Detail.FieldName = "ID_Client_Detail";
            this.colID_Client_Detail.Name = "colID_Client_Detail";
            this.colID_Client_Detail.OptionsColumn.ReadOnly = true;
            // 
            // colID_Client
            // 
            this.colID_Client.Caption = "Получатель платежа";
            this.colID_Client.ColumnEdit = this.riClient;
            this.colID_Client.FieldName = "ID_Client";
            this.colID_Client.Name = "colID_Client";
            this.colID_Client.Visible = true;
            this.colID_Client.VisibleIndex = 2;
            // 
            // riClient
            // 
            this.riClient.AutoHeight = false;
            this.riClient.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riClient.DataSource = this.payReceiverCompanyBindingSource;
            this.riClient.DisplayMember = "Name";
            this.riClient.Name = "riClient";
            this.riClient.NullText = "";
            this.riClient.ValueMember = "ID_Company";
            // 
            // payReceiverCompanyBindingSource
            // 
            this.payReceiverCompanyBindingSource.DataMember = "PayReceiverCompany";
            this.payReceiverCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Derive_Company
            // 
            this.colID_Derive_Company.Caption = "Управляющая компания";
            this.colID_Derive_Company.ColumnEdit = this.riDeriveCompany;
            this.colID_Derive_Company.FieldName = "ID_Derive_Company";
            this.colID_Derive_Company.Name = "colID_Derive_Company";
            this.colID_Derive_Company.Visible = true;
            this.colID_Derive_Company.VisibleIndex = 3;
            // 
            // riDeriveCompany
            // 
            this.riDeriveCompany.AutoHeight = false;
            this.riDeriveCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riDeriveCompany.DataSource = this.deriveCompanyBindingSource;
            this.riDeriveCompany.DisplayMember = "Name";
            this.riDeriveCompany.Name = "riDeriveCompany";
            this.riDeriveCompany.NullText = "";
            this.riDeriveCompany.ValueMember = "ID_Company";
            // 
            // deriveCompanyBindingSource
            // 
            this.deriveCompanyBindingSource.DataMember = "DeriveCompany";
            this.deriveCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // colID_Filter_Type
            // 
            this.colID_Filter_Type.Caption = "Тип кода фильтра";
            this.colID_Filter_Type.ColumnEdit = this.riFilterType;
            this.colID_Filter_Type.FieldName = "ID_Filter_Type";
            this.colID_Filter_Type.Name = "colID_Filter_Type";
            this.colID_Filter_Type.Visible = true;
            this.colID_Filter_Type.VisibleIndex = 6;
            // 
            // riFilterType
            // 
            this.riFilterType.AutoHeight = false;
            this.riFilterType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riFilterType.DataSource = this.filterTypeBindingSource;
            this.riFilterType.DisplayMember = "Name";
            this.riFilterType.Name = "riFilterType";
            this.riFilterType.NullText = "";
            this.riFilterType.ValueMember = "ID_Filter_Type";
            // 
            // filterTypeBindingSource
            // 
            this.filterTypeBindingSource.DataMember = "FilterType";
            this.filterTypeBindingSource.DataSource = this.mDataSet;
            // 
            // colBegin_Period
            // 
            this.colBegin_Period.Caption = "Действует С";
            this.colBegin_Period.ColumnEdit = this.riPeriod;
            this.colBegin_Period.FieldName = "Begin_Period";
            this.colBegin_Period.Name = "colBegin_Period";
            this.colBegin_Period.Visible = true;
            this.colBegin_Period.VisibleIndex = 4;
            // 
            // riPeriod
            // 
            this.riPeriod.AutoHeight = false;
            this.riPeriod.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riPeriod.DataSource = this.periodBindingSource;
            this.riPeriod.DisplayMember = "Name";
            this.riPeriod.Name = "riPeriod";
            this.riPeriod.NullText = "";
            this.riPeriod.ValueMember = "Period";
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // colEnd_Period
            // 
            this.colEnd_Period.Caption = "Действует ПО";
            this.colEnd_Period.ColumnEdit = this.riPeriod;
            this.colEnd_Period.FieldName = "End_Period";
            this.colEnd_Period.Name = "colEnd_Period";
            this.colEnd_Period.Visible = true;
            this.colEnd_Period.VisibleIndex = 5;
            // 
            // colAdvance
            // 
            this.colAdvance.Caption = "Аванс";
            this.colAdvance.FieldName = "Advance";
            this.colAdvance.Name = "colAdvance";
            // 
            // colRoundTo
            // 
            this.colRoundTo.Caption = "Тип округления";
            this.colRoundTo.FieldName = "RoundTo";
            this.colRoundTo.Name = "colRoundTo";
            // 
            // colPrefix
            // 
            this.colPrefix.Caption = "Префикс реестров";
            this.colPrefix.FieldName = "Prefix";
            this.colPrefix.Name = "colPrefix";
            // 
            // colShow_Meter_For_Services
            // 
            this.colShow_Meter_For_Services.Caption = "Открыть ввод показаний для услуг";
            this.colShow_Meter_For_Services.FieldName = "Show_Meter_For_Services";
            this.colShow_Meter_For_Services.Name = "colShow_Meter_For_Services";
            // 
            // colComment
            // 
            this.colComment.Caption = "Комментарий";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            // 
            // colDerive_Company_Code
            // 
            this.colDerive_Company_Code.Caption = "Код УК";
            this.colDerive_Company_Code.FieldName = "Derive_Company_Code";
            this.colDerive_Company_Code.Name = "colDerive_Company_Code";
            this.colDerive_Company_Code.Visible = true;
            this.colDerive_Company_Code.VisibleIndex = 7;
            // 
            // colIsChecked
            // 
            this.colIsChecked.Caption = "Действует";
            this.colIsChecked.FieldName = "IsChecked";
            this.colIsChecked.Name = "colIsChecked";
            this.colIsChecked.Visible = true;
            this.colIsChecked.VisibleIndex = 0;
            // 
            // HouseClientDetailFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "HouseClientDetailFilter";
            this.Size = new System.Drawing.Size(786, 451);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbHouseWithoutBF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.houseWithoutBillFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).EndInit();
            this.cmsClientDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeriveCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFilterType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TreeHouseHierarhy treeHouseHierarhy1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private BaseGrid baseGrid1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource clientDetailBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client_Detail;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riClient;
        private System.Windows.Forms.BindingSource payReceiverCompanyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Derive_Company;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riDeriveCompany;
        private System.Windows.Forms.BindingSource deriveCompanyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Bill_Filter;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riBillFilter;
        private System.Windows.Forms.BindingSource billFilterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBegin_Period;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEnd_Period;
        private DevExpress.XtraGrid.Columns.GridColumn colAdvance;
        private DevExpress.XtraGrid.Columns.GridColumn colRoundTo;
        private DevExpress.XtraGrid.Columns.GridColumn colPrefix;
        private DevExpress.XtraGrid.Columns.GridColumn colShow_Meter_For_Services;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Filter_Type;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riFilterType;
        private System.Windows.Forms.BindingSource filterTypeBindingSource;
        private System.Windows.Forms.ContextMenuStrip cmsClientDetail;
        private System.Windows.Forms.ToolStripMenuItem miGoToActiveFilters;
        private System.Windows.Forms.ToolStripMenuItem miCheckUncheckFilterForHouse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit cbHouseWithoutBF;
        private System.Windows.Forms.BindingSource houseWithoutBillFilterBindingSource;
        private System.Windows.Forms.Label lblHousesWithoutBF;
        private DevExpress.XtraGrid.Columns.GridColumn colDerive_Company_Code;
        private DevExpress.XtraGrid.Columns.GridColumn colIsChecked;
    }
}
