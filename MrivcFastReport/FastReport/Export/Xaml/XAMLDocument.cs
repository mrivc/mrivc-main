﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;
namespace FastReport.Export.XAML
{
    /// <summary>
    /// Contains Dashes enum
    /// </summary>
    public enum Dashes
    {
        /// <summary>
        /// Specifies the Dash.
        /// </summary>
        Dash,

        /// <summary>
        /// Specifies the Dot.
        /// </summary>
        Dot,

        /// <summary>
        /// Specifies the DashDot.
        /// </summary>
        DashDot,

        /// <summary>
        /// Specifies the DashDotDot.
        /// </summary>
        DashDotDot,

        /// <summary>
        /// Specifies the Double line.
        /// </summary>
        Double
    }

    /// <summary>
    /// 
    /// </summary>
    public class XAMLDocument
    {
        private XmlAttribute nsAttribute;
        private XmlElement root;
        private XmlElement Grid;
        private XmlElement Resources;
        private XmlElement Canvas;
        private XmlDocument doc = new XmlDocument();

        /// <summary>
        /// Create Window.
        /// </summary>
        public void CreateWindow(string name, float Width, float Height)
        {
            root = doc.CreateElement("Page");

            nsAttribute = doc.CreateAttribute("xmlns");
            nsAttribute.Value = "http://schemas.microsoft.com/winfx/2006/xaml/presentation";
            root.Attributes.Append(nsAttribute);

            nsAttribute = doc.CreateAttribute("xmlns", "x", "http://www.w3.org/2000/xmlns/");
            nsAttribute.Value = "http://schemas.microsoft.com/winfx/2006/xaml";
            root.Attributes.Append(nsAttribute);

            nsAttribute = doc.CreateAttribute("x", "Name", "http://schemas.microsoft.com/winfx/2006/xaml");
            nsAttribute.Value = "Window";
            root.Attributes.Append(nsAttribute);

            nsAttribute = doc.CreateAttribute("Title");
            nsAttribute.Value = "MainWindow";
            root.Attributes.Append(nsAttribute);
            
            if (Width == 0)
            {
                Width = 640;
            }
            if (Height == 0)
            {
                Height = 480;    
            }
                nsAttribute = doc.CreateAttribute("Width");
                nsAttribute.Value = FloatToString(Width);
                root.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("Height");
                nsAttribute.Value = FloatToString(Height);
                root.Attributes.Append(nsAttribute);     
           
            doc.AppendChild(root);
        }

        ///<summary>
        ///Create Grid.
        /// </summary>
        public void AddGrid()
        {
            Grid = doc.CreateElement("Grid");
            nsAttribute = doc.CreateAttribute("x", "Name", "http://schemas.microsoft.com/winfx/2006/xaml");
            nsAttribute.Value = "LayoutRoot";

            Grid.Attributes.Append(nsAttribute);
            root.AppendChild(Grid);
        }

        ///<summary>
        ///Create Canvas.
        /// </summary>
        public void AddCanvas()
        {
            Canvas = doc.CreateElement("Canvas");
            root.AppendChild(Canvas);
        }

        ///<summary>
        ///Create Resources tag
        ///</summary>
        private void AddResources()
        {
            Resources = doc.CreateElement("Page.Resources");
            root.AppendChild(Resources);
        }

        ///<summary>
        ///Add resource for TextObject
        ///</summary>
        public void AddResourceTextObject()
        {
            XmlElement Style = doc.CreateElement("Style");
            nsAttribute = doc.CreateAttribute("TargetType");
            nsAttribute.Value = "TextBox";
            Style.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("x", "Key", "http://schemas.microsoft.com/winfx/2006/xaml");
            nsAttribute.Value = "rTxt";
            Style.Attributes.Append(nsAttribute);
            Resources.AppendChild(Style);

            XmlElement Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "Background";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "{x:Null}";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "BorderBrush";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Black";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "BorderThickness";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "0";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "FontSize";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "10pt";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "FontFamily";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Arial";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "Padding";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "2,0,2,0";
            Setter.Attributes.Append(nsAttribute);            
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "TextWrapping";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Wrap";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "IsHitTestVisible";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "false";
            Setter.Attributes.Append(nsAttribute);            
            Style.AppendChild(Setter);
            
        }

        ///<summary>
        ///Add resource for TextObject with angle
        ///</summary>
        public void AddResourceTextObjectAngle()
        {
            XmlElement Style = doc.CreateElement("Style");
            nsAttribute = doc.CreateAttribute("TargetType");
            nsAttribute.Value = "UserControl";
            Style.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("x", "Key", "http://schemas.microsoft.com/winfx/2006/xaml");
            nsAttribute.Value = "rTxtE";
            Style.Attributes.Append(nsAttribute);
            Resources.AppendChild(Style);

            XmlElement Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "BorderBrush";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Black";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "FontSize";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "10pt";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "FontFamily";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Arial";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "Padding";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "2,0,2,0";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);

            Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "IsHitTestVisible";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "False";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);
        }

        ///<summary>
        ///Add resource for Line
        ///</summary>
        public void AddResourceLine()
        {
            XmlElement Style = doc.CreateElement("Style");
            nsAttribute = doc.CreateAttribute("TargetType");
            nsAttribute.Value = "Line";
            Style.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("x", "Key", "http://schemas.microsoft.com/winfx/2006/xaml");
            nsAttribute.Value = "rLn";
            Style.Attributes.Append(nsAttribute);
            Resources.AppendChild(Style);

            XmlElement Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "Stroke";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Black";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);
        }

        ///<summary>
        ///Add resource for Rectangle
        ///</summary>
        public void AddResourceRectangle()
        {
            XmlElement Style = doc.CreateElement("Style");
            nsAttribute = doc.CreateAttribute("TargetType");
            nsAttribute.Value = "Rectangle";
            Style.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("x", "Key", "http://schemas.microsoft.com/winfx/2006/xaml");
            nsAttribute.Value = "rRc";
            Style.Attributes.Append(nsAttribute);
            Resources.AppendChild(Style);

            XmlElement Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "Stroke";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Black";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);
        }

        ///<summary>
        ///Add resource for Ellipse
        ///</summary>
        public void AddResourceEllipse()
        {
            XmlElement Style = doc.CreateElement("Style");
            nsAttribute = doc.CreateAttribute("TargetType");
            nsAttribute.Value = "Ellipse";
            Style.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("x", "Key", "http://schemas.microsoft.com/winfx/2006/xaml");
            nsAttribute.Value = "rEl";
            Style.Attributes.Append(nsAttribute);
            Resources.AppendChild(Style);

            XmlElement Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "Stroke";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Black";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);
        }

        ///<summary>
        ///Add resource for Polygon
        ///</summary>
        public void AddResourcePolygon()
        {
            XmlElement Style = doc.CreateElement("Style");
            nsAttribute = doc.CreateAttribute("TargetType");
            nsAttribute.Value = "Polygon";
            Style.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("x", "Key", "http://schemas.microsoft.com/winfx/2006/xaml");
            nsAttribute.Value = "rPl";
            Style.Attributes.Append(nsAttribute);
            Resources.AppendChild(Style);

            XmlElement Setter = doc.CreateElement("Setter");
            nsAttribute = doc.CreateAttribute("Property");
            nsAttribute.Value = "Stroke";
            Setter.Attributes.Append(nsAttribute);
            nsAttribute = doc.CreateAttribute("Value");
            nsAttribute.Value = "Black";
            Setter.Attributes.Append(nsAttribute);
            Style.AppendChild(Setter);
        }

        ///<summary>
        ///Method for add TextObject.
        /// </summary>
        public void AddTextObject(float MarginLeft, float MarginTop, float Width, float Height,
        string HorizontalAlignment, string VerticalAlignment, string BorderBrush, float BorderThickness,//
        float LeftLine, float TopLine, float RightLine, float BottomLine, string LeftLineDashStile,
        string TopLineDashStile, string RightLineDashStile, string BottomLineDashStile, string colorLeftLine,
        string colorTopLine, string colorRightLine, string colorBottomLine, bool Shadow, string ShadowColor, float ShadowWidth, string Background, string BorderLines,
        string Text, float FontSize, string Foreground, string FontFamily, bool Bold, bool Italic,bool Underline,
        float PaddingLeft, float PaddingTop, float PaddingRight, float PaddingBottom, bool WordWrap, bool Glass, string colorTop)
        {
            //----Create TextBox--------------
            XmlElement TextBox = doc.CreateElement("TextBox");

            nsAttribute = doc.CreateAttribute("Style");
            nsAttribute.Value = "{StaticResource rTxt}";
            TextBox.Attributes.Append(nsAttribute);

            //attribute 1 Margin
            MarginLeft = MarginLeft + BorderThickness;
            MarginTop =  MarginTop + BorderThickness;
            if (MarginLeft != 0 || MarginTop != 0)
            {
                nsAttribute = doc.CreateAttribute("Margin");
                nsAttribute.Value = FloatToString(MarginLeft) + "," + FloatToString(MarginTop);
                TextBox.Attributes.Append(nsAttribute);
            }
            Width = Width + BorderThickness;
            Height = Height + BorderThickness;
            //attribute 2 Width
            if (Width != 0)
            {
                nsAttribute = doc.CreateAttribute("Width");
                nsAttribute.Value = FloatToString(Width);
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 3 Height
            if (Height != 0)
            {
                nsAttribute = doc.CreateAttribute("Height");
                nsAttribute.Value = FloatToString(Height);
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 4 HorizontalAlignment
            if (HorizontalAlignment != "Left" && HorizontalAlignment != null && HorizontalAlignment != "" && HorizontalAlignment != "Justify")
            {
                nsAttribute = doc.CreateAttribute("HorizontalAlignment");
                nsAttribute.Value = HorizontalAlignment;
                TextBox.Attributes.Append(nsAttribute);
            }

            //attribute 5 VerticalAlignment
            if (VerticalAlignment != "Top" && VerticalAlignment != null && VerticalAlignment != "")
            {
                nsAttribute = doc.CreateAttribute("VerticalAlignment");
                nsAttribute.Value = VerticalAlignment;
                TextBox.Attributes.Append(nsAttribute);
            }

            //attribute 6 BorderThickness
            string[] masLines = BorderLines.Split(',', ' ');//names lines borders 
            string AllBorderThickness = null;//set lines
            bool All = false;
            bool None = false;
            bool Left = false;
            bool Right = false;
            bool Top = false;
            bool Bottom = false;
            for (int i = 0; i < masLines.Length; i++)
            {
                if (masLines[i] == "All")
                {
                    All = true;
                }
                if (masLines[i] == "None")
                {
                    None = true;
                }
                if (masLines[i] == "Left")
                {
                    Left = true;
                }
                if (masLines[i] == "Right")
                {
                    Right = true;
                }
                if (masLines[i] == "Top")
                {
                    Top = true;
                }
                if (masLines[i] == "Bottom")
                {
                    Bottom = true;
                }
            }

            if(!None)
            {
                if (Left || All)
                {
                    if (LeftLineDashStile != "Solid" && LeftLineDashStile != "Double")
                        LeftLine = 0;
                    else
                        if (LeftLine == 0) LeftLine = 1;
                    AllBorderThickness = FloatToString(LeftLine);
                }
                else if (!Left)
                {
                    LeftLine = 0;
                    AllBorderThickness = "0";
                }

                if (Top || All)
                {
                    if (TopLineDashStile != "Solid" && TopLineDashStile != "Double")
                        TopLine = 0;
                    else
                        if (TopLine == 0) TopLine = 1;
                    AllBorderThickness = AllBorderThickness + "," + FloatToString(TopLine);
                }
                else if (!Top)
                {
                    TopLine = 0;
                    AllBorderThickness = AllBorderThickness + "," + "0";
                }

                if (Right || All)
                {
                    if (RightLineDashStile != "Solid" && RightLineDashStile != "Double")
                        RightLine = 0;
                    else
                        if (RightLine == 0) RightLine = 1;
                    AllBorderThickness = AllBorderThickness + "," + FloatToString(RightLine);
                }
                else if (!Right)
                {
                    RightLine = 0;
                    AllBorderThickness = AllBorderThickness + "," + "0";
                }

                if (Bottom || All)
                {
                    if (BottomLineDashStile != "Solid" && BottomLineDashStile != "Double")
                        BottomLine = 0;
                    else
                        if (BottomLine == 0) BottomLine = 1;
                    AllBorderThickness = AllBorderThickness + "," + FloatToString(BottomLine);
                }
                else if (!Bottom)
                {
                    BottomLine = 0;
                    AllBorderThickness = AllBorderThickness + "," + "0";
                }
            }
            if (LeftLine == TopLine && TopLine == RightLine && RightLine == BottomLine && BottomLine == LeftLine)
                AllBorderThickness = FloatToString(BottomLine);//Convert.ToString(BottomLine);
            if (!None && (Left || Top || Right || Bottom || All) && !(LeftLine == 0 && TopLine == 0 && RightLine == 0 && BottomLine == 0))
                  {
                       nsAttribute = doc.CreateAttribute("BorderThickness");
                       nsAttribute.Value = AllBorderThickness;
                       TextBox.Attributes.Append(nsAttribute);
                   }
                  //Dash---
                  if (Left || All)
                  {
                      if (LeftLineDashStile == "Dash")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft, MarginTop + Height, colorLeftLine, BorderThickness, colorLeftLine, Dashes.Dash);
                      }
                      if (LeftLineDashStile == "Dot")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft, MarginTop + Height, colorLeftLine, BorderThickness, colorLeftLine, Dashes.Dot);
                      }
                      if (LeftLineDashStile == "DashDot")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft, MarginTop + Height, colorLeftLine, BorderThickness, colorLeftLine, Dashes.DashDot);
                      }
                      if (LeftLineDashStile == "DashDotDot")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft, MarginTop + Height, colorLeftLine, BorderThickness, colorLeftLine, Dashes.DashDotDot);
                      }
                      if (LeftLineDashStile == "Double")
                      {
                          AddLine(MarginLeft + 4, MarginTop + 4, MarginLeft + 4, MarginTop + Height + 4, colorLeftLine, BorderThickness, colorLeftLine);
                      }
                  }

                  if (Top || All)
                  {
                      if (TopLineDashStile == "Dash")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft + Width, MarginTop, colorTopLine, BorderThickness, colorTopLine, Dashes.Dash);
                      }
                      if (TopLineDashStile == "Dot")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft + Width, MarginTop, colorTopLine, BorderThickness, colorTopLine, Dashes.Dot);
                      }
                      if (TopLineDashStile == "DashDot")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft + Width, MarginTop, colorTopLine, BorderThickness, colorTopLine, Dashes.DashDot);
                      }
                      if (TopLineDashStile == "DashDotDot")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft + Width, MarginTop, colorTopLine, BorderThickness, colorTopLine, Dashes.DashDotDot);
                      }
                      if (TopLineDashStile == "Double")
                      {
                          AddLine(MarginLeft, MarginTop, MarginLeft + Width + 4, MarginTop + 4, colorTopLine, BorderThickness, colorTopLine);
                      }
                  }

                  if (Right || All)
                  {
                      if (RightLineDashStile == "Dash")
                      {
                          AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine, Dashes.Dash);
                      }
                      if (RightLineDashStile == "Dot")
                      {
                          AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine, Dashes.Dot);
                      }
                      if (RightLineDashStile == "DashDot")
                      {
                          AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine, Dashes.DashDot);
                      }
                      if (RightLineDashStile == "DashDotDot")
                      {
                          AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine, Dashes.DashDotDot);
                      }
                      if (RightLineDashStile == "Double")
                      {
                          AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine);
                      }
                  }

                  if (Bottom || All)
                  {
                      if (BottomLineDashStile == "Dash")
                      {
                          AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine, Dashes.Dash);
                      }
                      if (BottomLineDashStile == "Dot")
                      {
                          AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine, Dashes.Dot);
                      }
                      if (BottomLineDashStile == "DashDot")
                      {
                          AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine, Dashes.DashDot);
                      }
                      if (BottomLineDashStile == "DashDotDot")
                      {
                          AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine, Dashes.DashDotDot);
                      }
                      if (BottomLineDashStile == "Double")
                      {
                          AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine);
                      }
                  }
            //Shadow-------------------
                   if (Shadow)
                   {
                       AddLine(MarginLeft + ShadowWidth, MarginTop + Height + ShadowWidth/2, MarginLeft + Width + ShadowWidth, MarginTop + Height + ShadowWidth/2, ShadowColor, ShadowWidth, ShadowColor);

                       AddLine(MarginLeft + Width + ShadowWidth/2, MarginTop + ShadowWidth, MarginLeft + Width + ShadowWidth/2, MarginTop + Height + ShadowWidth, ShadowColor, ShadowWidth, ShadowColor);
                   }
                   //attribute 7 BorderBrush
                   if (!None && BorderBrush != "#ff000000" && BorderBrush != null && BorderBrush != "Black")
                   {
                       nsAttribute = doc.CreateAttribute("BorderBrush");
                       nsAttribute.Value = BorderBrush;
                       TextBox.Attributes.Append(nsAttribute);
                   }

                   //attribute 8 Background
                   if (Glass)
                   {
                       XmlElement TextBoxBackground = doc.CreateElement("TextBox.Background");
                       TextBox.AppendChild(TextBoxBackground);

                       XmlElement LinearGradientBrush = doc.CreateElement("LinearGradientBrush");
                       nsAttribute = doc.CreateAttribute("StartPoint");
                       nsAttribute.Value = "0,0";
                       LinearGradientBrush.Attributes.Append(nsAttribute);
                       nsAttribute = doc.CreateAttribute("EndPoint");
                       nsAttribute.Value = "0,1";
                       LinearGradientBrush.Attributes.Append(nsAttribute);
                       TextBoxBackground.AppendChild(LinearGradientBrush);

                       XmlElement GradientStop = doc.CreateElement("GradientStop");
                       nsAttribute = doc.CreateAttribute("Color");
                       nsAttribute.Value = colorTop;
                       GradientStop.Attributes.Append(nsAttribute);
                       nsAttribute = doc.CreateAttribute("Offset");
                       nsAttribute.Value = "0.5";
                       GradientStop.Attributes.Append(nsAttribute);
                       LinearGradientBrush.AppendChild(GradientStop);

                       GradientStop = doc.CreateElement("GradientStop");
                       nsAttribute = doc.CreateAttribute("Color");
                       nsAttribute.Value = Background;
                       GradientStop.Attributes.Append(nsAttribute);
                       nsAttribute = doc.CreateAttribute("Offset");
                       nsAttribute.Value = "0.5";
                       GradientStop.Attributes.Append(nsAttribute);
                       LinearGradientBrush.AppendChild(GradientStop);
                   }
                   else
                   if (Background != null)
                   {
                    nsAttribute = doc.CreateAttribute("Background");
                    nsAttribute.Value = Background;
                    TextBox.Attributes.Append(nsAttribute);
                   }
            //attribute 9 Text
            if (Text != null && Text != "")
            {
                nsAttribute = doc.CreateAttribute("Text");
                nsAttribute.Value = Text;
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 10 FontSize
            if (FontSize != 0 && FontSize != 10)
            {
                nsAttribute = doc.CreateAttribute("FontSize");
                nsAttribute.Value = FloatToString(FontSize) + "pt";
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 11 Foreground
            if (Foreground != null && Foreground != "" && Foreground != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("Foreground");
                nsAttribute.Value = Foreground;
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 12 HorizontalContentAlignment
            if (HorizontalAlignment != "Left" && HorizontalAlignment != null && HorizontalAlignment != "" && HorizontalAlignment != "Justify")
            {
                nsAttribute = doc.CreateAttribute("HorizontalContentAlignment");
                nsAttribute.Value = HorizontalAlignment;
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 13 VerticalContentAlignment
            if (VerticalAlignment != "Top" && VerticalAlignment != null && VerticalAlignment != "")
            {
                nsAttribute = doc.CreateAttribute("VerticalContentAlignment");
                nsAttribute.Value = VerticalAlignment;
                TextBox.Attributes.Append(nsAttribute);
            }

            //attribute 14 FontFamily
            if (FontFamily != "Arial" && FontFamily != null && FontFamily != "")
            {
                nsAttribute = doc.CreateAttribute("FontFamily");
                nsAttribute.Value = FontFamily;
                TextBox.Attributes.Append(nsAttribute);
            }

            //attribute 15 FontWeight
            if (Bold)
            {
                nsAttribute = doc.CreateAttribute("FontWeight");
                nsAttribute.Value = "Bold";
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 16 Italic
            if (Italic)
            {
                nsAttribute = doc.CreateAttribute("FontStyle");
                nsAttribute.Value = "Italic";
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 17 Underline
            if (Underline)
            {
                nsAttribute = doc.CreateAttribute("TextDecorations");
                nsAttribute.Value = "Underline";
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 18 Paddings
            if (PaddingLeft != 2 || PaddingTop != 0 || PaddingRight != 2 || PaddingBottom != 0)
            {
                string Paddings;
                if (PaddingLeft == 0 && PaddingTop == 0 && PaddingRight == 0 && PaddingBottom == 0) Paddings = Convert.ToString(0);
                else if (PaddingLeft == PaddingTop && PaddingLeft == PaddingRight && PaddingLeft == PaddingBottom && PaddingLeft != 0) Paddings = FloatToString(PaddingLeft);//Convert.ToString(PaddingLeft);
                else
                Paddings = FloatToString(PaddingLeft) + "," + FloatToString(PaddingTop) + ","
                    + FloatToString(PaddingRight) + "," + FloatToString(PaddingBottom);
               
                nsAttribute = doc.CreateAttribute("Padding");
                nsAttribute.Value = Paddings;
                TextBox.Attributes.Append(nsAttribute);
            }
            //attribute 20 WordWrap           
                if (!WordWrap)
                {
                    nsAttribute = doc.CreateAttribute("TextWrapping");
                    nsAttribute.Value = "NoWrap";
                    TextBox.Attributes.Append(nsAttribute);
                }
            //Grid or Canvas------**-*-*-*-*-*-*-*-+*-+*-*+-*
            Canvas.AppendChild(TextBox);
        }

        ///<summary>
        ///Method for add TextObject with angle
        /// </summary>
        public void AddTextObject(float MarginLeft, float MarginTop, float Width, float Height,
        string HorizontalAlignment, string VerticalAlignment, string BorderBrush, float BorderThickness,
        float LeftLine, float TopLine, float RightLine, float BottomLine, string LeftLineDashStile,
        string TopLineDashStile, string RightLineDashStile, string BottomLineDashStile, string colorLeftLine,
        string colorTopLine, string colorRightLine, string colorBottomLine, bool Shadow, string ShadowColor, float ShadowWidth, string Background, string BorderLines,
        string Text, float FontSize, string Foreground, string FontFamily, bool Bold, bool Italic, bool Underline,
        float PaddingLeft, float PaddingTop, float PaddingRight, float PaddingBottom, bool WordWrap, float Angle, bool Glass, string colorTop)
        {
            //----Create UserControl--------------
            XmlElement UserControl = doc.CreateElement("UserControl");

            nsAttribute = doc.CreateAttribute("Style");
            nsAttribute.Value = "{StaticResource rTxtE}";
            UserControl.Attributes.Append(nsAttribute);

            //attribute 1 Margin
            if (MarginLeft != 0 || MarginTop != 0)
            {
                nsAttribute = doc.CreateAttribute("Margin");
                nsAttribute.Value = FloatToString(MarginLeft) + "," + FloatToString(MarginTop);
                UserControl.Attributes.Append(nsAttribute);
            }
            //attribute 2 Width
            if (Width != 0)
            {
                nsAttribute = doc.CreateAttribute("Width");
                nsAttribute.Value = FloatToString(Width);
                UserControl.Attributes.Append(nsAttribute);
            }
            //attribute 3 Height
            if (Height != 0)
            {
                nsAttribute = doc.CreateAttribute("Height");
                nsAttribute.Value = FloatToString(Height);
                UserControl.Attributes.Append(nsAttribute);
            }
            //attribute 4 HorizontalAlignment
            if (HorizontalAlignment != "Left" && HorizontalAlignment != null && HorizontalAlignment != "" && HorizontalAlignment != "Justify")
            {
                nsAttribute = doc.CreateAttribute("HorizontalAlignment");
                nsAttribute.Value = HorizontalAlignment;
                UserControl.Attributes.Append(nsAttribute);
            }

            //attribute 5 VerticalAlignment
            if (VerticalAlignment != "Top" && VerticalAlignment != null && VerticalAlignment != "")
            {
                nsAttribute = doc.CreateAttribute("VerticalAlignment");
                nsAttribute.Value = VerticalAlignment;
                UserControl.Attributes.Append(nsAttribute);
            }
            
            //attribute 6 BorderThickness
            string[] masLines = BorderLines.Split(',', ' ');//names lines borders 
            string AllBorderThickness = null;//set lines
            bool All = false;
            bool None = false;
            bool Left = false;
            bool Right = false;
            bool Top = false;
            bool Bottom = false;
            for (int i = 0; i < masLines.Length; i++)
            {
                if (masLines[i] == "All")
                {
                    All = true;
                }
                if (masLines[i] == "None")
                {
                    None = true;
                }
                if (masLines[i] == "Left")
                {
                    Left = true;
                }
                if (masLines[i] == "Right")
                {
                    Right = true;
                }
                if (masLines[i] == "Top")
                {
                    Top = true;
                }
                if (masLines[i] == "Bottom")
                {
                    Bottom = true;
                }
            }
            if (LeftLine == 0 && TopLine == 0 && RightLine == 0 && BottomLine == 0)
                AllBorderThickness = Convert.ToString(0);

            else if (LeftLine == TopLine && LeftLine == RightLine && LeftLine == BottomLine && LeftLine != 0) 
                AllBorderThickness = FloatToString(LeftLine);//Convert.ToString(LeftLine);

            else if (!None)
            {
                if (Left || All)
                {
                    if (LeftLineDashStile != "Solid" && LeftLineDashStile != "Double")
                        LeftLine = 0;
                    else
                        if (LeftLine == 0) LeftLine = 1;
                    AllBorderThickness = FloatToString(LeftLine);
                }
                else if (!Left)
                {
                    LeftLine = 0;
                    AllBorderThickness = "0";
                }

                if (Top || All)
                {
                    if (TopLineDashStile != "Solid" && TopLineDashStile != "Double")
                        TopLine = 0;
                    else
                        if (TopLine == 0) TopLine = 1;
                    AllBorderThickness = AllBorderThickness + "," + FloatToString(TopLine);
                }
                else if (!Top)
                {
                    TopLine = 0;
                    AllBorderThickness = AllBorderThickness + "," + "0";
                }

                if (Right || All)
                {
                    if (RightLineDashStile != "Solid" && RightLineDashStile != "Double")
                        RightLine = 0;
                    else
                        if (RightLine == 0) RightLine = 1;
                    AllBorderThickness = AllBorderThickness + "," + FloatToString(RightLine);
                }
                else if (!Right)
                {
                    RightLine = 0;
                    AllBorderThickness = AllBorderThickness + "," + "0";
                }

                if (Bottom || All)
                {
                    if (BottomLineDashStile != "Solid" && BottomLineDashStile != "Double")
                        BottomLine = 0;
                    else
                        if (BottomLine == 0) BottomLine = 1;
                    AllBorderThickness = AllBorderThickness + "," + FloatToString(BottomLine);
                }
                else if (!Bottom)
                {
                    BottomLine = 0;
                    AllBorderThickness = AllBorderThickness + "," + "0";
                }
            }
            if (LeftLine == 0 && TopLine == 0 && RightLine == 0 && BottomLine == 0) AllBorderThickness = "0";
            if (LeftLine == TopLine && TopLine == RightLine && RightLine == BottomLine && BottomLine == LeftLine)
                AllBorderThickness = FloatToString(BottomLine);//Convert.ToString(BottomLine);
            if (!None && (Left || Top || Right || Bottom || All) && !(LeftLine == 0 && TopLine == 0 && RightLine == 0 && BottomLine == 0))
                   {
                       nsAttribute = doc.CreateAttribute("BorderThickness");
                       nsAttribute.Value = AllBorderThickness;
                       UserControl.Attributes.Append(nsAttribute);
                   }

                   //attribute 7 BorderBrush
                   if (!None && BorderBrush != null && BorderBrush != "Black")
            {
                 nsAttribute = doc.CreateAttribute("BorderBrush");
                 nsAttribute.Value = BorderBrush;
                 UserControl.Attributes.Append(nsAttribute);
            }
            
                //Dash---
                   if (Left || All)
                   {
                       if (LeftLineDashStile == "Dash")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft, MarginTop + Height, colorLeftLine, BorderThickness, colorLeftLine, Dashes.Dash);
                       }
                       if (LeftLineDashStile == "Dot")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft, MarginTop + Height, colorLeftLine, BorderThickness, colorLeftLine, Dashes.Dot);
                       }
                       if (LeftLineDashStile == "DashDot")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft, MarginTop + Height, colorLeftLine, BorderThickness, colorLeftLine, Dashes.DashDot);
                       }
                       if (LeftLineDashStile == "DashDotDot")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft, MarginTop + Height, colorLeftLine, BorderThickness, colorLeftLine, Dashes.DashDotDot);
                       }
                       if (LeftLineDashStile == "Double")
                       {
                           AddLine(MarginLeft + 4, MarginTop + 4, MarginLeft + 4, MarginTop + Height + 4, colorLeftLine, BorderThickness, colorLeftLine);
                       }
                   }

                   if (Top || All)
                   {
                       if (TopLineDashStile == "Dash")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft + Width, MarginTop, colorTopLine, BorderThickness, colorTopLine, Dashes.Dash);
                       }
                       if (TopLineDashStile == "Dot")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft + Width, MarginTop, colorTopLine, BorderThickness, colorTopLine, Dashes.Dot);
                       }
                       if (TopLineDashStile == "DashDot")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft + Width, MarginTop, colorTopLine, BorderThickness, colorTopLine, Dashes.DashDot);
                       }
                       if (TopLineDashStile == "DashDotDot")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft + Width, MarginTop, colorTopLine, BorderThickness, colorTopLine, Dashes.DashDotDot);
                       }
                       if (TopLineDashStile == "Double")
                       {
                           AddLine(MarginLeft, MarginTop, MarginLeft + Width + 4, MarginTop + 4, colorTopLine, BorderThickness, colorTopLine);
                       }
                   }

                   if (Right || All)
                   {
                       if (RightLineDashStile == "Dash")
                       {
                           AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine, Dashes.Dash);
                       }
                       if (RightLineDashStile == "Dot")
                       {
                           AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine, Dashes.Dot);
                       }
                       if (RightLineDashStile == "DashDot")
                       {
                           AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine, Dashes.DashDot);
                       }
                       if (RightLineDashStile == "DashDotDot")
                       {
                           AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine, Dashes.DashDotDot);
                       }
                       if (RightLineDashStile == "Double")
                       {
                           AddLine(MarginLeft + Width, MarginTop, MarginLeft + Width, MarginTop + Height, colorRightLine, BorderThickness, colorRightLine);
                       }
                   }

                   if (Bottom || All)
                   {
                       if (BottomLineDashStile == "Dash")
                       {
                           AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine, Dashes.Dash);
                       }
                       if (BottomLineDashStile == "Dot")
                       {
                           AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine, Dashes.Dot);
                       }
                       if (BottomLineDashStile == "DashDot")
                       {
                           AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine, Dashes.DashDot);
                       }
                       if (BottomLineDashStile == "DashDotDot")
                       {
                           AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine, Dashes.DashDotDot);
                       }
                       if (BottomLineDashStile == "Double")
                       {
                           AddLine(MarginLeft, MarginTop + Height, MarginLeft + Width, MarginTop + Height, colorBottomLine, BorderThickness, colorBottomLine);
                       }
                   }
                //Shadow-------------------
                if (Shadow)
                {
                    AddLine(MarginLeft + ShadowWidth, MarginTop + Height + ShadowWidth / 2, MarginLeft + Width + ShadowWidth, MarginTop + Height + ShadowWidth / 2, ShadowColor, ShadowWidth, ShadowColor);

                    AddLine(MarginLeft + Width + ShadowWidth / 2, MarginTop + ShadowWidth, MarginLeft + Width + ShadowWidth / 2, MarginTop + Height + ShadowWidth, ShadowColor, ShadowWidth, ShadowColor);
                }

            //attribute 8 Background
            {
                if (Glass)
                {
                    XmlElement UserControlBackground = doc.CreateElement("UserControl.Background");
                    UserControl.AppendChild(UserControlBackground);

                    XmlElement LinearGradientBrush = doc.CreateElement("LinearGradientBrush");
                    nsAttribute = doc.CreateAttribute("StartPoint");
                    nsAttribute.Value = "0,0";
                    LinearGradientBrush.Attributes.Append(nsAttribute);
                    nsAttribute = doc.CreateAttribute("EndPoint");
                    nsAttribute.Value = "0,1";
                    LinearGradientBrush.Attributes.Append(nsAttribute);
                    UserControlBackground.AppendChild(LinearGradientBrush);

                    XmlElement GradientStop = doc.CreateElement("GradientStop");
                    nsAttribute = doc.CreateAttribute("Color");
                    nsAttribute.Value = colorTop;
                    GradientStop.Attributes.Append(nsAttribute);
                    nsAttribute = doc.CreateAttribute("Offset");
                    nsAttribute.Value = "0.5";
                    GradientStop.Attributes.Append(nsAttribute);
                    LinearGradientBrush.AppendChild(GradientStop);

                    GradientStop = doc.CreateElement("GradientStop");
                    nsAttribute = doc.CreateAttribute("Color");
                    nsAttribute.Value = Background;
                    GradientStop.Attributes.Append(nsAttribute);
                    nsAttribute = doc.CreateAttribute("Offset");
                    nsAttribute.Value = "0.5";
                    GradientStop.Attributes.Append(nsAttribute);
                    LinearGradientBrush.AppendChild(GradientStop);
                }
                else
                if (Background != null && Background == "")
                {
                    nsAttribute = doc.CreateAttribute("Background");
                    nsAttribute.Value = Background;
                    UserControl.Attributes.Append(nsAttribute);
                }
            }

            //attribute 9 Text
            if (Text != null && Text != "")
            {
                XmlElement EndoText = doc.CreateElement("TextBox");

                nsAttribute = doc.CreateAttribute("Style");
                nsAttribute.Value = "{StaticResource rTxt}";
                EndoText.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("Text");
                nsAttribute.Value = Text;
                EndoText.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("Width");
                nsAttribute.Value = FloatToString(Width-Math.Max(Math.Max(LeftLine,TopLine), Math.Max(RightLine,BottomLine)));
                EndoText.Attributes.Append(nsAttribute);

                //attribute 10 FontSize
                if (FontSize != 0 && FontSize != 10)
                {
                    nsAttribute = doc.CreateAttribute("FontSize");
                    nsAttribute.Value = FloatToString(FontSize) + "pt";
                    EndoText.Attributes.Append(nsAttribute);
                }
                //attribute 11 Foreground
                if (Foreground != null && Foreground != "" && Foreground != "#ff000000")
                {
                    nsAttribute = doc.CreateAttribute("Foreground");
                    nsAttribute.Value = Foreground;
                    EndoText.Attributes.Append(nsAttribute);
                }

                //attribute 14 FontFamily
                if (FontFamily != "Arial" && FontFamily != null && FontFamily != "")
                {
                    nsAttribute = doc.CreateAttribute("FontFamily");
                    nsAttribute.Value = FontFamily;
                    EndoText.Attributes.Append(nsAttribute);
                }

                //attribute 15 FontWeight
                if (Bold)
                {
                    nsAttribute = doc.CreateAttribute("FontWeight");
                    nsAttribute.Value = "Bold";
                    EndoText.Attributes.Append(nsAttribute);
                }
                //attribute 16 Italic
                if (Italic)
                {
                    nsAttribute = doc.CreateAttribute("FontStyle");
                    nsAttribute.Value = "Italic";
                    EndoText.Attributes.Append(nsAttribute);
                }
                //attribute 17 Underline
                if (Underline)
                {
                    nsAttribute = doc.CreateAttribute("TextDecorations");
                    nsAttribute.Value = "Underline";
                    EndoText.Attributes.Append(nsAttribute);
                }
                //attribute 18 Paddings
                if (!(PaddingLeft == 2 && PaddingTop == 0 && PaddingRight == 2 & PaddingBottom == 0))
                {
                    string Paddings;
                    if (PaddingLeft == 0 && PaddingTop == 0 && PaddingRight == 0 && PaddingBottom == 0) Paddings = Convert.ToString(0);
                    else if (PaddingLeft == PaddingTop && PaddingLeft == PaddingRight && PaddingLeft == PaddingBottom && PaddingLeft != 0) Paddings = FloatToString(PaddingLeft);//Convert.ToString(PaddingLeft);
                    else
                    Paddings = FloatToString(PaddingLeft) + "," + FloatToString(PaddingTop) + ","
                        + FloatToString(PaddingRight) + "," + FloatToString(PaddingBottom);
                            
                    nsAttribute = doc.CreateAttribute("Padding");
                    nsAttribute.Value = Paddings;
                    EndoText.Attributes.Append(nsAttribute);
                }

                //attribute 20 WordWrap
                    if (!WordWrap)
                    {
                        nsAttribute = doc.CreateAttribute("TextWrapping");
                        nsAttribute.Value = "NoWrap";
                        EndoText.Attributes.Append(nsAttribute);
                    }
                UserControl.AppendChild(EndoText);

                XmlElement TextBoxTransform = doc.CreateElement("TextBox.LayoutTransform");
                EndoText.AppendChild(TextBoxTransform);

                XmlElement TransformGroup = doc.CreateElement("TransformGroup");
                TextBoxTransform.AppendChild(TransformGroup);

                XmlElement RotateTransform = doc.CreateElement("RotateTransform");

                nsAttribute = doc.CreateAttribute("Angle");
                nsAttribute.Value = FloatToString(Angle);
                RotateTransform.Attributes.Append(nsAttribute);

                TransformGroup.AppendChild(RotateTransform);
            }
            //Grid or Canvas------**-*-*-*-*-*-*-*-+*-+*-*+-*
            Canvas.AppendChild(UserControl);
        }

        ///<summary>
        ///Method for add line.
        /// </summary>
        public void AddLine(float MarginLeft, float MarginTop, float X2, float Y2, string Stroke, float StrokeThickness,
            string Fill)
        {
            XmlElement Line = doc.CreateElement("Line");

            nsAttribute = doc.CreateAttribute("Style");
            nsAttribute.Value = "{StaticResource rLn}";
            Line.Attributes.Append(nsAttribute);

            //attribute ----------------------------------
            if (MarginLeft != 0)
            {
                nsAttribute = doc.CreateAttribute("X1");
                nsAttribute.Value = FloatToString(MarginLeft);
                Line.Attributes.Append(nsAttribute);
            }
            //attribute-----------------------------------
            if (MarginTop != 0)
            {
                nsAttribute = doc.CreateAttribute("Y1");
                nsAttribute.Value = FloatToString(MarginTop);
                Line.Attributes.Append(nsAttribute);
            }
           
            //attribute 2----------------------------------
            if (X2 != 0)
            {
                nsAttribute = doc.CreateAttribute("X2");
                nsAttribute.Value = FloatToString(X2);
                Line.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Y2 != 0)
            {
                nsAttribute = doc.CreateAttribute("Y2");
                nsAttribute.Value = FloatToString(Y2);
                Line.Attributes.Append(nsAttribute);
            }
           
            //attribute 6--------------------------------------------
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("Stroke");
                nsAttribute.Value = Stroke;
                Line.Attributes.Append(nsAttribute);
            }
            //attribute 7----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("StrokeThickness");
                nsAttribute.Value = FloatToString(StrokeThickness);
                Line.Attributes.Append(nsAttribute);
            } 
            Canvas.AppendChild(Line);
        }

        ///<summary>
        ///Method for add line with dash.
        /// </summary>
        public void AddLine(float MarginLeft, float MarginTop, float X2, float Y2, string Stroke, float StrokeThickness,
            string Fill, Dashes dash)
        {
            XmlElement Line = doc.CreateElement("Line");

            nsAttribute = doc.CreateAttribute("Style");
            nsAttribute.Value = "{StaticResource rLn}";
            Line.Attributes.Append(nsAttribute);

            //attribute ----------------------------------
            if (MarginLeft != 0)
            {
                nsAttribute = doc.CreateAttribute("X1");
                nsAttribute.Value = FloatToString(MarginLeft);
                Line.Attributes.Append(nsAttribute);
            }
            //attribute-----------------------------------
            if (MarginTop != 0)
            {
                nsAttribute = doc.CreateAttribute("Y1");
                nsAttribute.Value = FloatToString(MarginTop);
                Line.Attributes.Append(nsAttribute);
            }

            //attribute 2----------------------------------
            if (X2 != 0)
            {
                nsAttribute = doc.CreateAttribute("X2");
                nsAttribute.Value = FloatToString(X2);
                Line.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Y2 != 0)
            {
                nsAttribute = doc.CreateAttribute("Y2");
                nsAttribute.Value = FloatToString(Y2);
                Line.Attributes.Append(nsAttribute);
            }

            //attribute 6--------------------------------------------
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("Stroke");
                nsAttribute.Value = Stroke;
                Line.Attributes.Append(nsAttribute);
            }
            //attribute 7----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("StrokeThickness");
                nsAttribute.Value = FloatToString(StrokeThickness);
                Line.Attributes.Append(nsAttribute);
            }
            //attribute 8-----------------------------------------------
            string StrokeDashArray = null;
            if (dash == Dashes.Dash)
            {
                StrokeDashArray = "3";
            }
            if (dash == Dashes.Dot)
            {
                StrokeDashArray = "1";
            }
            if (dash == Dashes.DashDot)
            {
                StrokeDashArray = "1 3 1";
            }
            if (dash == Dashes.DashDotDot)
            {
                StrokeDashArray = "1 1 3 1 1";
            }
            if (StrokeDashArray != null)
            {
                nsAttribute = doc.CreateAttribute("StrokeDashArray");
                nsAttribute.Value = StrokeDashArray;
                Line.Attributes.Append(nsAttribute);
            }
            
            Canvas.AppendChild(Line);
        }
        /// <summary>
        /// Method to add rectangle.
        /// </summary>
        public void AddRectangle(float MarginLeft, float MarginTop, float Width, float Height,
                                  string Stroke, float StrokeThickness, string Fill, bool Rounded)
        {
            XmlElement Rectangle = doc.CreateElement("Rectangle");

            nsAttribute = doc.CreateAttribute("Style");
            nsAttribute.Value = "{StaticResource rRc}";
            Rectangle.Attributes.Append(nsAttribute);

            //attribute 1------------------------------
            if (MarginLeft != 0 || MarginTop != 0)
            {
                nsAttribute = doc.CreateAttribute("Margin");
                nsAttribute.Value = FloatToString(MarginLeft + StrokeThickness) + "," + FloatToString(MarginTop + StrokeThickness);
                Rectangle.Attributes.Append(nsAttribute);
            }
            //attribute 2----------------------------------
            if (Width != 0)
            {
                nsAttribute = doc.CreateAttribute("Width");
                nsAttribute.Value = FloatToString(Width);
                Rectangle.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Height != 0)
            {
                nsAttribute = doc.CreateAttribute("Height");
                nsAttribute.Value = FloatToString(Height);
                Rectangle.Attributes.Append(nsAttribute);
            }
            //attribute 4--------------------------------------------
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("Stroke");
                nsAttribute.Value = Stroke;
                Rectangle.Attributes.Append(nsAttribute);
            }
            //attribute 5----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("StrokeThickness");
                nsAttribute.Value = FloatToString(StrokeThickness);
                Rectangle.Attributes.Append(nsAttribute);
            }
            //attribute 6-----------------------------------------------
            if (Fill != null)
            {
                nsAttribute = doc.CreateAttribute("Fill");
                nsAttribute.Value = Fill;
                Rectangle.Attributes.Append(nsAttribute);
            }
            //attribute 7 Rounded
            if (Rounded)
            {
                nsAttribute = doc.CreateAttribute("RadiusX");
                nsAttribute.Value = "10";
                Rectangle.Attributes.Append(nsAttribute);

                nsAttribute = doc.CreateAttribute("RadiusY");
                nsAttribute.Value = "10";
                Rectangle.Attributes.Append(nsAttribute);
            }
            Canvas.AppendChild(Rectangle);
        }

        /// <summary>
        /// Method for add ellips.
        /// </summary>
        public void AddEllipse(float MarginLeft, float MarginTop, float Width, float Height,
             string Stroke, float StrokeThickness, string Fill)
        {
            XmlElement Ellipse = doc.CreateElement("Ellipse");

            nsAttribute = doc.CreateAttribute("Style");
            nsAttribute.Value = "{StaticResource rEl}";
            Ellipse.Attributes.Append(nsAttribute);

            //attribute 1------------------------------
            if (MarginLeft != 0 || MarginTop != 0)
            {
                nsAttribute = doc.CreateAttribute("Margin");
                nsAttribute.Value = FloatToString(MarginLeft + StrokeThickness) + "," + FloatToString(MarginTop + StrokeThickness);
                Ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 2----------------------------------
            if (Width != 0)
            {
                nsAttribute = doc.CreateAttribute("Width");
                nsAttribute.Value = FloatToString(Width);
                Ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Height != 0)
            {
                nsAttribute = doc.CreateAttribute("Height");
                nsAttribute.Value = FloatToString(Height);
                Ellipse.Attributes.Append(nsAttribute);
            }          
            //attribute 4--------------------------------------------
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("Stroke");
                nsAttribute.Value = Stroke;
                Ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 5----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("StrokeThickness");
                nsAttribute.Value = FloatToString(StrokeThickness);
                Ellipse.Attributes.Append(nsAttribute);
            }
            //attribute 6-----------------------------------------------
            if (Fill != null)
            {
                nsAttribute = doc.CreateAttribute("Fill");
                nsAttribute.Value = Fill;
                Ellipse.Attributes.Append(nsAttribute);
            }

            Canvas.AppendChild(Ellipse);
        }

        /// <summary>
        /// Method for add triangle.
        /// </summary>
        public void AddTriangle(float MarginLeft, float MarginTop, float Width, float Height,
             string Stroke, float StrokeThickness, string Fill)
        {
            XmlElement Polygon = doc.CreateElement("Polygon");

            nsAttribute = doc.CreateAttribute("Style");
            nsAttribute.Value = "{StaticResource rPl}";
            Polygon.Attributes.Append(nsAttribute);

            //attribute 1------------------------------
            if (MarginLeft != 0 || MarginTop != 0)
            {
                nsAttribute = doc.CreateAttribute("Margin");
                nsAttribute.Value = FloatToString(MarginLeft /*+ StrokeThickness*/) + "," + FloatToString(MarginTop/* + StrokeThickness*/);
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 2----------------------------------
            if (Width != 0)
            {
                nsAttribute = doc.CreateAttribute("Width");
                nsAttribute.Value = FloatToString(Width);
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Height != 0)
            {
                nsAttribute = doc.CreateAttribute("Height");
                nsAttribute.Value = FloatToString(Height);
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 4--------------------------------------------

            {
                string Points;
                float x1 = Width/2; 
                float y1 = 0;
                float x2 = Width; 
                float y2 = Height;
                float x3 = 0; 
                float y3 = Height;
                Points = FloatToString(x1) + "," + FloatToString(y1) + "," + FloatToString(x2) + "," + FloatToString(y2) + "," + FloatToString(x3) + "," + FloatToString(y3);
                nsAttribute = doc.CreateAttribute("Points");
                nsAttribute.Value = Points;
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 5----------------------------------------------
            if (Stroke != null && Stroke != "Black" && Stroke != "#ff000000")
            {
                nsAttribute = doc.CreateAttribute("Stroke");
                nsAttribute.Value = Stroke;
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 6-----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("StrokeThickness");
                nsAttribute.Value = FloatToString(StrokeThickness);
                Polygon.Attributes.Append(nsAttribute);
            }
        
            if (Fill != null)
            {
                nsAttribute = doc.CreateAttribute("Fill");
                nsAttribute.Value = Fill;
                Polygon.Attributes.Append(nsAttribute);
            }

            Canvas.AppendChild(Polygon);
        }

        /// <summary>
        /// Method for add Diamond.
        /// </summary>
        public void AddDiamond(float MarginLeft, float MarginTop, float Width, float Height,
             string Stroke, float StrokeThickness, string Fill)
        {
            XmlElement Polygon = doc.CreateElement("Polygon");

            nsAttribute = doc.CreateAttribute("Style");
            nsAttribute.Value = "{StaticResource rPl}";
            Polygon.Attributes.Append(nsAttribute);

            //attribute 1------------------------------
            if (MarginLeft != 0 || MarginTop != 0)
            {
                nsAttribute = doc.CreateAttribute("Margin");
                nsAttribute.Value = FloatToString(MarginLeft/* + StrokeThickness*/) + "," + FloatToString(MarginTop/* + StrokeThickness*/);
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 2----------------------------------
            if (Width != 0)
            {
                nsAttribute = doc.CreateAttribute("Width");
                nsAttribute.Value = FloatToString(Width);
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 3------------------------------------
            if (Height != 0)
            {
                nsAttribute = doc.CreateAttribute("Height");
                nsAttribute.Value = FloatToString(Height);
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 4--------------------------------------------

            {
                string Points;
                float x1 = Width/2; float y1 = 0;
                float x2 = Width; float y2 = Height/2;
                float x3 = Width/2; float y3 = Height;
                float x4 = 0; float y4 = Height/2;
                Points = FloatToString(x1) + "," + FloatToString(y1) + "," + FloatToString(x2) + "," + FloatToString(y2) + "," + FloatToString(x3) + "," + FloatToString(y3) + "," + FloatToString(x4) + "," + FloatToString(y4);
                nsAttribute = doc.CreateAttribute("Points");
                nsAttribute.Value = Points;
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 5----------------------------------------------
            if (Stroke != null)
            {
                nsAttribute = doc.CreateAttribute("Stroke");
                nsAttribute.Value = Stroke;
                Polygon.Attributes.Append(nsAttribute);
            }
            //attribute 6-----------------------------------------------
            if (StrokeThickness != 0 && StrokeThickness != 1)
            {
                nsAttribute = doc.CreateAttribute("StrokeThickness");
                nsAttribute.Value = FloatToString(StrokeThickness);
                Polygon.Attributes.Append(nsAttribute);
            }
        
            if (Fill != null)
            {
                nsAttribute = doc.CreateAttribute("Fill");
                nsAttribute.Value = Fill;
                Polygon.Attributes.Append(nsAttribute);
            }

            Canvas.AppendChild(Polygon);
        }

        /// <summary>
        /// Add image
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void AddImage(string filename, float left, float top, float width, float height)
        {
            if (!String.IsNullOrEmpty(filename))
            {
                XmlElement image = doc.CreateElement("Image");
                nsAttribute = doc.CreateAttribute("Source");
                nsAttribute.Value = filename;
                image.Attributes.Append(nsAttribute);
                if (left != 0 || top != 0)
                {
                    nsAttribute = doc.CreateAttribute("Margin");
                    nsAttribute.Value = FloatToString(left/* + StrokeThickness*/) + "," + FloatToString(top/* + StrokeThickness*/);
                    image.Attributes.Append(nsAttribute);
                }
                if (width != 0)
                {
                    nsAttribute = doc.CreateAttribute("Width");
                    nsAttribute.Value = FloatToString(width);
                    image.Attributes.Append(nsAttribute);
                }
                if (height != 0)
                {
                    nsAttribute = doc.CreateAttribute("Height");
                    nsAttribute.Value = FloatToString(height);
                    image.Attributes.Append(nsAttribute);
                }
                Canvas.AppendChild(image);
            }
        }

        private string FloatToString(double value)
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberGroupSeparator = String.Empty;
            provider.NumberDecimalSeparator = ".";
            return Convert.ToString(Math.Round(value, 2), provider);
        }

        /// <summary>
        /// Save xaml file.
        /// </summary>
        public void Save(string filename)
        {
            doc.Save(filename);
        }

        /// <summary>
        /// Save xaml stream.
        /// </summary>
        public void Save(Stream stream)
        {
            doc.Save(stream);
        }

        /// <param name="name"></param>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        public XAMLDocument(string name, float Width, float Height)
        {
            CreateWindow(name, Width, Height);
            AddResources();
            AddCanvas();           
        }

        /// <summary>
        /// 
        /// </summary>
        public XAMLDocument()
        {
        }
    }
}
