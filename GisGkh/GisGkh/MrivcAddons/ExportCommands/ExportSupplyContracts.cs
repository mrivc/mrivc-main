﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ExportSupplyContracts : MrivcExportCommand, ISingleHouseCommand
    {
        bool _isRSO;

        public string FiasHouseGuid { get; set; } = null;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                base.SenderID = value;
            }
        }

        public ExportSupplyContracts(string senderID) : base("exportSupplyResourceContract", "HomeManagementAsync", senderID, "exportSupplyResourceContractResult")
        {
            Description = "Экспорт договоров ресурсоснабжения";
        }

        public bool ContractExists(string senderID, string fiasHouseGuid)
        {
            var obj = DAccess.DataModule.ExecuteScalarQueryCommand1("select GIS.SupplyContractExists(@senderID,@fiasHouseGUID)", new[,] { { "@senderID", senderID }, { "@fiasHouseGUID", fiasHouseGuid } });
            return bool.Parse(obj.ToString());
        }

        new void ExecuteToTempDB()
        {
            //base.ExecuteToTempDB(new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) },_exportNodeName);
            base.ExecuteToTempDB(_exportNodeName);
        }

        public new void Execute()
        {
            DAccess.DataModule.ExecuteNonQueryCommand1(
                @"
                    delete from GIS.SupplyResourceContractDetail where SenderID = '" + SenderID + @"'
                    delete from GIS.SupplyResourceContract where SenderID = '" + SenderID + @"'
                    delete from GIS.SupplyResourceContractSubject where SenderID = '" + SenderID + @"'
                "
                );

            var isLastPage = false;
            string exportContractRootGUID = null;

            var templatePath = MrivcStatic.GisConfig.Services["HomeManagementAsync"].Methods["exportSupplyResourceContract_add_root"].Template;

            this.ExecuteToTempDB();

            if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
            {
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportSupplyResourceContractData", new object[,] { { "senderID", SenderID } });
            }

            var values = new List<Tuple<string[], string>>();
            values.Add(new Tuple<string[], string>(new string[] { "ExportContractRootGUID" }, ""));

            AddChildElement(templatePath, values, "exportSupplyResourceContractRequest");

            while (!isLastPage)
            {
                try
                {
                    exportContractRootGUID = DAccess.DataModule.ExecuteScalarQueryCommand1("select ExportContractRootGUID from #exportSupplyResourceContractResult IF Object_ID(N'tempdb..#exportSupplyResourceContractResult') is not null drop table #exportSupplyResourceContractResult ").ToString();
                }
                catch (SqlException)
                {
                    exportContractRootGUID = null;

                    try
                    {
                        isLastPage = bool.Parse(DAccess.DataModule.ExecuteScalarQueryCommand1("select IsLastPage from #exportSupplyResourceContractResult IF Object_ID(N'tempdb..#exportSupplyResourceContractResult') is not null drop table #exportSupplyResourceContractResult ").ToString());
                    }
                    catch (SqlException){ return; }
                }


                if (!isLastPage || !(exportContractRootGUID == null))
                {
                    Values = new List<Tuple<string[], string>> { new Tuple<string[], string>(new string[] { "ExportContractRootGUID" }, exportContractRootGUID) };

                    this.ExecuteToTempDB();

                    if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
                    {
                        DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportSupplyResourceContractData", new object[,] { { "senderID", SenderID } });
                    }
                }
            }

            var exportSupplyContractsObjectAddress = new ExportSupplyContractsObjectAddress(SenderID);
            exportSupplyContractsObjectAddress.Execute();
        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            //return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.ExportHouseData", new object[,] { { "houseGUID", FiasHouseGuid } }, "Houses", "Entrance", "Premises");
            return new DataSet();
        }
    }
}
