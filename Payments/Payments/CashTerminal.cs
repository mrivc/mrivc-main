﻿using System;
using System.Collections.Generic;
using System.Text;
using DrvFRLib;
using Payments.DS;
using System.Windows.Forms;
using System.Threading;
using Payments.LogicClasses;

namespace Payments
{
    public enum PasswordType { 
        User = 0,
        SysAdmin = 30
    }

    public class CashTerminal
    {
        /// <summary>
        /// Событие срабатывает при очередном получении статуса терминала
        /// </summary>
        /// <param name="sender">Текущий объект терминала</param>
        /// <param name="e">EventArgs</param>
        public delegate void TerminalStatusUpdatedEventHandler(DrvFRClass terminal, string statusInfo);
        public event TerminalStatusUpdatedEventHandler TerminalECRStatusUpdatedEvent;


        /// <summary>
        /// Событие срабатывает при выполнении той или иной комманды (регистрация продажи, покупки, внесение денег в кассу и т.д.)
        /// </summary>
        /// <param name="terminal"></param>
        /// <param name="commandName"></param>
        public delegate void TerminalCommandComplitedEventHandler(DrvFRClass terminal, string commandName);
        public event TerminalCommandComplitedEventHandler TerminalCommandComplitedEvent;


        private int drawerNumber;
        private bool openDrawerAfterPayment;
        private OperationRegister operationRegister;
        private CashRegister cashRegister;
        private int timeoutBetweenPrintCopy;
        
        DrvFRClass fis;

        public CashTerminal()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
            {
                MessageBox.Show("Вы работаете в режиме симуляции кассы.", "Информация", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (!DAccess.Instance.CurrentUser.ReadOnlyPrivilegy)
            {
                fis = new DrvFRClass();
                Connect();
            }

            drawerNumber = Properties.Settings.Default.DrawerNumber;
            openDrawerAfterPayment = Properties.Settings.Default.OpenDrawerAfterPayment;
            timeoutBetweenPrintCopy = Properties.Settings.Default.TimeoutBetweenPrintCopy;
        }

        /// <summary>
        /// Доступ к методом и свойствам кассы
        /// </summary>
        public DrvFRClass Terminal {
            get {
                return fis;
            }
        }

        /// <summary>
        /// Номер текущей открытой сессии
        /// </summary>
        //public int SessionNumber
        //{
        //    get {
        //        UpdateECRTerminalStatus(true);
        //        return fis.SessionNumber + 1; 
        //    }
        //}

        public OperationRegister OperationRegister
        {
            get {
                if (operationRegister == null)
                    operationRegister = new OperationRegister(fis);
                return operationRegister;
            }
        }

        public CashRegister CashRegister
        {
            get
            {
                if (cashRegister == null)
                    cashRegister = new CashRegister(fis);
                return cashRegister;
            }
        }

        public bool RequestPassword(PasswordType passwordType, string operationName)
        {
            if (operationName.Length == 0)
                operationName = "Запрос пароля";

            FrmPassword frm = new FrmPassword(passwordType, operationName);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int pass;
                if (int.TryParse(frm.Password, out pass))
                {
                    fis.Password = pass;
                    if (passwordType == PasswordType.SysAdmin && pass != fis.SysAdminPassword)
                    {
                        MessageBox.Show("Ошибка ввода пароля, повторите ввод.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else if (passwordType == PasswordType.User && pass != DAccess.Instance.CurrentUser.Fiscal_User_Password)
                    {
                        MessageBox.Show("Ошибка ввода пароля, повторите ввод.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                    return true;
                }
                else
                {
                    MessageBox.Show("Ошибка ввода пароля, повторите ввод.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return false;
        }

        private void DoTerminalCommandComplitedEvent(string commandName)
        {
            if (DAccess.Instance.CurrentUser.ReadOnlyPrivilegy)
                return;

            if (TerminalCommandComplitedEvent != null)
                TerminalCommandComplitedEvent(fis, commandName);
        }

        public string Connect()
        {
            fis.Password = 30;
            fis.ComNumber = Properties.Settings.Default.KKTComPort;
            fis.Timeout = Properties.Settings.Default.KKTTimeout;
            fis.BaudRate = Properties.Settings.Default.KKTBaudRate;
            fis.ComputerName = SystemInformation.ComputerName;
            fis.Connect();

            //UpdateECRTerminalStatus(true);
            return fis.ResultCode.ToString() + " - " + fis.ResultCodeDescription;
        }

        public string Connect2(int comNumber, int timeOut, int baudRate, string computerName)
        {
            fis.Password = 30;
            fis.ComNumber = comNumber;
            fis.Timeout = timeOut;
            fis.BaudRate = baudRate;
            fis.ComputerName = computerName;
            fis.Connect();

            Properties.Settings.Default.KKTBaudRate = baudRate;
            Properties.Settings.Default.KKTComPort = comNumber;
            Properties.Settings.Default.KKTTimeout = timeOut;
            Properties.Settings.Default.Save();

            //UpdateECRTerminalStatus(true);
            return fis.ResultCode.ToString() + " - " + fis.ResultCodeDescription;
        }

        #region Отчеты
        /// <summary>
        /// Печать суточного отчета с гашением
        /// </summary>
        public void PrintReportWithCleaning()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true) || !PaperExists())
                return;

            if (fis.ECRMode == 4) // Закрытая смена
            {
                MessageBox.Show("Состояние кассы: " + fis.ECRModeDescription + "\n\n"
                    + "Необходимо открыть смену, для этого выполните пункт меню \"Касса > Открыть смену\"",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!RequestPassword(PasswordType.SysAdmin, "Суточный отчет с гашением"))
                return;

            fis.PrintReportWithCleaning();
            DoTerminalCommandComplitedEvent("Печать суточного отчета с гашением");

            Thread.Sleep(2000);
            UpdateECRTerminalStatus(true);
        }

        /// <summary>
        /// Печать суточного отчета без гашения
        /// </summary>
        /// <param name="password">Пароль администратора кассы (задается в настройка кассы)</param>
        public void PrintReportWithoutCleaning()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true) || !PaperExists())
                return;

            if (!RequestPassword(PasswordType.SysAdmin, "Суточный отчет без гашения"))
                return;
            fis.PrintReportWithoutCleaning();
            DoTerminalCommandComplitedEvent("Печать суточного отчета без гашения");
        }

        /// <summary>
        /// Операция производит аннулирование (отмену) всего чека. При этом на чеке печатается «ЧЕК АННУЛИРОВАН»
        /// </summary>
        public void CancelCheck()
        {
            if (DAccess.Instance.CurrentUser.ReadOnlyPrivilegy || DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            UpdateECRTerminalStatus(false);

            if (fis.ECRMode != 8)
            {
                MessageBox.Show("Команда \"Отмена последнего чека\" работает только в режиме кассы - \"Открытый документ\" (ECRMode = 8).",
                    "Информация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!RequestPassword(PasswordType.SysAdmin, "Отмена последнего чека"))
                return;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.CancelCheck();

            UpdateECRTerminalStatus(false);

            DoTerminalCommandComplitedEvent("Выполнена отмена чека");
        }
        #endregion

        #region Проверки
        /// <summary>
        /// Проверки перед выполнением комманд терминала
        /// </summary>
        /// <param name="checkUserPrivilegy">Проверять или нет права пользователя</param>
        /// <returns></returns>
        public bool BeforeExecuteCommand(bool checkUserPrivilegy)
        {

            if (checkUserPrivilegy && !DAccess.Instance.CurrentUser.HasPriviligy())
                return false;

            if (Terminal.ResultCode == -1 || Terminal.ResultCode == -2 || Terminal.ResultCode == -3)
            {
                //    MessageBox.Show("Нет связи с кассой. Проверьте, включена ли касса.\nЕсли касса выключена, включите кассу и выполните пункт программы «Сервис – Обновить состояние кассы»",
                //        "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверка наличия бумаги в кассе
        /// </summary>
        /// <returns></returns>
        public bool PaperExists()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return true;

            UpdateECRTerminalStatus(false);
            if (fis.ECRAdvancedMode == 2) // Активное отсутствие бумаги
            {
                MessageBox.Show("Состояние кассы: " + fis.ECRAdvancedModeDescription + "\n\n"
                    + "В кассе закончилась бумага. Необходимо заправить рулон бумаги. После того как заправите новый рулон, перейдите в меню «Сервис» – «Продолжить печать».",
                    "Ошибка печати", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (fis.ECRAdvancedMode == 1) // Пассивное отсутствие бумаги
            {
                MessageBox.Show("Состояние кассы: " + fis.ECRAdvancedModeDescription + "\n\n"
                    + "В кассе закончилась бумага. Необходимо заправить рулон бумаги и повторить попытку последней операции.",
                    "Ошибка печати", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Проверка возможности операции продажи
        /// </summary>
        /// <returns>False - печать не возможна</returns>
        public bool CanPrintSale()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return true;

            if (!PaperExists())
                return false;

            UpdateECRTerminalStatus(true);

            if (fis.ECRMode == 3) // Открытая смена, 24 часа кончились
            {
                //MessageBox.Show("Состояние кассы: " + fis.ECRModeDescription + "\n\n"
                //    + "Необходимо закрыть смену, для этого напечатайте «Суточный отчет с гашением» (Касса > Суточный отчет с гашением)",
                //    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DAccess.Instance.HasErrors = true;
                return false;
            }

            if (fis.ECRMode == 4) // Закрытая смена
            {
                //MessageBox.Show("Состояние кассы: " + fis.ECRModeDescription + "\n\n"
                //    + "Необходимо открыть смену, для этого выполните пункт меню \"Касса > Открыть смену\"",
                //    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DAccess.Instance.HasErrors = true;
                return false;
            }

            if (fis.ECRMode != 2 && fis.ECRMode != 4 && fis.ECRMode != 7 && fis.ECRMode != 9)
            {
                //MessageBox.Show("Регистрация продаже не работает в режиме кассы: " + fis.ECRModeDescription,
                //    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DAccess.Instance.HasErrors = true;
                return false;
            }

            return true;
        }
        #endregion

        #region Комманды кассы

        /// <summary>
        /// Сбросить состояние ФН
        /// </summary>
        public void FNResetState()
        {
            if (DAccess.Instance.CurrentUser.ReadOnlyPrivilegy || DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!RequestPassword(PasswordType.SysAdmin, "Сброс состояния ФН"))
                return;

            fis.Password = fis.SysAdminPassword;
            fis.FNResetState();

            if (TerminalECRStatusUpdatedEvent != null)
                TerminalECRStatusUpdatedEvent(fis, "(" + fis.ECRMode + ") " + fis.ECRModeDescription);

            DoTerminalCommandComplitedEvent("Выполнен сброс состояния ФН");
        }

        public void UpdateECRTerminalStatus(bool isFullStatus)
        {
            if (DAccess.Instance.CurrentUser.ReadOnlyPrivilegy || DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            if (isFullStatus)
                fis.GetECRStatus();
            else
                fis.GetShortECRStatus();

            if (TerminalECRStatusUpdatedEvent != null)
                TerminalECRStatusUpdatedEvent(fis, "(" + fis.ECRMode + ") " + fis.ECRModeDescription);
        }

        /// <summary>
        /// Открытие смены
        /// </summary>
        public void OpenSession()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true))
                return;
            
            if (FNGetOfdMessageCount() > 0 && !DAccess.Instance.ProgramSettings.OFD_ALLOW_OPEN_SESSION)
            {
                MessageBox.Show("Запрещено открывать новую смену пока есть не отправленные данные в ОФД. Пожалуйста, обратитесь к системному администратору." , "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            UpdateECRTerminalStatus(false);
            if (fis.ECRMode == 2)
            {
                MessageBox.Show("Смена открыта, 24 часа не кончились.\nЧтобы открыть новую смену, касса должна находиться в статусе «Закрытая смена»", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (fis.ECRMode == 3)
            {
                MessageBox.Show("Смена открыта, 24 часа кончились.\nЧтобы открыть новую смену, касса должна находиться в статусе «Закрытая смена». Необходимо выполнить \"Суточный отчет с гашением (Z-отчет)\"", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (MessageBox.Show("Вы уверены, что хотите выполнить операцию открытия смены?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
                fis.OpenSession();
                UpdateECRTerminalStatus(true);
            }
            DoTerminalCommandComplitedEvent("Открытие смены");
        }

        /// <summary>
        /// Открыть денежный ящик.
        /// </summary>
        public void OpenDrawer()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true))
                return;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.DrawerNumber = drawerNumber;

            if (!fis.IsDrawerOpen)
                fis.OpenDrawer();
        }

        /// <summary>
        /// Прочитать значение поля настроечной таблицы
        /// </summary>
        /// <param name="tableNumber">Номер настроечной таблицы (1..15)</param>
        /// <param name="rowNumber">Номер ряда в таблице</param>
        /// <param name="fieldNumber">Номер поля в таблице</param>
        /// <returns></returns>
        public object ReadTableFieldValue(int tableNumber, int rowNumber, int fieldNumber)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return -1;

            if (!BeforeExecuteCommand(true))
                return -1;

            fis.TableNumber = 6;
            fis.RowNumber = 1;

                fis.ReadTable();

            fis.Password = fis.SysAdminPassword;
            fis.TableNumber = tableNumber;
            fis.RowNumber = rowNumber;
            fis.FieldNumber = fieldNumber;

            fis.ReadTable();

            if (IsStringField(tableNumber, fieldNumber))
                return fis.ValueOfFieldString;
            else
                return fis.ValueOfFieldInteger;
        }

        /// <summary>
        /// Записать значение в поле настроечной таблицы
        /// </summary>
        /// <param name="tableNumber">Номер настроечной таблицы (1..15)</param>
        /// <param name="rowNumber">Номер ряда в таблице</param>
        /// <param name="fieldNumber">Номер поля в таблице</param>
        /// <returns></returns>
        public void WriteTableFieldValue(int tableNumber, int rowNumber, int fieldNumber, string value)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true))
                return;

            fis.TableNumber = tableNumber;
            fis.RowNumber = rowNumber;
            fis.FieldNumber = fieldNumber;

            if (IsStringField(tableNumber, fieldNumber))
                fis.ValueOfFieldString = value;
            else
                fis.ValueOfFieldInteger = int.Parse(value);

            fis.WriteTable();

            DoTerminalCommandComplitedEvent("Записать значение в поле настроечной таблицы");
        }

        /// <summary>
        /// Отрезать чек
        /// </summary>
        private void CutCheck()
        {
            fis.StringForPrinting = string.Empty;
            fis.PrintString();
            fis.StringForPrinting = string.Empty;
            fis.PrintString();

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.CutType = true;
            fis.FeedLineCount = 1;
            fis.CutCheck();
        }

        public bool IsStringField(int tableNumber, int fieldNumber)
        {
            fis.TableNumber = tableNumber;
            fis.FieldNumber = fieldNumber;

            fis.GetFieldStruct();

            return fis.FieldType;
        }

        /// <summary>
        /// Регистрация продажи 
        /// </summary>
        /// <param name="price">Цена товара</param>
        /// <param name="cash">Наличные</param>
        /// <param name="quantity">Количество товара</param>
        /// <param name="firstString">Строка для печати</param>
        /// <param name="freeStrings">Дополнительные строки для вывода на печать в чеке (свободная информация)</param>
        /// <param name="printCopy">Печатать копию чека</param>
        /// <returns></returns>
        public bool SaleOld(decimal price, decimal cash, double quantity, 
            string firstString, List<string> freeStrings, int departmentNumber,
            bool printCopy)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return true;

            if (!BeforeExecuteCommand(true))
                return false;

            if (!CanPrintSale())
                return false;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.Price = price;
            fis.Quantity = quantity;
            fis.StringForPrinting = firstString;
            fis.Department = departmentNumber;
            fis.Sale();

            if (freeStrings != null)
                foreach (string str in freeStrings)
                {
                    fis.StringForPrinting = (str.Length > 50 ? str.Substring(0, 49) : str);
                    fis.PrintString();
                }

            fis.Summ1 = cash;
            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            
            fis.CloseCheck();

            if (printCopy)
            {
                Thread.Sleep(timeoutBetweenPrintCopy);
                fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
                fis.RepeatDocument();
            }

            DoTerminalCommandComplitedEvent("Регистрация продажи");

            return true;
        }

        /// <summary>
        /// Регистрация продажи 
        /// </summary>
        /// <param name="price">Цена товара</param>
        /// <param name="cash">Наличные</param>
        /// <param name="quantity">Количество товара</param>
        /// <param name="firstString">Строка для печати</param>
        /// <param name="freeStrings">Дополнительные строки для вывода на печать в чеке (свободная информация)</param>
        /// <param name="ofdProductName">Наименовани</param>
        /// <param name="printCopy">Печатать копию чека</param>
        /// <param name="taxGroup">Номер налоговой группы (1 - НДС 18%, 2 - НДС 10%, 3 - НДС 0%, 0 - БЕЗ НАЛОГА). Ставки налоговых групп хранятся в табл. 6 кассы.</param>
        /// <returns></returns>
        public bool Sale(decimal price, 
            decimal cash, 
            double quantity,
            string firstString, 
            List<string> freeStrings, 
            string ofdProductName, 
            string phoneEmail,
            int departmentNumber,
            bool printCopy,
            int taxGroup = 0)
        {
            var a =
            ReadTableFieldValue(6, taxGroup, 1).ToString();

            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return true;

            if (!BeforeExecuteCommand(true))
                return false;

            if (!CanPrintSale())
                return false;
            //taxGroup = 0;
            decimal taxValue = taxGroup == 0 ? 0 : decimal.Parse(ReadTableFieldValue(6, taxGroup, 1).ToString()) / 100 / 100;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;

            //Открываем чек продажи
            fis.CheckType = 0;
            fis.OpenCheck();

            //Отправка данных о покупателе 
            if (phoneEmail.Length > 0)
            {
                fis.CustomerEmail = phoneEmail;  //В качестве параметра можно передавать или телефон (+7хххххххххх) или e-mail: test@test.com  
                fis.FNSendCustomerEmail();
            }

            fis.Price = price;
            fis.Quantity = quantity;
            fis.StringForPrinting = firstString;
            fis.Department = departmentNumber;

            fis.Tax1 = taxGroup; // налоговая группа (0 - нет налоговой группы)
            fis.DiscountValue = 0; // скидка
            fis.ChargeValue = 0; // надбавка
            fis.DocumentName = ""; // имя скидки

            fis.StringForPrinting = "Сайт проверки ФПД: www.consumer.1-ofd.ru";
            fis.PrintString();

            fis.StringForPrinting = string.Empty;
            fis.PrintString();

            //Произвольная строка, в которой будет печататься полное наименование товара
            if (freeStrings != null)
                foreach (string str in freeStrings)
                {
                    fis.StringForPrinting = (str.Length > 50 ? str.Substring(0, 49) : str);
                    fis.PrintString();
                }

            //вычисляем сумму налога заложенного в товаре, чтобы напечатать на чеке
            if (taxGroup > 0)
            {
                decimal sumItog = price * (decimal)quantity;
                decimal taxSum = Math.Round((sumItog / (1 + taxValue) * taxValue), 2);
                fis.StringForPrinting = string.Format("в том числе НДС {0}% = {1}", taxValue*100, taxSum);
                fis.PrintString();

                fis.StringForPrinting = string.Empty;
                fis.PrintString();
            }


            fis.StringForPrinting = "//" + ofdProductName; //Сокращенное наименование товара (до 64 символов) для передачи на сервер ОФД без печати на чеке (//). 

            fis.FNDiscountOperation();

            //fis.Password = fis.SysAdminPassword;
            fis.CheckSubTotal();

            //fis.Summ1 = cash;



            //Безнал только по ЗН ККТ "001419"
            if (fis.SerialNumber == "001419")
            {
                fis.Summ2 = cash;
            }
            else
            {
                fis.Summ1 = cash;
            }




            fis.StringForPrinting = string.Empty;
            fis.CloseCheck();

            if (printCopy)
            {
                Thread.Sleep(timeoutBetweenPrintCopy);
                fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
                fis.RepeatDocument();
            }

            UpdateECRTerminalStatus(true);
            DoTerminalCommandComplitedEvent("Регистрация продажи");

            return true;
        }

        /// <summary>
        /// Операция возврата продажи
        /// Используется.
        /// </summary>
        /// <param name="price"></param>
        /// <param name="quantity"></param>
        /// <param name="departmentNumber"></param>
        /// <param name="firstString"></param>
        /// <returns></returns>
        public bool ReturnSale(decimal price, int quantity, int departmentNumber, string firstString)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return true;

            if (!BeforeExecuteCommand(true) || !PaperExists())
                return false;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.Quantity = quantity;
            fis.Price = price;
            fis.Department = departmentNumber;
            fis.StringForPrinting = firstString;
            fis.ReturnSale();

            fis.StringForPrinting = "";
            fis.Summ1 = price * quantity;
            fis.CloseCheck();

            DoTerminalCommandComplitedEvent("Возврат продажи");

            if (fis.ResultCode == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Продолжить печать
        /// </summary>
        public void ContinuePrint()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true) || !PaperExists())
                return;

            UpdateECRTerminalStatus(false);
            if (fis.ECRAdvancedMode == 3)
            {
                fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
                fis.ContinuePrint();
            }
            else
                MessageBox.Show("Данная команда работает только в режиме «После активного отсутствия бумаги» (после заправки нового рулона бумаги). Если вы только что заправили бумагу, то подождите 1-2 минуты и повторите данную команду.",
                    "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

            DoTerminalCommandComplitedEvent("Продолжить печать");
        }
        
        /// <summary>
        /// Печать дубликата последнего чека
        /// </summary>
        /// <returns></returns>
        public bool RepeatDocument()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return true;

            if (!BeforeExecuteCommand(true) || !PaperExists())
                return false;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.RepeatDocument();

            DoTerminalCommandComplitedEvent("Печать дубликата последнего чека");

            return true;
        }

        /// <summary>
        /// Выплата денег из кассы (инкасация)
        /// </summary>
        /// <param name="summ"></param>
        /// <returns></returns>
        public bool CashOutcome(decimal summ)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return true;

            if (!BeforeExecuteCommand(true))
                return false;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.Summ1 = summ;
            fis.CashOutcome();

            DoTerminalCommandComplitedEvent("Выплата денег из кассы (инкассация)");

            if (fis.ResultCode == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Внесение денег в кассу
        /// </summary>
        /// <param name="summ"></param>
        /// <returns></returns>
        public bool CashIncome(decimal summ)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return true;

            if (!BeforeExecuteCommand(true))
                return false;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.Summ1 = summ;
            fis.CashIncome();

            DoTerminalCommandComplitedEvent("Внесение денег в кассу");

            if (fis.ResultCode == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Сформировать чек коррекции ФН
        /// Чек коррекции может оформляться в случае необходимости скорректировать данные о продажах, переданные в ФНС. Чек коррекции может оформляться по предписанию инспектора налоговой службы.
        /// </summary>
        /// <param name="summ">Сумма</param>
        /// <param name="checkType">Тип чека:  «0» - продажа, «1» - покупка, «2» - возврат продажи, «3» - возврат покупки.</param>
        /// <returns></returns>
        public void FNBuildCorrectionReceipt(decimal summ, int checkType)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true))
                return;

            fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
            fis.CheckingType = checkType;
            fis.Summ1 = summ;
            fis.FNBuildCorrectionReceipt();

            UpdateECRTerminalStatus(true);
            DoTerminalCommandComplitedEvent("Чек коррекции ФН");
        }

        /// <summary>
        /// ФНЗапросСтатуса
        /// </summary>
        /// <returns></returns>
        public void FNGetStatus()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true))
                return;

            fis.Password = fis.SysAdminPassword;
            fis.FNGetStatus();
        }

        /// <summary>
        /// Синхронизация даты и времени кассы с системным временем ПК
        /// </summary>
        public void SetDateAndTime()
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!BeforeExecuteCommand(true))
                return;

            if (!RequestPassword(PasswordType.SysAdmin, "Синхронизация даты и времени"))
                return;

            fis.Password = fis.SysAdminPassword;
            fis.Date = DateTime.Now;
            fis.SetDate();
            fis.ConfirmDate();
            fis.Time = DateTime.Now;
            fis.SetTime();
            UpdateECRTerminalStatus(true);

            DoTerminalCommandComplitedEvent("Синхронизация даты и времени кассы с системным временем ПК");
        }
        #endregion

        #region ОФД

        /// <summary>
        /// Получить статус информационного обмена с ОФД
        /// </summary>
        /// <returns></returns>
        public string FNGetInfoExchangeStatus()
        {
            fis.Password = fis.SysAdminPassword;
            fis.FNGetInfoExchangeStatus();

            string result =
                CashTerminalDescriptor.GetInfoExchangeStatusFN(fis.InfoExchangeStatus) + Environment.NewLine +
                CashTerminalDescriptor.GetMessageStateFN(fis.MessageState) + Environment.NewLine +
                "Количество сообщений для ОФД:" + fis.MessageCount + Environment.NewLine +
                "Номер документа для ОФД первого в очереди:" + fis.DocumentNumber + Environment.NewLine +
                "Дата документа для ОФД первого в очереди:" + fis.Date + Environment.NewLine +
                "Время документа для ОФД первого в очереди:" + fis.Time + Environment.NewLine;

            return result;
        }

        /// <summary>
        /// Запрос количества ФД на которые нет квитанции
        /// </summary>
        /// <returns></returns>
        public int FNGetUnconfirmedDocCount()
        {
            fis.Password = fis.SysAdminPassword;
            fis.FNGetUnconfirmedDocCount();

            return fis.DocumentNumber;
        }

        /// <summary>
        /// Кол-во сообщений для ОФД
        /// </summary>
        /// <returns></returns>
        public int FNGetOfdMessageCount()
        {
            fis.Password = fis.SysAdminPassword;
            fis.FNGetInfoExchangeStatus();

            return fis.MessageCount;
        }

        #endregion

    }
}
