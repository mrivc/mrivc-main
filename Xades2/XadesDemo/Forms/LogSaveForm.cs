﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo.Forms
{
    public partial class LogSaveForm : Form
    {
        public LogSaveForm()
        {
            InitializeComponent();
        }

        private void btnSaveLog_Click(object sender, EventArgs e)
        {
            var dt = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.SaveLog_Test");

            foreach (DataRow r in dt.Rows)
            {
                var path = Application.StartupPath.ToString() + "\\Errors\\" + r["RequestName"].ToString() + ".xml";
                //var path = Path.Combine(Application.StartupPath.ToString(), "\\Errors", r["RequestName"].ToString()+".xml");
                File.WriteAllText(path, r["SendRequest"].ToString());

                path = Application.StartupPath.ToString() + "\\Errors\\" + r["ResponseName"].ToString() + ".xml";
                //path = Path.Combine(Application.StartupPath.ToString(), "\\Errors", r["ResponseName"].ToString() + ".xml");
                File.WriteAllText(path, r["Response"].ToString());
            }
        }
    }
}
