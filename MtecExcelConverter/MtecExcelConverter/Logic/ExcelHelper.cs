﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MtecExcelConverter
{
    using System.IO;
    using System.Threading;
    using System.Windows.Forms;

    using MtecExcelConverter.Helpers;

    using Excel = Microsoft.Office.Interop.Excel;

    public static class ExcelHelper
    {
        public static void ReadExcelFiles(ImportData importData)
        {
            List<FileInfo> files = FileHelper.GetDirectoryTree(importData.SourceDirectory, "*.xls");

            foreach (var fileInfo in files)
            {
                ReadExcelFile(importData, fileInfo.FullName);
            }
        }

        public static void ReadExcelFile(ImportData importData, string file)
        {
            var excelApp = new Excel.Application();

            Excel.Application xlApp;
            Excel.Workbook workbook;
            Excel.Worksheet worksheet;
            Excel.Range range;

            xlApp = new Excel.Application();

            workbook = xlApp.Workbooks.Open(file, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            worksheet = (Excel.Worksheet)workbook.Worksheets.get_Item(1);

            CellPosition accountPosition = FindIndexOfColumnData(worksheet, "Лицевой счет", string.Empty);
            CellPosition addressPosition = FindIndexOfColumnData(worksheet, "Адрес:", string.Empty);
            string periodPosition = "A";
            CellPosition sumTypePosition = FindIndexOfColumnData(worksheet, "всего начислено", string.Empty);
            CellPosition sumHeatingPosition = FindIndexOfColumnData(worksheet, "Отопление", string.Empty);
            CellPosition sumHotWaterMopPosition = FindIndexOfColumnData(worksheet, "МОП", string.Empty); //Подогрев воды МОП
            CellPosition sumHotWaterPosition = FindIndexOfColumnData(worksheet, "Подогрев воды", "МОП");

            string account = (worksheet.Cells[accountPosition.Row, accountPosition.Column] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
            string address = (worksheet.Cells[addressPosition.Row, addressPosition.Column] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();

            int startDataIndex = (sumHeatingPosition != null ? sumHeatingPosition.Row : sumHotWaterPosition.Row) + 1;

            for (int row = startDataIndex; row <= worksheet.Rows.Count; row++)
            {
                CellDataItem dataItem = new CellDataItem();
                dataItem.FileName = file;
                dataItem.Account = account;
                dataItem.Address = address;
                dataItem.Period = (worksheet.Cells[row, periodPosition] as Excel.Range).Value2;
                dataItem.SumType = (worksheet.Cells[row, sumTypePosition.Column] as Excel.Range).Value2;

                if (sumHeatingPosition != null)
                {
                    dataItem.SumHeating = (worksheet.Cells[row, sumHeatingPosition.Column] as Excel.Range).Value2;
                }

                if (sumHotWaterMopPosition != null)
                {
                    dataItem.SumHotWaterMop =
                        (worksheet.Cells[row, sumHotWaterMopPosition.Column] as Excel.Range).Value2;
                }

                if (sumHotWaterPosition != null)
                {
                    dataItem.SumHotWater = (worksheet.Cells[row, sumHotWaterPosition.Column] as Excel.Range).Value2;
                }

                if (dataItem.SumTypeValue != SummType.None)
                {
                    importData.AddData(dataItem);
                }
                else
                {
                    break;
                }
            }

            workbook.Close(true, null, null);
            xlApp.Quit();

            ReleaseExcelObject(worksheet);
            ReleaseExcelObject(workbook);
            ReleaseExcelObject(xlApp);
        }

        private static CellPosition FindIndexOfColumnData(Excel.Worksheet worksheet, string token, string notToken)
        {
            string[] columns = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q" };
            for (int row = 1; row <= 8; row++)
            {
                foreach (var column in columns)
                {
                    var value = (worksheet.Cells[row, column] as Microsoft.Office.Interop.Excel.Range).Value2;
                    if (value != null && value.ToString().Contains(token) && notToken == string.Empty)
                    {
                        return new CellPosition() { Row = row, Column = column };
                    }
                    else if (value != null && value.ToString().Contains(token) && !value.ToString().Contains(notToken))
                    {
                        return new CellPosition() { Row = row, Column = column };
                    }
                }
            }

            return null;
        }


        private static void ReleaseExcelObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }

    public class CellPosition
    {
        public string Column;

        public int Row;
    }
}
