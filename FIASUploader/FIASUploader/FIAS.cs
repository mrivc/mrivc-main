﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace FIASUploader
{
    static class FIAS
    {
        static string connectionString = Properties.Settings.Default.FIASconnection;
        static int rowNumber = Properties.Settings.Default.RowNumber;
        static bool murmanskRegionOnly = Properties.Settings.Default.MurmanskRegionOnly;

        public static void LoadXmlFiles()
        {
            DataSet dataSet = new DataSet();
            LoadXsdSchemas(dataSet);

            string fileName = string.Empty;
            string tableName = string.Empty;

            foreach (string file in Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory + "/xml", "*.xml"))
            {
                fileName = Path.GetFileName(file);
                tableName = GetTableNameByXmlFileName(fileName);

                Console.WriteLine("Загружается " + fileName + "...");

                SqlHelper.TruncateTable(string.Format("dbo.{0}", tableName), connectionString);
                LoadXml(file, dataSet.Tables[tableName]);

                Console.WriteLine("Завершено.");
            }

            //if (murmanskRegionOnly)
            //{
            //    Console.WriteLine("Выполняется процедура удаления адресов не в Мурманской области...");
            //    SqlHelper.MurmanskRegionOnly(connectionString);
            //}
        }

        public static void LoadXml(string fileName, DataTable dataTable)
        {
            using (XmlTextReader xmlTextReader = new XmlTextReader(fileName))
            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.TableLock))
            {
                sqlBulkCopy.BulkCopyTimeout = 0;

                SetupBulkCopy(sqlBulkCopy, dataTable);
                XmlDataReader reader = new XmlDataReader(xmlTextReader, dataTable, rowNumber);
                


                sqlBulkCopy.WriteToServer(reader);
            }
        }

        public static void LoadXsdSchemas(DataSet dataSet)
        {
            foreach (string file in Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory+"/xsd", "*.xsd"))
                dataSet.ReadXmlSchema(file);
        }


        private static void SetupBulkCopy(SqlBulkCopy sqlBulkCopy, DataTable dataTable)
        {
            foreach (DataColumn column in dataTable.Columns)
            {
                sqlBulkCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping(dataTable.Columns.IndexOf(column), column.ColumnName));
            }

            sqlBulkCopy.DestinationTableName = string.Format("dbo.{0}",dataTable.TableName);
        }

        static string GetTableNameByXmlFileName(string fileName)
        {
            if (fileName.StartsWith("AS_ACTSTAT"))
	           return "ActualStatus";
            if (fileName.StartsWith("AS_ADDROBJ"))
                return "Object";
            if (fileName.StartsWith("AS_CENTERST"))
                return "CenterStatus";
            if (fileName.StartsWith("AS_CURENTST"))
                return "CurrentStatus";
            if (fileName.StartsWith("AS_ESTSTAT"))
                return "EstateStatus";
            if (fileName.StartsWith("AS_HOUSEINT"))
                return "HouseInterval";
            if (fileName.StartsWith("AS_HOUSE"))
                return "House";
            if (fileName.StartsWith("AS_HSTSTAT"))
                return "HouseStateStatus";
            if (fileName.StartsWith("AS_INTVSTAT"))
                return "IntervalStatus";
            if (fileName.StartsWith("AS_LANDMARK"))
                return "Landmark";
            if (fileName.StartsWith("AS_NDOCTYPE"))
                return "NormativeDocumentType";
            if (fileName.StartsWith("AS_NORMDOC"))
                return "NormativeDocument";
            if (fileName.StartsWith("AS_OPERSTAT"))
                return "OperationStatus";
            if (fileName.StartsWith("AS_SOCRBASE"))
                return "AddressObjectType";
            if (fileName.StartsWith("AS_STRSTAT"))
                return "StructureStatus";
            else
               return null;
        }

    }
}
