﻿namespace MrivcControls.DataPickers
{
    partial class DabaBaseTablePicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddTabelsPicker = new EllisBillFilter.BaseDropDown();
            this.ddTabelsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.ddTabelsPicker.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddTabelsView)).BeginInit();
            this.SuspendLayout();
            // 
            // ddTabelsPicker
            // 
            this.ddTabelsPicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddTabelsPicker.EditValue = "";
            this.ddTabelsPicker.Location = new System.Drawing.Point(0, 0);
            this.ddTabelsPicker.Name = "ddTabelsPicker";
            this.ddTabelsPicker.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddTabelsPicker.Properties.ImmediatePopup = true;
            this.ddTabelsPicker.Properties.NullText = "";
            this.ddTabelsPicker.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.ddTabelsPicker.Properties.PopupFormSize = new System.Drawing.Size(0, 200);
            this.ddTabelsPicker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.ddTabelsPicker.Properties.View = this.ddTabelsView;
            this.ddTabelsPicker.Size = new System.Drawing.Size(253, 20);
            this.ddTabelsPicker.TabIndex = 0;
            // 
            // ddTabelsView
            // 
            this.ddTabelsView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.ddTabelsView.Name = "ddTabelsView";
            this.ddTabelsView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.ddTabelsView.OptionsView.ShowColumnHeaders = false;
            this.ddTabelsView.OptionsView.ShowGroupPanel = false;
            this.ddTabelsView.OptionsView.ShowIndicator = false;
            // 
            // DabaBaseTablePicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ddTabelsPicker);
            this.Name = "DabaBaseTablePicker";
            this.Size = new System.Drawing.Size(253, 21);
            ((System.ComponentModel.ISupportInitialize)(this.ddTabelsPicker.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddTabelsView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private EllisBillFilter.BaseDropDown ddTabelsPicker;
        private DevExpress.XtraGrid.Views.Grid.GridView ddTabelsView;
    }
}
