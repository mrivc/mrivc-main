﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XadesDemo
{
    public partial class ImportBillForm : Form
    {
        public int Period
        {
            get { return -1; }
        }

        public string SenderID
        {
            get { return ""; }
        }

        public ImportBillForm()
        {
            InitializeComponent();
        }

        private void ImportBillForm_Load(object sender, EventArgs e)
        {

        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            var importPaymentDocument = new ImportPaymentDocument(SenderID, Period);
            MrivcStatic.HouseIteration(importPaymentDocument, false);
        }

        private void prepareBtn_Click(object sender, EventArgs e)
        {
            var importPaymentDocument = new ImportPaymentDocument(SenderID, Period);
            var ds = MrivcStatic.HouseIteration(importPaymentDocument, true);

            munServDataGrid.DataSource = ds.Tables[2];

            payInfoGridView.DataSource = ds.Tables[0];
            billDataGrid.DataSource = ds.Tables[1];

            capitalRepairDataGrid.DataSource = ds.Tables[3];
            capitalRepairBillgridView.DataSource = ds.Tables[4];

            sumCheckDataGridView.DataSource = ds.Tables[5];

            importBtn.Enabled = true;
        }

        private void Period_Load(object sender, EventArgs e)
        {

        }

        private void billDataGrid_SelectionChanged(object sender, EventArgs e)
        {
            if (billDataGrid.CurrentRow == null)
                return;
            var text = billDataGrid.CurrentRow.Cells["Номер ПД"].Value.ToString();

            if (munServDataGrid.DataSource != null)
                ((DataTable)munServDataGrid.DataSource).DefaultView.RowFilter = $"[Номер ПД] = '{text}'";

           
        }
    }
}
