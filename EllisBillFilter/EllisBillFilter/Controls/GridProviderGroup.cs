﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    using DevExpress.XtraGrid.Columns;
    using DevExpress.XtraGrid.Views.Grid;

    using EllisBillFilter.DS;

    public partial class GridProviderGroup : UserControl
    {
        private MDataSet.ProviderGroupRow selectedProviderGroupRow;

        public GridProviderGroup()
        {
            InitializeComponent();
            gridProviderGroup1.InitEx();
            gridViewProviderGroup.OptionsView.ShowGroupPanel = false;
        }

        public MDataSet.ProviderGroupRow SelectedProviderGroupRow
        {
            get
            {
                return selectedProviderGroupRow;
            }
        }

        public GridView GridView
        {
            get
            {
                return gridViewProviderGroup;
            }
        }

        public void LoadData()
        {
            gridViewProviderGroup.Columns["Provider_Group"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            DAccess.DataModule.ProviderGroupSelect(mDataSet.ProviderGroup);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            gridViewProviderGroup.ClearColumnsFilter();
            gridViewProviderGroup.ActiveFilter.Add(gridViewProviderGroup.Columns["Provider_Group"], new ColumnFilterInfo("[Provider_Group] Like '%" + textBox1.Text + "%'", ""));
        }

        private void gridViewProviderGroup_DoubleClick(object sender, EventArgs e)
        {
            if (gridViewProviderGroup.FocusedRowHandle < 0)
            {
                selectedProviderGroupRow = null;
                return;
            }

            selectedProviderGroupRow = (MDataSet.ProviderGroupRow)gridViewProviderGroup.GetDataRow(gridViewProviderGroup.FocusedRowHandle);
        }
    }
}
