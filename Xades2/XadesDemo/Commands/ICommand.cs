using System;
using System.Collections.Generic;

namespace XadesDemo.Commands
{
    public interface ICommand
    {
        string Execute(string xmlBody, string senderID);
    }
}