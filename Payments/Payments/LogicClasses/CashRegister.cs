﻿using System;
using System.Collections.Generic;
using System.Text;
using DrvFRLib;
using Payments.DS;

namespace Payments
{
    public class CashRegister
    {
        DrvFRClass fis;

        public string LastCashRegisterName;
        public object LastCashRegisterValue;

        public CashRegister(DrvFRClass fis)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            this.fis = fis;
        }

        public object GetRegisterValue(int registerNumber)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return 0;

            try
            {
                fis.Password = DAccess.Instance.CurrentUser.Fiscal_User_Password;
                fis.RegisterNumber = registerNumber;
                fis.GetCashReg();
                LastCashRegisterName = fis.NameCashReg;
                LastCashRegisterValue = fis.ContentsOfCashRegister;
                return LastCashRegisterValue;
            }
            catch { }
            return 0;
        }
    }
}
