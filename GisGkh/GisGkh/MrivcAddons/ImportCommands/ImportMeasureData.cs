﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ImportMeasureData : MrivcImportCommand, ISingleHouseCommand, IMrivcImportCommandAsync
    {
        bool _isRSO;

        ExportMeasureData exportMeasure;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                base.SenderID = value;
            }
        }

        public ImportMeasureData(string senderID) : base("importMeteringDeviceValues", "DeviceMeteringAsync", senderID)
        {
            _importValueName = "MeteringDevicesValues";
            Description = "Синхронизация показаний приборов учета";

            exportMeasure = new ExportMeasureData(SenderID);
        }

        public string FiasHouseGuid { get; set; }

        public void ExportData()
        {
            //if (!_isRSO)
            //{
                if (!this.IsEmpty && !HasErrors)
                {
                    exportMeasure.FiasHouseGuid = FiasHouseGuid;
                    exportMeasure.Description = Description;
                    exportMeasure.CommandType = CommandType;
                    exportMeasure.Execute();

                    this.HasErrors = exportMeasure.HasErrors;
                }
            //}
        }

        public void ImportData()
        {
            //if (!_isRSO)
                base.Execute();
        }

        public DataSet PrepareToDataSet()
        {
            //if (!_isRSO)
            //{
                return DAccess.DataModule.GetDataSetByStoredProcedure1("[GIS].[Prepare_Import_Measure]", new object[,] { { "@SenderID", SenderID }, { "@FiasHouseGuid", FiasHouseGuid }, { "@empty", exportMeasure.HasErrors } }, "ImportMeasure");
            //}
            //else
            //    return DAccess.DataModule.GetDataSetByStoredProcedure1("[GIS].[Prepare_Import_Measure]", new object[,] { { "@SenderID", SenderID }, { "@FiasHouseGuid", FiasHouseGuid }, { "@empty", true } } , "ImportMeasure");

        }

        public override object Clone()
        {
            var exportMeasureDataClone = (ExportMeasureData)exportMeasure.Clone();
            var baseClone = (MrivcCommand)base.Clone();
            ((ImportMeasureData)baseClone).exportMeasure = exportMeasureDataClone;
            return baseClone;
        }

        public new void Execute()
        {
            ExportData();
            ImportData();
            ExportData();
        }

        protected override void CreateXmlBody()
        {
            var ds = PrepareToDataSet();

            Values = new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) };

            foreach (DataRow row in ds.Tables["ImportMeasure"].Select("Energy = 0"))
            {
                var values = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "MeteringDeviceRootGUID" },"MeteringDeviceRootGUID"),
                    new Tuple<string[], string>( new [] { "OneRateDeviceValue", "CurrentValue", "MunicipalResource", "Code" },"MunicipalResourceCode"),
                    new Tuple<string[], string>( new [] { "OneRateDeviceValue", "CurrentValue", "MunicipalResource", "GUID" },"MunicipalResourceGUID"),
                    new Tuple<string[], string>( new [] { "OneRateDeviceValue", "CurrentValue", "MunicipalResource", "Name" },"MunicipalResourceName"),
                    new Tuple<string[], string>( new [] { "OneRateDeviceValue", "CurrentValue", "MeteringValue" },"MeteringValue"),
                    new Tuple<string[], string>( new [] { "OneRateDeviceValue", "CurrentValue", "DateValue" },"DateValue")
                };

                AddChildElement(row, Service, "ImportMeteringDeviceValues_add", null, values, null, null, "importMeteringDeviceValuesRequest");
            }

            foreach (DataRow row in ds.Tables["ImportMeasure"].Select("Energy = 1"))
            {
                var values = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "MeteringDeviceRootGUID" },"MeteringDeviceRootGUID"),
                    new Tuple<string[], string>( new [] { "ElectricDeviceValue", "CurrentValue", "MeteringValueT1"},"MeteringValue"),
                    new Tuple<string[], string>( new [] { "ElectricDeviceValue", "CurrentValue", "DateValue" },"DateValue")
                };

                AddChildElement(row, Service, "ImportMeteringDeviceValues_el_add", null, values, null, null, "importMeteringDeviceValuesRequest");
            }
        }
    }
}
