set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

ALTER FUNCTION [Reports].[Account_Owner_List_Export_Domofon] (@ID_Account TID, @Period TID = null)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX)
	SET @Result = NULL

	IF @Period is null (SELECT @Period = Period FROM Period WHERE Is_Work_Period = 1)

	SELECT @Result = COALESCE(@Result + ', ', '') + COALESCE(Last_Name + ' ', '') + COALESCE(NULLIF(LEFT(First_Name,1),'')+'.','') + COALESCE(NULLIF(LEFT(Second_Name,1),'')+'.','')
	FROM VAccount_Owner
	WHERE ID_Account = @ID_Account and Period = @Period
  AND Visible_In_Bill = 1
	RETURN @Result
END

