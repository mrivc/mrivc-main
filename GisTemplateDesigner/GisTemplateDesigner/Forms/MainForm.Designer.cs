﻿namespace GisTemplateDesigner
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tablesTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.characteristicsDataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.entrancesDataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.premisesDataGridView = new System.Windows.Forms.DataGridView();
            this.prepareBtn = new System.Windows.Forms.Button();
            this.exportBtn = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.templatesTabControl = new System.Windows.Forms.TabControl();
            this.mkdTemplateTabPage = new System.Windows.Forms.TabPage();
            this.accountTemplateTabPage = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.mainInfoDataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.accountPremiseDataGridView = new System.Windows.Forms.DataGridView();
            this.meterTabPage = new System.Windows.Forms.TabPage();
            this.meterTabControl = new System.Windows.Forms.TabControl();
            this.infoMeterTabPage = new System.Windows.Forms.TabPage();
            this.infoMeterDataGridView = new System.Windows.Forms.DataGridView();
            this.measureTemplateTabPage = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.infoMeasureTabPage = new System.Windows.Forms.TabPage();
            this.measureDataGridView = new System.Windows.Forms.DataGridView();
            this.measureOdpuTemplateTabPage = new System.Windows.Forms.TabPage();
            this.measureOdpuDataGridView = new System.Windows.Forms.DataGridView();
            this.billTemplateTabPage = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.bill1TabPage = new System.Windows.Forms.TabPage();
            this.bill1DataGridView = new System.Windows.Forms.DataGridView();
            this.bill2TabPage = new System.Windows.Forms.TabPage();
            this.bill2DataGridView = new System.Windows.Forms.DataGridView();
            this.dpdTabPage = new System.Windows.Forms.TabPage();
            this.dpdDataGridView = new System.Windows.Forms.DataGridView();
            this.uoRadioButton = new System.Windows.Forms.RadioButton();
            this.typeGroupBox = new System.Windows.Forms.GroupBox();
            this.rsoRadioButton = new System.Windows.Forms.RadioButton();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.exportTabPage = new System.Windows.Forms.TabPage();
            this.importTabPage = new System.Windows.Forms.TabPage();
            this.importTemplateBtn = new System.Windows.Forms.Button();
            this.openImportTemplateBtn = new System.Windows.Forms.Button();
            this.importTabControl = new System.Windows.Forms.TabControl();
            this.accountImportTabPage = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.premisesImportDataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.accountImportDataGridView = new System.Windows.Forms.DataGridView();
            this.meterImportTabPage = new System.Windows.Forms.TabPage();
            this.meterImportDataGridView = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.gisCompanySingle1 = new MrivcControls.GisCompanySingle();
            this.gisCompany1 = new MrivcControls.GisCompany();
            this.measurePeriodPicker = new MrivcControls.PeriodPicker();
            this.measureOdpuPeriodPicker = new MrivcControls.PeriodPicker();
            this.periodPicker1 = new MrivcControls.PeriodPicker();
            this.tablesTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.characteristicsDataGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.entrancesDataGridView)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.premisesDataGridView)).BeginInit();
            this.templatesTabControl.SuspendLayout();
            this.mkdTemplateTabPage.SuspendLayout();
            this.accountTemplateTabPage.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainInfoDataGridView)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accountPremiseDataGridView)).BeginInit();
            this.meterTabPage.SuspendLayout();
            this.meterTabControl.SuspendLayout();
            this.infoMeterTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoMeterDataGridView)).BeginInit();
            this.measureTemplateTabPage.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.infoMeasureTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measureDataGridView)).BeginInit();
            this.measureOdpuTemplateTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measureOdpuDataGridView)).BeginInit();
            this.billTemplateTabPage.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.bill1TabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bill1DataGridView)).BeginInit();
            this.bill2TabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bill2DataGridView)).BeginInit();
            this.dpdTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dpdDataGridView)).BeginInit();
            this.typeGroupBox.SuspendLayout();
            this.mainTabControl.SuspendLayout();
            this.exportTabPage.SuspendLayout();
            this.importTabPage.SuspendLayout();
            this.importTabControl.SuspendLayout();
            this.accountImportTabPage.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.premisesImportDataGridView)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accountImportDataGridView)).BeginInit();
            this.meterImportTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meterImportDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tablesTabControl
            // 
            this.tablesTabControl.Controls.Add(this.tabPage1);
            this.tablesTabControl.Controls.Add(this.tabPage2);
            this.tablesTabControl.Controls.Add(this.tabPage3);
            this.tablesTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablesTabControl.Location = new System.Drawing.Point(3, 3);
            this.tablesTabControl.Name = "tablesTabControl";
            this.tablesTabControl.SelectedIndex = 0;
            this.tablesTabControl.Size = new System.Drawing.Size(890, 310);
            this.tablesTabControl.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.characteristicsDataGridView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(882, 284);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Характеристики";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // characteristicsDataGridView
            // 
            this.characteristicsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.characteristicsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.characteristicsDataGridView.Location = new System.Drawing.Point(3, 3);
            this.characteristicsDataGridView.Name = "characteristicsDataGridView";
            this.characteristicsDataGridView.Size = new System.Drawing.Size(876, 278);
            this.characteristicsDataGridView.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.entrancesDataGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(882, 284);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Подъезды";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // entrancesDataGridView
            // 
            this.entrancesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.entrancesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.entrancesDataGridView.Location = new System.Drawing.Point(3, 3);
            this.entrancesDataGridView.Name = "entrancesDataGridView";
            this.entrancesDataGridView.Size = new System.Drawing.Size(876, 278);
            this.entrancesDataGridView.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.premisesDataGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(882, 284);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Жилые помещения";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // premisesDataGridView
            // 
            this.premisesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.premisesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.premisesDataGridView.Location = new System.Drawing.Point(3, 3);
            this.premisesDataGridView.Name = "premisesDataGridView";
            this.premisesDataGridView.Size = new System.Drawing.Size(876, 278);
            this.premisesDataGridView.TabIndex = 0;
            // 
            // prepareBtn
            // 
            this.prepareBtn.Location = new System.Drawing.Point(365, 22);
            this.prepareBtn.Name = "prepareBtn";
            this.prepareBtn.Size = new System.Drawing.Size(113, 23);
            this.prepareBtn.TabIndex = 3;
            this.prepareBtn.Text = "Просмотр";
            this.prepareBtn.UseVisualStyleBackColor = true;
            this.prepareBtn.Click += new System.EventHandler(this.prepareBtn_Click);
            // 
            // exportBtn
            // 
            this.exportBtn.Location = new System.Drawing.Point(785, 22);
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(124, 23);
            this.exportBtn.TabIndex = 4;
            this.exportBtn.Text = "Экспорт";
            this.exportBtn.UseVisualStyleBackColor = true;
            this.exportBtn.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // templatesTabControl
            // 
            this.templatesTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templatesTabControl.Controls.Add(this.mkdTemplateTabPage);
            this.templatesTabControl.Controls.Add(this.accountTemplateTabPage);
            this.templatesTabControl.Controls.Add(this.meterTabPage);
            this.templatesTabControl.Controls.Add(this.measureTemplateTabPage);
            this.templatesTabControl.Controls.Add(this.measureOdpuTemplateTabPage);
            this.templatesTabControl.Controls.Add(this.billTemplateTabPage);
            this.templatesTabControl.Location = new System.Drawing.Point(3, 80);
            this.templatesTabControl.Name = "templatesTabControl";
            this.templatesTabControl.SelectedIndex = 0;
            this.templatesTabControl.Size = new System.Drawing.Size(904, 342);
            this.templatesTabControl.TabIndex = 6;
            // 
            // mkdTemplateTabPage
            // 
            this.mkdTemplateTabPage.BackColor = System.Drawing.Color.Transparent;
            this.mkdTemplateTabPage.Controls.Add(this.tablesTabControl);
            this.mkdTemplateTabPage.Location = new System.Drawing.Point(4, 22);
            this.mkdTemplateTabPage.Name = "mkdTemplateTabPage";
            this.mkdTemplateTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.mkdTemplateTabPage.Size = new System.Drawing.Size(896, 316);
            this.mkdTemplateTabPage.TabIndex = 0;
            this.mkdTemplateTabPage.Text = "МКД";
            // 
            // accountTemplateTabPage
            // 
            this.accountTemplateTabPage.BackColor = System.Drawing.Color.Transparent;
            this.accountTemplateTabPage.Controls.Add(this.tabControl1);
            this.accountTemplateTabPage.Location = new System.Drawing.Point(4, 22);
            this.accountTemplateTabPage.Name = "accountTemplateTabPage";
            this.accountTemplateTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.accountTemplateTabPage.Size = new System.Drawing.Size(896, 316);
            this.accountTemplateTabPage.TabIndex = 1;
            this.accountTemplateTabPage.Text = "Лицевые счета";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(890, 310);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.mainInfoDataGridView);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(882, 284);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "Основные сведения";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // mainInfoDataGridView
            // 
            this.mainInfoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainInfoDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainInfoDataGridView.Location = new System.Drawing.Point(3, 3);
            this.mainInfoDataGridView.Name = "mainInfoDataGridView";
            this.mainInfoDataGridView.Size = new System.Drawing.Size(876, 278);
            this.mainInfoDataGridView.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.accountPremiseDataGridView);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(882, 284);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Помещение";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // accountPremiseDataGridView
            // 
            this.accountPremiseDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.accountPremiseDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accountPremiseDataGridView.Location = new System.Drawing.Point(3, 3);
            this.accountPremiseDataGridView.Name = "accountPremiseDataGridView";
            this.accountPremiseDataGridView.Size = new System.Drawing.Size(876, 278);
            this.accountPremiseDataGridView.TabIndex = 0;
            // 
            // meterTabPage
            // 
            this.meterTabPage.BackColor = System.Drawing.Color.Transparent;
            this.meterTabPage.Controls.Add(this.meterTabControl);
            this.meterTabPage.Location = new System.Drawing.Point(4, 22);
            this.meterTabPage.Name = "meterTabPage";
            this.meterTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.meterTabPage.Size = new System.Drawing.Size(896, 316);
            this.meterTabPage.TabIndex = 3;
            this.meterTabPage.Text = "Счетчики";
            // 
            // meterTabControl
            // 
            this.meterTabControl.Controls.Add(this.infoMeterTabPage);
            this.meterTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meterTabControl.Location = new System.Drawing.Point(3, 3);
            this.meterTabControl.Name = "meterTabControl";
            this.meterTabControl.SelectedIndex = 0;
            this.meterTabControl.Size = new System.Drawing.Size(890, 310);
            this.meterTabControl.TabIndex = 1;
            // 
            // infoMeterTabPage
            // 
            this.infoMeterTabPage.Controls.Add(this.infoMeterDataGridView);
            this.infoMeterTabPage.Location = new System.Drawing.Point(4, 22);
            this.infoMeterTabPage.Name = "infoMeterTabPage";
            this.infoMeterTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.infoMeterTabPage.Size = new System.Drawing.Size(882, 284);
            this.infoMeterTabPage.TabIndex = 0;
            this.infoMeterTabPage.Text = "Сведения о ПУ";
            this.infoMeterTabPage.UseVisualStyleBackColor = true;
            // 
            // infoMeterDataGridView
            // 
            this.infoMeterDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.infoMeterDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoMeterDataGridView.Location = new System.Drawing.Point(3, 3);
            this.infoMeterDataGridView.Name = "infoMeterDataGridView";
            this.infoMeterDataGridView.Size = new System.Drawing.Size(876, 278);
            this.infoMeterDataGridView.TabIndex = 0;
            // 
            // measureTemplateTabPage
            // 
            this.measureTemplateTabPage.BackColor = System.Drawing.Color.Transparent;
            this.measureTemplateTabPage.Controls.Add(this.measurePeriodPicker);
            this.measureTemplateTabPage.Controls.Add(this.tabControl3);
            this.measureTemplateTabPage.Location = new System.Drawing.Point(4, 22);
            this.measureTemplateTabPage.Name = "measureTemplateTabPage";
            this.measureTemplateTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.measureTemplateTabPage.Size = new System.Drawing.Size(896, 316);
            this.measureTemplateTabPage.TabIndex = 4;
            this.measureTemplateTabPage.Text = "Показания";
            // 
            // tabControl3
            // 
            this.tabControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl3.Controls.Add(this.infoMeasureTabPage);
            this.tabControl3.Location = new System.Drawing.Point(3, 67);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(890, 246);
            this.tabControl3.TabIndex = 0;
            // 
            // infoMeasureTabPage
            // 
            this.infoMeasureTabPage.Controls.Add(this.measureDataGridView);
            this.infoMeasureTabPage.Location = new System.Drawing.Point(4, 22);
            this.infoMeasureTabPage.Name = "infoMeasureTabPage";
            this.infoMeasureTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.infoMeasureTabPage.Size = new System.Drawing.Size(882, 220);
            this.infoMeasureTabPage.TabIndex = 0;
            this.infoMeasureTabPage.Text = "Импорт показаний ИПУ";
            this.infoMeasureTabPage.UseVisualStyleBackColor = true;
            // 
            // measureDataGridView
            // 
            this.measureDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measureDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.measureDataGridView.Location = new System.Drawing.Point(3, 3);
            this.measureDataGridView.Name = "measureDataGridView";
            this.measureDataGridView.Size = new System.Drawing.Size(876, 214);
            this.measureDataGridView.TabIndex = 0;
            // 
            // measureOdpuTemplateTabPage
            // 
            this.measureOdpuTemplateTabPage.BackColor = System.Drawing.Color.Transparent;
            this.measureOdpuTemplateTabPage.Controls.Add(this.measureOdpuPeriodPicker);
            this.measureOdpuTemplateTabPage.Controls.Add(this.measureOdpuDataGridView);
            this.measureOdpuTemplateTabPage.Location = new System.Drawing.Point(4, 22);
            this.measureOdpuTemplateTabPage.Name = "measureOdpuTemplateTabPage";
            this.measureOdpuTemplateTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.measureOdpuTemplateTabPage.Size = new System.Drawing.Size(896, 316);
            this.measureOdpuTemplateTabPage.TabIndex = 5;
            this.measureOdpuTemplateTabPage.Text = "Показания ОДПУ";
            // 
            // measureOdpuDataGridView
            // 
            this.measureOdpuDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.measureOdpuDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measureOdpuDataGridView.Location = new System.Drawing.Point(3, 67);
            this.measureOdpuDataGridView.Name = "measureOdpuDataGridView";
            this.measureOdpuDataGridView.Size = new System.Drawing.Size(890, 246);
            this.measureOdpuDataGridView.TabIndex = 0;
            // 
            // billTemplateTabPage
            // 
            this.billTemplateTabPage.BackColor = System.Drawing.Color.Transparent;
            this.billTemplateTabPage.Controls.Add(this.periodPicker1);
            this.billTemplateTabPage.Controls.Add(this.tabControl2);
            this.billTemplateTabPage.Location = new System.Drawing.Point(4, 22);
            this.billTemplateTabPage.Name = "billTemplateTabPage";
            this.billTemplateTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.billTemplateTabPage.Size = new System.Drawing.Size(896, 316);
            this.billTemplateTabPage.TabIndex = 2;
            this.billTemplateTabPage.Text = "Квитанции";
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.bill1TabPage);
            this.tabControl2.Controls.Add(this.bill2TabPage);
            this.tabControl2.Controls.Add(this.dpdTabPage);
            this.tabControl2.Location = new System.Drawing.Point(3, 64);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(890, 249);
            this.tabControl2.TabIndex = 0;
            // 
            // bill1TabPage
            // 
            this.bill1TabPage.Controls.Add(this.bill1DataGridView);
            this.bill1TabPage.Location = new System.Drawing.Point(4, 22);
            this.bill1TabPage.Name = "bill1TabPage";
            this.bill1TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.bill1TabPage.Size = new System.Drawing.Size(882, 223);
            this.bill1TabPage.TabIndex = 0;
            this.bill1TabPage.Text = "Разделы 1-2";
            this.bill1TabPage.UseVisualStyleBackColor = true;
            // 
            // bill1DataGridView
            // 
            this.bill1DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bill1DataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bill1DataGridView.Location = new System.Drawing.Point(3, 3);
            this.bill1DataGridView.Name = "bill1DataGridView";
            this.bill1DataGridView.Size = new System.Drawing.Size(876, 217);
            this.bill1DataGridView.TabIndex = 0;
            // 
            // bill2TabPage
            // 
            this.bill2TabPage.Controls.Add(this.bill2DataGridView);
            this.bill2TabPage.Location = new System.Drawing.Point(4, 22);
            this.bill2TabPage.Name = "bill2TabPage";
            this.bill2TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.bill2TabPage.Size = new System.Drawing.Size(882, 223);
            this.bill2TabPage.TabIndex = 1;
            this.bill2TabPage.Text = "Разделы 3-6";
            this.bill2TabPage.UseVisualStyleBackColor = true;
            // 
            // bill2DataGridView
            // 
            this.bill2DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bill2DataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bill2DataGridView.Location = new System.Drawing.Point(3, 3);
            this.bill2DataGridView.Name = "bill2DataGridView";
            this.bill2DataGridView.Size = new System.Drawing.Size(876, 217);
            this.bill2DataGridView.TabIndex = 0;
            // 
            // dpdTabPage
            // 
            this.dpdTabPage.Controls.Add(this.dpdDataGridView);
            this.dpdTabPage.Location = new System.Drawing.Point(4, 22);
            this.dpdTabPage.Name = "dpdTabPage";
            this.dpdTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.dpdTabPage.Size = new System.Drawing.Size(882, 223);
            this.dpdTabPage.TabIndex = 2;
            this.dpdTabPage.Text = "ДПД";
            this.dpdTabPage.UseVisualStyleBackColor = true;
            // 
            // dpdDataGridView
            // 
            this.dpdDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dpdDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpdDataGridView.Location = new System.Drawing.Point(3, 3);
            this.dpdDataGridView.Name = "dpdDataGridView";
            this.dpdDataGridView.Size = new System.Drawing.Size(876, 217);
            this.dpdDataGridView.TabIndex = 0;
            // 
            // uoRadioButton
            // 
            this.uoRadioButton.AutoSize = true;
            this.uoRadioButton.Checked = true;
            this.uoRadioButton.Location = new System.Drawing.Point(11, 19);
            this.uoRadioButton.Name = "uoRadioButton";
            this.uoRadioButton.Size = new System.Drawing.Size(41, 17);
            this.uoRadioButton.TabIndex = 7;
            this.uoRadioButton.TabStop = true;
            this.uoRadioButton.Text = "УО";
            this.uoRadioButton.UseVisualStyleBackColor = true;
            this.uoRadioButton.CheckedChanged += new System.EventHandler(this.uoRadioButton_CheckedChanged);
            // 
            // typeGroupBox
            // 
            this.typeGroupBox.Controls.Add(this.rsoRadioButton);
            this.typeGroupBox.Controls.Add(this.uoRadioButton);
            this.typeGroupBox.Location = new System.Drawing.Point(10, 6);
            this.typeGroupBox.Name = "typeGroupBox";
            this.typeGroupBox.Size = new System.Drawing.Size(73, 68);
            this.typeGroupBox.TabIndex = 8;
            this.typeGroupBox.TabStop = false;
            this.typeGroupBox.Text = "Тип";
            // 
            // rsoRadioButton
            // 
            this.rsoRadioButton.AutoSize = true;
            this.rsoRadioButton.Location = new System.Drawing.Point(11, 42);
            this.rsoRadioButton.Name = "rsoRadioButton";
            this.rsoRadioButton.Size = new System.Drawing.Size(47, 17);
            this.rsoRadioButton.TabIndex = 9;
            this.rsoRadioButton.Text = "РСО";
            this.rsoRadioButton.UseVisualStyleBackColor = true;
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.exportTabPage);
            this.mainTabControl.Controls.Add(this.importTabPage);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 0);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(918, 451);
            this.mainTabControl.TabIndex = 11;
            // 
            // exportTabPage
            // 
            this.exportTabPage.BackColor = System.Drawing.Color.Transparent;
            this.exportTabPage.Controls.Add(this.gisCompanySingle1);
            this.exportTabPage.Controls.Add(this.prepareBtn);
            this.exportTabPage.Controls.Add(this.exportBtn);
            this.exportTabPage.Controls.Add(this.typeGroupBox);
            this.exportTabPage.Controls.Add(this.gisCompany1);
            this.exportTabPage.Controls.Add(this.templatesTabControl);
            this.exportTabPage.Location = new System.Drawing.Point(4, 22);
            this.exportTabPage.Name = "exportTabPage";
            this.exportTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.exportTabPage.Size = new System.Drawing.Size(910, 425);
            this.exportTabPage.TabIndex = 0;
            this.exportTabPage.Text = "Экспорт";
            // 
            // importTabPage
            // 
            this.importTabPage.BackColor = System.Drawing.Color.Transparent;
            this.importTabPage.Controls.Add(this.importTemplateBtn);
            this.importTabPage.Controls.Add(this.openImportTemplateBtn);
            this.importTabPage.Controls.Add(this.importTabControl);
            this.importTabPage.Location = new System.Drawing.Point(4, 22);
            this.importTabPage.Name = "importTabPage";
            this.importTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.importTabPage.Size = new System.Drawing.Size(910, 425);
            this.importTabPage.TabIndex = 1;
            this.importTabPage.Text = "Импорт";
            // 
            // importTemplateBtn
            // 
            this.importTemplateBtn.Location = new System.Drawing.Point(148, 6);
            this.importTemplateBtn.Name = "importTemplateBtn";
            this.importTemplateBtn.Size = new System.Drawing.Size(147, 23);
            this.importTemplateBtn.TabIndex = 2;
            this.importTemplateBtn.Text = "Импорт";
            this.importTemplateBtn.UseVisualStyleBackColor = true;
            this.importTemplateBtn.Click += new System.EventHandler(this.importTemplateBtn_Click);
            // 
            // openImportTemplateBtn
            // 
            this.openImportTemplateBtn.Location = new System.Drawing.Point(8, 6);
            this.openImportTemplateBtn.Name = "openImportTemplateBtn";
            this.openImportTemplateBtn.Size = new System.Drawing.Size(134, 23);
            this.openImportTemplateBtn.TabIndex = 1;
            this.openImportTemplateBtn.Text = "Открыть";
            this.openImportTemplateBtn.UseVisualStyleBackColor = true;
            this.openImportTemplateBtn.Click += new System.EventHandler(this.openImportTemplateBtn_Click);
            // 
            // importTabControl
            // 
            this.importTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.importTabControl.Controls.Add(this.accountImportTabPage);
            this.importTabControl.Controls.Add(this.meterImportTabPage);
            this.importTabControl.Location = new System.Drawing.Point(3, 35);
            this.importTabControl.Name = "importTabControl";
            this.importTabControl.SelectedIndex = 0;
            this.importTabControl.Size = new System.Drawing.Size(904, 387);
            this.importTabControl.TabIndex = 0;
            // 
            // accountImportTabPage
            // 
            this.accountImportTabPage.BackColor = System.Drawing.Color.Transparent;
            this.accountImportTabPage.Controls.Add(this.tabControl4);
            this.accountImportTabPage.Location = new System.Drawing.Point(4, 22);
            this.accountImportTabPage.Name = "accountImportTabPage";
            this.accountImportTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.accountImportTabPage.Size = new System.Drawing.Size(896, 361);
            this.accountImportTabPage.TabIndex = 0;
            this.accountImportTabPage.Text = "Лицевые счета";
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage4);
            this.tabControl4.Controls.Add(this.tabPage5);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(3, 3);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(890, 355);
            this.tabControl4.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.premisesImportDataGridView);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(882, 329);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Помещения";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // premisesImportDataGridView
            // 
            this.premisesImportDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.premisesImportDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.premisesImportDataGridView.Location = new System.Drawing.Point(3, 3);
            this.premisesImportDataGridView.Name = "premisesImportDataGridView";
            this.premisesImportDataGridView.Size = new System.Drawing.Size(876, 323);
            this.premisesImportDataGridView.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.accountImportDataGridView);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(882, 329);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "ЕЛС";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // accountImportDataGridView
            // 
            this.accountImportDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.accountImportDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accountImportDataGridView.Location = new System.Drawing.Point(3, 3);
            this.accountImportDataGridView.Name = "accountImportDataGridView";
            this.accountImportDataGridView.Size = new System.Drawing.Size(876, 323);
            this.accountImportDataGridView.TabIndex = 0;
            // 
            // meterImportTabPage
            // 
            this.meterImportTabPage.BackColor = System.Drawing.Color.Transparent;
            this.meterImportTabPage.Controls.Add(this.meterImportDataGridView);
            this.meterImportTabPage.Location = new System.Drawing.Point(4, 22);
            this.meterImportTabPage.Name = "meterImportTabPage";
            this.meterImportTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.meterImportTabPage.Size = new System.Drawing.Size(896, 361);
            this.meterImportTabPage.TabIndex = 1;
            this.meterImportTabPage.Text = "Счетчики";
            // 
            // meterImportDataGridView
            // 
            this.meterImportDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.meterImportDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meterImportDataGridView.Location = new System.Drawing.Point(3, 3);
            this.meterImportDataGridView.Name = "meterImportDataGridView";
            this.meterImportDataGridView.Size = new System.Drawing.Size(890, 355);
            this.meterImportDataGridView.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // gisCompanySingle1
            // 
            this.gisCompanySingle1.DataSource = null;
            this.gisCompanySingle1.DisplayMember = "SenderName";
            this.gisCompanySingle1.Location = new System.Drawing.Point(89, 6);
            this.gisCompanySingle1.Name = "gisCompanySingle1";
            this.gisCompanySingle1.ParameterName = "Зарегистрированные компании";
            this.gisCompanySingle1.Size = new System.Drawing.Size(270, 55);
            this.gisCompanySingle1.TabIndex = 9;
            this.gisCompanySingle1.ValueMember = "id_company";
            // 
            // gisCompany1
            // 
            this.gisCompany1.DataSource = null;
            this.gisCompany1.DisplayMember = "SenderName";
            this.gisCompany1.Location = new System.Drawing.Point(509, 6);
            this.gisCompany1.Name = "gisCompany1";
            this.gisCompany1.ParameterName = "Зарегистрированные компании";
            this.gisCompany1.Size = new System.Drawing.Size(270, 55);
            this.gisCompany1.TabIndex = 5;
            this.gisCompany1.ValueMember = "id_company";
            // 
            // measurePeriodPicker
            // 
            this.measurePeriodPicker.DataSource = null;
            this.measurePeriodPicker.DisplayMember = "period_name";
            this.measurePeriodPicker.Location = new System.Drawing.Point(3, 6);
            this.measurePeriodPicker.Name = "measurePeriodPicker";
            this.measurePeriodPicker.ParameterName = "Период";
            this.measurePeriodPicker.Size = new System.Drawing.Size(209, 55);
            this.measurePeriodPicker.TabIndex = 1;
            this.measurePeriodPicker.ValueMember = "period";
            // 
            // measureOdpuPeriodPicker
            // 
            this.measureOdpuPeriodPicker.DataSource = null;
            this.measureOdpuPeriodPicker.DisplayMember = "period_name";
            this.measureOdpuPeriodPicker.Location = new System.Drawing.Point(3, 6);
            this.measureOdpuPeriodPicker.Name = "measureOdpuPeriodPicker";
            this.measureOdpuPeriodPicker.ParameterName = "Период";
            this.measureOdpuPeriodPicker.Size = new System.Drawing.Size(209, 55);
            this.measureOdpuPeriodPicker.TabIndex = 1;
            this.measureOdpuPeriodPicker.ValueMember = "period";
            // 
            // periodPicker1
            // 
            this.periodPicker1.DataSource = null;
            this.periodPicker1.DisplayMember = "period_name";
            this.periodPicker1.Location = new System.Drawing.Point(3, 6);
            this.periodPicker1.Name = "periodPicker1";
            this.periodPicker1.ParameterName = "Период";
            this.periodPicker1.Size = new System.Drawing.Size(209, 55);
            this.periodPicker1.TabIndex = 10;
            this.periodPicker1.ValueMember = "period";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 451);
            this.Controls.Add(this.mainTabControl);
            this.Name = "MainForm";
            this.Text = "MkdForm";
            this.Load += new System.EventHandler(this.MkdForm_Load);
            this.tablesTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.characteristicsDataGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.entrancesDataGridView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.premisesDataGridView)).EndInit();
            this.templatesTabControl.ResumeLayout(false);
            this.mkdTemplateTabPage.ResumeLayout(false);
            this.accountTemplateTabPage.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainInfoDataGridView)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accountPremiseDataGridView)).EndInit();
            this.meterTabPage.ResumeLayout(false);
            this.meterTabControl.ResumeLayout(false);
            this.infoMeterTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.infoMeterDataGridView)).EndInit();
            this.measureTemplateTabPage.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.infoMeasureTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.measureDataGridView)).EndInit();
            this.measureOdpuTemplateTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.measureOdpuDataGridView)).EndInit();
            this.billTemplateTabPage.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.bill1TabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bill1DataGridView)).EndInit();
            this.bill2TabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bill2DataGridView)).EndInit();
            this.dpdTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dpdDataGridView)).EndInit();
            this.typeGroupBox.ResumeLayout(false);
            this.typeGroupBox.PerformLayout();
            this.mainTabControl.ResumeLayout(false);
            this.exportTabPage.ResumeLayout(false);
            this.importTabPage.ResumeLayout(false);
            this.importTabControl.ResumeLayout(false);
            this.accountImportTabPage.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.premisesImportDataGridView)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accountImportDataGridView)).EndInit();
            this.meterImportTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.meterImportDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tablesTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView characteristicsDataGridView;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView entrancesDataGridView;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView premisesDataGridView;
        private System.Windows.Forms.Button prepareBtn;
        private System.Windows.Forms.Button exportBtn;
        private MrivcControls.GisCompany gisCompany1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TabControl templatesTabControl;
        private System.Windows.Forms.TabPage mkdTemplateTabPage;
        private System.Windows.Forms.TabPage accountTemplateTabPage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView mainInfoDataGridView;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView accountPremiseDataGridView;
        private System.Windows.Forms.RadioButton uoRadioButton;
        private System.Windows.Forms.GroupBox typeGroupBox;
        private System.Windows.Forms.RadioButton rsoRadioButton;
        private MrivcControls.GisCompanySingle gisCompanySingle1;
        private System.Windows.Forms.TabPage billTemplateTabPage;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage bill1TabPage;
        private System.Windows.Forms.DataGridView bill1DataGridView;
        private System.Windows.Forms.TabPage bill2TabPage;
        private System.Windows.Forms.DataGridView bill2DataGridView;
        private System.Windows.Forms.TabPage dpdTabPage;
        private System.Windows.Forms.DataGridView dpdDataGridView;
        private MrivcControls.PeriodPicker periodPicker1;
        private System.Windows.Forms.TabPage meterTabPage;
        private System.Windows.Forms.TabControl meterTabControl;
        private System.Windows.Forms.TabPage infoMeterTabPage;
        private System.Windows.Forms.DataGridView infoMeterDataGridView;
        private System.Windows.Forms.TabPage measureTemplateTabPage;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage infoMeasureTabPage;
        private System.Windows.Forms.DataGridView measureDataGridView;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage exportTabPage;
        private System.Windows.Forms.TabPage importTabPage;
        private System.Windows.Forms.Button importTemplateBtn;
        private System.Windows.Forms.Button openImportTemplateBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MrivcControls.PeriodPicker measurePeriodPicker;
        private System.Windows.Forms.TabPage measureOdpuTemplateTabPage;
        private MrivcControls.PeriodPicker measureOdpuPeriodPicker;
        private System.Windows.Forms.DataGridView measureOdpuDataGridView;
        private System.Windows.Forms.TabControl importTabControl;
        private System.Windows.Forms.TabPage accountImportTabPage;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView premisesImportDataGridView;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView accountImportDataGridView;
        private System.Windows.Forms.TabPage meterImportTabPage;
        private System.Windows.Forms.DataGridView meterImportDataGridView;
    }
}