﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PrintDeliverySend.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("25")]
        public int SmtpServerPort {
            get {
                return ((int)(this["SmtpServerPort"]));
            }
            set {
                this["SmtpServerPort"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Persist Security Info=false;Integ" +
            "rated security=true")]
        public string MainConnection {
            get {
                return ((string)(this["MainConnection"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("c:\\111")]
        public string PathToBill {
            get {
                return ((string)(this["PathToBill"]));
            }
            set {
                this["PathToBill"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Счёт-квитанция по ЛС № @Account@ от МУП \"МРИВЦ\" за @Period@ (@BillKind@)")]
        public string MailSubject {
            get {
                return ((string)(this["MailSubject"]));
            }
            set {
                this["MailSubject"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"
                  Уважаемый абонент!

                  МУП ""МРИВЦ"" высылает счет-квитанцию по адресу @Address@ (ЛС № @Account@) по E-mail за @Period@ (@BillKind@).

                  1. Передать показания по индивидуальным приборам учета можно на сайте МУП ""МРИВЦ"" (http://mrivc.ru/zagruzka/)

                  2. По вопросам начисления можно обратиться в учетно-расчетную службу по вашему округу:

                  УРС-1 (Первомайский округ) – ул. Щербакова д. 26 (тел. 53−42−26)
                  УРС-3 (Ленинский округ) – ул. Хлобыстова д. 26 (тел. 22−60−39, 22−39−97, 22−61−11)
                  УРС-4 (Октябрьский округ) – ул. Софьи Перовской 16 а (тел. 45−07−10, 45−07−08, 60−13−14, 60−13−15)

                  Звонки принимаются:
                  Понедельник, вторник, пятница – 10.30−12.30
                  Среда – 14.00−16.00
                  Четверг –  09.00−12.00

                  График приема:
                  Понедельник, вторник – 13.30−18.45
                  Среда – 8.00−13.00
                  Четверг – приема нет
                  Пятница – 13.30−18.30
                  УРС-Кола – г. Кола, ул. Каменный остров д.5 (тел. 60−01−24)

                  График приема населения: пн, ср, пн (8:30–12:00), вт, чт (13:00–16:30)


                  Данное письмо сформировано автоматически и не требует ответа.")]
        public string MailBody {
            get {
                return ((string)(this["MailBody"]));
            }
            set {
                this["MailBody"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SendCopyTo {
            get {
                return ((string)(this["SendCopyTo"]));
            }
            set {
                this["SendCopyTo"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("billing@mrivc.ru")]
        public string SmtpServerMailFrom {
            get {
                return ((string)(this["SmtpServerMailFrom"]));
            }
            set {
                this["SmtpServerMailFrom"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("2015Ajylrg")]
        public string SmtpServerPswd {
            get {
                return ((string)(this["SmtpServerPswd"]));
            }
            set {
                this["SmtpServerPswd"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SmtpServerSSL {
            get {
                return ((bool)(this["SmtpServerSSL"]));
            }
            set {
                this["SmtpServerSSL"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("srv-proxy")]
        public string SmtpServerHost {
            get {
                return ((string)(this["SmtpServerHost"]));
            }
            set {
                this["SmtpServerHost"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("bobrovskiy@mrivc.ru;butorin@mrivc.ru")]
        public string SendLogsTo {
            get {
                return ((string)(this["SendLogsTo"]));
            }
            set {
                this["SendLogsTo"] = value;
            }
        }
    }
}
