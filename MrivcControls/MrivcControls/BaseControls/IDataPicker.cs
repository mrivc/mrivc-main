﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MrivcControls
{
    public interface IDataPicker
    {
        string ParameterName { get; set; }
        object DataSource { get; set; }
        string ValueMember { get; set; }
        string DisplayMember { get; set; }
        object SelectedValue { get; }
        string SelectedName { get; }
    }
}
