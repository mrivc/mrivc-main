﻿namespace MassBillNew
{
    partial class SingleBillByClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientsTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.execBtn = new System.Windows.Forms.Button();
            this.periodPicker1 = new MrivcControls.PeriodPicker();
            this.SuspendLayout();
            // 
            // clientsTextBox
            // 
            this.clientsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clientsTextBox.Location = new System.Drawing.Point(12, 104);
            this.clientsTextBox.Multiline = true;
            this.clientsTextBox.Name = "clientsTextBox";
            this.clientsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.clientsTextBox.Size = new System.Drawing.Size(426, 274);
            this.clientsTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Скопируйте список ID_Client\'ов";
            // 
            // execBtn
            // 
            this.execBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.execBtn.Location = new System.Drawing.Point(290, 384);
            this.execBtn.Name = "execBtn";
            this.execBtn.Size = new System.Drawing.Size(148, 23);
            this.execBtn.TabIndex = 2;
            this.execBtn.Text = "Сформировать";
            this.execBtn.UseVisualStyleBackColor = true;
            this.execBtn.Click += new System.EventHandler(this.execBtn_Click);
            // 
            // periodPicker1
            // 
            this.periodPicker1.DataSource = null;
            this.periodPicker1.DisplayMember = "Name";
            this.periodPicker1.Location = new System.Drawing.Point(12, 12);
            this.periodPicker1.Name = "periodPicker1";
            this.periodPicker1.ParameterName = "Период";
            this.periodPicker1.Size = new System.Drawing.Size(209, 60);
            this.periodPicker1.TabIndex = 3;
            this.periodPicker1.ValueMember = "Period";
            // 
            // SingleBillByClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 417);
            this.Controls.Add(this.periodPicker1);
            this.Controls.Add(this.execBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.clientsTextBox);
            this.Name = "SingleBillByClient";
            this.Text = "Тестовые квитанции для Сбербанка";
            this.Load += new System.EventHandler(this.SingleBillByClient_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox clientsTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button execBtn;
        private MrivcControls.PeriodPicker periodPicker1;
    }
}