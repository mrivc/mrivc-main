﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class ExportNsiListForm : Form
    {
        public ExportNsiListForm()
        {
            InitializeComponent();
        }


        private void exportNsiListButton_Click(object sender, EventArgs e)
        {
            var beginDate = DateTime.Now;
            Console.WriteLine($"Начата синхронизация за {beginDate}");

            var exportNsiList = new ExportNsiList();
            var nsiRegNumbers = MrivcStatic.GisConfig.ExportNsiRegistryNumbers;
            exportNsiList.NsiRegistryNumbers = nsiRegNumbers;
            exportNsiList.Execute();

            Console.WriteLine("Синхронизация завершена");
        }

        private void ExportNsiListForm_Load(object sender, EventArgs e)
        {
            pfStart.Fill();
            pfEnd.Fill();
        }

        private void exportNsiProviderBtn_Click(object sender, EventArgs e)
        {
            var beginDate = DateTime.Now;
            Console.WriteLine($"Начата синхронизация за {beginDate}");

            var SenderIdList = MrivcData.SenderList();
            foreach (string senderID in SenderIdList)
            {
                var exportDataProvider = new ExportDataProvider(senderID);
                var dataProviderRegNumbers = "1,51";
                exportDataProvider.ExecuteToDataBase(dataProviderRegNumbers.Split(','));
            }

            Console.WriteLine("Синхронизация завершена");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var beginDate = DateTime.Now;
            Console.WriteLine($"Начата синхронизация за {beginDate}");

            var exportOrgRegistry = new ExportOrgRegistry();
            exportOrgRegistry.Execute();

            Console.WriteLine("Синхронизация завершена");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Предварительная подготовка данных...");
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.PrepareData", new object[,] { { "sPeriod", pfStart.Period }, { "period", pfEnd.Period } });
            Console.WriteLine("Предварительная подготовка данных завершена");
        }
    }
}
