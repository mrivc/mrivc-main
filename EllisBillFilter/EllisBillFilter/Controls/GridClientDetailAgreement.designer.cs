﻿namespace EllisBillFilter
{
    partial class GridClientDetailAgreement
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridClientDetailAgreements = new EllisBillFilter.BaseGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miCloseAgreement = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpenAgreement = new System.Windows.Forms.ToolStripMenuItem();
            this.vClientDetailAgreementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.gridViewClientDetailAgreement = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Client_Detail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Agreement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreement_Number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreement_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBegin_Period = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colEnd_Period = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrincipal1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrincipal2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridClientDetailAgreements)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vClientDetailAgreementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientDetailAgreement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridClientDetailAgreements
            // 
            this.gridClientDetailAgreements.ContextMenuStrip = this.contextMenuStrip1;
            this.gridClientDetailAgreements.DataSource = this.vClientDetailAgreementBindingSource;
            this.gridClientDetailAgreements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridClientDetailAgreements.Location = new System.Drawing.Point(0, 0);
            this.gridClientDetailAgreements.MainView = this.gridViewClientDetailAgreement;
            this.gridClientDetailAgreements.Name = "gridClientDetailAgreements";
            this.gridClientDetailAgreements.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riPeriod});
            this.gridClientDetailAgreements.Size = new System.Drawing.Size(585, 192);
            this.gridClientDetailAgreements.TabIndex = 0;
            this.gridClientDetailAgreements.TrimCellValueBeforeValidate = true;
            this.gridClientDetailAgreements.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewClientDetailAgreement});
            this.gridClientDetailAgreements.SaveData += new EllisBillFilter.BaseGrid.SaveDataEventHandler(this.gridClientDetailAgreements_SaveData);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAdd,
            this.miRemove,
            this.toolStripSeparator1,
            this.miCloseAgreement,
            this.miOpenAgreement});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(221, 98);
            // 
            // miAdd
            // 
            this.miAdd.Image = global::EllisBillFilter.Properties.Resources.document_add;
            this.miAdd.Name = "miAdd";
            this.miAdd.Size = new System.Drawing.Size(220, 22);
            this.miAdd.Text = "Добавить договор";
            this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
            // 
            // miRemove
            // 
            this.miRemove.Image = global::EllisBillFilter.Properties.Resources.document_delete;
            this.miRemove.Name = "miRemove";
            this.miRemove.Size = new System.Drawing.Size(220, 22);
            this.miRemove.Text = "Удалить договор";
            this.miRemove.Click += new System.EventHandler(this.miRemove_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(217, 6);
            // 
            // miCloseAgreement
            // 
            this.miCloseAgreement.Image = global::EllisBillFilter.Properties.Resources.period_close;
            this.miCloseAgreement.Name = "miCloseAgreement";
            this.miCloseAgreement.Size = new System.Drawing.Size(220, 22);
            this.miCloseAgreement.Text = "Закрыть текущий договор";
            this.miCloseAgreement.Click += new System.EventHandler(this.miCloseAgreement_Click);
            // 
            // miOpenAgreement
            // 
            this.miOpenAgreement.Image = global::EllisBillFilter.Properties.Resources.period_open;
            this.miOpenAgreement.Name = "miOpenAgreement";
            this.miOpenAgreement.Size = new System.Drawing.Size(220, 22);
            this.miOpenAgreement.Text = "Открыть текущий договор";
            this.miOpenAgreement.Click += new System.EventHandler(this.miOpenAgreement_Click);
            // 
            // vClientDetailAgreementBindingSource
            // 
            this.vClientDetailAgreementBindingSource.DataMember = "VClient_Detail_Agreement";
            this.vClientDetailAgreementBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewClientDetailAgreement
            // 
            this.gridViewClientDetailAgreement.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Client_Detail,
            this.colID_Agreement,
            this.colAgreement_Number,
            this.colAgreement_Date,
            this.colDescription,
            this.colBegin_Period,
            this.colEnd_Period,
            this.colAgent,
            this.colPrincipal1,
            this.colPrincipal2,
            this.colComment,
            this.colCreatedBy,
            this.colCreationDate});
            this.gridViewClientDetailAgreement.GridControl = this.gridClientDetailAgreements;
            this.gridViewClientDetailAgreement.Name = "gridViewClientDetailAgreement";
            this.gridViewClientDetailAgreement.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewClientDetailAgreement.OptionsView.ShowGroupPanel = false;
            this.gridViewClientDetailAgreement.OptionsView.ShowIndicator = false;
            this.gridViewClientDetailAgreement.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewClientDetailAgreement_CellValueChanged);
            // 
            // colID_Client_Detail
            // 
            this.colID_Client_Detail.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colID_Client_Detail.AppearanceCell.Options.UseBackColor = true;
            this.colID_Client_Detail.FieldName = "ID_Client_Detail";
            this.colID_Client_Detail.Name = "colID_Client_Detail";
            this.colID_Client_Detail.OptionsColumn.AllowEdit = false;
            this.colID_Client_Detail.OptionsColumn.ReadOnly = true;
            // 
            // colID_Agreement
            // 
            this.colID_Agreement.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colID_Agreement.AppearanceCell.Options.UseBackColor = true;
            this.colID_Agreement.FieldName = "ID_Agreement";
            this.colID_Agreement.Name = "colID_Agreement";
            this.colID_Agreement.OptionsColumn.AllowEdit = false;
            this.colID_Agreement.OptionsColumn.ReadOnly = true;
            // 
            // colAgreement_Number
            // 
            this.colAgreement_Number.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colAgreement_Number.AppearanceCell.Options.UseBackColor = true;
            this.colAgreement_Number.Caption = "№ договора";
            this.colAgreement_Number.FieldName = "Agreement_Number";
            this.colAgreement_Number.Name = "colAgreement_Number";
            this.colAgreement_Number.OptionsColumn.AllowEdit = false;
            this.colAgreement_Number.OptionsColumn.ReadOnly = true;
            this.colAgreement_Number.Visible = true;
            this.colAgreement_Number.VisibleIndex = 0;
            this.colAgreement_Number.Width = 122;
            // 
            // colAgreement_Date
            // 
            this.colAgreement_Date.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colAgreement_Date.AppearanceCell.Options.UseBackColor = true;
            this.colAgreement_Date.Caption = "Дата договора";
            this.colAgreement_Date.FieldName = "Agreement_Date";
            this.colAgreement_Date.Name = "colAgreement_Date";
            this.colAgreement_Date.OptionsColumn.AllowEdit = false;
            this.colAgreement_Date.OptionsColumn.ReadOnly = true;
            this.colAgreement_Date.Visible = true;
            this.colAgreement_Date.VisibleIndex = 1;
            this.colAgreement_Date.Width = 134;
            // 
            // colDescription
            // 
            this.colDescription.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colDescription.AppearanceCell.Options.UseBackColor = true;
            this.colDescription.Caption = "Описание";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            this.colDescription.Width = 467;
            // 
            // colBegin_Period
            // 
            this.colBegin_Period.Caption = "Действует С*";
            this.colBegin_Period.ColumnEdit = this.riPeriod;
            this.colBegin_Period.FieldName = "Begin_Period";
            this.colBegin_Period.Name = "colBegin_Period";
            this.colBegin_Period.Visible = true;
            this.colBegin_Period.VisibleIndex = 3;
            this.colBegin_Period.Width = 192;
            // 
            // riPeriod
            // 
            this.riPeriod.AutoHeight = false;
            this.riPeriod.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riPeriod.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Period", "Period", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Work_Period", "Is_Work_Period", 86, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.riPeriod.DataSource = this.periodBindingSource;
            this.riPeriod.DisplayMember = "Name";
            this.riPeriod.DropDownRows = 10;
            this.riPeriod.Name = "riPeriod";
            this.riPeriod.NullText = "";
            this.riPeriod.ShowFooter = false;
            this.riPeriod.ShowHeader = false;
            this.riPeriod.ValueMember = "Period";
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // colEnd_Period
            // 
            this.colEnd_Period.Caption = "Действует По";
            this.colEnd_Period.ColumnEdit = this.riPeriod;
            this.colEnd_Period.FieldName = "End_Period";
            this.colEnd_Period.Name = "colEnd_Period";
            this.colEnd_Period.Visible = true;
            this.colEnd_Period.VisibleIndex = 4;
            this.colEnd_Period.Width = 175;
            // 
            // colAgent
            // 
            this.colAgent.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colAgent.AppearanceCell.Options.UseBackColor = true;
            this.colAgent.FieldName = "Agent";
            this.colAgent.Name = "colAgent";
            this.colAgent.OptionsColumn.AllowEdit = false;
            this.colAgent.OptionsColumn.ReadOnly = true;
            // 
            // colPrincipal1
            // 
            this.colPrincipal1.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colPrincipal1.AppearanceCell.Options.UseBackColor = true;
            this.colPrincipal1.FieldName = "Principal1";
            this.colPrincipal1.Name = "colPrincipal1";
            this.colPrincipal1.OptionsColumn.AllowEdit = false;
            this.colPrincipal1.OptionsColumn.ReadOnly = true;
            // 
            // colPrincipal2
            // 
            this.colPrincipal2.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colPrincipal2.AppearanceCell.Options.UseBackColor = true;
            this.colPrincipal2.FieldName = "Principal2";
            this.colPrincipal2.Name = "colPrincipal2";
            this.colPrincipal2.OptionsColumn.AllowEdit = false;
            this.colPrincipal2.OptionsColumn.ReadOnly = true;
            // 
            // colComment
            // 
            this.colComment.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colComment.AppearanceCell.Options.UseBackColor = true;
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colCreatedBy.AppearanceCell.Options.UseBackColor = true;
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            // 
            // colCreationDate
            // 
            this.colCreationDate.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.colCreationDate.AppearanceCell.Options.UseBackColor = true;
            this.colCreationDate.FieldName = "CreationDate";
            this.colCreationDate.Name = "colCreationDate";
            this.colCreationDate.OptionsColumn.AllowEdit = false;
            this.colCreationDate.OptionsColumn.ReadOnly = true;
            // 
            // GridClientDetailAgreement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridClientDetailAgreements);
            this.Name = "GridClientDetailAgreement";
            this.Size = new System.Drawing.Size(585, 192);
            ((System.ComponentModel.ISupportInitialize)(this.gridClientDetailAgreements)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vClientDetailAgreementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientDetailAgreement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BaseGrid gridClientDetailAgreements;
        private System.Windows.Forms.BindingSource vClientDetailAgreementBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewClientDetailAgreement;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Client_Detail;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Agreement;
        private DevExpress.XtraGrid.Columns.GridColumn colBegin_Period;
        private DevExpress.XtraGrid.Columns.GridColumn colEnd_Period;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreement_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAgent;
        private DevExpress.XtraGrid.Columns.GridColumn colPrincipal1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrincipal2;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colCreationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreement_Number;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miAdd;
        private System.Windows.Forms.ToolStripMenuItem miRemove;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private System.Windows.Forms.ToolStripMenuItem miCloseAgreement;
        private System.Windows.Forms.ToolStripMenuItem miOpenAgreement;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}
