﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Payments
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ru");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FrmAuthentication f = new FrmAuthentication();
            if (f.ShowDialog() == DialogResult.OK)
            {
                //try
                //{
                    SplashScreen.ShowSplashScreen();
                    FrmMain frm = new FrmMain();
                    SplashScreen.CloseForm();
                    Application.Run(frm);
                //}
                //catch (Exception ex)
                //{
                //    SplashScreen.CloseForm();
                //    if (ex.Message.IndexOf("COM class") > -1)
                //    {
                //        MessageBox.Show("Необходимо установить COM драйвер кассы. Драйвер находится в папке программы «Driver\\DrvFR_4.10_288.exe».\nДополнительно об установке программы можно прочитать в инструкции «Docums\\admin_guide.doc»." +
                //            "\n\nОшибка: " + ex.Message + "\n\n" + ex.StackTrace);
                //        return;
                //    }

                //    if (ex.InnerException != null)
                //        MessageBox.Show("ОШИБКА: " + ex.Message + "\n\n" + ex.StackTrace + "\n\n" + ex.InnerException.Message);
                //    else
                //        MessageBox.Show("ОШИБКА: " + ex.Message + "\n\n" + ex.StackTrace);
                //}
            }
        }
    }
}
