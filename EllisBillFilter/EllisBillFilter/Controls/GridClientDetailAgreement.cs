﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using DevExpress.XtraGrid.Views.Grid;

namespace EllisBillFilter
{
    public partial class GridClientDetailAgreement : UserControl
    {
        int idClientDetail;
        bool initialized;

        public GridClientDetailAgreement()
        {
            initialized = false;
            InitializeComponent();
        }

        public GridView View
        {
            get { return gridViewClientDetailAgreement; }
        }

        public void LoadData(int idClientDetail)
        {
            this.idClientDetail = idClientDetail;

            mDataSet.VClient_Detail_Agreement.Clear();
            DAccess.DataModule.ClientDetailAgreementSelect(mDataSet.VClient_Detail_Agreement, idClientDetail);

            if (!initialized)
            {
                mDataSet.Period.Merge(DAccess.Instance.Periods);
                mDataSet.Period.Rows[0].Delete();
                gridClientDetailAgreements.InitEx();
            }
            initialized = true;
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            FrmAgreement f = new FrmAgreement(true);
            if (f.ShowDialog() == DialogResult.OK)
            { 
                MDataSet.VClient_Detail_AgreementRow currRow = (MDataSet.VClient_Detail_AgreementRow)mDataSet.VClient_Detail_Agreement.NewRow();
                currRow.ID_Client_Detail = idClientDetail;
                currRow.ID_Agreement = f.AgreementData.ID_Agreement;
                currRow.Agreement_Number = f.AgreementData.Agreement_Number;
                currRow.Agreement_Date = f.AgreementData.Agreement_Date;
                currRow.Description = f.AgreementData.Description;
                currRow.Begin_Period = DAccess.Instance.WorkPeriod;
                currRow.Agent = DAccess.Instance.GetCompanyName(f.AgreementData.ID_Agent1);
                currRow.Principal1 = DAccess.Instance.GetCompanyName(f.AgreementData.ID_Agent2);
                
                try
                {
                    currRow.Principal2 = DAccess.Instance.GetCompanyName(f.AgreementData.ID_Agent3);
                    currRow.Comment = f.AgreementData.Comment;
                }
                catch { }

                try
                {
                    mDataSet.VClient_Detail_Agreement.AddVClient_Detail_AgreementRow(currRow);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("ID_Client_Detail, ID_Agreement"))
                        MessageBox.Show("Выбранный вами договор уже связан с текущим кодом фильтра.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        throw ex;
                }
                UpdateData();
            }
        }

        private void miRemove_Click(object sender, EventArgs e)
        {
            gridClientDetailAgreements.DeleteActiveRow();
        }

        private void UpdateData()
        {
            DAccess.DataModule.ClientDetailAgreementUpdate(mDataSet.VClient_Detail_Agreement);
        }

        private void gridClientDetailAgreements_SaveData()
        {
            UpdateData();
        }

        private void miOpenAgreement_Click(object sender, EventArgs e)
        {
            gridViewClientDetailAgreement.GetDataRow(gridViewClientDetailAgreement.FocusedRowHandle)["End_Period"] = DBNull.Value;
            UpdateData();
        }

        private void miCloseAgreement_Click(object sender, EventArgs e)
        {
            gridViewClientDetailAgreement.GetDataRow(gridViewClientDetailAgreement.FocusedRowHandle)["End_Period"] = DAccess.Instance.WorkPeriod;
            UpdateData();
        }

        private void gridViewClientDetailAgreement_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DataRow r = gridViewClientDetailAgreement.GetDataRow(gridViewClientDetailAgreement.FocusedRowHandle);
            if (r["End_Period"] != DBNull.Value && e.Column.FieldName == "End_Period")
            {
                if ((int)e.Value < (int)r["Begin_Period"])
                {
                    MessageBox.Show("Период закрытия договора должен быть больше либо равен периоду открытия.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    r["End_Period"] = DBNull.Value;
                }
            }
        }
    }
}
