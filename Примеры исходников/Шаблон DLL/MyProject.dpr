library MyProject;

{$R *.res}

uses
  EllisGKH_TLB in 'Uses\EllisGKH_TLB.pas',
  Forms in 'Uses\Forms.pas',
  Graphics in 'Uses\Graphics.pas',
  uLibrary in 'Uses\uLibrary.pas',
  uMDIChild in 'Uses\uMDIChild.pas' {MDIChildForm},
  uMessages in 'Uses\uMessages.pas',
  uProperties in 'Uses\uProperties.pas',
  uPropertyCollection in 'Uses\uPropertyCollection.pas',
  uReportUtils in 'Uses\uReportUtils.pas',
  uMyUnit in 'uMyUnit.pas' {frmMyUnit};

Procedure Execute(AOptions: TPropertyCollection);
Begin
  uLibrary.ExecuteMDIChild(AOptions, TfrmMyUnit);
End;

Exports Execute;

begin
end.
