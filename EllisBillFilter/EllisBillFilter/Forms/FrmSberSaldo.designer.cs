﻿namespace EllisBillFilter
{
    partial class FrmSberSaldo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSberSaldo));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbTypeDebt = new System.Windows.Forms.RadioButton();
            this.rbTypeCurrent = new System.Windows.Forms.RadioButton();
            this.cbPeriod = new System.Windows.Forms.ComboBox();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.clClients = new System.Windows.Forms.CheckedListBox();
            this.lblClients = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDeriveCompany = new System.Windows.Forms.ComboBox();
            this.deriveCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.cbBillFilter = new System.Windows.Forms.ComboBox();
            this.billFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbDBF = new System.Windows.Forms.CheckBox();
            this.cbSber = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtClientNames = new System.Windows.Forms.TextBox();
            this.btnFilePath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.saldoHouseFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ddHouseFilter = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cbShowOnlyPrintedClients = new System.Windows.Forms.CheckBox();
            this.btnSettings = new System.Windows.Forms.LinkLabel();
            this.rbOnlyUninhabitedAccounts = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saldoHouseFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddHouseFilter.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbOnlyUninhabitedAccounts);
            this.groupBox1.Controls.Add(this.rbTypeDebt);
            this.groupBox1.Controls.Add(this.rbTypeCurrent);
            this.groupBox1.Location = new System.Drawing.Point(2, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(138, 74);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тип выгрузки:";
            // 
            // rbTypeDebt
            // 
            this.rbTypeDebt.AutoSize = true;
            this.rbTypeDebt.Location = new System.Drawing.Point(11, 35);
            this.rbTypeDebt.Name = "rbTypeDebt";
            this.rbTypeDebt.Size = new System.Drawing.Size(75, 17);
            this.rbTypeDebt.TabIndex = 2;
            this.rbTypeDebt.Text = "Долговая";
            this.rbTypeDebt.UseVisualStyleBackColor = true;
            this.rbTypeDebt.CheckedChanged += new System.EventHandler(this.rbTypeDebt_CheckedChanged);
            // 
            // rbTypeCurrent
            // 
            this.rbTypeCurrent.AutoSize = true;
            this.rbTypeCurrent.Checked = true;
            this.rbTypeCurrent.Location = new System.Drawing.Point(11, 18);
            this.rbTypeCurrent.Name = "rbTypeCurrent";
            this.rbTypeCurrent.Size = new System.Drawing.Size(70, 17);
            this.rbTypeCurrent.TabIndex = 1;
            this.rbTypeCurrent.TabStop = true;
            this.rbTypeCurrent.Text = "Текущая";
            this.rbTypeCurrent.UseVisualStyleBackColor = true;
            // 
            // cbPeriod
            // 
            this.cbPeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbPeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPeriod.DataSource = this.periodBindingSource;
            this.cbPeriod.DisplayMember = "Name";
            this.cbPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPeriod.FormattingEnabled = true;
            this.cbPeriod.Location = new System.Drawing.Point(9, 31);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Size = new System.Drawing.Size(209, 21);
            this.cbPeriod.TabIndex = 1;
            this.cbPeriod.ValueMember = "Period";
            this.cbPeriod.SelectedIndexChanged += new System.EventHandler(this.cbPeriod_SelectedIndexChanged);
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clClients
            // 
            this.clClients.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.clClients.FormattingEnabled = true;
            this.clClients.Location = new System.Drawing.Point(382, 73);
            this.clClients.Name = "clClients";
            this.clClients.Size = new System.Drawing.Size(378, 154);
            this.clClients.TabIndex = 2;
            this.clClients.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clClients_ItemCheck);
            this.clClients.SelectedIndexChanged += new System.EventHandler(this.clClients_SelectedIndexChanged);
            // 
            // lblClients
            // 
            this.lblClients.AutoSize = true;
            this.lblClients.Location = new System.Drawing.Point(379, 36);
            this.lblClients.Name = "lblClients";
            this.lblClients.Size = new System.Drawing.Size(84, 13);
            this.lblClients.TabIndex = 3;
            this.lblClients.Text = "Клиенты (100): ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbPeriod);
            this.groupBox2.Location = new System.Drawing.Point(146, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(227, 74);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Период:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.cbDeriveCompany);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(5, 85);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(371, 53);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Для долговых квитанций:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(8, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Управляющая компания:";
            // 
            // cbDeriveCompany
            // 
            this.cbDeriveCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbDeriveCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbDeriveCompany.DataSource = this.deriveCompanyBindingSource;
            this.cbDeriveCompany.DisplayMember = "Name";
            this.cbDeriveCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeriveCompany.FormattingEnabled = true;
            this.cbDeriveCompany.Location = new System.Drawing.Point(150, 21);
            this.cbDeriveCompany.Name = "cbDeriveCompany";
            this.cbDeriveCompany.Size = new System.Drawing.Size(215, 21);
            this.cbDeriveCompany.TabIndex = 0;
            this.cbDeriveCompany.ValueMember = "ID_Company";
            // 
            // deriveCompanyBindingSource
            // 
            this.deriveCompanyBindingSource.DataMember = "DeriveCompany";
            this.deriveCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(14, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Фильтр по услугам:";
            // 
            // cbBillFilter
            // 
            this.cbBillFilter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbBillFilter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbBillFilter.DataSource = this.billFilterBindingSource;
            this.cbBillFilter.DisplayMember = "Bill_Filter";
            this.cbBillFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBillFilter.FormattingEnabled = true;
            this.cbBillFilter.Location = new System.Drawing.Point(155, 144);
            this.cbBillFilter.Name = "cbBillFilter";
            this.cbBillFilter.Size = new System.Drawing.Size(215, 21);
            this.cbBillFilter.TabIndex = 6;
            this.cbBillFilter.ValueMember = "ID_Bill_Filter";
            this.cbBillFilter.SelectedIndexChanged += new System.EventHandler(this.cbBillFilter_SelectedIndexChanged);
            // 
            // billFilterBindingSource
            // 
            this.billFilterBindingSource.DataMember = "BillFilter";
            this.billFilterBindingSource.DataSource = this.mDataSet;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(13, 242);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(111, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "Сформировать";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(130, 242);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(168, 205);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(179, 20);
            this.txtPath.TabIndex = 8;
            this.txtPath.Text = "c:\\";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Путь к папке для выгрузки:";
            // 
            // cbDBF
            // 
            this.cbDBF.AutoSize = true;
            this.cbDBF.Location = new System.Drawing.Point(211, 246);
            this.cbDBF.Name = "cbDBF";
            this.cbDBF.Size = new System.Drawing.Size(47, 17);
            this.cbDBF.TabIndex = 10;
            this.cbDBF.Text = "DBF";
            this.cbDBF.UseVisualStyleBackColor = true;
            // 
            // cbSber
            // 
            this.cbSber.AutoSize = true;
            this.cbSber.Location = new System.Drawing.Point(264, 246);
            this.cbSber.Name = "cbSber";
            this.cbSber.Size = new System.Drawing.Size(47, 17);
            this.cbSber.TabIndex = 11;
            this.cbSber.Text = "TXT";
            this.cbSber.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(469, 33);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(291, 20);
            this.textBox2.TabIndex = 12;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtClientNames
            // 
            this.txtClientNames.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtClientNames.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtClientNames.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtClientNames.ForeColor = System.Drawing.Color.Gray;
            this.txtClientNames.Location = new System.Drawing.Point(382, 232);
            this.txtClientNames.Multiline = true;
            this.txtClientNames.Name = "txtClientNames";
            this.txtClientNames.Size = new System.Drawing.Size(377, 53);
            this.txtClientNames.TabIndex = 13;
            // 
            // btnFilePath
            // 
            this.btnFilePath.Location = new System.Drawing.Point(351, 203);
            this.btnFilePath.Name = "btnFilePath";
            this.btnFilePath.Size = new System.Drawing.Size(24, 23);
            this.btnFilePath.TabIndex = 14;
            this.btnFilePath.Text = "...";
            this.btnFilePath.UseVisualStyleBackColor = true;
            this.btnFilePath.Click += new System.EventHandler(this.btnFilePath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Фильтр по домам:";
            // 
            // saldoHouseFilterBindingSource
            // 
            this.saldoHouseFilterBindingSource.DataMember = "SaldoHouseFilter";
            this.saldoHouseFilterBindingSource.DataSource = this.mDataSet;
            // 
            // ddHouseFilter
            // 
            this.ddHouseFilter.Location = new System.Drawing.Point(155, 173);
            this.ddHouseFilter.Name = "ddHouseFilter";
            this.ddHouseFilter.Properties.AutoHeight = false;
            this.ddHouseFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddHouseFilter.Properties.DataSource = this.saldoHouseFilterBindingSource;
            this.ddHouseFilter.Properties.DisplayMember = "house_address";
            this.ddHouseFilter.Properties.DropDownRows = 20;
            this.ddHouseFilter.Properties.PopupFormSize = new System.Drawing.Size(350, 200);
            this.ddHouseFilter.Properties.SelectAllItemCaption = "Выбрать все";
            this.ddHouseFilter.Properties.ValueMember = "id_house";
            this.ddHouseFilter.Size = new System.Drawing.Size(218, 21);
            this.ddHouseFilter.TabIndex = 17;
            // 
            // cbShowOnlyPrintedClients
            // 
            this.cbShowOnlyPrintedClients.AutoSize = true;
            this.cbShowOnlyPrintedClients.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.cbShowOnlyPrintedClients.Location = new System.Drawing.Point(382, 10);
            this.cbShowOnlyPrintedClients.Name = "cbShowOnlyPrintedClients";
            this.cbShowOnlyPrintedClients.Size = new System.Drawing.Size(257, 17);
            this.cbShowOnlyPrintedClients.TabIndex = 18;
            this.cbShowOnlyPrintedClients.Text = "показать клиентов только по истории печати";
            this.cbShowOnlyPrintedClients.UseVisualStyleBackColor = false;
            this.cbShowOnlyPrintedClients.CheckedChanged += new System.EventHandler(this.cbShowOnlyPrintedClients_CheckedChanged);
            // 
            // btnSettings
            // 
            this.btnSettings.AutoSize = true;
            this.btnSettings.Location = new System.Drawing.Point(13, 272);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(170, 13);
            this.btnSettings.TabIndex = 19;
            this.btnSettings.TabStop = true;
            this.btnSettings.Text = "Авторассылка реестров сальдо";
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // rbOnlyUninhabitedAccounts
            // 
            this.rbOnlyUninhabitedAccounts.AutoSize = true;
            this.rbOnlyUninhabitedAccounts.Location = new System.Drawing.Point(11, 52);
            this.rbOnlyUninhabitedAccounts.Name = "rbOnlyUninhabitedAccounts";
            this.rbOnlyUninhabitedAccounts.Size = new System.Drawing.Size(91, 17);
            this.rbOnlyUninhabitedAccounts.TabIndex = 3;
            this.rbOnlyUninhabitedAccounts.Text = "Нежилые ЛС";
            this.rbOnlyUninhabitedAccounts.UseVisualStyleBackColor = true;
            // 
            // FrmSberSaldo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 292);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbBillFilter);
            this.Controls.Add(this.cbShowOnlyPrintedClients);
            this.Controls.Add(this.clClients);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ddHouseFilter);
            this.Controls.Add(this.cbDBF);
            this.Controls.Add(this.lblClients);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbSber);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnFilePath);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtClientNames);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(780, 283);
            this.Name = "FrmSberSaldo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Реестры сальдо (кодов авторизации)";
            this.Load += new System.EventHandler(this.FrmSberSaldo_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saldoHouseFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddHouseFilter.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbTypeDebt;
        private System.Windows.Forms.RadioButton rbTypeCurrent;
        private System.Windows.Forms.ComboBox cbPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private DS.MDataSet mDataSet;
        private System.Windows.Forms.CheckedListBox clClients;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbBillFilter;
        private System.Windows.Forms.ComboBox cbDeriveCompany;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource deriveCompanyBindingSource;
        private System.Windows.Forms.BindingSource billFilterBindingSource;
        private System.Windows.Forms.CheckBox cbDBF;
        private System.Windows.Forms.CheckBox cbSber;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtClientNames;
        private System.Windows.Forms.Button btnFilePath;
        private System.Windows.Forms.Label lblClients;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource saldoHouseFilterBindingSource;
        private DevExpress.XtraEditors.CheckedComboBoxEdit ddHouseFilter;
        private System.Windows.Forms.CheckBox cbShowOnlyPrintedClients;
        private System.Windows.Forms.LinkLabel btnSettings;
        private System.Windows.Forms.RadioButton rbOnlyUninhabitedAccounts;
    }
}