﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Payments.Controls;
using Payments.LogicClasses;
using DevExpress.XtraGrid.Views.Grid;
using Payments.DS;
using System.Threading;

namespace Payments
{
    public partial class FrmPayServices : Form, IPayForm
    {
        PaymentClass paymentClass;
        GridView paymentDetailGridView;
        public bool NoCalc { get; set; } = false;

        public FrmPayServices(PaymentClass paymentClass)
        {
            this.paymentClass = paymentClass;
            InitializeComponent();
            if (paymentClass.IdPaymentClass != 4 && paymentClass.IdPaymentClass != 6 && paymentClass.IdPaymentClass != 7) //если не поверка,алые паруса и север сервис
                panelPayment1.DisableSummField();
            paymentDetailGridView = gridPaymentParameter1.PaymentDetailGridView;

            paymentDetailGridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(paymentDetailGridView_CellValueChanged);
        }

        void paymentDetailGridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "Parameter_Quantity" || e.Column.FieldName == "Parameter_Value")
                if (!NoCalc)
                    panelPayment1.PaymentSumm = RecalculatePrice();
        }

        private decimal RecalculatePrice()
        {
            decimal summ = 0;
            decimal value;
            PaymentsDataSet ds = ((BindingSource)paymentDetailGridView.DataSource).DataSource as PaymentsDataSet;
            if (ds != null)
            {
                foreach (PaymentsDataSet.PaymentParameterDetailRow row in ds.PaymentParameterDetail.Rows)
                {
                    if (decimal.TryParse(row.Parameter_Value, out value) &&
                            row["Parameter_Quantity"] != DBNull.Value)
                        summ += (value * (decimal)row.Parameter_Quantity);
                }
            }
            return summ;
        }

        public PanelPayment PaymentInfo
        {
            get
            {
                return panelPayment1;
            }
        }

        public Form Window {
            get { return this; }
        }

        public PaymentClass PaymentClass
        {
            get { return paymentClass; }
        }

        private bool panelPayment1_PayEvent(Controls.PanelPayment sender, EventArgs e)
        {
            //если оплата кредиткой
            if (sender.CashType == 2)
            {
                return PayByVisaCard();
            }

            if (cbSplitPaymentByServices.Checked)
                return PayInSeparetedChecksByServices();
            else
                return PayInOneCheck();
        }

        private bool PayByVisaCard()
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
            return true;
        }

        private bool PayInOneCheck()
        {
            List<PaymentParameterDatail> ppdList = gridPaymentParameter1.GetParameterWithQuantity();
            List<string> freeStrings = new List<string>();

            foreach (PaymentParameterDatail p in ppdList)
                freeStrings.Add(p.PrintName + " " + p.ParameterValue + "р*" + p.ParameterQuantity + " =" + (int.Parse(p.ParameterValue) * p.ParameterQuantity).ToString() + "р");



            

            if (paymentClass.IdPaymentClass == 4)
            {
                var list = new List<string>();
                list.Add(paymentClass.Name);
                list.Add("Ф.И.О.: "+gridPaymentParameter1.GetValue("FIO_POVERKA").ToString().Trim());
                list.Add("Адрес: " + gridPaymentParameter1.GetValue("ADRES_POVERKA").ToString().Trim());
                list.Add("Кол-во приборов: " + gridPaymentParameter1.GetValue("METERS_COUNT_POVERKA").ToString().Trim());
                list.Add("Телефон: " + gridPaymentParameter1.GetValue("PHONE_POVERKA").ToString().Trim());

               freeStrings.AddRange(list);
            }

            if (paymentClass.IdPaymentClass == 6)
            {
                var list = new List<string>();
                list.Add(paymentClass.Name);
                list.Add("Ф.И.О.: " + gridPaymentParameter1.GetValue("FIO_ALPAR").ToString().Trim());
                list.Add("Адрес: " + gridPaymentParameter1.GetValue("ADRES_ALPAR").ToString().Trim());
                list.Add("Номер договора: " + gridPaymentParameter1.GetValue("NOM_DOG").ToString().Trim());

                freeStrings.AddRange(list);
            }

            if (paymentClass.IdPaymentClass == 7)
            {
                var list = new List<string>();
                list.Add(paymentClass.Name);
                list.Add("Ф.И.О.: " + gridPaymentParameter1.GetValue("FIO_SERGKH").ToString().Trim());
                list.Add("Адрес: " + gridPaymentParameter1.GetValue("ADRES_SERGKH").ToString().Trim());

                freeStrings.AddRange(list);
            }

            var taxGroup = 0;
            if (paymentClass.IdPaymentClass == 2) //если услугаy наша то ндс 20
            {
                taxGroup = 1;
            }

            if (DAccess.CashTerminal.Sale(panelPayment1.PaymentSumm, panelPayment1.PaymentCash, 1,
                paymentClass.Name, freeStrings, paymentClass.Name, panelPayment1.PhoneEmail, Properties.Settings.Default.Department_Number, paymentClass.MakeCheckCopy, taxGroup))
            {

                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
                return true;
            }
            else
                return false;
        }

        private bool PayInSeparetedChecksByServices()
        {
            List<PaymentParameterDatail> ppdList = gridPaymentParameter1.GetParameterWithQuantity();
            List<string> freeStrings = new List<string>();

            decimal paymentSumm = 0;
            foreach (PaymentParameterDatail p in ppdList)
                paymentSumm += (decimal.Parse(p.ParameterValue) * p.ParameterQuantity);

            decimal paymentCash = 0;
            int i = 1;
            foreach (PaymentParameterDatail p in ppdList)
            {
                freeStrings.Clear();
                freeStrings.Add(p.PrintName + " " + p.ParameterValue + "р*" + p.ParameterQuantity + " =" + (int.Parse(p.ParameterValue) * p.ParameterQuantity).ToString() + "р");
                freeStrings.Add("");

                //всю сдачу пихаем в последний чек
                if (ppdList.Count == i)
                    paymentCash = panelPayment1.PaymentCash - paymentSumm + (decimal.Parse(p.ParameterValue) * p.ParameterQuantity);
                else
                    paymentCash = decimal.Parse(p.ParameterValue) * p.ParameterQuantity;

                Thread.Sleep(3000);
                DAccess.CashTerminal.Sale(decimal.Parse(p.ParameterValue), paymentCash, (double)p.ParameterQuantity,
                    paymentClass.Name, freeStrings, p.PrintName, panelPayment1.PhoneEmail, Properties.Settings.Default.Department_Number, paymentClass.MakeCheckCopy, 1);

                //MessageBox.Show(paymentCash.ToString());
                i++;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
            return true;
        }

        public void SavePaymentDetail()
        {
            if (paymentClass.IdPayment == -1)
                throw new Exception("Нельзя сохранить делали платежа в таблицу \"dbo.Payment_Parameter_Detail\", т.к. идентификатор платежа ID_Payment = -1");
            gridPaymentParameter1.SaveData();
        }

        private void FrmPayServices_Shown(object sender, EventArgs e)
        {
            gridPaymentParameter1.LoadLayout("gridPaymentParameterServices");
            gridPaymentParameter1.LoadData(paymentClass);
            if (PaymentClass.IdPaymentClass == 4 || PaymentClass.IdPaymentClass == 6 || PaymentClass.IdPaymentClass == 7)
            {
                NoCalc = true;
                cbSplitPaymentByServices.Checked = false;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Enter | Keys.Control))
            {
                panelPayment1.DoPay(new EventArgs());
                return true;
            }
            if (keyData == (Keys.D1 | Keys.Control))
            {
                DAccess.CashTerminal.OpenDrawer();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void panelPayment1_CommentFieldLostFocusEvent(PanelPayment sender, EventArgs e)
        {
            gridPaymentParameter1.Focus();
        }

        private void FrmPayServices_FormClosing(object sender, FormClosingEventArgs e)
        {
            gridPaymentParameter1.SaveLayout();
        }

        private void cbSplitPaymentByServices_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSplitPaymentByServices.Checked)
            {
                panelPayment1.DisableSummField();
            }
        }

        private void panelPayment1_Load(object sender, EventArgs e)
        {

        }
    }
}
