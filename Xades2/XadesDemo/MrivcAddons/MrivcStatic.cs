﻿using System;
using System.Configuration;
using System.Text;
using System.Xml;
using XadesDemo.Configurations.Sections;
using XadesDemo.Infrastructure;
using System.Data;
using System.IO;
using Xades.Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using MrivcControls.Data;
using MrivcControls;
using MrivcControls.Helpers;
using FastReport;

namespace XadesDemo
{
    public static class MrivcStatic
    {
        

        static GisServiceConfiguration gisConfig = (GisServiceConfiguration)ConfigurationManager.GetSection(Constants.GisServicesConfigSectionName);
        static string output = "";
        public static string Output
        {
            get
            {
                var _output = output;
                output = "";
                return _output;
            }
        }

        public static string CurrentSoapRequest { get; set; }

        public static GisServiceConfiguration GisConfig {
            get
            {
                return gisConfig;
            }
        }

        public static bool isRSO(string senderID)
        {
            return (bool)DAccess.DataModule.ExecuteScalarQueryCommand1("select isRso from gis.Provider where senderID = @senderID", new[,] { { "senderID", senderID } });
        }

        public static void WriteLogToConsole(DateTime beginDate)
        {
            Console.ForegroundColor = ConsoleColor.Gray;

            var logTable = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.LogPrint", new object[,] { { "beginDate", beginDate } });

            foreach (DataRow row in logTable.Rows)
            {
                if (int.Parse(row[1].ToString()) == 0)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                }

                var message = row[0].ToString();
                //var bytes = Encoding.GetEncoding(866).GetBytes(message);
                //message = Convert.ToString(bytes);

                Console.WriteLine(message);

                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        public static void WriteLog(MrivcCommandException mcex,bool addToReport, string description, string houseGuid)
        {
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.WriteLog",
                       new object[,] {
                            { "@senderID", mcex.SenderID },
                            { "@serviceName", mcex.Service },
                            { "@methodName", mcex.Method },
                            { "@houseGUID", houseGuid},
                            { "@message", mcex.Message },
                            { "@code", mcex.Code },
                            { "@sendRequest", mcex.SendRequest },
                            { "@messageGUID", mcex.MessageGUID },
                            { "@description", description },
                            { "@addToReport", addToReport },
                            { "@response", mcex.Response }
                       });
        }

        public static void WriteLog(MrivcCommandException mcex, bool addToReport, string description)
        {
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.WriteLog",
                       new object[,] {
                            { "@senderID", mcex.SenderID },
                            { "@serviceName", mcex.Service },
                            { "@methodName", mcex.Method },
                            { "@message", mcex.Message },
                            { "@code", mcex.Code },
                            { "@sendRequest", mcex.SendRequest },
                            { "@messageGUID", mcex.MessageGUID },
                            { "@description", description },
                            { "@addToReport", addToReport },
                            { "@response", mcex.Response }
                       });
        }

        public static void WriteLog(MrivcCommand command)
        {
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.WriteLog",
                       new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@description", command.Description },
                            { "@addToReport", command.AddToReport }
                       });
        }

        public static void WriteLog(MrivcCommand command, Exception ex)
        {
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.WriteLog",
                       new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@description", command.Description },
                            { "@addToReport", command.AddToReport },
                            { "@message", ex.Message }
                       });
        }

        public static void WriteLog(MrivcCommand command, Exception ex, string houseGuid)
        {
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.WriteLog",
                       new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@houseGUID", houseGuid},
                            { "@description", command.Description },
                            { "@addToReport", false },
                           { "@message", ex.Message }
                       });
        }

        public static void WriteLog(Exception ex)
        {
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.WriteLog",
                       new object[,] {
                            { "@senderID", null },
                            { "@serviceName", null },
                            { "@methodName", null },
                            { "@addToReport", false },
                           { "@message", ex.Message }
                       });
        }

        public static void WriteLog(MrivcCommand command, string houseGuid)
        {
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.WriteLog",
                       new object[,] {
                            { "@senderID", command.SenderID },
                            { "@serviceName", command.Service },
                            { "@methodName", command.Method },
                            { "@houseGUID", houseGuid},
                            { "@description", command.Description },
                            { "@addToReport", command.AddToReport }
                       });
        }

        public static Tuple<string,string> GetErrorFromResponse(string response)
        {
            
            var errCode = MrivcStatic.SelectNodeByName(MrivcStatic.ResultNodeName("ErrorCode"), response);

            if (errCode != null)
            {
                var err = MrivcStatic.SelectNodeByName(MrivcStatic.ResultNodeName("Description"), response);

                if (err == null)
                err = MrivcStatic.SelectNodeByName(MrivcStatic.ResultNodeName("ErrorMessage"), response);

                return new Tuple<string, string>(errCode.InnerText, err.InnerText);
            }
               
            else return null;
        }

        public static void AddToOutput (params string[] lines)
        {
            foreach (var _string in lines)
            {
                output += _string;
                output += Environment.NewLine;
            } 
        }

        public static void ClearOutput()
        {
            output = "";
        }

        public static string[] PrepareSendCommandArgs(string service, string methodName)
        {
            return new string[]{"send","-s",service,"-m",methodName,"-c",$"{methodName} request.csv","-o",$"{methodName} response.csv","-a","lanit:tv,n8!Ya"};
        }

        public static string[] PrepareGetStateCommandArgs(string service, string methodName, string messageGuid)
        {
            return new string[] { "get-state", "-s", service,"-g", messageGuid , "-o", $"{methodName} response.csv", "-a", "lanit:tv,n8!Ya" };
        }

        public static bool IsLastElement(XmlNode node)
        {
            if (node.ChildNodes.Count == 0 || (node.ChildNodes.Count == 1 && node.ChildNodes[0].NodeType == XmlNodeType.Text))
                return true;
            else
                return false;
        }

        public static string FindXPath(XmlNode node)
        {
            StringBuilder builder = new StringBuilder();
            while (node != null)
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Attribute:
                        builder.Insert(0, "/@" + node.LocalName);
                        node = ((XmlAttribute)node).OwnerElement;
                        break;
                    case XmlNodeType.Element:
                        builder.Insert(0, "/" + node.LocalName);
                        node = node.ParentNode;
                        break;
                    case XmlNodeType.Document:
                        return builder.ToString();
                    default:
                        throw new ArgumentException("Only elements and attributes are supported");
                }
            }
            throw new ArgumentException("Node was not in a document");
        }

        public static string GetMessageGuidBySendResponse(string sendResponse)
        {
            try
            {
                var ackGuidNode = SelectNodeByName("//*[local-name()='AckRequest']/*[local-name()='Ack']", sendResponse);
                var messageGuidNode = SelectNodeByName("//*[local-name()='MessageGUID']", ackGuidNode.OuterXml);
                return messageGuidNode.InnerText;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        public static string GetMessageGuidByGetStateResponse(string getStateResponse)
        {
            try
            {
                var gsNode = SelectNodeByName("//*[local-name()='getStateRequest']", getStateResponse);
                var messageGuidNode = SelectNodeByName("//*[local-name()='MessageGUID']", gsNode.OuterXml);
                return messageGuidNode.InnerText;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public static string ReplaceTransportGUID(string xmlBody)
        {
            var xd = new XmlDocument();
            xd.InnerXml = xmlBody;
            var tg = xd.SelectNodes(NodePathWithoutPrefix("TransportGUID"));
            foreach (XmlNode item in tg)
            {
                if (item.ParentNode.LocalName != "PaymentInformation")
                    item.InnerText = Guid.NewGuid().ToString("D");
            }
            return xd.InnerXml;
        }

        public static void RemoveNodesByName(string nodeName, ref string xmlText)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlText);
            var xm = xd.CreateNamespaceManager();
            XmlNodeList xnl = xd.SelectNodes(NodePathWithoutPrefix(nodeName), xm);

            foreach (XmlNode item in xnl)
            {
                var pn = item.ParentNode;
                //item.RemoveAll();
                pn.RemoveChild(item);
            }

            xmlText = xd.InnerXml;
        }

        public static XmlNode SelectNodeByName(string nodeName, string xmlText)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlText);
            var xm = xd.CreateNamespaceManager();
            return xd.SelectSingleNode(nodeName, xm);

        }

        public static XmlNodeList SelectNodesByName(string nodeName, string xmlText)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlText);
            var xm = xd.CreateNamespaceManager();
            return xd.SelectNodes(nodeName, xm);
        }

        public static string ResultNodeName(string nodeName)
        {
            return $"//*[local-name()='{nodeName}']"; 
        }

        public static string NodePathWithoutPrefix(params string[] nodeNames)
        {
            var res = "/";
            foreach (var nodeName in nodeNames)
            {
                res += $"/*[local-name()='{nodeName}']";
            }
            return res;
        }

        public static string PrepareXmlBody(string service, string method, IEnumerable<Tuple<string[],string>> valuePairs)
        {
            var templatePath = GisConfig.Services[service].Methods[method].Template;
            var template = File.ReadAllText(templatePath);

            var xd = new XmlDocument();
            xd.LoadXml(template);

            foreach (Tuple<string[],string> value in valuePairs)
            {
                var node = xd.SelectSingleNode(NodePathWithoutPrefix(value.Item1));
                node.InnerText = value.Item2;
            }

            return xd.InnerXml;
        }

        public static List<DataSet> HouseIteration(IEnumerable<ISingleHouseCommand> singleHouseCommandSet, string senderID, bool preview)
        {
            var resultSet = new List<DataSet>();

            var houseGuidList = MrivcData.HouseFiasGuidList(senderID);

            resultSet.Add(new DataSet());
            var ds = resultSet.Last();

            foreach (string houseGuid in houseGuidList)
            {
                foreach (ISingleHouseCommand singleHouseCommand in singleHouseCommandSet)
                {
                    var beginDate = DateTime.Now;
                    try
                    {
                            singleHouseCommand.FiasHouseGuid = houseGuid;
                            if (preview)
                            {
                                if (ds.Tables.Count == 0)
                                {
                                    ds = singleHouseCommand.PrepareToDataSet();
                                }
                                else
                                {
                                    ds.Merge(singleHouseCommand.PrepareToDataSet());
                                }
                            }
                            else
                                singleHouseCommand.Execute();
                    }
                    catch (MrivcCommandException mcex)
                    {
                        MrivcStatic.WriteLog(mcex,false, ((MrivcCommand)singleHouseCommand).Description);
                    }
                    catch (Exception ex)
                    {
                        MrivcStatic.WriteLog(((MrivcCommand)singleHouseCommand),ex,houseGuid);
                    }

                    MrivcStatic.WriteLogToConsole(beginDate);
                }
            }
            return resultSet;

        }

        public static string IndentXml (string xml)
        {
            XmlWriterSettings ws = new XmlWriterSettings() { Indent = true , OmitXmlDeclaration = true};
            var xe = XElement.Parse(xml);

            var sb = new StringBuilder();
            using (XmlWriter xw = XmlWriter.Create(sb, ws))
                xe.Save(xw);

            return sb.ToString();
        }

        public static DataSet HouseIteration(ISingleHouseCommand singleHouseCommand, bool preview)
        {
            var houseGuidList = MrivcData.HouseFiasGuidList(singleHouseCommand.SenderID);

            var ds = new DataSet();

            foreach (string houseGuid in houseGuidList)
            {
               singleHouseCommand.FiasHouseGuid = houseGuid;
               if (preview)
               {
                    if (ds.Tables.Count == 0)
                    {
                        ds = singleHouseCommand.PrepareToDataSet();
                    }
                    else
                    {
                        ds.Merge(singleHouseCommand.PrepareToDataSet());
                    }
                 }
                 else
                 singleHouseCommand.Execute();
              }

            return ds;
        }


        public static void SendCommandReportToAdmin(DateTime beginDate)
        {
            var emailSender = new EmailSender();
            emailSender.SmtpServerHost = "srv-proxy";
            emailSender.SmtpServerSSL = false;
            emailSender.SmtpServerMailFrom = "gis@mrivc.ru";
            emailSender.SmtpServerMailLogin = "gis@mrivc.ru";
            emailSender.SmtpServerPswd = "c579ltySyfwK";
            emailSender.SmtpServerPort = 25;

            var email = MrivcStatic.GisConfig.AdminEmails;

            var eExcelPath = $"test\\Ошибки {FileHelper.CreateCorrectFileName($"{beginDate} {DateTime.Now}")}.xlsx";
            var sExcelPath = $"test\\Успешно {FileHelper.CreateCorrectFileName($"{beginDate} {DateTime.Now}")}.xlsx";

            var eExcel = DAccess.DataModule.CreateGisCommandExcelReport(null, beginDate, false, eExcelPath);
            var sExcel = DAccess.DataModule.CreateGisCommandExcelReport(null, beginDate, true, sExcelPath);

            var paths = new List<string>();

            var mailBody = "";

            if (eExcel)
            {
                paths.Add(eExcelPath);
            }

            if (sExcel)
            {
                paths.Add(sExcelPath);
            }

            if (paths.Count != 0)
                emailSender.Send(email, paths.ToArray(), "МУП \"МРИВЦ\" Отчет по интеграции с ГИС \"ЖКХ\" за " + beginDate.ToShortDateString(), mailBody);
        }

        public static void PrepareExcelDescriptionReport(string senderID, DateTime beginDate, string savePath)
        {
            var es = new EnvironmentSettings();
            es.ReportSettings.ShowProgress = false;

            var path = Directory.GetCurrentDirectory() + "//_GISCommandReport.FRX";
            var report = new Report();
            report.Load(path);
            report.SetParameterValue("MainConnection", MrivcData.ConnectionString);
            report.SetParameterValue("id_company", senderID);
            report.SetParameterValue("date", beginDate);
            report.Prepare();

            var export = new FastReport.Export.OoXML.Excel2007Export();

            report.Export(export, savePath);
        }

        public static void SendCommandReport(string senderId, DateTime beginDate, DateTime endDate)
        {
            var dt = DAccess.DataModule.GetDataTableByQueryCommand1("select distinct senderID, senderName, email from GIS.VContacts where senderid = @senderId", new[,] { { "senderId", senderId } });

            var emailSender = new EmailSender();
            emailSender.SmtpServerHost = "srv-proxy";
            emailSender.SmtpServerSSL = false;
            emailSender.SmtpServerMailFrom = "gis@mrivc.ru";
            emailSender.SmtpServerMailLogin = "gis@mrivc.ru";
            emailSender.SmtpServerPswd = "c579ltySyfwK";
            emailSender.SmtpServerPort = 25;

            var date = DateTime.Today.ToShortDateString();

            foreach (DataRow row in dt.Rows)
            {
                var senderID = row["senderID"].ToString();
                var senderName = row["senderName"].ToString();
                var email = row["email"].ToString();

                //Текстовый отчет об ошибках
                var eReport = DAccess.DataModule.GisCommandReport(senderID, beginDate, false, endDate);
                var epath = $"test\\Ошибки {FileHelper.CreateCorrectFileName($"{date} {senderName}")}.txt";

                var now = DateTime.Now;

                //Текстовое описание ошибок
                var eReportDescription = DAccess.DataModule.GisErrorDescriptionReport(senderID, beginDate);
                var eDescriptionPath = $"test\\Описание ошибок {FileHelper.CreateCorrectFileName($"{date} {senderName}")}.txt";

                //Excel описание ошибок
                var eExcelPath = $"test\\Excel Описание ошибок {FileHelper.CreateCorrectFileName($"{date} {senderName}")}.xlsx";
                PrepareExcelDescriptionReport(senderID, beginDate, eExcelPath);

                //var sReport = DAccess.DataModule.GisCommandReport(senderID, beginDate, true);
                //var spath = $"test\\Успешно {FileHelper.CreateCorrectFileName($"{beginDate} {senderName}")}.txt";
                //var sExcelPath = $"test\\Успешно {FileHelper.CreateCorrectFileName($"{beginDate} {senderName}")}.xlsx";
                //var sExcel = DAccess.DataModule.CreateGisCommandExcelReport(senderID, beginDate, true, sExcelPath);

                var paths = new List<string>();
                var mailBody = DAccess.DataModule.ExecuteScalarQueryCommand1("select GIS.CreateMailText(@bd)", new object[,] { { "bd", beginDate } }).ToString();

                if (eReport != "")
                {
                    File.WriteAllText(epath, eReport);
                    paths.Add(epath);
                    paths.Add(eExcelPath);
                    //if (eExcelDescription)
                    //{
                    //    paths.Add(eExcelPath);
                    //}
                    if (eReportDescription != "")
                    {
                        File.WriteAllText(eDescriptionPath, eReportDescription);
                        paths.Add(eDescriptionPath);
                    }
                }

                //if (sReport != "")
                //{
                //    File.WriteAllText(spath, sReport);
                //    paths.Add(spath);
                //    //if (sExcel)
                //    //{
                //    //    paths.Add(sExcelPath);
                //    //}
                //}

                if (paths.Count != 0)
                    emailSender.Send(email, paths.ToArray(), "МУП \"МРИВЦ\" Отчет по интеграции с ГИС \"ЖКХ\" " + senderName + " за " + date, mailBody);

                //Список услуг

                var dt1 = DAccess.DataModule.GetDataTableByStoredProcedure1("GIS.ServicesReport", new[,] { { "senderID", senderID } });
                string s = "";
                foreach (DataRow row1 in dt1.Rows)
                {
                    s += row1[0].ToString() + " - " + row1[1].ToString() + Environment.NewLine;
                }

                var srpath = $"test/Список услуг {FileHelper.CreateCorrectFileName($"{date} {senderName}")}.txt";

                paths = new List<string>();
                mailBody = $@"Добрый день." + Environment.NewLine + Environment.NewLine + " Высылаем Вам список услуг для привязки к договорам управления/уставу. Инструкцию по привязке можно скачать по адресу http://mrivc.ru/GIS%20Uslugi.doc " + Environment.NewLine + Environment.NewLine + "По всем вопросам, связанным с интеграцией с ГИС «ЖКХ», просьба обращаться по адресу gis@mrivc.ru." + Environment.NewLine + Environment.NewLine + "Подробные инструкции по работе с услугами вы можете найти на официальном сайте ГИС \"ЖКХ\" dom.gosuslugi.ru в разделе \"Регламенты и инструкции\"." + Environment.NewLine + Environment.NewLine + "Внимание! Синхронизация платежных документов и приборов учета невозможна без привязки услуг к договорам управления\\уставам. Если у Вас уже привязаны данные услуги, то никаких действий не требуется, можете проигнорировать это сообщение." + Environment.NewLine + Environment.NewLine + "Данный отчет сгенерирован системой автоматически.";

                if (s != "")
                {
                    File.WriteAllText(srpath, s);
                    paths.Add(srpath);
                }

                if (paths.Count != 0)
                    emailSender.Send(email, paths.ToArray(), "МУП \"МРИВЦ\" Список услуг для интеграции с ГИС \"ЖКХ\" " + senderName + " за " + date, mailBody);

                //Конец. Список услуг
            }
        }
    }
}
