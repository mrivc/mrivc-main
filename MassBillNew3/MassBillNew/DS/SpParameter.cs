﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data;

namespace MassBillNew
{
    public class SpParameter
    {
        private SpParameterControlType parameterControlType;

        public SpParameter(string name,string description,object defaultValue,GroupControl groupControl, Control control, SpParameterControlType parameterControlType, Control checkBox)
        {
            this.name = name;
            this.description = description;
            this.defaultValue = defaultValue;
            this.groupControl = groupControl;
            this.control = control;
            this.parameterControlType = parameterControlType;
            this.checkBox = checkBox;

        }

        public SpParameter(string name, string description, object defaultValue, GroupControl groupControl, Control control, SpParameterControlType parameterControlType)
        {
            this.name = name;
            this.description = description;
            this.defaultValue = defaultValue;
            this.groupControl = groupControl;
            this.control = control;
            this.parameterControlType = parameterControlType;

        }

        public SpParameter(string name, string description, object defaultValue, Control control, SpParameterControlType parameterControlType)
        {
            this.name = name;
            this.description = description;
            this.defaultValue = defaultValue;
            this.control = control;
            this.parameterControlType = parameterControlType;

        }

        public SpParameterControlType ParameterControlType
        {
            get
            {
                return parameterControlType;
            }
            set
            {
                parameterControlType = value;
            }
        }

        string name;
        public string Name {
            get { 
                return name;
            }
            set
            {
                name = value;
            }
        }

        string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        object defaultValue;
        public object DefaultValue
        {
            get
            {
                return defaultValue;
            }
            set
            {
                defaultValue = value;
            }
        }

        GroupControl groupControl;
        public GroupControl RelatedGroupControl
        {
            get
            {
                return groupControl;
            }
            set
            {
                groupControl = value;
            }

        }

        Control control;
        public Control RelatedControl
        {
            get {
                return control;
            }
            set
            {
                control = value;
            }

        }

        public void RefreshEditValue()
        {
            switch (parameterControlType)
            {
                case SpParameterControlType.TextControl:
                    ((TextEdit)RelatedControl).Refresh();
                    break;
                case SpParameterControlType.DateTime:
                    ((DateEdit)RelatedControl).Refresh();
                    break;
                case SpParameterControlType.CheckedComboBoxList:
                    ((CheckedComboBoxEdit)RelatedControl).RefreshEditValue();
                    break;
                case SpParameterControlType.LookUpEdit:
                    ((LookUpEdit)RelatedControl).Refresh();
                    break;
                case SpParameterControlType.CheckEdit:
                    ((CheckEdit)RelatedControl).Refresh();
                    break;
            }
        }

        Control checkBox;
        public Control CheckBox
        {
            get
            {
                return checkBox;
            }
            set
            {
                checkBox = value;
            }
        }



        public object GetTextValue()
        {
            switch (parameterControlType)
            {
                case SpParameterControlType.TextControl:
                    return ((TextEdit)RelatedControl).Text;
                    
                case SpParameterControlType.DateTime:
                    return ((DateEdit)RelatedControl).Text;
                    
                case SpParameterControlType.CheckedComboBoxList:
                    return ((CheckedComboBoxEdit)RelatedControl).Text;

                case SpParameterControlType.LookUpEdit:
                    return ((LookUpEdit)RelatedControl).Text;

                case SpParameterControlType.CheckEdit:
                    return ((CheckEdit)RelatedControl).Text;
                    
            }
            return null;
        }

        public void SetControlValue(object value)
        {
            switch (parameterControlType)
            {
                case SpParameterControlType.TextControl:
                    ((TextEdit)RelatedControl).Text = value.ToString();
                    break;
                case SpParameterControlType.DateTime:
                    ((DateEdit)RelatedControl).EditValue = value;
                    break;
                case SpParameterControlType.CheckedComboBoxList:
                    { 
                         ((CheckedComboBoxEdit)RelatedControl).EditValue = value;
                         ((CheckedComboBoxEdit)RelatedControl).RefreshEditValue();
                     }
                    break;
                case SpParameterControlType.LookUpEdit:
                    {
                        if (value.ToString() == "")
                        {
                            ((LookUpEdit)RelatedControl).EditValue = value;
                        }
                        else
                        {
                            ((LookUpEdit)RelatedControl).EditValue = Convert.ToInt32(value);
                        }
                        
                    }
                    break;
                case SpParameterControlType.CheckEdit:
                    ((CheckEdit)RelatedControl).EditValue = value;
                    ((CheckEdit)RelatedControl).Checked = Convert.ToBoolean(value);
                    break;
            }
        }

        public object GetControlValue()
        {
            switch (parameterControlType)
            {
                case SpParameterControlType.TextControl:
                    return ((TextEdit)RelatedControl).Text;

                case SpParameterControlType.DateTime:
                    return ((DateEdit)RelatedControl).EditValue;

                case SpParameterControlType.CheckedComboBoxList:
                    return ((CheckedComboBoxEdit)RelatedControl).EditValue;

                case SpParameterControlType.LookUpEdit:
                    return ((LookUpEdit)RelatedControl).EditValue;

                case SpParameterControlType.CheckEdit:
                    return ((CheckEdit)RelatedControl).EditValue;

            }
            return null;
        }

        public void SetNullValue()
        {
            switch (parameterControlType)
            {
                case SpParameterControlType.TextControl:
                    { ((TextEdit)RelatedControl).Text = defaultValue.ToString(); break; }

                case SpParameterControlType.DateTime:
                    { ((DateEdit)RelatedControl).EditValue = defaultValue; break; }

                case SpParameterControlType.CheckedComboBoxList:
                    { ((CheckedComboBoxEdit)RelatedControl).EditValue = defaultValue; 
                        //((CheckEdit)CheckBox).Checked = true; 
                        break; }

                case SpParameterControlType.LookUpEdit:
                    { ((LookUpEdit)RelatedControl).EditValue = defaultValue; break; }

                case SpParameterControlType.CheckEdit:
                    { ((CheckEdit)RelatedControl).EditValue = defaultValue;
                    ((CheckEdit)RelatedControl).Checked = Convert.ToBoolean(defaultValue);
                        break; }

            }
            
        }

       


    }

    public enum SpParameterControlType { 
        DateTime,
        TextControl,
        CheckedComboBoxList,
        LookUpEdit,
        CheckEdit
    }
}

