set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go







-- =============================================
-- Author: Бобровский Андрей Александрович
-- Create date: 25.11.2013
-- Description: Получение информация для печати в чек-ордере при приеме платежей
-- ХП используется базой данных Payments, программой по приему платежей
-- =============================================
ALTER PROCEDURE [town].[Payment_Info]
		@Authcod varchar(24) 
AS
BEGIN
SET NOCOUNT ON
	declare @Derive_Company_Cod varchar(24) -- с 1 по 4 символ кода авторизации
	declare @Bill_Filter_Cod varchar(2) -- с 5 по 6 символ кода авторизации
	declare @Month int -- с 7 по 8 символ кода авторизации
	declare @Year int -- с 7 по 12 символ кода авторизации
	declare @Account varchar(12) -- с 13 по 24 символ кода авторизации
	declare @ID_Account int
	declare @ID_Bill_Filter int 
	declare @Period int

	set @Derive_Company_Cod = substring(@Authcod,1,4)
	set @Month = substring(@Authcod,7,2)
	set @Bill_Filter_Cod = substring(@Authcod,5,2)
	set @Year = substring(@Authcod,9,4)
	set @Account = substring(@Authcod,13,12)
	set @ID_Account = (select id_account from account where account = @account)
	set @ID_Bill_Filter = (select ID_Bill_Filter from dbo.Bill_Filter where Code=@Bill_Filter_Cod)
	set @Period = dbo.Period_By_Year_Month(@Year,@Month) 

	select ID_Derive_Company,
			c.[Name] as Derive_Company_Name,
			c.Code as Derive_Company_Code,
			bf.ID_Bill_Filter,
			Info as Bill_Filter_Name,
			cl.*,
			(select Address from dbo.VAccount_Address_All where Account = @Account) as Address,
			convert(money,0) as Sum_Tariff
--			(select isnull(sum(sum_tariff),0)+isnull(sum(Sum_Recalculation),0)
--			 from VSum_Circulate asb inner join dbo.Provider p on asb.ID_Provider=p.ID_Provider
--											 inner join dbo.Provider_Group pg on p.ID_Provider_Group=pg.ID_Provider_Group
--											 inner join dbo.Bill_Filter_Service_Provider_Group bfspg on bfspg.ID_Provider_Group = pg.ID_Provider_Group 
--																	and  bfspg.ID_Service=asb.ID_Service
--											 inner join Reports.Client_Detail cd on cd.ID_Bill_Filter=bfspg.ID_Bill_Filter 
--																		and cd.ID_Derive_Company=p.ID_Derive_Company
--																		and asb.Period >= cd.Begin_Period and asb.Period <= isnull(cd.End_Period,dbo.work_period())
--			 where id_account = @ID_Account and period=@Period and cd.ID_Bill_FIlter=@ID_Bill_Filter) as Sum_Tariff

	from Reports.Client_Detail cd inner join dbo.Company c on cd.ID_Derive_Company = c.ID_Company 
								  inner join dbo.Bill_Filter bf on cd.ID_Bill_Filter = bf.ID_Bill_Filter
								  inner join   (select *
												from (select ID_Company, Parameter, String_Value from dbo.Company_Parameter cpp
													  where Parameter in ('КОМ_НАИМ','КОМ_ИНН','КОМ_КПП','КОМ_РС','КОМ_БИК','КОМ_БАНК','КОД_КЛИЕНТА')) A
													  PIVOT (max(String_Value) FOR Parameter IN ([КОМ_НАИМ],[КОМ_ИНН],[КОМ_КПП],[КОМ_РС],[КОМ_БИК],[КОМ_БАНК],[КОД_КЛИЕНТА])) B
												) cl on cd.ID_Client=cl.ID_Company
	where c.Is_Derive_Company = 1 and c.Code=@Derive_Company_Cod and bf.Code = @Bill_Filter_Cod
		and  Begin_Period <= @Period
		and  End_Period is null
END





