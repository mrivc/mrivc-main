﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace MrivcHelpers.Helpers
{
    public class ExcelExport
    {
        Excel.Application excelApp = new Excel.Application();
        List<Excel._Worksheet> sheetsToSave = new List<Excel._Worksheet>();
        public string OpenFilePath { get; set; }

        public ExcelExport(string openFilePath)
        {
            OpenFilePath = openFilePath;
            Open(openFilePath);
        }

        public void Open(string OpenFilePath)
        {
            excelApp.Workbooks.Open(OpenFilePath);
            excelApp.DisplayAlerts = false;
        }

        public void LoadDataTable(DataTable Tbl, int headerRowNumber)
        {
            Excel._Worksheet workSheet = null;

            foreach (Excel._Worksheet sheet in excelApp.Sheets)
            {
                if (Tbl.TableName == sheet.Name)
                    workSheet = sheet;
            }

            //// column headings
            //for (int i = 0; i < Tbl.Columns.Count; i++)
            //{
            //    workSheet.Cells[1, (i + startRowNumber)] = Tbl.Columns[i].ColumnName;
            //}

            // rows
            for (int i = 0; i < Tbl.Rows.Count; i++)
            {
                // to do: format datetime values before printing
                for (int j = 0; j < Tbl.Columns.Count; j++)
                {
                    workSheet.Cells[(i + 1 + headerRowNumber), (j + 1)] = "'" + Tbl.Rows[i][j].ToString();
                }
            }

            sheetsToSave.Add(workSheet);
        }

        public void LoadDataSet(DataSet Ds, int headerRowNumber)
        {
            foreach (DataTable table in Ds.Tables)
            {
                LoadDataTable(table, headerRowNumber);
            }
        }

        public void SaveAs(string SaveFilePath)
        {
            foreach (Excel._Worksheet sheet in sheetsToSave)
            {
                sheet.SaveAs(SaveFilePath);
            }
            sheetsToSave.Clear();
            excelApp.Quit();
        }
    }
}
