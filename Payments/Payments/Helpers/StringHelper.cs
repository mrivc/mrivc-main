﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Payments
{
    public class StringHelper
    {
        public static string GetSubstring(string str, string startChar, string endChar)
        {
            int _ind1 = str.IndexOf(startChar);
            int _ind2 = str.LastIndexOf(endChar);

            if (_ind1 > 0 && _ind2 > _ind1)
                return str.Substring(str.IndexOf(startChar)+1, str.LastIndexOf(endChar) - str.IndexOf(startChar)-1);
            else
                return "";
        }

        public static string ReplaceEscape(string str)
        {
            str = str.Replace("'", "''");
            return str;
        }

        public static string ZeroCode(int value)
        {
            if (value < 10)
                return "0" + value.ToString();
            return value.ToString();
        }

        public static string DayCode(int day)
        {
            if (day < 10)
                return "0" + day.ToString();
            return day.ToString();
        }

        public static string GetShortAuthoCode(string authoCode)
        {
            if (authoCode.Length == 24)
            {
                return authoCode.Substring(0, 6) + authoCode.Substring(12);
            }

            return authoCode;
        }

        /// <summary>
        /// Наименование товара для передачи в ОФД (для коммунальных платежей)
        /// </summary>
        /// <param name="authoCode">Код авторизации</param>
        /// <param name="payeeAccount">Расчетный счет получателя платежа</param>
        /// <returns></returns>
        public static string GenerateOfdProductName(string authoCode, string payeeAccount)
        {
            return string.Format("Квартплата, ЛС {0}, Р.сч {1}", GetShortAuthoCode(authoCode), payeeAccount);
        }

        public static string GenerateOfdProductName(string authoCode, string payeeAccount, bool shortAccount)
        {
                return string.Format("Квартплата, ЛС {0}, Р.сч {1}", authoCode, payeeAccount); 
        }

        /// <summary>
        /// Наименование товара для передачи в ОФД (капитальный ремонт)
        /// </summary>
        /// <param name="authoCode">Лицевой счет</param>
        /// <returns></returns>
        public static string GenerateOfdProductNameCapitalRepair(string account)
        {
            return string.Format("Кап. ремонт, ЛС {0}", account);
        }

        public static bool IsValidEmail(string InputEmail)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(InputEmail);
            if (match.Success)
                return true;
            else
                return false;
        }
    }
}
