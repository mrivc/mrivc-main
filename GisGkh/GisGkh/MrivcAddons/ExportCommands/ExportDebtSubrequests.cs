﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GisGkh
{
    public class ExportDebtSubrequests : MrivcExportCommand, ISingleHouseCommand
    {
        readonly DateTime _startDate;
        readonly DateTime _endDate;
        readonly string _senderID;

        public string FiasHouseGuid { get; set; }

        public ExportDebtSubrequests(string senderID, DateTime startDate, DateTime endDate) : base("exportDebtSubrequests", "DebtRequestsServiceAsync", senderID, "exportDSRsResult")
        {
            _startDate = startDate;
            _endDate = endDate;
            _senderID = senderID;
            Description = "Выгрузка из ГИС ЖКХ запросов о наличии задолженности за ЖКУ (РСО)";
        }

        public void ExecuteToTempDB()
        {
            var sd = _startDate.ToString("yyyy-MM-dd");
            var ed = _endDate.ToString("yyyy-MM-dd");
            var values = new List<Tuple<string[], string>>();
            values.Add(new Tuple<string[], string>(new[] { "periodOfSendingRequest", "startDate" }, sd));
            values.Add(new Tuple<string[], string>(new[] { "periodOfSendingRequest", "endDate" }, ed));
            values.Add(new Tuple<string[], string>(new[] { "houseGUID" }, FiasHouseGuid));
            base.ExecuteToTempDB(values, _exportNodeName);
        }

        public new void Execute()
        {
            MrivcData.CreateTempExportDSRTables();
            this.ExecuteToTempDB();
            if (CommandType == AsyncCommandType.GetState || CommandType == AsyncCommandType.All)
            {
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportDSRData", new object[,] { { "@senderId", _senderID }, { "@FiasHouseGuid", FiasHouseGuid } });
                DAccess.DataModule.ExecuteNonQueryStoredProcedure1("[GIS].[SaveExportedDSRData]");
            }
        }

        public DataSet PrepareToDataSet()
        {
            throw new NotImplementedException();
        }
    }
}
