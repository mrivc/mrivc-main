﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GisGkh
{
    public partial class CloseAccountForm : Form
    {
        public CloseAccountForm()
        {
            InitializeComponent();
        }

        private void CloseAccountForm_Load(object sender, EventArgs e)
        {
            senderIdFilter1.Fill();
        }

        void CloseAccount(bool Cr)
        {
            MrivcStatic.GetStateWaitMin = -1;
            var closeAccountData = new CloseAccountData(senderIdFilter1.SenderID) { CapitalRepair = Cr };

            if (senderIdFilter1.HouseAll)
            {
                MrivcStatic.HouseIteration(closeAccountData, false);
            }
            else
            {
                closeAccountData.FiasHouseGuid = senderIdFilter1.FiasHouseGuid;
            }

            closeAccountData.Execute();
            MrivcStatic.GetStateWaitMin = MrivcStatic.GetStateWaitMinDefault;
        }

        private void btnCloseAccount_Click(object sender, EventArgs e)
        {

            CloseAccount(false);
        }

        private void btnCloseCr_Click(object sender, EventArgs e)
        {
            CloseAccount(true);
        }
    }
}
