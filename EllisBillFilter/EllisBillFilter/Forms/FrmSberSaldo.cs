﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;
using EllisBillFilter.Logic;
using Microsoft.Win32;

namespace EllisBillFilter
{
    using MrivcHelpers.Helpers;

    public partial class FrmSberSaldo : Form
    {
        MDataSet.ClientDataTable dtClient;

        public FrmSberSaldo()
        {
            InitializeComponent();

            dtClient = new MDataSet.ClientDataTable();
            this.Text = this.Text + (" (" + DAccess.DataModule.Connection.DataSource + " - " + DAccess.DataModule.Connection.Database + "): v") + System.Windows.Forms.Application.ProductVersion;
        }

        private void FrmSberSaldo_Load(object sender, EventArgs e)
        {
            mDataSet.Period.Merge(DAccess.Instance.Periods);
            mDataSet.Period.Rows[0].Delete();

            ReloadClients();

            MDataSet.DeriveCompanyRow r = DAccess.Instance.DeriveCompany.NewDeriveCompanyRow();
            r.ID_Company = -1;
            r.Name = "Неизвестная компания";
            r.Is_Derive_Company = true;
            r.Is_Pay_Receiver = false;
            r.Is_Provider = false;
            DAccess.Instance.DeriveCompany.Rows.InsertAt(r, 0);
            DAccess.Instance.DeriveCompany.AcceptChanges();

            mDataSet.DeriveCompany.Merge(DAccess.Instance.DeriveCompany);
            
            MDataSet.BillFilterRow r1 = DAccess.Instance.BillFilter.NewBillFilterRow();
            r1.ID_Bill_Filter = -1;
            r1.Bill_Filter = "Все";
            r1.Tag = -1;
            r1.Info = "Все";
            r1.Code = -1;
            DAccess.Instance.BillFilter.Rows.InsertAt(r1, 0);
            mDataSet.BillFilter.Merge(DAccess.Instance.BillFilter);

            ((ListBox)clClients).DataSource = dtClient;
            ((ListBox)clClients).ValueMember = "ID_Company";
            ((ListBox)clClients).DisplayMember = "Name";
        }

        public void ReloadClients()
        {            
            dtClient.Clear();
            DAccess.DataModule.ClientSelect(dtClient, (int)cbPeriod.SelectedValue, cbShowOnlyPrintedClients.Checked);
            lblClients.Text = "Клиенты (" + dtClient.Rows.Count.ToString() + "): ";
            txtClientNames.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!CheckFillForm())
                return;

            btnOK.Enabled = false;
            btnCancel.Enabled = false;

            object v = Registry.GetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Jet\\4.0\\Engines\\Xbase", "BDE", -1);
            if (v == null || (int)v == -1)
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Jet\\4.0\\Engines\\Xbase", "BDE", 2, RegistryValueKind.DWord);

            this.Cursor = Cursors.WaitCursor;


            string fileUnloadPath = txtPath.Text;
            if (!fileUnloadPath.EndsWith("\\"))
                fileUnloadPath = fileUnloadPath + "\\";
            
            //"12774,12768"     
            string clients = GetSelectedClients("ID_Company");
            
            ReestrGenerator.Generate((int)cbPeriod.SelectedValue, clients, (int)cbDeriveCompany.SelectedValue, (int)cbBillFilter.SelectedValue, GetFilterHouses(), cbSber.Checked, cbDBF.Checked, rbTypeDebt.Checked, fileUnloadPath, true, false, rbOnlyUninhabitedAccounts.Checked);
            //ReestrGenerator.Generate(24177, "12774,12768", (int)cbDeriveCompany.SelectedValue, (int)cbBillFilter.SelectedValue, cbSber.Checked, cbDBF.Checked, txtPath.Text);

            this.Cursor = Cursors.Default;
            btnCancel.Enabled = true;
            btnOK.Enabled = true;

            FlashWindow.Flash(this);
            MessageBox.Show(this, "Реестры сформированы успешно.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void rbTypeDebt_CheckedChanged(object sender, EventArgs e)
        {
            groupBox3.Enabled = rbTypeDebt.Checked;
        }

        private string GetSelectedClients(string field)
        { 
            string s = "";
            foreach (DataRowView r in clClients.CheckedItems)
                s += "," + r[field].ToString();
            
            if (s.Length > 0)
                s = s.Substring(1);
            
            return s;
        }

        private string GetFilterHouses()
        {
            if (ddHouseFilter.Properties.Items.Count ==
                ddHouseFilter.Properties.GetCheckedItems().ToString().Split(new char[] {','}).Length)
                return string.Empty;

            return ddHouseFilter.EditValue.ToString();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            var row = dtClient.Where(cName => cName["Name"].ToString().ToLower().Contains(textBox2.Text.ToLower())==true)
               .FirstOrDefault(); 

            if (row != null)
                clClients.SelectedValue = row.ID_Company;
        }

        private bool CheckFillForm()
        {
            if (!cbSber.Checked && !cbDBF.Checked)
            {
                MessageBox.Show(this, "Необходимо выбрать тип формируемого реестра: TXT, DBF.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (rbTypeCurrent.Checked && txtClientNames.Text.Length == 0)
            {
                MessageBox.Show(this, "Необходимо выбрать хотябы одного клиента.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (rbTypeDebt.Checked && (int)cbBillFilter.SelectedValue == -1)
            {
                MessageBox.Show(this, "Необходимо указать код фильтра.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;
        }

        private void clClients_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtClientNames.Text = GetSelectedClients("Name");
        }

        private void clClients_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            mDataSet.SaldoHouseFilter.Clear();
            string s = GetSelectedClients("ID_Company");

            string checkedCompany = ((DataRowView) clClients.Items[e.Index])["ID_Company"].ToString();
            if (s.Contains(checkedCompany))
                s = s.Replace(checkedCompany, "");
            else
                s = s + (s.Length == 0 ? checkedCompany : "," + checkedCompany);

            s = s.Replace(",,", ",");
            if (s.EndsWith(","))
                s = s.Substring(0, s.Length - 1);
            if (s.StartsWith(","))
                s = s.Substring(1, s.Length - 1);


            //MessageBox.Show(s);

            if (s.Length > 0)// && !clClients.CheckedItems.Contains(clClients.SelectedItem))
            {
                Cursor = Cursors.WaitCursor;
                DAccess.DataModule.FillHouseTableForSaldoFilter(mDataSet.SaldoHouseFilter, (int)cbPeriod.SelectedValue,
                    s);
                Cursor = Cursors.Default;
            }
        }

        private void btnFilePath_Click(object sender, EventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = dialog.SelectedPath.ToString();
            }
        }

        private void cbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPeriod.SelectedValue == null)
                return;

            ReloadClients();
        }

        private void cbShowOnlyPrintedClients_CheckedChanged(object sender, EventArgs e)
        {
            ReloadClients();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            FrmSberSaldoSettings f = new FrmSberSaldoSettings();
            f.Show();
        }

        private void cbBillFilter_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
