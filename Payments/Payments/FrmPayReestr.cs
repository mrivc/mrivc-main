﻿using Payments.DS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Payments.Controls;
using Payments.LogicClasses;

namespace Payments
{
    public partial class FrmPayReestr : Form, IPayForm
    {
        PaymentClass paymentClass;

        public FrmPayReestr(PaymentClass paymentClass)
        {
            InitializeComponent();
            this.paymentClass = paymentClass;

            var date = DateTime.Today;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, date.Day);
            var lastDayOfMonth = firstDayOfMonth.AddDays(1);

            dtpBeginDate.Value = firstDayOfMonth;
            dtpEndDate.Value = lastDayOfMonth;

            DAccess.DataModule.Connection1 = new System.Data.SqlClient.SqlConnection(Properties.Settings.Default.MurmanskConnectionString);
        }

        public PaymentClass PaymentClass
        {
            get { return paymentClass; }
        }

        public PanelPayment PaymentInfo
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Form Window
        {
            get { return this; }
        }

        public void SavePaymentDetail()
        {
            //if (paymentClass.IdPayment == -1)
            //    throw new exception("нельзя сохранить делали платежа в таблицу \"dbo.Payment_Parameter_Detail\", т.к. идентификатор платежа ID_Payment = -1");
            this.SaveDataDetail();
        }

        public void SaveDataDetail()
        {
            //удаляем не заполненные параметры
            foreach (PaymentsDataSet.PaymentParameterDetailRow row in paymentsDataSet.PaymentParameterDetail.Rows)
            {
                row.ID_Payment = paymentClass.IdPayment;

                if (row["Parameter_Quantity"] == DBNull.Value && paymentClass.ShowParameterQuantity == true)
                    row.Delete();

                if (row.RowState == DataRowState.Deleted)
                    continue;

                if (row["Parameter_Value"] == DBNull.Value && paymentClass.ShowParameterQuantity == false)
                    row.Delete();
            }

            paymentsDataSet.PaymentParameterDetail.AcceptChanges();
            foreach (PaymentsDataSet.PaymentParameterDetailRow row in paymentsDataSet.PaymentParameterDetail.Rows)
                row.SetAdded();

            DAccess.DataModule.PaymentParameterDetailUpdate(paymentsDataSet.PaymentParameterDetail);

            paymentsDataSet.PaymentParameterDetail.Clear();
        }

        private int SaveData()
        {
            return DAccess.DataModule.PaymentUpdate(paymentsDataSet.Payment);
        }

        private void btnSelectPacks_Click(object sender, EventArgs e)
        {
            //var date = DateTime.Today;
            //var firstDayOfMonth = new DateTime(date.Year, date.Month, date.Day);
            //var lastDayOfMonth = firstDayOfMonth.AddDays(1);

            //dtpBeginDate.Value = firstDayOfMonth;
            //dtpEndDate.Value = lastDayOfMonth;

            var bd = dtpBeginDate.Value;
            var ed = dtpEndDate.Value;

            dgvPacks.DataSource = DAccess.DataModule.GetDataTableByStoredProcedure1("SelectPackForKassa", new object[,] { { "beginDate", bd }, { "endDate", ed } });
        }

        //string GetEllisInfo(string parameter)
        //{
        //    var res = DAccess.DataModule.ExecuteScalar("")
        //}

        private void btnPayPacks_Click(object sender, EventArgs e)
        {
            DAccess.Instance.HasErrors = false;

            DAccess.CashTerminal.Terminal.Timeout = 3000; //увеличить таймаут кассы для корректной работы
            DAccess.CashTerminal.WriteTableFieldValue(17, 1, 7, "2"); //не печатать чек
            DAccess.CashTerminal.WriteTableFieldValue(1, 1, 28, "1"); //выключить звук


            var bd = dtpBeginDate.Value;
            var ed = dtpEndDate.Value;

            var dt = DAccess.DataModule.GetDataTableByStoredProcedure1("SelectBillForKassa", new object[,] { { "beginDate", bd }, { "endDate", ed } });
            
            dt.TableName = "Pack";

            var dt1 = new DataTable();
            dt1.TableName = "Bill";

            var ds = new DataSet();
            ds.Tables.Add(dt);
            ds.Tables.Add(dt1);

            if (ds.Tables["Pack"].Rows.Count == 0)
                return;

            foreach (DataRow packRow in ds.Tables["Pack"].Rows)
            {
                var bills = DAccess.DataModule.GetDataTableByStoredProcedure1("SelectBillDetailForKassa", new object[,] { { "id_pack", packRow["id_pack"] }, { "id_bill", packRow["id_bill"] }, { "account", packRow["account"].ToString() } });
                var services = DAccess.DataModule.GetDataTableByStoredProcedure1("SelectServicesForKassa", new object[,] { { "id_pack", packRow["id_pack"] }, { "id_bill", packRow["id_bill"] }, { "account", packRow["account"].ToString() } });

                //var bills = ds.Tables["Bill"].Select("id_bill = " + packRow["id_bill"].ToString());
                //var bills = ds.Tables["Bill"].Select();

                var ellisTableinfo = DAccess.DataModule.GetDataTableByStoredProcedure1("town.MrivcInformationForKassa");
                ellisTableinfo.TableName = "EllisTableInfo";

                List<string> freeStrings = new List<string>();
                //freeStrings.Add("Услуга: " + ellisTableinfo.Rows[0]["Bill_Filter_Name"].ToString());
                //freeStrings.Add("");
                freeStrings.Add("ПОЛУЧАТЕЛЬ: " + ellisTableinfo.Select("parameter = 'КОМ_НАИМ' ")[0]["String_Value"].ToString());
                freeStrings.Add("ИНН: " + ellisTableinfo.Select("parameter = 'КОМ_ИНН' ")[0]["String_Value"].ToString());
                freeStrings.Add("КПП: " + ellisTableinfo.Select("parameter = 'КОМ_КПП' ")[0]["String_Value"].ToString());
                freeStrings.Add("СЧЕТ: " + ellisTableinfo.Select("parameter = 'КОМ_РС' ")[0]["String_Value"].ToString());
                freeStrings.Add("БАНК: " + ellisTableinfo.Select("parameter = 'КОМ_БАНК' ")[0]["String_Value"].ToString());
                freeStrings.Add("");
                freeStrings.Add("Код авторизации: " + packRow["AuthCode"].ToString());

                if (packRow["ADDRESS"].ToString().Length > 27)
                {
                    freeStrings.Add("Адрес: " + packRow["ADDRESS"].ToString().Substring(0, 27));
                    freeStrings.Add("" + packRow["ADDRESS"].ToString().Substring(27));
                }
                else
                    freeStrings.Add("Адрес: " + packRow["ADDRESS"].ToString());

                freeStrings.Add("");

                freeStrings.Add("Телефон АО МРИВЦ 455091");

                freeStrings.Add("");

                freeStrings.Add("Сумма платежа: " + packRow["Sum_Pay"].ToString());
                //freeStrings.Add("Комиссия с платежа: " + panelPayment1.PaymentSummCommission.ToString());

                //List<PaymentParameterDatail> ppdList = gridPaymentParameter1.GetParameterWithQuantity();

                freeStrings.Add("");

                foreach (DataRow p in services.Rows)
                    freeStrings.Add(p["Name"].ToString() + " " + p["sum_pay"].ToString() + "р");

                freeStrings.Add("");

                string ofdProductName = StringHelper.GenerateOfdProductName(packRow["AuthCode"].ToString(), ellisTableinfo.Select("parameter = 'КОМ_РС' ")[0]["String_Value"].ToString());

                //if (DAccess.CashTerminal.Sale(panelPayment1.PaymentSummTotal, panelPayment1.PaymentCash, 1,
                //    paymentClass.Name, freeStrings, ofdProductName, panelPayment1.PhoneEmail, Properties.Settings.Default.Department_Number, paymentClass.MakeCheckCopy))


                //if (DAccess.CashTerminal.Sale(panelPayment1.PaymentSumm, panelPayment1.PaymentCash, 1,
                //paymentClass.Name, freeStrings, "Прочие услуги", panelPayment1.PhoneEmail, Properties.Settings.Default.Department_Number, paymentClass.MakeCheckCopy, taxGroup))

                var res = DAccess.CashTerminal.Sale((decimal)packRow["Sum_Pay"], (decimal)packRow["Sum_Pay"], 1,
                   "Квартплата", freeStrings, ofdProductName, packRow["email"].ToString(), Properties.Settings.Default.Department_Number, false);

                //var res = true;

                if (res)
                {
                    AddPayment(this, packRow, bills);
                } 

            }

            if (DAccess.Instance.HasErrors == true)
            {
                MessageBox.Show("Во время работы кассы возникли ошибки. Необходимо заново запустить загрузку оставшихся платежей с теми же датами");
                DAccess.Instance.HasErrors = false;
            }
            else
            {
                MessageBox.Show("Платежи по реестрам успешно загружены в кассу");
            }

            
        }

        /// <summary>
        /// Добавляем платеж в базу данных
        /// </summary>
        /// <param name="frm">Форма платежа с данными о платеже</param>
        private void AddPayment(IPayForm frm, DataRow pack, DataTable bills)
        {
            DAccess.CashTerminal.FNGetStatus();

            PaymentsDataSet.PaymentRow row = paymentsDataSet.Payment.NewPaymentRow();
            row.ID_Payment = -1;
            row.ID_Payment_Class = paymentClass.IdPaymentClass;//(int)ddPaymentClass.EditValue;
            row.IsStorno = false;
            row.Operation_Number = 1;//TEST DAccess.CashTerminal.OperationRegister.LastSaleNumber;
            //row.Session_Number = DAccess.CashTerminal.SessionNumber;
            row.Department_Number = Properties.Settings.Default.Department_Number;
            row.Payment_Cash = (decimal)pack["sum_pay"];                                        //frm.PaymentInfo.PaymentCash;
            row.Payment_Summ = (decimal)pack["sum_pay"];                                        //frm.PaymentInfo.PaymentSumm;
            row.Payment_Commission = 0;                                                         //frm.PaymentInfo.PaymentSummCommission;
            row.Payment_SummTotal = row.Payment_Summ + row.Payment_Commission;
            row.Payment_Charge = 0;                                                              //frm.PaymentInfo.PaymentCharge;
            row.Comment = pack["number"].ToString() + " от " + pack["date"].ToString() 
                //БДА 27.11.2019 добавил наименования банков
                + " " + pack["Pack_Type"].ToString();    //frm.PaymentInfo.PaymentComment;

            row.Payment_Date = (DateTime)pack["pay_date"];                                        //frm.PaymentInfo.PaymentDate;
            row.CreatedBy = DAccess.Instance.CurrentUser.Id_User;
            row.ID_Cash_Type = 3;                                                                   //frm.PaymentInfo.CashType;
            row.Creation_Date = DateTime.Now;
            if (DAccess.CashTerminal.Terminal != null)
            {                                                                                       
                row.DocumentNumber = DAccess.CashTerminal.Terminal.DocumentNumber;//row.DocumentNumber = 1;
            }

            paymentsDataSet.Payment.AddPaymentRow(row);
            frm.PaymentClass.IdPayment = SaveData(); //(int)gridViewPayment.GetDataRow(gridViewPayment.FocusedRowHandle)["ID_Payment"];

            foreach (DataRow bill in bills.Rows)
            {
                PaymentsDataSet.PaymentParameterDetailRow detailRow = paymentsDataSet.PaymentParameterDetail.NewPaymentParameterDetailRow();
                detailRow.Category_Type = bill["Category_Type"].ToString();
                detailRow.Category_Type_Rank = (int)bill["Category_Type_Rank"];
                detailRow.Code = bill["Code"].ToString();
                detailRow.CreatedBy = DAccess.Instance.CurrentUser.Id_User; 
                detailRow.Display_Name = bill["Display_Name"].ToString();
                detailRow.ID_Parameter_Type = (int)bill["ID_Parameter_Type"];
                detailRow.ID_Payment = frm.PaymentClass.IdPayment;
                detailRow.Parameter_Quantity = -1; //(int)bill["Parameter_Quantity"];
                detailRow.Parameter_Rank = (int)bill["Parameter_Rank"];
                detailRow.Parameter_Value = bill["Parameter_Value"].ToString();
                detailRow.Print_Name = bill["Print_Name"].ToString();

                paymentsDataSet.PaymentParameterDetail.AddPaymentParameterDetailRow(detailRow);
            }

            frm.SavePaymentDetail();
        }

        private void FrmPayReestr_Load(object sender, EventArgs e)
        {
            
        }
    }
}
