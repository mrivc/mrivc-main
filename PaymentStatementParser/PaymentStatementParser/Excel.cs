﻿using System.Collections.Generic;
using System.Data;
using System.IO;

using ExcelDataReader;
using ExcelLibrary;
using System.Threading;

namespace PaymentStatementParser
{
    public static class Excel
    {
        public static void WriteDataSetInExcel(string path, DataSet ds)
        {
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
            {
                DataSetHelper.CreateWorkbook(fs, ds);
            }
        }

        public static void WriteDataSetDictionaryInExcel(string path, string dirName, Dictionary<string, DataSet> dataSetsDictionary)
        {
            if (!Directory.Exists(path + dirName)) throw new System.Exception($@"Директории {path + dirName} не существует!");

            string fullPath = path + dirName + '\\';
            string fileName = string.Empty;

            foreach (DataSet ds in dataSetsDictionary.Values)
            {
                fileName = ds.Tables[0].TableName + ".xls";

                using (var fs = new FileStream(fullPath + fileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    DataSetHelper.CreateWorkbook(fs, ds);
                }
            }
        }

        public static DataTable ReadExcel(string file)
        {
            using (var stream = File.Open(file, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet();

                    return result.Tables[0];
                }
            }
        }
    }
}
