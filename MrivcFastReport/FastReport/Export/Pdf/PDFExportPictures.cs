﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using FastReport.Export;
using System.Drawing;
using FastReport.Utils;
using System.Drawing.Imaging;

namespace FastReport.Export.Pdf
{
    public partial class PDFExport : ExportBase
    {
        private Dictionary<string, long> picturesList;
        private List<long> picResList;

        private long IndexOfPicture(string hash)
        {
            long result = -1;

            if (picturesList.TryGetValue(hash, out result))
                return result;
            else
                return -1;
        }

        private long AddPicture(MemoryStream stream, string hash, int width, int height, MemoryStream mask)
        {
            long mask_index = 0;
            long i = IndexOfPicture(hash);
            if (i == -1)
            {
                if (mask != null)
                {
                    mask_index = UpdateXRef();
                    WriteLn(pdf, ObjNumber(mask_index));
                    WriteLn(pdf, "<< /Type /XObject");
                    WriteLn(pdf, "/Subtype /Image");
                    WriteLn(pdf, "/Width " + width.ToString());
                    WriteLn(pdf, "/Height " + height.ToString());
                    WriteLn(pdf, "/ColorSpace /DeviceGray /Matte[ 0 0 0] ");
                    WriteLn(pdf, "/BitsPerComponent 8");
                    WriteLn(pdf, "/Interpolate false");
                    WritePDFStream(pdf, mask, mask_index, FCompressed, FEncrypted, false, true, false);
                }

                i = UpdateXRef();
                picturesList.Add(hash, i);
                // save image
                WriteLn(pdf, ObjNumber(i));
                WriteLn(pdf, "<< /Type /XObject");
                WriteLn(pdf, "/Subtype /Image");
                WriteLn(pdf, "/Width " + width.ToString());
                WriteLn(pdf, "/Height " + height.ToString());
                WriteLn(pdf, "/ColorSpace /DeviceRGB");
                WriteLn(pdf, "/BitsPerComponent 8");
                WriteLn(pdf, "/Filter /DCTDecode");
                WriteLn(pdf, "/Interpolate false");
                if (mask != null)
                    WriteLn(pdf, "/SMask " + ObjNumberRef(mask_index));
                WritePDFStream(pdf, stream, i, false, FEncrypted, false, true, true);
            }
            if (picResList.IndexOf(i) == -1)
                picResList.Add(i);
            return i;
        }

        private void AddPictureObject(Stream outstream, ReportComponentBase obj, bool drawBorder, int quality)
        {
            if (obj.Width < 0.5f || obj.Height < 0.5f)
                return;

            long imageIndex = -1;
            float printZoom = FPrintOptimized ? 4 : 1;
            Border oldBorder = obj.Border.Clone();
            obj.Border.Lines = BorderLines.None;

            float width = obj.Width > paperWidth ? paperWidth : obj.Width;
            float height = obj.Height > paperHeight ? paperHeight : obj.Height;
            
            float bWidth = width == 0 ? 1 : obj.Width * PDF_DIVIDER;
            float bHeight = height == 0 ? 1 : obj.Height * PDF_DIVIDER;

            string imageHash = "";

            using (Bitmap image = new Bitmap((int)Math.Round(width * printZoom), (int)Math.Round(height * printZoom)))
            using (Graphics g = Graphics.FromImage(image))
            {
                g.TranslateTransform(-obj.AbsLeft * printZoom, -obj.AbsTop * printZoom);
                g.Clear(Color.Transparent);                
                obj.Draw(new FRPaintEventArgs(g, printZoom, printZoom, Report.GraphicCache));

                MemoryStream buff = new MemoryStream();
                ExportUtils.SaveJpeg(image, buff, quality);
                
                // we need hash of transparent image
                using (MemoryStream hashStream = new MemoryStream())
                {
                    image.Save(hashStream, ImageFormat.Png);
                    imageHash = Crypter.ComputeHash(hashStream);
                }

                MemoryStream mask_stream = null;

                // do the mask calculation when picture doesn't exist in a cache
                if (IndexOfPicture(imageHash) == -1)
                {

                    bool alpha = false;
                    byte[] mask_bytes;

                    // Please note that we support transparency only for 32bppArgb image formats
                    if (image.PixelFormat == PixelFormat.Format32bppArgb)
                    {
                        mask_bytes = new byte[image.Width * image.Height];

                        int raw_size = image.Width * image.Height;
                        Int32[] raw_picture = new Int32[raw_size];
                        
                        BitmapData bmpdata = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, image.PixelFormat);
                        IntPtr ptr = bmpdata.Scan0;
                        System.Runtime.InteropServices.Marshal.Copy(ptr, raw_picture, 0, raw_size);
                        image.UnlockBits(bmpdata);
                        
                        int j = 0;
                        for (int i = 0; i < raw_size; i++)
                        {
                            if (!alpha && (((UInt32)(raw_picture[i]) & 0xff000000) != 0xff000000))
                                alpha = true;
                            mask_bytes[j++] = (byte)(((UInt32)raw_picture[i]) >> 24);
                        }
                        
                        if (alpha)
                        {
                            mask_stream = new MemoryStream(mask_bytes);
                            mask_stream.Position = 0;
                        }
                    }
                }

                buff.Position = 0;
                imageIndex = AddPicture(buff, imageHash, image.Width, image.Height, mask_stream);                
            }

            StringBuilder sb = new StringBuilder(256);
            sb.AppendLine("q");

            if (obj is PictureObject)
                sb.Append(GetPDFFillTransparent(
                    Color.FromArgb((byte)((1 - (obj as PictureObject).Transparency) * 255f), Color.Black)));

            sb.Append(FloatToString(bWidth)).Append(" 0 0 ");
            sb.Append(FloatToString(bHeight)).Append(" ");
            sb.Append(FloatToString(GetLeft(obj.AbsLeft))).Append(" ");
            sb.Append(FloatToString(GetTop(obj.AbsTop + height)));
            sb.AppendLine(" cm");
            sb.Append("/Im").Append(imageIndex.ToString()).AppendLine(" Do");
            sb.AppendLine("Q");
            Write(outstream, sb.ToString());
            obj.Border = oldBorder;
            if (drawBorder)
                Write(outstream, DrawPDFBorder(obj.Border, obj.AbsLeft, obj.AbsTop, width, height));
        }
    }
}
