﻿namespace Payments
{
    partial class FrmPayEllis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPayEllis));
            this.txtAuthcod = new DevExpress.XtraEditors.TextEdit();
            this.lblAuthcod = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblClientName = new System.Windows.Forms.Label();
            this.lblClientBill = new System.Windows.Forms.Label();
            this.txtClientName = new DevExpress.XtraEditors.TextEdit();
            this.txtClientBill = new DevExpress.XtraEditors.TextEdit();
            this.panelPayment1 = new Payments.Controls.PanelPayment();
            this.gridPaymentParameter1 = new Payments.Controls.GridPaymentParameter();
            ((System.ComponentModel.ISupportInitialize)(this.txtAuthcod.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientBill.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtAuthcod
            // 
            this.txtAuthcod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAuthcod.EditValue = "";
            this.txtAuthcod.Location = new System.Drawing.Point(116, 11);
            this.txtAuthcod.Name = "txtAuthcod";
            this.txtAuthcod.Properties.Appearance.Options.UseTextOptions = true;
            this.txtAuthcod.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtAuthcod.Properties.Mask.EditMask = "#### ## ## #### ############";
            this.txtAuthcod.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtAuthcod.Size = new System.Drawing.Size(413, 20);
            this.txtAuthcod.TabIndex = 0;
            this.txtAuthcod.TextChanged += new System.EventHandler(this.txtAuthcod_TextChanged);
            this.txtAuthcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAuthcod_KeyDown);
            this.txtAuthcod.Leave += new System.EventHandler(this.txtAuthcod_Leave);
            // 
            // lblAuthcod
            // 
            this.lblAuthcod.AutoSize = true;
            this.lblAuthcod.Location = new System.Drawing.Point(10, 14);
            this.lblAuthcod.Name = "lblAuthcod";
            this.lblAuthcod.Size = new System.Drawing.Size(97, 13);
            this.lblAuthcod.TabIndex = 3;
            this.lblAuthcod.Text = "Код авторизации:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panelPayment1);
            this.groupBox1.Location = new System.Drawing.Point(7, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(528, 202);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // lblClientName
            // 
            this.lblClientName.AutoSize = true;
            this.lblClientName.Location = new System.Drawing.Point(10, 245);
            this.lblClientName.Name = "lblClientName";
            this.lblClientName.Size = new System.Drawing.Size(115, 13);
            this.lblClientName.TabIndex = 5;
            this.lblClientName.Text = "Получатель платежа:";
            // 
            // lblClientBill
            // 
            this.lblClientBill.AutoSize = true;
            this.lblClientBill.Location = new System.Drawing.Point(10, 271);
            this.lblClientBill.Name = "lblClientBill";
            this.lblClientBill.Size = new System.Drawing.Size(139, 13);
            this.lblClientBill.TabIndex = 6;
            this.lblClientBill.Text = "Счет получателя платежа:";
            // 
            // txtClientName
            // 
            this.txtClientName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtClientName.EditValue = "";
            this.txtClientName.Location = new System.Drawing.Point(155, 242);
            this.txtClientName.Name = "txtClientName";
            this.txtClientName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtClientName.Properties.Appearance.Options.UseBackColor = true;
            this.txtClientName.Properties.Appearance.Options.UseTextOptions = true;
            this.txtClientName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtClientName.Properties.ReadOnly = true;
            this.txtClientName.Size = new System.Drawing.Size(361, 20);
            this.txtClientName.TabIndex = 20;
            // 
            // txtClientBill
            // 
            this.txtClientBill.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtClientBill.EditValue = "";
            this.txtClientBill.Location = new System.Drawing.Point(155, 268);
            this.txtClientBill.Name = "txtClientBill";
            this.txtClientBill.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtClientBill.Properties.Appearance.Options.UseBackColor = true;
            this.txtClientBill.Properties.Appearance.Options.UseTextOptions = true;
            this.txtClientBill.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtClientBill.Properties.ReadOnly = true;
            this.txtClientBill.Size = new System.Drawing.Size(361, 20);
            this.txtClientBill.TabIndex = 21;
            // 
            // panelPayment1
            // 
            this.panelPayment1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPayment1.CashType = 1;
            this.panelPayment1.Location = new System.Drawing.Point(6, 12);
            this.panelPayment1.Name = "panelPayment1";
            this.panelPayment1.PaymentCash = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.panelPayment1.PaymentComment = "";
            this.panelPayment1.PaymentDate = new System.DateTime(2017, 3, 16, 9, 28, 33, 0);
            this.panelPayment1.PaymentSumm = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.panelPayment1.Size = new System.Drawing.Size(516, 188);
            this.panelPayment1.TabIndex = 0;
            this.panelPayment1.PayEvent += new Payments.Controls.PanelPayment.PayEventHandler(this.panelPayment1_PayEvent);
            this.panelPayment1.CommentFieldLostFocusEvent += new Payments.Controls.PanelPayment.CommentFieldEventHandler(this.panelPayment1_CommentFieldLostFocusEvent);
            // 
            // gridPaymentParameter1
            // 
            this.gridPaymentParameter1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPaymentParameter1.Location = new System.Drawing.Point(7, 295);
            this.gridPaymentParameter1.Name = "gridPaymentParameter1";
            this.gridPaymentParameter1.Size = new System.Drawing.Size(528, 325);
            this.gridPaymentParameter1.TabIndex = 2;
            // 
            // FrmPayEllis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 623);
            this.Controls.Add(this.txtClientBill);
            this.Controls.Add(this.txtClientName);
            this.Controls.Add(this.lblClientBill);
            this.Controls.Add(this.lblClientName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblAuthcod);
            this.Controls.Add(this.txtAuthcod);
            this.Controls.Add(this.gridPaymentParameter1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(559, 661);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(559, 661);
            this.Name = "FrmPayEllis";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оплата коммунальных платежей";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPayEllis_FormClosing);
            this.Shown += new System.EventHandler(this.FrmPayEllis_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.txtAuthcod.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtClientName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientBill.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.PanelPayment panelPayment1;
        private Controls.GridPaymentParameter gridPaymentParameter1;
        private DevExpress.XtraEditors.TextEdit txtAuthcod;
        private System.Windows.Forms.Label lblAuthcod;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblClientName;
        private System.Windows.Forms.Label lblClientBill;
        private DevExpress.XtraEditors.TextEdit txtClientName;
        private DevExpress.XtraEditors.TextEdit txtClientBill;

    }
}