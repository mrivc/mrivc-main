﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XadesDemo
{
    public class ExportHouseData : MrivcExportCommand, ISingleHouseCommand
    {
        bool _isRSO;

        public string FiasHouseGuid { get; set; } = null;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                base.SenderID = value;
            }
        }

        public ExportHouseData(string senderID) : base ("exportHouseData", "HomeManagementAsync", senderID, "exportHouseResult")
        {
            MrivcData.CreateTempExportHouseTables();
            Description = "Экспорт информации о домах, подъездах, помещениях из ГИС \"ЖКХ\"";
        }

        new void ExecuteToTempDB()
        {
            base.ExecuteToTempDB(new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) }, _exportNodeName);
        }

        public new void Execute()
        {
            this.ExecuteToTempDB();
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("GIS.ExportHouseData", new object[,] { { "houseGUID", FiasHouseGuid } });
            DAccess.DataModule.ExecuteNonQueryStoredProcedure1("[GIS].[SaveExportedHouseData]", new object[,] { { "isRso", _isRSO } });
        }

        public DataSet PrepareToDataSet()
        {
            this.ExecuteToTempDB();
            return DAccess.DataModule.GetDataSetByStoredProcedure1("GIS.ExportHouseData", new object[,] { { "houseGUID", FiasHouseGuid } }, "Houses", "Entrance", "Premises");
        }
    }
}
