﻿using DevExpress.XtraEditors.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MassBillNew
{
    public partial class FineCalculation : Form
    {
        public FineCalculation()
        {
            InitializeComponent();

            this.Text = StaticConfig.Title(this.Text);

            accountFilter1.Init(AccountFilter.FilterType.Account, true);

            SqlClass.Open();


            SqlClass.Close();
        }


        private void simpleButton1_Click(object sender, EventArgs e)
        {
            object[,] prms = {             
            { "begin_date", dateEdit1.DateTime },
            { "end_date",   dateEdit2.DateTime },

            { "account_query", accountFilter1.CreateSqlQuery()} };

            SqlClass.Open();
            SqlClass.ExecuteNonQuerySP("town.Fine_Calculation",prms);
            SqlClass.Close();

        }

        private void FineCalculation_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'murmanskDataSet.Company2' table. You can move, or remove it, as needed.
            this.company2TableAdapter.Fill(this.murmanskDataSet.Company2);
            // TODO: This line of code loads data into the 'murmanskDataSet.Service' table. You can move, or remove it, as needed.
            this.serviceTableAdapter.Fill(this.murmanskDataSet.Service);

        }

       
    }
}
