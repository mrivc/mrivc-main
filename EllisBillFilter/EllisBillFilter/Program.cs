﻿using System;
using System.Windows.Forms;
using EllisBillFilter.DS;
using EllisBillFilter.Helpers;

namespace EllisBillFilter
{
    using System.Collections.Generic;

    using EllisBillFilter.Properties;
    using LoadMeasure;
    using Lawsuit;
    using Forms;
    using Logic;
    using MrivcHelpers.Helpers;
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
            //try
            //{
            //string conn = "Provider=SQLNCLI.1;Password=;PersistSecurityInfo=True;UserID=;InitialCatalog=Murmansk;DataSource=Srv-mrivc7;UseProcedureforPrepare=1;AutoTranslate=True;PacketSize=4096;WorkstationID=OTO-EL1;UseEncryptionforData=False;Tagwithcolumncollationwhenpossible=False;MARSConnection=False;DataTypeCompatibility=0;TrustServerCertificate=False";
            string conn = "Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Persist Security Info=false;Integrated security=true;Connect Timeout=200";
            //string conn = "Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Persist Security Info=true;Integrated security=false;User ID=bobrovsky;Password=";
            //string conn = "Data Source=SRV-MRIVC7;Initial Catalog=Murmansk;Persist Security Info=true;Integrated security=false;User ID=butorin;Password=";
            Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                if (args.Length > 0 && !args[0].Equals("start", StringComparison.InvariantCultureIgnoreCase))
                {
                    string[] prms = string.Concat(args).Split(new Char[] { '|' });

           
                DAccess.DataModule.ConnectionString = prms[0];
                    MrivcControls.Data.DAccess.DataModule.ConnectionString = prms[0];

                switch (GetExecuteModule(prms))
                {
                        case "loadmeasure":
                            Application.Run(new LoadMeasureForm(DAccess.DataModule.ConnectionString));
                            break;
                        case "mainform":
                            Application.Run(new FrmMain());
                            break;
                        case "agreementform":
                            Application.Run(new FrmAgreement(true));
                            break;
                        case "printbillacc":
                            Application.Run(new FrmPrintBillAcc(GetAccountID(prms)));
                            break;
                        case "printdebtbillacc":
                            Application.Run(new FrmPrintDebtBillAcc(GetAccountID(prms), DAccess.Instance.WorkPeriod));
                            break;
                        case "claimadd":
                            Application.Run(new FrmClaim(GetAccountID(prms), DAccess.Instance.WorkPeriod));
                            break;
                        case "lawsuitadd":
                            Application.Run(new LawsuitAddForm(GetAccountID(prms), DAccess.Instance.WorkPeriod));
                            break;
                        case "lawsuitedit":
                            Application.Run(new LawsuitEditForm(GetAccountID(prms), DAccess.Instance.WorkPeriod));
                            break;
                        case "lawsuitsearch":
                            Application.Run(new LawsuitSearchForm());
                            break;
                        case "lawsuitgissearch":
                            Application.Run(new LawsuitSearchGISForm(DAccess.DataModule.ConnectionString));
                            break;
                        case "fb1paymentsmrivc":
                            FRHelper.Execute("FB1_Payments_MRIVC");
                            break;
                        case "fb2paymentskassaitog":
                            FRHelper.Execute("FB2_Payments_Kassa_Itog");
                            break;
                        case "sbersaldo":
                            Application.Run(new FrmSberSaldo());
                            break;
                        case "pivotprinthistory":
                            Application.Run(new FrmPrintHistoryPivotAnalisys());
                            break;
                        case "frmpivotsataticticbyservicedaccounts":
                            Application.Run(new FrmPivotSataticticByServicedAccounts());
                            break;
                        case "frmprinthistory":
                            Application.Run(new FrmPrintHistory());
                            break;
                        case "frmsbersaldosettings":
                            Application.Run(new FrmSberSaldoSettings());
                            break;
                        case "frmprintbilldelivery":
                            Application.Run(new FrmPrintBillDelivery(GetAccountID(prms)));
                            break;
                        case "frmexcelimport":
                            Application.Run(new FrmExcelImport());
                            break;
                        default:
                            MessageBox.Show(string.Concat(args));
                            break;
                    }
                }
                //запуск программы с аргументами => EllisBillFilter.exe start -sendreestr
                else if (args.Length > 0 && args[0].Equals("start", StringComparison.InvariantCultureIgnoreCase))
                {
                    conn = Settings.Default.MurmanskConnectionString;

               
                DAccess.DataModule.SetConnection(conn);
                    MrivcControls.Data.DAccess.DataModule.SetConnection(conn);

                    switch (args[1])
                    {
                        case "-sendreestr":
                            ReestrGenerator.SendReestNow();
                            break;
                    }
                }
                else
                {
                    DAccess.DataModule.SetConnection(conn);
                    MrivcControls.Data.DAccess.DataModule.SetConnection(conn);
                //Application.Run(new FrmClaim(433363, DAccess.Instance.WorkPeriod));
                //Application.Run(new FrmBillPrint(conn));
                //Application.Run(new FrmMain());
                //Application.Run(new FrmPrintBillDelivery(355885));
                //Application.Run(new FrmPrintBillAcc(366376));
                //Application.Run(new FrmPrintDebtBillAcc(311027, DAccess.Instance.WorkPeriod));
                Application.Run(new FrmSberSaldo());
                //Application.Run(new FrmExcelImport());
                //Application.Run(new FrmSberSaldoSettings());
                //Application.Run(new FrmPrintHistoryPivotAnalisys());
                //Application.Run(new FrmPrintHistory());
                //FRHelper.Execute("FB1_Payments_MRIVC");
                //Application.Run(new FrmPivotSataticticByServicedAccounts());
            }
            //}
            //catch (Exception ex)
            //{
            //    if (ex is ApplicationException)
            //    {
            //        FileHelper.WriteLog(ex.ToString());
            //        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return;
            //    }

            //    FileHelper.WriteLog(ex.ToString());
            //    MessageBox.Show(ex.ToString());
            //}
        }

        static string GetExecuteModule(string[] prms)
        {
            if (prms[2].ToLower().Contains("execute") && prms[2].IndexOf("=") > -1)
            {
                string execute = prms[2].Substring(prms[2].IndexOf("=") + 1);
                return execute.ToLower();
            }
            return "";
        }

        static int GetAccountID(string[] prms)
        {
            if (prms[3].ToLower().Contains("accountid") && prms[3].IndexOf("=") > -1)
            {
                string accountId = prms[3].Substring(prms[3].IndexOf("=") + 1);
                return int.Parse(accountId);
            }
            return -1;
        }
    }
}
