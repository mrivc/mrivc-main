using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FastReport.Export;
using FastReport.Export.Svg;
using FastReport.Utils;

namespace FastReport.Forms
{
    internal partial class SVGExportForm : BaseExportForm
    {
        public override void Init(ExportBase export)
        {
            base.Init(export);
            SVGExport SVGExport = Export as SVGExport;
            comboBox1.SelectedIndex = (int)SVGExport.ImageFormat;
        }
        
        protected override void Done()
        {
            base.Done();
            SVGExport SVGExport = Export as SVGExport;
            SVGExport.ImageFormat = (SVGImageFormat)comboBox1.SelectedIndex;
        }
        
        public override void Localize()
        {
            base.Localize();
            MyRes res = new MyRes("Export,SVG");
            Text = res.Get("");
            res = new MyRes("Export,Misc");
            gbOptions.Text = res.Get("Options");
            lblImageFormat.Text = res.Get("Pictures");
        }        
        
        public SVGExportForm()
        {
            InitializeComponent();
        }

    }
}

