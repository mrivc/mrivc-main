﻿namespace EllisBillFilter
{
    partial class FrmClientDetailEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.vGridControl1 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.clientDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mDataSet = new EllisBillFilter.DS.MDataSet();
            this.riClientPayReceiver = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.payReceiverCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.riDeriveCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.deriveCompanyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.riBillFilter = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.billFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.riPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.periodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.riServiceList = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.riFilterType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.filterTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.riPrefixButton = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.rowID_Client_Detail = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowID_Client = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowID_Derive_Company = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowID_Bill_Filter = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowBegin_Period = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowEnd_Period = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAdvance = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRoundTo = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPrefix = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowShow_Meter_For_Services = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowID_Filter_Type = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowBill_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.companyServiceNumberBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.companyServiceNumberBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riClientPayReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeriveCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riServiceList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFilterType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPrefixButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyServiceNumberBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyServiceNumberBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // vGridControl1
            // 
            this.vGridControl1.DataSource = this.clientDetailBindingSource;
            this.vGridControl1.Location = new System.Drawing.Point(0, 0);
            this.vGridControl1.Name = "vGridControl1";
            this.vGridControl1.RecordWidth = 250;
            this.vGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riClientPayReceiver,
            this.riDeriveCompany,
            this.riBillFilter,
            this.riPeriod,
            this.riServiceList,
            this.riFilterType,
            this.repositoryItemLookUpEdit1,
            this.riPrefixButton});
            this.vGridControl1.RowHeaderWidth = 224;
            this.vGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowID_Client_Detail,
            this.rowID_Client,
            this.rowID_Derive_Company,
            this.rowID_Bill_Filter,
            this.rowBegin_Period,
            this.rowEnd_Period,
            this.rowAdvance,
            this.rowRoundTo,
            this.rowPrefix,
            this.rowShow_Meter_For_Services,
            this.rowID_Filter_Type,
            this.rowBill_Name});
            this.vGridControl1.Size = new System.Drawing.Size(484, 216);
            this.vGridControl1.TabIndex = 0;
            // 
            // clientDetailBindingSource
            // 
            this.clientDetailBindingSource.DataMember = "ClientDetail";
            this.clientDetailBindingSource.DataSource = this.mDataSet;
            // 
            // mDataSet
            // 
            this.mDataSet.DataSetName = "MDataSet";
            this.mDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // riClientPayReceiver
            // 
            this.riClientPayReceiver.AutoHeight = false;
            this.riClientPayReceiver.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riClientPayReceiver.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Наименование", 250, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Company", "ID_Company", 85, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Provider", "Is_Provider", 65, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Derive_Company", "Is_Derive_Company", 107, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Pay_Receiver", "Is_Pay_Receiver", 91, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Meter_Future_Period_Day", "Meter_Future_Period_Day", 137, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Load_Measure_In_Future", "Load_Measure_In_Future", 134, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far)});
            this.riClientPayReceiver.DataSource = this.payReceiverCompanyBindingSource;
            this.riClientPayReceiver.DisplayMember = "Name";
            this.riClientPayReceiver.Name = "riClientPayReceiver";
            this.riClientPayReceiver.NullText = "";
            this.riClientPayReceiver.PopupFormMinSize = new System.Drawing.Size(280, 240);
            this.riClientPayReceiver.ShowFooter = false;
            this.riClientPayReceiver.ValueMember = "ID_Company";
            this.riClientPayReceiver.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.riClientPayReceiver_EditValueChanging);
            // 
            // payReceiverCompanyBindingSource
            // 
            this.payReceiverCompanyBindingSource.DataMember = "PayReceiverCompany";
            this.payReceiverCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // riDeriveCompany
            // 
            this.riDeriveCompany.AutoHeight = false;
            this.riDeriveCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riDeriveCompany.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Наименование", 250, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Company", "ID_Company", 85, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Provider", "Is_Provider", 65, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Derive_Company", "Is_Derive_Company", 107, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Pay_Receiver", "Is_Pay_Receiver", 91, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Meter_Future_Period_Day", "Meter_Future_Period_Day", 137, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Load_Measure_In_Future", "Load_Measure_In_Future", 134, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name265", "Name265", 55, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.riDeriveCompany.DataSource = this.deriveCompanyBindingSource;
            this.riDeriveCompany.DisplayMember = "Name";
            this.riDeriveCompany.Name = "riDeriveCompany";
            this.riDeriveCompany.NullText = "";
            this.riDeriveCompany.PopupFormMinSize = new System.Drawing.Size(280, 240);
            this.riDeriveCompany.ShowFooter = false;
            this.riDeriveCompany.ValueMember = "ID_Company";
            // 
            // deriveCompanyBindingSource
            // 
            this.deriveCompanyBindingSource.DataMember = "DeriveCompany";
            this.deriveCompanyBindingSource.DataSource = this.mDataSet;
            // 
            // riBillFilter
            // 
            this.riBillFilter.AutoHeight = false;
            this.riBillFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riBillFilter.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Bill_Filter", "ID_Bill_Filter", 82, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Bill_Filter", "Bill_Filter", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tag", "Tag", 28, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "Info", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("IsActivate", "Is Activate", 62, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Account_Type", "Account_Type", 79, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Position", "Position", 47, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far)});
            this.riBillFilter.DataSource = this.billFilterBindingSource;
            this.riBillFilter.DisplayMember = "Bill_Filter";
            this.riBillFilter.Name = "riBillFilter";
            this.riBillFilter.NullText = "";
            this.riBillFilter.PopupFormMinSize = new System.Drawing.Size(250, 240);
            this.riBillFilter.ShowHeader = false;
            this.riBillFilter.ShowLines = false;
            this.riBillFilter.ValueMember = "ID_Bill_Filter";
            // 
            // billFilterBindingSource
            // 
            this.billFilterBindingSource.DataMember = "BillFilter";
            this.billFilterBindingSource.DataSource = this.mDataSet;
            // 
            // riPeriod
            // 
            this.riPeriod.AutoHeight = false;
            this.riPeriod.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riPeriod.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Period", "Period", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Is_Work_Period", "Is_Work_Period", 86, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.riPeriod.DataSource = this.periodBindingSource;
            this.riPeriod.DisplayMember = "Name";
            this.riPeriod.Name = "riPeriod";
            this.riPeriod.NullText = "";
            this.riPeriod.ShowFooter = false;
            this.riPeriod.ShowHeader = false;
            this.riPeriod.ValueMember = "Period";
            // 
            // periodBindingSource
            // 
            this.periodBindingSource.DataMember = "Period";
            this.periodBindingSource.DataSource = this.mDataSet;
            // 
            // riServiceList
            // 
            this.riServiceList.AutoHeight = false;
            this.riServiceList.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.riServiceList.Name = "riServiceList";
            this.riServiceList.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.riServiceList_ButtonClick);
            // 
            // riFilterType
            // 
            this.riFilterType.AutoHeight = false;
            this.riFilterType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riFilterType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_Filter_Type", "ID_Filter_Type", 94, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riFilterType.DataSource = this.filterTypeBindingSource;
            this.riFilterType.DisplayMember = "Name";
            this.riFilterType.Name = "riFilterType";
            this.riFilterType.NullText = "";
            this.riFilterType.PopupFormMinSize = new System.Drawing.Size(100, 50);
            this.riFilterType.PopupWidth = 100;
            this.riFilterType.ShowFooter = false;
            this.riFilterType.ShowHeader = false;
            this.riFilterType.ValueMember = "ID_Filter_Type";
            // 
            // filterTypeBindingSource
            // 
            this.filterTypeBindingSource.DataMember = "FilterType";
            this.filterTypeBindingSource.DataSource = this.mDataSet;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // riPrefixButton
            // 
            this.riPrefixButton.AutoHeight = false;
            this.riPrefixButton.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.riPrefixButton.Name = "riPrefixButton";
            this.riPrefixButton.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.riPrefixButton_ButtonClick);
            // 
            // rowID_Client_Detail
            // 
            this.rowID_Client_Detail.Enabled = false;
            this.rowID_Client_Detail.Name = "rowID_Client_Detail";
            this.rowID_Client_Detail.Properties.Caption = "ИД";
            this.rowID_Client_Detail.Properties.FieldName = "ID_Client_Detail";
            this.rowID_Client_Detail.Properties.ReadOnly = true;
            this.rowID_Client_Detail.Visible = false;
            // 
            // rowID_Client
            // 
            this.rowID_Client.Name = "rowID_Client";
            this.rowID_Client.Properties.Caption = "Получатель платежа*";
            this.rowID_Client.Properties.FieldName = "ID_Client";
            this.rowID_Client.Properties.RowEdit = this.riClientPayReceiver;
            // 
            // rowID_Derive_Company
            // 
            this.rowID_Derive_Company.Name = "rowID_Derive_Company";
            this.rowID_Derive_Company.Properties.Caption = "Управляющая компания*";
            this.rowID_Derive_Company.Properties.FieldName = "ID_Derive_Company";
            this.rowID_Derive_Company.Properties.RowEdit = this.riDeriveCompany;
            // 
            // rowID_Bill_Filter
            // 
            this.rowID_Bill_Filter.Name = "rowID_Bill_Filter";
            this.rowID_Bill_Filter.Properties.Caption = "Код фильтра*";
            this.rowID_Bill_Filter.Properties.FieldName = "ID_Bill_Filter";
            this.rowID_Bill_Filter.Properties.RowEdit = this.riBillFilter;
            // 
            // rowBegin_Period
            // 
            this.rowBegin_Period.Name = "rowBegin_Period";
            this.rowBegin_Period.Properties.Caption = "Действует С*";
            this.rowBegin_Period.Properties.FieldName = "Begin_Period";
            this.rowBegin_Period.Properties.RowEdit = this.riPeriod;
            // 
            // rowEnd_Period
            // 
            this.rowEnd_Period.Name = "rowEnd_Period";
            this.rowEnd_Period.Properties.Caption = "Действует По";
            this.rowEnd_Period.Properties.FieldName = "End_Period";
            this.rowEnd_Period.Properties.RowEdit = this.riPeriod;
            // 
            // rowAdvance
            // 
            this.rowAdvance.Appearance.Options.UseTextOptions = true;
            this.rowAdvance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.rowAdvance.Height = 16;
            this.rowAdvance.Name = "rowAdvance";
            this.rowAdvance.Properties.Caption = "Аванс*";
            this.rowAdvance.Properties.FieldName = "Advance";
            // 
            // rowRoundTo
            // 
            this.rowRoundTo.Appearance.Options.UseTextOptions = true;
            this.rowRoundTo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.rowRoundTo.Name = "rowRoundTo";
            this.rowRoundTo.Properties.Caption = "Тип округления*";
            this.rowRoundTo.Properties.FieldName = "RoundTo";
            this.rowRoundTo.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            // 
            // rowPrefix
            // 
            this.rowPrefix.Appearance.Options.UseTextOptions = true;
            this.rowPrefix.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.rowPrefix.Name = "rowPrefix";
            this.rowPrefix.Properties.Caption = "Префикс (номер услуги СБ РФ)";
            this.rowPrefix.Properties.FieldName = "Prefix";
            this.rowPrefix.Properties.RowEdit = this.riPrefixButton;
            // 
            // rowShow_Meter_For_Services
            // 
            this.rowShow_Meter_For_Services.Appearance.Options.UseTextOptions = true;
            this.rowShow_Meter_For_Services.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.rowShow_Meter_For_Services.Name = "rowShow_Meter_For_Services";
            this.rowShow_Meter_For_Services.Properties.Caption = "Ввод показаний для услуг";
            this.rowShow_Meter_For_Services.Properties.FieldName = "Show_Meter_For_Services";
            this.rowShow_Meter_For_Services.Properties.RowEdit = this.riServiceList;
            // 
            // rowID_Filter_Type
            // 
            this.rowID_Filter_Type.Name = "rowID_Filter_Type";
            this.rowID_Filter_Type.Properties.Caption = "Тип кода фильтра";
            this.rowID_Filter_Type.Properties.FieldName = "ID_Filter_Type";
            this.rowID_Filter_Type.Properties.RowEdit = this.riFilterType;
            // 
            // rowBill_Name
            // 
            this.rowBill_Name.Name = "rowBill_Name";
            this.rowBill_Name.Properties.Caption = "Наименование квитанции";
            this.rowBill_Name.Properties.FieldName = "Bill_Name";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(307, 226);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "ОК";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(398, 226);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // companyServiceNumberBindingSource
            // 
            this.companyServiceNumberBindingSource.DataMember = "CompanyServiceNumber";
            this.companyServiceNumberBindingSource.DataSource = this.mDataSet;
            // 
            // companyServiceNumberBindingSource1
            // 
            this.companyServiceNumberBindingSource1.DataMember = "CompanyServiceNumber";
            this.companyServiceNumberBindingSource1.DataSource = this.mDataSet;
            // 
            // FrmClientDetailEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 257);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.vGridControl1);
            this.Name = "FrmClientDetailEdit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактор код фильтра";
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riClientPayReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.payReceiverCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDeriveCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deriveCompanyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riBillFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riServiceList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFilterType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPrefixButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyServiceNumberBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyServiceNumberBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource clientDetailBindingSource;
        private DS.MDataSet mDataSet;
        private DevExpress.XtraVerticalGrid.VGridControl vGridControl1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowID_Client_Detail;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowID_Client;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowID_Derive_Company;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowID_Bill_Filter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowBegin_Period;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowEnd_Period;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAdvance;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRoundTo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPrefix;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowShow_Meter_For_Services;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riClientPayReceiver;
        private System.Windows.Forms.BindingSource payReceiverCompanyBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riDeriveCompany;
        private System.Windows.Forms.BindingSource deriveCompanyBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riBillFilter;
        private System.Windows.Forms.BindingSource billFilterBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riPeriod;
        private System.Windows.Forms.BindingSource periodBindingSource;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit riServiceList;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowID_Filter_Type;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riFilterType;
        private System.Windows.Forms.BindingSource filterTypeBindingSource;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowBill_Name;
        private System.Windows.Forms.BindingSource companyServiceNumberBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource companyServiceNumberBindingSource1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit riPrefixButton;
    }
}