﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ImportAdditionalServices : MrivcImportCommand, IMrivcImportCommandAsync
    {
        ExportDataProvider exportDataProvider;

        public ImportAdditionalServices(string senderID) : base("importAdditionalServices", "NsiAsync", senderID)
        {
            _importValueName = "ImportAdditionalServiceType";
            Description = "Синхронизация услуг";

            exportDataProvider = new ExportDataProvider(SenderID);
        }

        public new void Execute()
        {
            ExportData();
            ImportData();
            ExportData();
        }

        public void ExportData()
        {
            if (!this.IsEmpty && !HasErrors)
            {
                exportDataProvider.AddToReport = this.AddToReport;
                exportDataProvider.Description = this.Description;
                exportDataProvider.CommandType = CommandType;
                var dataProviderRegNumbers = "1";
                exportDataProvider.ExecuteToDataBase(dataProviderRegNumbers.Split(','));

                HasErrors = exportDataProvider.HasErrors;
            }
        }

        public void ImportData()
        {
            if (!exportDataProvider.HasErrors)
                base.Execute();
        }

        protected override void CreateXmlBody()
        {
            var addServices = DAccess.DataModule.GetDataTableByQueryCommand1("exec GIS.Prepare_Import_Additional_Services @senderID", new[,] { { "@senderID", SenderID } });

            var commonValues = new[]
            {
                new Tuple<string[],string>(new [] { "AdditionalServiceTypeName" },"AdditionalServiceTypeName"),
                new Tuple<string[],string>(new [] { "OKEI" },"OKEI"),
            };

            var updateValues = new[]
            {
                new Tuple<string[],string>(new [] { "ElementGuid" },"ElementGuid")
            };

            foreach (DataRow row in addServices.Select())
            {
                AddChildElement(row, "NsiAsync", "importAdditionalServices_add", "importAdditionalServices_update", commonValues, null, updateValues, "importAdditionalServicesRequest");
            }
        }

        public override object Clone()
        {
            var exportDataProviderClone = (ExportDataProvider)exportDataProvider.Clone();
            var baseClone = (MrivcCommand)base.Clone();
            ((ImportAdditionalServices)baseClone).exportDataProvider = exportDataProviderClone;
            return baseClone;
        }
    }
}
