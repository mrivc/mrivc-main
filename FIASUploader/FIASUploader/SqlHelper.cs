﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASUploader
{
    static class SqlHelper
    {


        public static void TruncateTable(string tableName,string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);
            string sql = string.Format("truncate table {0}",tableName);
            SqlCommand sqlComm = new SqlCommand(sql, sqlConn);
            sqlConn.Open();
            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void MurmanskRegionOnly(string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);
            string sql = "dbo.MurmanskRegionOnly";
            SqlCommand sqlComm = new SqlCommand(sql, sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;
            sqlComm.CommandTimeout = 0;
            sqlConn.Open();
            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }


        public static string CreateTableScript(DataSet dataSet)
        {
            string s = string.Empty;

            foreach (DataTable table in dataSet.Tables)
            {
                s += "CREATE TABLE " + table.TableName + " (\n";

                foreach (DataColumn column in table.Columns)
                {
                    s += column.ColumnName + " " + GetSqlDbTypeName(column.DataType);

                    if (table.Columns.IndexOf(column) != table.Columns.Count - 1)
                        s += ",";

                    s += "\n";
                }

                s += ")\n\n";
            }

            return s;
        }

        static string GetSqlDbTypeName(Type theType)
        {
            SqlDbType sqlType = GetDBType(theType);

            switch (sqlType)
            {
                case SqlDbType.NVarChar:
                    return sqlType.ToString() + "(MAX)";
                case SqlDbType.VarChar:
                    return sqlType.ToString() + "(MAX)";
                case SqlDbType.Char:
                    return sqlType.ToString() + "(MAX)";
                case SqlDbType.NChar:
                    return sqlType.ToString() + "(MAX)";
                default:
                    return sqlType.ToString();
            }
        }


        private static SqlDbType GetDBType(Type theType)
        {
            SqlParameter p1;
            TypeConverter tc;
            p1 = new SqlParameter();
            tc = TypeDescriptor.GetConverter(p1.DbType);
            if (tc.CanConvertFrom(theType))
            {
                p1.DbType = (DbType)tc.ConvertFrom(theType.Name);
            }
            else
            {
                try
                {
                    p1.DbType = (DbType)tc.ConvertFrom(theType.Name);
                }
                catch (Exception)
                {
                    //Do Nothing; will return NVarChar as default
                }
            }


            return p1.SqlDbType;
        }
    }
}
