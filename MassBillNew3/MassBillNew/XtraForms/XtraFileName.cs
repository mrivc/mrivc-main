using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace MassBillNew
{
    public partial class XtraFileName : DevExpress.XtraEditors.XtraForm
    {
        private string fileName = string.Empty;
        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public XtraFileName(string exportFileName)
        {
            InitializeComponent();

            textEdit1.Text = exportFileName;
        }

        public XtraFileName()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            fileName = textEdit1.Text;
        }
    }
}