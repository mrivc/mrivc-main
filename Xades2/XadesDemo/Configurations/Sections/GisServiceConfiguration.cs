using System.Configuration;

namespace XadesDemo.Configurations.Sections
{
    public class GisServiceConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("Services")]
        public ServiceCollection Services
        {
            get { return (ServiceCollection) (base["Services"]); }
        } 

        [ConfigurationProperty("SenderId", IsRequired = true)]
        public string SenderId
        { get; set; }
        //MRIVC
        //{
        //    get { return (string)base["SenderId"]; }
        //    set { base["SenderId"] = value; }
        //}

        [ConfigurationProperty("SchemaVersion", IsRequired = true)]
        public string SchemaVersion
        {
            get { return (string)base["SchemaVersion"]; }
            set { base["SchemaVersion"] = value; }
        }

        [ConfigurationProperty("ExportNsiRegistryNumbers", IsRequired = true)]
        public string ExportNsiRegistryNumbers
        {
            get { return (string)base["ExportNsiRegistryNumbers"]; }
            set { base["ExportNsiRegistryNumbers"] = value; }
        }

        [ConfigurationProperty("AdminEmails", IsRequired = true)]
        public string AdminEmails
        {
            get { return (string)base["AdminEmails"]; }
            set { base["AdminEmails"] = value; }
        }

        [ConfigurationProperty("BaseUrl", IsRequired = true)]
        public string BaseUrl
        {
            get { return (string)base["BaseUrl"]; }
            set { base["BaseUrl"] = value; }
        }

        [ConfigurationProperty("SoapConfiguration", IsRequired = true)]
        public SoapConfiguration SoapConfiguration
        {
            get { return (SoapConfiguration)base["SoapConfiguration"]; }
            set { base["SoapConfiguration"] = value; }
        }
    }
}