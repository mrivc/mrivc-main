﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MrivcControls.Data
{
    public class DAccess
    {
        private static DAccess instance;
        private static DModule dModule;

        public static DAccess Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DAccess();
                }
                return instance;
            }
        }

        public static DModule DataModule
        {
            get
            {
                if (dModule == null)
                {
                    dModule = new DModule();
                }
                return dModule;
            }
        }

        int workPeriod = -1;
        public int WorkPeriod
        {
            get
            {
                if (workPeriod == -1)
                    workPeriod = DataModule.GetWorkPeriod();
                return workPeriod;
            }
        }

        int userID = -1;
        public int UserId
        {
            get
            {
                if (userID == -1)
                    userID = DataModule.GetCurrentUserId();
                return userID;
            }
        }

        int userIsAdmin = -1;
        public bool UserIsAdmin
        {
            get
            {
                if (userIsAdmin == -1)
                    userIsAdmin = DataModule.CurrentUserIsAdmin() ? 1 : 0;
                return userIsAdmin == 1 ? true : false;
            }
        }

        CDataSet.PeriodDataTable periods;
        public CDataSet.PeriodDataTable Periods
        {
            get
            {
                if (periods == null)
                {
                    periods = new CDataSet.PeriodDataTable();
                    DAccess.DataModule.PeriodSelect(periods);
                }
                return periods;
            }
        }

        CDataSet.CompanyDataTable company;
        public CDataSet.CompanyDataTable Company
        {
            get
            {
                if (company == null)
                {
                    company = new CDataSet.CompanyDataTable();
                    DAccess.DataModule.CompanySelect(company);
                }
                return company;
            }
        }

        CDataSet.EmployeeDataTable employee;
        public CDataSet.EmployeeDataTable Employee
        {
            get
            {
                if (employee == null)
                {
                    employee = new CDataSet.EmployeeDataTable();
                    DAccess.DataModule.EmployeeSelect(employee);
                }
                return employee;
            }
        }

        CDataSet.Bill_KindDataTable billKind;
        public CDataSet.Bill_KindDataTable BillKind
        {
            get
            {
                if (billKind == null)
                {
                    billKind = new CDataSet.Bill_KindDataTable();
                    DAccess.DataModule.BillKindSelect(billKind);
                }
                return billKind;
            }
        }

    }
}
