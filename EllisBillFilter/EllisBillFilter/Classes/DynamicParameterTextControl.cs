﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    public class DynamicParameterTextControl : DynamicParameterBaseControl
    {
        TextBox textBox;

        public DynamicParameterTextControl(DynamicParameter parameter) : base(parameter)
        {
        }

        public override object EditValue
        {
            get
            {
                return textBox.Text;
            }
        }

        protected override Control CreateControl()
        {
            textBox = new TextBox();
            return textBox;
        }
    }
}
