﻿using System;
using System.Windows.Forms;

namespace LoadingMeasureFromFile
{
    public partial class DataBaseConnectionForm : Form
    {
        private string _login;
        private string _password;

        public DataBaseConnectionForm()
        {
            InitializeComponent();

            Init();
        }

        private void Init()
        {
            _login = "";
            _password = "";

            foreach (Control control in AuthPanel.Controls)
            {
                control.KeyDown += EnterKeyDownHandler;
            }
        }

        private void CheckAuthFields()
        {
            if (_login == string.Empty) throw new EmptyFieldException("Логин");
            if (_password == string.Empty) throw new EmptyFieldException("Пароль");
        }

        private void ClearFields()
        {
            loginTextBox.Text = "";
            passwordTextBox.Text = "";
        }

        private void Authorization()
        {
            Hide();

            try
            {
                CheckAuthFields();

                User user = new User(_login, _password);

                using (MainForm childForm = new MainForm(user))
                {
                    childForm.ShowDialog(this);
                }
            }
            catch (EmptyFieldException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ClearFields();
            }

            Show();
        }

        private void EnterKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Authorization();
            }
        }

        private void authButton_Click(object sender, EventArgs e)
        {
            Authorization();
        }

        private void loginTextBox_TextChanged(object sender, EventArgs e)
        {
            string login = (sender as TextBox).Text;
            _login = login;
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            string password = (sender as TextBox).Text;
            _password = password;
        }
    }
}
