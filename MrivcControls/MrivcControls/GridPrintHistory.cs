﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MrivcControls.Data;

namespace MrivcControls
{
    using DevExpress.XtraGrid.Columns;
    using DevExpress.XtraGrid.Views.Base;

    using MrivcHelpers.Helpers;

    public partial class GridPrintHistory : UserControl
    {
        public GridPrintHistory()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            cDataSet.Period.Merge(DAccess.Instance.Periods);
            cDataSet.Employee.Merge(DAccess.Instance.Employee);
            cDataSet.Company.Merge(DAccess.Instance.Company);
            cDataSet.Bill_Kind.Merge(DAccess.Instance.BillKind);
            DAccess.DataModule.DeliveryTypeSelect(cDataSet.Print_Delivery_Type);
            cbPeriod.EditValue = DAccess.Instance.WorkPeriod;

            LoadLayout();
            gridView1.Layout += new EventHandler(gridLayoutSave);
            gridViewPrintHistoryDetail.Layout += new EventHandler(gridLayoutSave);
        }

        private void splitContainerControl1_SizeChanged(object sender, EventArgs e)
        {
            splitContainerControl1.SplitterPosition = (int)(splitContainerControl1.Size.Height / 1.7);
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            cDataSet.Print_History_Detail.Clear();
            if (gridView1.FocusedRowHandle < 0 || gridView1.GetDataRow(gridView1.FocusedRowHandle) == null
                || gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Print"] == DBNull.Value)
            {
                return;
            }

            if (gridView1.GetDataRow(gridView1.FocusedRowHandle)["Comment"] != DBNull.Value)
                txtComment.Text = gridView1.GetDataRow(gridView1.FocusedRowHandle)["Comment"].ToString();
            else
                txtComment.Text = "";

            ReloadPrintHistoryDetailData();
        }

        private void cbPeriod_EditValueChanged(object sender, EventArgs e)
        {
            ReloadPrintHistoryData();
        }

        private void cbAllUserHistory_CheckedChanged(object sender, EventArgs e)
        {
            ReloadPrintHistoryData();
        }

        public void ReloadPrintHistoryData()
        {
            if (cbPeriod.EditValue == null)
                return;

            cDataSet.Print_History.Clear();
            int userId = (cbAllUserHistory.Checked ? -1 : DAccess.Instance.UserId);
            DAccess.DataModule.PrintHistorySelect(cDataSet.Print_History, (int)cbPeriod.EditValue, userId);
        }

        private void ReloadPrintHistoryDetailData()
        {
            if (gridView1.FocusedRowHandle < 0 || gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Print"] == DBNull.Value)
                return;

            cDataSet.Print_History_Detail.Clear();
            DAccess.DataModule.PrintHistoryDetailSelect(cDataSet.Print_History_Detail, (long)gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Print"]);
        }

        public void SaveLayout()
        {
            gridView1.SaveLayoutToXml("gridViewPrintHistory.xml");
            gridViewPrintHistoryDetail.SaveLayoutToXml(gridViewPrintHistoryDetail.Name + ".xml");
        }

        public void LoadLayout()
        {
            if (FileHelper.ExistFile("gridViewPrintHistory.xml"))
                gridView1.RestoreLayoutFromXml("gridViewPrintHistory.xml");
            if (FileHelper.ExistFile(gridViewPrintHistoryDetail.Name + ".xml"))
                gridViewPrintHistoryDetail.RestoreLayoutFromXml(gridViewPrintHistoryDetail.Name + ".xml");

            gridView1.ActiveFilter.Remove(colIssued_Date);
        }

        void gridLayoutSave(object sender, EventArgs e)
        {
            SaveLayout();
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == colIssued_Date && gridView1.GetDataRow(e.RowHandle)["ID_Delivery_Type"] == DBNull.Value)
            {
                gridView1.GetDataRow(e.RowHandle)["ID_Delivery_Type"] = 1;
            }

            SavePrintHistory();
        }

        private void SavePrintHistory()
        { 
            ((BindingSource)gridView1.DataSource).EndEdit();
            DAccess.DataModule.PrintHistoryUpdate(cDataSet.Print_History);
        }

        private void SavePrintHistoryDetail()
        {
            ((BindingSource)gridPrintHistoryDetail.DataSource).EndEdit();
            DAccess.DataModule.PrintHistoryDetailUpdate(cDataSet.Print_History_Detail);
        }

        private void SetCheckedAllDetails(bool checkedState)
        {
            foreach (CDataSet.Print_History_DetailRow r in cDataSet.Print_History_Detail.Rows)
            {
                r.Checked = checkedState;
                r.CheckedBy = DAccess.Instance.UserId;
            }
        }

        private void cmsPrintHistory_Refresh_Click(object sender, EventArgs e)
        {
            ReloadPrintHistoryData();
        }

        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            if (gridView1.FocusedColumn.FieldName.ToLower() == "issued_date" && e.Value != DBNull.Value)
            {
                DataRow row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
                if ((bool)row["Checked"] != true)
                {
                    MessageBox.Show("Вы не можете выдать квитанции, пока не будет установлен признак, что квитанции проверены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    gridView1.ActiveEditor.Reset();
                    gridView1.ActiveEditor.ResetText();
                    e.Valid = false;
                    return;
                }

                if (DateTime.Parse(((DateTime)e.Value).ToShortDateString()) < DateTime.Parse(((DateTime)row["PrintDate"]).ToShortDateString()))
                {
                    MessageBox.Show("Дата выдачи квитанций должна быть больше или равна дате печати!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    gridView1.ActiveEditor.Reset();
                    gridView1.ActiveEditor.ResetText();
                    e.Valid = false;
                    return;
                }

                gridView1.GetDataRow(gridView1.FocusedRowHandle)["IssuedBy"] = DAccess.Instance.UserId;
            }
            e.Valid = true;
        }

        private void cmsPrintHistory_Checked_Click(object sender, EventArgs e)
        {
            if (gridView1.GetDataRow(gridView1.FocusedRowHandle)["Issued_Date"] != DBNull.Value)
            {
                MessageBox.Show("Вы не можете изменять статус проверки квитанций, т.к. они выданы заказчику!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gridView1.GetDataRow(gridView1.FocusedRowHandle)["CheckedBy"] = DAccess.Instance.UserId;
            gridView1.GetDataRow(gridView1.FocusedRowHandle)["Checked"] = !(bool)gridView1.GetDataRow(gridView1.FocusedRowHandle)["Checked"];
            SetCheckedAllDetails((bool)gridView1.GetDataRow(gridView1.FocusedRowHandle)["Checked"]);

            SavePrintHistory();
            SavePrintHistoryDetail();

            ReloadPrintHistoryDetailData();
        }

        private void cmsPrintHistory_Clear_IssuedDate_Click(object sender, EventArgs e)
        {
            gridView1.GetDataRow(gridView1.FocusedRowHandle)["Issued_Date"] = DBNull.Value;
            gridView1.GetDataRow(gridView1.FocusedRowHandle)["ID_Delivery_Type"] = DBNull.Value;
            SavePrintHistory();
        }

        private void cmdPrintHistory_Opening(object sender, CancelEventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                cmsPrintHistory_Checked.Enabled = false;
                cmdPrintDetail_CheckAll.Enabled = false;
                cmdPrintDetail_UnCheckAll.Enabled = false;
                cmsPrintHistory_Filter.Enabled = false;
                cmsPrintHistory_FilterClear.Enabled = false;
                cmsPrintHistory_Delete.Enabled = false;
                cmsPrintHistory_Clear_IssuedDate.Enabled = false;
                cmsPrintHistory_ShowChanges.Enabled = false;
                return;
            }

            cmsPrintHistory_Checked.Enabled = true;
            cmdPrintDetail_CheckAll.Enabled = true;
            cmdPrintDetail_UnCheckAll.Enabled = true;
            cmsPrintHistory_Filter.Enabled = true;
            cmsPrintHistory_FilterClear.Enabled = true;
            cmsPrintHistory_Delete.Enabled = true;
            cmsPrintHistory_Clear_IssuedDate.Enabled = true;
            cmsPrintHistory_ShowChanges.Enabled = true;


            if ((bool)gridView1.GetDataRow(gridView1.FocusedRowHandle)["Checked"] != true)
            {
                cmsPrintHistory_Checked.Text = "Установить статус Проверено";
                cmsPrintHistory_Checked.Image = Properties.Resources.check;
            }
            else
            {
                if (gridView1.GetDataRow(gridView1.FocusedRowHandle)["Issued_Date"] != DBNull.Value)
                {
                    cmsPrintHistory_Checked.Enabled = false;
                    cmdPrintDetail_CheckAll.Enabled = false;
                    cmdPrintDetail_UnCheckAll.Enabled = false;
                }

                cmsPrintHistory_Checked.Image = Properties.Resources.delete;
                cmsPrintHistory_Checked.Text = "Снять статус Проверки";
            }

            if (gridView1.GetDataRow(gridView1.FocusedRowHandle)["Issued_Date"] != DBNull.Value)
                cmsPrintHistory_Clear_IssuedDate.Enabled = true;
            else
                cmsPrintHistory_Clear_IssuedDate.Enabled = false;

            if ((int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["PrintBy"] != DAccess.Instance.UserId &&
                !DAccess.Instance.UserIsAdmin)
            {
                cmsPrintHistory_Checked.Enabled = false;
            }
        }

        private void cmsPrintHistory_Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(string.Format("Вы уверены что хотите удалить {0} запись(-ей) ?", gridView1.SelectedRowsCount), "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Cursor = Cursors.WaitCursor;
                gridView1.DeleteSelectedRows();
                SavePrintHistory();
                Cursor = Cursors.Default;
            }
        }

        private void cmdPrintDetail_CheckAll_Click(object sender, EventArgs e)
        {
            SetCheckedAllDetails(true);
            SavePrintHistoryDetail();
        }

        private void cmdPrintDetail_UnCheckAll_Click(object sender, EventArgs e)
        {
            SetCheckedAllDetails(false);
            SavePrintHistoryDetail();
        }

        private void cmdPrintDetail_Opening(object sender, CancelEventArgs e)
        {
            SetEnabledForDetailContextMenu(true);

            if ((bool)gridView1.GetDataRow(gridView1.FocusedRowHandle)["Checked"] == true)
            {
                if (gridView1.GetDataRow(gridView1.FocusedRowHandle)["Issued_Date"] != DBNull.Value)
                {
                    cmdPrintDetail_Checked.Enabled = false;
                    cmsPrintHistory_Checked.Enabled = false;
                    cmdPrintDetail_CheckAll.Enabled = false;
                    cmdPrintDetail_UnCheckAll.Enabled = false;
                }
            }

            if ((int)gridView1.GetDataRow(gridView1.FocusedRowHandle)["PrintBy"] != DAccess.Instance.UserId &&
                !DAccess.Instance.UserIsAdmin)
            {
                SetEnabledForDetailContextMenu(false);
            }

            if ((bool)gridViewPrintHistoryDetail.GetDataRow(gridViewPrintHistoryDetail.FocusedRowHandle)["Checked"] != true)
                cmdPrintDetail_Checked.Text = "Установить статус Проверено";
            else
                cmdPrintDetail_Checked.Text = "Снять статус Проверки";
        }

        /// <summary>
        /// Set enabled property for context menu - cmdPrintDetail
        /// </summary>
        /// <param name="enabled"></param>
        private void SetEnabledForDetailContextMenu(bool enabled)
        {
            cmdPrintDetail_Checked.Enabled = enabled;
            cmdPrintDetail_CheckAll.Enabled = enabled;
            cmdPrintDetail_UnCheckAll.Enabled = enabled;
        }

        private void cmsPrintDetail_Checked_Click(object sender, EventArgs e)
        {
            gridViewPrintHistoryDetail.GetDataRow(gridViewPrintHistoryDetail.FocusedRowHandle)["Checked"] = !(bool)gridViewPrintHistoryDetail.GetDataRow(gridViewPrintHistoryDetail.FocusedRowHandle)["Checked"];
            SavePrintHistoryDetail();
        }

        private void txtComment_Leave(object sender, EventArgs e)
        {
            gridView1.GetDataRow(gridView1.FocusedRowHandle)["Comment"] = txtComment.Text;
            SavePrintHistory();
        }

        private void cmsPrintHistory_Filter_CurrentDeriveCompanyAndBillFilter_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
                return;

            DataRow row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
            if (row["ID_Bill_Kind"] != DBNull.Value && row["DeriveCompany"] != DBNull.Value)
            {
                gridView1.ActiveFilter.Clear();

                gridView1.ActiveFilter.Add(
                    colID_Bill_Kind,
                    new ColumnFilterInfo("[ID_Bill_Kind] = " + row["ID_Bill_Kind"].ToString()));

                gridView1.ActiveFilter.Add(
                    colDeriveCompany,
                    new ColumnFilterInfo("[DeriveCompany] = '" + row["DeriveCompany"].ToString() + "'"));

                gridView1.ApplyColumnsFilter();
            }
        }

        private void cmsPrintHistory_Filter_CurrentDeriveCompany_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
                return;

            DataRow row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
            if (row["DeriveCompany"] != DBNull.Value)
            {
                gridView1.ActiveFilter.Clear();

                gridView1.ActiveFilter.Add(
                    colDeriveCompany,
                    new ColumnFilterInfo("[DeriveCompany] = '" + row["DeriveCompany"].ToString() + "'"));

                gridView1.ApplyColumnsFilter();
            }
        }

        private void cmsPrintHistory_Filter_CurrentBillKind_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
                return;

            DataRow row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
            if (row["ID_Bill_Kind"] != DBNull.Value)
            {
                gridView1.ActiveFilter.Clear();

                gridView1.ActiveFilter.Add(
                    colID_Bill_Kind,
                    new ColumnFilterInfo("[ID_Bill_Kind] = " + row["ID_Bill_Kind"].ToString()));

                gridView1.ApplyColumnsFilter();
            }
        }

        private void cmsPrintHistory_FilterClear_Click(object sender, EventArgs e)
        {
            gridView1.ActiveFilter.Clear();
        }

        private void lblButtonFind_Click(object sender, EventArgs e)
        {
            FilterDataByDeriveCompany();
        }

        private void txtDeriveCompany_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) FilterDataByDeriveCompany();
        }

        private void FilterDataByDeriveCompany()
        {
            gridView1.ActiveFilter.Clear();
            if (txtDeriveCompany.Text.Length > 1)
            {
                gridView1.Columns["DeriveCompany"].ClearFilter();
                gridView1.ActiveFilter.Add(new ViewColumnFilterInfo(gridView1.Columns["DeriveCompany"], new DevExpress.XtraGrid.Columns.ColumnFilterInfo("[DeriveCompany] like '%" + txtDeriveCompany.Text + "%'")));
            }
            gridView1.ApplyColumnsFilter();
        }

        private void показатьИсториюИзмененийToolStripMenuItem_Click(object sender, EventArgs e)
        {   
            if (gridView1.FocusedRowHandle < 0)
                return;
            DataRow row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
            FrmPrintHistoryChanges Phc = new FrmPrintHistoryChanges((long)row["ID_Print"]);
            Phc.ShowDialog();
        }

        private void cbIssueDelay_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIssueDelay.Checked)
            {
                gridView1.ActiveFilter.Clear();

                gridView1.ActiveFilter.Add(
                    colDeriveCompany,
                    new ColumnFilterInfo("[Issued_Date] is null"));
                //new ColumnFilterInfo("[IssuedDelay] > 1"));
                
                gridView1.ApplyColumnsFilter();
            }
            else
            {
                gridView1.ActiveFilter.Clear();
            }
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;

            // if ((int)gridView1.GetDataRow(e.RowHandle)["IssuedDelay"] > 1)
            if (gridView1.GetDataRow(e.RowHandle)["Issued_Date"] == DBNull.Value)
            {
                e.Appearance.BackColor = Color.Red;
            }
        }
    }
}
