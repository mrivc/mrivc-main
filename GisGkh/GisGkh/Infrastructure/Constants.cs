﻿namespace GisGkh.Infrastructure
{
    public static class Constants
    {
        public const string GisServicesConfigSectionName = "GisServicesConfig";
        public const string XadesConfigSectionName = "signingConfig";

        public const string SignElementName = "sign-element";
        public const string SoapContentXpath = "soap:Envelope/soap:Body/*";
        public const string SoapBodyXpath = "soap:Envelope/soap:Body";
        public const string SoapHeaderXpath = "soap:Envelope/soap:Header";
        //public const string SoapContentXpathNew = "ns2:Envelope/ns2:Body/*"; --BDA 29-01-18 Заминили на env
        public const string SoapContentXpathNew = "env:Envelope/env:Body/*";
        public const string SoapBodyXpathNew = "ns2:Envelope/ns2:Body";
        public const string SoapHeaderXpathNew = "ns2:Envelope/ns2:Header";
        public const string SignatureName = "Signature";

        public const string GetStateMethodName = "getState";
        public const string MessageGuidXpath = "./m:getStateRequest/m:MessageGUID";
    }
}
