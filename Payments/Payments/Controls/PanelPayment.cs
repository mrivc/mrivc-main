﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Payments.DS;

namespace Payments.Controls
{
    public partial class PanelPayment : UserControl
    {
        /// <summary>
        /// Событие срабатывает при нажатии на кнопку печати чека (Оплатить)
        /// Возвращет признак, успешно ли прошел платеж
        /// </summary>
        /// <param name="sender">Текущий объект панели</param>
        /// <param name="e">EventArgs</param>
        public delegate bool PayEventHandler(PanelPayment sender, EventArgs e);
        public event PayEventHandler PayEvent;

        public delegate void CommentFieldEventHandler(PanelPayment sender, EventArgs e);
        public event CommentFieldEventHandler CommentFieldLostFocusEvent;

        public delegate void PaymentSummEventHandler(PanelPayment sender, EventArgs e);
        public event PaymentSummEventHandler PaymentSummChangedEvent;

        private bool allowPayment = true;
        private decimal commissionRate = 0;
        private decimal commissionMinSumm = 0;

        public PanelPayment()
        {
            InitializeComponent();
            CashType = 1;
        }

        public void SetCommissionRate(decimal commissionRate, decimal commissionMinSumm)
        {
            this.commissionRate = commissionRate;
            this.commissionMinSumm = commissionMinSumm;
            lblCommissionPersent.Text = commissionRate.ToString() + "% =";
        }

        private void PanelPayment_Load(object sender, EventArgs e)
        {
            txtPaymentDate.EditValue = DateTime.Now;
        }

        public void SetDefaultFocus()
        {
            txtSumm.Focus();
        }

        public DateTime PaymentDate {
            get {
                if (txtPaymentDate.EditValue == null)
                    return DateTime.Now;
                return DateTime.Parse(txtPaymentDate.EditValue.ToString());
            }
            set {
                txtPaymentDate.EditValue = value;
            }
        }

        public decimal PaymentCash {
            get {
                return txtCash.Value;
            }
            set {
                txtCash.Value = value;
                RefreshCharge();
            }
        }

        public int CashType
        {
            get; set;
        }

        public void DisableSummField(bool disable = true)
        {
            txtSumm.Enabled = !disable;
        }

        public decimal PaymentSumm
        {
            get
            {
                return txtSumm.Value;
            }
            set
            {
                txtSumm.Value = value;
                CalculateTotalAndChargeSumm(value);
                //RefreshCharge();
            }
        }

        public string PhoneEmail
        {
            get
            {
                return txtPhone.Text.Trim().Replace(" ", string.Empty);
            }
            set
            {
                txtPhone.Text = value;
            }
        }

        public decimal PaymentSummCommission
        {
            get
            {
                return txtSummCommission.Value;
            }
        }

        public decimal PaymentSummTotal
        {
            get
            {
                return PaymentSummCommission + PaymentSumm;
            }
        }

        public decimal PaymentCharge
        {
            get
            {
                return decimal.Parse(txtCharge.EditValue.ToString());
            }
        }

        private void RefreshCharge()
        {
            txtCharge.Text = (PaymentCash - PaymentSumm).ToString();
        }

        public string PaymentComment
        {
            get
            {
                return txtComment.Text;
            }
            set
            {
                txtComment.Text = value;
            }
        }

        /// <summary>
        /// Возможноть оплаты. Если False, то запрещаем оплату. 
        /// </summary>
        /// <param name="allowPayment">Если False, то запрещаем оплату, иначе True.</param>
        public void SetAllowPayment(bool allowPayment)
        {
            this.allowPayment = allowPayment;
            btnMakePay.Enabled = allowPayment;
            btnMakePayVisa.Enabled = allowPayment;
        }

        private bool CheckPay()
        {
            if (!allowPayment)
                return false;

            if (!StringHelper.IsValidEmail(PhoneEmail) && PhoneEmail.Length > 0)
            {
                MessageBox.Show("E-mail адрес указан не правильно. Пожалуйста введите правильный e-mail адрес, либо оставьте поле пустым.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (PaymentCash < PaymentSumm)
            {
                MessageBox.Show("Сумма внесенных наличных должна быть больше суммы платежа.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (PaymentSumm == 0)
            {
                MessageBox.Show("Сумма платежа должна быть больше нуля.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (PaymentSumm > DAccess.Instance.ProgramSettings.MAX_SUM_PAY)
            {
                MessageBox.Show("Сумма платежа превышает максимально допустимую сумму в " + DAccess.Instance.ProgramSettings.MAX_SUM_PAY.ToString() + "руб.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        public void DoPay(EventArgs e)
        {
            if (!CheckPay())
                return;

            //время платежа должно практически совпадать со временем платежа по чеку
            PaymentDate = DateTime.Now;
            //тип оплаты - наличка
            this.CashType = 1;

            if (PayEvent != null)
            {
                if (PayEvent(this, e))
                {
                    if (Properties.Settings.Default.OpenDrawerAfterPayment)
                        DAccess.CashTerminal.OpenDrawer();
                }
            }
        }

        private void btnMakePay_Click(object sender, EventArgs e)
        {
            DoPay(e);
        }

        private void btnMakePayVisa_Click(object sender, EventArgs e)
        {
            if (!CheckPay())
                return;

            //тип оплаты - кредитная карта
            this.CashType = 2;

            if (PayEvent != null)
            {
                PayEvent(this, e);
            }
        }

        private void txtCash_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            txtCharge.Text = (decimal.Parse(e.NewValue.ToString()) - txtSummTotal.Value).ToString();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.GetNextControl(this.ActiveControl, true).Focus();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void CalculateTotalAndChargeSumm(decimal paymentSumm)
        {
            if (commissionRate > 0)
            {
                txtSummCommission.Value = (paymentSumm * commissionRate) / 100;
                if (txtSummCommission.Value < commissionMinSumm)
                    txtSummCommission.Value = commissionMinSumm;
            }

            txtSummTotal.Value = PaymentSummCommission + paymentSumm;

            PaymentCash = txtSummTotal.Value;//decimal.Parse(e.NewValue.ToString());
            txtCharge.Text = (PaymentCash - txtSummTotal.Value).ToString();
        }

        private void txtSumm_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            CalculateTotalAndChargeSumm(decimal.Parse(e.NewValue.ToString()));

            if (PaymentSummChangedEvent != null)
                PaymentSummChangedEvent(this, e);
        }

        private void txtComment_Leave(object sender, EventArgs e)
        {
            if (CommentFieldLostFocusEvent != null)
                CommentFieldLostFocusEvent(this, e);
        }

        private void txtPhone_Leave(object sender, EventArgs e)
        {
            if (!StringHelper.IsValidEmail(PhoneEmail) && PhoneEmail.Length > 0)
            {
                MessageBox.Show("E-mail адрес указан не правильно. Пожалуйста введите правильный e-mail адрес, либо оставьте поле пустым.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
