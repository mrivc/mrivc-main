﻿using System.Data;

namespace XadesDemo
{
    public interface ISingleHouseCommand
    {
        string FiasHouseGuid { get; set; }
        void Execute();
        DataSet PrepareToDataSet();
        string SenderID { get; set; }
    }
}
