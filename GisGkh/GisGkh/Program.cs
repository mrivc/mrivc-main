﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GisGkh;
using Helper.Data;
using System.Data.SqlClient;
using Helper;
using System.Runtime.InteropServices;
using static GisGkh.MrivcStatic;

namespace GisGkh
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            DAccess.DataModule.Connection1 = new SqlConnection(MrivcData.ConnectionString);
            
            if (args.Length > 0)
            {
                ////MrivcStatic.Check = true;
                ////MrivcStatic.Await = true;
                ////var startPeriod = -1;
                ////var endPeriod = -1;
                ////MrivcStatic.RunConsole(args, null, startPeriod, endPeriod, null);
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
        }
    }
}
