﻿namespace Payments.Controls
{
    partial class GridPaymentEllisReestr
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridPaymentEllis = new Payments.BaseControls.BaseGrid();
            this.vPaymentEllisReestFullBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.paymentsDataSet = new Payments.PaymentsDataSet();
            this.gridViewPaymentEllisReestr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Payment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCLIENT_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCLIENT_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colADRES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAUTHCOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKVITID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDATPLAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSUMPLAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPU1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOK1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPU2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOK2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPU3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOK3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPU4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOK4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCOMMENT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID_Loaded_Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblPaymentPeriodEnd = new System.Windows.Forms.Label();
            this.lblPaymentPeriodBegin = new System.Windows.Forms.Label();
            this.deEndDate = new DevExpress.XtraEditors.DateEdit();
            this.deBeginDate = new DevExpress.XtraEditors.DateEdit();
            this.btnCreateDbfReestr = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridPaymentEllis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPaymentEllisReestFullBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaymentEllisReestr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridPaymentEllis
            // 
            this.gridPaymentEllis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPaymentEllis.DataSource = this.vPaymentEllisReestFullBindingSource;
            this.gridPaymentEllis.Location = new System.Drawing.Point(0, 51);
            this.gridPaymentEllis.MainView = this.gridViewPaymentEllisReestr;
            this.gridPaymentEllis.Name = "gridPaymentEllis";
            this.gridPaymentEllis.Size = new System.Drawing.Size(689, 389);
            this.gridPaymentEllis.TabIndex = 0;
            this.gridPaymentEllis.TrimCellValueBeforeValidate = true;
            this.gridPaymentEllis.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPaymentEllisReestr});
            // 
            // vPaymentEllisReestFullBindingSource
            // 
            this.vPaymentEllisReestFullBindingSource.DataMember = "VPaymentEllisReestFull";
            this.vPaymentEllisReestFullBindingSource.DataSource = this.paymentsDataSet;
            // 
            // paymentsDataSet
            // 
            this.paymentsDataSet.DataSetName = "PaymentsDataSet";
            this.paymentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewPaymentEllisReestr
            // 
            this.gridViewPaymentEllisReestr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Payment,
            this.colCLIENT_NAME,
            this.colCLIENT_CODE,
            this.colLS,
            this.colADRES,
            this.colUK,
            this.colFAM,
            this.colIM,
            this.colOT,
            this.colAUTHCOD,
            this.colKVITID,
            this.colDATPLAT,
            this.colSUMPLAT,
            this.colPU1,
            this.colPOK1,
            this.colPU2,
            this.colPOK2,
            this.colPU3,
            this.colPOK3,
            this.colPU4,
            this.colPOK4,
            this.colCOMMENT,
            this.colFIO,
            this.colID_Loaded_Status});
            this.gridViewPaymentEllisReestr.GridControl = this.gridPaymentEllis;
            this.gridViewPaymentEllisReestr.GroupCount = 1;
            this.gridViewPaymentEllisReestr.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "ID_Payment", null, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SUMPLAT", null, "")});
            this.gridViewPaymentEllisReestr.Name = "gridViewPaymentEllisReestr";
            this.gridViewPaymentEllisReestr.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPaymentEllisReestr.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPaymentEllisReestr.OptionsBehavior.Editable = false;
            this.gridViewPaymentEllisReestr.OptionsView.ShowFooter = true;
            this.gridViewPaymentEllisReestr.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCLIENT_NAME, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colID_Payment, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewPaymentEllisReestr.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewPaymentEllisReestr_RowStyle);
            // 
            // colID_Payment
            // 
            this.colID_Payment.Caption = "ИД";
            this.colID_Payment.FieldName = "ID_Payment";
            this.colID_Payment.Name = "colID_Payment";
            this.colID_Payment.SummaryItem.DisplayFormat = "{0}";
            this.colID_Payment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.colID_Payment.Visible = true;
            this.colID_Payment.VisibleIndex = 0;
            // 
            // colCLIENT_NAME
            // 
            this.colCLIENT_NAME.Caption = "Расчетный  счет";
            this.colCLIENT_NAME.FieldName = "CLIENT_NAME";
            this.colCLIENT_NAME.Name = "colCLIENT_NAME";
            this.colCLIENT_NAME.Width = 121;
            // 
            // colCLIENT_CODE
            // 
            this.colCLIENT_CODE.Caption = "Код клиента";
            this.colCLIENT_CODE.FieldName = "CLIENT_CODE";
            this.colCLIENT_CODE.Name = "colCLIENT_CODE";
            // 
            // colLS
            // 
            this.colLS.FieldName = "LS";
            this.colLS.Name = "colLS";
            this.colLS.OptionsColumn.ReadOnly = true;
            // 
            // colADRES
            // 
            this.colADRES.FieldName = "ADRES";
            this.colADRES.Name = "colADRES";
            this.colADRES.Visible = true;
            this.colADRES.VisibleIndex = 1;
            this.colADRES.Width = 174;
            // 
            // colUK
            // 
            this.colUK.FieldName = "UK";
            this.colUK.Name = "colUK";
            this.colUK.Visible = true;
            this.colUK.VisibleIndex = 2;
            this.colUK.Width = 87;
            // 
            // colFAM
            // 
            this.colFAM.FieldName = "FAM";
            this.colFAM.Name = "colFAM";
            // 
            // colIM
            // 
            this.colIM.FieldName = "IM";
            this.colIM.Name = "colIM";
            // 
            // colOT
            // 
            this.colOT.FieldName = "OT";
            this.colOT.Name = "colOT";
            // 
            // colAUTHCOD
            // 
            this.colAUTHCOD.FieldName = "AUTHCOD";
            this.colAUTHCOD.Name = "colAUTHCOD";
            this.colAUTHCOD.Visible = true;
            this.colAUTHCOD.VisibleIndex = 3;
            this.colAUTHCOD.Width = 123;
            // 
            // colKVITID
            // 
            this.colKVITID.FieldName = "KVITID";
            this.colKVITID.Name = "colKVITID";
            this.colKVITID.OptionsColumn.ReadOnly = true;
            // 
            // colDATPLAT
            // 
            this.colDATPLAT.FieldName = "DATPLAT";
            this.colDATPLAT.Name = "colDATPLAT";
            this.colDATPLAT.OptionsColumn.ReadOnly = true;
            this.colDATPLAT.Visible = true;
            this.colDATPLAT.VisibleIndex = 4;
            this.colDATPLAT.Width = 102;
            // 
            // colSUMPLAT
            // 
            this.colSUMPLAT.DisplayFormat.FormatString = "c";
            this.colSUMPLAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSUMPLAT.FieldName = "SUMPLAT";
            this.colSUMPLAT.Name = "colSUMPLAT";
            this.colSUMPLAT.SummaryItem.DisplayFormat = "SUM={0:c}";
            this.colSUMPLAT.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colSUMPLAT.Visible = true;
            this.colSUMPLAT.VisibleIndex = 5;
            this.colSUMPLAT.Width = 153;
            // 
            // colPU1
            // 
            this.colPU1.FieldName = "PU1";
            this.colPU1.Name = "colPU1";
            this.colPU1.OptionsColumn.ReadOnly = true;
            // 
            // colPOK1
            // 
            this.colPOK1.FieldName = "POK1";
            this.colPOK1.Name = "colPOK1";
            this.colPOK1.Visible = true;
            this.colPOK1.VisibleIndex = 6;
            this.colPOK1.Width = 83;
            // 
            // colPU2
            // 
            this.colPU2.FieldName = "PU2";
            this.colPU2.Name = "colPU2";
            this.colPU2.OptionsColumn.ReadOnly = true;
            // 
            // colPOK2
            // 
            this.colPOK2.FieldName = "POK2";
            this.colPOK2.Name = "colPOK2";
            this.colPOK2.Visible = true;
            this.colPOK2.VisibleIndex = 7;
            this.colPOK2.Width = 73;
            // 
            // colPU3
            // 
            this.colPU3.FieldName = "PU3";
            this.colPU3.Name = "colPU3";
            this.colPU3.OptionsColumn.ReadOnly = true;
            // 
            // colPOK3
            // 
            this.colPOK3.FieldName = "POK3";
            this.colPOK3.Name = "colPOK3";
            this.colPOK3.Visible = true;
            this.colPOK3.VisibleIndex = 8;
            this.colPOK3.Width = 61;
            // 
            // colPU4
            // 
            this.colPU4.FieldName = "PU4";
            this.colPU4.Name = "colPU4";
            this.colPU4.OptionsColumn.ReadOnly = true;
            // 
            // colPOK4
            // 
            this.colPOK4.FieldName = "POK4";
            this.colPOK4.Name = "colPOK4";
            this.colPOK4.Visible = true;
            this.colPOK4.VisibleIndex = 9;
            this.colPOK4.Width = 66;
            // 
            // colCOMMENT
            // 
            this.colCOMMENT.FieldName = "COMMENT";
            this.colCOMMENT.Name = "colCOMMENT";
            this.colCOMMENT.OptionsColumn.ReadOnly = true;
            // 
            // colFIO
            // 
            this.colFIO.Caption = "Платеж создал";
            this.colFIO.FieldName = "FIO";
            this.colFIO.Name = "colFIO";
            // 
            // colID_Loaded_Status
            // 
            this.colID_Loaded_Status.Caption = "Выгружен в ЭЛЛИС";
            this.colID_Loaded_Status.FieldName = "ID_Loaded_Status";
            this.colID_Loaded_Status.Name = "colID_Loaded_Status";
            // 
            // lblPaymentPeriodEnd
            // 
            this.lblPaymentPeriodEnd.AutoSize = true;
            this.lblPaymentPeriodEnd.Location = new System.Drawing.Point(185, 18);
            this.lblPaymentPeriodEnd.Name = "lblPaymentPeriodEnd";
            this.lblPaymentPeriodEnd.Size = new System.Drawing.Size(19, 13);
            this.lblPaymentPeriodEnd.TabIndex = 12;
            this.lblPaymentPeriodEnd.Text = "по";
            // 
            // lblPaymentPeriodBegin
            // 
            this.lblPaymentPeriodBegin.AutoSize = true;
            this.lblPaymentPeriodBegin.Location = new System.Drawing.Point(12, 18);
            this.lblPaymentPeriodBegin.Name = "lblPaymentPeriodBegin";
            this.lblPaymentPeriodBegin.Size = new System.Drawing.Size(64, 13);
            this.lblPaymentPeriodBegin.TabIndex = 11;
            this.lblPaymentPeriodBegin.Text = "Платежи с:";
            // 
            // deEndDate
            // 
            this.deEndDate.EditValue = null;
            this.deEndDate.Location = new System.Drawing.Point(209, 15);
            this.deEndDate.Name = "deEndDate";
            this.deEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEndDate.Size = new System.Drawing.Size(97, 20);
            this.deEndDate.TabIndex = 10;
            this.deEndDate.EditValueChanged += new System.EventHandler(this.deEndDate_EditValueChanged);
            // 
            // deBeginDate
            // 
            this.deBeginDate.EditValue = null;
            this.deBeginDate.Location = new System.Drawing.Point(80, 15);
            this.deBeginDate.Name = "deBeginDate";
            this.deBeginDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deBeginDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deBeginDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deBeginDate.Size = new System.Drawing.Size(97, 20);
            this.deBeginDate.TabIndex = 9;
            this.deBeginDate.EditValueChanged += new System.EventHandler(this.deBeginDate_EditValueChanged);
            // 
            // btnCreateDbfReestr
            // 
            this.btnCreateDbfReestr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateDbfReestr.Location = new System.Drawing.Point(483, 13);
            this.btnCreateDbfReestr.Name = "btnCreateDbfReestr";
            this.btnCreateDbfReestr.Size = new System.Drawing.Size(195, 23);
            this.btnCreateDbfReestr.TabIndex = 13;
            this.btnCreateDbfReestr.Text = "Создать реестры платежей";
            this.btnCreateDbfReestr.Click += new System.EventHandler(this.btnCreateDbfReestr_Click);
            // 
            // GridPaymentEllisReestr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnCreateDbfReestr);
            this.Controls.Add(this.lblPaymentPeriodEnd);
            this.Controls.Add(this.lblPaymentPeriodBegin);
            this.Controls.Add(this.deEndDate);
            this.Controls.Add(this.deBeginDate);
            this.Controls.Add(this.gridPaymentEllis);
            this.Name = "GridPaymentEllisReestr";
            this.Size = new System.Drawing.Size(689, 440);
            ((System.ComponentModel.ISupportInitialize)(this.gridPaymentEllis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPaymentEllisReestFullBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaymentEllisReestr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBeginDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseControls.BaseGrid gridPaymentEllis;
        private System.Windows.Forms.BindingSource vPaymentEllisReestFullBindingSource;
        private PaymentsDataSet paymentsDataSet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPaymentEllisReestr;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Payment;
        private DevExpress.XtraGrid.Columns.GridColumn colCLIENT_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn colCLIENT_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn colLS;
        private DevExpress.XtraGrid.Columns.GridColumn colADRES;
        private DevExpress.XtraGrid.Columns.GridColumn colUK;
        private DevExpress.XtraGrid.Columns.GridColumn colFAM;
        private DevExpress.XtraGrid.Columns.GridColumn colIM;
        private DevExpress.XtraGrid.Columns.GridColumn colOT;
        private DevExpress.XtraGrid.Columns.GridColumn colAUTHCOD;
        private DevExpress.XtraGrid.Columns.GridColumn colKVITID;
        private DevExpress.XtraGrid.Columns.GridColumn colDATPLAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSUMPLAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPU1;
        private DevExpress.XtraGrid.Columns.GridColumn colPOK1;
        private DevExpress.XtraGrid.Columns.GridColumn colPU2;
        private DevExpress.XtraGrid.Columns.GridColumn colPOK2;
        private DevExpress.XtraGrid.Columns.GridColumn colPU3;
        private DevExpress.XtraGrid.Columns.GridColumn colPOK3;
        private DevExpress.XtraGrid.Columns.GridColumn colPU4;
        private DevExpress.XtraGrid.Columns.GridColumn colPOK4;
        private DevExpress.XtraGrid.Columns.GridColumn colCOMMENT;
        private System.Windows.Forms.Label lblPaymentPeriodEnd;
        private System.Windows.Forms.Label lblPaymentPeriodBegin;
        private DevExpress.XtraEditors.DateEdit deEndDate;
        private DevExpress.XtraEditors.DateEdit deBeginDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFIO;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Loaded_Status;
        private DevExpress.XtraEditors.SimpleButton btnCreateDbfReestr;
    }
}
