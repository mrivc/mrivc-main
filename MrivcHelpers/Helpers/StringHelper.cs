﻿namespace MrivcHelpers.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    public static class StringHelper
    {
        public static string GetSubstring(string str, string startChar, string endChar)
        {
            int _ind1 = str.IndexOf(startChar);
            int _ind2 = str.IndexOf(endChar, _ind1);

            if (_ind1 >= 0 && _ind2 > _ind1)
                return str.Substring(_ind1 + startChar.Length, _ind2 - _ind1 - startChar.Length);
            //return str.Substring(str.IndexOf(startChar)+1, str.LastIndexOf(endChar) - str.IndexOf(startChar)-1);
            else
                return "";
        }

        public static string GetSqlConnectionPart(this string sqlConnection, string part)
        {
            return GetSubstring(sqlConnection, part+"=", ";");
        }

        public static string ReplaceEscape(string str)
        {
            str = str.Replace("'", "''");
            return str;
        }

        public static string ReplaceQuote(this string str)
        {
            str = str.Replace("'", "''").Replace("\"", string.Empty).Replace("\\", string.Empty).Replace("/", string.Empty);
            return str;
        }

        public static string EncodingWinToDos(string value)
        {
            Encoding cp1251 = Encoding.GetEncoding(1251);
            Encoding cp866 = Encoding.GetEncoding(866);
            return cp866.GetString(cp1251.GetBytes(value));
        }

        public static string GetShortTimeDateString()
        {
            return ZeroCode(DateTime.Now.Month) + ZeroCode(DateTime.Now.Day) + ZeroCode(DateTime.Now.Hour) + ZeroCode(DateTime.Now.Minute) + ZeroCode(DateTime.Now.Millisecond);
        }

        public static string ShortDateStringForSQL(this DateTime date)
        {
            return date.Year.ToString() + ZeroCode(date.Month) + ZeroCode(date.Day);
        }

        public static string ZeroCode4(int value)
        {
            if (value < 10)
                return "000" + value.ToString();
            if (value < 100)
                return "00" + value.ToString();
            if (value < 1000)
                return "0" + value.ToString();
            return value.ToString();
        }

        public static string ZeroCode(int value)
        {
            if (value < 10)
                return "0" + value.ToString();
            return value.ToString();
        }

        public static string DayCode(int day)
        {
            if (day < 10)
                return "0" + day.ToString();
            return day.ToString();
        }

        public static bool IsValidEmail(this string emailAddress)
        {
            string[] mailsTo = emailAddress.Split(new char[] { ';', ',' });
            foreach (var mail in mailsTo)
            {
                var mailForCheck = mail.Trim();
                string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                    + @"([-a-z0-9!#$%&'*+=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                    + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

                var exp = new Regex(validEmailPattern, RegexOptions.IgnoreCase);

                if (!exp.IsMatch(mailForCheck) || !mailForCheck.Contains("@") || mailForCheck.Contains("/"))
                    return false;
            }

            return true;
        }

        public static List<string> CreateListFromMultiLineString(string _string)
        {
            List<string> list = new List<string>();

            using (StringReader reader = new StringReader(_string))
            {
                string line = "";
                do
                {
                    line = reader.ReadLine();
                    if (line != null)
                        list.Add(line);
                } while (line != null);
            }

            return list;
        }
    }
}
