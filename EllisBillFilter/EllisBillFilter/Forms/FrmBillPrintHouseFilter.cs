﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EllisBillFilter.DS;

namespace EllisBillFilter
{
    public partial class FrmBillPrintHouseFilter : Form
    {
        int idBillFilter;
        int idClientDetail;
        int period;
        long idPrint;
        string checkedHousesString;
        DataRow clientDetailInfo;
        DataTable dtHouse;

        public FrmBillPrintHouseFilter()
        {
            InitializeComponent();
        }

        public FrmBillPrintHouseFilter(long idPrint, DataRow data, int period)
        {
            InitializeComponent();
            this.idPrint = idPrint;
            this.clientDetailInfo = data;
            this.idClientDetail = (int)data["ID_Client_Detail"];
            this.idBillFilter = (int)data["ID_Bill_Filter"];
            this.period = period;

            lblStatus.Text = "";
        }

        private void FrmBillPrintHouseFilter_Shown(object sender, EventArgs e)
        {
            dtHouse = DAccess.DataModule.HouseBillFilterSelect(idClientDetail, period);
            ((ListBox)clHouseFilter).DataSource = dtHouse;
            ((ListBox)clHouseFilter).ValueMember = "id_house";
            ((ListBox)clHouseFilter).DisplayMember = "house_address";

            lblUK.Text = lblUK.Text + DAccess.Instance.GetDeriveCompanyName((int)clientDetailInfo["ID_Derive_Company"]);
            lblBillFilter.Text = lblBillFilter.Text + DAccess.Instance.GetBillFilterName(idBillFilter);

            CheckAllHouseItems(true);
            
        }

        private void CheckAllHouseItems(bool check)
        {
            for (int i = 0; i <= (clHouseFilter.Items.Count - 1); i++)
                if (check)
                {
                    checkBox1.Checked = true;
                    clHouseFilter.SetItemCheckState(i, CheckState.Checked);
                }
                else
                {
                    checkBox1.Checked = false;
                    clHouseFilter.SetItemCheckState(i, CheckState.Unchecked);
                }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckAllHouseItems(checkBox1.Checked);
        }

        public string CheckedHousesString
        {
            get {
                return checkedHousesString;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            btnPrint.Enabled = false;
            MakeHouseFilter();

            if (CheckedHousesString.Length == 0)
            {
                DialogResult = System.Windows.Forms.DialogResult.Cancel;
                return;
            }

            SetStatusText("подготовка списка лицевый счетов...");
            DAccess.DataModule.PrepareBillReportAccounts(idClientDetail, DAccess.Instance.WorkPeriod, CheckedHousesString);
            SetStatusText("подготовка данных для квитанций...");
            DAccess.DataModule.PrepareBillReportData(idPrint, idBillFilter, DAccess.Instance.WorkPeriod, false, false, -1, 0);

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private string MakeHouseFilter()
        {
            checkedHousesString = "";
            foreach (var item in clHouseFilter.CheckedItems)
            {
                var row = (item as DataRowView).Row;
                checkedHousesString += "," + row["id_house"];
            }
            if (checkedHousesString.Length > 0)
                checkedHousesString = checkedHousesString.Substring(1);
            return checkedHousesString;
        }

        private void SetStatusText(string text)
        {
            lblStatus.Text = text;
            lblStatus.Refresh();
        }
    }
}
