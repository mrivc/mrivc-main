﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastReport.Enums
{
    public enum FPaperSize
    {
        A4 = 3,
        A5 = 4
    }
}
