unit uMyUnit;

interface

uses
  ShellAPI,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  uMessages,
  EllisGKH_TLB,
  uPropertyCollection,
  uProperties,
  ImgList, StdCtrls,
  ADODB,
  uMDIChild,
  uReportUtils;

type
  TfrmMyUnit = class(TMDIChildForm)
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    FComObj: TObject;
    FEllisSrv: IEllisCOMServer;
    FEllisSearch: IEllisCOMSearch;
    FEllisFilter: IEllisCOMFilter;
    FEllisAccountSubService: IEllisAccountSubSystem;


    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMyUnit: TfrmMyUnit;

implementation

{$R *.dfm}

procedure TfrmMyUnit.Button1Click(Sender: TObject);
  var S : String;
begin
  inherited;
  FConnection := TADOConnection(Integer(FOptions[pConnection]));
  FImageList := TCustomImageList(Integer(FOptions[pImageList]));
  Caption := CaptionByDescription(HInstance);
  FComObj := TObject(Integer(FOptions[pEllisCOMServer]));
  Label2.Caption:= FComObj.ClassName;

  SetLength(S, MAX_PATH);
  GetModuleFileName(HInstance, PChar(S), MAX_PATH);
  Label1.Caption := ExtractFileName(S);

  ShellExecute(Handle, 'open', 'c:\Windows\notepad.exe', nil, nil, SW_SHOWNORMAL);
end;

end.
