﻿IF OBJECT_ID('TEMPDB..#tmpLoad') is not null drop table #tmpLoad

create table #tmpLoad (
	LS Varchar(12),
	ADRES Varchar(300),
	PU Varchar(12),
	SUMPLAT Float,
	ND Int,
	ADDR_NT Varchar(300),
	UK Varchar(100),
	AUTHCOD Varchar(24),
	KVITID Varchar(12),
	PU1 Varchar(40),
	POK1 Float,
	PU2 Varchar(40),
	POK2 Float,
	PU3 Varchar(40),
	POK3 Float,
	PU4 Varchar(40),
	POK4 Float,
	DATPLAT DateTime,
	UNO Varchar(20))

IF OBJECT_ID('TEMPDB..#tLoadErr') is not null drop table #tLoadErr
IF OBJECT_ID('TEMPDB..#ErrT') is not null drop table #ErrT

create table #ErrT (
    NPP Int, 
    DATPLAT DateTime,
    ADRES Varchar(150),
    AUTHCOD Varchar(24),
    SUMPLAT Float,
    OP1 Float,
    OP2 Float,
    OP3 Float,
    OP4 Float,
    NP1 Float,
    NP2 Float,
    NP3 Float,
    NP4 Float,
    COMMENT Varchar(300),
    Processed Varchar(10));