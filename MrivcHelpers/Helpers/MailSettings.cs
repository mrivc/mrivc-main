﻿namespace MrivcHelpers.Helpers
{
    public static class MailSettings
    {
        public static string MailTo { get; set; }

        public static string CopyTo { get; set; }

        public static string Subject { get; set; }

        public static string BodyText { get; set; }
    }
}
