﻿namespace EllisBillFilter
{
    partial class FrmPrintHistoryPivotAnalisys
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrintHistoryPivotAnalisys));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.customEdit1 = new MassBillNew.CustomEdit();
            this.pivotPrintHistory1 = new MrivcControls.PivotPrintHistory();
            this.gridPrintHistory1 = new MrivcControls.GridPrintHistory();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1132, 568);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pivotPrintHistory1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1124, 542);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Сводная таблица печати";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridPrintHistory1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1124, 542);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "История печати";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // customEdit1
            // 
            this.customEdit1.EditValue = "";
            this.customEdit1.Location = new System.Drawing.Point(778, 0);
            this.customEdit1.Name = "customEdit1";
            this.customEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.customEdit1.Properties.IncrementalSearch = false;
            this.customEdit1.Size = new System.Drawing.Size(100, 20);
            this.customEdit1.TabIndex = 2;
            // 
            // pivotPrintHistory1
            // 
            this.pivotPrintHistory1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotPrintHistory1.Location = new System.Drawing.Point(3, 3);
            this.pivotPrintHistory1.Name = "pivotPrintHistory1";
            this.pivotPrintHistory1.Size = new System.Drawing.Size(1118, 536);
            this.pivotPrintHistory1.TabIndex = 0;
            this.pivotPrintHistory1.Load += new System.EventHandler(this.pivotPrintHistory1_Load);
            // 
            // gridPrintHistory1
            // 
            this.gridPrintHistory1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPrintHistory1.Location = new System.Drawing.Point(3, 3);
            this.gridPrintHistory1.Name = "gridPrintHistory1";
            this.gridPrintHistory1.Size = new System.Drawing.Size(1118, 536);
            this.gridPrintHistory1.TabIndex = 0;
            // 
            // FrmPrintHistoryPivotAnalisys
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1132, 568);
            this.Controls.Add(this.customEdit1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrintHistoryPivotAnalisys";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Анализ печати v4.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPrintHistoryPivotAnalisys_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.customEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MrivcControls.PivotPrintHistory pivotPrintHistory1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private MrivcControls.GridPrintHistory gridPrintHistory1;
        private MassBillNew.CustomEdit customEdit1;
    }
}