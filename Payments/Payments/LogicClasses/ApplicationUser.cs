﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Payments.LogicClasses
{
    public class ApplicationUser
    {
        private int id_User;
        private string login;
        private string name;
        private string fullName;
        private string position;
        private string managerFio;
        private int fiscal_User_ID;
        private int fiscal_User_Password;
        private bool readOnlyPrivilegy;
        private bool filterPaymentsByUser;
        private bool simulateTerminalMode;
        private DateTime createdDate;

        public ApplicationUser(int id_User, string login, string name, string fullName, string position, string managerFio, int fiscal_User_ID,
            int fiscal_User_Password, bool readOnlyPrivilegy, bool filterPaymentsByUser, bool simulateTerminalMode, DateTime createdDate)
        {
            this.id_User = id_User;
            this.login = login;
            this.name = name;
            this.fullName = fullName;
            this.position = position;
            this.managerFio = managerFio;
            this.fiscal_User_ID = fiscal_User_ID;
            this.fiscal_User_Password = fiscal_User_Password;
            this.readOnlyPrivilegy = readOnlyPrivilegy;
            this.filterPaymentsByUser = filterPaymentsByUser;
            this.simulateTerminalMode = simulateTerminalMode;
            this.createdDate = createdDate;
        }

        public int Id_User
        {
            get { return id_User;  }
        }

        public string Login
        {
            get { return login; }
        }

        public string Name
        {
            get { return name; }
        }

        public string FullName
        {
            get { return fullName; }
        }

        public string Position
        {
            get { return position; }
        }

        public string ManagerFio
        {
            get { return managerFio; }
        }

        public int Fiscal_User_ID
        {
            get { return fiscal_User_ID; }
        }

        public int Fiscal_User_Password
        {
            get { return fiscal_User_Password; }
        }

        public bool ReadOnlyPrivilegy
        {
            get { return readOnlyPrivilegy; }
        }

        public bool FilterPaymentsByUser
        {
            get { return filterPaymentsByUser; }
        }

        /// <summary>
        /// Если значение True, то можно работать с программой будто бы подключена касса. 
        /// Даже если касса подключена и включен данный режим, то платежи будут записываться в БД, 
        /// а в кассовом аппарате регистрироваться не будут. Режим нужен для тестирования работы программы
        /// </summary>
        public bool SimulateTerminalMode
        {
            get { return simulateTerminalMode; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
        }

        /// <summary>
        /// Возвращает Id_User в зависимости от параметра FilterPaymentsByUser
        /// если параметр FilterPaymentsByUser = False, то возвращается -1 
        /// (т.е. платежи не фильтруются по текущему пользователю, а выбираеются все)
        /// </summary>
        public int InvariantUserId
        {
            get {
                if (filterPaymentsByUser)
                    return id_User;
                else
                    return -1;
            }
        }

        public bool HasPriviligy()
        {
            if (ReadOnlyPrivilegy)
            {
                MessageBox.Show("У пользователя не достаточно прав для выполнения данной операции.",
                    "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }
    }
}
