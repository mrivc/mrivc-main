﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop;
using Excel = Microsoft.Office.Interop.Excel;

namespace MrivcHelpers.Helpers
{
    public class ExcelImport
    {
        private List<string> excelSheets;
        private string filePath;

        public ExcelImport(string openFilePath)
        {
            OpenFilePath = openFilePath;
        }

        public string OpenFilePath
        {
            get
            {
                return filePath;
            }
            set
            {
                filePath = value;
                excelSheets = null;
            }
        }

        /// <summary>
        /// Возвращает листы Excel файла
        /// </summary>
        public List<string> ExcelSheets
        {
            get
            {
                if (excelSheets != null)
                    return excelSheets;

                excelSheets = new List<string>();

                string strConn = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={OpenFilePath};Extended Properties='Excel 12.0 Xml;HDR=Yes'";

                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        conn.Open();
                        DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        foreach (DataRow row in schemaTable.Rows)
                        {
                            excelSheets.Add(GetCorrectTableName(row["TABLE_NAME"].ToString()));
                        }
                    }
                }

                return excelSheets;
            }
        }

        /// <summary>
        /// Загрузка Excel файлы в DataTable (использует Microsoft.ACE.OLEDB.12)
        /// </summary>
        /// <param name="sheetName">Если указать пустую строку, то будет загружен первый лист</param>
        /// <param name="headerRowNumber">Количество строк, которые считаются заголовками</param>
        /// <returns></returns>
        public DataTable SaveToDataTable(string sheetName, int headerRowNumber)
        {
            if (headerRowNumber < 1)
                throw new Exception("Количество строк заголовка должно быть больше или равно 1");


            DataTable dt = new DataTable();
            string strConn = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={OpenFilePath};Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                using (OleDbDataAdapter da = new OleDbDataAdapter())
                {
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    if (ExcelSheets.Count > 0)
                        sheetName = string.IsNullOrEmpty(sheetName) ? ExcelSheets[0] : sheetName;

                    da.SelectCommand = new OleDbCommand("select * from [" + sheetName + "$]", conn);
                    da.Fill(dt);
                }
            }

            for (int i = 0; i < headerRowNumber - 1; i++)
            {
                dt.Rows.RemoveAt(0);
            }

            return dt;
        }

        /// <summary>
        /// Загрузка Excel файлы в DataTable (использует Excel.Application, грузит по ячейкам)
        /// </summary>
        /// <param name="sheetName">Если указать пустую строку, то будет загружен первый лист</param>
        /// <returns></returns>
        public DataTable ImportToDataTable(string sheetName)
        {
            System.Data.DataTable sheetData = null;
            using (var wrapper = new Helpers.ExcelWrapper())
            {
                Excel.Application oXL = wrapper.Excel;

                Excel.Workbook oWB = oXL.Workbooks.Open(this.OpenFilePath, 0, true);

                try
                {
                    foreach (Excel.Worksheet oSheet in oWB.Worksheets)
                    {
                        if (!TrimExcelSheetName(oSheet.Name).Contains(TrimExcelSheetName(sheetName)))
                            continue;

                        int nColumns = oSheet.UsedRange.Columns.Count;
                        int nRows = oSheet.UsedRange.Rows.Count;

                        if (nColumns > 0)
                        {
                            sheetData = new DataTable(oSheet.Name);

                            Excel.Range rHeaders = (Excel.Range)oSheet.UsedRange.Rows[1];

                            for (int j = 1; j <= nColumns; j++)
                            {
                                string columnName = this.GetExcelCellValue(oSheet, 1, j);

                                if (string.IsNullOrEmpty(columnName))
                                    continue;
                                else if (sheetData.Columns.Contains(columnName))
                                {
                                    int i = 1;
                                    string c;

                                    do
                                    {
                                        c = columnName + i.ToString();
                                        i += 1;
                                    }
                                    while (sheetData.Columns.Contains(c));

                                    sheetData.Columns.Add(c, typeof(string));
                                }
                                else
                                    sheetData.Columns.Add(columnName, typeof(string));
                            }

                            for (int i = 2; i <= nRows; i++)
                            {
                                DataRow sheetRow = sheetData.NewRow();

                                for (int j = 1; j <= nColumns; j++)
                                {
                                    string columnName = this.GetExcelCellValue(oSheet, 1, j);

                                    Excel.Range oRange = (Excel.Range)oSheet.Cells[i, j];

                                    if (!string.IsNullOrEmpty(columnName))
                                        sheetRow[columnName] = this.GetExcelCellValue(oSheet, i, j);
                            }

                                sheetData.Rows.Add(sheetRow);
                            }
                        }
                    }

                    return sheetData;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    oWB.Close();
                    oWB = null;
                    oXL.Quit();
                    oXL = null;
                    wrapper.Dispose();
                }
            }
        }

        private string GetExcelCellValue(Excel.Worksheet worksheet, int rowIndex, int columnIndex)
        {
            Excel.Range cellRange = (Excel.Range)worksheet.Cells[rowIndex, columnIndex];

            if (cellRange != null && cellRange.Value != null)
            {
                return cellRange.Value.ToString();
            }

            return string.Empty;
        }

        private string GetCorrectTableName(string name)
        {
            return name.Replace("$", string.Empty).Replace("'", string.Empty);
        }

        private string TrimExcelSheetName(string name)
        {
            return name.Replace("$", string.Empty).Replace("'", string.Empty).Replace("#", string.Empty).Replace(".", string.Empty).Replace("!", string.Empty).Replace("@", string.Empty).Replace("?", string.Empty);
        }
    }
}
