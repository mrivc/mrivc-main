﻿namespace Payments
{
    partial class frmPaymentFormControlSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.baseGrid1 = new Payments.BaseControls.BaseGrid();
            this.personIdentificationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.paymentsDataSet = new Payments.PaymentsDataSet();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIS_COMPANY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLASTNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIRSTNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMIDDLENAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colREGNUM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colREGNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colREGDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCOUNTRY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCODERF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDISTRICT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSTREET = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOUSE_NUMBER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOUSE_LITER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFLAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colADDR_REG_SAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colP_COUNTRY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colP_CODERF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colP_DISTRICT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colP_CITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colP_STREET = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colP_HOUSE_NUMBER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colP_HOUSE_LITER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colP_FLAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCTYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCSERIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCNUM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCORG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCBD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCPB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCCOUNTRY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCCITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCCITIZEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSUM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRIZN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCOMMENT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORGNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORGINN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORGADDRESS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORGADDRESSFACT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORGFIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRELATION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDELEGATEFIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRESENTFIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRESIVERFIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMPLOYEE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMPLOYEEDIRECTOR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtLastNameSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personIdentificationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // baseGrid1
            // 
            this.baseGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.baseGrid1.DataSource = this.personIdentificationBindingSource;
            this.baseGrid1.Location = new System.Drawing.Point(0, 38);
            this.baseGrid1.MainView = this.gridView1;
            this.baseGrid1.Name = "baseGrid1";
            this.baseGrid1.Size = new System.Drawing.Size(772, 467);
            this.baseGrid1.TabIndex = 0;
            this.baseGrid1.TrimCellValueBeforeValidate = true;
            this.baseGrid1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // personIdentificationBindingSource
            // 
            this.personIdentificationBindingSource.DataMember = "PersonIdentification";
            this.personIdentificationBindingSource.DataSource = this.paymentsDataSet;
            // 
            // paymentsDataSet
            // 
            this.paymentsDataSet.DataSetName = "PaymentsDataSet";
            this.paymentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIS_COMPANY,
            this.colLASTNAME,
            this.colFIRSTNAME,
            this.colMIDDLENAME,
            this.colINN,
            this.colREGNUM,
            this.colREGNAME,
            this.colREGDATE,
            this.colCOUNTRY,
            this.colCODERF,
            this.colDISTRICT,
            this.colCITY,
            this.colSTREET,
            this.colHOUSE_NUMBER,
            this.colHOUSE_LITER,
            this.colFLAT,
            this.colADDR_REG_SAME,
            this.colID,
            this.colP_COUNTRY,
            this.colP_CODERF,
            this.colP_DISTRICT,
            this.colP_CITY,
            this.colP_STREET,
            this.colP_HOUSE_NUMBER,
            this.colP_HOUSE_LITER,
            this.colP_FLAT,
            this.colDOCTYPE,
            this.colDOCSERIA,
            this.colDOCNUM,
            this.colDOCDATE,
            this.colDOCORG,
            this.colDOCBD,
            this.colDOCPB,
            this.colDOCCOUNTRY,
            this.colDOCCITY,
            this.colDOCCITIZEN,
            this.colSUM,
            this.colOSN,
            this.colPRIZN,
            this.colCOMMENT,
            this.colORGNAME,
            this.colORGINN,
            this.colORGADDRESS,
            this.colORGADDRESSFACT,
            this.colORGFIO,
            this.colRELATION,
            this.colDELEGATEFIO,
            this.colPRESENTFIO,
            this.colRESIVERFIO,
            this.colFIO,
            this.colEMPLOYEE,
            this.colEMPLOYEEDIRECTOR,
            this.colCreatedBy,
            this.colCreationDate});
            this.gridView1.GridControl = this.baseGrid1;
            this.gridView1.Name = "gridView1";
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // colIS_COMPANY
            // 
            this.colIS_COMPANY.Caption = "Организация";
            this.colIS_COMPANY.FieldName = "IS_COMPANY";
            this.colIS_COMPANY.Name = "colIS_COMPANY";
            this.colIS_COMPANY.Width = 113;
            // 
            // colLASTNAME
            // 
            this.colLASTNAME.Caption = "Фамилия";
            this.colLASTNAME.FieldName = "LASTNAME";
            this.colLASTNAME.Name = "colLASTNAME";
            this.colLASTNAME.Visible = true;
            this.colLASTNAME.VisibleIndex = 0;
            this.colLASTNAME.Width = 97;
            // 
            // colFIRSTNAME
            // 
            this.colFIRSTNAME.Caption = "Имя";
            this.colFIRSTNAME.FieldName = "FIRSTNAME";
            this.colFIRSTNAME.Name = "colFIRSTNAME";
            this.colFIRSTNAME.Visible = true;
            this.colFIRSTNAME.VisibleIndex = 1;
            this.colFIRSTNAME.Width = 97;
            // 
            // colMIDDLENAME
            // 
            this.colMIDDLENAME.Caption = "Отчество";
            this.colMIDDLENAME.FieldName = "MIDDLENAME";
            this.colMIDDLENAME.Name = "colMIDDLENAME";
            this.colMIDDLENAME.Visible = true;
            this.colMIDDLENAME.VisibleIndex = 2;
            this.colMIDDLENAME.Width = 97;
            // 
            // colINN
            // 
            this.colINN.FieldName = "INN";
            this.colINN.Name = "colINN";
            // 
            // colREGNUM
            // 
            this.colREGNUM.FieldName = "REGNUM";
            this.colREGNUM.Name = "colREGNUM";
            // 
            // colREGNAME
            // 
            this.colREGNAME.FieldName = "REGNAME";
            this.colREGNAME.Name = "colREGNAME";
            // 
            // colREGDATE
            // 
            this.colREGDATE.FieldName = "REGDATE";
            this.colREGDATE.Name = "colREGDATE";
            // 
            // colCOUNTRY
            // 
            this.colCOUNTRY.FieldName = "COUNTRY";
            this.colCOUNTRY.Name = "colCOUNTRY";
            // 
            // colCODERF
            // 
            this.colCODERF.FieldName = "CODERF";
            this.colCODERF.Name = "colCODERF";
            // 
            // colDISTRICT
            // 
            this.colDISTRICT.FieldName = "DISTRICT";
            this.colDISTRICT.Name = "colDISTRICT";
            // 
            // colCITY
            // 
            this.colCITY.FieldName = "CITY";
            this.colCITY.Name = "colCITY";
            // 
            // colSTREET
            // 
            this.colSTREET.FieldName = "STREET";
            this.colSTREET.Name = "colSTREET";
            // 
            // colHOUSE_NUMBER
            // 
            this.colHOUSE_NUMBER.FieldName = "HOUSE_NUMBER";
            this.colHOUSE_NUMBER.Name = "colHOUSE_NUMBER";
            // 
            // colHOUSE_LITER
            // 
            this.colHOUSE_LITER.FieldName = "HOUSE_LITER";
            this.colHOUSE_LITER.Name = "colHOUSE_LITER";
            // 
            // colFLAT
            // 
            this.colFLAT.FieldName = "FLAT";
            this.colFLAT.Name = "colFLAT";
            // 
            // colADDR_REG_SAME
            // 
            this.colADDR_REG_SAME.FieldName = "ADDR_REG_SAME";
            this.colADDR_REG_SAME.Name = "colADDR_REG_SAME";
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            // 
            // colP_COUNTRY
            // 
            this.colP_COUNTRY.FieldName = "P_COUNTRY";
            this.colP_COUNTRY.Name = "colP_COUNTRY";
            // 
            // colP_CODERF
            // 
            this.colP_CODERF.FieldName = "P_CODERF";
            this.colP_CODERF.Name = "colP_CODERF";
            // 
            // colP_DISTRICT
            // 
            this.colP_DISTRICT.FieldName = "P_DISTRICT";
            this.colP_DISTRICT.Name = "colP_DISTRICT";
            // 
            // colP_CITY
            // 
            this.colP_CITY.FieldName = "P_CITY";
            this.colP_CITY.Name = "colP_CITY";
            // 
            // colP_STREET
            // 
            this.colP_STREET.FieldName = "P_STREET";
            this.colP_STREET.Name = "colP_STREET";
            // 
            // colP_HOUSE_NUMBER
            // 
            this.colP_HOUSE_NUMBER.FieldName = "P_HOUSE_NUMBER";
            this.colP_HOUSE_NUMBER.Name = "colP_HOUSE_NUMBER";
            // 
            // colP_HOUSE_LITER
            // 
            this.colP_HOUSE_LITER.FieldName = "P_HOUSE_LITER";
            this.colP_HOUSE_LITER.Name = "colP_HOUSE_LITER";
            // 
            // colP_FLAT
            // 
            this.colP_FLAT.FieldName = "P_FLAT";
            this.colP_FLAT.Name = "colP_FLAT";
            // 
            // colDOCTYPE
            // 
            this.colDOCTYPE.Caption = "Документ";
            this.colDOCTYPE.FieldName = "DOCTYPE";
            this.colDOCTYPE.Name = "colDOCTYPE";
            this.colDOCTYPE.Visible = true;
            this.colDOCTYPE.VisibleIndex = 4;
            this.colDOCTYPE.Width = 89;
            // 
            // colDOCSERIA
            // 
            this.colDOCSERIA.Caption = "Номер";
            this.colDOCSERIA.FieldName = "DOCSERIA";
            this.colDOCSERIA.Name = "colDOCSERIA";
            this.colDOCSERIA.Visible = true;
            this.colDOCSERIA.VisibleIndex = 6;
            this.colDOCSERIA.Width = 89;
            // 
            // colDOCNUM
            // 
            this.colDOCNUM.Caption = "Серия";
            this.colDOCNUM.FieldName = "DOCNUM";
            this.colDOCNUM.Name = "colDOCNUM";
            this.colDOCNUM.Visible = true;
            this.colDOCNUM.VisibleIndex = 5;
            this.colDOCNUM.Width = 89;
            // 
            // colDOCDATE
            // 
            this.colDOCDATE.FieldName = "DOCDATE";
            this.colDOCDATE.Name = "colDOCDATE";
            // 
            // colDOCORG
            // 
            this.colDOCORG.FieldName = "DOCORG";
            this.colDOCORG.Name = "colDOCORG";
            // 
            // colDOCBD
            // 
            this.colDOCBD.Caption = "Дата рождения";
            this.colDOCBD.FieldName = "DOCBD";
            this.colDOCBD.Name = "colDOCBD";
            this.colDOCBD.Visible = true;
            this.colDOCBD.VisibleIndex = 3;
            this.colDOCBD.Width = 140;
            // 
            // colDOCPB
            // 
            this.colDOCPB.FieldName = "DOCPB";
            this.colDOCPB.Name = "colDOCPB";
            // 
            // colDOCCOUNTRY
            // 
            this.colDOCCOUNTRY.FieldName = "DOCCOUNTRY";
            this.colDOCCOUNTRY.Name = "colDOCCOUNTRY";
            // 
            // colDOCCITY
            // 
            this.colDOCCITY.FieldName = "DOCCITY";
            this.colDOCCITY.Name = "colDOCCITY";
            // 
            // colDOCCITIZEN
            // 
            this.colDOCCITIZEN.FieldName = "DOCCITIZEN";
            this.colDOCCITIZEN.Name = "colDOCCITIZEN";
            // 
            // colSUM
            // 
            this.colSUM.FieldName = "SUM";
            this.colSUM.Name = "colSUM";
            // 
            // colOSN
            // 
            this.colOSN.FieldName = "OSN";
            this.colOSN.Name = "colOSN";
            // 
            // colPRIZN
            // 
            this.colPRIZN.FieldName = "PRIZN";
            this.colPRIZN.Name = "colPRIZN";
            // 
            // colCOMMENT
            // 
            this.colCOMMENT.FieldName = "COMMENT";
            this.colCOMMENT.Name = "colCOMMENT";
            // 
            // colORGNAME
            // 
            this.colORGNAME.Caption = "Организация";
            this.colORGNAME.FieldName = "ORGNAME";
            this.colORGNAME.Name = "colORGNAME";
            this.colORGNAME.Width = 89;
            // 
            // colORGINN
            // 
            this.colORGINN.Caption = "ИНН организации";
            this.colORGINN.FieldName = "ORGINN";
            this.colORGINN.Name = "colORGINN";
            this.colORGINN.Width = 101;
            // 
            // colORGADDRESS
            // 
            this.colORGADDRESS.Caption = "Адрес организации";
            this.colORGADDRESS.FieldName = "ORGADDRESS";
            this.colORGADDRESS.Name = "colORGADDRESS";
            this.colORGADDRESS.Width = 89;
            // 
            // colORGADDRESSFACT
            // 
            this.colORGADDRESSFACT.FieldName = "ORGADDRESSFACT";
            this.colORGADDRESSFACT.Name = "colORGADDRESSFACT";
            // 
            // colORGFIO
            // 
            this.colORGFIO.FieldName = "ORGFIO";
            this.colORGFIO.Name = "colORGFIO";
            // 
            // colRELATION
            // 
            this.colRELATION.FieldName = "RELATION";
            this.colRELATION.Name = "colRELATION";
            // 
            // colDELEGATEFIO
            // 
            this.colDELEGATEFIO.FieldName = "DELEGATEFIO";
            this.colDELEGATEFIO.Name = "colDELEGATEFIO";
            // 
            // colPRESENTFIO
            // 
            this.colPRESENTFIO.FieldName = "PRESENTFIO";
            this.colPRESENTFIO.Name = "colPRESENTFIO";
            // 
            // colRESIVERFIO
            // 
            this.colRESIVERFIO.FieldName = "RESIVERFIO";
            this.colRESIVERFIO.Name = "colRESIVERFIO";
            // 
            // colFIO
            // 
            this.colFIO.FieldName = "FIO";
            this.colFIO.Name = "colFIO";
            // 
            // colEMPLOYEE
            // 
            this.colEMPLOYEE.FieldName = "EMPLOYEE";
            this.colEMPLOYEE.Name = "colEMPLOYEE";
            // 
            // colEMPLOYEEDIRECTOR
            // 
            this.colEMPLOYEEDIRECTOR.FieldName = "EMPLOYEEDIRECTOR";
            this.colEMPLOYEEDIRECTOR.Name = "colEMPLOYEEDIRECTOR";
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            // 
            // colCreationDate
            // 
            this.colCreationDate.FieldName = "CreationDate";
            this.colCreationDate.Name = "colCreationDate";
            // 
            // txtLastNameSearch
            // 
            this.txtLastNameSearch.Location = new System.Drawing.Point(124, 12);
            this.txtLastNameSearch.Name = "txtLastNameSearch";
            this.txtLastNameSearch.Size = new System.Drawing.Size(190, 20);
            this.txtLastNameSearch.TabIndex = 1;
            this.txtLastNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLastNameSearch_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Поиск по фамилии:";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(320, 9);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "поиск...";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // frmPaymentFormControlSelect
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(772, 505);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtLastNameSearch);
            this.Controls.Add(this.baseGrid1);
            this.Name = "frmPaymentFormControlSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPaymentFormControlSelect";
            this.Load += new System.EventHandler(this.frmPaymentFormControlSelect_Load);
            ((System.ComponentModel.ISupportInitialize)(this.baseGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personIdentificationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseControls.BaseGrid baseGrid1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource personIdentificationBindingSource;
        private PaymentsDataSet paymentsDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colIS_COMPANY;
        private DevExpress.XtraGrid.Columns.GridColumn colLASTNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colFIRSTNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colMIDDLENAME;
        private DevExpress.XtraGrid.Columns.GridColumn colINN;
        private DevExpress.XtraGrid.Columns.GridColumn colREGNUM;
        private DevExpress.XtraGrid.Columns.GridColumn colREGNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colREGDATE;
        private DevExpress.XtraGrid.Columns.GridColumn colCOUNTRY;
        private DevExpress.XtraGrid.Columns.GridColumn colCODERF;
        private DevExpress.XtraGrid.Columns.GridColumn colDISTRICT;
        private DevExpress.XtraGrid.Columns.GridColumn colCITY;
        private DevExpress.XtraGrid.Columns.GridColumn colSTREET;
        private DevExpress.XtraGrid.Columns.GridColumn colHOUSE_NUMBER;
        private DevExpress.XtraGrid.Columns.GridColumn colHOUSE_LITER;
        private DevExpress.XtraGrid.Columns.GridColumn colFLAT;
        private DevExpress.XtraGrid.Columns.GridColumn colADDR_REG_SAME;
        private DevExpress.XtraGrid.Columns.GridColumn colP_COUNTRY;
        private DevExpress.XtraGrid.Columns.GridColumn colP_CODERF;
        private DevExpress.XtraGrid.Columns.GridColumn colP_DISTRICT;
        private DevExpress.XtraGrid.Columns.GridColumn colP_CITY;
        private DevExpress.XtraGrid.Columns.GridColumn colP_STREET;
        private DevExpress.XtraGrid.Columns.GridColumn colP_HOUSE_NUMBER;
        private DevExpress.XtraGrid.Columns.GridColumn colP_HOUSE_LITER;
        private DevExpress.XtraGrid.Columns.GridColumn colP_FLAT;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCTYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCSERIA;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCNUM;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCDATE;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCORG;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCBD;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCPB;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCCOUNTRY;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCCITY;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCCITIZEN;
        private DevExpress.XtraGrid.Columns.GridColumn colSUM;
        private DevExpress.XtraGrid.Columns.GridColumn colOSN;
        private DevExpress.XtraGrid.Columns.GridColumn colPRIZN;
        private DevExpress.XtraGrid.Columns.GridColumn colCOMMENT;
        private DevExpress.XtraGrid.Columns.GridColumn colORGNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colORGINN;
        private DevExpress.XtraGrid.Columns.GridColumn colORGADDRESS;
        private DevExpress.XtraGrid.Columns.GridColumn colORGADDRESSFACT;
        private DevExpress.XtraGrid.Columns.GridColumn colORGFIO;
        private DevExpress.XtraGrid.Columns.GridColumn colRELATION;
        private DevExpress.XtraGrid.Columns.GridColumn colDELEGATEFIO;
        private DevExpress.XtraGrid.Columns.GridColumn colPRESENTFIO;
        private DevExpress.XtraGrid.Columns.GridColumn colRESIVERFIO;
        private DevExpress.XtraGrid.Columns.GridColumn colFIO;
        private DevExpress.XtraGrid.Columns.GridColumn colEMPLOYEE;
        private DevExpress.XtraGrid.Columns.GridColumn colEMPLOYEEDIRECTOR;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colCreationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private System.Windows.Forms.TextBox txtLastNameSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
    }
}