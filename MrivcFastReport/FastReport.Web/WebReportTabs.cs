﻿using System;
using System.Collections.Generic;
using System.Text;
using FastReport;

namespace FastReport.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class ReportTab
    {
        private Report fReport;
        private WebReportProperties fProperties;
        private string fName;

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return fName; }            
            set { fName = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Report Report
        {
            get { return fReport; }
            set { fReport = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public WebReportProperties Properties
        {
            get { return fProperties; }
            set { fProperties = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ReportTab()
        {
            fReport = new Report();
            fProperties = new WebReportProperties();
        }

        /// <summary>
        /// 
        /// </summary>
        public ReportTab(Report report)
        {
            fReport = report;
            fProperties = new WebReportProperties();
        }

        /// <summary>
        /// 
        /// </summary>
        public ReportTab(Report report, WebReportProperties properties)
        {
            fReport = report;
            fProperties = properties;
        }
    }
}
