﻿namespace EllisBillFilter.Forms
{
    partial class FrmExcelImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExcelImport));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabImport = new System.Windows.Forms.TabPage();
            this.btnImport = new System.Windows.Forms.Button();
            this.gridExcelImport1 = new EllisBillFilter.Controls.GridExcelImport();
            this.tabDataPrepare = new System.Windows.Forms.TabPage();
            this.btnBindAddress = new System.Windows.Forms.Button();
            this.gridBindAddress1 = new EllisBillFilter.Controls.GridBindAddress();
            this.tabEllisImport = new System.Windows.Forms.TabPage();
            this.btnExcuteDynamicImportScript = new System.Windows.Forms.Button();
            this.gridDynamicDataImport1 = new EllisBillFilter.Controls.GridDynamicDataImport();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.txtTimer = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1.SuspendLayout();
            this.tabImport.SuspendLayout();
            this.tabDataPrepare.SuspendLayout();
            this.tabEllisImport.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabImport);
            this.tabControl1.Controls.Add(this.tabDataPrepare);
            this.tabControl1.Controls.Add(this.tabEllisImport);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1277, 656);
            this.tabControl1.TabIndex = 0;
            // 
            // tabImport
            // 
            this.tabImport.Controls.Add(this.btnImport);
            this.tabImport.Controls.Add(this.gridExcelImport1);
            this.tabImport.Location = new System.Drawing.Point(4, 22);
            this.tabImport.Name = "tabImport";
            this.tabImport.Padding = new System.Windows.Forms.Padding(3);
            this.tabImport.Size = new System.Drawing.Size(1269, 630);
            this.tabImport.TabIndex = 0;
            this.tabImport.Text = "ИМПОРТ";
            this.tabImport.UseVisualStyleBackColor = true;
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImport.Image = global::EllisBillFilter.Properties.Resources.Database_Save;
            this.btnImport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImport.Location = new System.Drawing.Point(8, 595);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(158, 27);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "выполнить импорт";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // gridExcelImport1
            // 
            this.gridExcelImport1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridExcelImport1.Location = new System.Drawing.Point(3, 3);
            this.gridExcelImport1.Name = "gridExcelImport1";
            this.gridExcelImport1.Size = new System.Drawing.Size(1263, 581);
            this.gridExcelImport1.TabIndex = 0;
            // 
            // tabDataPrepare
            // 
            this.tabDataPrepare.Controls.Add(this.btnBindAddress);
            this.tabDataPrepare.Controls.Add(this.gridBindAddress1);
            this.tabDataPrepare.Location = new System.Drawing.Point(4, 22);
            this.tabDataPrepare.Name = "tabDataPrepare";
            this.tabDataPrepare.Padding = new System.Windows.Forms.Padding(3);
            this.tabDataPrepare.Size = new System.Drawing.Size(1269, 630);
            this.tabDataPrepare.TabIndex = 1;
            this.tabDataPrepare.Text = "СОЕДИНЕНИЕ АДРЕСОВ";
            this.tabDataPrepare.UseVisualStyleBackColor = true;
            // 
            // btnBindAddress
            // 
            this.btnBindAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBindAddress.Image = global::EllisBillFilter.Properties.Resources.Link;
            this.btnBindAddress.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBindAddress.Location = new System.Drawing.Point(6, 597);
            this.btnBindAddress.Name = "btnBindAddress";
            this.btnBindAddress.Size = new System.Drawing.Size(155, 27);
            this.btnBindAddress.TabIndex = 1;
            this.btnBindAddress.Text = "Связать лицевые счета";
            this.btnBindAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBindAddress.UseVisualStyleBackColor = true;
            this.btnBindAddress.Click += new System.EventHandler(this.btnBindAddress_Click);
            // 
            // gridBindAddress1
            // 
            this.gridBindAddress1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridBindAddress1.Location = new System.Drawing.Point(3, 3);
            this.gridBindAddress1.Name = "gridBindAddress1";
            this.gridBindAddress1.Size = new System.Drawing.Size(1263, 588);
            this.gridBindAddress1.TabIndex = 0;
            // 
            // tabEllisImport
            // 
            this.tabEllisImport.Controls.Add(this.btnExcuteDynamicImportScript);
            this.tabEllisImport.Controls.Add(this.gridDynamicDataImport1);
            this.tabEllisImport.Location = new System.Drawing.Point(4, 22);
            this.tabEllisImport.Name = "tabEllisImport";
            this.tabEllisImport.Padding = new System.Windows.Forms.Padding(3);
            this.tabEllisImport.Size = new System.Drawing.Size(1269, 630);
            this.tabEllisImport.TabIndex = 2;
            this.tabEllisImport.Text = "ЗАГРУЗКА ДАННЫХ В КАС \"ЭЛЛИС ЖКХ\"";
            this.tabEllisImport.UseVisualStyleBackColor = true;
            // 
            // btnExcuteDynamicImportScript
            // 
            this.btnExcuteDynamicImportScript.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcuteDynamicImportScript.Image = global::EllisBillFilter.Properties.Resources.Script_Run;
            this.btnExcuteDynamicImportScript.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcuteDynamicImportScript.Location = new System.Drawing.Point(5, 595);
            this.btnExcuteDynamicImportScript.Name = "btnExcuteDynamicImportScript";
            this.btnExcuteDynamicImportScript.Size = new System.Drawing.Size(96, 27);
            this.btnExcuteDynamicImportScript.TabIndex = 1;
            this.btnExcuteDynamicImportScript.Text = "Выполнить";
            this.btnExcuteDynamicImportScript.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcuteDynamicImportScript.UseVisualStyleBackColor = true;
            this.btnExcuteDynamicImportScript.Click += new System.EventHandler(this.btnExcuteDynamicImportScript_Click);
            // 
            // gridDynamicDataImport1
            // 
            this.gridDynamicDataImport1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDynamicDataImport1.Location = new System.Drawing.Point(3, 3);
            this.gridDynamicDataImport1.Name = "gridDynamicDataImport1";
            this.gridDynamicDataImport1.Size = new System.Drawing.Size(1263, 586);
            this.gridDynamicDataImport1.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtTimer});
            this.statusStrip1.Location = new System.Drawing.Point(0, 657);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1277, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // txtTimer
            // 
            this.txtTimer.Name = "txtTimer";
            this.txtTimer.Size = new System.Drawing.Size(41, 17);
            this.txtTimer.Text = "TIMER";
            // 
            // FrmExcelImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 679);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmExcelImport";
            this.Text = "Импорт данных";
            this.tabControl1.ResumeLayout(false);
            this.tabImport.ResumeLayout(false);
            this.tabDataPrepare.ResumeLayout(false);
            this.tabEllisImport.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabImport;
        private System.Windows.Forms.TabPage tabDataPrepare;
        private Controls.GridExcelImport gridExcelImport1;
        private System.Windows.Forms.Button btnImport;
        private Controls.GridBindAddress gridBindAddress1;
        private System.Windows.Forms.Button btnBindAddress;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel txtTimer;
        private System.Windows.Forms.TabPage tabEllisImport;
        private Controls.GridDynamicDataImport gridDynamicDataImport1;
        private System.Windows.Forms.Button btnExcuteDynamicImportScript;
    }
}