﻿using Helper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GisGkh
{
    public class ImportMeteringDevice : MrivcImportCommand, ISingleHouseCommand, IMrivcImportCommandAsync
    {
        public string FiasHouseGuid { get; set; }
        public bool UpdateEnabled { get; set; }

        ExportMeteringDevice exportMeteringDevice;

        bool _isRSO;
        bool _preview = true;

        public override string SenderID
        {
            get
            {
                return base.SenderID;
            }

            set
            {
                _isRSO = MrivcStatic.isRSO(value);
                base.SenderID = value;
            }
        }

        public ImportMeteringDevice(string senderID) : base("importMeteringDeviceData", "HomeManagementAsync", senderID)
        {
            _importValueName = "MeteringDevice";
            _maxImportValue = 100;
            Description = "Синхронизация приборов учета";

            exportMeteringDevice = new ExportMeteringDevice(SenderID);
        }


        public void ExportData()
        {
            //if (!_isRSO)
            //{
                bool execute = true;

                if (CheckBeforeExport)
                {
                    execute = !this.IsEmpty && !HasErrors;
                }

                if (execute)
                {
                    exportMeteringDevice.AddToReport = this.AddToReport;
                    exportMeteringDevice.FiasHouseGuid = this.FiasHouseGuid;
                    exportMeteringDevice.Description = Description;
                    exportMeteringDevice.CommandType = CommandType;
                    exportMeteringDevice.Execute();

                    this.HasErrors = exportMeteringDevice.HasErrors;
                }
            //}
        }

        public void ImportData()
        {
            if (!HasErrors)
            {
                //if (!_isRSO)
                    base.Execute();
            }
        }

        public void Execute()
        {
            CheckBeforeExport = false;
            ExportData();

            ImportData();

            CheckBeforeExport = true;
            ExportData();
        }

        public override object Clone()
        {
            var exportMeteringDeviceClone = (ExportMeteringDevice)exportMeteringDevice.Clone();
            var baseClone = (MrivcCommand)base.Clone();
            ((ImportMeteringDevice)baseClone).exportMeteringDevice = exportMeteringDeviceClone;
            return baseClone;
        }

        public DataSet PrepareToDataSet()
        {
            //if (!_isRSO)
            //{
                return DAccess.DataModule.GetDataSetByStoredProcedure1("[GIS].[Prepare_Import_Meter_Full]", new object[,] { { "@SenderID", SenderID }, { "@FiasHouseGuid", FiasHouseGuid }, { "@preview", _preview }, { "@empty", exportMeteringDevice.HasErrors }, { "@update", UpdateEnabled } }, "ImportMeter", "ImportFlatMeter", "ImportHouseMeter");
            //}
            //else
            //    return DAccess.DataModule.GetDataSetByStoredProcedure1("[GIS].[Prepare_Import_Meter_Full]", new object[,] { { "@SenderID", SenderID }, { "@FiasHouseGuid", FiasHouseGuid }, { "@preview", _preview }, { "@empty", true }, { "@update", UpdateEnabled } }, "ImportMeter", "ImportFlatMeter", "ImportHouseMeter");
        }

        protected override void CreateXmlBody()
        {
            _preview = false;
            var ds = PrepareToDataSet();
            _preview = true;

            Values = new[] { new Tuple<string[], string>(new[] { "FIASHouseGuid" }, FiasHouseGuid) };

            var createValues = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceNumber" },"MeteringDeviceNumber"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceStamp" },"MeteringDeviceStamp"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceModel" },"MeteringDeviceModel"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CommissioningDate" },"CommissioningDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "RemoteMeteringMode" },"RemoteMeteringMode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "FirstVerificationDate" },"FirstVerificationDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "VerificationInterval", "Code" },"VerificationIntervalCode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "VerificationInterval", "GUID"  },"VerificationIntervalGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "VerificationInterval", "Name"  },"VerificationIntervalName"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "FactorySealDate" },"FactorySealDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "TemperatureSensor" },"TemperatureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "PressureSensor" },"PressureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "Code" },"MunicipalResourceCode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "GUID"},"MunicipalResourceGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "Name"},"MunicipalResourceName"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy","MeteringValue" },"MeteringValue")
                };

            foreach (DataRow row in ds.Tables["ImportHouseMeter"].Select("Energy = 0"))
            {
                AddChildElement(row, "HomeManagementAsync", "importMeteringDeviceData_House_add", "importMeteringDeviceData_House_update", null, createValues, null, "importMeteringDeviceDataRequest");

                CounterCheck();
            }

            createValues = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceNumber" },"MeteringDeviceNumber"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceStamp" },"MeteringDeviceStamp"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceModel" },"MeteringDeviceModel"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CommissioningDate" },"CommissioningDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "RemoteMeteringMode" },"RemoteMeteringMode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "FirstVerificationDate" },"FirstVerificationDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "VerificationInterval", "Code" },"VerificationIntervalCode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "VerificationInterval", "GUID"  },"VerificationIntervalGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "VerificationInterval", "Name"  },"VerificationIntervalName"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "FactorySealDate" },"FactorySealDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "TemperatureSensor" },"TemperatureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "PressureSensor" },"PressureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceEnergy", "MeteringValueT1", },"MeteringValue")
                };

            foreach (DataRow row in ds.Tables["ImportHouseMeter"].Select("Energy = 1"))
            {
                AddChildElement(row, "HomeManagementAsync", "importMeteringDeviceData_House_en_add", "importMeteringDeviceData_House_en_update", null, createValues, null, "importMeteringDeviceDataRequest");

                CounterCheck();
            }


            createValues = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceNumber" },"MeteringDeviceNumber"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceStamp" },"MeteringDeviceStamp"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceModel" },"MeteringDeviceModel"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CommissioningDate" },"CommissioningDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "RemoteMeteringMode" },"RemoteMeteringMode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "FactorySealDate" },"FactorySealDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "TemperatureSensor" },"TemperatureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "PressureSensor" },"PressureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CollectiveApartmentDevice", "PremiseGUID" },"PremisesGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CollectiveApartmentDevice", "AccountGUID" },"AccountGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "Code" },"MunicipalResourceCode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "GUID"},"MunicipalResourceGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "Name"},"MunicipalResourceName"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy","MeteringValue" },"MeteringValue")
                };

            foreach (DataRow row in ds.Tables["ImportFlatMeter"].Select("Energy = 0"))
            {
                AddChildElement(row, "HomeManagementAsync", "importMeteringDeviceData_flat_add", "importMeteringDeviceData_flat_update", null, createValues, null, "importMeteringDeviceDataRequest");

                CounterCheck();
            }

            createValues = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceNumber" },"MeteringDeviceNumber"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceStamp" },"MeteringDeviceStamp"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceModel" },"MeteringDeviceModel"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CommissioningDate" },"CommissioningDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "RemoteMeteringMode" },"RemoteMeteringMode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "FactorySealDate" },"FactorySealDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "TemperatureSensor" },"TemperatureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "PressureSensor" },"PressureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CollectiveApartmentDevice", "PremiseGUID" },"PremisesGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CollectiveApartmentDevice", "AccountGUID" },"AccountGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceEnergy", "MeteringValueT1", },"MeteringValue")
                };

            foreach (DataRow row in ds.Tables["ImportFlatMeter"].Select("Energy = 1"))
            {
                AddChildElement(row, "HomeManagementAsync", "importMeteringDeviceData_flat_en_add", "importMeteringDeviceData_flat_en_update", null, createValues, null, "importMeteringDeviceDataRequest");

                CounterCheck();
            }

            createValues = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceNumber" },"MeteringDeviceNumber"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceStamp" },"MeteringDeviceStamp"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceModel" },"MeteringDeviceModel"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CommissioningDate" },"CommissioningDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "RemoteMeteringMode" },"RemoteMeteringMode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "FactorySealDate" },"FactorySealDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "TemperatureSensor" },"TemperatureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "PressureSensor" },"PressureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "ResidentialPremiseDevice", "PremiseGUID" },"PremisesGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "ResidentialPremiseDevice", "AccountGUID" },"AccountGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "Code" },"MunicipalResourceCode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "GUID"},"MunicipalResourceGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy", "MunicipalResource", "Name"},"MunicipalResourceName"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceNotEnergy","MeteringValue" },"MeteringValue")
                };

            foreach (DataRow row in ds.Tables["ImportMeter"].Select("Energy = 0"))
            {
                AddChildElement(row, "HomeManagementAsync", "importMeteringDeviceData_add", "importMeteringDeviceData_update", null, createValues, null, "importMeteringDeviceDataRequest");

                CounterCheck();
            }

            createValues = new List<Tuple<string[], string>>
                {
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceNumber" },"MeteringDeviceNumber"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceStamp" },"MeteringDeviceStamp"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "MeteringDeviceModel" },"MeteringDeviceModel"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "CommissioningDate" },"CommissioningDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "RemoteMeteringMode" },"RemoteMeteringMode"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "FactorySealDate" },"FactorySealDate"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "TemperatureSensor" },"TemperatureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "PressureSensor" },"PressureSensor"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "ResidentialPremiseDevice", "PremiseGUID" },"PremisesGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "BasicChatacteristicts", "ResidentialPremiseDevice", "AccountGUID" },"AccountGUID"),
                    new Tuple<string[], string>( new [] { "DeviceDataToCreate", "MunicipalResourceEnergy", "MeteringValueT1", },"MeteringValue")
                };

            foreach (DataRow row in ds.Tables["ImportMeter"].Select("Energy = 1"))
            {
                AddChildElement(row, "HomeManagementAsync", "importMeteringDeviceData_en_add", "importMeteringDeviceData_en_update", null, createValues, null, "importMeteringDeviceDataRequest");

                CounterCheck();
            }
        }
    }
}
