﻿using DevExpress.XtraEditors;
using MrivcHelpers.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EllisBillFilter
{
    public class DynamicParameterDateTimeControl: DynamicParameterBaseControl
    {
        DateEdit dateEdit;

        public DynamicParameterDateTimeControl(DynamicParameter parameter) : base(parameter)
        {
        }

        public override object EditValue
        {
            get
            {             
                return dateEdit.DateTime.ShortDateStringForSQL();
            }
        }

        protected override Control CreateControl()
        {
            dateEdit = new DateEdit();
            return dateEdit;
        }
    }
}
