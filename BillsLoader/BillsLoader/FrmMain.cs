﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BillsLoader.DM;
using System.IO;
using BillsLoader.Logic;
using System.Threading;

namespace BillsLoader
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void LoadReestr()
        {
            listBox1.Items.Clear();
            ReestrLoader.Loaded += new LoadedEventHandler(ReestrLoader_Loaded);
            ReestrLoader.ScanAndLoadAllReestr();
        }

        public void LoadReestrAndClose()
        {
            LoadReestr();
            Thread.Sleep(5000);
            Close();
        }

        void ReestrLoader_Loaded(int totalReestr, int loadedCount, ReestrInfo reestrInfo, string loadedStatus)
        {
            lblStatus.Text = "Статус: загружено " + loadedCount.ToString() + " реестров из " + totalReestr.ToString() + " реестров.";
            listBox1.Items.Add(reestrInfo.SourceFile.FullName + " - " + loadedStatus + " (" + reestrInfo.Error + ")");
            lblStatus.Refresh();
            listBox1.Refresh();
        }
    }
}
