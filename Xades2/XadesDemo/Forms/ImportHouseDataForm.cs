﻿using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Linq;

namespace XadesDemo
{
    public partial class ImportHouseDataForm : Form
    {
        public string SenderID
        {
            get
            {
                return "";
            }
        }

        public ImportHouseDataForm()
        {
            InitializeComponent();
        }

        private void ImportHouseDataForm_Load(object sender, EventArgs e)
        {

        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            var importHouseData = new ImportHouseData(SenderID);
            importHouseData.AddToReport = true;
            MrivcStatic.HouseIteration(importHouseData, false);

            var importAccountData = new ImportAccountData(SenderID);
            importAccountData.AddToReport = true;
            MrivcStatic.HouseIteration(importAccountData, false);
        }

        private void addHouseDatabtn_Click(object sender, EventArgs e)
        {

        }

        private void houseDataGridView_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void entranceDataGridView_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void previewImportBtn_Click(object sender, EventArgs e)
        {
            var importHouseData = new ImportHouseData(SenderID);

            var ds = MrivcStatic.HouseIteration(importHouseData, true);

            houseDataGridView.DataSource = ds.Tables[0];
            entranceDataGridView.DataSource = ds.Tables[1];
            premisesDataGridView.DataSource = ds.Tables[2];

            var importAccountData = new ImportAccountData(SenderID);

            var ds1 = MrivcStatic.HouseIteration(importAccountData, true);

            accountDataGridView.DataSource = ds1.Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var importAccountData = new ImportAccountData(SenderID);
            var ds = MrivcStatic.HouseIteration(importAccountData, true);

            accountDataGridView.DataSource = ds.Tables[0];
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var importAccountData = new ImportAccountData(SenderID);
            MrivcStatic.HouseIteration(importAccountData, false);
        }
    }
}
