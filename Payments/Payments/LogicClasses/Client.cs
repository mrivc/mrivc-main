using System;

namespace Payments.LogicClasses
{
    using Payments.DS;

    public class Client 
    {
        private string attributes;

        public Client(string code)
        {
            this.Code = code;
            this.CompanyId = int.Parse(DAccess.Instance.GetClientFilterFieldValue(code, "ID_Company"));
            this.Name = DAccess.Instance.GetClientFilterFieldValue(code, "Name");
            this.Prefix = DAccess.Instance.GetClientFilterFieldValue(code, "Prefix");
            this.Attributes = DAccess.Instance.GetClientFilterFieldValue(code, "Attributes");
            this.CommissionRate = Math.Round(decimal.Parse(DAccess.Instance.GetClientFilterFieldValue(code, "CommissionRate")), 2);
            this.CommissionMinSumm = Math.Round(decimal.Parse(DAccess.Instance.GetClientFilterFieldValue(code, "CommissionMinSumm")), 2);
            this.Comment = DAccess.Instance.GetClientFilterFieldValue(code, "Comment");
            this.Phone = DAccess.Instance.GetClientFilterFieldValue(code, "Phone");
        }

        /// <summary>
        /// Gets or sets the company id.
        /// </summary>
        public int CompanyId { get; set; }

        public string Phone { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Prefix { get; set; }

        public string Attributes 
        {
            get
            {
                return attributes;
            }

            set
            {
                attributes = value;
            }
        }

        public decimal CommissionRate { get; set; }

        public decimal CommissionMinSumm { get; set; }

        public string Comment { get; set; }

        /// <summary>
        /// Gets the attribute - ����������.
        /// </summary>
        public string PayeerName
        {
            get
            {
                return this.GetAttributeValue("����������");
            }
        }

        /// <summary>
        /// Gets the attribute - ���.
        /// </summary>
        public string Kpp
        {
            get
            {
                return this.GetAttributeValue("���");
            }
        }

        /// <summary>
        /// Gets the attribute - ����.
        /// </summary>
        public string Bill
        {
            get
            {
                return this.GetAttributeValue("����");
            }
        }

        /// <summary>
        /// Gets the attribute - ����.
        /// </summary>
        public string BankName
        {
            get
            {
                return this.GetAttributeValue("����");
            }
        }

        public string GetAttributeValue(string attributeName)
        {
            string[] atts = attributes.Split(new[] { ';' });
            string attName = string.Empty;
            string attValue = string.Empty;

            foreach (var att in atts)
            {
                string[] s = att.Split(new[] { '=' });
                attName = s[0];
                attValue = s[1];
                if (attName.ToLower() == attributeName.ToLower())
                    break;
            }
            return attValue;
        }
    }
}