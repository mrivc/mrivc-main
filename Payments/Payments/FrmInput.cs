﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Payments
{
    public partial class FrmInput : Form
    {
        public FrmInput()
        {
            InitializeComponent();
        }

        public FrmInput(string formName, string fieldLabel, bool isPassword)
        {
            InitializeComponent();
            Text = formName;
            lblField.Text = fieldLabel;

            textEdit1.Properties.PasswordChar = '*';
        }

        public FrmInput(string formName, string fieldLabel)
        {
            InitializeComponent();
            Text = formName;
            lblField.Text = fieldLabel;
            //textEdit1.EditValue = 0;

            //textEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //textEdit1.Properties.Mask.EditMask = "c2";
            //textEdit1.Properties.Mask.UseMaskAsDisplayFormat = true;
        }

        public string Value
        {
            get { return textEdit1.EditValue.ToString(); }
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void FrmInput_Load(object sender, EventArgs e)
        {

        }
    }
}
