﻿using CommandLine;
using CommandLine.Text;
using MrivcControls.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using Xades.Implementations;
using XadesDemo.Commands;
using XadesDemo.Configurations.Options;
using XadesDemo.Configurations.Sections;
using XadesDemo.Infrastructure;

namespace XadesDemo
{
    public class MrivcCommand
    {
        protected MrivcXmlFormatter mrivcXmlFormatter = new MrivcXmlFormatter();

        public bool HasErrors { get; set; }

        public string Method { get; set; }
        public string Service { get; set; }
        public string Description { get; set; }
        public virtual string SenderID { get; set; }
        public int GetStateCommandWaitCount { get; set; } = 1000;
        public float GetStateCommandWaitDelay { get; set; } = 1F;
        public bool AddToReport { get; set; } = false;


        public IEnumerable<Tuple<string[], string>> Values
        {
            set
            {
                mrivcXmlFormatter.Values = value;
            }
        }

        public MrivcCommand(string method, string service, string senderID)
        {
            Method = method;
            Service = service;
            SenderID = senderID;

            LoadRoot();
        }

        public MrivcCommand(string method, string service)
        {
            Method = method;
            Service = service;

            LoadRoot();
        }

        void LoadRoot()
        {
            var rootPath = File.ReadAllText(MrivcStatic.GisConfig.Services[Service].Methods[Method].Template);
            mrivcXmlFormatter.LoadRoot(rootPath);
        }

        string ExecuteCommand(string[] args, string xmlBody, string senderID)
        {
            var gisConfig = (GisServiceConfiguration)ConfigurationManager.GetSection(Constants.GisServicesConfigSectionName);
            var xadesConfig = (SigningConfiguration)ConfigurationManager.GetSection(Constants.XadesConfigSectionName);
            var xadesService = new GostXadesBesService();

            SentenceBuilder.UseSentenceBuilder(new RussianSentenceBuilder());

            ICommand command = Parser.Default.ParseArguments<SignOptions, VerifyOptions, CertificateOptions, GetStateOptions, SendOptions>(args)
                    .MapResult(
                        (SignOptions o) => new SignCommand(o, xadesService, xadesConfig),
                        (VerifyOptions o) => new VerifyCommand(o, xadesService, xadesConfig),
                        (SendOptions o) => new SendCommand(o, xadesService, xadesConfig, gisConfig),
                        (GetStateOptions o) => new GetStateCommand(o, xadesService, xadesConfig, gisConfig),
                        (CertificateOptions o) => new CertificateCommand(o),
                        errors => (ICommand)null);

            if (command != null)
            {
                return command.Execute(xmlBody, senderID);
            }
            else
            {
                return "";
            }
        }

        string ExecuteGetStateCommand(string[] getStateArgs, string xmlBody)
        {
            var output = "";
            var status = 1;
            var i = 0;
            var n = GetStateCommandWaitCount;
            var t = GetStateCommandWaitDelay;
            var getStateResult = "";

            while (status == 1 || status == 2)  //Если статус "в обработке", то выполнить get-stae снова n раз пока не будет полчучен статус "обработан"
            {
                getStateResult = ExecuteCommand(getStateArgs, xmlBody, SenderID);

                output += getStateResult;

                var res = MrivcStatic.SelectNodeByName("//ns4:RequestState", getStateResult);
                status = int.Parse(res.InnerText);

                if (status == 1 || status == 2)
                {
                    var sleepTimeMs = (int)(t * 1000);
                    Thread.Sleep(sleepTimeMs);
                }

                i++;
                if (i == n)
                {
                    throw new MrivcCommandException("MRIVC_TIMEOUT",$"МРИВЦ: Превышено время ожидания обработки команды", SenderID, Service, Method);
                }
            }

            return getStateResult;
        }

        protected virtual XmlNode AddChildElement(string childElementTemplatePath, IEnumerable<Tuple<string[],string>> values, string elementToAdd)
        {
            var child = File.ReadAllText(childElementTemplatePath);
            return mrivcXmlFormatter.AddChildElement(child,  new[] { elementToAdd } , values);
        }

        protected XmlNode AddChildElement(string childElementTemplatePath, IEnumerable<Tuple<string[], string>> values, XmlNode xmlNodeToAdd)
        {
            var child = File.ReadAllText(childElementTemplatePath);
            return mrivcXmlFormatter.AddChildElement(child, xmlNodeToAdd, values);
        }

        public virtual string Execute()
        {
            try
            {
                HasErrors = false;

                var xmlBody = mrivcXmlFormatter.XmlBody;
                xmlBody = MrivcStatic.ReplaceTransportGUID(xmlBody);

                xmlBody = MrivcStatic.IndentXml(xmlBody);

                var sendArgs = MrivcStatic.PrepareSendCommandArgs(Service, Method);
                //MrivcStatic.AddToOutput("-------------Send result---------------");
                MrivcStatic.CurrentSoapRequest = "";
                var sendResponse = ExecuteCommand(sendArgs, xmlBody, SenderID);

                var messageGuid = MrivcStatic.GetMessageGuidBySendResponse(sendResponse);

                var soapRequest = MrivcStatic.CurrentSoapRequest;

                ErrorCheck(sendResponse, soapRequest, messageGuid);

                var getStateArgs = MrivcStatic.PrepareGetStateCommandArgs(Service, Method, messageGuid);
                //MrivcStatic.AddToOutput("-------------Get State result----------");
                if (messageGuid == "")
                {
                    var ex = new MrivcCommandException("MRIVC_MGUID_MISSING","МРИВЦ: Не удалось выполнить команду get-state: MessageGUID отсутствует", SenderID, Service, Method);
                    ex.SendRequest = soapRequest;
                    throw ex;
                }
                var getStateXmlBody = MrivcStatic.PrepareXmlBody(Service, "getState", new[] { new Tuple<string[], string>(new[] { "MessageGUID" }, messageGuid) });
                var getStateResponse = ExecuteGetStateCommand(getStateArgs, getStateXmlBody);

                ErrorCheck(getStateResponse, soapRequest, messageGuid);

                    if (this is ISingleHouseCommand)
                        MrivcStatic.WriteLog(this, ((ISingleHouseCommand)this).FiasHouseGuid);
                    else
                        MrivcStatic.WriteLog(this);

                return getStateResponse;
            }
            catch (MrivcCommandException mcex)
            {
                    if (this is ISingleHouseCommand)
                        MrivcStatic.WriteLog(mcex, AddToReport, Description,((ISingleHouseCommand)this).FiasHouseGuid);
                    else
                        MrivcStatic.WriteLog(mcex, AddToReport, Description);

                HasErrors = true;
                return null;
            }
            catch (Exception ex)
            {
                if (this is ISingleHouseCommand)
                    MrivcStatic.WriteLog(this, ex, ((ISingleHouseCommand)this).FiasHouseGuid);
                else
                    MrivcStatic.WriteLog(this, ex);

                HasErrors = true;
                return null;
            }
        }

        public string Execute(IEnumerable<Tuple<string[],string>> values)
        {
            mrivcXmlFormatter.LoadRoot(MrivcStatic.PrepareXmlBody(Service, Method, values));
            return Execute();
        }

        DataSet ExecuteToDataSetBase(string response, string nodeName)
        {
            if (!(response == null || response == ""))
            {
                var res = MrivcStatic.SelectNodesByName(MrivcStatic.ResultNodeName(nodeName), response);

                var resXml = "<mrivcResult>";
                foreach (XmlNode item in res)
                {
                    resXml += item.OuterXml;
                }
                resXml += "</mrivcResult>";
                //resXml = MrivcStatic.IndentXml(resXml);

                try
                {
                    MrivcStatic.RemoveNodesByName("Terminate", ref resXml);
                }
                catch (Exception)
                {}

                var ds = new DataSet();
                ds.ReadXml(new StringReader(resXml));
                return ds;
            }
            else
                return new DataSet();
        }

        public DataSet ExecuteToDataSet(IEnumerable<Tuple<string[], string>> values, string nodeName)
        {
            var response = Execute(values);
            return ExecuteToDataSetBase(response, nodeName);
        }

        public DataSet ExecuteToDataSet(string nodeName)
        {
            var response = Execute();
            return ExecuteToDataSetBase(response, nodeName);
        }

        protected void ErrorCheck(string response, string sendRequest, string messageGUID)
        {
            var err = MrivcStatic.GetErrorFromResponse(response);

            if (err != null)
                if(!(this is MrivcExportCommand && err.Item1 == "INT002012"))
                {
                    response = MrivcStatic.IndentXml(response);

                    var ex = new MrivcCommandException(err.Item1, err.Item2, SenderID, Service, Method);
                    ex.SendRequest = sendRequest;
                    ex.MessageGUID = messageGUID;
                    ex.Response = response;
                    throw ex;
                }
        }

        protected void ErrorCheck(string response, string sendRequest)
        {
            var err = MrivcStatic.GetErrorFromResponse(response);

            if (err != null)
                if (!(this is MrivcExportCommand && err.Item1 == "INT002012"))
                {
                    response = MrivcStatic.IndentXml(response);

                    var ex = new MrivcCommandException(err.Item1, err.Item2, SenderID, Service, Method);
                    ex.SendRequest = sendRequest;
                    ex.Response = response;
                    throw ex; 
                }
        }

        public virtual void ExecuteToTempDB(IEnumerable<Tuple<string[], string>> values, string nodeName)
        {
            var ds = ExecuteToDataSet(values, nodeName);
            DAccess.DataModule.CopyDataSetToTempDB(ds);
        }


        public virtual void ExecuteToTempDB(string nodeName)
        {
            DAccess.DataModule.CopyDataSetToTempDB(ExecuteToDataSet(nodeName));
        }
    }
}
