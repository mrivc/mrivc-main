﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Payments.DS;
using System.Globalization;
using System.Threading;
using DevExpress.XtraEditors;
using Word = Microsoft.Office.Interop.Word;
using Payments.Helpers;

namespace Payments
{
    using System.IO;

    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            Init();
        }

        public void Init()
        {
            toolStripConnectionInfoDBEllisItem.Text = toolStripConnectionInfoDBEllisItem.Text.Replace("DB_NAME", DAccess.Instance.ProgramSettings.ELLIS_DB_NAME);
            toolStripConnectionInfoDBEllisItem.Text = toolStripConnectionInfoDBEllisItem.Text.Replace("SERVER_NAME", DAccess.Instance.ProgramSettings.ELLIS_DB_SERVER);
            toolStripConnectionInfoDBPaymentItem.Text = toolStripConnectionInfoDBPaymentItem.Text.Replace("DB_NAME", DAccess.DataModule.ConnectionPaymentsDBName);
            toolStripConnectionInfoDBPaymentItem.Text = toolStripConnectionInfoDBPaymentItem.Text.Replace("SERVER_NAME", DAccess.DataModule.ConnectionPaymentsServerName);
            toolStripUserInfo.Text = "Пользователь: " + DAccess.Instance.CurrentUser.Name;
            toolStripProgramVersion.Text = toolStripProgramVersion.Text.Replace("<version>", System.Windows.Forms.Application.ProductVersion);

            DAccess.CashTerminal.TerminalECRStatusUpdatedEvent += new CashTerminal.TerminalStatusUpdatedEventHandler(CashTerminal_TerminalStatusUpdatedEvent);
            DAccess.CashTerminal.TerminalCommandComplitedEvent += new CashTerminal.TerminalCommandComplitedEventHandler(CashTerminal_TerminalCommandComplitedEvent);
            DAccess.CashTerminal.UpdateECRTerminalStatus(false);

            CultureInfo inf = new CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name);
            System.Threading.Thread.CurrentThread.CurrentCulture = inf;
            inf.NumberFormat.NumberDecimalSeparator = ".";
            gridPayments1.LoadData();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            gridPayments1.SplitContainer.SplitterPosition = (gridPayments1.SplitContainer.Height / 2) + 100; 
        }

        /// <summary>
        /// The cash terminal_ terminal command complited event.
        /// </summary>
        /// <param name="terminal">
        /// The terminal.
        /// </param>
        /// <param name="commandName">
        /// The command name.
        /// </param>
        void CashTerminal_TerminalCommandComplitedEvent(DrvFRLib.DrvFRClass terminal, string commandName)
        {
            Text = "Касса (" + DAccess.CashTerminal.Terminal.ResultCode + " - " + DAccess.CashTerminal.Terminal.ResultCodeDescription + ") - последняя операция: " + commandName;
            RefreshOfdStatusInfo();
        }

        void CashTerminal_TerminalStatusUpdatedEvent(DrvFRLib.DrvFRClass terminal, string statusInfo)
        {
            if (terminal.ResultCode == -1 || terminal.ResultCode == -2)
                MessageBox.Show("Нет связи с кассой. Проверьте, включена ли касса.\nЕсли касса выключена, включите кассу и выполните пункт программы «Сервис – Обновить состояние кассы»",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

            Text = "Касса (" + terminal.ResultCode + " - " + terminal.ResultCodeDescription + ")";
            toolStripTerminalState.Text = "Статус кассы (" + DateTime.Now.ToLongTimeString() + "): " + statusInfo;
            toolStripTerminalSubStatus.Text = "Режим кассы: " + terminal.ECRAdvancedModeDescription;

            RefreshOfdStatusInfo();
            statusStrip1.Refresh();
        }

        public void RefreshOfdStatusInfo()
        {
            toolStripOfdDocs.Text = "Документов для ОФД: " + DAccess.CashTerminal.FNGetOfdMessageCount();
        }

        private void FrmMain_Shown(object sender, EventArgs e)
        {
            //gridPayments1.LoadData();
        }

        private void miFileExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void miServiceTerminalStatusRefresh_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.UpdateECRTerminalStatus(true);
        }

        private void miTerminalReportWithCleaning_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.WriteTableFieldValue(17, 1, 7, "0"); // печатать чек
            if (DAccess.CashTerminal.ReadTableFieldValue(17, 1, 7).ToString() == "0")
            {
                DAccess.CashTerminal.PrintReportWithCleaning();
            }
            else
            {
                MessageBox.Show("Печать бумаги отключена, повторите попытку или обратитесь к администратору");
            }
            
            
        }

        private void miTerminalPrintReportWithoutCleaning_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.WriteTableFieldValue(17, 1, 7, "0"); // печатать чек
            if (DAccess.CashTerminal.ReadTableFieldValue(17, 1, 7).ToString() == "0")
            {
                DAccess.CashTerminal.PrintReportWithoutCleaning();
            }
            else
            {
                MessageBox.Show("Печать бумаги отключена, повторите попытку или обратитесь к администратору");
            }
        }

        private void miTerminalOpenSession_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.OpenSession();
        }

        private void miTerminalOpenDrawer_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.OpenDrawer();
        }

        private void miServiceTerminalSetDateAndTime_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.SetDateAndTime();
        }

        private void miServiceTerminalContinuePrint_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.ContinuePrint();
        }

        //private void toolStripCancelCheck_Click(object sender, EventArgs e)
        //{
        //    DAccess.CashTerminal.SysAdminCancelCheck();
        //}

        private void miTerminalOperationRegister_DropDownOpening(object sender, EventArgs e)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!DAccess.CashTerminal.BeforeExecuteCommand(true))
                return;

            int cashRegister;
            string cashRegisterValue;

            foreach (ToolStripItem mi in miTerminalOperationRegister.DropDownItems)
            {
                if (mi is ToolStripMenuItem)
                {
                    cashRegister = int.Parse(mi.Name.Substring(mi.Name.Length - 3));
                    cashRegisterValue = DAccess.CashTerminal.CashRegister.GetRegisterValue(cashRegister).ToString();
                    mi.Text = mi.Text.Replace(StringHelper.GetSubstring(mi.Text, "<", ">"), cashRegisterValue + " руб.");
                    
                }
            }
        }

        private void miTerminalCashIncome_Click(object sender, EventArgs e)
        {
            if (!DAccess.Instance.CurrentUser.HasPriviligy())
                return;

            FrmInput frm = new FrmInput("Внесение денег в кассу", "Введите сумму:");
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DAccess.CashTerminal.CashIncome(decimal.Parse(frm.Value));
            }
        }

        private void miTerminalCashOutcome_Click(object sender, EventArgs e)
        {
            if (!DAccess.Instance.CurrentUser.HasPriviligy())
                return;

            FrmInput frm = new FrmInput("Инкассация", "Введите сумму:");
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DAccess.CashTerminal.CashOutcome(decimal.Parse(frm.Value));
            }
        }

        private void miServiceTerminalRepeatDocument_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.RepeatDocument();
        }

        private void toolStripConnectionInfoDBEllisItem_Click(object sender, EventArgs e)
        {
            FrmInput frm = new FrmInput("Введите ваш пароль", "Введите ваш пароль от ЭЛЛИС ЖКХ", true);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DAccess.DataModule.ConnectionStringToEllis = "Data Source=" + DAccess.Instance.ProgramSettings.ELLIS_DB_SERVER + ";Initial Catalog=" + DAccess.Instance.ProgramSettings.ELLIS_DB_NAME + ";Persist Security Info=True;User ID=" + DAccess.Instance.CurrentUser.Login + ";Password=" + frm.Value;
                Cursor = Cursors.WaitCursor;
                try
                {
                    DAccess.DataModule.SetEllisConnection();
                    Cursor = Cursors.Default;
                    MessageBox.Show("Соединение  с  базой данных ЭЛЛИС ЖКХ установлено успешно.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Соединение установить не удалось. Ошибка: " + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void toolStripConnectionInfo_DropDownOpening(object sender, EventArgs e)
        {
            toolStripConnectionInfoDBEllisItem.Text = "Соединение с БД ЭЛЛИС: <SERVER_NAME> - <DB_NAME>";
            toolStripConnectionInfoDBEllisItem.Text = toolStripConnectionInfoDBEllisItem.Text.Replace("DB_NAME", DAccess.Instance.ProgramSettings.ELLIS_DB_NAME);
            toolStripConnectionInfoDBEllisItem.Text = toolStripConnectionInfoDBEllisItem.Text.Replace("SERVER_NAME", DAccess.Instance.ProgramSettings.ELLIS_DB_SERVER);
        }

        private void miFileTerminalConnect_Click(object sender, EventArgs e)
        {
            FrmConnection f = new FrmConnection();
            f.ShowDialog();
        }

        private void miFileTerminalTableSettingsReset_Click(object sender, EventArgs e)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!DAccess.CashTerminal.BeforeExecuteCommand(true))
                return;

            if (DAccess.CashTerminal.RequestPassword(PasswordType.SysAdmin, "Сброс настроек кассы"))
            {
                Cursor = Cursors.WaitCursor;
                DAccess.CashTerminal.WriteTableFieldValue(1, 1, 2, "0");
                DAccess.CashTerminal.WriteTableFieldValue(1, 1, 6, "0");
                DAccess.CashTerminal.WriteTableFieldValue(1, 1, 7, "2");
                DAccess.CashTerminal.WriteTableFieldValue(1, 1, 19, "1");
                DAccess.CashTerminal.WriteTableFieldValue(1, 1, 20, "1");
                DAccess.CashTerminal.WriteTableFieldValue(1, 1, 30, "0");
                Cursor = Cursors.Default;
                MessageBox.Show("Настройки кассы успешно сброшены на стандартные.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void miHelpTerminal_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Docums\\user_manual.pdf");
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Delete | Keys.Control))
            {
                if (MessageBox.Show("Вы уверены,  что хотите аннулировать последний документ?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    DAccess.CashTerminal.CancelCheck();
                return true;
            }
            if (keyData == (Keys.F8 | Keys.Control))
            {
                if (MessageBox.Show("Вы уверены, что хотите очистить базу данных?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor = Cursors.WaitCursor;
                    DAccess.DataModule.ClearDb();
                    gridPayments1.ReLoadData();
                    Cursor = Cursors.Default;
                }
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void miCalc_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void miHelpUserGuide_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Docums\\user_guide.doc");
        }

        private void miServiceTerminalEllisReestr_Click(object sender, EventArgs e)
        {
            FrmPaymentEllisReestr frm = new FrmPaymentEllisReestr();
            frm.Show();
        }

        private void miServiceTerminalTxtReestr_Click(object sender, EventArgs e)
        {
            FrmPaymentTxtReestr frm = new FrmPaymentTxtReestr();
            frm.Show();
        }

        private void miFileTerminalUserUpdate_Click(object sender, EventArgs e)
        {
            if (DAccess.Instance.CurrentUser.SimulateTerminalMode)
                return;

            if (!DAccess.CashTerminal.BeforeExecuteCommand(true))
                return;

            if (DAccess.CashTerminal.RequestPassword(PasswordType.SysAdmin, "Обновление пользователя кассы"))
            {
                Cursor = Cursors.WaitCursor;
                DAccess.CashTerminal.WriteTableFieldValue(2, DAccess.Instance.CurrentUser.Fiscal_User_ID, 1, DAccess.Instance.CurrentUser.Fiscal_User_Password.ToString());
                DAccess.CashTerminal.WriteTableFieldValue(2, DAccess.Instance.CurrentUser.Fiscal_User_ID, 2, "КАССИР:" + DAccess.Instance.CurrentUser.Name);
                Cursor = Cursors.Default;
                MessageBox.Show("Пользователь кассы обновлен успешно.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        Word._Application oWord = new Word.Application();
        private void miDocumentControlFormPay_Click(object sender, EventArgs e)
        {
            frmPaymentFormControl _f = new frmPaymentFormControl();
            _f.ShowDialog();
        }

        private void miDocumentControlFormPayFreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Docums\\Формы_контроля_платежа.doc");
        }

        private void miServiceTerminalLoadRegistryVerificationSingle_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.Multiselect = false;
            openFileDialog1.RestoreDirectory = true;
            int id = -1;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    id = RegistryVerificationHelper.LoadToDbFromFile(99999, openFileDialog1.FileName);
                    this.Cursor = Cursors.Default;
                    RegistryVerificationHelper.SetLoadedStatus(id, true);
                    MessageBox.Show("Загрузка закончена.");
                }
                catch (Exception ex)
                {
                    RegistryVerificationHelper.SetLoadedStatus(id, false);
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void miServiceTerminalLoadRegistryVerificationMass_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Укажите папку с реестрами";
            fbd.ShowNewFolderButton = false;

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                List<FileInfo> fileList = new List<FileInfo>();
                FileHelper.GetDirectoryTree(new DirectoryInfo(fbd.SelectedPath), fileList);
                backgroundWorker1.RunWorkerAsync(fileList);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            List<FileInfo> fileList = (List<FileInfo>)e.Argument;
            foreach (var fileInfo in fileList)
            {
                int id = RegistryVerificationHelper.LoadToDbFromFile(99999, fileInfo.FullName);
                RegistryVerificationHelper.SetLoadedStatus(id, true);
            }
            MessageBox.Show("Загрузка закончена.");
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("backgroundWorker1_RunWorkerCompleted");
        }

        private void miOfdStatus_Click(object sender, EventArgs e)
        {
            FrmOfdStatus f = new FrmOfdStatus();
            f.Show();
        }

        private void miTerminalCorrectionReceipt_Click(object sender, EventArgs e)
        {
            if (!DAccess.Instance.CurrentUser.HasPriviligy())
                return;

            var mi = sender as ToolStripMenuItem;
            int checkType = int.Parse(mi.Tag.ToString());

            FrmInput frm = new FrmInput(string.Format("Сформировать чек коррекции {0}", mi.Text), "Введите сумму:");
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DAccess.CashTerminal.FNBuildCorrectionReceipt(decimal.Parse(frm.Value), checkType);
            }
        }

        private void miCancelCheck_Click(object sender, EventArgs e)
        {
            DAccess.CashTerminal.CancelCheck();
        }

        private void menuItemReestrPorverka_Click(object sender, EventArgs e)
        {
            FrmReestrPoverka f = new FrmReestrPoverka();
            f.Show();
        }
    }
}
